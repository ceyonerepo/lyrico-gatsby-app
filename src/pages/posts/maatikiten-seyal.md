---
title: "maatikiten song lyrics"
album: "Seyal"
artist: "Siddharth Vipin"
lyricist: "Siddharth Vipin"
director: "Ravi Abbulu"
path: "/albums/seyal-lyrics"
song: "Maatikiten"
image: ../../images/albumart/seyal.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wODlOYS1kA0"
type: "love"
singers:
  - Aysha Farheen
  - Prabha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">convent kaalam thodangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="convent kaalam thodangi"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu mayangi lo lonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu mayangi lo lonu"/>
</div>
<div class="lyrico-lyrics-wrapper">than follow senju jollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than follow senju jollu"/>
</div>
<div class="lyrico-lyrics-wrapper">valinchu road nananji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valinchu road nananji"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvula thirinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvula thirinchi "/>
</div>
<div class="lyrico-lyrics-wrapper">towser kilinchi day night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="towser kilinchi day night"/>
</div>
<div class="lyrico-lyrics-wrapper">ah daily nanum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah daily nanum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">ninachu odambu melinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninachu odambu melinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ok sollita thala kaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok sollita thala kaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">puriyalaye ada ye amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyalaye ada ye amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pala naala sketch ayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala naala sketch ayum"/>
</div>
<div class="lyrico-lyrics-wrapper">poten root iyum viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poten root iyum viten"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku venum unnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku venum unnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">heart signal ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart signal ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumba vantha pagachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumba vantha pagachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna kadavul potta link
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna kadavul potta link"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than conform panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than conform panna"/>
</div>
<div class="lyrico-lyrics-wrapper">angel nee than lover aaita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angel nee than lover aaita"/>
</div>
<div class="lyrico-lyrics-wrapper">thala kaalu puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala kaalu puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ayyo ye amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ayyo ye amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sari panni kaati nalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari panni kaati nalla "/>
</div>
<div class="lyrico-lyrics-wrapper">semma love un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma love un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kaluvi than oothinala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluvi than oothinala"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal jasthi un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal jasthi un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalal ethi thalla kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalal ethi thalla kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum foodball illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum foodball illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paithiyama follow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paithiyama follow"/>
</div>
<div class="lyrico-lyrics-wrapper">panni bike tire thenjuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni bike tire thenjuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi matikiten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi matikiten "/>
</div>
<div class="lyrico-lyrics-wrapper">ipo mulikira di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo mulikira di"/>
</div>
<div class="lyrico-lyrics-wrapper">tholla thangalada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholla thangalada "/>
</div>
<div class="lyrico-lyrics-wrapper">illa yenguradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa yenguradi"/>
</div>
<div class="lyrico-lyrics-wrapper">romba vaiyra da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba vaiyra da"/>
</div>
<div class="lyrico-lyrics-wrapper">iyo aluthura di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyo aluthura di"/>
</div>
<div class="lyrico-lyrics-wrapper">ena theendina da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena theendina da"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum virumbuna di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum virumbuna di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">padutha en dream la 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padutha en dream la "/>
</div>
<div class="lyrico-lyrics-wrapper">nee than mulicha bed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than mulicha bed"/>
</div>
<div class="lyrico-lyrics-wrapper">coffe um nee than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coffe um nee than "/>
</div>
<div class="lyrico-lyrics-wrapper">cell phone la wall paper
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone la wall paper"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than ring tone la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than ring tone la"/>
</div>
<div class="lyrico-lyrics-wrapper">un korata athu than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un korata athu than "/>
</div>
<div class="lyrico-lyrics-wrapper">siricha athu simponi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siricha athu simponi "/>
</div>
<div class="lyrico-lyrics-wrapper">kacheri muracha athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kacheri muracha athu"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum venam kandravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum venam kandravi"/>
</div>
<div class="lyrico-lyrics-wrapper">puducha propose pannitom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puducha propose pannitom "/>
</div>
<div class="lyrico-lyrics-wrapper">buildup ne panura yendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buildup ne panura yendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna asathida theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna asathida theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum en love kuraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum en love kuraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">unna correct pannen kadaisila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna correct pannen kadaisila"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum ipa kooda polamburene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum ipa kooda polamburene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi matikiten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi matikiten "/>
</div>
<div class="lyrico-lyrics-wrapper">ipo mulikira di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo mulikira di"/>
</div>
<div class="lyrico-lyrics-wrapper">tholla thangalada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholla thangalada "/>
</div>
<div class="lyrico-lyrics-wrapper">illa yenguradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa yenguradi"/>
</div>
<div class="lyrico-lyrics-wrapper">romba vaiyra da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba vaiyra da"/>
</div>
<div class="lyrico-lyrics-wrapper">iyo aluthura di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyo aluthura di"/>
</div>
<div class="lyrico-lyrics-wrapper">ena theendina da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena theendina da"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum virumbuna di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum virumbuna di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">torchure athu ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="torchure athu ennoda"/>
</div>
<div class="lyrico-lyrics-wrapper">duty darling unna pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duty darling unna pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">ooty scooty nee ootuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooty scooty nee ootuna "/>
</div>
<div class="lyrico-lyrics-wrapper">beauty nasuka nenanju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beauty nasuka nenanju "/>
</div>
<div class="lyrico-lyrics-wrapper">wheel la maati veruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wheel la maati veruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">ne velagi ponalum poruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne velagi ponalum poruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">site adipen aanalum kaduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="site adipen aanalum kaduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kandapadi thitunalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kandapadi thitunalum"/>
</div>
<div class="lyrico-lyrics-wrapper">following panuvendi epothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="following panuvendi epothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">entha ponnaiyum pidikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha ponnaiyum pidikala"/>
</div>
<div class="lyrico-lyrics-wrapper">un pola evalaiyum rasikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pola evalaiyum rasikala"/>
</div>
<div class="lyrico-lyrics-wrapper">unna correct pannen kadaisila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna correct pannen kadaisila"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum ipo kooda polamburane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum ipo kooda polamburane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi matikiten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi matikiten "/>
</div>
<div class="lyrico-lyrics-wrapper">ipo mulikira di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo mulikira di"/>
</div>
<div class="lyrico-lyrics-wrapper">tholla thangalada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholla thangalada "/>
</div>
<div class="lyrico-lyrics-wrapper">illa yenguradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa yenguradi"/>
</div>
<div class="lyrico-lyrics-wrapper">romba vaiyra da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba vaiyra da"/>
</div>
<div class="lyrico-lyrics-wrapper">iyo aluthura di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyo aluthura di"/>
</div>
<div class="lyrico-lyrics-wrapper">ena theendina da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena theendina da"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum virumbuna di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum virumbuna di"/>
</div>
</pre>
