---
title: "odakara oyilaga song lyrics"
album: "Naruvi"
artist: "Christy"
lyricist: "Yugabharathi"
director: "Raja Muralidharan"
path: "/albums/naruvi-lyrics"
song: "Odakara Oyilaga"
image: ../../images/albumart/naruvi.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pPfPxBiRCVU"
type: "happy"
singers:
  - Maghizini Manimaran
  - Hariharasudhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kothangudi valiba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothangudi valiba "/>
</div>
<div class="lyrico-lyrics-wrapper">pasanga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasanga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nimirthu parungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirthu parungada"/>
</div>
<div class="lyrico-lyrics-wrapper">naa vaanam yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa vaanam yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">paada poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paada poren"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu kelungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu kelungada"/>
</div>
<div class="lyrico-lyrics-wrapper">thillu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thillu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">kuda vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuda vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadungada ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadungada ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthattam podungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthattam podungada"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthattam podungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthattam podungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei niruthudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei niruthudi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru oorla vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru oorla vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">yara thillu irukanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yara thillu irukanu "/>
</div>
<div class="lyrico-lyrics-wrapper">kekura thaatha ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekura thaatha ne"/>
</div>
<div class="lyrico-lyrics-wrapper">ukaru ivala nan pathukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukaru ivala nan pathukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odakara oyilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odakara oyilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aada varum kuyilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada varum kuyilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">koda veyil kuliraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koda veyil kuliraga"/>
</div>
<div class="lyrico-lyrics-wrapper">pathene pathi azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathene pathi azhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kola podi niramaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola podi niramaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kova pazha rusiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kova pazha rusiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">vaara iva thaniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaara iva thaniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">vera enna venu pazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera enna venu pazhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru nodi kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodi kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">pirive venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirive venam"/>
</div>
<div class="lyrico-lyrics-wrapper">inanju irunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inanju irunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">pagale venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagale venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavula kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">udaiye venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiye venam"/>
</div>
<div class="lyrico-lyrics-wrapper">katha kathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha kathappa"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vidave venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vidave venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam poora"/>
</div>
<div class="lyrico-lyrics-wrapper">kariyira aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kariyira aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanava moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanava moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">thara venum parisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara venum parisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bodhai yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai yera"/>
</div>
<div class="lyrico-lyrics-wrapper">pulugura meesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulugura meesai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinju pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinju pona"/>
</div>
<div class="lyrico-lyrics-wrapper">vandi mesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandi mesai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podum aatathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podum aatathula"/>
</div>
<div class="lyrico-lyrics-wrapper">oore ezhunthu nikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore ezhunthu nikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aadum aatathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum aatathila"/>
</div>
<div class="lyrico-lyrics-wrapper">ilasu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilasu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">siragu adikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragu adikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odakara oyilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odakara oyilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aada varum kuyilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada varum kuyilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">koda veyil kuliraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koda veyil kuliraga"/>
</div>
<div class="lyrico-lyrics-wrapper">pathene pathi azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathene pathi azhaga"/>
</div>
</pre>
