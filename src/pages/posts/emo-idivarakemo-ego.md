---
title: "emo idivarakemo song lyrics"
album: "Ego"
artist: "Sai Karthik "
lyricist: "Balaji"
director: "RV Subramanyam"
path: "/albums/ego-lyrics"
song: "Emo Idivarakemo"
image: ../../images/albumart/ego.jpg
date: 2018-01-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7KVE86DBRB0"
type: "love"
singers:
  - Shreya Ghoshal
  - Dinkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">emo idhivarakemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo idhivarakemo"/>
</div>
<div class="lyrico-lyrics-wrapper">teliyani preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teliyani preme"/>
</div>
<div class="lyrico-lyrics-wrapper">kalipindemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalipindemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neelo nanu choosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo nanu choosa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vadilesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vadilesa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo prati aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo prati aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">neede telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neede telusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theesey prati swasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theesey prati swasha"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvai bratikesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvai bratikesa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaa migilindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaa migilindi"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve telusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">praname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="praname "/>
</div>
<div class="lyrico-lyrics-wrapper">evarani adigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarani adigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">pranam poyina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranam poyina "/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvani nenantunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvani nenantunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo vidipoyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo vidipoyamo"/>
</div>
<div class="lyrico-lyrics-wrapper">chivariki okatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chivariki okatai"/>
</div>
<div class="lyrico-lyrics-wrapper">manamayyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamayyamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo idi kalayemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo idi kalayemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anipinchela kalisamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anipinchela kalisamemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">padadhanukunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padadhanukunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">padipoya nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padipoya nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">gadipa prati sekanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadipa prati sekanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalalo vethikaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalo vethikaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">edaloo marichaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edaloo marichaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">eduruga nuvvunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduruga nuvvunte"/>
</div>
<div class="lyrico-lyrics-wrapper">emi cheppanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emi cheppanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">oopiri thakithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopiri thakithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ontari oohake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ontari oohake"/>
</div>
<div class="lyrico-lyrics-wrapper">pranam posthunnavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranam posthunnavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo adi manasemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo adi manasemo"/>
</div>
<div class="lyrico-lyrics-wrapper">kanipinchelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanipinchelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">okatayaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okatayaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo idi kalayemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo idi kalayemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anipinchela kalisamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anipinchela kalisamemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neelo nanu choosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo nanu choosa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vadilesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vadilesa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo prati aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo prati aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">neede telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neede telusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ikapai bugoolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ikapai bugoolam"/>
</div>
<div class="lyrico-lyrics-wrapper">tiragadha manakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tiragadha manakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">thiyyani kaburalak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiyyani kaburalak"/>
</div>
<div class="lyrico-lyrics-wrapper">daari choopaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daari choopaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adugai santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugai santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadavadha pratinimisam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadavadha pratinimisam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalise manasulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalise manasulake"/>
</div>
<div class="lyrico-lyrics-wrapper">haayi panchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haayi panchaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oopire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopire "/>
</div>
<div class="lyrico-lyrics-wrapper">marichina rojila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marichina rojila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasokatundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasokatundani"/>
</div>
<div class="lyrico-lyrics-wrapper">thelipenu ee premegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelipenu ee premegaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo idi manasemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo idi manasemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anipinchela okatayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anipinchela okatayamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emo idi kalayemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emo idi kalayemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anipinchela kalisamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anipinchela kalisamemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neelo nanu choosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo nanu choosa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne vadilesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne vadilesa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo prati aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo prati aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">neede telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neede telusa"/>
</div>
</pre>
