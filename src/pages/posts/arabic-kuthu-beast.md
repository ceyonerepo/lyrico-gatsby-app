---
title: "arabic kuthu song lyrics"
album: "Beast"
artist: "Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Nelson"
path: "/albums/beast-lyrics"
song: "Arabic Kuthu"
image: ../../images/albumart/beast.jpg
date: 2022-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8FAUEv_E_xQ"
type: "love"
singers:
  - Anirudh Ravichander
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pithadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pithadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Holi holi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Holi holi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakathula sirikkum rangoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakathula sirikkum rangoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkathula mayangura doli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkathula mayangura doli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathula avanum dhan gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathula avanum dhan gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly adipoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly adipoli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi vandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi vandhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh cutie nee sweety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh cutie nee sweety"/>
</div>
<div class="lyrico-lyrics-wrapper">Un beauty adhil maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un beauty adhil maati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa mellamma mellamma gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa mellamma mellamma gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha sollala sollala podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha sollala sollala podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal vaati padam kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal vaati padam kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vaati kanna kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vaati kanna kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Love ah konjama konjama yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love ah konjama konjama yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senjita senjita oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senjita senjita oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan yaaro sokka vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro sokka vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasukulla kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukulla kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola konjikittaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola konjikittaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love ah vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaro kick ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro kick ah vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vayasukulla mudhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vayasukulla mudhal mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Feel ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feel ah vachaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halamithi habibo halamithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo halamithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo halamithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo halamithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo halamithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo halamithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo halamithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo halamithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Habidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey beedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey beedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balibaadhi balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balibaadhi balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balihabeedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balihabeedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallae vallae balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallae vallae balihabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey beedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey beedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balibaadhi balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balibaadhi balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balihabeedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balihabeedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallae vallae balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallae vallae balihabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halwa ee balubabeedhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa ee balubabeedhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee balubabeedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee balubabeedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa ee balubabeedhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa ee balubabeedhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vathaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vathaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa ee balubabeedhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa ee balubabeedhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee balubabeedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee balubabeedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa ee balubabeedhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa ee balubabeedhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vathaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vathaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Holi holi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Holi holi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakathula sirikkum rangoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakathula sirikkum rangoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkathula mayangura doli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkathula mayangura doli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothathula avanum dhan gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathula avanum dhan gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly adipoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly adipoli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh cutie nee sweety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh cutie nee sweety"/>
</div>
<div class="lyrico-lyrics-wrapper">Un beauty adhil maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un beauty adhil maati"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa mellamma mellamma gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa mellamma mellamma gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha sollala sollala podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha sollala sollala podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal vaati padam kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal vaati padam kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vaati kanna kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vaati kanna kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-a konjama konjama yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-a konjama konjama yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senjita senjita oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senjita senjita oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan yaaro sokka vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro sokka vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasukulla kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukulla kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola konjikittanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola konjikittanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love ah vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaro kick ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro kick ah vachaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vayasukulla mudhal mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vayasukulla mudhal mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Feel ah vachaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feel ah vachaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi mithii vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi mithii vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi habibo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pithadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala pitha pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala pitha pithadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malama pithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malama pithadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey beedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey beedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balibaadhi balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balibaadhi balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Balihabeedha balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balihabeedha balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallae vallae balihabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallae vallae balihabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi balihabi halamithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi balihabi halamithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Halamithi balibaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi balibaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halamithi habibo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halamithi habibo"/>
</div>
</pre>
