---
title: "love pana uttranum song lyrics"
album: "Paava Kadhaigal"
artist: "Anirudh Ravichander"
lyricist: "Stony Psyko"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Love Pana Uttranum"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qQvpIzGpZ3k"
type: "rap"
singers:
  - Stony Psyko
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kelu Love Pannaa Vuttaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Love Pannaa Vuttaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutturu Vuttu Vutturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutturu Vuttu Vutturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Pannaa Vuttaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannaa Vuttaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutturu Vuttu Vutturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutturu Vuttu Vutturu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei Love Pandra Aala Nee Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei Love Pandra Aala Nee Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peace Solli Vaazha Nee Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peace Solli Vaazha Nee Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Pandra Aala Nee Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pandra Aala Nee Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Kaaranam Kodu Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Kaaranam Kodu Neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonjila Morappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonjila Morappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovers Paarththaalae Yaeruthaa Kaduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovers Paarththaalae Yaeruthaa Kaduppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Veruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Veruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Thee Pola Paravura Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Thee Pola Paravura Neruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hold Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Hae Way The Second Nee Niruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Way The Second Nee Niruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Theencha Ennaththa Thiruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Theencha Ennaththa Thiruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Cross Pannaatha Limit-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Cross Pannaatha Limit-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Kidaikkum Unakku Rivet-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Kidaikkum Unakku Rivet-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinju Kolvathae Sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinju Kolvathae Sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi Madham Paarkka Maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Madham Paarkka Maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Piragu Naanga Commit-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Piragu Naanga Commit-Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottu Pottu Kadhal Adakka Mudiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu Pottu Kadhal Adakka Mudiyaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottunaalum Kadhala Thadukka Mudiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottunaalum Kadhala Thadukka Mudiyaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaallaalaiyum Kadhala Arukka Mudiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaallaalaiyum Kadhala Arukka Mudiyaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalargalai Azhiththaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalargalai Azhiththaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Ingae Azhiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Ingae Azhiyaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Pannaa Vuttaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannaa Vuttaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutturu Vuttu Vutturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutturu Vuttu Vutturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Pannaa Vuttaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannaa Vuttaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutturu Vuttu Vutturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutturu Vuttu Vutturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Pannaa Vuttaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannaa Vuttaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutturu Vuttu Vutturu Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutturu Vuttu Vutturu Aah"/>
</div>
</pre>
