---
title: "munnavittu song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "Bharani"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Munnavittu"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DNLLVrQHss8"
type: "love"
singers:
  - Jithin Raj
  - Sumithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ye munna vittu pinna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye munna vittu pinna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam yengi yengi pakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam yengi yengi pakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu adichu kekuraye kannaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu adichu kekuraye kannaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katikida aasa iruntha solaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katikida aasa iruntha solaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ok nu sonnan na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok nu sonnan na "/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda vaalthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda vaalthuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">maaten nu soonen na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaten nu soonen na"/>
</div>
<div class="lyrico-lyrics-wrapper">mannodu sernthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannodu sernthuduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye munna vittu pinna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye munna vittu pinna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam yengi yengi pakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam yengi yengi pakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu adichu kekuraye kannaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu adichu kekuraye kannaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katikida aasa iruntha solaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katikida aasa iruntha solaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pottu vachu poo muduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu vachu poo muduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu pothu than puduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu pothu than puduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">alli alli anaikum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli alli anaikum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">adangathathu athu adangathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangathathu athu adangathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">katti katti kathalichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti katti kathalichu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni ponna aatharichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni ponna aatharichu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu thodangum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu thodangum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyathathu athu puriyathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyathathu athu puriyathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula kavitha pola pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kavitha pola pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala yavum mathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala yavum mathura"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum ullathu enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum ullathu enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathu kathal thanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathu kathal thanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam irukum varaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam irukum varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuvum vaalum thanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvum vaalum thanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vaalum thanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vaalum thanaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye munna vittu pinna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye munna vittu pinna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam yengi yengi pakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam yengi yengi pakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu adichu kekuraye kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu adichu kekuraye kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katikida aasa iruntha solamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katikida aasa iruntha solamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannam rendum kathalikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannam rendum kathalikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kai rendum poo parikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai rendum poo parikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ullan thullu oodi aadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullan thullu oodi aadi "/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaduthu athu vilaiyaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaduthu athu vilaiyaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla thee pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla thee pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham unna kai pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham unna kai pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal konjam manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal konjam manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumaruthu athu thadumaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumaruthu athu thadumaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaiye naan nenachu yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaiye naan nenachu yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">unaka naan valuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaka naan valuren"/>
</div>
<div class="lyrico-lyrics-wrapper">nenachu kitathu nenachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenachu kitathu nenachu "/>
</div>
<div class="lyrico-lyrics-wrapper">kitathu unna thana di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitathu unna thana di"/>
</div>
<div class="lyrico-lyrics-wrapper">anachu kitathu anachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anachu kitathu anachu "/>
</div>
<div class="lyrico-lyrics-wrapper">kitathu vaala thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitathu vaala thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vaala thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vaala thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye munna vittu pinna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye munna vittu pinna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam yengi yengi pakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam yengi yengi pakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu adichu kekuraye kannaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu adichu kekuraye kannaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katikida aasa iruntha solaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katikida aasa iruntha solaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ok nu sonnan na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok nu sonnan na "/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda vaalthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda vaalthuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">maaten nu soonen na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaten nu soonen na"/>
</div>
<div class="lyrico-lyrics-wrapper">mannodu sernthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannodu sernthuduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye munna vittu pinna ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye munna vittu pinna ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam yengi yengi pakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam yengi yengi pakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu adichu kekuraye kannaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu adichu kekuraye kannaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katikida aasa iruntha solaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katikida aasa iruntha solaiya"/>
</div>
</pre>
