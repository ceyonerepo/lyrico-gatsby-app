---
title: "hey yela sad version song lyrics"
album: "Hippi"
artist: "Nivas K. Prasanna"
lyricist: "Anantha Sriram"
director: "Krishna"
path: "/albums/hippi-lyrics"
song: "Hey Yela Sad Version"
image: ../../images/albumart/hippi.jpg
date: 2019-06-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ayZYjGvwLok"
type: "sad"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey elaa etepu choosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey elaa etepu choosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee kalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee kalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">ninchundi needalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninchundi needalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey elaa etepu vellinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey elaa etepu vellinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee alaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee alaa "/>
</div>
<div class="lyrico-lyrics-wrapper">munchindi nijamulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munchindi nijamulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraasane bharinchaleni mounamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraasane bharinchaleni mounamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nireekshane sahinchaleni praanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nireekshane sahinchaleni praanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichaanu nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichaanu nenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey elaa etepu vellinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey elaa etepu vellinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee alaa munchindi nijamulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee alaa munchindi nijamulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalidaaka cherinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalidaaka cherinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saagaraanni tosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagaraanni tosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kshanam sarenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kshanam sarenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagaram maro teeraanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaram maro teeraanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Daatene ivvaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daatene ivvaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa edaa edaari endamaavilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa edaa edaari endamaavilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarene ee chotane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarene ee chotane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ato ito prayaanamevaipuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ato ito prayaanamevaipuko"/>
</div>
</pre>
