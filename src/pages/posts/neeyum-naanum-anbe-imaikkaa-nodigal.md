---
title: "neeyum naanum anbe song lyrics"
album: "Imaikkaa Nodigal"
artist: "Hiphop Tamizha"
lyricist: "Kabilan"
director: "R. Ajay Gnanamuthu"
path: "/albums/imaikkaa-nodigal-lyrics"
song: "Neeyum Naanum Anbe"
image: ../../images/albumart/imaikkaa-nodigal.jpg
date: 2018-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dImiR3Sr8Wo"
type: "love"
singers:
  - Raghu Dixit
  - Sathyaprakash
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayul Kaalam Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul Kaalam Yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Neeye Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Neeye Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal Naangum Porthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Naangum Porthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Naam Thoongalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Naam Thoongalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korthukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korthukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paadhai Nee En Paadham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadhai Nee En Paadham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pogum Dhooram Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Dhooram Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaanam Nee En Bhoomi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanam Nee En Bhoomi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aadhi Andham Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aadhi Andham Neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paadhai Nee En Paadham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadhai Nee En Paadham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pogum Dhooram Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Dhooram Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaanam Nee En Bhoomi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanam Nee En Bhoomi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aadhi Andham Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aadhi Andham Neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimozhi Poley Nee Vaazhvaai Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimozhi Poley Nee Vaazhvaai Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhal Pirindhaalum Veezhven Mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhal Pirindhaalum Veezhven Mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini Poove Un Kadhal Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini Poove Un Kadhal Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Kandenae Ennai Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Kandenae Ennai Unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaamadhamaai Unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamadhamaai Unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimadiyaai Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimadiyaai Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoongavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoongavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thevaiyai Naan Theerkkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thevaiyai Naan Theerkkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Venneeril Meenaai Neendhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venneeril Meenaai Neendhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalai Kadan Vaangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalai Kadan Vaangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Naane Thaanguvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naane Thaanguvaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paadhiyum En Meedhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paadhiyum En Meedhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondredhaan Endru Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondredhaan Endru Vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangalil Neer Sindhinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangalil Neer Sindhinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Appothae Seththu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appothae Seththu Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalai Ora Pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Ora Pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaindhu Nammai Paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaindhu Nammai Paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thevai illai Penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thevai illai Penney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraga Vaalalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraga Vaalalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paathai Nee En Paatham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathai Nee En Paatham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pogum Dhooram Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Dhooram Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaanam Nee En Boomi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanam Nee En Boomi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aadhi Andham Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aadhi Andham Neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paathai Nee En Paatham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathai Nee En Paatham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pogum Dhooram Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Dhooram Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaanam Nee En Boomi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanam Nee En Boomi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aadhi Andham Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aadhi Andham Neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Korththukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Korththukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Ellai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Ellai Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhalaam"/>
</div>
</pre>
