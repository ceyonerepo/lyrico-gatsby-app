---
title: "ra rammandi uru song lyrics"
album: "Godse"
artist: "Sunil Kashyap"
lyricist: "Saraswati Putra Ramajogayya Sastry"
director: "Gopiganesh Pattabhi"
path: "/albums/godse-lyrics"
song: "Ra Rammandi Uru"
image: ../../images/albumart/godse.jpg
date: 2022-06-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZjW0vQQTqyM"
type: "happy"
singers:
  -	Ram Miriyala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ra Rammandi Uru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Rammandi Uru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayyindi Husharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayyindi Husharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagamandukundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagamandukundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakaala Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakaala Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachhanaina Chelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachhanaina Chelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palle Parisaraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Parisaraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthakaalamainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthakaalamainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvaledhu Naa Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvaledhu Naa Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gattu Polimerallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattu Polimerallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti Raadaarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti Raadaarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa Adugu Pettagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Adugu Pettagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulakarinthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulakarinthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pairu Pantala Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairu Pantala Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Thadamangaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Thadamangaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Puttinatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Puttinatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Ooyaloogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Ooyaloogene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Odi Kore Chantipaapadila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Odi Kore Chantipaapadila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Chera Pilichindhi Ee Seema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Chera Pilichindhi Ee Seema"/>
</div>
<div class="lyrico-lyrics-wrapper">Komma Remmalugaa Entha Etthuna Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komma Remmalugaa Entha Etthuna Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Peru Moolamainadhi Chirunaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Peru Moolamainadhi Chirunaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Yelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelo Yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idigo Perigina Illu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idigo Perigina Illu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Thirigina Veedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Thirigina Veedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Kshemamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Kshemamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Palakarinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Palakarinchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adigo Chadivina School
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigo Chadivina School"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Ne Gelichina Ground
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ne Gelichina Ground"/>
</div>
<div class="lyrico-lyrics-wrapper">Marala Nannu Choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marala Nannu Choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu Paravasinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu Paravasinchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ika Chaal Chaalu Ee Dhooraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Chaal Chaalu Ee Dhooraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalusundhaam Randi Nesthaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalusundhaam Randi Nesthaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gillikajjaalu Konte Saradaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gillikajjaalu Konte Saradaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Gurthu Chesukundaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Gurthu Chesukundaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naati Anubhavaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naati Anubhavaalu"/>
</div>
</pre>
