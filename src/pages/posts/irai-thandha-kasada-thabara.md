---
title: "irai thandha song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Irai Thandha"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wxDKRaJ_1To"
type: "emotional"
singers:
  - Deepthi Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">irai thantha kodai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai thantha kodai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvin uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvin uyir neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">maraiyatha oli neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraiyatha oli neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">madi meedhu thuyilvaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi meedhu thuyilvaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unai petrathanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai petrathanale"/>
</div>
<div class="lyrico-lyrics-wrapper">naan arputhamanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan arputhamanen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vetridamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vetridamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vida maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irai thantha kodai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai thantha kodai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvin uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvin uyir neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thavamai thavamirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavamai thavamirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pala naal sumanthirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala naal sumanthirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">uthithathu oli vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthithathu oli vilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">unathannai vayitrinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathannai vayitrinile"/>
</div>
<div class="lyrico-lyrics-wrapper">vali yethum kodukamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali yethum kodukamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vali kandu pirantha pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali kandu pirantha pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">veli vantha pinnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veli vantha pinnale"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinthathu en ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinthathu en ulagu"/>
</div>
<div class="lyrico-lyrics-wrapper">urave nee irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urave nee irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire en uyirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyirai"/>
</div>
<div class="lyrico-lyrics-wrapper">aagum aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagum aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irai thantha kodai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai thantha kodai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvin uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvin uyir neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai ninaipathilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai ninaipathilai"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum un ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum un ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">munnai piravi thotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnai piravi thotu"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinthathu nam uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinthathu nam uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakethum illai kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakethum illai kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vitu naan piriyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vitu naan piriyen"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena yethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena yethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyirai naan maraven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyirai naan maraven"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivan enaku alitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan enaku alitha"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevan jeevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevan jeevan"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum ennudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum ennudan"/>
</div>
<div class="lyrico-lyrics-wrapper">ondragum aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondragum aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irai thantha kodai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai thantha kodai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvin uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvin uyir neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unai petrathanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai petrathanale"/>
</div>
<div class="lyrico-lyrics-wrapper">naan arputhamanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan arputhamanen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vetridamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vetridamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vida maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irai thantha kodai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai thantha kodai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvin uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvin uyir neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir neeye"/>
</div>
</pre>
