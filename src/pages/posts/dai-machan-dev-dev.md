---
title: "dai machan dev song lyrics"
album: "Dev"
artist: "Harris Jayaraj"
lyricist: "Vivek"
director: "Rajath Ravishankar"
path: "/albums/dev-lyrics"
song: "Dai Machan Dev"
image: ../../images/albumart/dev.jpg
date: 2019-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CWw0AXJzyjk"
type: "happy"
singers:
  - Naresh Iyer
  - Velmurugan
  - Malavika Manoj
  - Deepika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machi Rock Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Rock Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththa Kilikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththa Kilikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Parakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Parakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Therikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Therikkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa Palasa Idhil Yaar Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Palasa Idhil Yaar Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakke Sollum Indha Journey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakke Sollum Indha Journey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Cup Time Mai Eduthu Sip Adichidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Cup Time Mai Eduthu Sip Adichidu"/>
</div>
<div class="lyrico-lyrics-wrapper">One Sec Whole Long Life-ah Konjum Rusichidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Sec Whole Long Life-ah Konjum Rusichidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Machan Dev Riding Away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Machan Dev Riding Away"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Machan Dev Crazy-em Brave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Machan Dev Crazy-em Brave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One Trip-la Friendship-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Trip-la Friendship-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkai Virikkum Aeroplanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Virikkum Aeroplanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thost-oda Thaan One Date-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thost-oda Thaan One Date-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukku Idhu Honeymoon-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukku Idhu Honeymoon-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Engal Vazhiye Enjoy Ejamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Engal Vazhiye Enjoy Ejamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Inime Ulagam Engal Vasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Inime Ulagam Engal Vasama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Yenggira Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Yenggira Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Machine-la Nee Yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machine-la Nee Yeralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Rock Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Rock Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththa Kilikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththa Kilikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Parakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Parakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Therikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Therikkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Varum Nodi Magizhvathu Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varum Nodi Magizhvathu Yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai Vittu Neeyum Nanaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Vittu Neeyum Nanaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathanai Mella Paranthida Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathanai Mella Paranthida Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal Athu Varaiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Athu Varaiyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Varum Endru Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Varum Endru Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Ingum Poopathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Ingum Poopathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Rasippom Vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Rasippom Vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Yaarukkum Kaapathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Yaarukkum Kaapathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Rock Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Rock Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththa Kilikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththa Kilikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Parakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Parakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Therikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Therikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa Palasa Idhil Yaar Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Palasa Idhil Yaar Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakke Sollum Indha Journey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakke Sollum Indha Journey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Cup Time Mai Eduthu Sip Adichidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Cup Time Mai Eduthu Sip Adichidu"/>
</div>
<div class="lyrico-lyrics-wrapper">One Sec Hold Long Life-eh Konjum Rusichidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Sec Hold Long Life-eh Konjum Rusichidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Machan Dev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Machan Dev"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan Riding Away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Riding Away"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Machan Dev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Machan Dev"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan Crazy-em Brave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Crazy-em Brave"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Machan Dev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Machan Dev"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Hey Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Machan Dev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Machan Dev"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Hey Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Machan Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Hey Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Machan Hey Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Machan Hey Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Machan Dev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Machan Dev"/>
</div>
</pre>
