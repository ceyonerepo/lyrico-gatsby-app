---
title: 'polakattum para para song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Vishnu Edavan'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Polakattum Para Para'
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/dZN4TD9Ane0'
type: 'mass'
singers: 
- Shanthosh Narayanan
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Ungalukku rendu nimisham
<input type="checkbox" class="lyrico-select-lyric-line" value="Ungalukku rendu nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Time tharen
<input type="checkbox" class="lyrico-select-lyric-line" value="Time tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja enna konnuttu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudinja enna konnuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala kaapathikonga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ungala kaapathikonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hahahahahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hmm hmm hmm hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmm hmm hmm hmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Polakattum para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Polakattum para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikattum alappara
<input type="checkbox" class="lyrico-select-lyric-line" value="Therikattum alappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pada pada
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma pada pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Velukkum thara thara
<input type="checkbox" class="lyrico-select-lyric-line" value="Velukkum thara thara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thappaadhu thappadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappaadhu thappadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu aduchaa enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappu aduchaa enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikaadha narambu illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikaadha narambu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottu aduchaa…adraaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kottu aduchaa…adraaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vidiyira vara vara
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyira vara vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Alarattum thara thara
<input type="checkbox" class="lyrico-select-lyric-line" value="Alarattum thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyura vara vara
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiyura vara vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattatha kora kora
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattatha kora kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yemanukkum saavundu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemanukkum saavundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan moracha adadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan moracha adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambhi soodam yethu saami
<input type="checkbox" class="lyrico-select-lyric-line" value="Thambhi soodam yethu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan sirichaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan sirichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooru mulukka sandhu bondhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooru mulukka sandhu bondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi paathukka nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Thedi paathukka nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanai ethukka satham kodukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivanai ethukka satham kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Komban illai ini…mavanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Komban illai ini…mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahahahahahah
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahahahah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy heyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hmm hmm hmm hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmm hmm hmm hmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adikkira adiyil ada
<input type="checkbox" class="lyrico-select-lyric-line" value="Adikkira adiyil ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavulu kilinji thongattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavulu kilinji thongattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichathu yaaru verum satham
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichathu yaaru verum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu sollattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu sollattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkira adiyil ada
<input type="checkbox" class="lyrico-select-lyric-line" value="Adikkira adiyil ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavulu kilinji thongattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavulu kilinji thongattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichathu yaaru verum satham
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichathu yaaru verum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu sollattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu sollattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bodhai thelinju pattai edukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Bodhai thelinju pattai edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilambhi varavan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Kilambhi varavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thonga vuttu thozha urippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna thonga vuttu thozha urippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli vechavan daa…vaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Solli vechavan daa…vaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Polakattum para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Polakattum para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikattum alappara
<input type="checkbox" class="lyrico-select-lyric-line" value="Therikattum alappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pada pada
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma pada pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Velukkum thara thara
<input type="checkbox" class="lyrico-select-lyric-line" value="Velukkum thara thara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thappaadhu thappadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappaadhu thappadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu aduchaa enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappu aduchaa enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikaadha narambu illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikaadha narambu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottu aduchaa…adraaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kottu aduchaa…adraaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vidiyira vara vara
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyira vara vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Alarattum thara thara
<input type="checkbox" class="lyrico-select-lyric-line" value="Alarattum thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyura vara vara
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiyura vara vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattatha kora kora
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattatha kora kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yemanukkum saavundu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemanukkum saavundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan moracha adadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan moracha adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambhi soodam yethu saami
<input type="checkbox" class="lyrico-select-lyric-line" value="Thambhi soodam yethu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan sirichaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan sirichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahahahahahah
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahahahah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy heyyya
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy heyyya"/>
</div>
</pre>