---
title: "pattaamboochi pola song lyrics"
album: "Sarkarai Thookalai Oru Punnagai"
artist: "Rajesh Appukuttan"
lyricist: "Kattalai Jeya"
director: "Mahesh Padmanabhan "
path: "/albums/sarkarai-thookalai-oru-punnagai-lyrics"
song: "Pattaamboochi Pola"
image: ../../images/albumart/sarkarai-thookalai-oru-punnagai.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JRlpZW70YUg"
type: "love"
singers:
  - Alap Raju
  - Suchitra Krishnamurthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pattam poochi pola suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pola suthum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna patchi aalai kothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna patchi aalai kothum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettu kathi parvai kuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettu kathi parvai kuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kittum poovin motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kittum poovin motham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paarka than pesa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarka than pesa than"/>
</div>
<div class="lyrico-lyrics-wrapper">theenda than thoonda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theenda than thoonda than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala than saaga than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala than saaga than"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal varthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal varthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theerentum aasaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerentum aasaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ooratum meesaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooratum meesaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thotal kaalai kuda rathiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thotal kaalai kuda rathiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">thee thee sudamal kulirkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee thee sudamal kulirkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">athe en uravaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athe en uravaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">poo poo parikamal thenum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo poo parikamal thenum "/>
</div>
<div class="lyrico-lyrics-wrapper">varuthe athe ethir vanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuthe athe ethir vanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nilavinil oru paguthiyai vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavinil oru paguthiyai vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">manangalai maru paguthiyil aduki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manangalai maru paguthiyil aduki"/>
</div>
<div class="lyrico-lyrics-wrapper">oru virupam nathiyinil nanaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru virupam nathiyinil nanaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaluvi kaluvi jolikum alage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluvi kaluvi jolikum alage"/>
</div>
<div class="lyrico-lyrics-wrapper">kallukurve mullum palame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallukurve mullum palame"/>
</div>
<div class="lyrico-lyrics-wrapper">kollum alage enna tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollum alage enna tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">mutha kadale vithai tharuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutha kadale vithai tharuve"/>
</div>
<div class="lyrico-lyrics-wrapper">methai ooramai ennai tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methai ooramai ennai tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">megathai urasi vaanathai vilungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathai urasi vaanathai vilungi"/>
</div>
<div class="lyrico-lyrics-wrapper">thagathai thanikum paravai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagathai thanikum paravai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil mogathai veruthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil mogathai veruthi "/>
</div>
<div class="lyrico-lyrics-wrapper">kovathai parithu thegathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovathai parithu thegathai "/>
</div>
<div class="lyrico-lyrics-wrapper">kalaum kalavai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaum kalavai naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattam poochi pola suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pola suthum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna patchi aalai kothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna patchi aalai kothum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettu kathi parvai kuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettu kathi parvai kuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kittum poovin motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kittum poovin motham"/>
</div>
</pre>
