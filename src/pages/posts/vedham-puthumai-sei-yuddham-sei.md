---
title: "vedham pudhumai sei song lyrics"
album: "Yuddham Sei"
artist: "Krishnakumar"
lyricist: "Mahakavi Bharathiyar"
director: "Mysskin"
path: "/albums/yuddham-sei-lyrics"
song: "Vedham Pudhumai Sei"
image: ../../images/albumart/yuddham-sei.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sMS3hbsAk5o"
type: "melody"
singers:
  - Mysskin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaedham pudhumai sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaedham pudhumai sei"/>
</div>
<div class="lyrico-lyrics-wrapper">vaedham vaedham pudhumai sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaedham vaedham pudhumai sei"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiyaa silaiyaa nenjikkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyaa silaiyaa nenjikkul"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiyaa silaiyaa nenjikkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyaa silaiyaa nenjikkul"/>
</div>
<div class="lyrico-lyrics-wrapper">kodumai edhirththu nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodumai edhirththu nil"/>
</div>
<div class="lyrico-lyrics-wrapper">kodumai kodumai edhirththu nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodumai kodumai edhirththu nil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaettidum thunindhu nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaettidum thunindhu nil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaettidum kaettidum thunindhu nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaettidum kaettidum thunindhu nil"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadhaal kondadhaal thirundhi nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadhaal kondadhaal thirundhi nil"/>
</div></pre>
