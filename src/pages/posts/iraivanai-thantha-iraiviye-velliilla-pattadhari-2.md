---
title: "iraivanai thantha iraiviye song lyrics"
album: "Vellaiilla Pattadhari 2"
artist: "Sean Roldan"
lyricist: "Dhanush"
director: "Soundarya Rajinikanth"
path: "/albums/vellaiilla-pattadhari-2-lyrics"
song: "Iraivanai Thantha Iraiviye"
image: ../../images/albumart/vellaiilla-pattadhari-2.jpg
date: 2017-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cKSuLHyJFlE"
type: "Love"
singers:
  - Sean Roldan
  - M. M. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iraivanai Thandha Iraiviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai Thandha Iraiviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulunil Kaanum Oviyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulunil Kaanum Oviyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanaai Thandha Iraiviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanaai Thandha Iraiviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulunil Kaanum Oviyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulunil Kaanum Oviyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuyarilum Ennai Thaangum Deviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarilum Ennai Thaangum Deviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirvarai Undhan Madiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirvarai Undhan Madiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigalai Pokkum Kaadhal Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalai Pokkum Kaadhal Paarvayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagame Kaalin Adiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame Kaalin Adiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Uyire Undhan Porule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Uyire Undhan Porule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Naan Alli Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Naan Alli Tharavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannane Kannane Endhan Mannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannane Kannane Endhan Mannane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Sorgaththai Kaiyil Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgaththai Kaiyil Tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulle Ennavo Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulle Ennavo Sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulle Kuyil Paadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulle Kuyil Paadudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiramai Kangalo Izhukkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiramai Kangalo Izhukkudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathellam Ingu Pookudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathellam Ingu Pookudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhi Thayum Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Thayum Neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraimeedhu Neeradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraimeedhu Neeradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthamidu Muthamizhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamidu Muthamizhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanaiyum Ennavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum Ennavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhile Thean Paayudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhile Thean Paayudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanai Thandha Iraiviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai Thandha Iraiviyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanaai Thandha Iraiviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanaai Thandha Iraiviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulunil Kaanum Oviyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulunil Kaanum Oviyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuyarilum Ennai Thaangum Deviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarilum Ennai Thaangum Deviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirvarai Undhan Madiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirvarai Undhan Madiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigalai Pokkum Kaadhal Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalai Pokkum Kaadhal Paarvayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagame Kaalin Adiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame Kaalin Adiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Uyire Undhan Porule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Uyire Undhan Porule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Naan Alli Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Naan Alli Tharavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannane Kannane Endhan Mannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannane Kannane Endhan Mannane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorgathai Kaiyil Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathai Kaiyil Tharavaa"/>
</div>
</pre>
