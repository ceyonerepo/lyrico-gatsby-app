---
title: "janani song lyrics"
album: "RRR Telugu"
artist: "Maragathamani"
lyricist: "	MM Keeravaani"
director: "S.S. Rajamouli "
path: "/albums/rrr-telugu-lyrics"
song: "Janani"
image: ../../images/albumart/rrr-telugu.jpg
date: 2022-03-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xdpJWh5u-EI"
type: "mass"
singers:
  - MM Keeravaani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Janani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya Bharata Janani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya Bharata Janani"/>
</div>
<div class="lyrico-lyrics-wrapper">Janani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paadha Dhuli Thilakamto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paadha Dhuli Thilakamto"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaram Prakashamavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaram Prakashamavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nishkalanka Charitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nishkalanka Charitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Suprabhatamanavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Suprabhatamanavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Neeli Neeli Gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neeli Neeli Gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Satha Vispulinga Mayamayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satha Vispulinga Mayamayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Hawana Ganga Dhvanuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Hawana Ganga Dhvanuley"/>
</div>
<div class="lyrico-lyrics-wrapper">Harinaasha Garjanamulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harinaasha Garjanamulai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan Niswanaalu Naa Seda Theerchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Niswanaalu Naa Seda Theerchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Laali Jolalavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Laali Jolalavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani"/>
</div>
</pre>
