---
title: "patta kelu vaartha naalu song lyrics"
album: "C/o Kadhal"
artist: "Sweekar Agasthi"
lyricist: "Karthik Netha"
director: "Hemambar Jasti"
path: "/albums/co-kadhal-lyrics"
song: "Paata Kelu Vaartha Naalu"
image: ../../images/albumart/co-kadhal.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zfRQUVJsqgk"
type: "Enjoy"
singers:
  - Gana Mani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">patta kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vartha nalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vartha nalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuni kadaiyila poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuni kadaiyila poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patta kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vartha nalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vartha nalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuni kadaiyila poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuni kadaiyila poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">namma thuni kadaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thuni kadaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathu oram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu oram "/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyadikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyadikum "/>
</div>
<div class="lyrico-lyrics-wrapper">kammalu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammalu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuku alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuku alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">udambula antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambula antha"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam medu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam medu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ava elloraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava elloraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">siripa vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siripa vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ana maripoi thaviduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ana maripoi thaviduva"/>
</div>
<div class="lyrico-lyrics-wrapper">inoru katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inoru katchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emma sugaru BP
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma sugaru BP"/>
</div>
<div class="lyrico-lyrics-wrapper">ehtuvumila vayasannalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ehtuvumila vayasannalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam ana kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam ana kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">love pannuven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love pannuven "/>
</div>
<div class="lyrico-lyrics-wrapper">set analum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="set analum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emma sugaru BP
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma sugaru BP"/>
</div>
<div class="lyrico-lyrics-wrapper">ehtuvumila vayasannalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ehtuvumila vayasannalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam ana kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam ana kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">love pannuven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love pannuven "/>
</div>
<div class="lyrico-lyrics-wrapper">set analum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="set analum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patta kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vartha nalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vartha nalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuni kadaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuni kadaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha thuni kadaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thuni kadaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha thuni kadaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thuni kadaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">poduranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduranga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi saleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi saleu"/>
</div>
</pre>
