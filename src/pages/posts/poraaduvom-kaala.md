---
title: "poraaduvom song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Dopeadelicz - Logan"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Poraaduvom"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YsEGrAz5P2A"
type: "mass"
singers:
  - Dopeadelicz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iruttil vazhgirai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttil vazhgirai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruttu nambikkaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruttu nambikkaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veragil venthu nee saavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veragil venthu nee saavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanikkai peril ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanikkai peril ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal silaikkum lanjam kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal silaikkum lanjam kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi kumiyuthu undiyalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kumiyuthu undiyalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattil aanaal panjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattil aanaal panjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirathaalum mathaththaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirathaalum mathaththaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirinthu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthu vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Manitha abimaanathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitha abimaanathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namellaam maranthu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namellaam maranthu vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasin thiruvilaiyadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasin thiruvilaiyadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu naam mayangi vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu naam mayangi vittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaiyalam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyalam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaithu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithu vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai izhanthu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai izhanthu vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam iranthu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam iranthu vittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alatchiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alatchiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhai uyir endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai uyir endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Alatchiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alatchiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panamdhan noyin maruthuvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panamdhan noyin maruthuvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthuvamanaiyin arasiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthuvamanaiyin arasiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthavi seiya thaguthi irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthavi seiya thaguthi irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonamaga nirkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonamaga nirkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonamaga nirkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonamaga nirkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonamaga nirkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonamaga nirkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomaigal vaazhum idathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaigal vaazhum idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalai virkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalai virkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalai virkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalai virkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalai virkkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalai virkkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilam neer engal urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam neer engal urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal varumaigal ozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal varumaigal ozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu puratchi uruvaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu puratchi uruvaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal thalamurai kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal thalamurai kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal kangal thoongum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal kangal thoongum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal iruthi moochu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal iruthi moochu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi padaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi padaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkurom veriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkurom veriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali naangellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali naangellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom naangal poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha inquilab hai aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha inquilab hai aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuvanna karaila savdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuvanna karaila savdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Juntagalte ghamtyanche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juntagalte ghamtyanche "/>
</div>
<div class="lyrico-lyrics-wrapper">paiseKartahai baad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiseKartahai baad"/>
</div>
<div class="lyrico-lyrics-wrapper">Tya madhe vyapari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tya madhe vyapari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghusle sat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghusle sat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaila vyapaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaila vyapaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Khotya jahirata aikun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khotya jahirata aikun"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta padli hal mohat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta padli hal mohat"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuvanna nahi kaamdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuvanna nahi kaamdham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hast jaidya che lamb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hast jaidya che lamb"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fukatchi nakyavarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fukatchi nakyavarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora dharun basle abhiman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora dharun basle abhiman"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishayat jhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishayat jhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vismaran ni tyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vismaran ni tyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Wadlya vedna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wadlya vedna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Duskala var ghotalyanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duskala var ghotalyanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Shetkaryancha jeev ghetla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shetkaryancha jeev ghetla"/>
</div>
<div class="lyrico-lyrics-wrapper">Haat kaidya che lamb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haat kaidya che lamb"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inqulab k liye sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inqulab k liye sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab ek hi maksad se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab ek hi maksad se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Badlav k liye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badlav k liye hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haat kaidya che lamb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haat kaidya che lamb"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladenga hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladenga hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inqulab k liye sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inqulab k liye sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Khade hai hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khade hai hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab ek hi maksad se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab ek hi maksad se"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhude hai hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhude hai hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badlav k liye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badlav k liye hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haat kaidya che lamb
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haat kaidya che lamb"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladenga hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladenga hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilam neer engal urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam neer engal urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal varumaigal ozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal varumaigal ozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu puratchi uruvaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu puratchi uruvaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal thalamurai kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal thalamurai kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal kangal thoongum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal kangal thoongum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal iruthi moochu varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal iruthi moochu varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi padaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi padaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkurom veriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkurom veriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali naangellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali naangellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom naangal poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi padaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi padaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkurom veriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkurom veriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali naangellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali naangellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom naangal poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi padaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi padaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkurom veriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkurom veriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali naangellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali naangellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom naangal poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom naangal poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho Ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho Ho ho"/>
</div>
</pre>
