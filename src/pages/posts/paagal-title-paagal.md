---
title: "paagal iitle song lyrics"
album: "Paagal"
artist: "Radhan"
lyricist: "Chandrabose"
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "Paagal Title"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/af7DT6QQAqU"
type: "mass"
singers:
  - Ram Miriyala
  - Mama Sing
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">College sandhulu thirigaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College sandhulu thirigaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovellu cafe lu thirigaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovellu cafe lu thirigaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinema haallaku vellaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema haallaku vellaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi oka seat u nu vethikaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi oka seat u nu vethikaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaayigaani kanabadagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaayigaani kanabadagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you antaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you antaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa pilla nundi reply raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa pilla nundi reply raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Modatiki vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modatiki vasthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google google google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google google google"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl friend nu vethike google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl friend nu vethike google"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu paagal paagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu paagal paagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakosam bathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam bathike paagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google google google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google google google"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl friend nu vethike google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl friend nu vethike google"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu paagal paagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu paagal paagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakosam bathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam bathike paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha aaha, aaha aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha aaha, aaha aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla nuvvu sye ante chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla nuvvu sye ante chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rye ani vachhesthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rye ani vachhesthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nye antu cheppoddhe pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nye antu cheppoddhe pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kye ani edusthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kye ani edusthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera jaise premikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera jaise premikudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli neeku dhorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli neeku dhorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Preminchi choodave pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preminchi choodave pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandage neeku ammathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandage neeku ammathodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aha preminche paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha preminche paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchistha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchistha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu leni single
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu leni single"/>
</div>
<div class="lyrico-lyrics-wrapper">Janma kenni baadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma kenni baadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvistha navvul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvistha navvul"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojistha puvvul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojistha puvvul"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppukunte jindagi motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppukunte jindagi motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku jil jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku jil jil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nini puvvullona petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nini puvvullona petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosukunta raave ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosukunta raave ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam kadatha katthilanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam kadatha katthilanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala raathi khilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala raathi khilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv etta unna em chesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv etta unna em chesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaaledhe malla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaaledhe malla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv oppukunte motha moguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv oppukunte motha moguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Motham jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motham jilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammayi kanapadagane sagam premisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayi kanapadagane sagam premisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayi oppukunte mana pagal saru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayi oppukunte mana pagal saru"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththam premisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththam premisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapancham pedhdhadhi antadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapancham pedhdhadhi antadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prayathnam chesthu untaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prayathnam chesthu untaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee oollo workout avvakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee oollo workout avvakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkooriki pothaanantaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkooriki pothaanantaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Telescope kannulathoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telescope kannulathoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalisthuntaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalisthuntaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Horoscope kalavakapoina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horoscope kalavakapoina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradisthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammailo ammanu chusthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammailo ammanu chusthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa premani anveshisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa premani anveshisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Teginchi mundhuku pothaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teginchi mundhuku pothaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugimpu ee kadhakenaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugimpu ee kadhakenaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google google google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google google google"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl friend nu vethike google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl friend nu vethike google"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu paagal paagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu paagal paagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakosam bathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam bathike paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google google google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google google google"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl friend nu vethike google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl friend nu vethike google"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu paagal paagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu paagal paagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakosam bathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam bathike paagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google google google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google google google"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl friend nu vethike google
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl friend nu vethike google"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu paagal paagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu paagal paagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakosam bathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam bathike paagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Premakosam brathike paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakosam brathike paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Rey paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey paagal"/>
</div>
</pre>
