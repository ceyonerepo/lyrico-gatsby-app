---
title: "sayya song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Anuradha Sriram"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Sayya"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r2CeHMg_osg"
type: "love"
singers:
  - Gowry Lekshmi
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaa ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaa ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaa ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaa ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagulaa nagulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagulaa nagulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un tholil karaiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholil karaiyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugilaa mugilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugilaa mugilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu uraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu uraiyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thedava thedava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thedava thedava"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal thirudava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal thirudava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodi podhumae podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodi podhumae podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu iniya vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu iniya vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyyo vegam koodi chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo vegam koodi chella"/>
</div>
<div class="lyrico-lyrics-wrapper">En hormone thavikidhu seidhi solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En hormone thavikidhu seidhi solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo megam modhi kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo megam modhi kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sattena saaral theendi chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sattena saaral theendi chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam koodi vandhu vaattudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam koodi vandhu vaattudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodum puruvaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum puruvaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kollvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kollvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manneeyum peedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manneeyum peedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirar evvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar evvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraai maarsumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraai maarsumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum sirumen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum sirumen"/>
</div>
<div class="lyrico-lyrics-wrapper">Marungu izhaval kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marungu izhaval kandaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodum puruvaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum puruvaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kollvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kollvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manneeyum peedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manneeyum peedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirar evvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar evvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraai maarsumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraai maarsumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum sirumen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum sirumen"/>
</div>
<div class="lyrico-lyrics-wrapper">Marungu izhaval kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marungu izhaval kandaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thennavanae ennavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennavanae ennavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru udhadum inaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru udhadum inaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En viratham mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viratham mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodiyae nindridavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodiyae nindridavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveliyum kuraiyanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveliyum kuraiyanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal moodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal moodavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu maramaai Katti vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu maramaai Katti vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vilaga thittam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vilaga thittam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil pudhaiyaMayangi sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil pudhaiyaMayangi sariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu oonjalaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu oonjalaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu charamaai Thottu urasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu charamaai Thottu urasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti malayaai muththam kuviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti malayaai muththam kuviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu maraiya viralum inaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu maraiya viralum inaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambu vegam koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambu vegam koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeevan Unnodu serumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan Unnodu serumaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam koodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam koodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu poo idhuthanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu poo idhuthanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaman kal kudithanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaman kal kudithanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatrum ooo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatrum ooo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta udanae soodera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta udanae soodera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkul pudhaindhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul pudhaindhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukkul urandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukkul urandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchathin vazhiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchathin vazhiyinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">En vasam izhandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vasam izhandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vasam sarindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vasam sarindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai patta udaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai patta udaiyena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee maaru Jeevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maaru Jeevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu serumaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu serumaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam koodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam koodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodum puruvaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum puruvaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kollvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kollvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manneeyum peedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manneeyum peedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirar evvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar evvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraai maarsumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraai maarsumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum sirumen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum sirumen"/>
</div>
<div class="lyrico-lyrics-wrapper">Marungu izhaval kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marungu izhaval kandaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya sayya sayya sayyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya sayya sayya sayyaaaa"/>
</div>
</pre>
