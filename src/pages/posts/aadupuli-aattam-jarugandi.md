---
title: "aadupuli aattam song lyrics"
album: "Jarugandi"
artist: "Bobo Shashi"
lyricist: "AN Pitchumani - K Chandru"
director: "AN Pitchumani"
path: "/albums/jarugandi-lyrics"
song: "Aadupuli Aattam"
image: ../../images/albumart/jarugandi.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uADcHjFoYQM"
type: "happy"
singers:
  -  Deva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa eswaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa eswaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadupuli aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadupuli aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu odi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu odi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi poraar sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi poraar sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvathingu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathingu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu onnu senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu onnu senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu thappa pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thappa pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekku thappa inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekku thappa inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki kondathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki kondathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha vida innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha vida innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam dharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam dharmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Expiry aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Expiry aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappukku serina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappukku serina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyenna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyenna aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhilula pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhilula pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaan pudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaan pudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyila soruguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila soruguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiya aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiya aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiya aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiya aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa nannana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa nannana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanannaanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanannaanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa nannana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa nannana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa naanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa naanaa naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanannaanaa naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanannaanaa naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula velakkennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula velakkennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi theduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi theduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkala sikkavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkala sikkavae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ponaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ponaloo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Light-ta nee nenaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light-ta nee nenaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Matteril ippoLife-eh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matteril ippoLife-eh "/>
</div>
<div class="lyrico-lyrics-wrapper">fuse poiyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fuse poiyiduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch-eh podaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch-eh podaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Escape aiyutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape aiyutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyoo koondu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo koondu kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatti kitta eli neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti kitta eli neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavan ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavan ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam kattittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam kattittan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyil ava maattalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyil ava maattalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Petrol rate-u yerura pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petrol rate-u yerura pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan crime rate-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan crime rate-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo egiri pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo egiri pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiri pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadupuli aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadupuli aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu odi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu odi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi poraar sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi poraar sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvathingu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathingu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu onnu senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu onnu senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu thappa pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thappa pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekku thappa inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekku thappa inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki kondathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki kondathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha vida innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha vida innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam dharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam dharmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Expiry aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Expiry aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappukku serina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappukku serina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyenna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyenna aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhilula pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhilula pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaan pudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaan pudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyila soruguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila soruguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiya aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiya aachu"/>
</div>
</pre>
