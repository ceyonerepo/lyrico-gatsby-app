---
title: "song of life song lyrics"
album: "Gamanam"
artist: "Ilaiyaraaja"
lyricist: "Krishna Kanth"
director: "Sujana Rao"
path: "/albums/gamanam-lyrics"
song: "Song Of Life"
image: ../../images/albumart/gamanam.jpg
date: 2021-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/O6wLBMB0WqE"
type: "melody"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa AaAa AaAa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa AaAa AaAa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Khuda-Sha-He Mardha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Khuda-Sha-He Mardha"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mere Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mere Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Khuda Sher Hai Yajudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Khuda Sher Hai Yajudan"/>
</div>
<div class="lyrico-lyrics-wrapper">O Mere Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mere Moula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ali Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ali Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri Mannathonko Sun Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri Mannathonko Sun Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ali Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ali Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri Mushkile Mitta Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri Mushkile Mitta Moula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudulu Thirugu Nadhe Kadalina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudulu Thirugu Nadhe Kadalina"/>
</div>
<div class="lyrico-lyrics-wrapper">Padava Nadapamani Adagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padava Nadapamani Adagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidugu Padina Prathi Ksanamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidugu Padina Prathi Ksanamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Nilapamani Pilavaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Nilapamani Pilavaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatale Chaalisthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatale Chaalisthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mere Moulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mere Moulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ali Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ali Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhava Bandhanaana Munchaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhava Bandhanaana Munchaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ali Moula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ali Moula"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Srunkalaalu Tenchaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Srunkalaalu Tenchaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andani Aakaashaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andani Aakaashaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Korene Nela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korene Nela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa AaAa Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa AaAa Aa Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerani Aashenantu Oppukovelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerani Aashenantu Oppukovelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammukuni Kadhilina Gamanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammukuni Kadhilina Gamanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchakika Ontari Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchakika Ontari Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedukuni Karigenu Nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedukuni Karigenu Nayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedukagaa Kolavaku Sahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedukagaa Kolavaku Sahanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere Moula Hazi Moulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Moula Hazi Moulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapadheraa Aadhukoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapadheraa Aadhukoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupulu Ennenno Asaletu Velleno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupulu Ennenno Asaletu Velleno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalu Kadaku Etu Cherenemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalu Kadaku Etu Cherenemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudulu Thirugu Nadhe Kadalina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudulu Thirugu Nadhe Kadalina"/>
</div>
<div class="lyrico-lyrics-wrapper">Padava Nadapamani Adagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padava Nadapamani Adagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidugu Padina Prathi Ksanamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidugu Padina Prathi Ksanamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Nilapamani Pilavaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Nilapamani Pilavaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatale Chaalisthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatale Chaalisthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Mere Moulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mere Moulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okkaro Thappe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkaro Thappe "/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthe Sardhukolevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthe Sardhukolevaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa AaAa Aa AaAA Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa AaAa Aa AaAA Aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokame Ekam Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokame Ekam Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiksha Vesthaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiksha Vesthaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukani Vadalavu Gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukani Vadalavu Gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapamani Kalugadaa Chalanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapamani Kalugadaa Chalanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanani Tharumidhe Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanani Tharumidhe Tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rodhanaku Jarupika Dahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rodhanaku Jarupika Dahanam"/>
</div>
</pre>
