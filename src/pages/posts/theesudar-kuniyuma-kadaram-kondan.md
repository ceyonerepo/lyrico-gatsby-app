---
title: "theesudar kuniyuma song lyrics"
album: "Kadaram Kondan"
artist: "Ghibran"
lyricist: "Viveka"
director: "Rajesh M. Selva"
path: "/albums/kadaram-kondan-lyrics"
song: "Theesudar Kuniyuma"
image: ../../images/albumart/kadaram-kondan.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4HiDe9LHXLE"
type: "mass"
singers:
  - Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam Thaan Kizhiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thaan Kizhiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Nokkam Kondavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Nokkam Kondavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagam Thaniyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagam Thaniyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeri Vaaa Melae Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Vaaa Melae Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhanai Seidhavan Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seidhavan Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadha Manidhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadha Manidhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesathin Thalaivanum Munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesathin Thalaivanum Munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Oruvanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Oruvanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meeri Vaa Yeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Vaa Yeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam Yedhum Koora Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam Yedhum Koora Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakkugal Nooru Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkugal Nooru Sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyagam Nindru Ketkkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyagam Nindru Ketkkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Thaan Pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Thaan Pesumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eli Kooda Malai Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eli Kooda Malai Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Patchi Kadal Thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Patchi Kadal Thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Vazhi Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Vazhi Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theesudar Kuniyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesudar Kuniyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayum Ambu Paadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Ambu Paadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindru Paniyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindru Paniyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Melae Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Melae Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhanai Seidhavan Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seidhavan Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadha Manidhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadha Manidhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeri Vaa Yeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Vaa Yeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam Yedhum Koora Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam Yedhum Koora Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakkugal Nooru Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkugal Nooru Sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyagam Nindru Ketkkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyagam Nindru Ketkkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Thaan Pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Thaan Pesumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eli Kooda Malai Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eli Kooda Malai Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Patchi Kadal Thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Patchi Kadal Thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Vazhi Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Vazhi Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never Ever Give It Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Ever Give It Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai Maari Ponavargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Maari Ponavargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanaigal Seidhathillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanaigal Seidhathillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthida Koodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthida Koodathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Veyil Anjida Koodaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Veyil Anjida Koodaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Say Say Bayathukku No Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Say Say Bayathukku No Way"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Say Say Sombalukku No Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Say Say Sombalukku No Way"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Say Say Sorvukku No Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Say Say Sorvukku No Way"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Say Muyarchikku Jey Jey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Say Muyarchikku Jey Jey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meeri Vaa Yeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Vaa Yeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanam Yedhum Koora Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanam Yedhum Koora Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Vaa Meeri Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Vaa Meeri Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakkugal Nooru Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkugal Nooru Sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyagam Nindru Ketkkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyagam Nindru Ketkkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Thaan Pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Thaan Pesumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Thaandiyae Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Thaandiyae Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Soodalaam Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Soodalaam Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Unnadhu Go Go Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unnadhu Go Go Go"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Kaatu Gethu Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatu Gethu Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eli Kooda Malai Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eli Kooda Malai Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Patchi Kadal Thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Patchi Kadal Thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Vazhi Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Vazhi Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eli Kooda Malai Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eli Kooda Malai Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Patchi Kadal Thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Patchi Kadal Thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Vazhi Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Vazhi Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Mattum Podhum"/>
</div>
</pre>
