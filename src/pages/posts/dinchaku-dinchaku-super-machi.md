---
title: "dinchaku dinchaku song lyrics"
album: "Super Machi"
artist: "Thaman S"
lyricist: "Ramajogayya Sastry"
director: "Puli Vasu"
path: "/albums/super-machi-lyrics"
song: "Dinchaku Dinchaku"
image: ../../images/albumart/super-machi.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iDVoiBkIKjc"
type: "happy"
singers:
  - Saketh Komanduri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Dinchaku Dinchaku Dhanchingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dinchaku Dinchaku Dhanchingu"/>
</div>
<div class="lyrico-lyrics-wrapper">DJ Vaala PunchingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="DJ Vaala PunchingU"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimma Thirigi Kattayyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimma Thirigi Kattayyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimaaku WiringU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimaaku WiringU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Super Duper SettingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Duper SettingU"/>
</div>
<div class="lyrico-lyrics-wrapper">Sound Light FittingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound Light FittingU"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Pub Debbaku Ontlo JoshU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Pub Debbaku Ontlo JoshU"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Retlu PumpingO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Retlu PumpingO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abbo Idhi Ambiance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbo Idhi Ambiance"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichha Mind BlowingO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichha Mind BlowingO"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Itu Drink GirlsU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Itu Drink GirlsU"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannugotti CallingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannugotti CallingU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
1 2 3 <div class="lyrico-lyrics-wrapper">Antaandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antaandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee DanceU Ka FlooringU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee DanceU Ka FlooringU"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagganantu Dhookuthaandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagganantu Dhookuthaandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arikaallaloni SpringU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arikaallaloni SpringU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Comming To KummingU 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming To KummingU "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Comming To KummingU 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming To KummingU "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Comming To KummingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming To KummingU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo Edho Magic Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Edho Magic Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Kooda Music Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Kooda Music Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaa Maava Panchesukundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaa Maava Panchesukundhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Positive Energy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Positive Energy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solo Paatalu Thrillemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo Paatalu Thrillemundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandaga Cheddhaam Padhi Mandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandaga Cheddhaam Padhi Mandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chotainaa Em Chesthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chotainaa Em Chesthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Smile Ye Mana Emoji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smile Ye Mana Emoji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey BoostU MilkU Bujjaayilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey BoostU MilkU Bujjaayilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukantha ActingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukantha ActingU"/>
</div>
<div class="lyrico-lyrics-wrapper">Monotomy Break-aipogaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monotomy Break-aipogaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Ledhu CheatingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Ledhu CheatingU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Ante Intheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ante Intheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Party Pandaga Song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Party Pandaga Song"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill Maaro Chindhulathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill Maaro Chindhulathona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanni Cheddhaam Double StrongU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Cheddhaam Double StrongU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Comming To KummingU 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming To KummingU "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Comming Comming To KummingU 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming Comming To KummingU "/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Comming To KummingU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Comming To KummingU"/>
</div>
</pre>
