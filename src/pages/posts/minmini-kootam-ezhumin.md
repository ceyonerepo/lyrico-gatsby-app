---
title: "minmini kootam song lyrics"
album: "Ezhumin"
artist: "Ganesh Chandrasekaran"
lyricist: "Mohan Raja"
director: "VP Viji"
path: "/albums/ezhumin-lyrics"
song: "Minmini Kootam"
image: ../../images/albumart/ezhumin.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/38f3HRAFn1A"
type: "happy"
singers:
  - Jagadeesh
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Laalala laalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalala laalala laa laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala laa laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalala laalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala laalala laa laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala laalala laa laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku ellai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku ellai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanamum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudan oppidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudan oppidum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondham ingu undavathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham ingu undavathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham athil nooravathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham athil nooravathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ingu anbagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam ingu anbagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku nooru mugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku nooru mugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma endra sollanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma endra sollanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin maru sollagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin maru sollagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna naan solvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna naan solvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaalae vazhum yugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae vazhum yugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku ellai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku ellai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanamum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudan oppidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudan oppidum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaigalum nanaithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaigalum nanaithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalum adikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalum adikirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaigalum sirithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigalum sirithidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal virigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal virigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalum vizhithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalum vizhithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarangal kidaikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarangal kidaikirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal marainthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal marainthidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrodu poo pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu poo pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovodu thean pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovodu thean pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae anbaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae anbaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyillai naanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyillai naanillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamendru aanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamendru aanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kuda anbaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kuda anbaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbendra sollalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbendra sollalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undana ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undana ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerae thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerae thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathu vinmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathu vinmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvondrum sonthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrum sonthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyendrum yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyendrum yaarumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullin nuni melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin nuni melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panithuli peranbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panithuli peranbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvaru vidiyalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvaru vidiyalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavan peranbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavan peranbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valimai tharuvathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valimai tharuvathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyin peranbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyin peranbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvin arthamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvin arthamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathin peranbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathin peranbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerum anbaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum anbaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sintha thembaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sintha thembaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda nanban manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda nanban manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbodu nee sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu nee sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollellaam vedhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollellaam vedhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendamae kaasu panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendamae kaasu panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneeril thalladum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril thalladum"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbukku ilaiyondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbukku ilaiyondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana padagaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana padagaagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbathil thindaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbathil thindaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukku nee kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku nee kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbondrae varamaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbondrae varamaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini kootamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minmini kootamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku ellai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku ellai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanamum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyum siriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyum siriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudan oppidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudan oppidum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondham ingu undavathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham ingu undavathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham athil nooravathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham athil nooravathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam ingu anbaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam ingu anbaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbukku nooru mugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku nooru mugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma endra sollanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma endra sollanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin maru sollaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin maru sollaagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna naan solvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna naan solvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae vazhum yugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae vazhum yugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laalala laalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalala laalala laa laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala laa laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Laalala laalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalala laalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala laalala laa laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala laalala laa laa"/>
</div>
</pre>
