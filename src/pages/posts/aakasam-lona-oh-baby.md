---
title: "aakasam lona song lyrics"
album: "Oh Baby"
artist: "Mickey J. Meyer"
lyricist: "Lakshmi Bhupala"
director: "B.V. Nandini Reddy"
path: "/albums/oh-baby-lyrics"
song: "Aakasam Lona"
image: ../../images/albumart/oh-baby.jpg
date: 2019-07-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ka13JF4CZt8"
type: "sad"
singers:
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aakasam Lona Ekkaki Megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasam Lona Ekkaki Megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokaanidhaa Vaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokaanidhaa Vaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadi Veedhi Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi Veedhi Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanubhaala Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanubhaala Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhachudaku Naanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhachudaku Naanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana Pege Tana Thodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana Pege Tana Thodai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana Konge Needai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana Konge Needai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arachethi Thalaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arachethi Thalaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru Cheripaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru Cheripaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati Gaayaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati Gaayaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenade Saapalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenade Saapalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduraithe Naakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduraithe Naakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Jola Paadaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Jola Paadaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ontarai Unnaa Odipoledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ontarai Unnaa Odipoledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantaga Unte Kannire Kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantaga Unte Kannire Kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatenthunna Velugune Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatenthunna Velugune Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bosinavvullo Na Bidda Sendhrude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bosinavvullo Na Bidda Sendhrude"/>
</div>
<div class="lyrico-lyrics-wrapper">Mm Pade Bhaadhallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mm Pade Bhaadhallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vode Odharpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vode Odharpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kusalamadige Manishi Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kusalamadige Manishi Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirundho Ledho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirundho Ledho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaliki Vaniki Telusukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaliki Vaniki Telusukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathiki Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathiki Unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati Gaayaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati Gaayaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenade Saapalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenade Saapalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduraithe Naakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduraithe Naakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Jola Paadaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Jola Paadaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kannaa"/>
</div>
</pre>
