---
title: "lazy song song lyrics"
album: "Oh Manapenne"
artist: "Vishal Chandrashekhar"
lyricist: "Mohan Rajan"
director: "Kaarthikk Sundar"
path: "/albums/oh-manapenne-lyrics"
song: "Lazy Song"
image: ../../images/albumart/oh-manapenne.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IJ5wvaE_ej4"
type: "lazy"
singers:
  - Sinduri Vishal
  - Lady Kash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanmoodi kidakaaney paiyaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kidakaaney paiyaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodil ilatha pozhuthu uuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodil ilatha pozhuthu uuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum porumaiya veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum porumaiya veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan urangidum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan urangidum pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu kidapaan porandu padupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu kidapaan porandu padupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvai nuni kooda kaadhil nuzhaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvai nuni kooda kaadhil nuzhaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam mudindhaal thavazhndhu nadapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam mudindhaal thavazhndhu nadapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu sombal udan kaapi kudipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhu sombal udan kaapi kudipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmoodi kidakaaney paiyaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kidakaaney paiyaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodil ilatha pozhuthu uuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodil ilatha pozhuthu uuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum porumaiya veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum porumaiya veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan urangidum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan urangidum pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thethi kizhipaan dhaadi valarpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thethi kizhipaan dhaadi valarpaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimida mul pola asandhu nadapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimida mul pola asandhu nadapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arattai adipaan alandhu siripaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arattai adipaan alandhu siripaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhai mozham pottu Kotti kazhipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhai mozham pottu Kotti kazhipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geetha dhuniku thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geetha dhuniku thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thadhrukita thom nachu rahe gori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thadhrukita thom nachu rahe gori"/>
</div>
<div class="lyrico-lyrics-wrapper">Tham thi thaam thai tha thai kita thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tham thi thaam thai tha thai kita thaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geetha dhuniku thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geetha dhuniku thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thadhrukita thom nachu rahe gori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thadhrukita thom nachu rahe gori"/>
</div>
<div class="lyrico-lyrics-wrapper">Tham thi thaam thai tha thai kita thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tham thi thaam thai tha thai kita thaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma aalu peru karthik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma aalu peru karthik"/>
</div>
<div class="lyrico-lyrics-wrapper">He don’t hear the clock tick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He don’t hear the clock tick"/>
</div>
<div class="lyrico-lyrics-wrapper">Poriyiyal padichittu porichadha saptukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyiyal padichittu porichadha saptukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cricket-la scene-u podum namma casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cricket-la scene-u podum namma casanova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan muzhuchi slow-va ulla whiskey kood cola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan muzhuchi slow-va ulla whiskey kood cola"/>
</div>
<div class="lyrico-lyrics-wrapper">Cock-a-doodle doo he got the monday blues
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cock-a-doodle doo he got the monday blues"/>
</div>
<div class="lyrico-lyrics-wrapper">Now what’s he gonna do he got the monday blues
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now what’s he gonna do he got the monday blues"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu kidappan porandu paduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu kidappan porandu paduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvai nuni kooda kaadhil nuzhaippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvai nuni kooda kaadhil nuzhaippan"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam mudindhal thavazhndhu nadappan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam mudindhal thavazhndhu nadappan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhu sombal udan kaapi kudippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhu sombal udan kaapi kudippan"/>
</div>
</pre>
