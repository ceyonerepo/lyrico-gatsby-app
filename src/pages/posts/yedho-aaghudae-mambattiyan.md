---
title: "yedho aaghudae song lyrics"
album: "Mambattiyan"
artist: "S Thaman"
lyricist: "Na. Muthukumar"
director: "Thiagarajan"
path: "/albums/mambattiyan-lyrics"
song: "Yedho Aaghudae"
image: ../../images/albumart/mambattiyan.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/StvXXlKOP9M"
type: "love"
singers:
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannil Un Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Un Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Anjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Anjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Un Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Un Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin Oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin Oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Podhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Podhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum Needhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum Needhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayil Un Kayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayil Un Kayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhayal Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhayal Konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalil Un Kaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil Un Kaalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusin Kenjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusin Kenjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Un Maaman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Un Maaman"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjanum Nee Vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjanum Nee Vaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Aagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Aagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukul Edho Aaghudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukul Edho Aaghudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Marudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Marudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhkayil Ellaam Maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhkayil Ellaam Maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvrai Illaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvrai Illaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli Ilaamal Miratudhu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli Ilaamal Miratudhu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Yen Edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Edhanaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai Sollaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Sollaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugavari Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugavari Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkudhu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkudhu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Unnaal Unnaal Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Unnaal Unnaal Thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Un Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Un Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Anjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Anjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Un Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Un Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin Oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin Oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Podhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Podhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum Needhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum Needhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayil Un Kayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayil Un Kayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhayal Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhayal Konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalil Un Kaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil Un Kaalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusin Kenjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusin Kenjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Un Maaman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Un Maaman"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjanum Nee Vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjanum Nee Vaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Aagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Aagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukul Edho Aaghudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukul Edho Aaghudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Murai Nee Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Murai Nee Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Ennodu Vazhkindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Ennodu Vazhkindradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Oh Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Oh Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Mudhal Nee Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Mudhal Nee Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uryirukkul Eppodhum Ketkindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uryirukkul Eppodhum Ketkindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai Kaanaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Kaanaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirakkam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirakkam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Mudhal Adivarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Mudhal Adivarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolludhu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolludhu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Thunbam Rendum Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Thunbam Rendum Unnaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Aagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Aagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukul Edho Aaghudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukul Edho Aaghudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Marudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Marudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhkayil Ellaam Maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhkayil Ellaam Maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvrai Illaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvrai Illaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli Ilaamal Miratudhu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli Ilaamal Miratudhu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Yen Edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Edhanaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai Sollaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Sollaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugavari Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugavari Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkudhu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkudhu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Unnaal Unnaal Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Unnaal Unnaal Thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Un Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Un Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Anjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Anjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Un Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Un Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin Oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin Oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Podhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Podhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhanum Needhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhanum Needhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayil Un Kayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayil Un Kayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhayal Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhayal Konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalil Un Kaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil Un Kaalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusin Kenjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusin Kenjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Un Maaman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Un Maaman"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjanum Nee Vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjanum Nee Vaama"/>
</div>
</pre>
