---
title: "shero shero song lyrics"
album: "Jackpot"
artist: "Vishal Chandrasekhar"
lyricist: "Vivek"
director: "Kalyaan"
path: "/albums/jackpot-lyrics"
song: "Shero Shero"
image: ../../images/albumart/jackpot.jpg
date: 2019-08-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LccE1P3CyTM"
type: "intro"
singers:
  - Sinduri Vishal
  - Brindha Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dol Damakku Daa Damakku Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dol Damakku Daa Damakku Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruku Sillaata Pillaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Sillaata Pillaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Silenta Nazhuvattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Silenta Nazhuvattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Lady Star Vellaattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Lady Star Vellaattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Dhuttu Yennaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Dhuttu Yennaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhuttu Unnaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhuttu Unnaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummeettaa Astaenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummeettaa Astaenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennodadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennodadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Area-la Avulu-uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area-la Avulu-uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poven Da Koralu-uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poven Da Koralu-uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Solla Nastanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Solla Nastanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daaar Danakku Vachaalae Rocket-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaar Danakku Vachaalae Rocket-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooor Jinukkum Yekkaav -Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooor Jinukkum Yekkaav -Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaa Valachu Vaikkadha Pocket-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaa Valachu Vaikkadha Pocket-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaal Parakkum Sokkaav-oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaal Parakkum Sokkaav-oda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Sillakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Sillakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Naan Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Naan Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Sillakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Sillakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Venna Shero Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Venna Shero Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Naan Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Naan Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Dhuttu Yennaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Dhuttu Yennaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhuttu Unnaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhuttu Unnaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummeettaa Astaenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummeettaa Astaenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennodadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennodadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Area-la Avulu-uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area-la Avulu-uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poven Da Koralu-uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poven Da Koralu-uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Solla Nastanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Solla Nastanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Avalvu Adukki Nasta-va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Avalvu Adukki Nasta-va"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechaalum Vayiru Alavu Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechaalum Vayiru Alavu Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Pogum Hero-va Irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Pogum Hero-va Irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamaa Nee Uttaalum Varamaattey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamaa Nee Uttaalum Varamaattey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Yellaa Reel-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Yellaa Reel-um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Sillakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Sillakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Venna Shero Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Venna Shero Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Naan Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Naan Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pokkiri Chittu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkiri Chittu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagaa Bittu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagaa Bittu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaram Thaan Yerumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaram Thaan Yerumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu Vuttaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Vuttaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valvala Meenu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valvala Meenu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Speed-u Maanu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speed-u Maanu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava Vutta-Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava Vutta-Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatta Mataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatta Mataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allaarumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaarumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbu Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbu Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappu-nu Dive Adchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappu-nu Dive Adchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Galleeju Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galleeju Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meyyaalumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meyyaalumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gare Aachumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gare Aachumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Right Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Yaedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrong Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrong Yaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Conpees-u Maa Bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conpees-u Maa Bejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamkonu Pollaannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamkonu Pollaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Collection Gallaa Kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collection Gallaa Kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooor Adchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooor Adchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olaiyil Potu Nee Sethaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olaiyil Potu Nee Sethaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Marappa Allaa Dhuttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marappa Allaa Dhuttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Sillakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Sillakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Naan Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Naan Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shero Shero Shero Sillakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shero Shero Shero Sillakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Venna Shero Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Venna Shero Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Naan Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Naan Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joro Joro Joro Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joro Joro Joro Maa"/>
</div>
</pre>
