---
title: "andarathil thonguthamma song lyrics"
album: "Merku Thodarchi Malai"
artist: "Ilaiyaraaja"
lyricist: "unknown"
director: "Lenin Bharathi"
path: "/albums/merku-thodarchi-malai-lyrics"
song: "Andarathil Thonguthamma"
image: ../../images/albumart/merku-thodarchi-malai.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZKWArTZ3Ixo"
type: "sad"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">antharathil thonguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antharathil thonguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham ethum illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham ethum illama"/>
</div>
<div class="lyrico-lyrics-wrapper">aelai vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelai vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">munthi ingu vanthathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthi ingu vanthathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha ithu ennathunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha ithu ennathunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">neer thangi kollathu thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer thangi kollathu thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thangathu thangathu thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangathu thangathu thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">vendatha nee thanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendatha nee thanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">venam nu sollathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam nu sollathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antharathil thonguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antharathil thonguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham ethum illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham ethum illama"/>
</div>
<div class="lyrico-lyrics-wrapper">aelai vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelai vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">munthi ingu vanthathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthi ingu vanthathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha ithu ennathunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha ithu ennathunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanam pathu boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam pathu boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">pola vaalkai kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola vaalkai kedakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">maanathoda vaalthu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanathoda vaalthu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">manna kodukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna kodukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">sanam aala paduthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanam aala paduthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">otha nodi vetti puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha nodi vetti puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">thoora thesam poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoora thesam poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en mannu ena paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mannu ena paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thirumbi atha paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thirumbi atha paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">en mannu ena paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mannu ena paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thirumbi atha paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thirumbi atha paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">elelu thalaimuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elelu thalaimuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pola pogalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pola pogalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antharathil thonguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antharathil thonguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham ethum illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham ethum illama"/>
</div>
<div class="lyrico-lyrics-wrapper">aelai vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelai vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">munthi ingu vanthathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthi ingu vanthathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha ithu ennathunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha ithu ennathunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara ketka"/>
</div>
</pre>
