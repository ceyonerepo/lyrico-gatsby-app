---
title: "ranga rattinam song lyrics"
album: "Kuruthi Aattam"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Sri Ganesh"
path: "/albums/kuruthi-aattam-lyrics"
song: "Ranga Rattinam"
image: ../../images/albumart/kuruthi-aattam.jpg
date: 2022-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IF_rykgoee4"
type: "love"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey eyy eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eyy eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ranga raatinam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ranga raatinam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Una vattam poduthae kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una vattam poduthae kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham vaanavil nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham vaanavil nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thathi thaavuren melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena thathi thaavuren melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae nee oru raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae nee oru raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Una paakum bothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una paakum bothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi thookam pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi thookam pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji naanumae thalatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji naanumae thalatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum naalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum naalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirunaala aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunaala aachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaathi un vaartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi un vaartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkum nenjil theanaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkum nenjil theanaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga sernthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga sernthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullum aaven poonthottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullum aaven poonthottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ranga raatinam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ranga raatinam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Una vattam poduthae kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una vattam poduthae kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham vaanavil nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham vaanavil nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thathi thaavuren melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena thathi thaavuren melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram pera paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram pera paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai thaan un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai thaan un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavani potta en rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavani potta en rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatura vaazh naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatura vaazh naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalula kolam nee poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalula kolam nee poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae poonjola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae poonjola"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadaiya pesum un pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadaiya pesum un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peechuthae seempaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peechuthae seempaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonguna kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonguna kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasoda veesura thoorala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasoda veesura thoorala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkedhum illai velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkedhum illai velai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaathi un vaartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi un vaartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkum nenjil theanaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkum nenjil theanaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga sernthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga sernthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullum aaven poonthottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullum aaven poonthottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ranga raatinam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ranga raatinam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Una vattam poduthae kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una vattam poduthae kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham vaanavil nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham vaanavil nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thathi thaavuren melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena thathi thaavuren melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae nee oru raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae nee oru raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Una paakum pothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una paakum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi thookam pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi thookam pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji naanumae thalatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji naanumae thalatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum naalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum naalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirunaala aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunaala aachae"/>
</div>
</pre>
