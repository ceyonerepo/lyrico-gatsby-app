---
title: "magaraaniye song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "Viveka"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Magaraaniye"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9swS_OPbEOY"
type: "melody"
singers:
  - Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalo Thalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalo Thalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalo Thalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalo Thalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaraaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinniravu Kadanthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniravu Kadanthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yosippaen Thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yosippaen Thoongaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavum Paarkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavum Paarkkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithakann Vaangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithakann Vaangaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Oru Uyarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Oru Uyarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavae Peraasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavae Peraasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Antha Nodiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Antha Nodiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum En Uyir Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum En Uyir Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Ennuyir Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Ennuyir Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaraaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyangal Ellaam Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyangal Ellaam Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Nindroru Paadam Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Nindroru Paadam Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovinangal Ellaam Koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovinangal Ellaam Koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ezhil Poloru Poovai Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ezhil Poloru Poovai Pookkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaajaalam Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaajaalam Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Kannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayai Naanum Maari Vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai Naanum Maari Vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalae Pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalae Pinnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollida Vaarthaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollida Vaarthaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam Yedhammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam Yedhammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Naanoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Naanoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasagan Thaanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaasagan Thaanamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sandhana Kaiviral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhana Kaiviral"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthidum Mannumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthidum Mannumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamendraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamendraagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhira Maayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhira Maayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaraaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinniravu Kadanthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniravu Kadanthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yosippaen Thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yosippaen Thoongaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavum Paarkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavum Paarkkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithakann Vaangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithakann Vaangaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Oru Uyarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Oru Uyarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavae Peraasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavae Peraasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Antha Nodiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Antha Nodiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum En Uyir Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum En Uyir Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Ennuyir Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Ennuyir Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaraaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Vanna Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Vanna Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Vellam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Vellam Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Thangam"/>
</div>
</pre>
