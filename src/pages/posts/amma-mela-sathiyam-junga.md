---
title: "amma mela sathiyam song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Amma Mela Sathiyam"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0ynrqmlPcIA"
type: "love"
singers:
  - Jagadeesh Kumar
  - Pavithra Gokul
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvanthae naaku chaala ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvanthae naaku chaala ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nu kaathanttae naaku chaala kastham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nu kaathanttae naaku chaala kastham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae enakku east-um west-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanae enakku east-um west-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum kedacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum kedacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba athirshtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba athirshtam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei nee ippudu choodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei nee ippudu choodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ikkada choodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ikkada choodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pora route-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pora route-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kooda venundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kooda venundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan ulloor paiyandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ulloor paiyandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nelloor ponnudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nelloor ponnudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai katta porandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai katta porandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sollu jarugandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sollu jarugandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei nee ippudu choodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei nee ippudu choodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ikkada choodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ikkada choodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pora route-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pora route-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kooda venundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kooda venundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai enga paathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai enga paathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan whistle-u adippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan whistle-u adippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ulla yeritta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ulla yeritta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bus flight-ah maarundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus flight-ah maarundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan dhammu adikka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dhammu adikka maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thanni adikka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thanni adikka maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pepsi kudikka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pepsi kudikka maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana ketta pazhakkam onnu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana ketta pazhakkam onnu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil thinamum rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil thinamum rasippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee amma medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee amma medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naina medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naina medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee appa medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee appa medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thatha medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thatha medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uncle medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uncle medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aunty medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aunty medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee akka medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee akka medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thambudu medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thambudu medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un amma mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un amma mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un appa mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un appa mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aaya mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aaya mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thattha mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thattha mela sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un uncle mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uncle mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aunty mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aunty mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un akka mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un akka mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka purushan melaiyum sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka purushan melaiyum sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaala kushi yenthukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaala kushi yenthukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethukkanttae maatladura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethukkanttae maatladura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvae mana heart-u medhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvae mana heart-u medhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka seat-u pottu unnaarura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka seat-u pottu unnaarura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Telugum theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugum theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadam theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadam theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindi pudikkathu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindi pudikkathu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">A jokes theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A jokes theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad words theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad words theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnae onnu theriyum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnae onnu theriyum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla unnai…theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla unnai…theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee amma medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee amma medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naina medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naina medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee appa medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee appa medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thatha medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thatha medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uncle medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uncle medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aunty medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aunty medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee akka medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee akka medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thambudu medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thambudu medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un amma mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un amma mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un appa mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un appa mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aaya mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aaya mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thattha mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thattha mela sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un uncle mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uncle mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aunty mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aunty mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un akka mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un akka mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka purushan melaiyum sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka purushan melaiyum sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu mixaiyinthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu mixaiyinthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaaiyinthi bus poyinthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaaiyinthi bus poyinthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra naa boyfriend unga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra naa boyfriend unga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi ullasangaa urchakangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi ullasangaa urchakangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai habit-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai habit-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Porn-o sabalist-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porn-o sabalist-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Party addict-u illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party addict-u illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Psycho sadist-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Psycho sadist-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa accused-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa accused-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana oru thappu seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana oru thappu seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu thanamaa…rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu thanamaa…rasippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee amma medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee amma medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naina medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naina medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee appa medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee appa medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thatha medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thatha medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uncle medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uncle medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aunty medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aunty medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee akka medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee akka medhu oddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thambudu medhu oddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thambudu medhu oddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un amma mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un amma mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un appa mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un appa mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aaya mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aaya mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thattha mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thattha mela sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un cousin mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un cousin mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un annan mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un annan mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anni mela sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anni mela sathiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni amma melaiyum sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni amma melaiyum sathiyam"/>
</div>
</pre>
