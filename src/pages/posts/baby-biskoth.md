---
title: "baby song lyrics"
album: "Biskoth"
artist: "Radhan"
lyricist: "Radhan"
director: "R. Kannan"
path: "/albums/biskoth-lyrics"
song: "Baby"
image: ../../images/albumart/biskoth.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oNFcdpTo4FQ"
type: "love"
singers:
  - Yogi Sekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai Yaethukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yaethukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saerthukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saerthukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Maaman Naanthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Maaman Naanthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Rani Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Rani Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yaethukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yaethukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saerthukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saerthukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Maaman Naanthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Maaman Naanthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Rani Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Rani Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Kadhal Ennoda Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kadhal Ennoda Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga Saernthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Saernthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Book-Il Un Sign-Ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Book-Il Un Sign-Ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-Um Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-Um Aayachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Kadhal Ennoda Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kadhal Ennoda Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga Saernthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Saernthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Book-Il Un Sign-Ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Book-Il Un Sign-Ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-Um Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-Um Aayachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yaethukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yaethukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saerthukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saerthukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maaman Naanthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maaman Naanthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Rani Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Rani Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavile Therinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavile Therinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalai Koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalai Pidithu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalai Pidithu Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Ennai Izhuthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Ennai Izhuthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sittaaga Paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittaaga Paranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottaga Malarnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottaga Malarnthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naan Paartha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Paartha Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivvunu Thaan Irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvunu Thaan Irunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Muththi Paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Muththi Paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paralogam Theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paralogam Theriyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaasaa Vedikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasaa Vedikkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnala Kaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala Kaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthuthu Suthuthu Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthuthu Suthuthu Paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullara Pala Kuyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara Pala Kuyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathuthu Kathuthu Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathuthu Kathuthu Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichitiye Morachitiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichitiye Morachitiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipdi Ipdi Panni Kavuthutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipdi Ipdi Panni Kavuthutiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthutiye Ennai Surutitiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthutiye Ennai Surutitiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Beeda Pola Madichitiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Beeda Pola Madichitiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morratu Single-Ah Irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morratu Single-Ah Irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Uruti Porati Thaan Edutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Uruti Porati Thaan Edutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theratti Paal-Ah Pola Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theratti Paal-Ah Pola Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thirudi Thinna Thaan Ninachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thirudi Thinna Thaan Ninachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yaethukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yaethukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saerthukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saerthukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maaman Naanthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maaman Naanthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Rani Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Rani Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jivvunu Kannu Tea Kadai Bun-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvunu Kannu Tea Kadai Bun-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethandi En Maan-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethandi En Maan-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Jillunu Moonu Side-La Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillunu Moonu Side-La Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallatha En Queen-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallatha En Queen-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullara Poonthu Assault-U Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara Poonthu Assault-U Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Somita Gin-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Somita Gin-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Myma Nee Ennai Maama-Nu Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Myma Nee Ennai Maama-Nu Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaladi Mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaladi Mannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Iruntha Thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Iruntha Thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Suda Vacha Thaan Venni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Suda Vacha Thaan Venni"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakki Sil Pettu Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakki Sil Pettu Maame"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thookka Porendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thookka Porendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yaethukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yaethukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Saerthukka Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Saerthukka Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maaman Naanthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maaman Naanthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Rani Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Rani Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Kadhal Ennoda Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kadhal Ennoda Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga Saernthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Saernthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Book-Il Un Sign-Ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Book-Il Un Sign-Ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-Um Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-Um Aayachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Kadhal Ennoda Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kadhal Ennoda Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaga Saernthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Saernthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Book-Il Un Sign-Ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Book-Il Un Sign-Ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-Um Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-Um Aayachu"/>
</div>
</pre>
