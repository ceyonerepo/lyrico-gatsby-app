---
title: "maar maar song lyrics"
album: "Hippi"
artist: "Nivas K. Prasanna"
lyricist: "Sreemani"
director: "Krishna"
path: "/albums/hippi-lyrics"
song: "Maar Maar"
image: ../../images/albumart/hippi.jpg
date: 2019-06-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lazYccwT0w4"
type: "happy"
singers:
  - K.G. Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey jingidi jingidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jingidi jingidi"/>
</div>
<div class="lyrico-lyrics-wrapper">jingidi zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jingidi zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">single ayi poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="single ayi poyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey jingidi jingidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jingidi jingidi"/>
</div>
<div class="lyrico-lyrics-wrapper">jingidi happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jingidi happy"/>
</div>
<div class="lyrico-lyrics-wrapper">pongale shuru ayindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongale shuru ayindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey jingidi jingidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jingidi jingidi"/>
</div>
<div class="lyrico-lyrics-wrapper">jingidi zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jingidi zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">single ayi poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="single ayi poyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey jingidi jingidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jingidi jingidi"/>
</div>
<div class="lyrico-lyrics-wrapper">jingidi happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jingidi happy"/>
</div>
<div class="lyrico-lyrics-wrapper">pongale shuru ayindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongale shuru ayindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey mass beatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey mass beatey"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyi kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyi kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey class katti pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey class katti pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey glass cheptha pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey glass cheptha pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee laugh eh loss eh yettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee laugh eh loss eh yettu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey ishq lo padithe kick ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ishq lo padithe kick ey"/>
</div>
<div class="lyrico-lyrics-wrapper">arey okey ayithe chick ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey okey ayithe chick ey"/>
</div>
<div class="lyrico-lyrics-wrapper">are break up ayindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="are break up ayindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">picha luck eh luck eh luck eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="picha luck eh luck eh luck eh"/>
</div>
<div class="lyrico-lyrics-wrapper">arey bonaalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey bonaalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">modhalaindhi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhalaindhi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">mana praanalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana praanalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">full safe u raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="full safe u raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">settle ayipoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settle ayipoyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari nee dhigule poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari nee dhigule poyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">settle ayipoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settle ayipoyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari nee dhigule poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari nee dhigule poyindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey love u givvantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey love u givvantaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ollu kovvey pattuntaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ollu kovvey pattuntaru"/>
</div>
<div class="lyrico-lyrics-wrapper">mari chancey gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari chancey gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">ichamante chedugudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichamante chedugudu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadestharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadestharu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey poovutho premistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey poovutho premistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">navvu navvutho premistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvu navvutho premistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">arey mozey kaani theerindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey mozey kaani theerindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">katthulu kuripistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katthulu kuripistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nakharaalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nakharaalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">chalinchara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalinchara "/>
</div>
<div class="lyrico-lyrics-wrapper">mari bakaraalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari bakaraalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">kaabomu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaabomu raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallu muntha namminodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu muntha namminodu"/>
</div>
<div class="lyrico-lyrics-wrapper">matthulona theluthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matthulona theluthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">beer u lona jaarinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beer u lona jaarinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhoshale challu thaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhoshale challu thaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallu muntha namminodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu muntha namminodu"/>
</div>
<div class="lyrico-lyrics-wrapper">matthulona theluthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matthulona theluthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">beer u lona jaarinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beer u lona jaarinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhoshale challu thaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhoshale challu thaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">maguvani nammavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maguvani nammavante"/>
</div>
<div class="lyrico-lyrics-wrapper">theyguvatho vellavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theyguvatho vellavante"/>
</div>
<div class="lyrico-lyrics-wrapper">dhummai pothaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhummai pothaav"/>
</div>
<div class="lyrico-lyrics-wrapper">anthe macha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthe macha"/>
</div>
<div class="lyrico-lyrics-wrapper">ammayi ante mamaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammayi ante mamaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ammayi ante mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammayi ante mama"/>
</div>
<div class="lyrico-lyrics-wrapper">mana head ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana head ki "/>
</div>
<div class="lyrico-lyrics-wrapper">zandu balm ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zandu balm ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ammayi ante mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammayi ante mama"/>
</div>
<div class="lyrico-lyrics-wrapper">mana head ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana head ki "/>
</div>
<div class="lyrico-lyrics-wrapper">zandu balm ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zandu balm ah"/>
</div>
<div class="lyrico-lyrics-wrapper">hey zanda peeke dhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey zanda peeke dhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">mana gundenu dhaache dhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana gundenu dhaache dhama"/>
</div>
<div class="lyrico-lyrics-wrapper">hey zanda peeke dhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey zanda peeke dhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">mana gundenu dhaache dhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana gundenu dhaache dhama"/>
</div>
<div class="lyrico-lyrics-wrapper">are thirunaal ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="are thirunaal ika"/>
</div>
<div class="lyrico-lyrics-wrapper">modhalaindhi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhalaindhi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">mana kurralu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana kurralu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">full safe u raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="full safe u raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">settle ayipoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settle ayipoyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari nee dhigule poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari nee dhigule poyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">settle ayipoyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settle ayipoyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thingari thingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thingari thingari"/>
</div>
<div class="lyrico-lyrics-wrapper">thingari nee dhigule poyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingari nee dhigule poyindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey love u givvantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey love u givvantaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ollu kovvey pattuntaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ollu kovvey pattuntaru"/>
</div>
<div class="lyrico-lyrics-wrapper">mari chancey gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari chancey gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">ichamante chedugudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichamante chedugudu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadestharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadestharu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey poovutho premistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey poovutho premistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">navvu navvutho premistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvu navvutho premistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">arey mozey kaani theerindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey mozey kaani theerindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">katthulu kuripistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katthulu kuripistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nakharaalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nakharaalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">chalinchara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalinchara "/>
</div>
<div class="lyrico-lyrics-wrapper">mari bakaraalu ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari bakaraalu ika"/>
</div>
<div class="lyrico-lyrics-wrapper">kaabomu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaabomu raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">maar maar teen maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maar maar teen maar"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaar dhaar kabar dhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaar dhaar kabar dhaar"/>
</div>
</pre>
