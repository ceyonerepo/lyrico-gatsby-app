---
title: "aandae nootrandae song lyrics"
album: "Mugavari"
artist: "Deva"
lyricist: "Vairamuthu"
director: "V. Z. Durai"
path: "/albums/mugavari-lyrics"
song: "Aandae Nootrandae"
image: ../../images/albumart/mugavari.jpg
date: 2000-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5JJUU5xhqog"
type: "happy"
singers:
  - Naveen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aandae Noottrandae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandae Noottrandae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaagum Noottrandae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaagum Noottrandae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyagam Vaazha Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyagam Vaazha Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Vaasalil Kolam Idu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Vaasalil Kolam Idu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suga Velicham Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suga Velicham Nee Thara Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellam Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Megam Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Megam Nee Thara Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathaigal Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathaigal Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhega Thisukkal Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhega Thisukkal Nee Thara Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraichal Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraichal Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil Inisai Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Inisai Nee Thara Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavukku Poi Varavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavukku Poi Varavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Kannukku Siragu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Kannukku Siragu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vidiyalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vidiyalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejnil Ulaippukku Valimai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejnil Ulaippukku Valimai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noottrandae Noottrandae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noottrandae Noottrandae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noigal Ellaam Kalaivaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noigal Ellaam Kalaivaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhukkillaatha Kaatrum Neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukkillaatha Kaatrum Neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Agilam Muzhuthum Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilam Muzhuthum Tharuvaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Petrolum Theernthu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petrolum Theernthu Vittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karkaalam Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karkaalam Tharuvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaana Vaaganam Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnaana Vaaganam Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porkaalam Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkaalam Tharuvaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Nizhal Orae Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Nizhal Orae Nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondu Vaa Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondu Vaa Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Pagal Orae Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Pagal Orae Nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondu Vaa Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondu Vaa Nee Kondu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiyae Pesaatha Puththulagam Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyae Pesaatha Puththulagam Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Illaatha Poi Sollaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Illaatha Poi Sollaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Ulagam Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Ulagam Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Bhoogambam Engum Neraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Bhoogambam Engum Neraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Bhoomiyai Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Bhoomiyai Nee Kondu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illarathil Pengalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illarathil Pengalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Inba Nilai Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inba Nilai Tharuvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaiyal Arai Ozhintha Veedugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaiyal Arai Ozhintha Veedugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Maarkellam Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Maarkellam Tharuvaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhi Sumakkum Kuzhanthaigalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhi Sumakkum Kuzhanthaigalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthagangal Kuraippaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthagangal Kuraippaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pareetchai Indri Kalviyum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pareetchai Indri Kalviyum Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada Thittam Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Thittam Tharuvaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Mozhi Orae Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Mozhi Orae Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondu Vaa Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondu Vaa Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Nilaa Orae Vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Nilaa Orae Vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondu Vaa Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondu Vaa Nee Kondu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porae Illaatha Pon Ulagam Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porae Illaatha Pon Ulagam Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Paaramal Manam Paarkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Paaramal Manam Paarkindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kaadhal Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kaadhal Nee Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai Kekkaamal Kan Thugilaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Kekkaamal Kan Thugilaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ulagam Nee Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ulagam Nee Kondu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Pudhu Aandae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Pudhu Aandae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Pookkum Nootrandae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Pookkum Nootrandae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Nee Thara Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Punnagai Nee Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Punnagai Nee Thara Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkalamnuzhainthu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkalamnuzhainthu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae Poocharam Nattu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Poocharam Nattu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anugundu Athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anugundu Athanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacific Kadalil Kotti Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacific Kadalil Kotti Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithargal Virumbum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithargal Virumbum Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Manitharai Vaazha Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Manitharai Vaazha Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthuvam Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthuvam Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Maanudam Vaazha Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Maanudam Vaazha Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavukku Poi Varavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavukku Poi Varavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Kannukku Siragu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Kannukku Siragu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vidiyalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vidiyalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejnil Ulaippukku Valimai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejnil Ulaippukku Valimai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome Come Come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come 21st Century
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come 21st Century"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come 21st Century
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come 21st Century"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome Come Come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come 21st Century
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come 21st Century"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome Come Come 21st Century
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Come Come 21st Century"/>
</div>
</pre>
