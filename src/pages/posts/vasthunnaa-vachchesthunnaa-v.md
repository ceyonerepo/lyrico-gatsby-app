---
title: "vasthunnaa vachchesthunnaa song lyrics"
album: "V"
artist: "Amit Trivedi"
lyricist: "Sirivennela Seetharama Sastry"
director: "Mohana Krishna - Indraganti"
path: "/albums/v-lyrics"
song: "Vasthunnaa Vachchesthunnaa"
image: ../../images/albumart/v.jpg
date: 2020-09-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/h9LJu9Fap7Y"
type: "love"
singers:
  - Shreya Ghoshal
  - Amit Trivedi
  - Anurag Kulkarni	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chusthunna Chusthune Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunna Chusthune Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppainaa Padaneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppainaa Padaneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaanani Chebuthunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaanani Chebuthunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku Vinipinchadu Thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Vinipinchadu Thelusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusthunna Chusthune Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunna Chusthune Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppainaa Padaneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppainaa Padaneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Chesthunna Naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Chesthunna Naa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhyasanthaa Nee Meedhe Thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhyasanthaa Nee Meedhe Thelusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choodani Aaganani Oohala Ubalaatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choodani Aaganani Oohala Ubalaatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Usi Koduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usi Koduthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasthunna Vachhesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vachhesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddannaa Vadilesthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddannaa Vadilesthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavvisthu Kanabadakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvisthu Kanabadakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uvvetthuna Urikosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uvvetthuna Urikosthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusthunna Chusthune Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunna Chusthune Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppainaa Padaneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppainaa Padaneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaanani Chebuthunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaanani Chebuthunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku Vinipinchadu Thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Vinipinchadu Thelusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheliya Cheliya Nee Thalape Thariminde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Cheliya Nee Thalape Thariminde"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Alalayye Aaratame Penchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Alalayye Aaratame Penchanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyo Kshanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyo Kshanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Dhooram Kalagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Dhooram Kalagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Baanamlaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Baanamlaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Virahaanni Vetaadagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahaanni Vetaadagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muripinche Musthaabai Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muripinche Musthaabai Unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Darikosthe Andisthaagaa Aanandhamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darikosthe Andisthaagaa Aanandhamgaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippati Ee Oppandhaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippati Ee Oppandhaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Ibbandulu Thappinchaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ibbandulu Thappinchaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatitho Cheppinchaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatitho Cheppinchaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaantham Ippinchaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaantham Ippinchaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasthunna Vachhesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vachhesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddannaa Vadilesthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddannaa Vadilesthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavvisthu Kanabadakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvisthu Kanabadakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uvvetthuna Urikosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uvvetthuna Urikosthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusthunna Chusthune Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunna Chusthune Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppainaa Padaneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppainaa Padaneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaanani Chebuthunnaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaanani Chebuthunnaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku Vinipinchadu Thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Vinipinchadu Thelusaa"/>
</div>
</pre>
