---
title: "kannil song lyrics"
album: "Kappela"
artist: "Sushin Shyam"
lyricist: "Vishnu Shobhana"
director: "Muhammad Musthafa"
path: "/albums/kappela-lyrics"
song: "Kannil"
image: ../../images/albumart/kappela.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/R44vLHE_poE"
type: "love"
singers:
  - Sooraj Santhosh
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannil Vidarum Rathaarangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vidarum Rathaarangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Pularum Pookkalangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Pularum Pookkalangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukil Padaranuyirukonda Mazhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukil Padaranuyirukonda Mazhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikal Parayaathithal Virinja Kadhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikal Parayaathithal Virinja Kadhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhakalo Vazhikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhakalo Vazhikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirathodunna Puthiya Karakalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirathodunna Puthiya Karakalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaam Theliyum Rathaaram Njaanaakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaam Theliyum Rathaaram Njaanaakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaliraay Nirayum Pookkalam Njaanaakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliraay Nirayum Pookkalam Njaanaakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukil Padaranuyirukonda Mazha Njaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukil Padaranuyirukonda Mazha Njaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikal Parayaathithal Virinja Kadha Njaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikal Parayaathithal Virinja Kadha Njaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaveyil Chirakumaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaveyil Chirakumaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuliruthunnuminakalaay Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliruthunnuminakalaay Naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naghamunayoronnil Uthiraa Niramaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naghamunayoronnil Uthiraa Niramaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhayude Chuvaril Shalabhangal Pol Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayude Chuvaril Shalabhangal Pol Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naghamunayoronnil Uthiraa Niramaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naghamunayoronnil Uthiraa Niramaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhayude Chuvaril Shalabhangal Pol Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayude Chuvaril Shalabhangal Pol Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Theruvil Varnangal Thedumpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Theruvil Varnangal Thedumpol"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravashamaay Padarum Chedithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravashamaay Padarum Chedithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalirilakalilonnaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalirilakalilonnaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaay Cherum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaay Cherum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalathilavolam Alasamayoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalathilavolam Alasamayoorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthumazha Nolkunnee Nimishangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthumazha Nolkunnee Nimishangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagoshathinnaveshappazhuthukalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagoshathinnaveshappazhuthukalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashangal Minnunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashangal Minnunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaatheeyil Kaatodum Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaatheeyil Kaatodum Pole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Vidarum Rathaarangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vidarum Rathaarangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Pularum Pookkalangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Pularum Pookkalangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukil Padaranuyirukonda Mazhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukil Padaranuyirukonda Mazhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikal Parayaathithal Virinja Kadhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikal Parayaathithal Virinja Kadhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhakalo Vazhikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhakalo Vazhikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirathodunna Puthiya Karakalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirathodunna Puthiya Karakalo"/>
</div>
</pre>
