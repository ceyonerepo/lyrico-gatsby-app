---
title: "androidu song lyrics"
album: "Kobbari Matta"
artist: "Kamran"
lyricist: "Kittu Vissapragada"
director: "Rupak Ronaldson"
path: "/albums/kobbari-matta-lyrics"
song: "Androidu"
image: ../../images/albumart/kobbari-matta.jpg
date: 2019-08-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XVkugXyyy5g"
type: "happy"
singers:
  - L.V. Revanth
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bhoolokam Banthechesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoolokam Banthechesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Footbaal Aadevinthode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Footbaal Aadevinthode"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugetthe Piduge Veedele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugetthe Piduge Veedele"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhukampam Vache Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhukampam Vache Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyyala Ugepillode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyyala Ugepillode"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedurante Bedtime Story Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedurante Bedtime Story Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulni Shoe Lacega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulni Shoe Lacega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattese Ghanude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattese Ghanude"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaga Sudilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Sudilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilli Moggese Powerithade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilli Moggese Powerithade"/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu "/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock Star"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is Gonna Like A War
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is Gonna Like A War"/>
</div>
<div class="lyrico-lyrics-wrapper">Stronger Than China Wall
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stronger Than China Wall"/>
</div>
<div class="lyrico-lyrics-wrapper">He Looks Soo Killer Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Looks Soo Killer Yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">He Got The Super Power
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Got The Super Power"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is The Real Smart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is The Real Smart"/>
</div>
<div class="lyrico-lyrics-wrapper">His Name Is Burning Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="His Name Is Burning Star"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laavalo Bajjilevestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavalo Bajjilevestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunami Lo Swimmingechestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunami Lo Swimmingechestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyramidpai Pesaratte Tinta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyramidpai Pesaratte Tinta"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Naa Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Naa Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onewaylo 120 Kodatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onewaylo 120 Kodatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Runwaypai Jogginge Chestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Runwaypai Jogginge Chestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasane Errabusekkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasane Errabusekkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavente Mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavente Mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondalapai Pushups Istha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondalapai Pushups Istha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundelapai Bhoomai Mostha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelapai Bhoomai Mostha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastam Naaku Biscuittelera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastam Naaku Biscuittelera"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppetlo Nippulane Daastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppetlo Nippulane Daastha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thufanu Uffani Udestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thufanu Uffani Udestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Prakruthi Naaku Playgroundeleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prakruthi Naaku Playgroundeleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulni Shoe Lacega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulni Shoe Lacega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattese Ghanude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattese Ghanude"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaga Sudilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Sudilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilli Moggese Powerithade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilli Moggese Powerithade"/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu "/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Planetstho Golilaadestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Planetstho Golilaadestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryudine Fry Chesukutinta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryudine Fry Chesukutinta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandralni Strawtho Taagestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandralni Strawtho Taagestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhe World Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhe World Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbultho Chamate Thudichesta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbultho Chamate Thudichesta"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadallo Surfinge Chestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadallo Surfinge Chestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Huricanelo Cheruke Korikestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huricanelo Cheruke Korikestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakinde Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakinde Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rampamtho Thala Duvvestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rampamtho Thala Duvvestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gallilo Rocketho Lestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gallilo Rocketho Lestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parasuitho Vaakitlo Vaalthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parasuitho Vaakitlo Vaalthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dragonstho Stovveligistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dragonstho Stovveligistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinosaurke Golusestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinosaurke Golusestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Escalatorni Treadmilchesestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escalatorni Treadmilchesestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulni Shoe Lacega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulni Shoe Lacega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattese Ghanude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattese Ghanude"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaga Sudilone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga Sudilone"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilli Moggese Powerithade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilli Moggese Powerithade"/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu "/>
</div>
<div class="lyrico-lyrics-wrapper">Androidu Androidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androidu Androidu"/>
</div>
</pre>
