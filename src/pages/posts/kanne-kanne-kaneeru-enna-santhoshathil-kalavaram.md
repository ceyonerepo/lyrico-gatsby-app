---
title: "kanne kanne kaneeru enna song lyrics"
album: "Santhoshathil Kalavaram"
artist: "Sivanag"
lyricist: "Mani Amudhavan"
director: "Kranthi Prasad"
path: "/albums/santhoshathil-kalavaram-lyrics"
song: "Kanne Kanne Kaneeru Enna"
image: ../../images/albumart/santhoshathil-kalavaram.jpg
date: 2018-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hkvkQeCVr30"
type: "sad"
singers:
  - Abhay Jodhpurkar
  - Madhumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">kanneer enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneer enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalai enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalai enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangi kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangi kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">yengaathe neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengaathe neyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kade theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kade theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">patri kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patri kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">kanneer sinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneer sinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai unda"/>
</div>
<div class="lyrico-lyrics-wrapper">kandam vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandam vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">kandam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">oyaamal pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaamal pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veril neere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veril neere"/>
</div>
<div class="lyrico-lyrics-wrapper">theevin nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theevin nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyai kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyai kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">sariyaai kaatumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sariyaai kaatumo"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nathi neengal thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi neengal thane"/>
</div>
<div class="lyrico-lyrics-wrapper">athil neenthum meene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil neenthum meene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nangal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nangal thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbane "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nathi neerai thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi neerai thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">oru theyum vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru theyum vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enai theenduma sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai theenduma sol"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maname vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maname vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhiyil eeram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyil eeram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyal innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal innum "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">iniyum nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniyum nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayangal aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayangal aarum"/>
</div>
</pre>
