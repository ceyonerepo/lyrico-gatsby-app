---
title: "allola kallolam song lyrics"
album: "Plan Panni Pannanum"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Badri Venkatesh"
path: "/albums/plan-panni-pannanum-lyrics"
song: "Allola Kallolam"
image: ../../images/albumart/plan-panni-pannanum.jpg
date: 2021-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/26xFJYf3nBs"
type: "sad"
singers:
  - Remya Nambeesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allola Allolaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allola Allolaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Allola Allolaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allola Allolaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allola Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allola Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nenaichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nenaichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Allola Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allola Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panninaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panninaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ennum Boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ennum Boodham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Engo Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Engo Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhka Oru Kadhaiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka Oru Kadhaiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirappaa Ezhuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirappaa Ezhuthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivennaa…eppodhum Sollaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivennaa…eppodhum Sollaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Oru Nambikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Oru Nambikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Thodaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Thodaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakku Illaama Edhukkum Nillaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakku Illaama Edhukkum Nillaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerathai Paarthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerathai Paarthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Baarathai Koottaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Baarathai Koottaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil Thannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Thannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Velicham Varaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Velicham Varaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvikku Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikku Ellaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Sollitu Pogaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Sollitu Pogaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanoda Thunbamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanoda Thunbamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannudaiya Thunbamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannudaiya Thunbamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Enni Manasu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Enni Manasu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Kooda Marandhu Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Kooda Marandhu Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhamellaam Parandhuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhamellaam Parandhuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukkulla Pirivedhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukkulla Pirivedhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnukkulla Manasundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukkulla Manasundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchiyamum Adhilundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchiyamum Adhilundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Unmai Aakkidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Unmai Aakkidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Varadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Varadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoruththar Latchiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoruththar Latchiyathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallerinju Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallerinju Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosamaana Pazhakkamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosamaana Pazhakkamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Oyaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Oyaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullu Thottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu Thottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Kaayathai Paarkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Kaayathai Paarkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrikku Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrikku Munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta Kaayangal Nikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Kaayangal Nikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanai Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanai Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Kaanama Poyaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Kaanama Poyaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogathin Pinnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogathin Pinnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sandhosam Varadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sandhosam Varadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogathin Pinnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogathin Pinnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sandhosam Varadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sandhosam Varadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogathin Pinnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogathin Pinnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sandhosam Varadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sandhosam Varadhaa"/>
</div>
</pre>
