---
title: "maalai varum vennila song lyrics"
album: "Nenjam Marappathilai"
artist: "Yuvan Shankar Raja"
lyricist: "Dhanush - Yuvan Shankar Raja"
director: "Selvaraghavan"
path: "/albums/nenjam-marappathilai-song-lyrics"
song: "Maalai Varum Vennila"
image: ../../images/albumart/nenjam-marappathilai.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7yhePo7GYXM"
type: "love"
singers:
  - Yuvan Shankar Raja
  - Dhanush
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maalai Varum Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Varum Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhukkul Venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhukkul Venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeporri Paavaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeporri Paavaya"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thedal Un Madiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thedal Un Madiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manasa Thottu Poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasa Thottu Poraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dhinusaa Edho Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhinusaa Edho Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kanni Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kanni Vechchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Unakkum Kadhal Vandhuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unakkum Kadhal Vandhuduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Varum Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Varum Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhukkul Venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhukkul Venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vazhvil Pala Thunaithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvil Pala Thunaithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thevai Oru Thunaithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thevai Oru Thunaithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal Odalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal Odalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayinum Theyalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayinum Theyalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setrile Thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setrile Thaamarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serndhu Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vidhi En Vidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vidhi En Vidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Vidhithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vidhithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manasa Thottu Poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasa Thottu Poraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dhinusaa Edho Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhinusaa Edho Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kanni Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kanni Vechchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Unakkum Kadhal Vandhuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unakkum Kadhal Vandhuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhi Thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhi Thuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhi oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu Nizhal Edhu Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Nizhal Edhu Nijam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Imaikkirai Naan Summakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Imaikkirai Naan Summakiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Aasaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kelvigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malare Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malare Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manasa Thottu Poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasa Thottu Poraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dhinusaa Edho Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhinusaa Edho Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kanni Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kanni Vechchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Unakkum Kadhal Vandhuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unakkum Kadhal Vandhuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramasamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramsay Ramsay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramsay Ramsay"/>
</div>
</pre>
