---
title: "chal wahin chalein song lyrics"
album: "Saina"
artist: "Amaal Mallik"
lyricist: "Manoj Muntashir"
director: "Amole Gupte"
path: "/albums/saina-lyrics"
song: "Chal Wahin Chalein"
image: ../../images/albumart/saina.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/3oDQbY3is1s"
type: "Affection"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jahan saanson ne daud lagayi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan saanson ne daud lagayi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan neendon se koyi ladayi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan neendon se koyi ladayi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan paidon ka saya nadi tak hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan paidon ka saya nadi tak hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan jheelon mein chaand abhi tak hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan jheelon mein chaand abhi tak hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jahan hasne pe shartein na ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan hasne pe shartein na ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Log jeene se darte na ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Log jeene se darte na ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Befikar apne ghar se nikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Befikar apne ghar se nikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasta dil ko tere pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasta dil ko tere pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raah mein shaam hogi kahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raah mein shaam hogi kahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh musaafir kahaan sochta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh musaafir kahaan sochta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jahaan ankhein aansu na jaanein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahaan ankhein aansu na jaanein"/>
</div>
<div class="lyrico-lyrics-wrapper">Muskurane ke hon sau bahaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muskurane ke hon sau bahaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roshni pyaar jaisi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roshni pyaar jaisi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitaare bhi humne hai aazmayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitaare bhi humne hai aazmayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh zameen yaad aayi to hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh zameen yaad aayi to hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasmaanon se bhi laut aayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasmaanon se bhi laut aayein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jahan sar pe koi haath phere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan sar pe koi haath phere"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan apno ne rang ho bikhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan apno ne rang ho bikhere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal wahin chalein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal wahin chalein"/>
</div>
<div class="lyrico-lyrics-wrapper">Na jate ho jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na jate ho jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan ke raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan ke raaste"/>
</div>
</pre>
