---
title: "kooduvittu koodu song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Thamarai - Inno Genga"
director: "Madhan Karky"
path: "/albums/bogan-lyrics"
song: "Kooduvittu Koodu"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yXQV6zFDRmg"
type: "happy"
singers:
  - Jyothi Nooran
  - Arvind Swamy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerach selavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerach selavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai thuravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai thuravu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poli uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poli uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam kadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam kadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil ulavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil ulavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhinthadhu iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhinthadhu iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena gnyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena gnyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kolbavan moodan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolbavan moodan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyum naatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyum naatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin peyrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin peyrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Solbavan bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solbavan bogan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu paanjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu paanjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meni vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meni vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meni meinjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meni meinjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnae bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnae bogan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">melae saanjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melae saanjan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai thiratchai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai thiratchai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itchai moottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itchai moottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyo keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyo keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nattu nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nattu nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyathilae serthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyathilae serthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motha boomiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha boomiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogathu jothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogathu jothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bogan thindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bogan thindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerinil boganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerinil boganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kaamanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kaamanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolvaan beedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvaan beedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnil mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum bogan villaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum bogan villaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan villaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan villaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam loaded gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam loaded gun"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mutham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mutham "/>
</div>
<div class="lyrico-lyrics-wrapper">thuppum dragon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuppum dragon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Listen up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Listen up"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbum arivum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbum arivum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panbum kazhudhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbum kazhudhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhavikendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhavikendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivali maathirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivali maathirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangum porulin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangum porulin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilai pattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilai pattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi paarpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi paarpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porul ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porul ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanadhae enbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanadhae enbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani oruvankullae ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani oruvankullae ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru prabanjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru prabanjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marainjirukkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marainjirukkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan manaveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan manaveli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai naasa pesaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai naasa pesaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Grahangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pandhaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai pandhaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbiduvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbiduvaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karunguli kullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunguli kullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbiduvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbiduvaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pudhu pudhayalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pudhu pudhayalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhiduvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhiduvaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuvadhum rusithadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvadhum rusithadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhiduvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhiduvaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnil mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan villaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan villaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam loaded gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam loaded gun"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mutham thuppum dragon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mutham thuppum dragon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hit me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu paanjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu paanjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meni vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meni vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meni meinjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meni meinjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnae bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnae bogan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">melae saanjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melae saanjan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai thiratchai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai thiratchai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itchai moottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itchai moottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyo keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyo keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nattu nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nattu nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyathilae serthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyathilae serthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh lovely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh lovely"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motha boomiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha boomiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogathu jothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogathu jothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bogan thindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bogan thindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerinil boganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerinil boganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kaamanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kaamanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolvaan beedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvaan beedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnil mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum bogan villaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum bogan villaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan villaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan villaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Say it again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say it again"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B O G A N
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B O G A N"/>
</div>
<div class="lyrico-lyrics-wrapper">Say boga boga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say boga boga"/>
</div>
<div class="lyrico-lyrics-wrapper">BOGAN
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BOGAN"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam loaded gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam loaded gun"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mutham thuppum dragon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mutham thuppum dragon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazha pirandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha pirandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam aala pirandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam aala pirandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bogan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bogan"/>
</div>
</pre>
