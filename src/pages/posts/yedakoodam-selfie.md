---
title: "yedakoodam song lyrics"
album: "Selfie"
artist: "GV Prakash Kumar"
lyricist: "Arivu"
director: "Mathi Maran"
path: "/albums/selfie-lyrics"
song: "Yedakoodam"
image: ../../images/albumart/selfie.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lUmj0si-A_s"
type: "mass"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thottu Paaru Thottu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaru Thottu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora Kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattupaadu Vittu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattupaadu Vittu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukkutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buthikulla Buthikullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthikulla Buthikullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathukkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathukkatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha Thudikuthu Una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Thudikuthu Una"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavugraaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavugraaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soora Kenda Kottayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kenda Kottayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Era Kunju Vetta Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era Kunju Vetta Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Araathaana Moolaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araathaana Moolaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara Vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Kanthal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Kanthal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Potta Route-U La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Potta Route-U La"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Paatha Kaatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Paatha Kaatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangatha Seekiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangatha Seekiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangitta Oru End Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangitta Oru End Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeda Koodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeda Koodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemathu Paadam Paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemathu Paadam Paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedakoodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedakoodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemathu Paadam Paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemathu Paadam Paadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Vazhi Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Vazhi Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvathai Parugidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathai Parugidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangidu Muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangidu Muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathenavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvedu Uruvedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvedu Uruvedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhiyena Malayinai Thuzhaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhiyena Malayinai Thuzhaiyidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerangidu Thunivathe Thunaiyenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerangidu Thunivathe Thunaiyenavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaam Ellam Thaarai Vaarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaam Ellam Thaarai Vaarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu Un Pokile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Un Pokile"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettai Ellam Thookki Potte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettai Ellam Thookki Potte"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeru Nee Metile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru Nee Metile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanagave Aalagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanagave Aalagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanana Medhavi Aanalum Aadama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanana Medhavi Aanalum Aadama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Odidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Odidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soora Kenda Kottayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kenda Kottayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Era Kunju Vetta Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era Kunju Vetta Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Araathaana Moolaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araathaana Moolaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara Vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Kanthal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Kanthal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Potta Route-U La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Potta Route-U La"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Paatha Kaatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Paatha Kaatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangatha Seekiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangatha Seekiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangitta Oru End Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangitta Oru End Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeda Koodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeda Koodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemathu Paadam Paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemathu Paadam Paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedakoodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedakoodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemathu Paadam Paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemathu Paadam Paadam"/>
</div>
</pre>
