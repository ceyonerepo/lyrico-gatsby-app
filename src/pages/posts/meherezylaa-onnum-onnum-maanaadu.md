---
title: "meherezylaa song lyrics"
album: "Maanaadu"
artist: "Yuvan Shankar Raja"
lyricist: "Madhan Karky"
director: "Venkat Prabhu"
path: "/albums/maanaadu-lyrics"
song: "Meherezylaa"
image: ../../images/albumart/maanaadu.jpg
date: 2021-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vQAs_P9JrJQ"
type: "Mass"
singers:
  - Yuvan Shankar Raja
  - Rizwan
  - Raja Bhavatharini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Onnum Onnum Rendulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Onnum Rendulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Inga Bundle-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Inga Bundle-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kuda Vetkamillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kuda Vetkamillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Namma Friendlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Namma Friendlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suruman Theettum Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suruman Theettum Vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam Vantha Dracula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam Vantha Dracula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikka Panni Nikkum Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikka Panni Nikkum Intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan Life-ae Joke-laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyan Life-ae Joke-laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masthana Pola Mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthana Pola Mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaanae Aala Thukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaanae Aala Thukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnala Ninnu Kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Ninnu Kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga Sernthoomlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga Sernthoomlaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Inbam Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Inbam Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Inbam Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Inbam Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Bhoomi Paarula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Bhoomi Paarula"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Usuru Thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Usuru Thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Manasil Otha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Manasil Otha Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Othukitta Pothumlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othukitta Pothumlaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Avala Paarulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Avala Paarulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Neeyaa Vazhulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Neeyaa Vazhulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara Venaam Maatha Venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara Venaam Maatha Venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purunchikitta Pothumlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purunchikitta Pothumlaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mothal Illama Uravilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothal Illama Uravilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandainnu Vantha Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandainnu Vantha Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannippu Ketta Thavarilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannippu Ketta Thavarilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhakai Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhakai Unnodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Inbam Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Inbam Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathin Melae Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathin Melae Unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mithanthu Vanthen Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithanthu Vanthen Thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bhoomi Engum Pookaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bhoomi Engum Pookaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Kannalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Kannalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masha Allah Masha Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masha Allah Masha Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Masha Allah Masha Allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masha Allah Masha Allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Neethan Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Neethan Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Inbam Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Inbam Kadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meherezylaa Meherezylaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meherezylaa Meherezylaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Alla Ellaiyillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Alla Ellaiyillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Inbam Kadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Inbam Kadhalaa"/>
</div>
</pre>
