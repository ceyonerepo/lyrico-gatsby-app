---
title: "sokkura penne song lyrics"
album: "Devi 2"
artist: "Sam C. S."
lyricist: "Arunraja Kamaraj"
director: "A.L. Vijay"
path: "/albums/devi-2-lyrics"
song: "Sokkura Penne"
image: ../../images/albumart/devi-2.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OHmg-8GMqqc"
type: "love"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Ponnungo Avala Paathengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponnungo Avala Paathengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunguma Colorungo Thanga Selayaingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunguma Colorungo Thanga Selayaingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada En Manasa Thavikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada En Manasa Thavikka"/>
</div>
 <div class="lyrico-lyrics-wrapper">Vitta Coloru Avalungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Coloru Avalungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ramango Naan Illengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ramango Naan Illengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Seethaiyum Kooda Illengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Seethaiyum Kooda Illengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada One Side Ah Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada One Side Ah Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Vizhum Love Storyngo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Vizhum Love Storyngo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungappa Enakku Uncle-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungappa Enakku Uncle-u Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ilangatti Naan Single-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilangatti Naan Single-u Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Vantha Pongaluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Vantha Pongaluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkama Pona Jungle-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkama Pona Jungle-u Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungappa Enakku Uncle-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungappa Enakku Uncle-u Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ilangatti Naan Single-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilangatti Naan Single-u Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Vantha Pongaluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Vantha Pongaluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkama Pona Jungle-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkama Pona Jungle-u Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kirukkana Aakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kirukkana Aakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Kadhalukku Koottaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Kadhalukku Koottaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Vechen Perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Vechen Perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukka Maatrendraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukka Maatrendraale "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Muzhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Muzhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkma Sikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkma Sikka "/>
</div>
<div class="lyrico-lyrics-wrapper">Vechutale Dhinusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechutale Dhinusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Kadhalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Kadhalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiya Vechu Kizhicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiya Vechu Kizhicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Adi Vaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Adi Vaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Ennai Thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Ennai Thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Adi Illama Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Adi Illama Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladuren Kolladhadiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladuren Kolladhadiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kirukkana Aakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kirukkana Aakatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Pechi Moochi Ellam Pudhusu Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Pechi Moochi Ellam Pudhusu Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Kaachu Moochu Ellam Sweet-u Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Kaachu Moochu Ellam Sweet-u Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnala Naanum Varen Uyira Kudukka Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnala Naanum Varen Uyira Kudukka Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Venamnu Sollathadi Manasu Valikkum Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venamnu Sollathadi Manasu Valikkum Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rosu Un Kanna Rendum Rosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosu Un Kanna Rendum Rosu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bangarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bangarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesu Nee Love-u Varthai Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu Nee Love-u Varthai Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bangarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bangarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass’u Namma Rendu Perum Onna Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass’u Namma Rendu Perum Onna Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasaagidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasaagidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kirukkana Aakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkura Penne Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne Sokkura Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Neeyum Sokkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Neeyum Sokkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkura Nenje Vekkura Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Nenje Vekkura Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kirukkana Aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kirukkana Aakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkura Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkura Penne"/>
</div>
</pre>
