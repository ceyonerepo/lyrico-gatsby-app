---
title: "ye di raasathi song lyrics"
album: "100"
artist: "Sam C.S."
lyricist: "Kavita Thomas"
director: "Sam Anton"
path: "/albums/100-lyrics"
song: "Ye Di Raasathi"
image: ../../images/albumart/100.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ryvt86hRijs"
type: "love"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yen Nenjila Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nenjila Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aasaigal Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aasaigal Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yen Nenjila Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nenjila Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aasaigal Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aasaigal Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenja Pichi Pichi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenja Pichi Pichi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi Thachi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi Thachi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhu Konji Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhu Konji Konji Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Aasa Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Aasa Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenja Pichi Pichi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenja Pichi Pichi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi Thachi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi Thachi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhu Konji Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhu Konji Konji Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Aasa Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Aasa Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yen Nenjila Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nenjila Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aasaigal Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aasaigal Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Naanum Paathen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Naanum Paathen Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naala Thookkam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naala Thookkam Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikka Vachaaye Adiye Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikka Vachaaye Adiye Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sethachaaye Kodiye Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sethachaaye Kodiye Mulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Naanum Paathen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Naanum Paathen Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naala Thookkam Ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naala Thookkam Ille"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikka Vachaaye Adiye Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikka Vachaaye Adiye Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sethachaaye Kodiye Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sethachaaye Kodiye Mulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokku Podi Thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokku Podi Thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Vaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththu Kuli Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththu Kuli Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Neeyum Rendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Neeyum Rendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Vaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachi Nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachi Nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenja Pichi Pichi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenja Pichi Pichi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi Thachi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi Thachi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhu Konji Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhu Konji Konji Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Aasa Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Aasa Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Nee Enakkulla Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Nee Enakkulla Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraagi Ullukulla Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraagi Ullukulla Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kadhal Thanthu Thanthu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kadhal Thanthu Thanthu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkaakki Vittu Vittu Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkaakki Vittu Vittu Konna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Nee Enakkulla Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Nee Enakkulla Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraagi Ullukulla Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraagi Ullukulla Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kadhal Thanthu Thanthu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kadhal Thanthu Thanthu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkaakki Vittu Vittu Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkaakki Vittu Vittu Konna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Penna Padacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Penna Padacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhrammanukku Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhrammanukku Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Rasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Rasana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Suzhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Suzhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Maatum Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Maatum Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nizhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nizhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenja Pichi Pichi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenja Pichi Pichi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi Thachi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi Thachi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhu Konji Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhu Konji Konji Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Aasa Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Aasa Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenja Pichi Pichi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenja Pichi Pichi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ulla Vachi Thachi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ulla Vachi Thachi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhu Konji Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhu Konji Konji Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Aasa Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Aasa Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yen Nenjila Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Nenjila Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Di Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Di Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aasaigal Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aasaigal Thantha"/>
</div>
</pre>
