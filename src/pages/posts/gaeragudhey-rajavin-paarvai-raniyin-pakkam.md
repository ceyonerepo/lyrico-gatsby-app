---
title: "gaeragudhey song lyrics"
album: "Rajavin Paarvai Raniyin Pakkam"
artist: "Leander Lee Marty"
lyricist: "Vignesh K Jeyapal"
director: "Azhagu Raj"
path: "/albums/rajavin-paarvai-raniyin-pakkam-lyrics"
song: "Gaeragudhey"
image: ../../images/albumart/rajavin-paarvai-raniyin-pakkam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qDg1jl5OqG8"
type: "love"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannu munnu than theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu munnu than theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">gaeragudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaeragudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam aranju kallil enaichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam aranju kallil enaichu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanna parikum alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna parikum alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">silayaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silayaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">melilayvum irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melilayvum irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi asaivil valachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi asaivil valachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nelicha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelicha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan banthavaaga thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan banthavaaga thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">ena ethetho ava senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena ethetho ava senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">adi intha botha theliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi intha botha theliya"/>
</div>
<div class="lyrico-lyrics-wrapper">oru anju aaru naal aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru anju aaru naal aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannu munnu than theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu munnu than theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">gaeragudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaeragudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni kinni naan podala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni kinni naan podala"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumaaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumaaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thikki thavuren mutti mothuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikki thavuren mutti mothuren"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam illama nan valiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam illama nan valiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">pitham piduchu than thiriyurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pitham piduchu than thiriyurene"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalule nan nulaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalule nan nulaiyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kitta nerungave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta nerungave"/>
</div>
<div class="lyrico-lyrics-wrapper">oru aasa oru aasaiyil thavikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru aasa oru aasaiyil thavikirene"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam varave nan pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam varave nan pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">nan pesa thayangurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pesa thayangurene"/>
</div>
<div class="lyrico-lyrics-wrapper">thonda kuzhi varai vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thonda kuzhi varai vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">varthaiyum enga ponuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varthaiyum enga ponuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga enga ponuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga enga ponuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">manda kolambure un pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manda kolambure un pola"/>
</div>
<div class="lyrico-lyrics-wrapper">oruthiya papenu nenachuthilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthiya papenu nenachuthilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan banthavaaga thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan banthavaaga thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">ena ethetho ava senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena ethetho ava senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">adi intha botha theliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi intha botha theliya"/>
</div>
<div class="lyrico-lyrics-wrapper">oru anju aaru naal aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru anju aaru naal aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannu munnu than theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu munnu than theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">gaeragudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaeragudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni kinni naan podala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni kinni naan podala"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumaaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumaaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thikki thavuren mutti mothuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikki thavuren mutti mothuren"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam illama nan valiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam illama nan valiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">pitham piduchu than thiriyurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pitham piduchu than thiriyurene"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalule un azhagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalule un azhagile"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhagile un azhagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhagile un azhagile"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhagile un azhagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhagile un azhagile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kandukaama ne pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandukaama ne pona "/>
</div>
<div class="lyrico-lyrics-wrapper">ne pona viduvana naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pona viduvana naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unakandi thavama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakandi thavama than"/>
</div>
<div class="lyrico-lyrics-wrapper">thavama than kedapene nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavama than kedapene nan"/>
</div>
<div class="lyrico-lyrics-wrapper">mota madiyil kotti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mota madiyil kotti "/>
</div>
<div class="lyrico-lyrics-wrapper">kaigire vaththal aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigire vaththal aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi podanum un pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi podanum un pola"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya papenu nenachuthilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya papenu nenachuthilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan banthavaaga thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan banthavaaga thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">ena ethetho ava senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena ethetho ava senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">adi intha botha theliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi intha botha theliya"/>
</div>
<div class="lyrico-lyrics-wrapper">oru anju aaru naal aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru anju aaru naal aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangam aranju kallil enaichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam aranju kallil enaichu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanna parikum alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna parikum alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">silayaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silayaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">melilayvum irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melilayvum irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi asaivil valachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi asaivil valachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nelicha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelicha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnodu nanum seramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu nanum seramal"/>
</div>
<div class="lyrico-lyrics-wrapper">en annam annam kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en annam annam kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu thooram koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu thooram koodathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ne indri ethum oodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne indri ethum oodathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaneer kooda vendame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaneer kooda vendame"/>
</div>
<div class="lyrico-lyrics-wrapper">enneramum nan vaalvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enneramum nan vaalvene"/>
</div>
<div class="lyrico-lyrics-wrapper">nan maari ponen unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan maari ponen unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye enaku gaeragudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye enaku gaeragudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gaeragudhey gaeragudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaeragudhey gaeragudhey"/>
</div>
</pre>
