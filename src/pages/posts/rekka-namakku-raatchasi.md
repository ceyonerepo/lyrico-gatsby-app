---
title: "rekka namakku song lyrics"
album: "Raatchasi"
artist: "Sean Roldan"
lyricist: "Sy. Gowthamraj"
director: "Sy Gowthamraj"
path: "/albums/raatchasi-lyrics"
song: "Rekka Namakku"
image: ../../images/albumart/raatchasi.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0KowwXsjPFs"
type: "happy"
singers:
  - Srinidhi S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rekka Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Molachiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Molachiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapom Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapom Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ippa Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ippa Sernthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Puthusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Puthusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Pirandhirukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Pirandhirukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthaattaam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaattaam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukae Unna Vechi Kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukae Unna Vechi Kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirama Irundhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirama Irundhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorae Unna Suthum Vandaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae Unna Suthum Vandaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekka Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Molachiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Molachiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapom Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapom Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ippa Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ippa Sernthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Puthusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Puthusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Pirandhirukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Pirandhirukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthaattaam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaattaam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam Pottukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Pottukalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattum Paadikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattum Paadikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhichidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhichidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootaa Serndhukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaa Serndhukalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Pottukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pottukalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Monanja Thuninja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monanja Thuninja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaagalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalaala Pala Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalaala Pala Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Irukae Namma Jeyikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Irukae Namma Jeyikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Ennanu Therinji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Ennanu Therinji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandadhum Thurakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandadhum Thurakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Manushanum Thanidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Manushanum Thanidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkula Iruppadhu Idhudhaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkula Iruppadhu Idhudhaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaa Thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaa Thirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukae Unna Vechi Kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukae Unna Vechi Kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirama Irundhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirama Irundhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorae Unna Suthum Vandaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae Unna Suthum Vandaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvi Ketukittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi Ketukittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhila Thedikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhila Thedikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruvaa Unakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruvaa Unakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aagalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhuthil Yerikitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhuthil Yerikitaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Therikitaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Therikitaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaa Unaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaa Unaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Theetalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Theetalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhayum Pudhusupola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhayum Pudhusupola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Adhayum Dhinusupola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Adhayum Dhinusupola"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Senjaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Senjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Thurakkum Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Thurakkum Munnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Manushanum Thanidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Manushanum Thanidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkula Iruppadhu Idhudhaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkula Iruppadhu Idhudhaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaa Thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaa Thirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Unakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukae Unna Vechi Kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukae Unna Vechi Kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirama Irundhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirama Irundhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorae Unna Suthum Vandaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae Unna Suthum Vandaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekka Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Molachiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Molachiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapom Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapom Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Ippa Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Ippa Sernthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Puthusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Puthusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Pirandhirukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Pirandhirukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthaattaam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaattaam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Rekka Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Rekka Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Molachirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molachirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Rekka Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Rekka Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Molachirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molachirukku"/>
</div>
</pre>
