---
title: "drill karte song lyrics"
album: "Punya Paap"
artist: "Karan Kanchan - NDS"
lyricist: "DIVINE - Dutchavelli"
director: "Divine"
path: "/albums/punya-paap-lyrics"
song: "Drill Karte"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/1nXiRcY0jUY"
type: "happy"
singers:
  - DIVINE
  - Dutchavelli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uk wale bole, bhai tu hai proper
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uk wale bole, bhai tu hai proper"/>
</div>
<div class="lyrico-lyrics-wrapper">Boys from the naka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys from the naka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mera crew"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mеra crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mеra crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wisdom hai bars, loyalty ya royalty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wisdom hai bars, loyalty ya royalty"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhimaan buddha bola kuch nahi joint pi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhimaan buddha bola kuch nahi joint pi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisika band toh kisika kholta hai woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisika band toh kisika kholta hai woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut saare peete bas bolta nahi tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut saare peete bas bolta nahi tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo bolta bas bolta chalta nahi tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bolta bas bolta chalta nahi tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tеra kya sunoon mere ghar ka nahi tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tеra kya sunoon mere ghar ka nahi tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere peeche time kaun jal paani tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere peeche time kaun jal paani tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaanwar toh public hai ban kare zoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanwar toh public hai ban kare zoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan mantar hai gaane thode antaryami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan mantar hai gaane thode antaryami"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat 10 ke baad chalu road wale gaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat 10 ke baad chalu road wale gaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Road ko chhod hum mor ko nachadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Road ko chhod hum mor ko nachadein"/>
</div>
<div class="lyrico-lyrics-wrapper">Floor ko nachadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Floor ko nachadein"/>
</div>
<div class="lyrico-lyrics-wrapper">Police kare hands up aur chor ko nachadein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police kare hands up aur chor ko nachadein"/>
</div>
<div class="lyrico-lyrics-wrapper">Door khole kitne jo room mein wo jaante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door khole kitne jo room mein wo jaante"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bahar baithe jalre woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bahar baithe jalre woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Wo bahar baithe jalte reh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wo bahar baithe jalte reh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazdoor wale pair jab tak jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazdoor wale pair jab tak jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tab tak chalte rahe hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tab tak chalte rahe hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Halke re tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halke re tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazdoor wale pair jab tak jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazdoor wale pair jab tak jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tab tak chalte rahe hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tab tak chalte rahe hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Halke re tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halke re tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazdoor wale pair jab tak jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazdoor wale pair jab tak jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tab tak chalte rehe hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tab tak chalte rehe hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Halke re tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halke re tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazdoor wale pair jab tak jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazdoor wale pair jab tak jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tab tak chalte rehe hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tab tak chalte rehe hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Halke re tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halke re tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Halke re, halke re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halke re, halke re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera dhan tera shun kuch pal ka woh halka hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera dhan tera shun kuch pal ka woh halka hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhimaan shishya bana game poora sir ka hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhimaan shishya bana game poora sir ka hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya jeena banke roshni ya jeena banke parchhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya jeena banke roshni ya jeena banke parchhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mera crew"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keep walking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep walking"/>
</div>
<div class="lyrico-lyrics-wrapper">While all the haters in my comments keep talking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="While all the haters in my comments keep talking"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm running up a cheque with bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm running up a cheque with bro"/>
</div>
<div class="lyrico-lyrics-wrapper">If it ain't about the doe, it can't be important
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If it ain't about the doe, it can't be important"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm miles out where the lamb got slaughtered
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm miles out where the lamb got slaughtered"/>
</div>
<div class="lyrico-lyrics-wrapper">Big four in my hand be cautious
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big four in my hand be cautious"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm gonna run a man down in his forces
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm gonna run a man down in his forces"/>
</div>
<div class="lyrico-lyrics-wrapper">Needs be I'm boom the door in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needs be I'm boom the door in"/>
</div>
<div class="lyrico-lyrics-wrapper">Needs be I'm gonna blow the whole place up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needs be I'm gonna blow the whole place up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I won't leave no warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I won't leave no warning"/>
</div>
<div class="lyrico-lyrics-wrapper">See how they slept on me like a mattress on the floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See how they slept on me like a mattress on the floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Bro but I soon be touring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bro but I soon be touring"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh, you used to be the man back then
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh, you used to be the man back then"/>
</div>
<div class="lyrico-lyrics-wrapper">Then you got too boring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then you got too boring"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah, I really had a plan back then
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah, I really had a plan back then"/>
</div>
<div class="lyrico-lyrics-wrapper">I was in a fast lane in a foreign
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I was in a fast lane in a foreign"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick that thot out the bando
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick that thot out the bando"/>
</div>
<div class="lyrico-lyrics-wrapper">She didn't want to show the mandem love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She didn't want to show the mandem love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Break down the doors
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break down the doors"/>
</div>
<div class="lyrico-lyrics-wrapper">My mum knows I'm a hustler
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My mum knows I'm a hustler"/>
</div>
<div class="lyrico-lyrics-wrapper">These niggas don't want no smoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="These niggas don't want no smoke"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm on a boat to india
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm on a boat to india"/>
</div>
<div class="lyrico-lyrics-wrapper">Divine and dutchavelli no they can't get rid of us
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divine and dutchavelli no they can't get rid of us"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main mera khudka main khud se karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mera khudka main khud se karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo bhi khudse main khude pe hi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo bhi khudse main khude pe hi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste kharab tab to lit mere shoes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste kharab tab to lit mere shoes"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbaad mat kar waqt mera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbaad mat kar waqt mera tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wafadaar hoshiyar khabardaar mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wafadaar hoshiyar khabardaar mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal chalte reh tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal chalte reh tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar for bar sabse hard misaal mera crew
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar for bar sabse hard misaal mera crew"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
</pre>
