---
title: "the thimmarusu song lyrics"
album: "Thimmarusu"
artist: "Sricharan Pakala"
lyricist: "Kittu Vissapragada"
director: "Sharan Koppisetty"
path: "/albums/thimmarusu-lyrics"
song: "The Thimmarusu"
image: ../../images/albumart/thimmarusu.jpg
date: 2021-07-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wFAC5PHe95U"
type: "intro"
singers:
  - Raghu Dixit
  - Jyotsna Pakala
  - Ambika Sashittal
  - Yamini Ghantasala 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Nalla kotulona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nalla kotulona "/>
</div>
<div class="lyrico-lyrics-wrapper">nyaya devathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyaya devathena"/>
</div>
<div class="lyrico-lyrics-wrapper">pranamochhi neela maarindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranamochhi neela maarindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rate kattaleni route 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rate kattaleni route "/>
</div>
<div class="lyrico-lyrics-wrapper">lona nuvve sagaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lona nuvve sagaga"/>
</div>
<div class="lyrico-lyrics-wrapper">hey daari thappakunda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey daari thappakunda "/>
</div>
<div class="lyrico-lyrics-wrapper">vyuvaham edho raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vyuvaham edho raasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantri laga untu kshanamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantri laga untu kshanamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju kani rajai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju kani rajai "/>
</div>
<div class="lyrico-lyrics-wrapper">maatathoti yuddam chesada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatathoti yuddam chesada"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane radhe ye labamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane radhe ye labamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopa roopamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopa roopamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina saage ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina saage ee "/>
</div>
<div class="lyrico-lyrics-wrapper">yuddamu satyaveshanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yuddamu satyaveshanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane radhe ye labamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane radhe ye labamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopa roopamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopa roopamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina saage ee yuddamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina saage ee yuddamu "/>
</div>
<div class="lyrico-lyrics-wrapper">satyanni shodinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satyanni shodinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmarusu oho Thimmarusu oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmarusu oho Thimmarusu oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmarusu oho Thimmarusu oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmarusu oho Thimmarusu oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmanne rakshinche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmanne rakshinche "/>
</div>
<div class="lyrico-lyrics-wrapper">margamlo ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="margamlo ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Geethalne thakindi ee puranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geethalne thakindi ee puranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karama yogulunna chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karama yogulunna chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Paridhi daati saguthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paridhi daati saguthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethi maata thapputhunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethi maata thapputhunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala gathulu marchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala gathulu marchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappatadugu vesinattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappatadugu vesinattu "/>
</div>
<div class="lyrico-lyrics-wrapper">telivithoni aataladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="telivithoni aataladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daariloki techhukunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daariloki techhukunte "/>
</div>
<div class="lyrico-lyrics-wrapper">thappe kadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappe kadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane radhe ye labamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane radhe ye labamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopa roopamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopa roopamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina saage ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina saage ee "/>
</div>
<div class="lyrico-lyrics-wrapper">yuddamu satyaveshanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yuddamu satyaveshanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raane radhe ye labamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raane radhe ye labamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopa roopamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopa roopamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina saage ee yuddamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina saage ee yuddamu "/>
</div>
<div class="lyrico-lyrics-wrapper">satyanni shodinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satyanni shodinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmarusu oho Thimmarusu oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmarusu oho Thimmarusu oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmarusu oho Thimmarusu oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmarusu oho Thimmarusu oho"/>
</div>
</pre>
