---
title: 'evano oruvan vaasickiran song lyrics'
album: 'Alaipayuthey'
artist: 'A R Rahman'
lyricist: 'Vairamuthu'
director: 'Maniratnam'
path: '/albums/Alaipayuthey-song-lyrics'
song: 'Evano Oruvan Vaasickiran'
image: ../../images/albumart/Alaipayuthey.jpg
date: 2000-04-14
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/Ol2oJlHjKsw'
type: 'sad'
singers: 
- Swarnalatha
---

Alaipayuthe song lyrics

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thavam pol irundhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavam pol irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yosikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai thavanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhai thavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraiyil nesikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Muraiyil nesikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thavam pol irundhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavam pol irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yosikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai thavanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhai thavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraiyil nesikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Muraiyil nesikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kettu kettu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu kettu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirangukiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kirangukiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketpadhu evano
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketpadhu evano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyavillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ariyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaattu moongilin
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaattu moongilin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhukullae avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhukullae avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhum ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhum ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyavillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Puriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pullanguzhalae poonguzhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullanguzhalae poonguzhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum oru jaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum oru jaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullanguzhalae poonguzhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullanguzhalae poonguzhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum oru jaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum oru jaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ullae urangum yekkathilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullae urangum yekkathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari paadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sari paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kangalai varudum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kangalai varudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenisaiyil en
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaenisaiyil en"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam kavalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhiruppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Marandhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Innisai mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Innisai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai endraal naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Illai endraal naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endro endro
<input type="checkbox" class="lyrico-select-lyric-line" value="Endro endro"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandhiruppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Irandhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Urakkam illa munniravil
<input type="checkbox" class="lyrico-select-lyric-line" value="Urakkam illa munniravil"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulmanadhil
<input type="checkbox" class="lyrico-select-lyric-line" value="En ulmanadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru maarudhalaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru maarudhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urakkam illa munniravil
<input type="checkbox" class="lyrico-select-lyric-line" value="Urakkam illa munniravil"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulmanadhil
<input type="checkbox" class="lyrico-select-lyric-line" value="En ulmanadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru maarudhalaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru maarudhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Erakkam illa iravugalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Erakkam illa iravugalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu evano anuppum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu evano anuppum"/>
</div>
<div class="lyrico-lyrics-wrapper">arudhalaa
<input type="checkbox" class="lyrico-select-lyric-line" value="arudhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Endhan sogam theervadharku
<input type="checkbox" class="lyrico-select-lyric-line" value="Endhan sogam theervadharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pol marundhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu pol marundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piridhillaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Piridhillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kuzhalai pol
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kuzhalai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuvadharku athanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhuvadharku athanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal enakkillayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kangal enakkillayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan Yaasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evano oruvan vaasikkiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Evano oruvan vaasikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil irundhu naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruttil irundhu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaasikkiren"/>
</div>
</pre>