---
title: "dhana dhana dhann song lyrics"
album: "Mallesham"
artist: "Mark K Robin"
lyricist: "Goreti Venkanna"
director: "Raj R"
path: "/albums/mallesham-lyrics"
song: "Dhana Dhana Dhann"
image: ../../images/albumart/mallesham.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/DkxtO-6cMZU"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Palle Odilona Pillala Gudi Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Odilona Pillala Gudi Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Rellu Podalona Gaali Sayyata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rellu Podalona Gaali Sayyata "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paata Paadeti Pillalu Oota Selimiloo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Paadeti Pillalu Oota Selimiloo "/>
</div>
<div class="lyrico-lyrics-wrapper">Aataladeti Pillalu Mota Girakaloo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataladeti Pillalu Mota Girakaloo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palle Odilona Pillala Gudi Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Odilona Pillala Gudi Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Rellu Podalona Gaali Sayyata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rellu Podalona Gaali Sayyata"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagulona Sepole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagulona Sepole "/>
</div>
<div class="lyrico-lyrics-wrapper">Legadhooda Ganthole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Legadhooda Ganthole "/>
</div>
<div class="lyrico-lyrics-wrapper">Yeyra Sindheyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeyra Sindheyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeyra Sitikeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeyra Sitikeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanakdhana Dharuveyra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanakdhana Dharuveyra "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakjana Sitikeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakjana Sitikeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanakdhana Dharuveyra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanakdhana Dharuveyra "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakjana Sitikeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakjana Sitikeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagale Vennelaa Kaaseti Vaagullona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale Vennelaa Kaaseti Vaagullona "/>
</div>
<div class="lyrico-lyrics-wrapper">Kurise Muvvole Thadipindi Jallu Vaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurise Muvvole Thadipindi Jallu Vaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola Puppodulu Sindese Gaalullona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola Puppodulu Sindese Gaalullona "/>
</div>
<div class="lyrico-lyrics-wrapper">Egise Alavole Thadimindi Nemali Eeka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egise Alavole Thadimindi Nemali Eeka "/>
</div>
<div class="lyrico-lyrics-wrapper">Merise Methaani Isaka Pariseenaa Odilonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise Methaani Isaka Pariseenaa Odilonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Persinaa Gulaka Raalla Gudilo Thangadi Veena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Persinaa Gulaka Raalla Gudilo Thangadi Veena "/>
</div>
<div class="lyrico-lyrics-wrapper">Sitti Sethula Pittaa Goolle Kattinaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitti Sethula Pittaa Goolle Kattinaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pikili Pittala Gootiki Rammani Pilisinara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pikili Pittala Gootiki Rammani Pilisinara "/>
</div>
<div class="lyrico-lyrics-wrapper">Elukaa Puli Mekaa Pilli Gadda Kodiyaata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elukaa Puli Mekaa Pilli Gadda Kodiyaata "/>
</div>
<div class="lyrico-lyrics-wrapper">Daagoodu Moothaa Danda Koru Sindata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daagoodu Moothaa Danda Koru Sindata "/>
</div>
<div class="lyrico-lyrics-wrapper">Kappaa Ganthulathone Thokkudu Billaata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappaa Ganthulathone Thokkudu Billaata "/>
</div>
<div class="lyrico-lyrics-wrapper">Sukkaala Sooru Daate Sirragone Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkaala Sooru Daate Sirragone Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Yeyra Sindheyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeyra Sindheyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeyra Botheyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeyra Botheyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanakdhana Dharuveyra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanakdhana Dharuveyra "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakjana Sitikeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakjana Sitikeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhana Dhana Dhann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Dhana Dhann"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanakdhana Dharuveyra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanakdhana Dharuveyra "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Jana Jana Jan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Jana Jan "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakjana Sitikeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakjana Sitikeyra"/>
</div>
</pre>
