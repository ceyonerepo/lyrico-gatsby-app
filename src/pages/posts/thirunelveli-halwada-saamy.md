---
title: "thirunelveli halwa da song lyrics"
album: "Saamy"
artist: "Harris Jayaraj "
lyricist: "Na. Muthukumar"
director: "Hari"
path: "/albums/saamy-song-lyrics"
song: "Thirunelveli Halwa Da"
image: ../../images/albumart/saamy.jpg
date: 2003-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rxrCTW_XzJ0"
type: "Intro"
singers:
  - Palakkad Sriram 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aarumugam Saamiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumugam Saamiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuthamaa Velu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuthamaa Velu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyanaaru Saamiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyanaaru Saamiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyilathaan Soolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyilathaan Soolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaimuga Saamiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaimuga Saamiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaiyarpatti Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaiyarpatti Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Paadum Aasamiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paadum Aasamiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Bottle Beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Bottle Beeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Udu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Udu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennadhu Ennadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadhu Ennadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirupathike Laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathike Laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutu Kadai Halwa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutu Kadai Halwa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idly Kadai Aaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idly Kadai Aaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutu Kattai Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutu Kattai Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Keta Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keta Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliya Kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Kaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nellaiyappar Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nellaiyappar Samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya Irupaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Irupaan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi Vandha Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi Vandha Samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaiya Theerkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiya Theerkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Gandhimadhi Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Gandhimadhi Samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediye Theerpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye Theerpaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Pettaiyile Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Pettaiyile Samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palayamkottaiyil Jail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palayamkottaiyil Jail"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Railu Koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Railu Koovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirupathike Laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathike Laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutu Kadai Halwa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutu Kadai Halwa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idly Kadai Aaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idly Kadai Aaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutu Kattai Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutu Kattai Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Keta Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keta Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi Muttainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Muttainu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi Muttainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Muttainu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindalu Panna Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindalu Panna Koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttaiku Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaiku Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaiya Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaiya Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Aayirum Gopalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Aayirum Gopalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudikiraanu Kudikiraanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikiraanu Kudikiraanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kevala Padutha Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kevala Padutha Koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
500 <div class="lyrico-lyrics-wrapper">Adichum Out Aagalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichum Out Aagalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tendulkar Thaan Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tendulkar Thaan Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanchipuram Pattu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchipuram Pattu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhani Malai Mottai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhani Malai Mottai Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondicheri Milly Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondicheri Milly Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Madurai Yila Malli Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Madurai Yila Malli Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyiluku Nenthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyiluku Nenthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vita Kaalai Indha Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vita Kaalai Indha Saamy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovamunnu Vandhu Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovamunnu Vandhu Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootathoda Kaali Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathoda Kaali Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbakonam Vethala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbakonam Vethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Konjam Kooda Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Konjam Kooda Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Kaala Sakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Kaala Sakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Cheku Pole Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Cheku Pole Suthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Daaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Daaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Daaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Daaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirupathike Laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathike Laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutu Kadai Halwa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutu Kadai Halwa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idly Kadai Aaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idly Kadai Aaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutu Kattai Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutu Kattai Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Keta Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keta Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halumu Kulama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halumu Kulama"/>
</div>
<div class="lyrico-lyrics-wrapper">Omayo Sile Sile Saamy Saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omayo Sile Sile Saamy Saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Halumu Kulama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halumu Kulama"/>
</div>
<div class="lyrico-lyrics-wrapper">Omayo Sile Sile Saamy Saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omayo Sile Sile Saamy Saamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu Silu Silu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silu Silu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silu Silu Silu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silu Silu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Memaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Memaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilu Jilu Jilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilu Jilu Jilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilu Jilu Jilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilu Jilu Jilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundari Vanthaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Vanthaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halumu Kulama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halumu Kulama"/>
</div>
<div class="lyrico-lyrics-wrapper">Omayo Sile Sile Saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omayo Sile Sile Saamy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Touri Talkies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Touri Talkies"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalu Mele Vaathiyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalu Mele Vaathiyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Padangal Paarthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padangal Paarthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Steringai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steringai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Pogum Rootai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Pogum Rootai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Valainchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Valainchavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootu Pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootu Pota"/>
</div>
<div class="lyrico-lyrics-wrapper">Lorry Settula Thookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Settula Thookam"/>
</div>
<div class="lyrico-lyrics-wrapper">Potu Vaazhndhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu Vaazhndhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lattiya Vachu Muttiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lattiya Vachu Muttiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pekkum Vithai Ellam Therinchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pekkum Vithai Ellam Therinchavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothukuzhi Venna Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothukuzhi Venna Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiruchenduril Vellam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiruchenduril Vellam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai La Enna Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai La Enna Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni Konjam Kooda Illa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni Konjam Kooda Illa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazham Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Vidu Solluradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vidu Solluradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamy Da Saamy Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamy Da Saamy Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallu Katta Aal Irundha Kaami Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallu Katta Aal Irundha Kaami Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoothukudi Uppu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi Uppu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oothikita Mappu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oothikita Mappu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dindukallu Pootu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dindukallu Pootu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Solluradhu Rightu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Solluradhu Rightu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tirunelveli Halwa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirunelveli Halwa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Trichy Mala Kotta Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trichy Mala Kotta Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirupathike Laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirupathike Laddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutu Kadai Halwa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutu Kadai Halwa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idly Kadai Aaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idly Kadai Aaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutu Kattai Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutu Kattai Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Keta Saamy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keta Saamy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliya Kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Kaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nellaiyappar Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nellaiyappar Samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya Irupaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Irupaan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi Vandha Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi Vandha Samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaiya Theerkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiya Theerkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Gandhimadhi Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Gandhimadhi Samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediye Theerpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye Theerpaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Pettaiyile Samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Pettaiyile Samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palayamkottaiyil Jail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palayamkottaiyil Jail"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Railu Koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Railu Koovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovum Koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovum Koovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovum Koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovum Koovum"/>
</div>
</pre>
