---
title: "o kalala kathala song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "O Kalala Kathala"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SNjRbqvzozI"
type: "sad"
singers:
  - Sathya Prakash
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O Kalala Kathalaa Kalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Kalala Kathalaa Kalise"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorale Theeralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorale Theeralai"/>
</div>
<div class="lyrico-lyrics-wrapper">O Jathaga Jagamai Kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Jathaga Jagamai Kadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaale Praanaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaale Praanaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Vidhiye Vidhigaa Kalipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Vidhiye Vidhigaa Kalipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohinchani Malupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchani Malupai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Dhishale Okatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Dhishale Okatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niliche Tholi Vekuvalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niliche Tholi Vekuvalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kshaname Manake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kshaname Manake"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorike Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorike Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamdhai Kadavaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamdhai Kadavaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manatho Nadiche Ee Dhaarilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manatho Nadiche Ee Dhaarilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalane Okkatiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalane Okkatiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Rekkulane Okkatiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Rekkulane Okkatiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Dhikkulane Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Dhikkulane Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaati Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaati Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Korukune Kotthajagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Korukune Kotthajagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherukune O Swetcha Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherukune O Swetcha Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadichina Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadichina Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam Yedho Chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam Yedho Chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Manassu Pai Mandhe Poose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassu Pai Mandhe Poose"/>
</div>
<div class="lyrico-lyrics-wrapper">Manthramunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthramunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirantharam Needalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirantharam Needalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Untunnadhi Thaanega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untunnadhi Thaanega"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushassulo Oopiri Panche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushassulo Oopiri Panche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali Paatalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Paatalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Chinukedho Thaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Chinukedho Thaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiguresthunte Chaitram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguresthunte Chaitram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadi Kannullo Virise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Kannullo Virise"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Navve Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Navve Nee Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidipolevu Gandhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidipolevu Gandhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Poola Kundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Poola Kundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Avi Kanaraani Bandhaalule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avi Kanaraani Bandhaalule"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaarilona Dhaarilona Dhaarilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarilona Dhaarilona Dhaarilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Rekkulane Okkatiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Rekkulane Okkatiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Dhikkulane Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Dhikkulane Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaati Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaati Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Korukune Kotthajagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Korukune Kotthajagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherukune O Swetcha Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherukune O Swetcha Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhidhitthara Dhitthaithathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhidhitthara Dhitthaithathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhidhitthara Dhitthaithakathom Thakathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhidhitthara Dhitthaithakathom Thakathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaraseema Kanduvarave Ye Ye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaraseema Kanduvarave Ye Ye "/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaraseema Kanduvarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaraseema Kanduvarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Madhura Nirana Madhunithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Madhura Nirana Madhunithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambuja Netra Chandra Vadhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuja Netra Chandra Vadhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Morkaadhinii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Morkaadhinii"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku Ledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Ledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Inko Janmalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Inko Janmalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaarilo Poolai Poose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaarilo Poolai Poose"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana Jallulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Jallulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanthamayye Pravaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanthamayye Pravaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Varnaalatho Saavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varnaalatho Saavaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pratheeksham Pacchaga Navve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pratheeksham Pacchaga Navve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Lecheti Paadhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Lecheti Paadhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraaduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraaduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipisthundhi Ee Kaalamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipisthundhi Ee Kaalamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Rekkulane Okkatiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Rekkulane Okkatiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Dhikkulane Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Dhikkulane Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaati Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaati Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Korukune Kotthajagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Korukune Kotthajagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherukune O Swetcha Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherukune O Swetcha Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">O Kalala Kathalaa Kalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Kalala Kathalaa Kalise"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorale Theeralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorale Theeralai"/>
</div>
<div class="lyrico-lyrics-wrapper">O Jathaga Jagamai Kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Jathaga Jagamai Kadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaale Praanaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaale Praanaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Vidhiye Vidhigaa Kalipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Vidhiye Vidhigaa Kalipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohinchani Malupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchani Malupai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Dhishale Okatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Dhishale Okatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niliche Tholi Vekuvalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niliche Tholi Vekuvalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kshaname Manake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kshaname Manake"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorike Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorike Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamdhai Kadavaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamdhai Kadavaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manatho Nadiche Ee Dhaarilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manatho Nadiche Ee Dhaarilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Rekkulane Okkatiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Rekkulane Okkatiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Dhikkulane Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Dhikkulane Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaati Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaati Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Korukune Kotthajagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Korukune Kotthajagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherukune O Swetcha Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherukune O Swetcha Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Payanam"/>
</div>
</pre>
