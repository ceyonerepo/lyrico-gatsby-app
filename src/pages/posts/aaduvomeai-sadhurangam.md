---
title: "aaduvomeai song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Bharathiyar"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Aaduvomeai"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j7IlB7lcG3w"
type: "mass"
singers:
  - Manikka Vinayagam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaduvomae pallupaaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae pallupaaduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha suthanthiram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha suthanthiram "/>
</div>
<div class="lyrico-lyrics-wrapper">adainthu vittomendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adainthu vittomendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae pallupaaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae pallupaaduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha suthanthiram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha suthanthiram "/>
</div>
<div class="lyrico-lyrics-wrapper">adainthu vittomendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adainthu vittomendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae pallupaaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae pallupaaduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha suthanthiram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha suthanthiram "/>
</div>
<div class="lyrico-lyrics-wrapper">adainthu vittomendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adainthu vittomendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum suthanthiram enbathae paechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum suthanthiram enbathae paechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum suthanthiram enbathae paechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum suthanthiram enbathae paechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam ellorum samam enbadhu urudhiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ellorum samam enbadhu urudhiyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum suthanthiram enbathae paechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum suthanthiram enbathae paechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam ellorum samam enbadhu urudhiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ellorum samam enbadhu urudhiyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu kondae vetri oodhuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu kondae vetri oodhuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu kondae vetri oodhuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu kondae vetri oodhuvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangu kondae vetri oodhuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu kondae vetri oodhuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai tharanikkellaam eduththothuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai tharanikkellaam eduththothuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu kondae vetri oodhuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu kondae vetri oodhuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai tharanikkellaam eduththothuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai tharanikkellaam eduththothuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhavukkum thozhilukkum vandhanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhavukkum thozhilukkum vandhanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhavukkum thozhilukkum vandhanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhavukkum thozhilukkum vandhanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaenil undu kalithirupporai nindhanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaenil undu kalithirupporai nindhanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhavukkum thozhilukkum vandhanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhavukkum thozhilukkum vandhanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaenil undu kalithirupporai nindhanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaenil undu kalithirupporai nindhanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhalukku neer paaichi maaya maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhalukku neer paaichi maaya maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhalukku neer paaichi maaya maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhalukku neer paaichi maaya maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum veenarukku uzhaithudalum ooya maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum veenarukku uzhaithudalum ooya maattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamirukkum naadu namadhenbadharindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamirukkum naadu namadhenbadharindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamirukkum naadu namadhenbadharindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamirukkum naadu namadhenbadharindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu namakkae urimaiyaam enbadhai arindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu namakkae urimaiyaam enbadhai arindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamirukkum naadu namadhenbadharindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamirukkum naadu namadhenbadharindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu namakkae urimaiyaam enbadhai arindhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu namakkae urimaiyaam enbadhai arindhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil evarkkum ini adimai seiyom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil evarkkum ini adimai seiyom"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil evarkkum ini adimai seiyom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil evarkkum ini adimai seiyom"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil evarkkum ini adimai seiyom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil evarkkum ini adimai seiyom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pari poorananukkae adimai seidhu vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pari poorananukkae adimai seidhu vaazhvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvomae pallupaaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae pallupaaduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha suthanthiram adainthu vittomendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha suthanthiram adainthu vittomendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvom aaduvom aaduvom aaduvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom aaduvom aaduvom aaduvomae"/>
</div>
</pre>
