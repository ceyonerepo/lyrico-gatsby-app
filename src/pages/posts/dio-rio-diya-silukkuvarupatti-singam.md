---
title: "dio rio diya song lyrics"
album: "Silukkuvarupatti Singam"
artist: "Leon James"
lyricist: "Madhan Karky"
director: "Chella Ayyavu"
path: "/albums/silukkuvarupatti-singam-lyrics"
song: "Dio Rio Diya"
image: ../../images/albumart/silukkuvarupatti-singam.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/59iJYqeLjVo"
type: "happy"
singers:
  - Naresh Iyer
  - Sunidhi Chauhan
  - Santhosh Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaaaaaaaaaaaaaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaaaaaaaaaaaaaa haaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sambaa sambaa kaaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sambaa sambaa kaaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaa vambaa neththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa vambaa neththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenjeela thookki pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenjeela thookki pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kammaa kammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kammaa kammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela summaa ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela summaa ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam yenjeela aagi pochuaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam yenjeela aagi pochuaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sambaa sambaa kaaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sambaa sambaa kaaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaa vambaa neththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa vambaa neththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenjeela thookki pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenjeela thookki pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kammaa kammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kammaa kammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela summaa ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela summaa ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam yenjeela aagi pochuaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam yenjeela aagi pochuaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poruppaa kooththu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruppaa kooththu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthii vandhen aiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthii vandhen aiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil verththukotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil verththukotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppaa ninnen aiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppaa ninnen aiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usain bolt-u ottaththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usain bolt-u ottaththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga dhaan aataththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga dhaan aataththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosa chedi thotaththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosa chedi thotaththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga dhaan kootathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga dhaan kootathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodae podaa nottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodae podaa nottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella maiyaala ezhudhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella maiyaala ezhudhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya assai kaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya assai kaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna moochaala uchchari daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna moochaala uchchari daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithikkura kaara molagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithikkura kaara molagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ey paththikura eera viragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ey paththikura eera viragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ey ey rekka rendu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ey ey rekka rendu "/>
</div>
<div class="lyrico-lyrics-wrapper">vechcha padagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechcha padagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ey ey makka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ey ey makka "/>
</div>
<div class="lyrico-lyrics-wrapper">inga vandhaa kanagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga vandhaa kanagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchi kootamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi kootamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Quater biriyani kuduthadhandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quater biriyani kuduthadhandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Patchi ivala paakka thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patchi ivala paakka thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndha koottam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndha koottam paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan sambaa sambaa kaaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan sambaa sambaa kaaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaa vambaa neththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa vambaa neththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenjeela thookki pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenjeela thookki pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kammaa kammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kammaa kammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela summaa ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela summaa ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam yenjeela aagi pochuaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam yenjeela aagi pochuaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poruppaa kooththu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruppaa kooththu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthii vandhen aiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthii vandhen aiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil verththukotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil verththukotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppaa ninnen aiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppaa ninnen aiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa usain bolt-u ottaththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa usain bolt-u ottaththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga dhaan aataththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga dhaan aataththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosa chedi thotaththukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosa chedi thotaththukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga dhaan kootathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga dhaan kootathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy ennoda ne aada ready ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ennoda ne aada ready ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy dio rio diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy dio rio diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy illaiyina seeti adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy illaiyina seeti adiyaa"/>
</div>
</pre>
