---
title: "sengaandhale song lyrics"
album: "Aranmanai 3"
artist: "C. Sathya"
lyricist: "Pa. Vijay"
director: "Sundar C"
path: "/albums/aranmanai-3-lyrics"
song: "Sengaandhale"
image: ../../images/albumart/aranmanai-3.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hOsvc67wiOI"
type: "happy"
singers:
  - Reema
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sengaandhale Unai Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhale Unai Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Yendhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Yendhava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaithen Unnai Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaithen Unnai Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppen Endrum Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppen Endrum Unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Un Kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Un Kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Theenduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Theenduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mithandhen Kaatril Katraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithandhen Kaatril Katraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthen Iravil Nizhalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthen Iravil Nizhalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Un Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Un Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kaanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kaanuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaararo Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaararo Aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaararo Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaararo Aarariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinna Malar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Malar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyalai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyalai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Malarndhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Malarndhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Enna Uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Enna Uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkka Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirkka Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvil Asaindhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvil Asaindhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Perum Annai Vaazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Perum Annai Vaazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyellam Oor Varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellam Oor Varame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Marbile Koodu Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Marbile Koodu Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Urangidum Naal Varume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Urangidum Naal Varume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal Kasiyum Idhazhodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Kasiyum Idhazhodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kaanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam Kooda Osai Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam Kooda Osai Indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Pogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Pogume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengaandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Thendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Thendrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Yendhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Yendhava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Erigaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Erigaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pol Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pol Vandhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Dhisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Dhisaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttukul Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttukul Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham Thandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham Thandhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Ondru Podhathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Ondru Podhathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu Jenmam Naan Sumappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Jenmam Naan Sumappen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhvile Ore Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhvile Ore Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Vaithu Paarthu Kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vaithu Paarthu Kolven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmel Thoosum Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel Thoosum Theendamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapen Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapen Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Muzhudhum Unakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Muzhudhum Unakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhven Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhven Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengaandhale Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhale Aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Thendrale Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Thendrale Aarariro"/>
</div>
</pre>
