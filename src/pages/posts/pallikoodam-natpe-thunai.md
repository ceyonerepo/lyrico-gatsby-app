---
title: "pallikoodam song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Pallikoodam"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s-c28K3qYvk"
type: "friendship"
singers:
  - Sanjith Hegde
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pallikoodathula Paadam Paduchathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodathula Paadam Paduchathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Natpu Paduchom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Natpu Paduchom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Vayasula Naanga Aluthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Vayasula Naanga Aluthathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Senthu Siruchom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Senthu Siruchom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasukkoru Panjam Vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukkoru Panjam Vanthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasathukku Panjamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathukku Panjamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Sanda Pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Sanda Pottaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Vanjam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Vanjam illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanbana Pol Yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanbana Pol Yarum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Bhoomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bhoomiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpukkuthaan Eedey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpukkuthaan Eedey illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Bhoomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bhoomiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi illa Betham illa Natpukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi illa Betham illa Natpukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Irukkura Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Irukkura Varaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Kavalaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Kavalaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanba Vaada Nanba Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba Vaada Nanba Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kalloori Naatkalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kalloori Naatkalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga College Pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga College Pogaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patti Thottiyellaam Atti Pottuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thottiyellaam Atti Pottuduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tea Kadai Pothavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea Kadai Pothavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">City kulla Settum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City kulla Settum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engala Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpukkulla Prechchanathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukkulla Prechchanathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathe illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathe illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanbana Pol Yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanbana Pol Yarum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Bhoomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bhoomiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpukkuthaan Eedey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpukkuthaan Eedey illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Bhoomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bhoomiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi illa Betham illa Natpukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi illa Betham illa Natpukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Irukkura Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Irukkura Varaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Kavalaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Kavalaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Mudunchu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Mudunchu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Niyabagam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Niyabagam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Macha Nu Ini Yaru Kupuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Macha Nu Ini Yaru Kupuduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanba Thirumbi Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba Thirumbi Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
</pre>
