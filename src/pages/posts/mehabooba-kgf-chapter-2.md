---
title: "mehabooba song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Madhurakavi"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Mehabooba"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ty2Cy4S04dU"
type: "melody"
singers:
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Va va en anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va en anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvin peranbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvin peranbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthai kan munbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthai kan munbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu nijama sol anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu nijama sol anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kangalum kadhal pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalum kadhal pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">En tharunam malarum vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En tharunam malarum vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un tholgalil saayum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholgalil saayum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thulirum perazhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thulirum perazhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehabooba mae teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba mae teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba mae teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba mae teri mehabooba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo vaika poongatru seer seithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vaika poongatru seer seithathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vaanam poo thoovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vaanam poo thoovuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjal mozhi pesidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjal mozhi pesidum "/>
</div>
<div class="lyrico-lyrics-wrapper">oomai kili naanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oomai kili naanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai vali theerkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai vali theerkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">marundhaalan ne thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marundhaalan ne thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin ver neengidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin ver neengidum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam ithu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam ithu thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin neer vaarkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin neer vaarkum "/>
</div>
<div class="lyrico-lyrics-wrapper">mugilalan ne thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugilalan ne thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaigal theendum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigal theendum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaninthen thaninthen salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaninthen thaninthen salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini vaazhkaiyil yethu maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vaazhkaiyil yethu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eduthen puthu jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eduthen puthu jananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehabooba mae teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba mae teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba mae teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba mae teri mehabooba"/>
</div>
</pre>
