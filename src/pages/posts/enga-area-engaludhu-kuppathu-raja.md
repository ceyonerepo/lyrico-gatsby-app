---
title: "enga area engaludhu song lyrics"
album: "Kuppathu Raja"
artist: "G. V. Prakash Kumar"
lyricist: "Logan"
director: "Baba Bhaskar"
path: "/albums/kuppathu-raja-lyrics"
song: "Enga Area Engaludhu"
image: ../../images/albumart/kuppathu-raja.jpg
date: 2019-04-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JPAQXB7_Dxc"
type: "happy"
singers:
  - Santhosh Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alattiaa Neratthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alattiaa Neratthula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vanthu Adichitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vanthu Adichitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaku Oru Neram Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Oru Neram Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appo Unnai Senjiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Unnai Senjiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oola Utthar Aaluyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oola Utthar Aaluyinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Neeyum Nenacchita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Neeyum Nenacchita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakka Murakka Noola Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakka Murakka Noola Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Valicchitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Valicchitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athara Pathara Vidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athara Pathara Vidura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakki Madakki Sethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Madakki Sethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhula Pondhula Veetula Rootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhula Pondhula Veetula Rootula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Local Rappu Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Local Rappu Songu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angayum Engayum Engeyum Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angayum Engayum Engeyum Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Local Rappu Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Local Rappu Songu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichittu Ponavan Nattukkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichittu Ponavan Nattukkinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Local Rappu Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Local Rappu Songu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Irundhavan Pichukinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Irundhavan Pichukinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongura Singatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongura Singatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatti Thaan Eluppaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Thaan Eluppaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangalaam Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalaam Maasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaara Nee Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Nee Irundhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanaa Nee Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanaa Nee Irundhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Dhaan Bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Dhaan Bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennaikku Naanga Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaikku Naanga Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engala Asachukka Aaley Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Asachukka Aaley Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Mudinjaaka Modhi Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Mudinjaaka Modhi Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangalaam Ennaikkum Thaaru Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalaam Ennaikkum Thaaru Maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangura Vaangula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangura Vaangula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangura Thaangula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangura Thaangula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Vittaa Ellaam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Vittaa Ellaam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Ungaludhu Ungaludhu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Ungaludhu Ungaludhu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engaludhu Engaludhu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaludhu Engaludhu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaga Mottham Total Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Mottham Total Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Engaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Engaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Ungaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Ungaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Engaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Engaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Ungaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Ungaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Engaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Engaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Ungaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Ungaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Engaludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Engaludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Ungaludhuu Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Ungaludhuu Yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathura Saathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathura Saathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora Vittu Ooduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Ooduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatura Kaatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatura Kaatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavukuthu Aaduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavukuthu Aaduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookura Thookkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookura Thookkula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuni Mani Theduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni Mani Theduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkura Thaakkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkura Thaakkula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Paata Paaduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Paata Paaduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagu Jigu Jigunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagu Jigu Jigunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkidhu Pakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkidhu Pakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholukkulla Kuthuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholukkulla Kuthuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonthuduva Pattunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonthuduva Pattunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyi Kaalu Otharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyi Kaalu Otharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevul Rendum Setharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevul Rendum Setharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithaa Maatula Onnu Thattunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithaa Maatula Onnu Thattunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sowkaarpettai Sethurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sowkaarpettai Sethurum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangalaam Nekku Maatuna Ooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalaam Nekku Maatuna Ooku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammalaam Yaarunu Solli Nee Kaatuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammalaam Yaarunu Solli Nee Kaatuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seenula Thotthu Thottinaa Saaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenula Thotthu Thottinaa Saaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappaana Vaartaiyil Peraludhu Naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaana Vaartaiyil Peraludhu Naakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engarundhu Enga Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engarundhu Enga Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Araajagam Pannuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araajagam Pannuringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Edam Thandhadhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Edam Thandhadhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam Neenga Pesureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Neenga Pesureenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam Neenga Pesureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Neenga Pesureenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam Neenga Pesureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Neenga Pesureenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Neenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Paav Pajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Paav Pajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paani Poori Engaludhuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paani Poori Engaludhuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Tea Bajji Biscuit Engaludhuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Tea Bajji Biscuit Engaludhuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaana Kaaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaana Kaaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaana Kaaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kaaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Kaasu Panam Vandhaakooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Kaasu Panam Vandhaakooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Ellam Maara Maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Ellam Maara Maatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Irundhey Kuli Parichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Irundhey Kuli Parichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala Thaaney Vaara Maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Thaaney Vaara Maatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Gettapoda Vandhavenlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Gettapoda Vandhavenlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattavathaan Pottukinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattavathaan Pottukinaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Edam Naanga Thandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Edam Naanga Thandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhamaathaan Aakikinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamaathaan Aakikinaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhainga Naangalaam Paavam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhainga Naangalaam Paavam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinja Thuniyoda Velaikku Povomdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinja Thuniyoda Velaikku Povomdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayodum Kosuvodum Thoonguvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayodum Kosuvodum Thoonguvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Oru Velai Sothukku Yenguvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Oru Velai Sothukku Yenguvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olai Veetula Vaalndhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olai Veetula Vaalndhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangallaam Ennaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangallaam Ennaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppatthu Raaja Dhaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppatthu Raaja Dhaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Vaada Machaan Kedaa Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Vaada Machaan Kedaa Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Opponentu Oramkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opponentu Oramkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocket Thaanda Vaaya Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket Thaanda Vaaya Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaindukkundaa Jaava Settuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaindukkundaa Jaava Settuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Vidu Aiyayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Vidu Aiyayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keela Vidu Ithaan Thaag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela Vidu Ithaan Thaag"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perala Vidu Aiyayooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perala Vidu Aiyayooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perala Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perala Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei All Areavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei All Areavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocket Maama Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket Maama Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Hamaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Hamaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Ours
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Ours"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Yours
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Yours"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Area Yours
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Area Yours"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Area Ours
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Area Ours"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hamaraa Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamaraa Hamaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hamaraa Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamaraa Hamaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumaara Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumaara Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumaara Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumaara Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumaara Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumaara Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumaara Thumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumaara Thumaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hamaraa Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamaraa Hamaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hamaraa Hamaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamaraa Hamaraa"/>
</div>
</pre>
