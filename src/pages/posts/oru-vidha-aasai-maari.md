---
title: 'oru vidha aasai varugiradha song lyrics'
album: 'Maari'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari-song-lyrics'
song: 'Oru Vidha Aasai'
image: ../../images/albumart/Maari.jpg
date: 2015-07-17
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/3t1z1ld43N0'
type: 'happy'
singers: 
- Vineeth Srinivasan
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Oru vidha aasai varugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vidha aasai varugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vidha bothai tharugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthu vidha bothai tharugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae duet vargiradhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavilae duet vargiradhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Duetil foreign varugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Duetil foreign varugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai kuthu pattukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvarai kuthu pattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthura unakku melody pidikkiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuthura unakku melody pidikkiradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vidha aasai varugiradhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vidha aasai varugiradhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vidha bothai tharugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthu vidha bothai tharugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa…aaa…aa….aaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…aa….aaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rabbaa rab bab baa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rabbaa rab bab baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap bap paa rab bab paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rap bap paa rab bab paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rabbaa rab bab baa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rabbaa rab bab baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap bap paa rab bab paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rap bap paa rab bab paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Size-ah paapadhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Size-ah paapadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nice-ah idipadhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nice-ah idipadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivvunnu irukiradhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jivvunnu irukiradhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Slighta moraipathum
<input type="checkbox" class="lyrico-select-lyric-line" value="Slighta moraipathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Brighta siripadhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Brighta siripadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Lighta inikuradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Lighta inikuradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mutton vettuviyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutton vettuviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku ippo
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maligai rusikkiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Maligai rusikkiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesa murukuviyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesa murukuviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal vaasam manakiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Koondhal vaasam manakiradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pudikaama nadikaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudikaama nadikaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadichaalum nadakaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadichaalum nadakaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu unna chase pani
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu unna chase pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikaama mudikaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adikaama mudikaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rabbaa rab bab baa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rabbaa rab bab baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap bap paa rab bab paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rap bap paa rab bab paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rabbaa rab bab baa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rabbaa rab bab baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap bap paa rab bab paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Rap bap paa rab bab paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vidha aasai varugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vidha aasai varugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vidha bothai tharugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthu vidha bothai tharugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae duet vargiradhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavilae duet vargiradhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Duetil foreign varugiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Duetil foreign varugiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai kuthu pattukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvarai kuthu pattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthura unakku melody pidikkiradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuthura unakku melody pidikkiradha"/>
</div>
</pre>