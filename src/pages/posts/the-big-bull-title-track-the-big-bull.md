---
title: "the big bull - title track song lyrics"
album: "The Big Bull"
artist: "Wily Frenzy"
lyricist: "Ajey Nagar (CARRYMINATI)"
director: "Kookie Gulati"
path: "/albums/the-big-bull-lyrics"
song: "The Big Bull - Title Track"
image: ../../images/albumart/the-big-bull.jpg
date: 2021-04-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/vQq_zaGcspM"
type: "Title Song"
singers:
  - Ajey Nagar (CARRYMINATI)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">To kaise hai aap log?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To kaise hai aap log?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalne walo ki to rooh bhi jalani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalne walo ki to rooh bhi jalani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inki bhookh bhi to maine hi mitani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inki bhookh bhi to maine hi mitani hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inko kya pata maine kari kitni mehnat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inko kya pata maine kari kitni mehnat"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari baaton se tha main poora sehmat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari baaton se tha main poora sehmat"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari zindagi inhone mujhko rulaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari zindagi inhone mujhko rulaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inko bhi to mila tha jo maine kamaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inko bhi to mila tha jo maine kamaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rote rote bhi inka dhanda maine chalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rote rote bhi inka dhanda maine chalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir bhi inhone hai saara dhanda mera khaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir bhi inhone hai saara dhanda mera khaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh saari inki maya inka hi kala saya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh saari inki maya inka hi kala saya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inke mayazaal se main bhi to bach na paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inke mayazaal se main bhi to bach na paya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inhein lagta hai main ek fakeer hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inhein lagta hai main ek fakeer hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar yeh hath hain to main inki lakeer hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar yeh hath hain to main inki lakeer hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin hathon ne hai mujhko dabaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin hathon ne hai mujhko dabaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn hathon ki to dekh beta main zanjeer hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn hathon ki to dekh beta main zanjeer hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ameeron wala khwaab chaato inki dhool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ameeron wala khwaab chaato inki dhool"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikhne mein sundar yeh kaante wale phool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikhne mein sundar yeh kaante wale phool"/>
</div>
<div class="lyrico-lyrics-wrapper">Phool se bhara dekh mera pool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phool se bhara dekh mera pool"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum hoge yahan ke principle par main hun poora school
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum hoge yahan ke principle par main hun poora school"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalne walo ki to rooh bhi jalani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalne walo ki to rooh bhi jalani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inki bhookh bhi to maine hi mitani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inki bhookh bhi to maine hi mitani hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaan'na chahoge kya tum paison ki jaat?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan'na chahoge kya tum paison ki jaat?"/>
</div>
<div class="lyrico-lyrics-wrapper">Samjho baap hai ya samjho ise ashirwad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samjho baap hai ya samjho ise ashirwad"/>
</div>
<div class="lyrico-lyrics-wrapper">Banata todta yeh zindagiyan raaton raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banata todta yeh zindagiyan raaton raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharni jeb ya banani poori zaaydad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharni jeb ya banani poori zaaydad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">System hai badalna ya phir karna us'se blame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="System hai badalna ya phir karna us'se blame"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakadna chahte mujhe ya phir game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakadna chahte mujhe ya phir game"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hun woh bull jo kabhi na hoga tame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hun woh bull jo kabhi na hoga tame"/>
</div>
<div class="lyrico-lyrics-wrapper">Hath hai hilte inke jab yeh karte mujhpe aim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hath hai hilte inke jab yeh karte mujhpe aim"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gareeb hai chup kyunki ameeron ki easy bail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gareeb hai chup kyunki ameeron ki easy bail"/>
</div>
<div class="lyrico-lyrics-wrapper">Ameer hai khush kyunki gareebon ki lagi sale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ameer hai khush kyunki gareebon ki lagi sale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghalti hai kiski? Aur badle mein kaun jaata jail?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghalti hai kiski? Aur badle mein kaun jaata jail?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban'na hai cheetah ya rehna tumhe bas ek snail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban'na hai cheetah ya rehna tumhe bas ek snail"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazboor hun kabhi na hoti plate full
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazboor hun kabhi na hoti plate full"/>
</div>
<div class="lyrico-lyrics-wrapper">Harqatein maanega samaaj meri shamefull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harqatein maanega samaaj meri shamefull"/>
</div>
<div class="lyrico-lyrics-wrapper">Akela hi karunga main saara weight pull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akela hi karunga main saara weight pull"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaad rakhna I'm the one and only big bull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaad rakhna I'm the one and only big bull"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let's go!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's go!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalne walo ki to rooh bhi jalani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalne walo ki to rooh bhi jalani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek kahaani hai jo sabko sunani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kahaani hai jo sabko sunani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inki bhookh bhi to maine hi mitani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inki bhookh bhi to maine hi mitani hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The one and only big bull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The one and only big bull"/>
</div>
<div class="lyrico-lyrics-wrapper">The one and only big bull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The one and only big bull"/>
</div>
</pre>
