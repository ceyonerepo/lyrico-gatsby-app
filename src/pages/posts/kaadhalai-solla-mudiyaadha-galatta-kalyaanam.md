---
title: "kaadhalai solla mudiyaadha song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Kabilan"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Kaadhalai Solla Mudiyaadha"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pk98_qag-Sw"
type: "happy"
singers:
  - Yazin Nizar
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal poo en kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal poo en kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhum un aaladho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum un aaladho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai kadanaga tharuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai kadanaga tharuvaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai kadhal thaangatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai kadhal thaangatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vizhiyavathu thoongatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vizhiyavathu thoongatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhi irunthum vazhi irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhi irunthum vazhi irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhalai solla mudiyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhalai solla mudiyadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vizhi inbam aanathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vizhi inbam aanathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vizhi vanmam aanathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vizhi vanmam aanathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaram vizhinthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaram vizhinthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandukkum naduve thavithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandukkum naduve thavithene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasam adhu vaasam veesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam adhu vaasam veesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam adhu vaasam veesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam adhu vaasam veesuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kangal kannaadi aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangal kannaadi aanal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin munne ennai kaana koodatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin munne ennai kaana koodatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam thedi naan poga maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam thedi naan poga maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettodu vennila neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettodu vennila neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil thogaiyo en kai regaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogaiyo en kai regaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sehrum varai sernthuruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sehrum varai sernthuruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival oru kaadhal azhaga nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival oru kaadhal azhaga nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru vizhi thaayin mozhiya nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru vizhi thaayin mozhiya nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyil naan dhinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyil naan dhinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pullin meedhu paniyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pullin meedhu paniyaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaruyir unnai maravene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaruyir unnai maravene"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaridam ennai tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam ennai tharuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye sol neeye sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye sol neeye sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru minmini illa iravanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru minmini illa iravanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasam adhu vaasam veesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam adhu vaasam veesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam adhu vaasam veesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam adhu vaasam veesuthadi"/>
</div>
</pre>
