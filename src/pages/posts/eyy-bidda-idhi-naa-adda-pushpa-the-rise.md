---
title: "eyy bidda idhi naa adda song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/pushpa-the-rise-lyrics"
song: "Eyy Bidda Idhi Naa Adda"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/pHHig1XBML0"
type: "happy"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa pakka nadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa pakka nadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee pakka nadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee pakka nadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalapaina aakasham mukka nadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalapaina aakasham mukka nadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa thappu nene ee oppu nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa thappu nene ee oppu nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappoppulu thagalette nippu nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappoppulu thagalette nippu nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaithe kottetodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaithe kottetodu "/>
</div>
<div class="lyrico-lyrics-wrapper">bummidhe puttaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bummidhe puttaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttada adhi malla nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttada adhi malla nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu minchi yedhigetodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu minchi yedhigetodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkodu unnadu soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkodu unnadu soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadante adhi repati nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadante adhi repati nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne thippana meesamata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne thippana meesamata"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethilona goddalata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethilona goddalata"/>
</div>
<div class="lyrico-lyrics-wrapper">Sesindhe yuddhamata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sesindhe yuddhamata "/>
</div>
<div class="lyrico-lyrics-wrapper">seyyandhe sandhi ata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyyandhe sandhi ata"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Eyy bidda idhi naa adda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu yetlo visirestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu yetlo visirestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne sepatho tirigostha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne sepatho tirigostha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gada karraku guchhestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada karraku guchhestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne jandala egirestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne jandala egirestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu mattilo paathesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu mattilo paathesi "/>
</div>
<div class="lyrico-lyrics-wrapper">mayam chestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayam chestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne karidhaina kanijam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne karidhaina kanijam "/>
</div>
<div class="lyrico-lyrics-wrapper">la malli dorikestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la malli dorikestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Eyy bidda idhi naa adda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evadra evadra nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadra evadra nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inumunu inumunu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inumunu inumunu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu kalchithe katthavuthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu kalchithe katthavuthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadra evadra nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadra evadra nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattini mattini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattini mattini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu thokkithe itukavuthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu thokkithe itukavuthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadra evadra nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadra evadra nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayini rayini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayini rayini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gayam kani chesarante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gayam kani chesarante"/>
</div>
<div class="lyrico-lyrics-wrapper">Khayanga devudni avuthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khayanga devudni avuthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene thaggedhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene thaggedhele"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Eyy bidda idhi naa adda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Eyy bidda idhi naa adda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene thaggedhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene thaggedhele"/>
</div>
</pre>
