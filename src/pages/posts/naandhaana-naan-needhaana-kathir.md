---
title: "naandhaana naan needhaana song lyrics"
album: "Kathir"
artist: "Prashant Pillai"
lyricist: "Uma Devi"
director: "Dhinesh Palanivel"
path: "/albums/kathir-lyrics"
song: "Naandhaana Naan Needhaana"
image: ../../images/albumart/kathir.jpg
date: 2022-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zuq-Hs1hw0M"
type: "love"
singers:
  - Gowtham Bharadwaj
  - Keerthana Vaidyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naandhaana Naan Needhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhaana Naan Needhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharangal Megamaagudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharangal Megamaagudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vaazha Poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vaazha Poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayathin Needhi Maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathin Needhi Maane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhiye Nee Vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiye Nee Vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Nee Thaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Nee Thaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Unnale Niramaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Unnale Niramaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirame Nee Vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirame Nee Vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamengum Poovaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum Poovaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannale Thannale Uravagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannale Thannale Uravagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naandhaana Naan Needhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhaana Naan Needhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharangal Megamaagudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharangal Megamaagudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vaazha Poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vaazha Poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayathin Needhi Maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathin Needhi Maane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirkaalangal Theevaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirkaalangal Theevaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Thedum Dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thedum Dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaarthai En Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaarthai En Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thozhaa Needhan Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thozhaa Needhan Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Meedhu Vilakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Meedhu Vilakke"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaakkum Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaakkum Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee Piranthaayo En Aaviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee Piranthaayo En Aaviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave Nee Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Nee Vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal Vendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal Vendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Saayum Un Tholgal Needhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Saayum Un Tholgal Needhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naandhaana Naan Needhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhaana Naan Needhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharangal Megamaagudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharangal Megamaagudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vaazha Poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vaazha Poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayathin Needhi Maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathin Needhi Maane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhiye Nee Vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiye Nee Vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Nee Thaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Nee Thaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Unnale Niramaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Unnale Niramaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirame Nee Vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirame Nee Vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamengum Poovaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum Poovaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannale Thannale Uravagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannale Thannale Uravagiraai"/>
</div>
</pre>
