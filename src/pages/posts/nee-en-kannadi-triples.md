---
title: "nee en kannadi song lyrics"
album: "Triples"
artist: "Vishal Chandrashekhar"
lyricist: "Charukesh Sekar"
director: "Charukesh Sekar"
path: "/albums/triples-lyrics"
song: "Nee En Kannadi"
image: ../../images/albumart/triples.jpg
date: 2020-12-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9HqEU-kBpmo"
type: "love"
singers:
  - Govind Prasad
  - Sinduri Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee en kannadi thaandatha pinbam yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee en kannadi thaandatha pinbam yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan kannodu sergindra neram idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan kannodu sergindra neram idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ponnaana ennagal unnale vannangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ponnaana ennagal unnale vannangal"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaanil maara kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaanil maara kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan un kai patrum thooramthil paavai yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un kai patrum thooramthil paavai yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan nenjodu ondragum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan nenjodu ondragum kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru nirkkathu paadhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru nirkkathu paadhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">selgindra thoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selgindra thoorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvai neela kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvai neela kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale vikkalgal nooraayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale vikkalgal nooraayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal nooraayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal nooraayiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale yeakkangal nooraayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale yeakkangal nooraayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha kai sehra yen thamatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha kai sehra yen thamatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaga thaane naan uyir vaazhgirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga thaane naan uyir vaazhgirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naane maripenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naane maripenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaga thaane naan uyir vaazhgirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga thaane naan uyir vaazhgirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naane maripenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naane maripenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un veettil en naatkal pogattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veettil en naatkal pogattume"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangu suvattrukul naam kadhal vaazhttume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangu suvattrukul naam kadhal vaazhttume"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meethu naan konda aasaithane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meethu naan konda aasaithane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu pookkindra pookkal thaan paadattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu pookkindra pookkal thaan paadattume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale ellame ponnanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale ellame ponnanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu thannale tharunam thitthippanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu thannale tharunam thitthippanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondragum neramthil iru nenjangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondragum neramthil iru nenjangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru moondragum nodi thedi alaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru moondragum nodi thedi alaiyuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaga thaane naan uyir vaazhgirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga thaane naan uyir vaazhgirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naane maripenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naane maripenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaga thaane naan uyir vaazhgirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga thaane naan uyir vaazhgirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naane maripenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naane maripenadi"/>
</div>
</pre>
