---
title: "thee illai song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Vaali"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Thee Illai"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XLqosCaQ-kw"
type: "love"
singers:
  - Naresh Iyer
  - Mukesh
  - Gopal Rao
  - Mahathi
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thee illai Pugai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee illai Pugai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vezhvi Seigiraai Vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vezhvi Seigiraai Vizhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool illai Thari illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool illai Thari illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Neigiraai Manadhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Neigiraai Manadhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovillai Madal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovillai Madal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Thenaipeigiraai Uyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Thenaipeigiraai Uyirile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Unnidam Izhakkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Unnidam Izhakkiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum illaiyaai Irukkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum illaiyaai Irukkiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnum Pinnum Chinnam Vaippaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnum Pinnum Chinnam Vaippaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Chinnadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinnadhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Vilaiyaai Thandhenae Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Vilaiyaai Thandhenae Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Vaangikkondenae Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Vaangikkondenae Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Aadaikkondatho Thennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Aadaikkondatho Thennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegunalaai Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegunalaai Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithooral Pottaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithooral Pottaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirmayir Pizhaithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirmayir Pizhaithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaathu Kaiyai Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaathu Kaiyai Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyoram Maiyai Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyoram Maiyai Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyilondru Ezhuthidu Udhattaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyilondru Ezhuthidu Udhattaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilakkiya Kaniyai Vizhungiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkiya Kaniyai Vizhungiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhungiya Nenjam Pozhungiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhungiya Nenjam Pozhungiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Saatchi Pothaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Saatchi Pothaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Mothaatha Kadhal Othatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Mothaatha Kadhal Othatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee illai Pugai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee illai Pugai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Velvi Seigiraai Vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Velvi Seigiraai Vizhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool illai Thari illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool illai Thari illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Neigiraai Manadhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Neigiraai Manadhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovillai Madalillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovillai Madalillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Thaenaipeigiraai Uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Thaenaipeigiraai Uyirilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Unnidam Izhakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Unnidam Izhakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum illaiyaai Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum illaiyaai Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnum Pinnum Chinnam Vaippaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnum Pinnum Chinnam Vaippaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Chinnadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinnadhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punal Vele Veetru Pani Vaadai Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punal Vele Veetru Pani Vaadai Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudaindhathu Namakkoru Pudhu Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudaindhathu Namakkoru Pudhu Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadar Karai Naarai Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadar Karai Naarai Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhingu Oorai Koottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaindhingu Oorai Koottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Nagarvalam Varam Paarththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Nagarvalam Varam Paarththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu Silu Vendru Kuliradikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silu Vendru Kuliradikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu Thodu Endru Thalirthudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Thodu Endru Thalirthudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru Paarvai Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru Paarvai Neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Eduppaaiya Unnul Olippaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Eduppaaiya Unnul Olippaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee illai Pugai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee illai Pugai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaelvi Seigiraai Viliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaelvi Seigiraai Viliyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool illai Thari illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool illai Thari illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaadhal Neigiraai Manathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaadhal Neigiraai Manathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovillai Madal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovillai Madal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Thenaipeigiraai Uyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Thenaipeigiraai Uyirile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Unnidam Ilakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Unnidam Ilakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum illaiyaai Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum illaiyaai Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnum Pinnum Chinnam Vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnum Pinnum Chinnam Vaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Chinnadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinnadhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Vilaiyaai Thandhenae Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Vilaiyaai Thandhenae Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Vaangikkondenae Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Vaangikkondenae Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Aadaikkondadho Thennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Aadaikkondadho Thennai"/>
</div>
</pre>
