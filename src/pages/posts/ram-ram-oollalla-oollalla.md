---
title: "ram ram song lyrics"
album: "Oollalla Oollalla"
artist: "Joy Rayarala"
lyricist: "Kasarla shyam"
director: "sathiya prakash"
path: "/albums/oollalla-oollalla-lyrics"
song: "Ram Ram"
image: ../../images/albumart/oollalla-oollalla.jpg
date: 2020-01-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/spD1FON1BUo"
type: "love"
singers:
  - Mangli
  - Roll Rida
  - Raghu Babu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jingala Jingo Jingala Jingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingala Jingo Jingala Jingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingala Jingo Jingala Jingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingala Jingo Jingala Jingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingala Jingo Jingala Jingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingala Jingo Jingala Jingo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingala Jingala Jingala Jingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingala Jingala Jingala Jingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jing Jing Jing Jing Jing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Jing Jing Jing Jing"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram Jappanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram Jappanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parayi Mall Appana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parayi Mall Appana"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnyanikosthe Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnyanikosthe Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Thaganaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Thaganaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo…la Oo…la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo…la Oo…la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palleturu Nadhu Deko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palleturu Nadhu Deko"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakisthan Arupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakisthan Arupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukistamundaduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukistamundaduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkinti Saruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkinti Saruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo…la Oo…la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo…la Oo…la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tie Dry Vesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tie Dry Vesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Age kindha Dachuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Age kindha Dachuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthi Pori Chusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthi Pori Chusuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaburlla Disko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaburlla Disko"/>
</div>
<div class="lyrico-lyrics-wrapper">Byanku Lonu Thisuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Byanku Lonu Thisuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Caru Bangla Konukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caru Bangla Konukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotlu Muta Kattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotlu Muta Kattukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelo Ladon Vusko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelo Ladon Vusko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daanaveera Shurakarna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanaveera Shurakarna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati Manushuluraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikinantha Dochukunedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikinantha Dochukunedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati Manushuluraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki Mukku Donda Pandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Mukku Donda Pandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambineshan Endhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambineshan Endhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Musoloniki Paduchupilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musoloniki Paduchupilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu Chudu Dasaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Chudu Dasaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oolala Oolala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oolala Oolala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adollu kuda Shudu Attlane Paduvaddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adollu kuda Shudu Attlane Paduvaddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Phone Laa Rendu Simmulesi Vaduthunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Phone Laa Rendu Simmulesi Vaduthunnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Emo Looking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Emo Looking"/>
</div>
<div class="lyrico-lyrics-wrapper">Nextemo Huggingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nextemo Huggingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu After Lovvingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu After Lovvingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selfilo Kissingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfilo Kissingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsapp Chatingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp Chatingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Next Handu Clipingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Next Handu Clipingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tvlo Brekingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tvlo Brekingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daanaveera Shurakarna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanaveera Shurakarna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati Manushuluraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikinantha Dochukunedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikinantha Dochukunedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati Manushuluraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manchi Chesinonni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Chesinonni"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadu Gurthu Pettukoduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadu Gurthu Pettukoduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchinonni Janmala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchinonni Janmala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadi Marchi Poduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadi Marchi Poduraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo..lala oolala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo..lala oolala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo..lala oolala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo..lala oolala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeda Evadi Dabba Vaade Kottukovaleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeda Evadi Dabba Vaade Kottukovaleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaka Vuntune Esaru Pettale raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaka Vuntune Esaru Pettale raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oola Oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oola Oola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shortcuttu Chusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shortcuttu Chusuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Master Planu Vesukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master Planu Vesukoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Blacku Danda Chesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blacku Danda Chesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottlu Kotti Posuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottlu Kotti Posuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Bildappulicchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bildappulicchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalugurni Thippuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalugurni Thippuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Social Medialananna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Social Medialananna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Peru Thecchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Peru Thecchuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daanaveera Shurakarna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanaveera Shurakarna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaati Manushuluraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikinantha Dochukunedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikinantha Dochukunedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenati Manushuluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati Manushuluraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swameelu Babala Venaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swameelu Babala Venaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajana Cheyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajana Cheyyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Se Se Cameraalalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Se Se Cameraalalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanni Bukku Cheyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanni Bukku Cheyyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo…la Oo…la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo…la Oo…la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakka Paisalunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakka Paisalunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Houlaganni Laggamadale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Houlaganni Laggamadale"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Loveru Vunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Loveru Vunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanni Linlapetti Thiragale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanni Linlapetti Thiragale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo…la Oo…la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo…la Oo…la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Battebaju Panulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Battebaju Panulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajarlla Cheyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajarlla Cheyyale"/>
</div>
<div class="lyrico-lyrics-wrapper">FaceBook Twitterla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="FaceBook Twitterla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethulenno Cheppale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethulenno Cheppale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomai Friend Edekada Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Friend Edekada Trendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai Friend Edekada Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Friend Edekada Trendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai Friend Edekada Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Friend Edekada Trendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai Friend Edekada Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Friend Edekada Trendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai Friend Edekada Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Friend Edekada Trendu"/>
</div>
</pre>
