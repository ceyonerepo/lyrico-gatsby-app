---
title: "Enakkenna Aachu song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Enakkenna Aachu"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I7k8Q-bG86s"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enakenna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakenna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unai sutriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai sutriye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir meyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir meyuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">udal theyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal theyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">unai serave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai serave"/>
</div>
<div class="lyrico-lyrics-wrapper">varam ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thinanthorum naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinanthorum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moochu mothum thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu mothum thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum atra nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum atra nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedukindren naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedukindren naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ore nodiyil un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore nodiyil un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthalul paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthalul paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">vera ondrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera ondrum "/>
</div>
<div class="lyrico-lyrics-wrapper">thedavillai naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedavillai naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagagave niram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagagave niram maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">verodu naan vilunthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verodu naan vilunthenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unai parthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai parthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh enaketho aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh enaketho aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enake en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake en moochadi"/>
</div>
</pre>
