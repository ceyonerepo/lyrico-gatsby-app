---
title: "nee tholai thoorathil song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Vno - Arungopal"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Nee Tholai Thoorathil"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/288ExHF8rik"
type: "sad"
singers:
  - Aalaap Raju
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naan Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naan Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kanavaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kanavaaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naan Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naan Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kanavaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kanavaaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Vazhigindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Vazhigindradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Koodudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Koodudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurinji Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurinji Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakkillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakkillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Asaivilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Asaivilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakadhu Puriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakadhu Puriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhum Yeno Vilagi Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhum Yeno Vilagi Sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkul Naan Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul Naan Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Illai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Illai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhaai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhaai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Theda Malar Vaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Theda Malar Vaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thediya Malargalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediya Malargalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo Sendru Poothathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Sendru Poothathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaai Ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaai Ponathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ninaivaal Enai Vathaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivaal Enai Vathaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivae Uyirai Thindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivae Uyirai Thindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Mattum Thaan Kidakindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Mattum Thaan Kidakindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Nee Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Nee Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Vazhigindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Vazhigindradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Koodudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Koodudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurinji Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurinji Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakkillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakkillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naan Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naan Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kanavaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kanavaaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvilae"/>
</div>
</pre>
