---
title: "nuvve nuvve song lyrics"
album: "Red"
artist: "Mani Sharma"
lyricist: "Sirivennela Sitarama Sastry"
director: "Kishore Tirumala"
path: "/albums/red-lyrics"
song: "Nuvve Nuvve Nuvve Nuvvunte"
image: ../../images/albumart/red.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uaD9CblxV4s"
type: "love"
singers:
  - Ramya Behara
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvve Nuvve Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Nuvve Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chaalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chaalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare Varam Kore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare Varam Kore"/>
</div>
<div class="lyrico-lyrics-wrapper">Panemi Ledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panemi Ledhugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedaari Dhaarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaari Dhaarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraye Vaanagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraye Vaanagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanantha Thaanugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanantha Thaanugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhiloche Kaanukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhiloche Kaanukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sparshe Cheppindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sparshe Cheppindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Sagame Vunnaanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Sagame Vunnaanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Kariginnaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Kariginnaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenantu Purthayinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenantu Purthayinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Vunnattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Vunnattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakuda Teliyadhu Ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakuda Teliyadhu Ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvantu Rakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvantu Rakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenundunnaa Lenattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenundunnaa Lenattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Lokamlo Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Lokamlo Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholijantani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholijantani"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinche Premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinche Premante"/>
</div>
<div class="lyrico-lyrics-wrapper">Piche Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piche Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Piche Lekunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Piche Lekunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Premedhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premedhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatisthe Thappundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatisthe Thappundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Kaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Kaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve Nuvve Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Nuvve Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chaalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chaalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare Varam Kore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare Varam Kore"/>
</div>
<div class="lyrico-lyrics-wrapper">Panemi Leadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panemi Leadhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedari Darilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedari Darilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraye Vanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraye Vanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanantha Thaanugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanantha Thaanugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilochche Kanukaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilochche Kanukaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendhuku Jeevinchaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhuku Jeevinchaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinchindhante Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinchindhante Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhuku Antu Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhuku Antu Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupisthayi Praanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupisthayi Praanaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevvaritho Cheppoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvaritho Cheppoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Iddaridhe Ee Guttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Iddaridhe Ee Guttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naa Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naa Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Guvvalle Goodunu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guvvalle Goodunu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetu Vellaalo Vethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Vellaalo Vethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhulai Edhurochchindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhulai Edhurochchindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Evarikivvaali Annandhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Evarikivvaali Annandhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Unnaanannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Unnaanannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Kadhaa"/>
</div>
</pre>
