---
title: 'manogari song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Manogari'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/05N3OYBejls"
type: 'Enjoy'
singers: 
- Haricharan
- Mohana Bhogaraju
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Urukkiyo.. natchathira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukkiyo.. natchathira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirakkiyo.. en azhagin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirakkiyo.. en azhagin"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral saaral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porukki minukki sedhukki padhitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukki minukki sedhukki padhitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooral… mooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooral… mooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukki irukki serukkai erikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukki irukki serukkai erikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaral.. aaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaral.. aaral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mano…oooo..gari..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mano…oooo..gari.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mano…oooo..gari..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mano…oooo..gari.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallan naano unnai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallan naano unnai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan ullam kollai pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan ullam kollai pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai vittu meeri undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai vittu meeri undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagugal thulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagugal thulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki sokki sokki nirkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki sokki sokki nirkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olithu maraitha valathai edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olithu maraitha valathai edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal.. thedal..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal.. thedal.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urukkiyo.. natchathira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukkiyo.. natchathira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirakkiyo.. en azhagin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirakkiyo.. en azhagin"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral saaral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mega thundai vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega thundai vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal padaithaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal padaithaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru… en thedal veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru… en thedal veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaandhal poovai killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaandhal poovai killi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiviral seidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiviral seidhaano"/>
</div>
  <div class="lyrico-lyrics-wrapper">Veru… en thedal veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru… en thedal veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi kanda ven sanggil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi kanda ven sanggil"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan anal ondrai seidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan anal ondrai seidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaali irandai pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaali irandai pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thanam rendai seidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thanam rendai seidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhakkida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhakkida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mano…ooo…gari….   Manogari…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mano…ooo…gari….   Manogari…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mano..ooo…gari…   Manogari….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mano..ooo…gari…   Manogari…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovai vittu poovil thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai vittu poovil thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenai unnum vandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenai unnum vandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paagam vittu paagam paagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paagam vittu paagam paagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavinen!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavinen!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olithu maraitha valathai edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olithu maraitha valathai edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal.. thedal..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal.. thedal.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urukkiyo.. natchathira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukkiyo.. natchathira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirakkiyo.. en azhagin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirakkiyo.. en azhagin"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral saaral"/>
</div>
</pre>