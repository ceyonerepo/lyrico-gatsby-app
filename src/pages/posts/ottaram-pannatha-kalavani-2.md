---
title: "ottaram pannatha song lyrics"
album: "Kalavani 2"
artist: "Mani Amudhavan"
lyricist: "Mani Amudhavan"
director: "A. Sarkunam"
path: "/albums/kalavani-2-lyrics"
song: "Ottaram Pannatha"
image: ../../images/albumart/kalavani-2.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/btB_Uu0OKLM"
type: "love"
singers:
  - Mani Amuthavan
  - Namtha Babu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iruntha Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iruntha Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku Naane Thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku Naane Thevaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil Nanum Varen Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil Nanum Varen Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathan Kanduputta Yetta Yetta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathan Kanduputta Yetta Yetta Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa En Kooda Vanthu Kuppa Kotta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa En Kooda Vanthu Kuppa Kotta Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanavilla Pola Nee Vanthu Enna Valacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanavilla Pola Nee Vanthu Enna Valacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanamellam Natchathiram Pola Kotti Eracha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanamellam Natchathiram Pola Kotti Eracha"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Ithayama Enakulla Thudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Ithayama Enakulla Thudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaikkave Illaiye Engiruntha Molacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaikkave Illaiye Engiruntha Molacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooram Ninnu Siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram Ninnu Siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaikinnu Azhacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaikinnu Azhacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye Aana Puliya Kanda Maana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Aana Puliya Kanda Maana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathan Kanduputta Yetta Yetta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathan Kanduputta Yetta Yetta Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa En Kooda Vanthu Kuppa Kotta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa En Kooda Vanthu Kuppa Kotta Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaya Suthum Pillaiya Kakkathula Kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaya Suthum Pillaiya Kakkathula Kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniyilla Neram Kooda Kalli Pola Velanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniyilla Neram Kooda Kalli Pola Velanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitherumbu Thalaiyil Sakkaraiya Sumantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitherumbu Thalaiyil Sakkaraiya Sumantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttalum Theeya Suthi Vittilaga Parantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttalum Theeya Suthi Vittilaga Parantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Kolanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Kolanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vathaama Neranja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathaama Neranja"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage Aanaa Puliya Kanda Maana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Aanaa Puliya Kanda Maana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathan Kanduputta Yetta Yetta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathan Kanduputta Yetta Yetta Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa En Kooda Vanthu Kuppa Kotta Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa En Kooda Vanthu Kuppa Kotta Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaram Pannatha Unkooda Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottaram Pannatha Unkooda Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Enna Nan Unakkaga Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Enna Nan Unakkaga Thaaren"/>
</div>
</pre>
