---
title: "uyire en uyire song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Vno - Arungopal"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Uyire En Uyire"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4_JMtef6jEs"
type: "love"
singers:
  - Arunraja Kamaraj
  - Thameem Ansari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirae En Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae En Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivil Aval Mugam Kattathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivil Aval Mugam Kattathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravae En Iravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravae En Iravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavindri Un Neram Kadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavindri Un Neram Kadanthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Moodinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Moodinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithirunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Bodhai Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Bodhai Yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paavaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paavaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naan Theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naan Theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkullae Tholaithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkullae Tholaithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaamal Urangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaamal Urangaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavithenae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavithenae Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinamdhorum Unkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinamdhorum Unkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Dhooram Naan Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Dhooram Naan Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu Viral Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu Viral Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Naal Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Naal Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum Unnai Kannil Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Unnai Kannil Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaanaalum Kann Vizhithirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaanaalum Kann Vizhithirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Karuvizhi Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karuvizhi Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavara Kavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavara Kavara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iru Vizhi Paarvai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iru Vizhi Paarvai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethara Sethara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethara Sethara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Mayilae Ennai Konjum Kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Mayilae Ennai Konjum Kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Ingu Thedi Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Ingu Thedi Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurinji Poovadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurinji Poovadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthai Perisaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthai Perisaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Mounam Melisaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Mounam Melisaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyalum Isaiyum Nadagamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyalum Isaiyum Nadagamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum Nodiyil Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum Nodiyil Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyooo Ennai Indha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyooo Ennai Indha Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthum Paadu Podhumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthum Paadu Podhumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Peyarai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Peyarai Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhiyaakinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyaakinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Thaan Naan Pogavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Thaan Naan Pogavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigaaramaai Unai Sutrinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaramaai Unai Sutrinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaarthai Kooren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaarthai Kooren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Pothum Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Pothum Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirum Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuthillaa Kaviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthillaa Kaviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullillaa Malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullillaa Malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solli Poyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solli Poyen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kurinji Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kurinji Poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Naatkkal Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naatkkal Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalaai Vazhvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalaai Vazhvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambanin Kavigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambanin Kavigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotridum Vagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotridum Vagaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Vaarthaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Vaarthaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Korpennae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korpennae Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae En Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae En Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivil Aval Mugam Kattathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivil Aval Mugam Kattathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravae En Iravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravae En Iravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavindri Un Neram Kadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavindri Un Neram Kadanthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Moodinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Moodinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithirunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Bodhai Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Bodhai Yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paavaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paavaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthae Poguthae Poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthae Poguthae Poguthae"/>
</div>
</pre>
