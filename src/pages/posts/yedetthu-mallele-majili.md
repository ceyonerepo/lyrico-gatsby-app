---
title: "yedetthu mallele song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Shiva Nirvana"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "Yedetthu Mallele"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/n9I4Ut3-RUs"
type: "sad"
singers:
  - Kaala Bhairava
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedettu mallele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedettu mallele"/>
</div>
<div class="lyrico-lyrics-wrapper">Koppulona chere daare lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppulona chere daare lede"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee todu koyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee todu koyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddugookevelaa kooyalede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddugookevelaa kooyalede"/>
</div>
<div class="lyrico-lyrics-wrapper">Raayettu ala teradaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raayettu ala teradaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheraraave cheliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheraraave cheliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eepoddu peedakala daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eepoddu peedakala daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidarove sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidarove sakhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kantireppa kalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kantireppa kalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneetilona kathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneetilona kathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundelona sadine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelona sadine"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oopiraina oosune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oopiraina oosune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oopiraaginaa usurupoyinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oopiraaginaa usurupoyinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadiliponanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadiliponanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taarette taane taane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taarette taane taane"/>
</div>
<div class="lyrico-lyrics-wrapper">taareraareraare taareraare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taareraareraare taareraare"/>
</div>
<div class="lyrico-lyrics-wrapper">Taarette taane naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taarette taane naane"/>
</div>
<div class="lyrico-lyrics-wrapper">taarenaanenaane taarenaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taarenaanenaane taarenaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Taarette taare taarenaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taarette taare taarenaane"/>
</div>
<div class="lyrico-lyrics-wrapper">taareraare taareraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taareraare taareraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Taarette taare taareraare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taarette taare taareraare"/>
</div>
<div class="lyrico-lyrics-wrapper">taareraare taareraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taareraare taareraa"/>
</div>
</pre>
