---
title: "Yaerumayileri song lyrics"
album: "Sardar"
artist: "G. V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "P.S Mithran"
path: "/albums/sardar-lyrics"
song: "Yaerumayileri"
image: ../../images/albumart/sardar.jpg
date: 2022-10-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_qdwl3ErV0I?controls=0"
type: "Devotional"
singers:
  - Karthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kundraana kundrathilae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundraana kundrathilae.."/>
</div>
<div class="lyrico-lyrics-wrapper">Koman avan kudi irukkaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koman avan kudi irukkaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kodangi paattu eduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodangi paattu eduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuthadi arogara arogara arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuthadi arogara arogara arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gummuna gummiruttu konjuna jallikkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummuna gummiruttu konjuna jallikkattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valli un kurukkazhagu damukku diyyaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valli un kurukkazhagu damukku diyyaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varama vanthae nippen dimukku duppaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varama vanthae nippen dimukku duppaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alluna allikattu aasaiya mallukattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluna allikattu aasaiya mallukattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valli un kanukkaalu damukku dappaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valli un kanukkaalu damukku dappaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannaathi paarayellaam vazhukki nikkalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaathi paarayellaam vazhukki nikkalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ottu maangani pola oyilaana un mookku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu maangani pola oyilaana un mookku"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttiye koda saanjaa adhu thaane en sokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttiye koda saanjaa adhu thaane en sokku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottazhagu valliyamma podanume neththichutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottazhagu valliyamma podanume neththichutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu allikitta kattikkuven thaali katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu allikitta kattikkuven thaali katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru mayileri vilaiyaadi varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru mayileri vilaiyaadi varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettum vinai theerthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettum vinai theerthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yenthi kolven naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai yenthi kolven naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarumuga saami arulaasi tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumuga saami arulaasi tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthum thamizh kettae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthum thamizh kettae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai peiyum uchi vaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai peiyum uchi vaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye eesane unakkappan endraalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye eesane unakkappan endraalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbamae unai venda andaathae sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamae unai venda andaathae sogam"/>
</div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee pinjilae pazhuthennu sonnaalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pinjilae pazhuthennu sonnaalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamae unai venda koodum santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamae unai venda koodum santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvai en vaayoora varum solle vadivele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvai en vaayoora varum solle vadivele"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai yaam pugazhnthaale dhinanthorum Thirunaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai yaam pugazhnthaale dhinanthorum Thirunaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru mayileri vilaiyaadi varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru mayileri vilaiyaadi varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettum vinai theerthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettum vinai theerthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yenthi kolven naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai yenthi kolven naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarumuga saami arulaasi tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumuga saami arulaasi tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthum thamizh kettae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthum thamizh kettae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai peiyum uchi vaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai peiyum uchi vaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadivaalam enakkethum kidaiyaathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadivaalam enakkethum kidaiyaathappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thaandi malai thaandi jeyippenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thaandi malai thaandi jeyippenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi kaakka kulam kaakka piranthenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kaakka kulam kaakka piranthenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaiyorai poli poda ezhunthenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaiyorai poli poda ezhunthenappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kudineerullum iruppenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudineerullum iruppenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhuvor nenjai unarvenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhuvor nenjai unarvenappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthalethappa mudivethappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalethappa mudivethappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuvor kannai thudaippenappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuvor kannai thudaippenappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru mayileri vilaiyaadi varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru mayileri vilaiyaadi varuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettum vinai theerthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettum vinai theerthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yenthi kolven naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai yenthi kolven naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarumuga saami arulaasi tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumuga saami arulaasi tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthum thamizh kettae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthum thamizh kettae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai peiyum uchi vaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai peiyum uchi vaanae"/>
</div>
</pre>
