---
title: "vidhai pola maraivaaga song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Vidhai Pola Maraivaaga"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YHNTB4hvKnM"
type: "Mass"
singers:
  - Hariharan  
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vidhai Pola Maraivaaga Vazhnthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Pola Maraivaaga Vazhnthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Thaan Madha Yaanai Pol Varugiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Thaan Madha Yaanai Pol Varugiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaiyaalam Theriyaadha Udaiyodu Thirnthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalam Theriyaadha Udaiyodu Thirnthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaiyodu Nadai Payilgiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyodu Nadai Payilgiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Pozhuthum Urangaadha Kaavalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Pozhuthum Urangaadha Kaavalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Sumai Kandu Miraladha Sevagan Kolgaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Sumai Kandu Miraladha Sevagan Kolgaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaradha Komagan Ivan Dhisai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaradha Komagan Ivan Dhisai Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathidum Thirumagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathidum Thirumagan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Vandhu Vizhunthaalum Idiyaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Vandhu Vizhunthaalum Idiyaadhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idar Enna Vanthaalum Idaraathavan Patharaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idar Enna Vanthaalum Idaraathavan Patharaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitharaamal Adhiraamal Udhiraamal Irukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharaamal Adhiraamal Udhiraamal Irukindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam Petravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Petravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagai Enna Vanthaalum Patharadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai Enna Vanthaalum Patharadhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhavigal Thadhuthaalum Paniyaadhavan Oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhavigal Thadhuthaalum Paniyaadhavan Oor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodi Ethirthaalum Miraladhavan Otraiyaai Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi Ethirthaalum Miraladhavan Otraiyaai Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perai Vadham Seidhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai Vadham Seidhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirigal Evar Enru Ennadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirigal Evar Enru Ennadhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholviyin Vizhumbilum Thuvalaadhavan Vilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholviyin Vizhumbilum Thuvalaadhavan Vilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thathaalum Valaiyadhavan Elimaikku Ivan\
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thathaalum Valaiyadhavan Elimaikku Ivan\"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum Thunaiyanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Thunaiyanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyaaga Nindru Marporil Vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyaaga Nindru Marporil Vendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adutha Thalam Ethuvendr Aaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Thalam Ethuvendr Aaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Pola Maraivaaga Vazhnthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Pola Maraivaaga Vazhnthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Thaan Madha Yaanai Pol Varugiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Thaan Madha Yaanai Pol Varugiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaiyaalam Theriyaadha Udaiyodu Thirinthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalam Theriyaadha Udaiyodu Thirinthava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaiyodu Nadai Payilgiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyodu Nadai Payilgiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Irundhaalum Padai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Irundhaalum Padai Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Dharaniyai Pizhakindra Edai Thaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Dharaniyai Pizhakindra Edai Thaan Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulan Ainthum Thelivaaga Irukindravan Puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulan Ainthum Thelivaaga Irukindravan Puram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangum Elidhaaga Valaikindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangum Elidhaaga Valaikindravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaiyaatha Thadaigalai Udaikindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyaatha Thadaigalai Udaikindravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaai Saathanai Padaikindravan Erimalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaai Saathanai Padaikindravan Erimalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolambaaga Kothikindravan Kuri Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolambaaga Kothikindravan Kuri Vaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samarile Jeikindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarile Jeikindravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Ena Seeri Amila Mazhai Thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Ena Seeri Amila Mazhai Thoovi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukindri Desathai Azhagaaga Thudaikindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukindri Desathai Azhagaaga Thudaikindravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Pola Maraivaaga Vazhnthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Pola Maraivaaga Vazhnthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Thaan Madha Yaanai Pol Varugiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Thaan Madha Yaanai Pol Varugiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaiyaalam Theriyaadha Udaiyodu Thirinthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalam Theriyaadha Udaiyodu Thirinthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaiyodu Nadai Payilgiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyodu Nadai Payilgiran"/>
</div>
</pre>
