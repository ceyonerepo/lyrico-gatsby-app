---
title: 'ilamai thirumbudhe song lyrics'
album: 'Petta'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Karthik Subbaraj'
path: '/albums/petta-song-lyrics'
song: 'Ilamai Thirumbudhe'
image: ../../images/albumart/petta.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TKeU1bLlAcc"
type: 'love'
singers: 
- Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ilamai thirumbudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilamai thirumbudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puiryaatha puthiraachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Puiryaatha puthiraachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya thudippilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhaya thudippilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani kaathum soodachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kaathum soodachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey thulli kuthikuthu nenjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thulli kuthikuthu nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam varavillai konjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookam varavillai konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai varum ena anjum
<input type="checkbox" class="lyrico-select-lyric-line" value="Maalai varum ena anjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum mudhal paruvam
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum mudhal paruvam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaigal seeppai theduthae thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal seeppai theduthae thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal unnai theduthae maanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kangal unnai theduthae maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal medhuvaai poguthu veenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkal medhuvaai poguthu veenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella thoduthae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella thoduthae kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaigal seeppai theduthae thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal seeppai theduthae thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal unnai theduthae maanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kangal unnai theduthae maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal medhuvaai poguthu veenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkal medhuvaai poguthu veenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella thoduthae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella thoduthae kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey ilamai thirumbudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey ilamai thirumbudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puiryaatha puthiraachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Puiryaatha puthiraachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya thudippilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhaya thudippilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani kaathum soodachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kaathum soodachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mhmm vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mhmm vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazha thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa en kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa en kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthu thaan paarpoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhnthu thaan paarpoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil korppoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanavil korppoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaigaiyil thaangathevai
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaigaiyil thaangathevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thol thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thol thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani maram naanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani maram naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottamaai neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thottamaai neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaalibathin ellaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaalibathin ellaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal vantha mullaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaasal vantha mullaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum varai pogalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum varai pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pizhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna pizhiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oorae nammai paarpathu polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorae nammai paarpathu polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho bimbam thondruthu maanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedho bimbam thondruthu maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal tharaiyil kolam poda
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalgal tharaiyil kolam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella thoduthae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella thoduthae kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilamai thirumbudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilamai thirumbudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puiryaatha puthiraachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Puiryaatha puthiraachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya thudippilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhaya thudippilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani kaathum soodachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kaathum soodachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey thulli kuthikuthu nenjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thulli kuthikuthu nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam varavillai konjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookam varavillai konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai varum ena anjum
<input type="checkbox" class="lyrico-select-lyric-line" value="Maalai varum ena anjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum mudhal paruvam
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum mudhal paruvam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal seeppai theduthae thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal seeppai theduthae thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal unnai theduthae maanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kangal unnai theduthae maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal medhuvaai poguthu veenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkal medhuvaai poguthu veenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella thoduthae kaadhalae (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella thoduthae kaadhalae"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilamai thirumbudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilamai thirumbudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puiryaatha puthiraachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Puiryaatha puthiraachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya thudippilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhaya thudippilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani kaathum soodachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kaathum soodachae"/>
</div>
</pre>