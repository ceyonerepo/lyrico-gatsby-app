---
title: "iraiva iraiva song lyrics"
album: "Sei"
artist: "NyX Lopez"
lyricist: "Yuga Bharathih"
director: "Raj Babu"
path: "/albums/sei-song-lyrics"
song: "Iraiva Iraiva"
image: ../../images/albumart/sei.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yMbQdDBciio"
type: "devotional"
singers:
  - Atif Ail
  - Saptaswara Rishu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagadha paadhai unarndhavan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadha paadhai unarndhavan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbennum vazhiyae arindhavan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbennum vazhiyae arindhavan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraana podhum koduppavan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraana podhum koduppavan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theengondru nadanthal thaduppavan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengondru nadanthal thaduppavan neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanathathai nee kaatuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanathathai nee kaatuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamalae karaiyetruvaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamalae karaiyetruvaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodathathai nee pokkuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodathathai nee pokkuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraitheeravae karam neettuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraitheeravae karam neettuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannethirinilae nee therivadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannethirinilae nee therivadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un adi thozhuthaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un adi thozhuthaal "/>
</div>
<div class="lyrico-lyrics-wrapper">thuyar varuvadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyar varuvadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un oruvanukkae ingu eedu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un oruvanukkae ingu eedu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inai yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai yaarumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanum mannum unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum mannum unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyam suzhalum unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyam suzhalum unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum yaavum unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum yaavum unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrum indrum unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrum indrum unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai kooda unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai kooda unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum vaazhvae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum vaazhvae unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollathathum poi aanathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollathathum poi aanathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvayil thuulagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvayil thuulagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaanathum melaanathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaanathum melaanathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aanayil kai kudumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aanayil kai kudumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgalum nee thanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalum nee thanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangalum nee thanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangalum nee thanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayangalo nee enbadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangalo nee enbadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen intha maayamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha maayamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai yaarai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai yaarai solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumae unarthoru paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae unarthoru paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendinaal kidaithidum yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendinaal kidaithidum yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuuyathilum thuuyavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuuyathilum thuuyavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaigalin naayaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaigalin naayaganae"/>
</div>
</pre>