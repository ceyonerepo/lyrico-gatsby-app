---
title: "are dhammare song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Kirithiya"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "Are Dhammare"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Neha Bhasin
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilaavey vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilaavey vennilaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">velichamulla veedu nee velikkadhavai moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velichamulla veedu nee velikkadhavai moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">mirattudhey inbachoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirattudhey inbachoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">velichamulla veedu vaa velikkadhavai moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velichamulla veedu vaa velikkadhavai moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">koochamey thooram Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koochamey thooram Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaraai vaaraai vennilaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal kanakkappoattu vaithu pennai kannaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal kanakkappoattu vaithu pennai kannaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naal kanakkai poada viduvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal kanakkai poada viduvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal thangal seidhuvittu ennai pin naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal thangal seidhuvittu ennai pin naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">maanganigal thinnacholluvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanganigal thinnacholluvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal thangal seidhuvittu ennai pin naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal thangal seidhuvittu ennai pin naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">maanganigal thinnacholluvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanganigal thinnacholluvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilandhaadi veeraa irukkeney soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilandhaadi veeraa irukkeney soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalandhaadi inba karaiyeradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalandhaadi inba karaiyeradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kurundhaadi veeraa kurippaarthu joaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurundhaadi veeraa kurippaarthu joaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">virundhaali konjam pasiyaaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virundhaali konjam pasiyaaradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathirikku pazhakkamatra vervai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathirikku pazhakkamatra vervai"/>
</div>
<div class="lyrico-lyrics-wrapper">en meley poothirukka seidhuvaruvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en meley poothirukka seidhuvaruvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirukka siraipidikkum poarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukka siraipidikkum poarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai pagal mudindhum pallikolluvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai pagal mudindhum pallikolluvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai vaaraai vennilaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai vaaraai vennilaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">kelaai kelaai endhan kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelaai kelaai endhan kadhaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theroadum saalai vilaiyaadum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theroadum saalai vilaiyaadum kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">poaraadu solvadhai en segaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaraadu solvadhai en segaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">neeraadumboadhu ninaigindra maadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeraadumboadhu ninaigindra maadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayaadhu kaaranam neeyillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayaadhu kaaranam neeyillaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thammarey thammarey thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thammarey thammarey thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">suga vanmurai en rasigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga vanmurai en rasigaa"/>
</div>
</pre>
