---
title: "sinnavaada song lyrics"
album: "Ashoka Vanamlo Arjuna Kalyanam"
artist: "Jay Krish"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Vidya Sagar Chinta"
path: "/albums/ashoka-vanamlo-arjuna-kalyanam-lyrics"
song: "Sinnavaada"
image: ../../images/albumart/ashoka-vanamlo-arjuna-kalyanam.jpg
date: 2022-05-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/q_Wwn7jHufc"
type: "happy"
singers:
  - Ananya Bhat
  - Gowtham Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaggolu padakoy pillavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaggolu padakoy pillavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchi ginchi sochayinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchi ginchi sochayinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Labam ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labam ledhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabba vinantavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabba vinantavera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata paata aatu potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata paata aatu potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mayara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara rakumara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rakumara"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana chushalera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana chushalera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha raase pani naadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha raase pani naadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha thondhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha thondhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaggolu padakoy pillavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaggolu padakoy pillavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchi ginchi sochayinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchi ginchi sochayinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Labam ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labam ledhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabba vinantavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabba vinantavera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata paata aatu potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata paata aatu potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mayara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalapulu moose kalavarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalapulu moose kalavarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuni mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuni mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalaku nanne aasha vadhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalaku nanne aasha vadhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasha ledu dosa ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha ledu dosa ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhira nee sodhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira nee sodhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Burra dhaka pone podha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burra dhaka pone podha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethilo oodhedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethilo oodhedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupchap untu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupchap untu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chustha unte poyedhi emundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chustha unte poyedhi emundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari sari vishayame kurasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari sari vishayame kurasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppesey o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppesey o saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugule thadabade vadhugulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugule thadabade vadhugulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhu sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhu sanchari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rara rakumara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rakumara"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana chushalera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana chushalera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha raase pani naadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha raase pani naadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha thondhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha thondhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaggolu padakoy pillavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaggolu padakoy pillavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchi ginchi sochayinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchi ginchi sochayinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Labam ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labam ledhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Orori sinnavada sinnavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori sinnavada sinnavada"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabba vinantavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabba vinantavera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata paata aatu potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata paata aatu potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mayara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mayara"/>
</div>
</pre>
