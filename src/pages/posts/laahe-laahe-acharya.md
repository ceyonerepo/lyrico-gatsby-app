---
title: "laahe laahe song lyrics"
album: "Acharya"
artist: "Manisharma"
lyricist: "Ramajogayya sastry"
director: "Koratala Siva"
path: "/albums/acharya-lyrics"
song: "Laahe Laahe - Kondala Raju Bangaru Konda"
image: ../../images/albumart/acharya.jpg
date: 2022-04-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/65P3H1idDQE"
type: "mass"
singers:
  - Harika Narayan
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondala raaju bangaru konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondala raaju bangaru konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda jaathiki anda dhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda jaathiki anda dhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maddhe rathiri lechi mangala gowry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddhe rathiri lechi mangala gowry"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallelu kosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallelu kosindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatini maalelu kadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatini maalelu kadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchu kondal saamini talachindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchu kondal saamini talachindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mello melikala naagula dhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mello melikala naagula dhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula vedini egiri padanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula vedini egiri padanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti vibudhi jala jala raali padanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti vibudhi jala jala raali padanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saambadu kadhilinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambadu kadhilinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma pilupuku saami attharu segalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma pilupuku saami attharu segalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vila vila naliginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vila vila naliginde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kora kora koruvulu mande kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora kora koruvulu mande kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadalirabosina simpiri kurulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadalirabosina simpiri kurulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerrati kopalegisina kunkum bottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerrati kopalegisina kunkum bottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela kaasindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela kaasindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peniviti raakanu telisi seemathangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peniviti raakanu telisi seemathangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soggulu pusindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soggulu pusindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ubalatanga mundhatikuriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ubalatanga mundhatikuriki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyavatharam chusina kaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyavatharam chusina kaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhaa shankamshulam bairagesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhaa shankamshulam bairagesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhani sanigindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhani sanigindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Impuga ee putaina raaleva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Impuga ee putaina raaleva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani sanuvuga kasirindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani sanuvuga kasirindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokalele enthodaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokalele enthodaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokuva madise sonthintlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokuva madise sonthintlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammori gaddam patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammori gaddam patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathimalinavi addala naamalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathimalinavi addala naamalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu magala nadumana adda raavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu magala nadumana adda raavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettanti niyamalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettanti niyamalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okato jaamuna kaligina virahama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okato jaamuna kaligina virahama"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendojamuki mudhirina viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendojamuki mudhirina viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sardhukupoye sarasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sardhukupoye sarasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhire velaku mudo jaamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhire velaku mudo jaamaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhika perige nalugo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhika perige nalugo "/>
</div>
<div class="lyrico-lyrics-wrapper">jamuki gullo gantalu modhalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jamuki gullo gantalu modhalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laahe laahe laahe laahe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laahe laahe laahe laahe "/>
</div>
<div class="lyrico-lyrics-wrapper">laahe laahe laahele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laahe laahe laahele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi oka rojidhi jarige ghattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi oka rojidhi jarige ghattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedamokamayyi ekam avvatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedamokamayyi ekam avvatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaadhi alavatillki alakalallone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaadhi alavatillki alakalallone"/>
</div>
<div class="lyrico-lyrics-wrapper">Kila kilamanukotam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kila kilamanukotam"/>
</div>
<div class="lyrico-lyrics-wrapper">Swayana chebuthunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swayana chebuthunnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubandhalu kada there paatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubandhalu kada there paatam"/>
</div>
</pre>
