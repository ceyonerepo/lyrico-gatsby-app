---
title: "mella mellaga song lyrics"
album: "ABCD"
artist: "Judah Sandhy"
lyricist: "Krishna Kanth"
director: "Sanjeev Reddy"
path: "/albums/abcd-lyrics"
song: "Mella Mellaga"
image: ../../images/albumart/abcd.jpg
date: 2019-05-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VQTx4eswiFY"
type: "love"
singers:
  - Sid Sriram
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha rangu challaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha rangu challaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo matthu laaga allaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo matthu laaga allaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaa nizam oke kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaa nizam oke kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayomayanga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayomayanga undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chero sagam panche vidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chero sagam panche vidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhemito bagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhemito bagundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellagaa gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellagaa gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha rangu challaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha rangu challaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo matthu laaga allaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo matthu laaga allaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho cheruthu yedho kotthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho cheruthu yedho kotthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maro nenula maaraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro nenula maaraane"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha rammani alaa velitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha rammani alaa velitho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaanne ila aapaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaanne ila aapaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhukemo mundhu ledhe ee haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukemo mundhu ledhe ee haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhademo alluthune neevaipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhademo alluthune neevaipoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi kshanam santhoshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi kshanam santhoshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Neneppudu choodandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neneppudu choodandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchame choosanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchame choosanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela yedhi ladhanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela yedhi ladhanthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello kottha rangu challaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello kottha rangu challaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo matthu laaga allaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo matthu laaga allaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manase lopale manase ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase lopale manase ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Murise neevila kalise ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murise neevila kalise ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishalu rojulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishalu rojulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichenu chethilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichenu chethilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenunta needala ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenunta needala ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone anni velalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone anni velalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga nacchade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga nacchade"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaredho thecchaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaredho thecchaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga nacchade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga nacchade"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashalevo icchaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashalevo icchaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha rangu challaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha rangu challaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella mella mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella mella mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo matthu laaga allaavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo matthu laaga allaavee"/>
</div>
</pre>
