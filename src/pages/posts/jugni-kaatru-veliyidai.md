---
title: "jugni song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Shellee"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Jugni"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tSQSh0Qgm1Y"
type: "happy"
singers:
  -	A R Rahman
  - Tejinder Singh
  - Raja Kumari (Rap vocals)
  - Shikara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni ni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni jugni oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragg ragg jedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragg ragg jedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindu rajj main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindu rajj main"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni ni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni jugni oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragg ragg jedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragg ragg jedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindu rajj main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindu rajj main"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avval umda jagg hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avval umda jagg hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jistha jisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jistha jisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Har sheru uchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har sheru uchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usthaa roop hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usthaa roop hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Distha disthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distha disthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni ni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni jugni oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri haan chalegi haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri haan chalegi haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri na chalegi naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri na chalegi naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri haan chalegi haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri haan chalegi haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri na chalegi naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri na chalegi naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni ni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni jugni oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Stars on my crown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stars on my crown"/>
</div>
<div class="lyrico-lyrics-wrapper">Ain't no way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ain't no way"/>
</div>
<div class="lyrico-lyrics-wrapper">Iam coming down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iam coming down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Flying high
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flying high"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">In a pink diamond sea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In a pink diamond sea"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a bird set me free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a bird set me free"/>
</div>
<div class="lyrico-lyrics-wrapper">You're my sweet reverie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You're my sweet reverie"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">Above the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Above the clouds"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top of the world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top of the world"/>
</div>
<div class="lyrico-lyrics-wrapper">Half of the sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Half of the sky"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyramid trees
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyramid trees"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun in my eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun in my eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">You were there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You were there"/>
</div>
<div class="lyrico-lyrics-wrapper">You were not there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You were not there"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A bridge to heaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A bridge to heaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Up in the clouds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up in the clouds"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyone is happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyone is happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singing out loud
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singing out loud"/>
</div>
<div class="lyrico-lyrics-wrapper">You were there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You were there"/>
</div>
<div class="lyrico-lyrics-wrapper">You were not there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You were not there"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Headphones blazing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Headphones blazing"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapping in my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapping in my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Dancing in my mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dancing in my mind"/>
</div>
<div class="lyrico-lyrics-wrapper">Singing out loud
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singing out loud"/>
</div>
<div class="lyrico-lyrics-wrapper">Anti gravity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anti gravity"/>
</div>
<div class="lyrico-lyrics-wrapper">Angles everywhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angles everywhere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Words flow with
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Words flow with"/>
</div>
<div class="lyrico-lyrics-wrapper">No control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No control"/>
</div>
<div class="lyrico-lyrics-wrapper">See how how
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See how how"/>
</div>
<div class="lyrico-lyrics-wrapper">We roll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We roll"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Like a women on a misson
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like a women on a misson"/>
</div>
<div class="lyrico-lyrics-wrapper">I see clearly you my vision
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I see clearly you my vision"/>
</div>
<div class="lyrico-lyrics-wrapper">I would be wishing that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I would be wishing that"/>
</div>
<div class="lyrico-lyrics-wrapper">This moments lasts forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This moments lasts forever"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni ni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jugni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni ni jugni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni ni jugni oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragg ragg jedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragg ragg jedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindu rajj main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindu rajj main"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashakey ishaqey de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashakey ishaqey de"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajju dhajju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajju dhajju"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaiyaan gaiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaiyaan gaiyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juguni jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni jo jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni jo jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juguni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juguni"/>
</div>
</pre>
