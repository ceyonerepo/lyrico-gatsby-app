---
title: "thirudathey song lyrics"
album: "Marainthirunthu Paarkum Marmam Enna"
artist: "Achu Rajamani"
lyricist: "Pa Vijay"
director: "R. Rahesh"
path: "/albums/marainthirunthu-paarkum-marmam-enna-lyrics"
song: "Thirudathey"
image: ../../images/albumart/marainthirunthu-paarkum-marmam-enna.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zIWPsZFMcqE"
type: "mass"
singers:
  - Achu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thirudathe thirudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudathe thirudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruda thaan thaaga alaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruda thaan thaaga alaiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudathe thirudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudathe thirudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruditu palp aai maarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruditu palp aai maarathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh gold parkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh gold parkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannum rendum zooming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannum rendum zooming"/>
</div>
<div class="lyrico-lyrics-wrapper">pannum naaku thonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannum naaku thonga"/>
</div>
<div class="lyrico-lyrics-wrapper">potu puthi maarungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potu puthi maarungada"/>
</div>
<div class="lyrico-lyrics-wrapper">abish panathenu mind 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abish panathenu mind "/>
</div>
<div class="lyrico-lyrics-wrapper">voice sketch podum heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voice sketch podum heart"/>
</div>
<div class="lyrico-lyrics-wrapper">kalati potu kaiya vaikum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalati potu kaiya vaikum da"/>
</div>
<div class="lyrico-lyrics-wrapper">thangatha vangi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangatha vangi "/>
</div>
<div class="lyrico-lyrics-wrapper">vangi kuvichainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangi kuvichainga"/>
</div>
<div class="lyrico-lyrics-wrapper">ambo nu pogum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambo nu pogum "/>
</div>
<div class="lyrico-lyrics-wrapper">pothu thavichainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu thavichainga"/>
</div>
<div class="lyrico-lyrics-wrapper">police kaalil vilunthu aluthainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="police kaalil vilunthu aluthainga"/>
</div>
<div class="lyrico-lyrics-wrapper">gold na ghost kooda konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gold na ghost kooda konjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirudathe thirudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudathe thirudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruda thaan thaaga alaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruda thaan thaaga alaiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ladies ellam inga nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladies ellam inga nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pora naga kadainga theaf 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora naga kadainga theaf "/>
</div>
<div class="lyrico-lyrics-wrapper">life kula risk rusk than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life kula risk rusk than"/>
</div>
<div class="lyrico-lyrics-wrapper">time aera aera kootam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time aera aera kootam "/>
</div>
<div class="lyrico-lyrics-wrapper">kadaiyil kuraiya villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaiyil kuraiya villai"/>
</div>
<div class="lyrico-lyrics-wrapper">sait kadaiku poga meethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sait kadaiku poga meethi"/>
</div>
<div class="lyrico-lyrics-wrapper">theft thaan kalyanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theft thaan kalyanam "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi vantha kadan vangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi vantha kadan vangu"/>
</div>
<div class="lyrico-lyrics-wrapper">thokathan otiyaanam sumai thangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thokathan otiyaanam sumai thangu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvonaa kalavu pona ennagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvonaa kalavu pona ennagum"/>
</div>
<div class="lyrico-lyrics-wrapper">world market ellam gold
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world market ellam gold"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirudathe baby thirudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudathe baby thirudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruditu noga alaiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruditu noga alaiyaathe"/>
</div>
</pre>
