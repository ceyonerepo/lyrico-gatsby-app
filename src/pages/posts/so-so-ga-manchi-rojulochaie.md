---
title: "so so ga song lyrics"
album: "Manchi Rojulochaie"
artist: "Anup Rubens"
lyricist: "Krishna Kanth"
director: "Maruthi"
path: "/albums/manchi-rojulochaie-lyrics"
song: "So So Ga"
image: ../../images/albumart/manchi-rojulochaie.jpg
date: 2021-11-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MOL1rnvXHgg"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Soo Sooga Unna Nanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo Sooga Unna Nanney"/>
</div>
<div class="lyrico-lyrics-wrapper">So Specialey Chesaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Specialey Chesaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo Gaaney Borai Untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo Gaaney Borai Untey"/>
</div>
<div class="lyrico-lyrics-wrapper">Soul-ey Nindavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soul-ey Nindavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhara Verey Andhagatthelunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhara Verey Andhagatthelunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaku Pove Naa Kalley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaku Pove Naa Kalley"/>
</div>
<div class="lyrico-lyrics-wrapper">Endharilona Entha Dooramunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endharilona Entha Dooramunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Choopu Nanne Allenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Choopu Nanne Allenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Baby Muddhu Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Baby Muddhu Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Me Baby Nuvu Na Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Baby Nuvu Na Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Okate Le Nuvu Nenu Okatele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Okate Le Nuvu Nenu Okatele"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvulu Rendaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvulu Rendaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Okateley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Okateley"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Okate Le Nuvu Nenu Okatele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Okate Le Nuvu Nenu Okatele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalu Okate Dhaarulu Okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalu Okate Dhaarulu Okate"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Iddharidhi Gamyam Okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Iddharidhi Gamyam Okate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soo Sooga Unna Nanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo Sooga Unna Nanney"/>
</div>
<div class="lyrico-lyrics-wrapper">So Specialey Chesaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Specialey Chesaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo Gaaney Borai Untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo Gaaney Borai Untey"/>
</div>
<div class="lyrico-lyrics-wrapper">Soul-ey Nindavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soul-ey Nindavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Peru Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Peru Raasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallalone Achesinane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallalone Achesinane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundellone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundellone"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavulapaine Muddhe Aduguthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavulapaine Muddhe Aduguthaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatuka Cheripe Kannerey Raaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatuka Cheripe Kannerey Raaneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiponu Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiponu Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Baby Muddhu Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Baby Muddhu Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Me Baby Nuvu Na Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Baby Nuvu Na Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Okate Le Nuvu Nenu Okatele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Okate Le Nuvu Nenu Okatele"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvulu Rendaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvulu Rendaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Okateley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Okateley"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Okate Le Nuvu Nenu Okatele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Okate Le Nuvu Nenu Okatele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalu Okate Dhaarulu Okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalu Okate Dhaarulu Okate"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Iddharidhi Gamyam Okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Iddharidhi Gamyam Okate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soo Sooga Unna Nanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo Sooga Unna Nanney"/>
</div>
<div class="lyrico-lyrics-wrapper">So Specialey Chesaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Specialey Chesaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo Gaaney Borai Untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo Gaaney Borai Untey"/>
</div>
<div class="lyrico-lyrics-wrapper">Soul-ey Nindavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soul-ey Nindavey"/>
</div>
</pre>
