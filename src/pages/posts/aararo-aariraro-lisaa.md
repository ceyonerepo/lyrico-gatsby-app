---
title: "aararo aariraro song lyrics"
album: "Lisaa"
artist: "Santhosh Dhayanidhi"
lyricist: "Mani Amuthavan"
director: "Raju Viswanath"
path: "/albums/lisaa-lyrics"
song: "Aararo Aariraro"
image: ../../images/albumart/lisaa.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WiUL5RBsn4U"
type: "melody"
singers:
  - Bamba Bakya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaraaroo Aariraroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroo Aariraroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Unna Thoonga Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Unna Thoonga Vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Naan Thoongavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Naan Thoongavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavellam Kann Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellam Kann Mulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Ooo Aaaa Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo Aaaa Aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaroo Aariraroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroo Aariraroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Unna Thoonga Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Unna Thoonga Vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Naan Thoongavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Naan Thoongavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavellam Kann Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellam Kann Mulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna En Karuvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna En Karuvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanginenae Usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanginenae Usurula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Yen Theruvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yen Theruvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesi Pona Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesi Pona Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Meththai Ketkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Meththai Ketkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkukiren Oru Oramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkukiren Oru Oramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Kotti Theerkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Kotti Theerkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkuthae Vizhi Eerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkuthae Vizhi Eerama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perappulla Pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perappulla Pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Venumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Venumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthu Sella Varusham Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthu Sella Varusham Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram Podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Kannukkenna Baaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Kannukkenna Baaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkaadhae Thalli Thooramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkaadhae Thalli Thooramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaroo Aariraroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroo Aariraroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Unna Thoonga Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Unna Thoonga Vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Naan Thoongavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Naan Thoongavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavellam Kann Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellam Kann Mulichen"/>
</div>
</pre>
