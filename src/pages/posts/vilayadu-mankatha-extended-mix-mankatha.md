---
title: "vilayadu mankatha extended mix song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Vilayadu Mankatha Extended Mix"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Tfly3m65kK8"
type: "mass"
singers:
  - Yuvan Shankar Raja
  - Ranjith
  - Anitha Karthikeyan
  - Premgi Amaren
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Drinking too much
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drinking too much"/>
</div>
<div class="lyrico-lyrics-wrapper">Smoking too much
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smoking too much"/>
</div>
<div class="lyrico-lyrics-wrapper">I got my head twisted all over me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got my head twisted all over me"/>
</div>
<div class="lyrico-lyrics-wrapper">Drinking too much
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drinking too much"/>
</div>
<div class="lyrico-lyrics-wrapper">Smoking too much
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smoking too much"/>
</div>
<div class="lyrico-lyrics-wrapper">I got my head twisted all over me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got my head twisted all over me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadava Arangetri Pada Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadava Arangetri Pada Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaargal Koodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaargal Koodavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Pottu Thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Pottu Thedavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil Puthithaana Thozhane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Puthithaana Thozhane "/>
</div>
<div class="lyrico-lyrics-wrapper">Pugalkoorum Seedane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugalkoorum Seedane "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vaa Vaa Dheerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vaa Vaa Dheerane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathinai Maatrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathinai Maatrada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhchiyai Yetrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhchiyai Yetrada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraigalai Neekada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraigalai Neekada"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thdaigalai Thookki Potu Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thdaigalai Thookki Potu Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalukkul Neruppada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukkul Neruppada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvugal Kothipada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvugal Kothipada"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vidhi Ezhuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vidhi Ezhuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Yea"/>
</div>
<div class="lyrico-lyrics-wrapper">Puratchi Seithu Katta Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratchi Seithu Katta Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadava Arangetri Pada Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadava Arangetri Pada Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaargal Koodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaargal Koodavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Pottu Thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Pottu Thedavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil Puthithaana Thozhane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Puthithaana Thozhane "/>
</div>
<div class="lyrico-lyrics-wrapper">Pugalkoorum Seedane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugalkoorum Seedane "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vaa Vaa Dheerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vaa Vaa Dheerane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theenda Vaa Ennai Thottu Thoondavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda Vaa Ennai Thottu Thoondavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thannai Thaandavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thannai Thaandavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Aanai Aandava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Aanai Aandava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulu Moga Thoothuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulu Moga Thoothuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Jothi Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Jothi Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi Indri Solla Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Indri Solla Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithanai Vizhikka Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai Vizhikka Vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivinai Thuvaithu Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivinai Thuvaithu Vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavinai Jeikavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinai Jeikavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanathai Thozhilil Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanathai Thozhilil Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinai Perukivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinai Perukivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarvinil Paninthu Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarvinil Paninthu Vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyai Nilaikka Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Nilaikka Vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathai Thirumbi Parka Vai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai Thirumbi Parka Vai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthi enbathe sakthi enbathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthi enbathe sakthi enbathai"/>
</div>
<div class="lyrico-lyrics-wrapper">katru kollada en nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katru kollada en nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">pakthi enbathai tholilil vaithu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakthi enbathai tholilil vaithu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham vetri than en anba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham vetri than en anba"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu puthukural thirukural thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu puthukural thirukural thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai purintha pin thadai ethu munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai purintha pin thadai ethu munne"/>
</div>
<div class="lyrico-lyrics-wrapper">nee porupinai etru puthu pani aatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee porupinai etru puthu pani aatru"/>
</div>
<div class="lyrico-lyrics-wrapper">poga vendum mele muneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga vendum mele muneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
</pre>
