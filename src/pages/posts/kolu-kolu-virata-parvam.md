---
title: "kolu kolu song lyrics"
album: "Virata Parvam"
artist: "Suresh Bobbili"
lyricist: "Chandrabose"
director: "Venu Udugula"
path: "/albums/virata-parvam-lyrics"
song: "Kolu Kolu"
image: ../../images/albumart/virata-parvam.jpg
date: 2022-06-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qZxWRuKGn2U"
type: "love"
singers:
  -	Divya Mallika
  - Suresh Bobbili
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolu koloo koloyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu koloo koloyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommaa chivarana poolu poose kolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommaa chivarana poolu poose kolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulaanti sinnadhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulaanti sinnadhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Moggayindhi sigguthoti koloyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moggayindhi sigguthoti koloyamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolu kolamma kolo kolo naa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu kolamma kolo kolo naa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase melukoni choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase melukoni choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalo nindina vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo nindina vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaa mundhara unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaa mundhara unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu nidhura raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu nidhura raadhule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolu kolamma kolo kolo naa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu kolamma kolo kolo naa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase melukoni choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase melukoni choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalo nindina vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo nindina vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaa mundhara unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaa mundhara unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu nidhura raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu nidhura raadhule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pillagaadi maatalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pillagaadi maatalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaajulalle maarchukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaajulalle maarchukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali dhooli bottu pettukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali dhooli bottu pettukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurravaadi choopulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurravaadi choopulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Koppulona muduchukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppulona muduchukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Allarantha nallapoosalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allarantha nallapoosalanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi goorchi aalochane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi goorchi aalochane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadiponi aaraadhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadiponi aaraadhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali laaga mello vaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali laaga mello vaaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolu kolamma kolo kolo naa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu kolamma kolo kolo naa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase melukoni choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase melukoni choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalo nindina vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo nindina vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaa mundhara unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaa mundhara unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu nidhura raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu nidhura raadhule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhamemo vaadidhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhamemo vaadidhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanamemo naadhi anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanamemo naadhi anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi pedhavi thoti navvuthuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi pedhavi thoti navvuthuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aksharaalu vaadivanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aksharaalu vaadivanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardhamantha nenu anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardhamantha nenu anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi gonthu thoti palukuthuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi gonthu thoti palukuthuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanamanthaa vaadenantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamanthaa vaadenantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Praayamanthaa vaadenantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praayamanthaa vaadenantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi premai nenu brathakanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi premai nenu brathakanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolu kolamma kolo kolo naa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolu kolamma kolo kolo naa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase melukoni choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase melukoni choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalo nindina vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo nindina vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaa mundhara unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaa mundhara unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu nidhura raadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu nidhura raadhule"/>
</div>
</pre>
