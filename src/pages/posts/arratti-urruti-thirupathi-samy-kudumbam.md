---
title: "arratti urruti song lyrics"
album: "Thirupathi Samy Kudumbam"
artist: "Sam D Raj"
lyricist: "Sorkko"
director: "Suresh Shanmugam"
path: "/albums/thirupathi-samy-kudumbam-lyrics"
song: "Arratti Urruti"
image: ../../images/albumart/thirupathi-samy-kudumbam.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HLnG1_ShwQw"
type: "happy"
singers:
  - Guru
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thusta sakthigala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thusta sakthigala "/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi sathiduvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi sathiduvar"/>
</div>
<div class="lyrico-lyrics-wrapper">kasta kalathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasta kalathula "/>
</div>
<div class="lyrico-lyrics-wrapper">kadathi kathiduvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadathi kathiduvar"/>
</div>
<div class="lyrico-lyrics-wrapper">yanaiyin vahanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yanaiyin vahanam "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthiraiyin vaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthiraiyin vaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">rendilum eruvar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendilum eruvar "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathai maruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathai maruvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chinna chinna anukkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna anukkal"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda ulagai miratuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda ulagai miratuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">siriya pullum puyalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siriya pullum puyalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thandi nimirthu nikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandi nimirthu nikume"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga marukum ilaigar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga marukum ilaigar"/>
</div>
<div class="lyrico-lyrics-wrapper">kootam jalli kattu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam jalli kattu da"/>
</div>
<div class="lyrico-lyrics-wrapper">thaduthu paru thadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaduthu paru thadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">udaiyum nerupu aruvi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiyum nerupu aruvi da"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttu thorkum varikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttu thorkum varikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyal velicham elupum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal velicham elupum"/>
</div>
<div class="lyrico-lyrics-wrapper">siriya theni kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siriya theni kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">padaiyai virati adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaiyai virati adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">engalin regai than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalin regai than "/>
</div>
<div class="lyrico-lyrics-wrapper">thanga regai yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga regai yai"/>
</div>
<div class="lyrico-lyrics-wrapper">thotathum marume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotathum marume "/>
</div>
<div class="lyrico-lyrics-wrapper">vettri vaagaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettri vaagaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vagra pralaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagra pralaya"/>
</div>
<div class="lyrico-lyrics-wrapper">magya mahirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magya mahirana"/>
</div>
<div class="lyrico-lyrics-wrapper">samyathirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samyathirana"/>
</div>
<div class="lyrico-lyrics-wrapper">agnee saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agnee saala"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana na than na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana na than na "/>
</div>
<div class="lyrico-lyrics-wrapper">than na thanana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than na thanana "/>
</div>
<div class="lyrico-lyrics-wrapper">thana thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thanana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana na than na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana na than na "/>
</div>
<div class="lyrico-lyrics-wrapper">than na thanana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than na thanana "/>
</div>
<div class="lyrico-lyrics-wrapper">thana thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">othungi pogum makalukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi pogum makalukum"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram iruku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram iruku da"/>
</div>
<div class="lyrico-lyrics-wrapper">pathungi payum mirugaithaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathungi payum mirugaithaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">egiri adikum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egiri adikum da"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya mozhiyum puthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya mozhiyum puthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhiyum namaku ethuku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyum namaku ethuku da"/>
</div>
<div class="lyrico-lyrics-wrapper">mela kaiya vachu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela kaiya vachu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">mocham uruthi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mocham uruthi da"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaitha seyalgal yarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaitha seyalgal yarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nesama vasama nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesama vasama nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanatha irutai virati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanatha irutai virati "/>
</div>
<div class="lyrico-lyrics-wrapper">anaithum nalamai nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithum nalamai nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula thillu da neril nillu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula thillu da neril nillu da"/>
</div>
<div class="lyrico-lyrics-wrapper">miratala urutala ethirthu vellu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miratala urutala ethirthu vellu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vagra pralaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagra pralaya"/>
</div>
<div class="lyrico-lyrics-wrapper">magya mahirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magya mahirana"/>
</div>
<div class="lyrico-lyrics-wrapper">samyathirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samyathirana"/>
</div>
<div class="lyrico-lyrics-wrapper">agnee saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agnee saala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">araka paraka ulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araka paraka ulaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">makala kakkum samingo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makala kakkum samingo"/>
</div>
<div class="lyrico-lyrics-wrapper">aratty uritti mirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aratty uritti mirati "/>
</div>
<div class="lyrico-lyrics-wrapper">viratum ayyanarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratum ayyanarungo"/>
</div>
</pre>
