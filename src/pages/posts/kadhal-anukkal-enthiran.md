---
title: "kadhal anukkal udambil song lyrics"
album: "Enthiran"
artist: "A.R. Rahman"
lyricist: "Vairamuthu"
director: "S. Shankar"
path: "/albums/enthiran-lyrics"
song: "Kadhal Anukkal Udambil"
image: ../../images/albumart/enthiran.jpg
date: 2010-07-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gpVA5mx73UE"
type: "Love"
singers:
  - Vijay Prakash
  - Shreya Ghosal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal Anukkal Udambil Eththanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Anukkal Udambil Eththanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neutron Electron Un Neela Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neutron Electron Un Neela Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththam Eththanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththam Eththanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ninaithaal Dhisukkal Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaithaal Dhisukkal Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Sindhanai Aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Sindhanai Aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanaa Sanaa Orey Vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanaa Sanaa Orey Vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Moththam Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Moththam Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Newton Newtonin Vidhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Newton Newtonin Vidhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Nesam Nesam Edhir Vinaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Nesam Nesam Edhir Vinaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aayiram Vinmeen Thirattiya Punnagaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aayiram Vinmeen Thirattiya Punnagaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Moththam Neeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Moththam Neeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mutrum Ariviyal Piththan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mutrum Ariviyal Piththan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal Mutham Ketpathil Jithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Mutham Ketpathil Jithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Dheem Dhom Dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Dheem Dhom Dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem Dhom Dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Dhom Dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem Dhom Dhom Manadhil Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Dhom Dhom Manadhil Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean Thean Idhazhil Yutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean Thean Idhazhil Yutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja Poovil Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poovil Raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem Dhom Dhom Manadhil Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Dhom Dhom Manadhil Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaam Poochi Pattaam Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaam Poochi Pattaam Poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgalai Kondu Thaan Rusi Ariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalai Kondu Thaan Rusi Ariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kollum Manidha Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kollum Manidha Poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai Konduthaan Rusi Ariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Konduthaan Rusi Ariyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odugira Thanniyil Thanniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odugira Thanniyil Thanniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oxygen Miga Athigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen Miga Athigam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugira Manasukkul Manasukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugira Manasukkul Manasukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal Miga Athigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Miga Athigam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiye Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiye Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Kadhalai Aindhey Nodiyil Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kadhalai Aindhey Nodiyil Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesam Valarkka Oru Neram Odhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam Valarkka Oru Neram Odhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nenjam Veengi Vittathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjam Veengi Vittathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Idaiyai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Idaiyai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Pizhaippil Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Pizhaippil Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalin Neramum Ilaithuvittathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Neramum Ilaithuvittathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Anukkal Udambil Eththanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Anukkal Udambil Eththanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neutron Electron Un Gaandha Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neutron Electron Un Gaandha Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththam Eththanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththam Eththanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ninaithaal Dhisukkal Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaithaal Dhisukkal Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Sindhanai Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Sindhanai Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanaa Sanaa Orey Vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanaa Sanaa Orey Vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Moththam Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Moththam Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Newton Newtonin Vidhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Newton Newtonin Vidhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Nesam Nesam Edhir Vinaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Nesam Nesam Edhir Vinaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aayiram Vinmeen Thirattiya Punnagaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aayiram Vinmeen Thirattiya Punnagaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Moththam Neeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Moththam Neeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthenil Ossabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthenil Ossabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Oh Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oh Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathil Muththa Gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathil Muththa Gulabi"/>
</div>
</pre>
