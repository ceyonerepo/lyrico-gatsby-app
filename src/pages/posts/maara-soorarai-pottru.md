---
title: "maara song lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "Arivu"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Maara"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-11-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tXE1ipZKsGI"
type: "mass"
singers:
  - Suriya
  - G V Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Parundhaaguthu oor kuruvi
<input type="checkbox" class="lyrico-select-lyric-line" value="Parundhaaguthu oor kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanangadhathu en piravi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanangadhathu en piravi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanga pala madangavuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Adanga pala madangavuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadutha adha odachi varuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadutha adha odachi varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ippa vandhu modhu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippa vandhu modhu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandhu paaru daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta vandhu paaru daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattarundha kaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattarundha kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju mela yera podhu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenju mela yera podhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thimirudaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimirudaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimira thimira nimuru daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimira thimira nimuru daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilama nilama unaru daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilama nilama unaru daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam payanam thodaru daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Payanam payanam thodaru daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaa ippa naanum vera daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaa ippa naanum vera daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandhu paaru daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta vandhu paaru daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru daa……………..
<input type="checkbox" class="lyrico-select-lyric-line" value="Paaru daa…………….."/>
</div>
</pre>
