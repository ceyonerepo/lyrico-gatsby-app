---
title: "yengirundhai nee song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Nadhi Vijaykumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Yengirundhai Nee"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9lFMgz9cSbo"
type: "love"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engirundhaai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirundhaai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Engiruppaai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engiruppaai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En seivaai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seivaai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam padaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam padaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyum padaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyum padaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalai iyakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalai iyakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir padaithaaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir padaithaaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae padaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae padaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae koduththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae koduththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae eduththaai serithaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae eduththaai serithaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai yen padaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai yen padaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakken koduththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakken koduththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen nee eduththaai solvaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen nee eduththaai solvaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer thuli polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer thuli polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjinil niraindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjinil niraindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Irai neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irai neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananam maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananam maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum nigazhlum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum nigazhlum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyai yenoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyai yenoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee padaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee padaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyenae naan ariyenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyenae naan ariyenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullathinaalae unarnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullathinaalae unarnthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandha vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandha vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Per aanandham endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per aanandham endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal vizhuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal vizhuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thozhuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thozhuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal vizhuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal vizhuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thozhuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thozhuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arulaal naan ezhuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arulaal naan ezhuvenae"/>
</div>
</pre>
