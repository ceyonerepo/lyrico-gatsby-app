---
title: 'Deerane song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Deerane'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E8m57zr0ks0"
type: 'Love'
singers: 
- Ramya Behara
- Deepu
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ohnana ohnana ohnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohnana ohnana ohnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohnaa naan senthenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohnaa naan senthenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohnana ohnana ohnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohnana ohnana ohnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nee vandu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nee vandu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharathil oru venmathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharathil oru venmathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai azhaithenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai azhaithenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhira logathu sundari unakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhira logathu sundari unakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaai muzhaithenaaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaai muzhaithenaaa…aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerane ulagam undhan keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerane ulagam undhan keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deerane nee ninaithaalae…ae… ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deerane nee ninaithaalae…ae… ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkama.. asathiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkama.. asathiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En madiyeru naan paduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En madiyeru naan paduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai vazhi nadathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai vazhi nadathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga naan maaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga naan maaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigalai thagarthindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai thagarthindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaigalai nagarthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaigalai nagarthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkindren unai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkindren unai kaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhum indha aruviyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum indha aruviyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadaiyanin sadaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadaiyanin sadaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ingae ethirkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ingae ethirkindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un poo mugam athai kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un poo mugam athai kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha boomi rendaaga naan pizhapenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomi rendaaga naan pizhapenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerane ulagam undhan keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerane ulagam undhan keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deerane nee ninaithaalae…ae… ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deerane nee ninaithaalae…ae… ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaramaai muzhaithu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaramaai muzhaithu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varum antha varam ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varum antha varam ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigarangal thulaithu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigarangal thulaithu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi meedhu vizhi vaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi meedhu vizhi vaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vijitharipurudhiradhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijitharipurudhiradhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalithara shikharaka thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalithara shikharaka thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulakutharathilitha gambheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulakutharathilitha gambheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayaviraat veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayaviraat veera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayagaganathala bheekara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayagaganathala bheekara"/>
</div>
<div class="lyrico-lyrics-wrapper">Garjjadhara gara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garjjadhara gara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhaya rasa kaasara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhaya rasa kaasara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijitha madhu paara paara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijitha madhu paara paara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhayagaramshav vibhavasindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayagaramshav vibhavasindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suparadhangam bharanarandhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suparadhangam bharanarandhi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayagaramshav vibhavasindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayagaramshav vibhavasindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suparadhangam bharanarandhi.. ((3) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suparadhangam bharanarandhi.. ((3) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerane ulagam undhan keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerane ulagam undhan keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deerane nee ninaithaalae…ae… ((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deerane nee ninaithaalae…ae… ((4) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorenae enadhu meni ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorenae enadhu meni ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaranae un viral yengae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaranae un viral yengae"/>
</div>
</pre>