---
title: "nee yenadharuginil nee song lyrics"
album: "Oh Manapenne"
artist: "Vishal Chandrashekhar"
lyricist: "Niranjan Bharathi"
director: "Kaarthikk Sundar"
path: "/albums/oh-manapenne-lyrics"
song: "Nee Yenadharuginil Nee"
image: ../../images/albumart/oh-manapenne.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_oxYLalWx4s"
type: "love"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee yenadharuginil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yenadharuginil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai vidaa ooru kavithayai kidaiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai vidaa ooru kavithayai kidaiyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yenadharuginil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yenadharuginil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai vidaa ooru punithamum irukkaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai vidaa ooru punithamum irukkaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattril poo poola nenjaam koothaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattril poo poola nenjaam koothaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankaal paakaatha vekkam pandaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankaal paakaatha vekkam pandaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu varain theendaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu varain theendaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru imbam kaai neettuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru imbam kaai neettuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava nijama idhu irandum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava nijama idhu irandum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaa araigindra thedalkal thevai thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaa araigindra thedalkal thevai thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilaa mazhaiya ithu vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilaa mazhaiya ithu vaanavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai anaikindra aagaayam naane naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai anaikindra aagaayam naane naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal paanidum paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal paanidum paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjaam kekkindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaam kekkindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada ooru vidha mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada ooru vidha mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannoraam pookkindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoraam pookkindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pokaathathu saakaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokaathathu saakaathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu yen yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu yen yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodaathathu vaazhaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodaathathu vaazhaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennosu unn vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennosu unn vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuvarai unaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvarai unaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvondru uravaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvondru uravaanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava nijama idhu irandum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava nijama idhu irandum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaa araigindra thedalkal thevai thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaa araigindra thedalkal thevai thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilaa mazhaiya ithu vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilaa mazhaiya ithu vaanavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai anaikindra aagaayam naane naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai anaikindra aagaayam naane naana"/>
</div>
</pre>
