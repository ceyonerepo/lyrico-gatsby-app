---
title: "ay pilla song lyrics"
album: "Love Story"
artist: "Pawan Ch"
lyricist: "Chaithanya Pingali"
director: "Sekhar Kammula"
path: "/albums/love-story-lyrics"
song: "Ay Pilla"
image: ../../images/albumart/love-story.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/my4yAvsiLqM"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ay Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ay Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruguna Podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruguna Podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Vaipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Vaipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantaga Undamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantaga Undamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanche Dunki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanche Dunki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka Chaka Urukutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka Chaka Urukutu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Rangula Villuni Teesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rangula Villuni Teesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Vaipu Vanthena Vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vaipu Vanthena Vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enno Talapulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno Talapulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evo Kalathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo Kalathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Batuke Poravu Tunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batuke Poravu Tunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaallo Patangi Malle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaallo Patangi Malle"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegire Kalale Navi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegire Kalale Navi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Niraasala Uyyaalatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Niraasala Uyyaalatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddu Maapula Madhye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddu Maapula Madhye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakantu Undinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakantu Undinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Undanthaa Ika Neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undanthaa Ika Neeke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Beruku Lekundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Beruku Lekundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Iga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Iga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Batuku Antunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Batuku Antunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ninna Nedu Repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ninna Nedu Repu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurchi Neekai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurchi Neekai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichane Talagadagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichane Talagadagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Talanu Vaalchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Talanu Vaalchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Terichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Terichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Duniya Milamila Chude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Duniya Milamila Chude"/>
</div>
<div class="lyrico-lyrics-wrapper">Vache Malupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vache Malupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rastha Velugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rastha Velugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaare Chinukula Jalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaare Chinukula Jalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduguu Pekaaa Malle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduguu Pekaaa Malle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nannu Alle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nannu Alle"/>
</div>
<div class="lyrico-lyrics-wrapper">Podde Teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podde Teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Galli Poduguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galli Poduguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aade Pillala Hore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aade Pillala Hore"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakantu Undinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakantu Undinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Undantha Ika Neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undantha Ika Neeke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ay Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ay Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruguna Podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruguna Podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Vaipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Vaipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantaga Undamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantaga Undamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paare Nadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paare Nadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalalu Unnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalalu Unnaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chere Dare Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chere Dare Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedukutunnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedukutunnaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gunde Oli Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gunde Oli Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aachi Tuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aachi Tuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andinchaa Jaatarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andinchaa Jaatarala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kshanamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kshanamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chathi Paina Soli Chusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chathi Paina Soli Chusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Merupula Jaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Merupula Jaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningina Mabbulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningina Mabbulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iche Bahumati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iche Bahumati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelana Kanipistunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelana Kanipistunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Maare Needalu Geese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maare Needalu Geese"/>
</div>
<div class="lyrico-lyrics-wrapper">Tele Bommalu Chude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tele Bommalu Chude"/>
</div>
<div class="lyrico-lyrics-wrapper">Patnam Cherina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patnam Cherina"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala Punthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala Punthalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallela Santhala Baare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallela Santhala Baare"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakantu Undinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakantu Undinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Undantha Ika Neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undantha Ika Neeke"/>
</div>
</pre>
