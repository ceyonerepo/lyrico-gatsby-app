---
title: "rangule song lyrics"
album: "Rang De"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/rang-de-lyrics"
song: "Rangule - Naa Kallalo Kotha"
image: ../../images/albumart/rang-de.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/pnrmT7PbYE4"
type: "love"
singers:
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha neelirangu pongene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha neelirangu pongene"/>
</div>
<div class="lyrico-lyrics-wrapper">Avi ninnu choosinappude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avi ninnu choosinappude"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa chempalo kottha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa chempalo kottha"/>
</div>
<div class="lyrico-lyrics-wrapper">Erupu rangu puttene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erupu rangu puttene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nannu choosinappude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nannu choosinappude"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvelle dhaarantha..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelle dhaarantha.."/>
</div>
<div class="lyrico-lyrics-wrapper">Pachha rangesinattundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachha rangesinattundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vente nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vente nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaalake pasupu poosindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaalake pasupu poosindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu choodananni kotha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu choodananni kotha rangule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu choodananni kotha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu choodananni kotha rangule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O navvu nuvvu visiraavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O navvu nuvvu visiraavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kshanam rangu telupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kshanam rangu telupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kaatukishtam annaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kaatukishtam annaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa poota rangu nalupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa poota rangu nalupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chethi sparshe thaakindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethi sparshe thaakindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa onti rangu chengaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa onti rangu chengaavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mouname o mullaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mouname o mullaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paluku rangu gulabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paluku rangu gulabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamaade rangele edaadhikosaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamaade rangele edaadhikosaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ee holile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ee holile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishaanikosaari nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishaanikosaari nee valle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu choodananni kottha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu choodananni kottha rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu choodananni kottha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu choodananni kottha rangule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu palakarinche prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu palakarinche prathisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pulakarinthadhi ye rango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pulakarinthadhi ye rango"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee meppu pondhe prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee meppu pondhe prathisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa goppathanamudhi ye rango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa goppathanamudhi ye rango"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu kopaginchi samayamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kopaginchi samayamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa bujjagimpudhe rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa bujjagimpudhe rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu vidichi velle velallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu vidichi velle velallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa edhana vedhanedhi ye rango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa edhana vedhanedhi ye rango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hariville aa yedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hariville aa yedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulni dhinchadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulni dhinchadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manase nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manase nee valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevela rangulni vedhajalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevela rangulni vedhajalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeru lenivenno kottha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeru lenivenno kottha rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pakkanunte rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pakkanunte rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu lenivenno kottha rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu lenivenno kottha rangule"/>
</div>
</pre>
