---
title: "theme of kolaigaran song lyrics"
album: "Kolaigaran"
artist: "Simon K. King"
lyricist: "Dhamayanthi"
director: "Andrew Louis"
path: "/albums/kolaigaran-song-lyrics"
song: "Theme Of Kolaigaran"
image: ../../images/albumart/kolaigaran.jpg
date: 2019-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/59ZfhZjGPRU"
type: "theme"
singers:
  - Shilvi Sharon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoo Hooo Ooo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hooo Ooo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hooo Oooo Oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hooo Oooo Oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kolaigaaran Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kolaigaaran Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kolaigaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kolaigaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai Gaaaranaannnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai Gaaaranaannnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kolaigaaran Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kolaigaaran Hmm Mmm"/>
</div>
</pre>
