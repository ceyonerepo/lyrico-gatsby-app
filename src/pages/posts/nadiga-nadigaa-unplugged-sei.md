---
title: "nadiga nadigaa unplugged song lyrics"
album: "Sei"
artist: "NyX Lopez"
lyricist: "Madhan Karky"
director: "Raj Babu"
path: "/albums/sei-song-lyrics"
song: "Nadiga Nadigaa Unplugged"
image: ../../images/albumart/sei.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dFhwu80VNWU"
type: "love"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nadiga nadigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiga nadigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiga nadigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiga nadigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu idhazhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu idhazhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann ayarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann ayarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru rasigai pol thooram nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru rasigai pol thooram nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naalum poosithae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naalum poosithae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai chottu chottu chottaai raskkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai chottu chottu chottaai raskkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai vaithu vazhkai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vaithu vazhkai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyakkida yosithae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyakkida yosithae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meedhu kaadhal kondu kidakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meedhu kaadhal kondu kidakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppanaigal anindhdhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppanaigal anindhdhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanaiyil seidha ulagam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanaiyil seidha ulagam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhdha pin than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhdha pin than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaigal kalaindhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaigal kalaindhdhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirvaanamaai indru sirikkiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvaanamaai indru sirikkiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiga nadigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiga nadigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiga nadigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiga nadigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu idhazhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu idhazhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann ayarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann ayarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engengeyo odi alaindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengeyo odi alaindha"/>
</div>
<div class="lyrico-lyrics-wrapper">En varudangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En varudangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan paartha pulliyil kuvindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan paartha pulliyil kuvindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaedhilo naan thediya inimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaedhilo naan thediya inimaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan sorkalil kidaithadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan sorkalil kidaithadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhimaatra vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhimaatra vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhimaatra vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhimaatra vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai en manadhai chamaithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai en manadhai chamaithaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyil keetru thandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil keetru thandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir kaatru thandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir kaatru thandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvai kaadhalaal seeramaithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvai kaadhalaal seeramaithaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppanaigal anindhdhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppanaigal anindhdhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanaiyil seidha ulagam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanaiyil seidha ulagam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhdha pin than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhdha pin than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaigal kalaindhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaigal kalaindhdhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirvaanamaai indru sirikkiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvaanamaai indru sirikkiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaa aaa aaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaa aaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaaaa aa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaaaa aa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai engum mulladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai engum mulladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kaadhal thaanae nimmadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kaadhal thaanae nimmadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae sagiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar varaindha punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar varaindha punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kankaldhaanae thoorigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kankaldhaanae thoorigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaigeezhaai kidandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaigeezhaai kidandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam rasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam rasithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathin ennam neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathin ennam neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollikkoduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollikkoduthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavidhaigal puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaigal puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikkaamal irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikkaamal irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal naan unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal naan unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruu purindhukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruu purindhukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppanaigal anindhdhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppanaigal anindhdhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanaiyil seidha ulagam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanaiyil seidha ulagam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhdha pin than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhdha pin than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaigal kalaindhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaigal kalaindhdhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirvaanamaai indru sirikkiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvaanamaai indru sirikkiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyaa mozhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyaa mozhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanil ennai chiraipidithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanil ennai chiraipidithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudipaa sirippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipaa sirippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanil ennai kollaiyadithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanil ennai kollaiyadithaai"/>
</div>
</pre>