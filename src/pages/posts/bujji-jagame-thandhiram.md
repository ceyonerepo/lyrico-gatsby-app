---
title: "bujji song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Bujji"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2020-07-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7Y6GrgSJnNQ"
type: "love"
singers:
  - Anirudh Ravichander
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
    <div class="lyrico-lyrics-wrapper">Enna mattum love you pannu bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum love you pannu bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mattum darling sollu bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum darling sollu bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mattum killi vaiyi bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum killi vaiyi bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mattum follow pannu bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum follow pannu bujji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Silovettu scene-u moonu ya
<input type="checkbox" class="lyrico-select-lyric-line" value="Silovettu scene-u moonu ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vutta kaadhal mania
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu vutta kaadhal mania"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkae correct-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkae correct-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Connect-ah vanthuttaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Connect-ah vanthuttaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu vachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali kattum place-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaali kattum place-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pair adicha
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma pair adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla adhaan soora maasu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorukulla adhaan soora maasu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hu hu hu hooo.. (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hu hu hu hooo.."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna mattum sweet-ah paaru bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum sweet-ah paaru bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mattum huggy pannu bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum huggy pannu bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna konji shy-ah aakku bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna konji shy-ah aakku bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mattum world-ah maathu bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mattum world-ah maathu bujji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karuppatti potta jaggery
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppatti potta jaggery"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thotta gold-u robbery
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai thotta gold-u robbery"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku correct-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku correct-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Connect-ah vanthuttaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Connect-ah vanthuttaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu vachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali kattum place-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaali kattum place-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pair adicha
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma pair adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla adhaan soora maasu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorukulla adhaan soora maasu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu vachi……
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu vachi……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu vachi……
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu vachi……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pah style-u-na avaloda style-u thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pah style-u-na avaloda style-u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava munna azhagula edhuvumae fail-u thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava munna azhagula edhuvumae fail-u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pah smile-u-na avaloda smile-u thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pah smile-u-na avaloda smile-u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamudi odhukkuvaa adhula naan jail-u thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalamudi odhukkuvaa adhula naan jail-u thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Babe un dance-u area-la fans-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Babe un dance-u area-la fans-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Babe un glance-u miss world-u chance-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Babe un glance-u miss world-u chance-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey en rose-u meet panna romance-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey en rose-u meet panna romance-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey en rose-u meet panlana un dreams-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey en rose-u meet panlana un dreams-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Izhukkura idikkura chemistry enga padikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Izhukkura idikkura chemistry enga padikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiss adichu udambukkulla sugar-ah yethura
<input type="checkbox" class="lyrico-select-lyric-line" value="Kiss adichu udambukkulla sugar-ah yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Veralula verala naan tag-u panna thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Veralula verala naan tag-u panna thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukkuda edam vittu angry yethura
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukkuda edam vittu angry yethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Where-u bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Where-u bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji"/>
</div>
<div class="lyrico-lyrics-wrapper">Where-u where-u bujji
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u where-u bujji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu vachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali kattum place-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaali kattum place-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pair adicha
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma pair adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla adhaan soora maasu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorukulla adhaan soora maasu (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu bujji….
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu bujji…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Where-u bujji naalu bujji….
<input type="checkbox" class="lyrico-select-lyric-line" value="Where-u bujji naalu bujji…."/>
</div>
<div class="lyrico-lyrics-wrapper">Hey where-u bujji naalu bujji….
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey where-u bujji naalu bujji…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai mattum..hmmmm hmm hmmm wyunng wyunggg
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai mattum..hmmmm hmm hmmm wyunng wyunggg"/>
</div>
</pre>
