---
title: "kalavarame song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "Madhan Karky"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Kalavarame"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JMh8FiJsZJw"
type: "love"
singers:
  - Pradeep Kumar
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaaaaaaoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaaaaaaoh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaramae kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaramae kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilumae unthan mugam varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilumae unthan mugam varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagalae siruppalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagalae siruppalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa thiruda vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa thiruda vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaramae aasai kanimaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaramae aasai kanimaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanilumae adhuvae nilavaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanilumae adhuvae nilavaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal korkka uyir serkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal korkka uyir serkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa inaiya vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa inaiya vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa…idhu thirumanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa…idhu thirumanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendammae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendammae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh kalacharam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh kalacharam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaramae kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaramae kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilumae unthan mugam varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilumae unthan mugam varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagalae siruppalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagalae siruppalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa thiruda vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa thiruda vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavariyum enthan kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavariyum enthan kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu pesi vidathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu pesi vidathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai vida paavam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai vida paavam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil ethuvumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil ethuvumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavugal vaanil thondrin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavugal vaanil thondrin"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidum vinmeen yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidum vinmeen yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginai muzhudhum kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginai muzhudhum kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenmam podhavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam podhavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaathollai illaadha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaathollai illaadha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai illaadha kaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai illaadha kaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai thedi chendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennai thedi chendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa aaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa aaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaahaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaahaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilai marai kaayaai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai marai kaayaai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalai adhu ennil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalai adhu ennil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabalangal ellaam sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabalangal ellaam sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kuzhandhai endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kuzhandhai endraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham madam naanam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham madam naanam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Michammena yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michammena yedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucham thoda naanum sendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucham thoda naanum sendren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandae viyandhu nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandae viyandhu nindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidhar unarndhu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhar unarndhu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidha kaadhal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidha kaadhal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai thaandi chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai thaandi chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravae varuvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae varuvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaramae aasai kanimaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaramae aasai kanimaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanilumae adhuvae nilavaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanilumae adhuvae nilavaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal korkka uyir serkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal korkka uyir serkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa inaiya vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa inaiya vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa…idhu thirumanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa…idhu thirumanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendammae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendammae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh kalacharam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh kalacharam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmm"/>
</div>
</pre>
