---
title: "atchi putchi song lyrics"
album: "Sketch"
artist: "S. Thaman"
lyricist: "Vijay Chandar"
director: "Vijay Chandar"
path: "/albums/sketch-lyrics"
song: "Atchi Putchi"
image: ../../images/albumart/sketch.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nq8GTPavdm0"
type: "happy"
singers:
  - Vijay Chandar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Atchi Putchi Kathukkitchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchi Putchi Kathukkitchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaa Gibaar Tatchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaa Gibaar Tatchey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nezaar Bazaar Malaai Kuska
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nezaar Bazaar Malaai Kuska"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Mitta Sketchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Mitta Sketchey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So So Sokkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So So Sokkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Girkal Vita Breaku Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girkal Vita Breaku Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kailasam Poi Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailasam Poi Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Romba Sharpuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Romba Sharpuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaar Uddum Pullaikellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaar Uddum Pullaikellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Paatha Alluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Paatha Alluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Poondhu Paathaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Poondhu Paathaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Manasu Goldu Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Manasu Goldu Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Ottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Ottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Othungi Nikkara Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othungi Nikkara Aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Dhool Parandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Dhool Parandhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salphat Mala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salphat Mala"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhu Nikkara Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhu Nikkara Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Geth Ah Goal Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geth Ah Goal Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Coola Nikkum Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coola Nikkum Aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edu Edu Edu Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Edu Edu Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Edu Edu Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Edu Edu Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi Mittav Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi Mittav Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dawlat Vandi Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dawlat Vandi Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Sketcha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Thookkuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi Mittav Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi Mittav Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dawlat Vandi Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dawlat Vandi Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Sketcha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Thookkuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So So Sokkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So So Sokkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Girkal Vita Breaku Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girkal Vita Breaku Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kailasam Poi Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailasam Poi Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Romba Sharpuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Romba Sharpuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sketchu Sketchu Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Sketchu Sketchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Sketchu Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Sketchu Sketchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Sketchu Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Sketchu Sketchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pisiru Illa Sketchey Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisiru Illa Sketchey Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kitta Aal Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kitta Aal Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Olinji Olari Peela Utta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinji Olari Peela Utta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirattalaana Ride Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirattalaana Ride Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sentiment Ah Scene Ah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sentiment Ah Scene Ah Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevullu Mela Pura Udu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevullu Mela Pura Udu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunda Kanji Sumaar Moonji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunda Kanji Sumaar Moonji"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaranalum Kalaaichidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaranalum Kalaaichidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atchi Putchi Atchi Putchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchi Putchi Atchi Putchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atchi Putchi Atchi Putchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchi Putchi Atchi Putchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atchi Putchi Atchi Putchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchi Putchi Atchi Putchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi Hoi Hoi Hoi Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atchi Putchi Kathukkitchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchi Putchi Kathukkitchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaa Gibaar Ttatchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaa Gibaar Ttatchey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nezaar Bazaar Malai Kuska
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nezaar Bazaar Malai Kuska"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Mitta Sketchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Mitta Sketchey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jallikatta Thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikatta Thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellakaran Pottan Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakaran Pottan Sketchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukku Tamilnadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku Tamilnadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottathu Paar Skectchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottathu Paar Skectchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sithava Thooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithava Thooka"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavanandhan Pottan Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavanandhan Pottan Sketchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavanananukku Baanathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavanananukku Baanathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottan Raman Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottan Raman Sketchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rowdy Dha Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdy Dha Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Revolveru Keedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolveru Keedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutta Scene Aayidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Scene Aayidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheko Saala Godhavulla Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheko Saala Godhavulla Kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathi Dhoosu Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathi Dhoosu Thattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaraana Aaluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaraana Aaluu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edu Edu Edu Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Edu Edu Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Edu Edu Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Edu Edu Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi Mittav Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi Mittav Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dawlat Vandi Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dawlat Vandi Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Sketcha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Thookkuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gilbert Vandi Mittav Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilbert Vandi Mittav Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dawlat Vandi Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dawlat Vandi Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Sketcha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu Thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu Thookkuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So So Sokkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So So Sokkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Girkal Vita Breaku Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girkal Vita Breaku Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kailasam Poi Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kailasam Poi Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Romba Sharpuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Romba Sharpuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaar Uddum Pullaikellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaar Uddum Pullaikellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Paatha Alluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Paatha Alluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Poondhu Paathaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Poondhu Paathaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchu Manasu Goldu Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchu Manasu Goldu Ma"/>
</div>
</pre>
