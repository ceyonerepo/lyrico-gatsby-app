---
title: "ennena seidhom ingu song lyrics"
album: "Mayakkam Enna"
artist: "G.V. Prakash Kumar"
lyricist: "Selvaraghavan"
director: "Selvaraghavan"
path: "/albums/mayakkam-enna-lyrics"
song: "Ennena Seidhom Ingu"
image: ../../images/albumart/mayakkam-enna.jpg
date: 2011-11-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/abccWuyuJqA"
type: "melody"
singers:
  - Harish Raghavendra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennena Seithom Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennena Seithom Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuvarai Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvarai Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengu Ponom Vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengu Ponom Vandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithi Ennum Perilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithi Ennum Perilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanatha Thuyaram Kannilaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanatha Thuyaram Kannilaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyatha Salanam Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyatha Salanam Nenjilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Enniyathu Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Enniyathu Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi Vanthathum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi Vanthathum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannathiyil Salanam Velluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannathiyil Salanam Velluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaana Punnaigai Seivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana Punnaigai Seivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Paarvayil Kolvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Paarvayil Kolvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enbathu Naan Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enbathu Naan Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Solgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaaga Irupavan Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaga Irupavan Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneerai Thudaipavan Poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai Thudaipavan Poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjilae Unai Vaanginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjilae Unai Vaanginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Serkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Serkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyin Porulthaan Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyin Porulthaan Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthuthaan Paathaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthuthaan Paathaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathai Solgiraai Bayam Kolgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathai Solgiraai Bayam Kolgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Sooriyanin Aathikama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Sooriyanin Aathikama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Paravaigalum Bothikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Paravaigalum Bothikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Sooriyanin Aathikama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Sooriyanin Aathikama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Paravaigalum Bothikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Paravaigalum Bothikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unathu Arasaangam Perun Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu Arasaangam Perun Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Athilae Oru Siru Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Athilae Oru Siru Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Annaithukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Annaithukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Marugi Nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Marugi Nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudum Theeyum Sugamaai Theendidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudum Theeyum Sugamaai Theendidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Enniyathu Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Enniyathu Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi Vanthathum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi Vanthathum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannathiyil Salanam Velluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannathiyil Salanam Velluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullirukkum Unnai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirukkum Unnai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyamal Alaivoar Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyamal Alaivoar Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaraiya Nee Kadal Alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvaraiya Nee Kadal Alaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaigal Yerivarum Oru Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaigal Yerivarum Oru Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathiyil Moozhgi Ezhum Perum Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathiyil Moozhgi Ezhum Perum Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaigal Yerivarum Oru Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaigal Yerivarum Oru Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathiyil Moozhgi Ezhum Perum Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathiyil Moozhgi Ezhum Perum Kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennil Kadavul Yaar Thedugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Kadavul Yaar Thedugirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyai Avarin Pin Odugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyai Avarin Pin Odugirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Paarka Vaitha Kallai Pesa Vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Paarka Vaitha Kallai Pesa Vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Perunthaayin Karunai Marakirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perunthaayin Karunai Marakirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Neram Enniyathu Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Neram Enniyathu Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi Vanthathum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi Vanthathum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannathiyil Salanam Velluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannathiyil Salanam Velluma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaana Punnaigai Seivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana Punnaigai Seivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Paarvayil Kolvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Paarvayil Kolvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enbathu Naan Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enbathu Naan Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Solgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaaga Irupavan Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaga Irupavan Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneerai Thudaipavan Poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai Thudaipavan Poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjilae Unai Vaanginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjilae Unai Vaanginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Serkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Serkiraai"/>
</div>
</pre>
