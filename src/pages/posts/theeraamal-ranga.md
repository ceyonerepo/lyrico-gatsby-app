---
title: "theeraamal song lyrics"
album: "Ranga"
artist: "Ramjeevan"
lyricist: "Karthik Netha"
director: "Vinod DL"
path: "/albums/ranga-lyrics"
song: "Theeraamal"
image: ../../images/albumart/ranga.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_JKNje8kmmk"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thodarmazhaieinil Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarmazhaieinil Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Mazhai Kaatrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Mazhai Kaatrey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Paarkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Puzhudhiyil Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Puzhudhiyil Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamoru Murai Thoori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamoru Murai Thoori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovanamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovanamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Theeramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Theeramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaindha Theruvil Poogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindha Theruvil Poogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Theeramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Theeramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba Thirumba Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba Thirumba Vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paartha Nee Dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paartha Nee Dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyandhu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyandhu Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodar Mazhaiyinil Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodar Mazhaiyinil Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Mazhai Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Mazhai Kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Paarkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Puzhudhiyil Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Puzhudhiyil Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamoru Murai Thoori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamoru Murai Thoori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovanamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovanamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrin Kaambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrin Kaambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum Poovindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum Poovindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum Kadhai Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Kadhai Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Earkindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Earkindrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerampoga Koondhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerampoga Koondhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyode Arugil Vandhaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyode Arugil Vandhaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Silikkindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silikkindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesadhha Varthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesadhha Varthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Koorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Koorudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaladha Maayaikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaladha Maayaikul"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Serkkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Serkkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Theeramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Theeramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaindha Theruvil Poogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindha Theruvil Poogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalaiaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaiaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Theeramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Theeramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba Thirumba Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba Thirumba Vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paartha Nee Dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paartha Nee Dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyandhu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyandhu Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodar Mazhaiyinil Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodar Mazhaiyinil Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Mazhai Kaatrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Mazhai Kaatrey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Paarkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Puzhudhiyil Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Puzhudhiyil Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamoru Murai Thoori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamoru Murai Thoori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovanamaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovanamaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeramal Theeramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramal Theeramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaindha Theruvil Poogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindha Theruvil Poogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Meedhu Thol Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Meedhu Thol Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalaiaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaiaagiren"/>
</div>
</pre>
