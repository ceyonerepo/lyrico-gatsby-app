---
title: "kadhal en kadhal song lyrics"
album: "Mayakkam Enna"
artist: "G.V. Prakash Kumar"
lyricist: "Selvaraghavan - Dhanush"
director: "Selvaraghavan"
path: "/albums/mayakkam-enna-lyrics"
song: "Kadhal En Kadhal"
image: ../../images/albumart/mayakkam-enna.jpg
date: 2011-11-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oSQUlW29E-s"
type: "sad"
singers:
  - Dhanush
  - Selvaraghavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal En Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Kanneerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Kanneerula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pochu Adhu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pochu Adhu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thanneerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thanneerula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Machi Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Machi Udra"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Ennai Paada Ududa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Ennai Paada Ududa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paadiyae Theeruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paadiyae Theeruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Paadi Thola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Paadi Thola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal En Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Kanneerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Kanneerula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pochu Adhu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pochu Adhu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thanneerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thanneerula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Pudhu Kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Pudhu Kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhaana Nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhaana Nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Venneerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Venneerula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida Avala Udhaida Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida Avala Udhaida Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidra Avala Thevaiyae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidra Avala Thevaiyae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Purila Ulagam Therila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Purila Ulagam Therila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariya Varala Onnumae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariya Varala Onnumae Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Suthuthu Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Suthuthu Suthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyum Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyum Suthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppunnu Adicha Beerunila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppunnu Adicha Beerunila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthukka Paduthukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthukka Paduthukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae Thelinjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae Thelinjidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaila Adikkira Morinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaila Adikkira Morinilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Sonniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Sonniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhula Vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhula Vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Soupula Thenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soupula Thenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Thaan Thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Thaan Thaangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dream Ellaam Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dream Ellaam Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Acid Oothitta Kannukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acid Oothitta Kannukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban Azhuvura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban Azhuvura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtama Iirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtama Iirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kooda Ava Worthae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kooda Ava Worthae Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenooruna Nenjukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenooruna Nenjukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallooruthae Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallooruthae Enna Solla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Oh Padagirukku Valai Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Padagirukku Valai Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalukkulla Meena Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalukkulla Meena Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennaanda Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennaanda Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kaadhal Mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaadhal Mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnunga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vaazhvin Saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vaazhvin Saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaadi Poyi Naa Kanden Nyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaadi Poyi Naa Kanden Nyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaachu Saami Enakkithuvae Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaachu Saami Enakkithuvae Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida Avala Udhaida Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida Avala Udhaida Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidra Avala Thevaiyae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidra Avala Thevaiyae Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maan Vizhi Thaen Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan Vizhi Thaen Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kili Naan Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kili Naan Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhali Kaadhali En Figure Kannagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhali Kaadhali En Figure Kannagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friends-U Kooda Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friends-U Kooda Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkanum Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkanum Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Vanthutta Romba Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Vanthutta Romba Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Suttava Uruppada Maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suttava Uruppada Maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thavira Enakkonnum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thavira Enakkonnum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kanavirukku Kalare Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kanavirukku Kalare Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam Paakkuren Kadhaiyae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam Paakkuren Kadhaiyae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambirukku Uyirae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambirukku Uyirae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravirukku Peyare Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravirukku Peyare Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaamda Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaamda Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kadhal Mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kadhal Mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnunga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vaazhvin Saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vaazhvin Saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaadi Poyi Naa Kanden Nyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaadi Poyi Naa Kanden Nyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaachu Saami Podhum Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaachu Saami Podhum Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Da Avala Udhaida Avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Da Avala Udhaida Avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidra Avala Thevaiyae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidra Avala Thevaiyae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Purila Ulagam Therila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Purila Ulagam Therila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariya Varala Onnumae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariya Varala Onnumae Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Suthuthu Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Suthuthu Suthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyum Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyum Suthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppunnu Adicha Beerunila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppunnu Adicha Beerunila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthukka Paduthukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthukka Paduthukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae Thelinjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae Thelinjidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaila Adikkira Morinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaila Adikkira Morinilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan Heygudnight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Heygudnight"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahogudnight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahogudnight"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeiigudnight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeiigudnight"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanku So Much Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanku So Much Machi"/>
</div>
</pre>
