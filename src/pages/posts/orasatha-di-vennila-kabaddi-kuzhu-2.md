---
title: "orasatha di song lyrics"
album: "Vennila Kabaddi Kuzhu 2"
artist: "V. Selvaganesh"
lyricist: "Vijayasagar"
director: "Selva Sekaran"
path: "/albums/vennila-kabaddi-kuzhu-2-lyrics"
song: "Orasatha Di"
image: ../../images/albumart/vennila-kabaddi-kuzhu-2.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tMSL95fRgq0"
type: "love"
singers:
  - Karthik
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orasathadi Orasadhadi Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasathadi Orasadhadi Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambukula Sollama Kollama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambukula Sollama Kollama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasu Vedikudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu Vedikudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orasadhada Orasadhada Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasadhada Orasadhada Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkula Unkuda Thanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkula Unkuda Thanala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri Nadakudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri Nadakudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikura Siripula Kadaincha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikura Siripula Kadaincha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Moraikura Moraipula Udancha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Moraikura Moraipula Udancha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinunkala Polambala Keta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinunkala Polambala Keta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pitham Muthidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pitham Muthidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Mutham Kodukirenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Mutham Kodukirenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethira Ninnu Pakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethira Ninnu Pakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethira Ninnu Pakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethira Ninnu Pakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethula Enna Thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethula Enna Thakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethula Enna Thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethula Enna Thakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethuku Onnu Kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuku Onnu Kekura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuku Onnu Kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuku Onnu Kekura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varavu Selava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Selava"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavu Selava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Selava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaporu Senju Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaporu Senju Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkaporil Vecha Theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaporil Vecha Theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikichida Pazhum Manasu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichida Pazhum Manasu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanni Oothi Unna Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni Oothi Unna Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharalama Nenaikurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharalama Nenaikurendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pathu Thaan Enna Nenaicha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pathu Thaan Enna Nenaicha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enna Anaikura Anaipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Anaikura Anaipula"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Anainja Usurum Kulirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Anainja Usurum Kulirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unna Nenaikura Nenaipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Nenaikura Nenaipula"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjai Pura Nerambithan Vazhiyanumdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Pura Nerambithan Vazhiyanumdi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varanji Vachi Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varanji Vachi Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Varanji Vachi Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varanji Vachi Paakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranji Ninnu Thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranji Ninnu Thakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranji Ninnu Thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranji Ninnu Thakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korainja Patcham Kekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korainja Patcham Kekkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Korainja Patcham Kekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korainja Patcham Kekkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varavu Selava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Selava"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavu Selava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Selava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orasadhadi Orasadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasadhadi Orasadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasadhadi Orasadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasadhadi Orasadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadiya Jollikum Meyni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiya Jollikum Meyni"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi Naan Ninna Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Naan Ninna Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Thakki Than Kannu Kosumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Thakki Than Kannu Kosumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeneramum Pal Ilichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeneramum Pal Ilichu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pinnadi Nee Alanjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pinnadi Nee Alanjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Kannu Mookku Than Vechu Pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Kannu Mookku Than Vechu Pesuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Velaiyae Illa Di Ooruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Velaiyae Illa Di Ooruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Vetiya Pesi Thirium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Vetiya Pesi Thirium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolaiyae Illai Da Unnaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyae Illai Da Unnaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kali Mannai Thinna Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali Mannai Thinna Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Marachu Than Thinna Venum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachu Than Thinna Venum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukula Pakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukula Pakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukula Pakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukula Pakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunthu Mutham Kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunthu Mutham Kekura"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunthu Mutham Kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunthu Mutham Kekura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maliva Enna Saikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maliva Enna Saikura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maliva Enna Saikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maliva Enna Saikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhiku Pazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhiku Pazhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhiku Pazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhiku Pazhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orasadhadi Orasadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasadhadi Orasadhadi"/>
</div>
</pre>
