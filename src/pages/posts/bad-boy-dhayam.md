---
title: "bad boy song lyrics"
album: "Dhayam"
artist: "Sathish Selvam"
lyricist: "Kavidhai Gundar"
director: "Kannan Rangaswamy"
path: "/albums/dhayam-lyrics"
song: "Bad Boy"
image: ../../images/albumart/dhayam.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UJuJGcPY3pM"
type: "mass"
singers:
  - Emcee Jesz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bad Bad Bad Boy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Bad Bad Boy "/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Bad Bad Boy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Bad Bad Boy "/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Bad Bad Boy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Bad Bad Boy "/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Bad Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Bad Bad Boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Am The Defination Of A Bad Boy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am The Defination Of A Bad Boy "/>
</div>
<div class="lyrico-lyrics-wrapper">Ask Them Bad Boys 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ask Them Bad Boys "/>
</div>
<div class="lyrico-lyrics-wrapper">Get To Know Me Pretty Boy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get To Know Me Pretty Boy "/>
</div>
<div class="lyrico-lyrics-wrapper">Looks Like A Situation 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looks Like A Situation "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No More Time Duration 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No More Time Duration "/>
</div>
<div class="lyrico-lyrics-wrapper">Me Let Me Take Them Actions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Me Let Me Take Them Actions"/>
</div>
<div class="lyrico-lyrics-wrapper">My Eyes Be Kazhugu Watching You 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Eyes Be Kazhugu Watching You "/>
</div>
<div class="lyrico-lyrics-wrapper">Kothi Thooka Poaren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothi Thooka Poaren "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What You Gonna Do 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What You Gonna Do "/>
</div>
<div class="lyrico-lyrics-wrapper">Kedu Keatta Mirugam Naanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedu Keatta Mirugam Naanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Eduthual Yethayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Eduthual Yethayum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vetai Aduven Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetai Aduven Paaru "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Pola Sharpu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Pola Sharpu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaa Villain 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaa Villain "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaegaadha Paruppu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaegaadha Paruppu "/>
</div>
<div class="lyrico-lyrics-wrapper">Oththi Neeyum Annanthu Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththi Neeyum Annanthu Paaru "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeman Moonji Thaeriyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeman Moonji Thaeriyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Inimaela Unakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaela Unakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetta Neram Vidiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetta Neram Vidiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappi Oda Mudiyadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappi Oda Mudiyadhu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vella Mudiyadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vella Mudiyadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kettavan Konda Arasan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kettavan Konda Arasan "/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Punniyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Punniyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeriyadhavan Kanakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeriyadhavan Kanakku "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha I Told You Baby 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha I Told You Baby "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paavam Punniyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paavam Punniyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaadha Arakan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaadha Arakan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dangerous So Dangerous 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous So Dangerous "/>
</div>
<div class="lyrico-lyrics-wrapper">Dangerous So Dangerous 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous So Dangerous "/>
</div>
<div class="lyrico-lyrics-wrapper">Dangerous So Dangerous 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous So Dangerous "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dangerous Notorious 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous Notorious "/>
</div>
<div class="lyrico-lyrics-wrapper">Most Time I Be So Fabulous 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Most Time I Be So Fabulous "/>
</div>
<div class="lyrico-lyrics-wrapper">Some Of Them Hate Me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Some Of Them Hate Me "/>
</div>
<div class="lyrico-lyrics-wrapper">But Still Number One Champion 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Still Number One Champion "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Arimugam Unakku Unn 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Arimugam Unakku Unn "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Yezhuththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Yezhuththu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thodangida Illaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thodangida Illaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Vilakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhayam Varugudhaa Nadungudhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayam Varugudhaa Nadungudhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Padharudha Manasudhan Thudiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharudha Manasudhan Thudiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkudhu Idhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkudhu Idhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Aadukalam Vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Aadukalam Vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu Mothida Balam Irukka Ne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Mothida Balam Irukka Ne "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjadha Singatha Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjadha Singatha Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ravanan Ennoda Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravanan Ennoda Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan King'u Naan Gethu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan King'u Naan Gethu "/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyum Vedikkaiya Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyum Vedikkaiya Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Scene Nu Semma Scene Nu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Scene Nu Semma Scene Nu "/>
</div>
<div class="lyrico-lyrics-wrapper">Soorai Aada Poaraen Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorai Aada Poaraen Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan King'u Naan Gethu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan King'u Naan Gethu "/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyum Vedikkaiya Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyum Vedikkaiya Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Scene Nu Semma Scene Nu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Scene Nu Semma Scene Nu "/>
</div>
<div class="lyrico-lyrics-wrapper">Soorai Aada Poaraen Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorai Aada Poaraen Paaru"/>
</div>
</pre>
