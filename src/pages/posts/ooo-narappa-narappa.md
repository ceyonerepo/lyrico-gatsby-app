---
title: "ooo narappa song lyrics"
album: "Narappa"
artist: "Mani Sharma"
lyricist: "Ananta Sriram"
director: "Srikanth Addala"
path: "/albums/narappa-lyrics"
song: "Ooo Narappa"
image: ../../images/albumart/narappa.jpg
date: 2021-07-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wIKt4WzSmjg"
type: "happy"
singers:
  - Dhanunjay
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooo Narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante ittagundhe narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante ittagundhe narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu chudangane iparindhoy naa reppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu chudangane iparindhoy naa reppa"/>
</div>
<div class="lyrico-lyrics-wrapper">O kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kanti reppai kaasukunta kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kanti reppai kaasukunta kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jantai anti pettukunta ee janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jantai anti pettukunta ee janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eneyvaa itta itta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eneyvaa itta itta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde emantuntadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde emantuntadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Eneyle aashala chitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eneyle aashala chitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa erukey em avundadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa erukey em avundadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhalega bagundhe silakaa nee maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhalega bagundhe silakaa nee maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakay aadinche pilakay ayipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakay aadinche pilakay ayipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalake norure eluge manapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalake norure eluge manapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthundadhe hoyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthundadhe hoyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo Narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante ittagundhe narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante ittagundhe narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu chudangane iparindhoy naa reppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu chudangane iparindhoy naa reppa"/>
</div>
<div class="lyrico-lyrics-wrapper">O kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kanti reppai kaasukunta kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kanti reppai kaasukunta kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jantai anti pettukunta ee janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jantai anti pettukunta ee janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ballari santhakelli o 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ballari santhakelli o "/>
</div>
<div class="lyrico-lyrics-wrapper">basedu pallelu tena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="basedu pallelu tena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallilu nuvu testhe mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallilu nuvu testhe mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pranale badhulichheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pranale badhulichheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhiri gulloki kalisi podama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhiri gulloki kalisi podama"/>
</div>
<div class="lyrico-lyrics-wrapper">Madaka siralona manuvadeddhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madaka siralona manuvadeddhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruvu kaannunchi sakkani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruvu kaannunchi sakkani "/>
</div>
<div class="lyrico-lyrics-wrapper">gudise manamesukundhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gudise manamesukundhama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo Narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvante ittagundhe narappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvante ittagundhe narappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu chudangane iparindhoy naa reppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu chudangane iparindhoy naa reppa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kanti reppai kaasukunta kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kanti reppai kaasukunta kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jantai anti pettukunta ee janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jantai anti pettukunta ee janma"/>
</div>
</pre>
