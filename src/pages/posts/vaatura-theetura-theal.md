---
title: "vaatura theetura song lyrics"
album: "Theal"
artist: "C. Sathya"
lyricist: "Lavarthan"
director: "Harikumar"
path: "/albums/theal-lyrics"
song: "Vaatura Theetura"
image: ../../images/albumart/theal.jpg
date: 2022-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XaVfIp29Cgw"
type: "happy"
singers:
  - V.M. Mahalingam
  - Meenakshi Elayaraja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaatura theetura kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaatura theetura kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seekiran thuthiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seekiran thuthiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">maatuna katura enakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatuna katura enakulla"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu kathala mooturaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu kathala mooturaya"/>
</div>
<div class="lyrico-lyrics-wrapper">malaya kotura manasa thatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaya kotura manasa thatura"/>
</div>
<div class="lyrico-lyrics-wrapper">verasa muttura nenjula nee ottura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verasa muttura nenjula nee ottura"/>
</div>
<div class="lyrico-lyrics-wrapper">orala kuthura urava nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orala kuthura urava nikura"/>
</div>
<div class="lyrico-lyrics-wrapper">kurava kokkula maati enna sikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurava kokkula maati enna sikura"/>
</div>
<div class="lyrico-lyrics-wrapper">un kedukula ulla alati melutura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kedukula ulla alati melutura"/>
</div>
<div class="lyrico-lyrics-wrapper">enna surutu pola neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna surutu pola neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mella urunji ilukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella urunji ilukura"/>
</div>
<div class="lyrico-lyrics-wrapper">seetu edukura enna tip adikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seetu edukura enna tip adikura"/>
</div>
<div class="lyrico-lyrics-wrapper">vadicha thanni soodu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadicha thanni soodu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">aavi parakura yen muraikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavi parakura yen muraikira"/>
</div>
<div class="lyrico-lyrics-wrapper">murandu pidikura irutti anachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murandu pidikura irutti anachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannathula umma umma kudukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannathula umma umma kudukura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muratha pola pudaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muratha pola pudaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">suratha pola adikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suratha pola adikira"/>
</div>
<div class="lyrico-lyrics-wrapper">uratha potu valatha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uratha potu valatha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">veena yendi viratura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena yendi viratura"/>
</div>
<div class="lyrico-lyrics-wrapper">kitta vantha kanaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta vantha kanaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">etti ninna konaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti ninna konaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">vattam potu ullukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vattam potu ullukula"/>
</div>
<div class="lyrico-lyrics-wrapper">kottam neyum adikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottam neyum adikira"/>
</div>
<div class="lyrico-lyrics-wrapper">keera pola aanju enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keera pola aanju enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanju pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanju pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">aara pola valanju nelunju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara pola valanju nelunju"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oodura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oodura"/>
</div>
<div class="lyrico-lyrics-wrapper">vandi potu kedaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandi potu kedaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasa nondi aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasa nondi aakura"/>
</div>
<div class="lyrico-lyrics-wrapper">konji pesi thavikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji pesi thavikum"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa panchar aakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa panchar aakura"/>
</div>
<div class="lyrico-lyrics-wrapper">yendi parakura usura edukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendi parakura usura edukura"/>
</div>
<div class="lyrico-lyrics-wrapper">iruki anachu kannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruki anachu kannathula"/>
</div>
<div class="lyrico-lyrics-wrapper">umma umma umma kudukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umma umma umma kudukura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuchi mittai color la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuchi mittai color la"/>
</div>
<div class="lyrico-lyrics-wrapper">vala vala nu irukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala vala nu irukura"/>
</div>
<div class="lyrico-lyrics-wrapper">uchi kotti enna thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi kotti enna thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">maiya vachu kidakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maiya vachu kidakura"/>
</div>
<div class="lyrico-lyrics-wrapper">vidala padu paduthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidala padu paduthura"/>
</div>
<div class="lyrico-lyrics-wrapper">anala pola koluthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anala pola koluthura"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal pottu kedakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal pottu kedakum "/>
</div>
<div class="lyrico-lyrics-wrapper">enna solalu pola ilukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solalu pola ilukura"/>
</div>
<div class="lyrico-lyrics-wrapper">thotti vanthu ullukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotti vanthu ullukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">paaya virikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaya virikira"/>
</div>
<div class="lyrico-lyrics-wrapper">petti pola enna yendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petti pola enna yendi"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum mulikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum mulikira"/>
</div>
<div class="lyrico-lyrics-wrapper">mutta pota kozhi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutta pota kozhi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unna poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali kattu nanum un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali kattu nanum un"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda vaaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda vaaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">yendi parakura usura edukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendi parakura usura edukura"/>
</div>
<div class="lyrico-lyrics-wrapper">iruki anachu kannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruki anachu kannathula"/>
</div>
<div class="lyrico-lyrics-wrapper">umma umma umma kudukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umma umma umma kudukura"/>
</div>
</pre>
