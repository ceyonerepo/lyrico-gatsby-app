---
title: "rise of shyam song lyrics"
album: "Shyam Singha Roy"
artist: "Mickey J. Meyer"
lyricist: "Krishna Kanth"
director: "Rahul Sankrityan"
path: "/albums/shyam-singha-roy-lyrics"
song: "Rise Of Shyam"
image: ../../images/albumart/shyam-singha-roy.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KcZl788ZOps"
type: "mass"
singers:
  - Anurag Kulkarni
  - Vishal Dadlani
  - Cizzy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puttindha o aksharame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttindha o aksharame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kagithapu kadupu cheelche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kagithapu kadupu cheelche"/>
</div>
<div class="lyrico-lyrics-wrapper">Anyayam thale tenche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anyayam thale tenche"/>
</div>
<div class="lyrico-lyrics-wrapper">Are karavaalamlaa padhuna kalamera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are karavaalamlaa padhuna kalamera"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey egisi padu alajadi vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey egisi padu alajadi vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thiragabadina sangramam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thiragabadina sangramam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey venukanadani chaitanyam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey venukanadani chaitanyam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha roy singgha roy singha roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha roy singgha roy singha roy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patasulne likisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patasulne likisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam kosam shraamisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam kosam shraamisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Janam kosam thapisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam kosam thapisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey agnananni paathara vesthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey agnananni paathara vesthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthu unna prathi poota paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthu unna prathi poota paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana netthuru sirala paareyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana netthuru sirala paareyraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Medalu vanche raoyulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medalu vanche raoyulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavi prashnal yuddhamera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavi prashnal yuddhamera"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhuram rangunna jandara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhuram rangunna jandara"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey egisi padu alajadi vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey egisi padu alajadi vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thiragabadina sangramam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thiragabadina sangramam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey venukanadani chaitanyam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey venukanadani chaitanyam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha roy singgha roy singha roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha roy singgha roy singha roy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey egisi padu alajadi vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey egisi padu alajadi vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thiragabadina sangramam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thiragabadina sangramam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey venukanadani chaitanyam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey venukanadani chaitanyam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha roy singgha roy singha roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha roy singgha roy singha roy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garjinche muddera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garjinche muddera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellodaina nallodaina theda ledhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellodaina nallodaina theda ledhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Swathantryam nee swapnamraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swathantryam nee swapnamraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye krodhalu udvegalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye krodhalu udvegalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnem cheyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnem cheyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudilo gadiulo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudilo gadiulo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sthree shakthiki inthati kastala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sthree shakthiki inthati kastala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalale thempe aa kaalikake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalale thempe aa kaalikake"/>
</div>
<div class="lyrico-lyrics-wrapper">Charabattuthu sankella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charabattuthu sankella"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle ee swechha sadhyam ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle ee swechha sadhyam ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey egisi padu alajadi vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey egisi padu alajadi vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thiragabadina sangramam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thiragabadina sangramam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey venukanadani chaitanyam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey venukanadani chaitanyam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha roy singgha roy singha roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha roy singgha roy singha roy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey egisi padu alajadi vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey egisi padu alajadi vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey thiragabadina sangramam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey thiragabadina sangramam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey venukanadani chaitanyam vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey venukanadani chaitanyam vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Shyam Singha Roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shyam Singha Roy"/>
</div>
<div class="lyrico-lyrics-wrapper">Singha roy singgha roy singha roy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singha roy singgha roy singha roy"/>
</div>
</pre>
