---
title: "evo evo kalale song lyrics"
album: "Love Story"
artist: "Pawan Ch"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Sekhar Kammula"
path: "/albums/love-story-lyrics"
song: "Evo Evo Kalale"
image: ../../images/albumart/love-story.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/U95K3FS5rEI"
type: "happy"
singers:
  - Jonita Gandhi
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evo evo kalale enno enno therale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo evo kalale enno enno therale"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni dhaati manas hey egirindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni dhaati manas hey egirindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne nene geliche kshanalive kanuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nene geliche kshanalive kanuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhalake adhupe ledhandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhalake adhupe ledhandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rampam thararampam thararampam edhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rampam thararampam thararampam edhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rampam thararampam thararampam kathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rampam thararampam thararampam kathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kottha kottha rekkalochinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kottha kottha rekkalochinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento gaganamlo thiriga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento gaganamlo thiriga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kottha kottha oopirandhinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kottha kottha oopirandhinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento thamakamlo muniga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento thamakamlo muniga"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalaki vachindhi vidudhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalaki vachindhi vidudhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde sadi paadindhi kila kila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde sadi paadindhi kila kila"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola thadi merisindhi mila mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola thadi merisindhi mila mila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanti thadi navvindhi gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanti thadi navvindhi gala gala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohinchaledhasale oogindhile manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchaledhasale oogindhile manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakulo ipude hey paduthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakulo ipude hey paduthondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Are are arere ila ela jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are are arere ila ela jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshame chinuke dhukindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshame chinuke dhukindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rampam thararampam thararampam edhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rampam thararampam thararampam edhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rampam thararampam thararampam kathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rampam thararampam thararampam kathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kallalona prema uttharalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kallalona prema uttharalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento asaleppudu kanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento asaleppudu kanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento gunde chaatu sittharalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento gunde chaatu sittharalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento edhureppudu avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento edhureppudu avale"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho ila okkokka kshanamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho ila okkokka kshanamuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Daacheyyana okkokka varamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daacheyyana okkokka varamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho ila okkokka ruthuvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho ila okkokka ruthuvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogeyyana okkokka guruthuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogeyyana okkokka guruthuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itu vaipo atu vaipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu vaipo atu vaipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Itu vaipu manake teliyani vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu vaipu manake teliyani vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepu vihariddham chalre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu vihariddham chalre"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento mounamantha moota vippinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento mounamantha moota vippinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento sarigamale paade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento sarigamale paade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento vaana villu gajje kattinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento vaana villu gajje kattinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento kathakaline aade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento kathakaline aade"/>
</div>
<div class="lyrico-lyrics-wrapper">Galloki visarali godugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galloki visarali godugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana swechaki veyyoddhu thodugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana swechaki veyyoddhu thodugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarihaddhulu dhaatali adugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarihaddhulu dhaatali adugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana joruki adharali pidugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana joruki adharali pidugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento allibilli haayi manthanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento allibilli haayi manthanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento mana madhyana jarige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento mana madhyana jarige"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento chinna chinna chilipi thandhanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento chinna chinna chilipi thandhanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento veyyinthalu perige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento veyyinthalu perige"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento aashalanni poosaguchhadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento aashalanni poosaguchhadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento mundhepudu ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento mundhepudu ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento dhyasa kuda dhaari thappadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento dhyasa kuda dhaari thappadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento gammatthuga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento gammatthuga undhe"/>
</div>
</pre>
