---
title: "idhudhaan en kadhai song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Kabilan"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Idhudhaan En Kadhai"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s1qDxoqRcmY"
type: "sad"
singers:
  - Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Thaan En Kadhal Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan En Kadhal Kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Mel Naan Vaazhum Varaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Mel Naan Vaazhum Varaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiyellam Moovarum Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiyellam Moovarum Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Neethan Idhayathin Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Neethan Idhayathin Raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin Azhage Idhai Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Azhage Idhai Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalai Vaazhum Nijam Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalai Vaazhum Nijam Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevi Kela Paadal Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi Kela Paadal Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Isaiyanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Isaiyanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevi Kela Paadal Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi Kela Paadal Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Isaiyanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Isaiyanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiye Naan Kanneeril Mazhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiye Naan Kanneeril Mazhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaiye Naan Vazhthal Pizhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaiye Naan Vazhthal Pizhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Palamai Vanthen Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Palamai Vanthen Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhu Varali Aanen Sagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhu Varali Aanen Sagiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyave Vanthen Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyave Vanthen Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatru Kathayai Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatru Kathayai Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhayin Paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhayin Paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Vazhi Thaan Iru Vazhi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vazhi Thaan Iru Vazhi Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Oru Vazhi Naan Oru Vazhi Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Oru Vazhi Naan Oru Vazhi Yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Siragin Paravai Parakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Siragin Paravai Parakkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuve En Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuve En Nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Thaan En Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan En Kadhai"/>
</div>
</pre>
