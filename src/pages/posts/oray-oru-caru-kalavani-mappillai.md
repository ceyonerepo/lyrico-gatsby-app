---
title: "oray oru caru song lyrics"
album: "Kalavani Mappillai"
artist: "NR Raghunanthan"
lyricist: "Eknaath"
director: "Gandhi Manivasagam"
path: "/albums/kalavani-mappillai-lyrics"
song: "Oray Oru Caru"
image: ../../images/albumart/kalavani-mappillai.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xU0RGvMM6Mc"
type: "happy"
singers:
  - Christopher Stanley
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orae oru car-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru car-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum athu thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum athu thaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottuvaru sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottuvaru sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soda bottle ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soda bottle ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli gundu-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli gundu-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kokki pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kokki pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikatha car-u unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikatha car-u unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vaada inga mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vaada inga mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thaan da poiyin ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thaan da poiyin ella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkapporu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkapporu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-ku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-ku nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiyar-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiyar-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora thaandi poguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora thaandi poguthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-um speed-ah oduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-um speed-ah oduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru solli nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru solli nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha galatta konjamum kuraiyalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha galatta konjamum kuraiyalada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora thaandi poguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora thaandi poguthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-um speed-ah oduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-um speed-ah oduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru solli nadakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru solli nadakuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha galatta konjamum kuraiyalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha galatta konjamum kuraiyalada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh car-eh vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh car-eh vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi nadakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi nadakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada car-u kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada car-u kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Comedy-um irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comedy-um irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-eh vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-eh vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi nadakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi nadakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada car-u kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada car-u kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Comedy-um irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comedy-um irukuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otturaan otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaan otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-eh nalla otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-eh nalla otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaan otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaan otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-eh nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-eh nalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyula otturan pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyula otturan pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ntha car-um tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ntha car-um tharumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kathaiya kelu nalla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kathaiya kelu nalla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kaadhal nenju kulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaadhal nenju kulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam kathuthu sound-u mella thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam kathuthu sound-u mella thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha car-um tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha car-um tharumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kathaiya kelu nalla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kathaiya kelu nalla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kaadhal nenju kulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaadhal nenju kulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam kathuthu sound-u mella thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam kathuthu sound-u mella thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kannu munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kannu munna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi onnu thonguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi onnu thonguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kaalam neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kaalam neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu kittu yenguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu kittu yenguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu munna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi onnu thonguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi onnu thonguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kaalam neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kaalam neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu kittu yenguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu kittu yenguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otturaan otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaan otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-eh nalla otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-eh nalla otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaan otturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaan otturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-eh nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-eh nalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyula otturan pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyula otturan pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae oru car-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru car-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum athu thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum athu thaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottuvaru sir-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottuvaru sir-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soda bottle ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soda bottle ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli gundu-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli gundu-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kokki pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kokki pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikatha car-u unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikatha car-u unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vaada inga mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vaada inga mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thaan da poiyin ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thaan da poiyin ella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkapporu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkapporu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-ku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-ku nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiyar-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiyar-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tham thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram tha tham thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram tha tham thaam"/>
</div>
</pre>
