---
title: "poi sonna posukiduven song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Madhan Karky"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Poi Sonna Posukiduven"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q_a4GZFEOs4"
type: "Sad"
singers:
  - Nikhita Gandhi
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaadhal kadai ondru solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal kadai ondru solven"/>
</div>
<div class="lyrico-lyrics-wrapper">kathaiyin mudivai nee solvaya?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiyin mudivai nee solvaya?"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisi moochu en marbil endrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisi moochu en marbil endrai"/>
</div>
<div class="lyrico-lyrics-wrapper">indro avalodu vanthu nindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indro avalodu vanthu nindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">muttal thanam inthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttal thanam inthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal alakum neral vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal alakum neral vanthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evalavu kaami?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalavu kaami?"/>
</div>
<div class="lyrico-lyrics-wrapper">poi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posukiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posukiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posukiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posukiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thola thola kadhal enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola kadhal enge?"/>
</div>
<div class="lyrico-lyrics-wrapper">thola nee thantha vaakum enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola nee thantha vaakum enge?"/>
</div>
<div class="lyrico-lyrics-wrapper">thola thola neeyo inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola neeyo inge"/>
</div>
<div class="lyrico-lyrics-wrapper">thola thola kadhal enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola kadhal enge?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muthangal pothatho?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthangal pothatho?"/>
</div>
<div class="lyrico-lyrics-wrapper">kelayo thola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelayo thola"/>
</div>
<div class="lyrico-lyrics-wrapper">parisai poliven muthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parisai poliven muthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum thottakkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum thottakkalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sollu sollu meiyai sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu sollu meiyai sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha poi sonnal posu posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha poi sonnal posu posukiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thola thola kadhal enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola kadhal enge?"/>
</div>
<div class="lyrico-lyrics-wrapper">thola nee thantha vaakum enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola nee thantha vaakum enge?"/>
</div>
<div class="lyrico-lyrics-wrapper">thola thola neeyo inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola neeyo inge"/>
</div>
<div class="lyrico-lyrics-wrapper">thola thola kadhal enge?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thola thola kadhal enge?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">innum unakai... en nenjam thudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum unakai... en nenjam thudika"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal unako avalai pidika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal unako avalai pidika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna kadhal kondu meendum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kadhal kondu meendum vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum... naan unai manippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum... naan unai manippen"/>
</div>
<div class="lyrico-lyrics-wrapper">netrin kaayam yavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrin kaayam yavum "/>
</div>
<div class="lyrico-lyrics-wrapper">indru naam marappoma vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru naam marappoma vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa kalgal pinni kolvom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa kalgal pinni kolvom vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naam ondrai otti kolvom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam ondrai otti kolvom vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un kadhal aalam enna kaataayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kadhal aalam enna kaataayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poi sonna...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi sonna..."/>
</div>
<div class="lyrico-lyrics-wrapper">posukiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="posukiduven"/>
</div>
</pre>
