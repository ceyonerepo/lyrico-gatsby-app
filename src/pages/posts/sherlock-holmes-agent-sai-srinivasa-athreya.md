---
title: "sherlock holmes song lyrics"
album: "Agent Sai Srinivasa Athreya"
artist: "Mark K Robin"
lyricist: "Krishnakanth"
director: "Swaroop RSJ"
path: "/albums/agent-sai-srinivasa-athreya-lyrics"
song: "Sherlock Holmes"
image: ../../images/albumart/agent-sai-srinivasa-athreya.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HrEtnOJ75CI"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sherlock Holmes Kay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sherlock Holmes Kay"/>
</div>
<div class="lyrico-lyrics-wrapper">Chayla Veedu Lay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chayla Veedu Lay"/>
</div>
<div class="lyrico-lyrics-wrapper">Global Style Kay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Global Style Kay"/>
</div>
<div class="lyrico-lyrics-wrapper">Copy Cat Lay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Copy Cat Lay"/>
</div>
<div class="lyrico-lyrics-wrapper">Buildup Ay Full Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buildup Ay Full Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Business Ay Dull Anthay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Business Ay Dull Anthay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutikay Lekuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutikay Lekuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Suit Tho Hungamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suit Tho Hungamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Face Ay Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face Ay Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Casey Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Casey Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero Gadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero Gadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cashey Undadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cashey Undadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fate Ay Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fate Ay Maaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hollywood La Attitudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hollywood La Attitudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brain Ay Choostey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brain Ay Choostey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Speedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Speedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Address Thappina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Address Thappina"/>
</div>
<div class="lyrico-lyrics-wrapper">Einstein Veedu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Einstein Veedu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Offer Antuney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Offer Antuney"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper Ad Isthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper Ad Isthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Loss Ayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Loss Ayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker Veeday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Veeday"/>
</div>
<div class="lyrico-lyrics-wrapper">Agent Anteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agent Anteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Patent Naadhantaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patent Naadhantaadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Topper Makeup Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topper Makeup Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Flop Avuthadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flop Avuthadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelladorala Vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelladorala Vesham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pillagade Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pillagade Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ay Desi Jasoos
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ay Desi Jasoos"/>
</div>
<div class="lyrico-lyrics-wrapper">Pseudo Shadow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pseudo Shadow"/>
</div>
<div class="lyrico-lyrics-wrapper">Face Ay Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face Ay Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Casey Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Casey Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero Gadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero Gadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cashey Undadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cashey Undadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fate Ay Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fate Ay Maaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hollywood La Attitudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hollywood La Attitudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brain Ay Choostey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brain Ay Choostey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Speedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Speedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Address Thappina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Address Thappina"/>
</div>
<div class="lyrico-lyrics-wrapper">Einstein Veedu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Einstein Veedu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh"/>
</div>
</pre>
