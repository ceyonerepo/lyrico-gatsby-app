---
title: "manusana manusan song lyrics"
album: "Sankarankovil"
artist: "Rajini"
lyricist: "Vivega"
director: "Palanivel Raja"
path: "/albums/sankarankovil-lyrics"
song: "Manusana Manusan"
image: ../../images/albumart/sankarankovil.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nPUz32p2u0U"
type: "motivational"
singers:
  - Rajini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manushana manushan saapuduraanda thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana manushan saapuduraanda thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu maaruvadheppo theeruvadheppu nammakkavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu maaruvadheppo theeruvadheppu nammakkavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushana manushan saapuduraanda thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana manushan saapuduraanda thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu maaruvadheppo theeruvadheppu nammakkavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu maaruvadheppo theeruvadheppu nammakkavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sappudum porulil kalappadam seiyum kaalam aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sappudum porulil kalappadam seiyum kaalam aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey saththiyam needhi gnaayam ellaam kaathaapoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey saththiyam needhi gnaayam ellaam kaathaapoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">erigira veettil pudungura koottam thambipayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erigira veettil pudungura koottam thambipayale"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppil eriyiravaraikkum eththanai aattam thambipayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppil eriyiravaraikkum eththanai aattam thambipayale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushana manushan saapuduraanda thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana manushan saapuduraanda thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu maaruvadheppo theeruvadheppu nammakkavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu maaruvadheppo theeruvadheppu nammakkavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambadhu Rooba lanjam kettaa siraiyil poattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambadhu Rooba lanjam kettaa siraiyil poattom"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaa ambadhu koadi adichavanukku Ottupoattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaa ambadhu koadi adichavanukku Ottupoattom"/>
</div>
<div class="lyrico-lyrics-wrapper">porandhu valarumvaraikkum pasanga ammaachellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porandhu valarumvaraikkum pasanga ammaachellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada seragu molaichaa avala anuppum mudhiyor illam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada seragu molaichaa avala anuppum mudhiyor illam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushana manushan saapuduraanda thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana manushan saapuduraanda thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu maaruvadheppo theeruvadheppu nammakkavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu maaruvadheppo theeruvadheppu nammakkavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai irundha ulagam vera thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai irundha ulagam vera thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">naama ippo vaazhum ulagam vera thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama ippo vaazhum ulagam vera thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaiya ulagam ilaignar kaiyil kedakkavenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaiya ulagam ilaignar kaiyil kedakkavenum"/>
</div>
<div class="lyrico-lyrics-wrapper">inga nadakkura kodumai ellaam muzhusaa maarida venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga nadakkura kodumai ellaam muzhusaa maarida venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushana manushan saapuduraanda thambippayaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushana manushan saapuduraanda thambippayaley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu maaruvadheppo theeruvadheppu nammakkavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu maaruvadheppo theeruvadheppu nammakkavale"/>
</div>
</pre>
