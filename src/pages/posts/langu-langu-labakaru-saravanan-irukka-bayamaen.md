---
title: "langu langu labakaru song lyrics"
album: "Saravanan Irukka Bayamaen"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/saravanan-irukka-bayamaen-lyrics"
song: "Langu Langu Labakaru"
image: ../../images/albumart/saravanan-irukka-bayamaen.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vFHEOG5qgSc"
type: "happy"
singers:
  -	Praniti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttiyila soru pongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttiyila soru pongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi vachcha korangu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi vachcha korangu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaatha enna vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaatha enna vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookkaruppen othungu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkaruppen othungu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattiyila soru ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattiyila soru ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakillaatha amukku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakillaatha amukku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettikatha pesayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettikatha pesayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Soriya vaikkum serangu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soriya vaikkum serangu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaiyila unna thechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaiyila unna thechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakkiduven chappathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkiduven chappathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyila unna veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyila unna veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Porichiduven raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porichiduven raasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi mutta kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi mutta kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kannu illa bun-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kannu illa bun-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Omelet-ah pottu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omelet-ah pottu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pova poren thinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pova poren thinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo ethukku potti nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo ethukku potti nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Panna venaam lootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna venaam lootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegathukkum aadinaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegathukkum aadinaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aava neeyum raatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aava neeyum raatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittru vitturu thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittru vitturu thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velanju thongura avara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velanju thongura avara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanji unna kozhambu vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanji unna kozhambu vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyumaa nee nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyumaa nee nimira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalu valantha pakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu valantha pakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arivu pochu makki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arivu pochu makki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhinju pona dowserula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhinju pona dowserula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa pochu kokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa pochu kokki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttiyila soru pongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttiyila soru pongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi vachcha korangu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi vachcha korangu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaatha enna vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaatha enna vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookkaruppen othungu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkaruppen othungu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattiyila soru ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattiyila soru ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakillaatha amukku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakillaatha amukku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettikatha pesayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettikatha pesayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Soriya vaikkum serangu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soriya vaikkum serangu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaiyila unna thechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaiyila unna thechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakkiduven chappathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkiduven chappathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyila unna veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyila unna veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Porichiduven raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porichiduven raasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Langu langu labakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langu langu labakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Otturaanga scooteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otturaanga scooteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dingu dingu dimmikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu dingu dimmikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthipaaru tractoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthipaaru tractoru"/>
</div>
</pre>
