---
title: "va kanakku song lyrics"
album: "Manmadha Leelai"
artist: "Premgi Amaren"
lyricist: "Gangai Amaren"
director: "Venkat Prabhu"
path: "/albums/manmadha-leelai-lyrics"
song: "Va Kanakku"
image: ../../images/albumart/manmadha-leelai.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/c5NM8C94Bro"
type: "love"
singers:
  - Ajay Krishnaa
  - Swagatha S Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Kanakku Paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kanakku Paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkam Ketkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkam Ketkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Therinji Theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Therinji Theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaiyum Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaiyum Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuku Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuku Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Sernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Sernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Kaarraanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Kaarraanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruku Yaeralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Yaeralam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduppom Tharalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduppom Tharalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koranja Seatharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koranja Seatharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraika Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraika Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theranthu Moodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theranthu Moodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu Odathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu Odathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Kanakku Paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kanakku Paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkaam Ketkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkaam Ketkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Nee Therinji Theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nee Therinji Theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaiyum Puriyama Puriyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaiyum Puriyama Puriyalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seraadha Shruthi Senthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraadha Shruthi Senthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalam Thappu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Thappu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalamnu Onnum Illiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalamnu Onnum Illiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevalum Aadhamum Kandathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevalum Aadhamum Kandathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Inga Nindrathendra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Inga Nindrathendra "/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu Valliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Valliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaraathathu Manmadhan Kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraathathu Manmadhan Kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan Porapathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan Porapathe "/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vagaieyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vagaieyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanathe Aagatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathe Aagatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaicha Yedathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaicha Yedathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku Yerakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Yerakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeduppom Tharalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduppom Tharalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koranja Seatharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koranja Seatharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraikka Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikka Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therantha Moodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therantha Moodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu Odathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu Odathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Kanakku Paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Kanakku Paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkam Ketkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkam Ketkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Nee Therinji Theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nee Therinji Theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaiyum Puriyama Puriyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaiyum Puriyama Puriyalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuku Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuku Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Sernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Sernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Kaaranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruku Yearalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Yearalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduppom Tharalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduppom Tharalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koranja Seatharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koranja Seatharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraikka Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikka Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therantha Moodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therantha Moodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu Odathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu Odathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keppoma"/>
</div>
</pre>
