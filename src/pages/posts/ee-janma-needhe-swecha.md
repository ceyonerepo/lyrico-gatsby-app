---
title: "ee janma needhe song lyrics"
album: "Swecha"
artist: "Bhole Shawali"
lyricist: "Ram Pydisetty"
director: "KPN Chawhan"
path: "/albums/swecha-lyrics"
song: "Ee Janma Needhe"
image: ../../images/albumart/swecha.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/5eNnrufqX-8"
type: "love"
singers:
  - Sai Madhavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ee Janma Needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janma Needhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukunna lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunna lope"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallo Kannire Kalipaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallo Kannire Kalipaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yadhalo Pralayaagni Nilipave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yadhalo Pralayaagni Nilipave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnati Prema Kathalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati Prema Kathalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalpithamele Anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpithamele Anukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Premisthe Vyadha Thappadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premisthe Vyadha Thappadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaro Ante NavvuKunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaro Ante NavvuKunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kathalo Nenunnani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kathalo Nenunnani"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedusthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Janma Needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janma Needhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukunna lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunna lope"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallo Kannire Kalipaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallo Kannire Kalipaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yadhalo Pralayaagni Nilipave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yadhalo Pralayaagni Nilipave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vishame Pusina Katthulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishame Pusina Katthulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Podichina Praanam Nilichede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podichina Praanam Nilichede"/>
</div>
<div class="lyrico-lyrics-wrapper">Shatruvula chusthunte Pranam Pothondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shatruvula chusthunte Pranam Pothondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeenu Kaalchu Nippulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeenu Kaalchu Nippulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Varshapu Chinukai Thadisede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varshapu Chinukai Thadisede"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramgaa Nanu Tharimesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramgaa Nanu Tharimesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithilaa Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithilaa Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho mayedho Nee Kannullo Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho mayedho Nee Kannullo Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Bhashaki Baavam Teliyaka Mundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Bhashaki Baavam Teliyaka Mundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Porabaduthu Manase neelo chikkukundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porabaduthu Manase neelo chikkukundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati Dhaka Naa Hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati Dhaka Naa Hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Manttu Egirindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Manttu Egirindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Kadhante Prathi udhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Kadhante Prathi udhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vila vilamantu Antundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vila vilamantu Antundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Majune Salim lu Edchindhendhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majune Salim lu Edchindhendhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Needey telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needey telise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Janma Needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janma Needhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukunna lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunna lope"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallo Kannire Kalipaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallo Kannire Kalipaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yadhalo Pralayaagni Nilipave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yadhalo Pralayaagni Nilipave"/>
</div>
</pre>
