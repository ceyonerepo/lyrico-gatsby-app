---
title: "sampaddhoy nanne song lyrics"
album: "Seven"
artist: "Chaitan Bharadwaj"
lyricist: "Subham Viswanadh"
director: "Nizar Shafi"
path: "/albums/seven-lyrics"
song: "Sampaddhoy Nanne"
image: ../../images/albumart/seven.jpg
date: 2019-06-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/w-ZamVaouso"
type: "love"
singers:
  - Madhushree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sampoddoy nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sampoddoy nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">meesamanti soodito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesamanti soodito"/>
</div>
<div class="lyrico-lyrics-wrapper">andagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Soododdoy nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soododdoy nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">kode eedu sooputo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kode eedu sooputo"/>
</div>
<div class="lyrico-lyrics-wrapper">tuntaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tuntaroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttupakkanunna vaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttupakkanunna vaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">saddu sette oppukoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saddu sette oppukoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Recchagottamaaku nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Recchagottamaaku nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantaa golenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantaa golenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maayalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maayalone"/>
</div>
<div class="lyrico-lyrics-wrapper">telaane nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="telaane nene"/>
</div>
<div class="lyrico-lyrics-wrapper">kougilintalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kougilintalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka ninnu nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka ninnu nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">iddarante piccholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iddarante piccholle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacchaa bottu laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacchaa bottu laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu antukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu antukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosukuntinoy daachukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosukuntinoy daachukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninni janmalo kumkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninni janmalo kumkum "/>
</div>
<div class="lyrico-lyrics-wrapper">bottulaa pettukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottulaa pettukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde gullo maate istunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde gullo maate istunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sampoddoy nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sampoddoy nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">meesamanti soodito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesamanti soodito"/>
</div>
<div class="lyrico-lyrics-wrapper">andagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Soododdoy nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soododdoy nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">kode eedu sooputo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kode eedu sooputo"/>
</div>
<div class="lyrico-lyrics-wrapper">tuntaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tuntaroda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaku vakka pantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaku vakka pantale"/>
</div>
<div class="lyrico-lyrics-wrapper">siggu bugga pandele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siggu bugga pandele"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedaalalo toorupanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedaalalo toorupanta"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalindile"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu rekkalocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu rekkalocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">teli teli podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teli teli podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanna jaajalle maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanna jaajalle maari"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chacchi puttaale ivvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacchi puttaale ivvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo lo choodu daagundi nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo choodu daagundi nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattilaanti nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattilaanti nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">otti chesinaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otti chesinaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepamalle choosukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepamalle choosukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka ninnu nenu inti mungita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka ninnu nenu inti mungita"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacchaa bottu laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacchaa bottu laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu antukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu antukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosukuntinoy daachukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosukuntinoy daachukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninni janmalo kumkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninni janmalo kumkum "/>
</div>
<div class="lyrico-lyrics-wrapper">bottulaa pettukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottulaa pettukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde gullo maate istunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde gullo maate istunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Challa challa gaalule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa challa gaalule"/>
</div>
<div class="lyrico-lyrics-wrapper">pilli moggalesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilli moggalesele"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli billi aasalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli billi aasalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheti gaaju laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheti gaaju laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">maarchi nannu uncheynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarchi nannu uncheynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali mettalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali mettalle"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chutti petteynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chutti petteynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla bangaare hayyaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla bangaare hayyaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo choodu nindundi nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo choodu nindundi nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaju bomma meedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaju bomma meedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dishti chukka nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dishti chukka nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelalle maariporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelalle maariporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadukuntu nenu chanti paapalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukuntu nenu chanti paapalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacchaa bottu laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacchaa bottu laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu antukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu antukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosukuntinoy daachukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosukuntinoy daachukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninni janmalo kumkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninni janmalo kumkum "/>
</div>
<div class="lyrico-lyrics-wrapper">bottulaa pettukuntinoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottulaa pettukuntinoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde gullo maate istunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde gullo maate istunnaa"/>
</div>
</pre>
