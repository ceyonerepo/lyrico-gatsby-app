---
title: "love like me song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Japjeet Dhillon"
path: "/albums/above-all-lyrics"
song: "Love Like Me"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/l0fmjWRfoeE"
type: "Love"
singers:
  - Jassa Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jadon koyi hiqq naal laau sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadon koyi hiqq naal laau sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu pakka cheta mera aau sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu pakka cheta mera aau sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Gore gall nu jo leke ditte chawa na tweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gore gall nu jo leke ditte chawa na tweet"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi zulfan hata ke galon laau sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi zulfan hata ke galon laau sohniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera pauga bhulekha baar baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera pauga bhulekha baar baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere baare tan zaroor ohnu dassegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere baare tan zaroor ohnu dassegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann rovenga tera te utton hassegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann rovenga tera te utton hassegi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere baare tan zaroor ohnu dassegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere baare tan zaroor ohnu dassegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann rovenga tera te utton hassegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann rovenga tera te utton hassegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohne tainu khaas kade chauna nai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohne tainu khaas kade chauna nai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada tere naal main vi mud auna nai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada tere naal main vi mud auna nai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh jhatt labh jande ajjkal yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh jhatt labh jande ajjkal yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare nai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare nai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kheda jazbaatan naal aam jehiyan gallan ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kheda jazbaatan naal aam jehiyan gallan ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Khore tainu mere layin bnaya nai si allah ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khore tainu mere layin bnaya nai si allah ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kheda jazbaatan naal aam jehiyan gallan ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kheda jazbaatan naal aam jehiyan gallan ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Khore tainu mere layin bnaya nai si allah ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khore tainu mere layin bnaya nai si allah ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Zind lekhe laayi bhora kitta na lihaaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zind lekhe laayi bhora kitta na lihaaz"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye rakhi na rakaane hun dhillon kolo aas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye rakhi na rakaane hun dhillon kolo aas"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tera sarda assi vi laange saar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tera sarda assi vi laange saar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare nai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare nai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teriyan mohabbatan da asar hi kujh aisa si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teriyan mohabbatan da asar hi kujh aisa si"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke assi beparvaah ho gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke assi beparvaah ho gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinni vaari tainu maaf karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinni vaari tainu maaf karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Assi kinni vaari tabaah ho gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assi kinni vaari tabaah ho gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab ilzaam shaq shikwe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab ilzaam shaq shikwe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal saade vall sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal saade vall sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh jo saade naal kar challeya sajjna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh jo saade naal kar challeya sajjna"/>
</div>
<div class="lyrico-lyrics-wrapper">Changi gall nahi, changi gall nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Changi gall nahi, changi gall nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu mere wangu sohniye pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu mere wangu sohniye pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dassi je koyi kare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dassi je koyi kare ni"/>
</div>
</pre>
