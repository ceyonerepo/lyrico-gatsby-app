---
title: "kathellam poo manaka song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Yugabharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Kathellam Poo Manaka"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7XcUvPNWY-g"
type: "love"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaathellaam poo manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathellaam poo manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalellaam meen sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalellaam meen sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothaatam un vanappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothaatam un vanappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vanthu poonthiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vanthu poonthiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkatha jodhi ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkatha jodhi ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthenae un mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthenae un mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nokkama pogurathenna velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokkama pogurathenna velli nilavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu kaatu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu kaatu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil thisai ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil thisai ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetu meetu meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu meetu meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil isai ah meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil isai ah meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka kattura unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka kattura unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli konda enna thanga nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli konda enna thanga nilavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal vazhi theruvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal vazhi theruvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amuthooriya mugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amuthooriya mugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaana kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaana kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraiyaadhoo kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyaadhoo kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyaathae oliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyaathae oliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirai maraiyaathae piraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai maraiyaathae piraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal madi polae ennaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal madi polae ennaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thaluvaayoo thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thaluvaayoo thunaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna pola yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pola yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda varuvaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda varuvaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullavarai mann melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavarai mann melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadoodi naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadoodi naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum ennai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo ooo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo ooo hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu kaatu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu kaatu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil thisai ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil thisai ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetu meetu meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu meetu meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil isai ah meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil isai ah meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka kattura unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka kattura unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli konda enna thanga nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli konda enna thanga nilavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai mozhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai mozhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinthae kaadavoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthae kaadavoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravin kilaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravin kilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaigal koodavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaigal koodavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyarkkai madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkkai madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Marikkum aganthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marikkum aganthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinjal nilavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjal nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukkum kulanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkum kulanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivanin sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanin sangeetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugai aaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugai aaguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyil per andam uravaaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyil per andam uravaaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaaguthae sugamaaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaaguthae sugamaaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendaamae oorgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae oorgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae pergal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae pergal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikaamal povom thadaiyangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikaamal povom thadaiyangalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu kaatu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu kaatu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil thisai ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil thisai ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetu meetu meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetu meetu meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil isai ah meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil isai ah meetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka kattura unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka kattura unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli konda enna thanga nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli konda enna thanga nilavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga nilavae…ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga nilavae…ae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga nilavae..ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga nilavae..ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga nilavae….ae….ae….ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga nilavae….ae….ae….ae…"/>
</div>
</pre>
