---
title: "bhalwani gedi song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Sagar Deol Films"
path: "/albums/above-all-lyrics"
song: "Bhalwani Gedi"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/qo0sHr5KvVk"
type: "Enjoy"
singers:
  - Jassa Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gur sidhu music!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gur sidhu music!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ditto kohinoor jiyan ankhiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ditto kohinoor jiyan ankhiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakk patla te vall bhare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakk patla te vall bhare ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiye tu dil utte lag gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiye tu dil utte lag gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulliyan te haase ne kuware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulliyan te haase ne kuware ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takk sohniye dupehr jeha mukhda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takk sohniye dupehr jeha mukhda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallo malli tere vall aun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallo malli tere vall aun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gedi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gedi laun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gedi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gedi laun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gedi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gedi laun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagani te lagga jatt patteya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagani te lagga jatt patteya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohne chadd ditte saare maahde kamm ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohne chadd ditte saare maahde kamm ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Unglan te chalda si area
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unglan te chalda si area"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainna rakhda si munda kalla dum ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainna rakhda si munda kalla dum ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagani te lagga jatt patteya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagani te lagga jatt patteya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohne chadd ditte saare maahde kamm ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohne chadd ditte saare maahde kamm ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Unglan te chalda si area
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unglan te chalda si area"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainna rakhda si munda kalla dum ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainna rakhda si munda kalla dum ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaska bandookan wala chadd ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaska bandookan wala chadd ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Husan tere de utte gaun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Husan tere de utte gaun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere khwaab jehde rahinde aa adhoore ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere khwaab jehde rahinde aa adhoore ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillon time naal kardu oh poore ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillon time naal kardu oh poore ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil'on khara jama sohniye fareb naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil'on khara jama sohniye fareb naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu horaan naal feel kare safe naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu horaan naal feel kare safe naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere khwaab jehde rahinde aa adhoore ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere khwaab jehde rahinde aa adhoore ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillon time naal kardu oh poore ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillon time naal kardu oh poore ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil'on khara jama sohniye fareb naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil'on khara jama sohniye fareb naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu horaan naal feel kare safe naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu horaan naal feel kare safe naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanu hoyi na khabar pata laggeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanu hoyi na khabar pata laggeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadon vicho vichi tainu assi chaun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadon vicho vichi tainu assi chaun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathon billo honi nahiyon riffla di shaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathon billo honi nahiyon riffla di shaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahengi je la deya jind ikko na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahengi je la deya jind ikko na"/>
</div>
<div class="lyrico-lyrics-wrapper">Unj tan bhatere pange laye ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unj tan bhatere pange laye ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sacchi aashqui ch pehli vaari hoye aa fana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sacchi aashqui ch pehli vaari hoye aa fana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tera kajla na maut kite banje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tera kajla na maut kite banje"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere utte luck azmaun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere utte luck azmaun lagg paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Town tere town tere balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Town tere town tere balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt bhalwani gehdi laun lagg paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt bhalwani gehdi laun lagg paye"/>
</div>
</pre>
