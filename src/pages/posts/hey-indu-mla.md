---
title: "hey indu song lyrics"
album: "MLA"
artist: "Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Upendra Madhav"
path: "/albums/mla-lyrics"
song: "Hey Indu"
image: ../../images/albumart/mla.jpg
date: 2018-03-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/DuHsh4EgtrA"
type: "love"
singers:
  -	Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sannajaaji ninu chuste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannajaaji ninu chuste"/>
</div>
<div class="lyrico-lyrics-wrapper">Dietingu chestade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dietingu chestade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadaana hey chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadaana hey chinnadaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandamaame eduroste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandamaame eduroste"/>
</div>
<div class="lyrico-lyrics-wrapper">Makeuppu vestade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makeuppu vestade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadaana hey chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadaana hey chinnadaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beautylo nuv everestuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautylo nuv everestuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhumi mide nuv bestuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhumi mide nuv bestuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohakandani o twistuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohakandani o twistuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu daachina Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu daachina Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">kalle ennadu muyanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalle ennadu muyanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Indu O Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Indu O Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraku padake nuv mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraku padake nuv mundu"/>
</div>
<div class="lyrico-lyrics-wrapper">O Indu naa Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Indu naa Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramokkate munumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramokkate munumundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sannajaaji ninu chuste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannajaaji ninu chuste"/>
</div>
<div class="lyrico-lyrics-wrapper">Dietingu chestade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dietingu chestade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadaana hey chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadaana hey chinnadaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandamaame eduroste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandamaame eduroste"/>
</div>
<div class="lyrico-lyrics-wrapper">Makeuppu vestade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makeuppu vestade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadaana hey chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadaana hey chinnadaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello Antanante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello Antanante "/>
</div>
<div class="lyrico-lyrics-wrapper">celluphonai puttestane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="celluphonai puttestane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallo kostanante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallo kostanante "/>
</div>
<div class="lyrico-lyrics-wrapper">suryunne jokottestane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suryunne jokottestane"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo lifenduke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo lifenduke "/>
</div>
<div class="lyrico-lyrics-wrapper">naalo half avvave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo half avvave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillo phojenduke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillo phojenduke "/>
</div>
<div class="lyrico-lyrics-wrapper">dillo place ivvave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dillo place ivvave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkanunte nuv 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkanunte nuv "/>
</div>
<div class="lyrico-lyrics-wrapper">chaale prapanchame vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaale prapanchame vaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Indu O Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Indu O Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraku padake nuv mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraku padake nuv mundu"/>
</div>
<div class="lyrico-lyrics-wrapper">O Indu naa Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Indu naa Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramokkate munumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramokkate munumundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkille neekoste 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkille neekoste "/>
</div>
<div class="lyrico-lyrics-wrapper">thalachinonne thannosta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalachinonne thannosta"/>
</div>
<div class="lyrico-lyrics-wrapper">paivaade poweriste 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paivaade poweriste "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mind napai maliista
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mind napai maliista"/>
</div>
<div class="lyrico-lyrics-wrapper">neetho nee neede 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho nee neede "/>
</div>
<div class="lyrico-lyrics-wrapper">vuntunde O pute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuntunde O pute"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dyase nee vente 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dyase nee vente "/>
</div>
<div class="lyrico-lyrics-wrapper">thirige day and nighte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirige day and nighte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkapilla neketta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkapilla neketta "/>
</div>
<div class="lyrico-lyrics-wrapper">chepte ekkutade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepte ekkutade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Indu O Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Indu O Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraku padake nuv mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraku padake nuv mundu"/>
</div>
<div class="lyrico-lyrics-wrapper">O Indu naa Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Indu naa Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramokkate munumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramokkate munumundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Indu O Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Indu O Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraku padake nuv mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraku padake nuv mundu"/>
</div>
<div class="lyrico-lyrics-wrapper">O Indu naa Indu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Indu naa Indu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaramokkate munumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaramokkate munumundu"/>
</div>
</pre>
