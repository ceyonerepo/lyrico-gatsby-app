---
title: 'aaha kalyanam song lyrics'
album: 'Petta'
artist: 'Anirudh Ravichander'
lyricist: 'Ku Karthik'
director: 'Karthik Subbaraj'
path: '/albums/petta-song-lyrics'
song: 'Aaha Kalyanam'
image: ../../images/albumart/petta.jpg
date: 2019-10-25
youtubeLink: "https://www.youtube.com/embed/9rxMk64fJvk"
type: 'festival'
lang: tamil
singers: 
- Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasa nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa aaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha paarae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootha paarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ponnu mappilla joruu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnu mappilla joruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna seruthu ooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna seruthu ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mic-ku settula paatu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mic-ku settula paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Serudhu manasu malaiya pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serudhu manasu malaiya pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya poosuna kannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maiya poosuna kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam pesudhu ninnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetkam pesudhu ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan paakkura paarva
<input type="checkbox" class="lyrico-select-lyric-line" value="Paiyan paakkura paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla irukku yedho onnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla irukku yedho onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasa nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa aaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha paarae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootha paarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maruthaani vechathu yaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Maruthaani vechathu yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai ellaam sevakkuthu paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai ellaam sevakkuthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha ela pandhiya potta
<input type="checkbox" class="lyrico-select-lyric-line" value="Pacha ela pandhiya potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa sanamum theduthu sooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththa sanamum theduthu sooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pangaali sandaiya paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Pangaali sandaiya paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththaathu quarteru beeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Paththaathu quarteru beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaachum sollidum ooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhaachum sollidum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanaththa panni paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalyanaththa panni paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasa nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa aaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha paarae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootha paarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhigu dhigu dhigu dhigu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhigu dhigu dhigu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhigu dhigu dhigu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhigu dhigu dhigu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kalkattu pottaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalkattu pottaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponjaathi thittuvaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponjaathi thittuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal adi vaangunaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kal adi vaangunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattabomman pola neeyum nikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattabomman pola neeyum nikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovaththa mootta thaan kattanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kovaththa mootta thaan kattanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam veeramaaga pesunaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorellaam veeramaaga pesunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetil vaaya moodanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Veetil vaaya moodanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vaatti ava sonna
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaatti ava sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru vaatti kekkaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Maru vaatti kekkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadha seiyanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sonnadha seiyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu naa nikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillu naa nikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Super star aanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Singamaa vaazhndhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Singamaa vaazhndhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukkul konjanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Veettukkul konjanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo kenjanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Appappo kenjanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sittaa nee parandhu varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sittaa nee parandhu varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta ava manasa thodanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thotta ava manasa thodanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta un paasam padanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Patta un paasam padanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba neeyum alli tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anba neeyum alli tharanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kattah varum kaasum panamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattah varum kaasum panamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa athu pogum dhenamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhaa athu pogum dhenamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta nambikka nadanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Natta nambikka nadanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththaa thaandaa kaiya vidanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Seththaa thaandaa kaiya vidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasa nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasa nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa haa hahaa haha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa haa hahaa haha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa aaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa kalyanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootha paarae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootha paarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhigu dhigu dhigu dhigu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhigu dhigu dhigu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhigu dhigu dhigu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhigu dhigu dhigu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigu dhagu dhigu dhagu dubbukae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhigu dhagu dhigu dhagu dubbukae"/>
</div>
</pre>