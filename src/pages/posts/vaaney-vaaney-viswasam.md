---
title: 'vaaney vaaney song lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Viveka'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Vaaney Vaaney'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
lang: tamil
singers:
- Hariharan
- Shreya Ghoshal
youtubeLink: "https://www.youtube.com/embed/UxOk1eZOrto"
type: 'duet'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Maangalyam thanthunaenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Maangalyam thanthunaenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama jeevana haethuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama jeevana haethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantae badhnaami subhagae twam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kantae badhnaami subhagae twam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeva saradhadat sadham
<input type="checkbox" class="lyrico-select-lyric-line" value="Jeeva saradhadat sadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jhum jhum jhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara rara thara rara raraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thara rara thara rara raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum jhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum jhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaara raara raa raa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaara raara raa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum jhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanae vaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae vaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un …
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan un …"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jhum jhum jhum…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum….."/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum jhum……….
<input type="checkbox" class="lyrico-select-lyric-line" value="Jhum jhum jhum………."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum dhum dhum dhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanae vaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae vaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un …
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan un …"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="En arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kann arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vendumae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mann adiyilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mann adiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vendumae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Solla mudiyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla mudiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollil adangaatha nesamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollil adangaatha nesamum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enna mudiyaadha aasaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mudiyaadha aasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidathil thondruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidathil thondruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neethaanae ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae ponjaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae un saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un saripaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae ponjaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae un saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un saripaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanae vaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae vaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un …
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan un …"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum dhum dhum (6 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhum dhum dhum"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaaa….aaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaaa….aaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iniyavalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Iniyavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu iru vizhi mun
<input type="checkbox" class="lyrico-select-lyric-line" value="Unadhu iru vizhi mun"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazharasa kuvalaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazharasa kuvalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuntha erumbin nilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhuntha erumbin nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhu nilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Enadhu nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaga viruppam illaiyae poovae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilaga viruppam illaiyae poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhisayanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhisayanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhu pala varudam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirandhu pala varudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arinthavai maranthathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Arinthavai maranthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu ninaivil indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Enathu ninaivil indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu mugam
<input type="checkbox" class="lyrico-select-lyric-line" value="Unadhu mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavira edhuvum illaiyae anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavira edhuvum illaiyae anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veraarum vaazhaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Veraarum vaazhaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vaazhvidhu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Peru vaazhvidhu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithaalae manam engum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninaithaalae manam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thoovudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai thoovudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mazhalaiyin vaasam podhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhalaiyin vaasam podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyinil vaanam podhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaiyinil vaanam podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kanamae unai pirinthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kanamae unai pirinthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir malar kaatru pogumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir malar kaatru pogumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neethaanae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm mm mm"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponjaathi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hmm mm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm mm"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naanae un
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm mm mm"/>
</div>
  <div class="lyrico-lyrics-wrapper">Saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Saripaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae ponjaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae un saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un saripaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanae vaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae vaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un …
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan un …"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="En arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kann arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vendumae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mann adiyilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mann adiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un arugilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vendumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Solla mudiyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla mudiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollil adangaatha nesamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollil adangaatha nesamum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Enna mudiyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna mudiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidathil thondruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidathil thondruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neethaanae ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae ponjaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae un saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un saripaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponjaathi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ponjaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponjaathi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naanae un
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naanae un
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanae un"/>
</div>
  <div class="lyrico-lyrics-wrapper">Saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Saripaadhi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Saripaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Saripaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanae…."/>
</div>
</pre>