---
title: "angel vandhaaley song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Angel Vandhaaley"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RuAxW6HAL8w"
type: "love"
singers:
  - Devi Sri Prasad
  - Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Angel vandhaaley vandhaaley oru poovodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angel vandhaaley vandhaaley oru poovodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oonjal seidhaalae sidhaalae en nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjal seidhaalae sidhaalae en nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai oru vaarthai sonnalae en kaadhodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai oru vaarthai sonnalae en kaadhodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvin vaNNangal maariyadhae indru ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvin vaNNangal maariyadhae indru ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angel vandhaaley vandhaaley oru poovodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angel vandhaaley vandhaaley oru poovodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oonjal seidhaalae sidhaalae en nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjal seidhaalae sidhaalae en nenjodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un koondhal vaguppil love paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koondhal vaguppil love paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">padikkum maanavanaai irundheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padikkum maanavanaai irundheney"/>
</div>
<div class="lyrico-lyrics-wrapper">hey un maeni azhagai aaraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey un maeni azhagai aaraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vignayani pol indru aanaynay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vignayani pol indru aanaynay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam success dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam success dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">inimel kiss kiss dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel kiss kiss dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaanam suzhalum en bhoomi ellamay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaanam suzhalum en bhoomi ellamay"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaan hey vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaan hey vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerukul poothirundha poovondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerukul poothirundha poovondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">neendhi vandhu arindhaayay nandri uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neendhi vandhu arindhaayay nandri uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukkul thaithirundha uravondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkul thaithirundha uravondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollum munn arindhaayay nandri uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollum munn arindhaayay nandri uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">undhan maarbil padarndhu vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan maarbil padarndhu vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan uyiril uraindhu vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan uyiril uraindhu vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uravae uravae idhu oru prabham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravae uravae idhu oru prabham"/>
</div>
<div class="lyrico-lyrics-wrapper">neerukul poothirundha poovondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerukul poothirundha poovondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">neendhi vandhu arindhaayay nandri uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neendhi vandhu arindhaayay nandri uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angel vandhalae vandhaaley oru poovodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angel vandhalae vandhaaley oru poovodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oonjal seidhaalae sidhaalae en nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjal seidhaalae sidhaalae en nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai oru vaarthai sonnalae en kaadhodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai oru vaarthai sonnalae en kaadhodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvin vaNNangal maariyadhae indru ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvin vaNNangal maariyadhae indru ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">angel vandhalae vandhaaley oru poovodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angel vandhalae vandhaaley oru poovodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oonjal seidhaalae sidhaalae en nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjal seidhaalae sidhaalae en nenjodu"/>
</div>
</pre>
