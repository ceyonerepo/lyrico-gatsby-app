---
title: "po pove song lyrics"
album: "Suryakantham"
artist: "Mark K Robin"
lyricist: "Krishna Kanth"
director: "Pranith Bramandapally"
path: "/albums/suryakantham-lyrics"
song: "Po Pove"
image: ../../images/albumart/suryakantham.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fq4_gGknuHA"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaddanna Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddanna Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhache Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhache Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthunna Ardam Undhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthunna Ardam Undhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhante Nadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhante Nadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choope Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choope Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Ayina Cheyisthundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Ayina Cheyisthundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranaale Praanamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranaale Praanamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iche Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iche Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtale Thagginchimdhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtale Thagginchimdhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechunna Rakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechunna Rakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shapam Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shapam Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigochimdhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigochimdhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Emunnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emunnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeredhale Baadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeredhale Baadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Lopale Emayinadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Lopale Emayinadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kaalame Raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kaalame Raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Pove,Po Pove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Pove,Po Pove"/>
</div>
<div class="lyrico-lyrics-wrapper">Pove Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pove Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premante Thyagam Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premante Thyagam Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Pove,Po Pove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Pove,Po Pove"/>
</div>
<div class="lyrico-lyrics-wrapper">Pove Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pove Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premunte Rajee Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premunte Rajee Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayenu Manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayenu Manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarele Manishe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarele Manishe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosaalu Chesenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosaalu Chesenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagindhi Manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagindhi Manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Thanu Kalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Thanu Kalise"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Badha Peremite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Badha Peremite"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Pove Po Pove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Pove Po Pove"/>
</div>
<div class="lyrico-lyrics-wrapper">Pove Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pove Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premante Thyagam Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premante Thyagam Kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Pove Po Pove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Pove Po Pove"/>
</div>
<div class="lyrico-lyrics-wrapper">Pove Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pove Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premunte Rajee Kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premunte Rajee Kaadha"/>
</div>
</pre>
