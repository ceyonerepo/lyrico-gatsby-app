---
title: "nesamae song lyrics"
album: "Sivakumarin Sabadham"
artist: "Hiphop Tamizha"
lyricist: "Ko Sesha"
director: "Hiphop Tamizha"
path: "/albums/sivakumarin-sabadham-lyrics"
song: "Nesamae"
image: ../../images/albumart/sivakumarin-sabadham.jpg
date: 2021-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PHFGf2djJD8"
type: "happy"
singers:
  - Anthony Daasan
  - Sudharshan Ashok
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nesamae nesamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesamae nesamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peril neerai serkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peril neerai serkkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasamae swaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasamae swaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookum paadhai dhooramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookum paadhai dhooramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai nenjai irumbakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai nenjai irumbakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai vaanai varamaakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai vaanai varamaakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai undhan varalaaru oor pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai undhan varalaaru oor pesumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai sindha thayangadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai sindha thayangadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri endrum maravadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri endrum maravadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai nindraal adhu thaanae santhoshamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai nindraal adhu thaanae santhoshamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi manidhanukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi manidhanukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhaa paasaam irunthirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhaa paasaam irunthirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodi thalaimuraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi thalaimuraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha thottram thodarndhu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha thottram thodarndhu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr kaakkum kaigal yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr kaakkum kaigal yaavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namm nesam kaakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm nesam kaakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti vaitha aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti vaitha aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodu serumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu serumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesamae nesamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesamae nesamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peril neerai serkkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peril neerai serkkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai nenjai irumbakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai nenjai irumbakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumbakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumbakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai vaanai varamaakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai vaanai varamaakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamaakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai undhan varalaaru oor pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai undhan varalaaru oor pesumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru oor pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru oor pesumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai sindha thayangadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai sindha thayangadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri endrum maravadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri endrum maravadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai nindraal adhu thaanae santhoshamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai nindraal adhu thaanae santhoshamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu thaanae santhoshamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu thaanae santhoshamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai nenjai irumbakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai nenjai irumbakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai vaanai varamaakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai vaanai varamaakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai undhan varalaaru oor pesumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai undhan varalaaru oor pesumae"/>
</div>
</pre>
