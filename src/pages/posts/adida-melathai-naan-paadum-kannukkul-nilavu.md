---
title: "adida melathai naan paadum song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Adida Melathai Naan Paadum"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dz7QPlq1ins"
type: "happy"
singers:
  - S.P. Balasubramanyam
  - S.N. Surendar
  - Arun Mozhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae Yeennadaa Nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Yeennadaa Nada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalam Thappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Thappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Thaalaththula Nadaraannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Thaalaththula Nadaraannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhhh Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhhh Hoiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhhh Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhhh Hoiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambaare Ketkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambaare Ketkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaan Machchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaan Machchaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjaale Ada Koovaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjaale Ada Koovaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jikkaan Jikkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jikkaan Jikkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanjaave Thadavaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanjaave Thadavaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittaan Kittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittaan Kittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Ada Neelaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Ada Neelaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaan Kettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaan Kettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Kattaa Panaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattaa Panaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Serththu Vechchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Serththu Vechchavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Kotta Muzhippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Kotta Muzhippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kolu Maraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kolu Maraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Manushandhaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Manushandhaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Kettu Thavippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Kettu Thavippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadanai Adhigam Vaangi Thavichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanai Adhigam Vaangi Thavichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam Varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam Varala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Muzhudhum Vizhikiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Muzhudhum Vizhikiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu Thanamaa Kaadhal Valarththavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Thanamaa Kaadhal Valarththavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenamum Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenamum Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Muzhichu Kedakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Muzhichu Kedakkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naamellaam Yoggiyandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamellaam Yoggiyandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaan Machchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaan Machchaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Kan Muzhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Kan Muzhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechchaan Vechchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechchaan Vechchaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyila Bambaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyila Bambaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatti Vechchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatti Vechchaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame Yandhiramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Yandhiramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaththi Vechchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaththi Vechchaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambaare Ketkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambaare Ketkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaan Machchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaan Machchaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjaale Ada Koovaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjaale Ada Koovaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jikkaan Jikkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jikkaan Jikkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanjaave Thadavaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanjaave Thadavaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittaan Kittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittaan Kittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Ada Neelaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Ada Neelaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaan Kettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaan Kettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangeedhathin Sangadhi Sarigamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeedhathin Sangadhi Sarigamapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambikku Cholli Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambikku Cholli Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi Suruthi Pidichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Suruthi Pidichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigamappa Dhammaare Dhammu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigamappa Dhammaare Dhammu Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koratta Koratta Jadhi Podudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koratta Koratta Jadhi Podudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urundu Porandu Oorulagam Orangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urundu Porandu Oorulagam Orangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangum Kiligal Ippa Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Kiligal Ippa Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuppu Ezhuppu Ada Namma Paattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuppu Ezhuppu Ada Namma Paattula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sayyaare Sikkimukki Sikkikichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyaare Sikkimukki Sikkikichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyaare Vekkappattu Ottikichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyaare Vekkappattu Ottikichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaale Kichchu Muchchu Vachchikichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaale Kichchu Muchchu Vachchikichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Thottu Thottu Paththikichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Thottu Thottu Paththikichchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heiiii Saambaare Ketkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heiiii Saambaare Ketkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaan Machchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaan Machchaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjaale Ada Koovaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjaale Ada Koovaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jikkaan Jikkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jikkaan Jikkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanjaave Thadavaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanjaave Thadavaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittaan Kittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittaan Kittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Ada Neelaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Ada Neelaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaan Kettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaan Kettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adidaa Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adidaa Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukkaadhe Enna Un Roottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhe Enna Un Roottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pididaa Raagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pididaa Raagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavatta Beettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavatta Beettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadhe Indha Vilaiyaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadhe Indha Vilaiyaattukku"/>
</div>
</pre>
