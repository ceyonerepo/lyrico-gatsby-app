---
title: 'mic testing song lyrics'
album: 'Mandela'
artist: 'Bharath Sankar'
lyricist: 'Arivu'
director: 'Madonne Ashwin'
path: '/albums/mandela-song-lyrics'
song: 'Mic Testing'
image: ../../images/albumart/mandela.jpg
date: 2021-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZRBJwuy5uLQ"
type: 'Motivational'

singers:
- Mano Eswaran
- Dhanush
---

<pre class="lyrics-native">
  
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mic testing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mic testing"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu rendu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu rendu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Set-ah pottu solla poren naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Set-ah pottu solla poren naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkuthu de kedaikkuthu de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkuthu de kedaikkuthu de"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathellam ippo palikkuthu de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathellam ippo palikkuthu de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thatti kumbala thaan kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thatti kumbala thaan kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu serndhu weight-ah konjam kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu serndhu weight-ah konjam kaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikkanum de mudikkanum de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikkanum de mudikkanum de"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukka vandha etti othaikkanum de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka vandha etti othaikkanum de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkaama thalli nikkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkaama thalli nikkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthikkaama maiyum vaikkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthikkaama maiyum vaikkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathanume ranagalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathanume ranagalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirunthanime illai thiruthanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunthanime illai thiruthanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthan vote-ukku thaan thaale thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan vote-ukku thaan thaale thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkum aattathukku podu thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum aattathukku podu thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedutha koottathukku thaale thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedutha koottathukku thaale thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu gate-ah podu udane udane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu gate-ah podu udane udane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilasaana pattalamum pakkuvama onnachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilasaana pattalamum pakkuvama onnachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhasana thathuvamum thappunnu thaan aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhasana thathuvamum thappunnu thaan aayachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju varusham kenji kedappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju varusham kenji kedappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja nimithi eppo kanji kudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja nimithi eppo kanji kudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni paaru ellarukkaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni paaru ellarukkaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai podu un sandhathikkaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai podu un sandhathikkaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nelaikkanum de thazhaikkanum de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaikkanum de thazhaikkanum de"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukka vandha etti odhaikkanum de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka vandha etti odhaikkanum de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthan vote-ukku thaan thaale thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan vote-ukku thaan thaale thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkum aattathukku podu thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum aattathukku podu thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedutha koottathukku thaale thillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedutha koottathukku thaale thillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu gate-ah podu udane udane udane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu gate-ah podu udane udane udane"/>
</div>
</pre>