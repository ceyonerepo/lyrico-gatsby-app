---
title: "maane thene song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Rajkumar"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Maane Thene"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i55jg9WJ6X8"
type: "love"
singers:
  - Velmurugan
  - Lakshmi Pradeep
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maanae thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae thaenae ezhuthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae thaenae ezhuthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi mayilae kuyilae paadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi mayilae kuyilae paadava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumaari koyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumaari koyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ninnu paakayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ninnu paakayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru maari aaguthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maari aaguthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondikku ondi ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondikku ondi ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimira nee pesayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimira nee pesayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenaattam inikuthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenaattam inikuthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi sirikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi sirikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En seeppum sikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seeppum sikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponds powder kekkuthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponds powder kekkuthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maanae thaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mittaaiya minukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mittaaiya minukkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piece-u piece-ah kizhikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piece-u piece-ah kizhikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevathula namma pera thaan ezhuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevathula namma pera thaan ezhuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodiya paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodiya paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiri neram paniyila paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri neram paniyila paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Radio kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radio kekkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayir soru neethandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayir soru neethandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvaadu naanthandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvaadu naanthandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkooda nee nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkooda nee nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Power ranger aavendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power ranger aavendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi onnai pola azhagi illadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi onnai pola azhagi illadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En jodi neeyae thaandi sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jodi neeyae thaandi sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kithappu katta vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithappu katta vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthatham poda vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthatham poda vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodathula thanni pudikkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodathula thanni pudikkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam thalumbuthu manasu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam thalumbuthu manasu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padathula pakkura rajini-ya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padathula pakkura rajini-ya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakuren unnai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakuren unnai thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil rekka thevayilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil rekka thevayilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam thaan nottukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam thaan nottukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sticker pottu vekkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sticker pottu vekkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikitta nethiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikitta nethiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu peru mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu peru mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thaniya paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thaniya paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sound-ah pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sound-ah pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta vaarthai rasikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta vaarthai rasikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta vaarthai rasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta vaarthai rasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi maanae thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maanae thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae thaenae ezhuthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae thaenae ezhuthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi mayilae kuyilae paadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi mayilae kuyilae paadava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumaari koyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumaari koyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ninnu paakayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ninnu paakayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru maari aaguthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maari aaguthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondikku ondi ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondikku ondi ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimira nee pesayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimira nee pesayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenaattam inikuthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenaattam inikuthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi sirikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi sirikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En seeppum sikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seeppum sikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponds powder kekkuthadi adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponds powder kekkuthadi adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un thavaniyil varum vaasathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thavaniyil varum vaasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaruthadi en manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaruthadi en manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thavaniyil varum vaasathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thavaniyil varum vaasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaruthadi oru vaarthai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaruthadi oru vaarthai solla"/>
</div>
</pre>
