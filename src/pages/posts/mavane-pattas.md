---
title: 'mavane song lyrics'
album: 'Pattas'
artist: 'Vivek Mervin'
lyricist: 'Mervin Solomon, Vivek Siva'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Mavane'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Vivek
- Arivu
- Siva
youtubeLink: 'https://www.youtube.com/embed/QfF8baf_L2g'
type: 'mass'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaguthu vaa ippa vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaguthu vaa ippa vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalai thalai thalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai thalai thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un narambugal thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un narambugal thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam irangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalam irangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandilum verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannirandilum verithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Palamadangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Palamadangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un urai thodangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un urai thodangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaivan irukindra idathinai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagaivan irukindra idathinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adainthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee adainthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniyaa …vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaa …vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irangura neramidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee irangura neramidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaa …vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sariyaa …vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ilakkinai thodangidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un ilakkinai thodangidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uruvam siridhena
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruvam siridhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikindra narigalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikindra narigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puruvam erigindra
<input type="checkbox" class="lyrico-select-lyric-line" value="Puruvam erigindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupinil anaithidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nerupinil anaithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thorppathu yaarena
<input type="checkbox" class="lyrico-select-lyric-line" value="Thorppathu yaarena"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkkuthu kalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Parkkuthu kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendravanaai unnai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vendravanaai unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Matruthu ranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Matruthu ranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthu ezhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhunthu ezhuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranin gunam
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeranin gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyae kidaiyaadhadhu yudham
<input type="checkbox" class="lyrico-select-lyric-line" value="Irudhiyae kidaiyaadhadhu yudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thorppathu yaarena
<input type="checkbox" class="lyrico-select-lyric-line" value="Thorppathu yaarena"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkkuthu kalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Parkkuthu kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendravanaai unnai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vendravanaai unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Matruthu ranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Matruthu ranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthu ezhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhunthu ezhuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranin gunam
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeranin gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyae kidaiyaadhadhu yudham
<input type="checkbox" class="lyrico-select-lyric-line" value="Irudhiyae kidaiyaadhadhu yudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaguthu vaa ippa vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaguthu vaa ippa vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaguthu vaa ippa vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaguthu vaa ippa vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha morappen
<input type="checkbox" class="lyrico-select-lyric-line" value="Moracha morappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thodanunnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna thodanunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenacha azhippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenacha azhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanthaniya modhavariya
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannanthaniya modhavariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaikku naanum readiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandaikku naanum readiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa sonthakaranukkellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeppa sonthakaranukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli viduyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Solli viduyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeh somba valattadha nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh somba valattadha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba vetchukathada vamba
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba vetchukathada vamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketta paiyan
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan ketta paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba romba
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Cool-ah thaan vandhu nippendaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Cool-ah thaan vandhu nippendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna paiyan un appandaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Chinna paiyan un appandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga vandhirukkendaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaaga vandhirukkendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo nee vaadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo nee vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mookkula naakkula
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookkula naakkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthura sokkula
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuthura sokkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethuraporaan siru vandu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sethuraporaan siru vandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru petchula vaakkula
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru petchula vaakkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya nee vitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaaya nee vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetchira poran anugundu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetchira poran anugundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaakkidavaa thookkidavaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaakkidavaa thookkidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagavanai motham neekidava
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagavanai motham neekidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthidavaa maathidavaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthidavaa maathidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi vanthaa saathidavaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Marubadi vanthaa saathidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai panthaadum pangaali naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai panthaadum pangaali naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalae nee gaali thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthaalae nee gaali thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Minjaathae un body thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Minjaathae un body thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaathae ennaikum thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Anjaathae ennaikum thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennai pola sandaikkaaran
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai pola sandaikkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumilaa ingathan
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarumilaa ingathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring kulla vandhu paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring kulla vandhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirukken vellathan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathirukken vellathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaguthu vaa ippa vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaguthu vaa ippa vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna modhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna modhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen nee ippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen nee ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaada…"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaguthu vaa ippa vaada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaguthu vaa ippa vaada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uruvam siridhena
<input type="checkbox" class="lyrico-select-lyric-line" value="Uruvam siridhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikindra narigalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikindra narigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puruvam erigindra
<input type="checkbox" class="lyrico-select-lyric-line" value="Puruvam erigindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupinil anaithidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nerupinil anaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhichidu…vizhithdu… porthidu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhichidu…vizhithdu… porthidu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhikka vantha pagai ver aruthidu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhikka vantha pagai ver aruthidu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mavanae enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Mavanae enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya varen
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyaaguthu vaa ippa vaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyaaguthu vaa ippa vaada"/>
</div>
</pre>