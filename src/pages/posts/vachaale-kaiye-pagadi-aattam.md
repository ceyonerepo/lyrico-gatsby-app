---
title: "vachaale kaiye song lyrics"
album: "Pagadi Aattam"
artist: "Karthik Raja"
lyricist: "Vijay Saagar"
director: "Ram K Chandran"
path: "/albums/pagadi-aattam-lyrics"
song: "Vachaale Kaiye"
image: ../../images/albumart/pagadi-aattam.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ndVJcsqZxaA"
type: "sad"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vachaale kaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachaale kaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaththa seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaththa seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enge pogume kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge pogume kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetta veliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta veliyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam poochithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochithan"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">patta kastatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kastatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vitta vervaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitta vervaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">marakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thudithu yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithu yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivala paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivala paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithukku neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithukku neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">pathila kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathila kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyaaga pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyaaga pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda pechi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiyumo manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiyumo manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poiyaaga pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyaaga pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda pechi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiyumo manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiyumo manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poiyaaga pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyaaga pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda pechi"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiyumo manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiyumo manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum sollu sollu"/>
</div>
</pre>
