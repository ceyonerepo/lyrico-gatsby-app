---
title: "deva emmai song lyrics"
album: "Visithiran"
artist: "GV Prakash"
lyricist: "Yugabharathi"
director: "M Padmakumar"
path: "/albums/visithiran-lyrics"
song: "Deva Emmai"
image: ../../images/albumart/visithiran.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T-PtlsT9qUE"
type: "melody"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dheva Emmai Kaappaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheva Emmai Kaappaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thempum Emmai Thettrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thempum Emmai Thettrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Theerkkum Aanmaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Theerkkum Aanmaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Pongum Ootrin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Pongum Ootrin "/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalvaari Thottathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvaari Thottathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Seidha Thiyagathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Seidha Thiyagathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelaadha Or Jeevan Ingedhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelaadha Or Jeevan Ingedhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullodum Poovodum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullodum Poovodum "/>
</div>
<div class="lyrico-lyrics-wrapper">Poongaatru Thannodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatru Thannodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unperai Oyamal Sollvomaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unperai Oyamal Sollvomaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meippan Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meippan Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meyum Aadum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meyum Aadum Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaamal Munnera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamal Munnera "/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaaram Neeyendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaaram Neeyendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Un Vazhvendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Un Vazhvendru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaadho Yezhulagamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaadho Yezhulagamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantaadi Nirporai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantaadi Nirporai "/>
</div>
<div class="lyrico-lyrics-wrapper">Maravaamal Kaappattrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaamal Kaappattrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Nesan Unpole Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Nesan Unpole Yaaro Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaniruppavane Uyir Koduppavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaniruppavane Uyir Koduppavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhariginile Neengum Paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhariginile Neengum Paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Iruppuvane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Iruppuvane "/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyar Thudaippavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyar Thudaippavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathazhaginile Ongum Logam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathazhaginile Ongum Logam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai Thaanga Mudiyaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai Thaanga Mudiyaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Novum Yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novum Yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jebiththaale Tharuvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jebiththaale Tharuvaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Shemam Shemam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shemam Shemam Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheva Emmai Kaappaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheva Emmai Kaappaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thempum Emmai Thettrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thempum Emmai Thettrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Theerkkum Aanmaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Theerkkum Aanmaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Pongum Ootrin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Pongum Ootrin "/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaamal Munnera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamal Munnera "/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaaram Neeyendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaaram Neeyendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Un Vazhvendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Un Vazhvendru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaadho Yezhulagamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaadho Yezhulagamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantaadi Nirporai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantaadi Nirporai "/>
</div>
<div class="lyrico-lyrics-wrapper">Maravaamal Kaappattrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaamal Kaappattrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Nesan Unpole Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Nesan Unpole Yaaro Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karunayin Magane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunayin Magane "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangarai Unaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangarai Unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidham Thozhuthirundhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidham Thozhuthirundhal "/>
</div>
<div class="lyrico-lyrics-wrapper">Nerum Nanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerum Nanmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suga Kirubaiyinaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suga Kirubaiyinaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Paravulagilume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravulagilume"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Thaarum Oruvan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Thaarum Oruvan "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Vaangi Emai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Vaangi Emai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha Naathan Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha Naathan Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Naangal Jepithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Naangal Jepithale"/>
</div>
<div class="lyrico-lyrics-wrapper">Theernthe Pogum Noye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theernthe Pogum Noye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheva Emmai Kaappaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheva Emmai Kaappaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thempum Emmai Thettrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thempum Emmai Thettrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Theerkkum Aanmaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Theerkkum Aanmaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Pongum Ootrin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Pongum Ootrin "/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Neeye"/>
</div>
</pre>
