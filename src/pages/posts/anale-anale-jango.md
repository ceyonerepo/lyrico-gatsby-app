---
title: "anale anale song lyrics"
album: "Jango"
artist: "Ghibran"
lyricist: "N. Idhaya"
director: "Mano Karthikeyan"
path: "/albums/jango-lyrics"
song: "Anale Anale"
image: ../../images/albumart/jango.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gW7XpvYfl5I"
type: "mass"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Analae analae nee azhagin analae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analae analae nee azhagin analae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanalae kanalae en kanavin kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanalae kanalae en kanavin kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagalae nagalae nee nilavin nagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagalae nagalae nee nilavin nagalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaaliyin paadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaaliyin paadalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum pagalum un ninaivin thanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum pagalum un ninaivin thanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithae nagarum en nagara pagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithae nagarum en nagara pagalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraga silaiya un vizhiyin madalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraga silaiya un vizhiyin madalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thediya thedalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thediya thedalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagiyae sagiyae en kanavin vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae sagiyae en kanavin vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai thirudum un vizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai thirudum un vizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin visaiyae un idhazhin isaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin visaiyae un idhazhin isaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En paadhai un dhisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadhai un dhisaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagida ninaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagida ninaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai piriyum pozhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai piriyum pozhuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugena karaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhugena karaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai puriyum pozhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai puriyum pozhuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru siru sinungal varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru siru sinungal varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanangal sugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanangal sugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan theduthae theduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan theduthae theduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru variyaai nee naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru variyaai nee naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindha kuralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindha kuralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thani irundhalIllai porulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani thani irundhalIllai porulae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru badhil tharuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru badhil tharuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada uyiraai varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada uyiraai varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaatchigal maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaatchigal maarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae indri naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae indri naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeya thaeipirai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeya thaeipirai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhalaal kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhalaal kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal seivom naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal seivom naamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiveli kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal kodumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal kodumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyindri enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyindri enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalgal verumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalgal verumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeratha theeratha kaadhal theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha theeratha kaadhal theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analae analae nee azhagin analae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analae analae nee azhagin analae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanalae kanalae en kanavin kanalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanalae kanalae en kanavin kanalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagalae nagalae nee nilavin nagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagalae nagalae nee nilavin nagalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaaliyin paadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaaliyin paadalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagiyae sagiyae en kanavin vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae sagiyae en kanavin vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai thirudum un vizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai thirudum un vizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin visaiyae un idhazhin isaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin visaiyae un idhazhin isaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En paadhai un dhisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadhai un dhisaiyae"/>
</div>
</pre>
