---
title: "anukoledhe song lyrics"
album: "Juvva"
artist: "M M Keeravani"
lyricist: "Ananta Sriram"
director: "Trikoti Peta"
path: "/albums/juvva-lyrics"
song: "Anukoledhe"
image: ../../images/albumart/juvva.jpg
date: 2018-02-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/I5_tWu2AK6k"
type: "happy"
singers:
  -	Hymath
  - Sony
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo Thappa Oo Thappa Oo Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Thappa Oo Thappa Oo Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappapapparo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappapapparo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Thappa Oo Thappa Oo Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Thappa Oo Thappa Oo Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappapapparo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappapapparo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Thappa Oo Thappa Oo Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Thappa Oo Thappa Oo Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappapapparo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappapapparo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Thappa Oo Thappa Oo Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Thappa Oo Thappa Oo Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappapapparo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappapapparo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoneledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Kalaieka Nijamani Anukoledhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kalaieka Nijamani Anukoledhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalika Nijamani Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalika Nijamani Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jatha Nijamani Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jatha Nijamani Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gundello Gubulu Vundhi Kastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gundello Gubulu Vundhi Kastha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallallo Kadhulutundhi Kostha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallo Kadhulutundhi Kostha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Picha Picha Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Picha Picha Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vecha Vecha Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vecha Vecha Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Kacha Kacha Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kacha Kacha Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Racha Racha Gunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Racha Racha Gunadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoneledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharalanni Tharumaru Avtayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharalanni Tharumaru Avtayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharulanni Naa Dharikosthayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharulanni Naa Dharikosthayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamochi Kalikindha Vuntadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamochi Kalikindha Vuntadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathakamlo Geethalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakamlo Geethalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Girru Girru Mantu Thirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girru Girru Mantu Thirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta Ratha Rasthayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta Ratha Rasthayani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoneledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppenaina Thiyathiyaga Untadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppenaina Thiyathiyaga Untadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippulaina Vennalni Testhayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippulaina Vennalni Testhayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappulanni Meppulu Andhukuntayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappulanni Meppulu Andhukuntayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppaleni Thirulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppaleni Thirulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellu Chellu Mantu Chempa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellu Chellu Mantu Chempa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhulu Andhukuntadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhulu Andhukuntadhani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Assalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Assalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assalu Assalu Anukoneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Assalu Anukoneledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Thappa Oo Thappa Oo Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Thappa Oo Thappa Oo Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappapapparo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappapapparo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kalaieka Nijamani Anukoledhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kalaieka Nijamani Anukoledhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalika Nijamani Anukoledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalika Nijamani Anukoledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jatha Nijamani Anukoneledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jatha Nijamani Anukoneledhe"/>
</div>
</pre>
