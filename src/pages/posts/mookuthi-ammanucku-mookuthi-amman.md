---
title: "aadi kuththu lyrics"
album: "Mookuthi Amman"
artist: "Girishh Gopalakrishnan"
lyricist: "Pa Vijay"
director: "RJ Balaji  & NJ Saravanan"
path: "/albums/mookuthi-amman-song-lyrics"
song: "Aadi Kuththu"
image: ../../images/albumart/mookuthi-amman.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YzrIHjVUB0E"
type: "devotional"
singers:
  - L.R.Eswari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mookuthi ammanukku ponga vaippom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi ammanukku ponga vaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil veppilayai yenthi vanthu varam ketpom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil veppilayai yenthi vanthu varam ketpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookuthi ammanukku ponga vaippom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi ammanukku ponga vaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil veppilayai yenthi vanthu varam ketpom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil veppilayai yenthi vanthu varam ketpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirusoolam nayagiye vaadiyamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirusoolam nayagiye vaadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirusoolam nayagiye vaadiyamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirusoolam nayagiye vaadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha thirunaalil vendiyatha thaadiyamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha thirunaalil vendiyatha thaadiyamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singa mugam bavanathil sivappu selai katti
<input type="checkbox" class="lyrico-select-lyric-line" value="Singa mugam bavanathil sivappu selai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bannari ammanaga bavani vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bannari ammanaga bavani vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bavani vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Bavani vanthalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma bavani vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma bavani vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangapadi therunile kungumam poosikittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thangapadi therunile kungumam poosikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandumaari ammanaga maari vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandumaari ammanaga maari vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maari vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari vanthalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumaari vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Karumaari vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magamaayi peru sonna
<input type="checkbox" class="lyrico-select-lyric-line" value="Magamaayi peru sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruganame noiyu vizhagum
<input type="checkbox" class="lyrico-select-lyric-line" value="Maruganame noiyu vizhagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayapuram ammanaga soozhnthu vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Samayapuram ammanaga soozhnthu vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Omgaari orangattu odugalal malayittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Omgaari orangattu odugalal malayittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Masani ammanaga neenthi vanthalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Masani ammanaga neenthi vanthalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatha unakku vekkum neiyil vilakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Aatha unakku vekkum neiyil vilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pindam erangu theerum paavam kanakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Pindam erangu theerum paavam kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amman arul thaan enga kooda irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Amman arul thaan enga kooda irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam thuyaram ini ethu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunbam thuyaram ini ethu namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththi suththi sooran thaan vettaikku vaaran
<input type="checkbox" class="lyrico-select-lyric-line" value="Suththi suththi sooran thaan vettaikku vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhi kettu sooran ivan kottaikku vaaran
<input type="checkbox" class="lyrico-select-lyric-line" value="Budhi kettu sooran ivan kottaikku vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu vaippala soochamakaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Vittu vittu vaippala soochamakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu pattu nippala veppalakaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattu pattu nippala veppalakaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchcham thalaiyil karagam suthuthu esakki mariyamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchcham thalaiyil karagam suthuthu esakki mariyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu thalaiyum pathara vaikkathu bhadrakali amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Paththu thalaiyum pathara vaikkathu bhadrakali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhanamaari amma enga sangadam theerum amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Santhanamaari amma enga sangadam theerum amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaye mookuthi amma nalla vazhiya kaattu amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaye mookuthi amma nalla vazhiya kaattu amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma mookuthi amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Amma mookuthi amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookuthi amma!!!
<input type="checkbox" class="lyrico-select-lyric-line" value="Mookuthi amma!!!"/>
</div>
</pre>
