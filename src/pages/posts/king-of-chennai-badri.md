---
title: "king of chennai song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "King Of Chennai"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kBdiBQtBC5o"
type: "mass"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">King Of Chennai Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of Chennai Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigalai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigalai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ellaam Ennai Thaedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ellaam Ennai Thaedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Jeansgalai Kizhithu Aniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Jeansgalai Kizhithu Aniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Soldiergal Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Soldiergal Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Edhirigal Kurukkil Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Edhirigal Kurukkil Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Norukkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Norukkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeyaeyaeyaeyaeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeyaeyaeyaeyaeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King Of Chennai Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of Chennai Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigalai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigalai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ellaam Ennai Thaedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ellaam Ennai Thaedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Kelu Yohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Kelu Yohh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalil Irukkum Kaandhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Irukkum Kaandhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Figuregalai Izhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figuregalai Izhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralai Asaithu Sodakku Pottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralai Asaithu Sodakku Pottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idipugal Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idipugal Thudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Catwalk Pogum Maiyil Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Catwalk Pogum Maiyil Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Pinnaal Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pinnaal Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern Kiligal Ennai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Kiligal Ennai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragadikkum Sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragadikkum Sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King Of Chennai Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of Chennai Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigalai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigalai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ellaam Ennai Thaedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ellaam Ennai Thaedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Kelu Yohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Kelu Yohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhinil Kadukkan Aninthiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhinil Kadukkan Aninthiruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Stylegalai Padaippom Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stylegalai Padaippom Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaigal Virithu Vaithu Internettil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaigal Virithu Vaithu Internettil"/>
</div>
<div class="lyrico-lyrics-wrapper">Figuregalai Madippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figuregalai Madippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhudhaan Idhudhaan Pudhu Fashion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Idhudhaan Pudhu Fashion"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum Pudhumai Pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Pudhumai Pirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudam Muzhudhum Love Season
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudam Muzhudhum Love Season"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Irukkum Inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Irukkum Inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King Of Chennai Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of Chennai Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigalai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigalai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ellaam Ennai Thaedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ellaam Ennai Thaedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Jeansgalai Kizhithu Aniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Jeansgalai Kizhithu Aniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Soldiergal Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Soldiergal Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Edhirigal Kurukkil Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Edhirigal Kurukkil Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Norukkuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Norukkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeyaeyaeyaeyaeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeyaeyaeyaeyaeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King Of Chennai Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of Chennai Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigalai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigalai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Ellaam Ennai Thaedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Ellaam Ennai Thaedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Kelu Yohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Kelu Yohh"/>
</div>
</pre>
