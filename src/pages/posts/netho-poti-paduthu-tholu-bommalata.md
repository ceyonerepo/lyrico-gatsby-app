---
title: "netho poti paduthu song lyrics"
album: "Tholu Bommalata"
artist: "Suresh Bobbili"
lyricist: "Chaitanya Prasad"
director: "Viswanath Maganti"
path: "/albums/tholu-bommalata-lyrics"
song: "Netho Poti Paduthu"
image: ../../images/albumart/tholu-bommalata.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YjCQBDcGZVU"
type: "love"
singers:
  - Yazir Nasin
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">netho poti paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netho poti paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">saradaga nedee koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradaga nedee koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">saganaa chalaregana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saganaa chalaregana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neetho pate untu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho pate untu"/>
</div>
<div class="lyrico-lyrics-wrapper">neelage rai raimantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelage rai raimantu"/>
</div>
<div class="lyrico-lyrics-wrapper">regana oregganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="regana oregganaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pillanuvnatho unte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillanuvnatho unte "/>
</div>
<div class="lyrico-lyrics-wrapper">chalabaguntunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalabaguntunde"/>
</div>
<div class="lyrico-lyrics-wrapper">neetho gadipe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho gadipe "/>
</div>
<div class="lyrico-lyrics-wrapper">kalam enthomadhurame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam enthomadhurame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee maatlatho thelisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maatlatho thelisi"/>
</div>
<div class="lyrico-lyrics-wrapper">bolthapadathavunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bolthapadathavunde"/>
</div>
<div class="lyrico-lyrics-wrapper">ina manase nee thopadhileme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ina manase nee thopadhileme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">i want you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i want you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvnaalo nennelo cheralile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvnaalo nennelo cheralile "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">i told you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i told you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">nenneela nuvunaala maraalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenneela nuvunaala maraalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adhirenadakastyle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhirenadakastyle "/>
</div>
<div class="lyrico-lyrics-wrapper">sogase o missle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sogase o missle"/>
</div>
<div class="lyrico-lyrics-wrapper">i wanna stay with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i wanna stay with you"/>
</div>
<div class="lyrico-lyrics-wrapper">i wanna play with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i wanna play with you"/>
</div>
<div class="lyrico-lyrics-wrapper">you just dont go time for
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you just dont go time for"/>
</div>
<div class="lyrico-lyrics-wrapper">yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yo yo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modern valentine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modern valentine"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvunaprema wine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvunaprema wine"/>
</div>
<div class="lyrico-lyrics-wrapper">i wanna live with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i wanna live with you"/>
</div>
<div class="lyrico-lyrics-wrapper">i wanna die with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i wanna die with you"/>
</div>
<div class="lyrico-lyrics-wrapper">you have to be all the way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you have to be all the way"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidipom memantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidipom memantu "/>
</div>
<div class="lyrico-lyrics-wrapper">verahale levantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verahale levantu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi roju maadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi roju maadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">chattedhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chattedhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jagame maadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagame maadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi chota memuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi chota memuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">premante memantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante memantu"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevidhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevidhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">i want you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i want you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvnaalo nennelo cheralile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvnaalo nennelo cheralile "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">i told you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i told you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">nenneela nuvunaala maraalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenneela nuvunaala maraalile"/>
</div>
</pre>
