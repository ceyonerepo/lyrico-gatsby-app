---
title: "mavanae yaarukittae song lyrics"
album: "Dilluku Dhuddu 2"
artist: "Shabir"
lyricist: "Gana Vinod - Shabir"
director: "Rambhala"
path: "/albums/dhilluku-dhuddu-2-lyrics"
song: "Mavanae Yaarukittae"
image: ../../images/albumart/dhilluku-dhuddu-2.jpg
date: 2019-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eQJr3S1WXfE"
type: "mass"
singers:
  - Gana Vinoth
  - Shabir
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaimalu kaimalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaimalu kaimalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya vutta dammalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya vutta dammalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadaarnu buttarnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadaarnu buttarnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onjiduven sittaanjinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onjiduven sittaanjinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaandre vanaandre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaandre vanaandre"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayamoottu podaandre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayamoottu podaandre"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kolaru summaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kolaru summaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangiduva pallaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangiduva pallaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macha vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha area-ahve malichamaagunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha area-ahve malichamaagunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama illana antha oore pulichamaavuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama illana antha oore pulichamaavuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechi senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munji kizhinji thenju pogunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un munji kizhinji thenju pogunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ditch idicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ditch idicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un naara manda gera aagumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un naara manda gera aagumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra polakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra polakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra polakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra polakuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangellam gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangellam gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">You don’t be silly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don’t be silly"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongira singatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongira singatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti elippitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti elippitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kona goyya marathu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kona goyya marathu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Korangu maattikichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korangu maattikichan"/>
</div>
<div class="lyrico-lyrics-wrapper">Korangu pudikka pona misiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korangu pudikka pona misiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gownu maattikichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gownu maattikichan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhakartha dhakartha dhakartha dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhakartha dhakartha dhakartha dhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhakartha dhakartha dhakartha dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhakartha dhakartha dhakartha dhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook instagram twitter ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook instagram twitter ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama photo potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama photo potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Like share therikkum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like share therikkum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nashtam kashtam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nashtam kashtam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanakku kaalu thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanakku kaalu thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha area-yala enga maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha area-yala enga maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma maasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha area-yala enga maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha area-yala enga maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma maasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Utadha kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utadha kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee munnerava maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee munnerava maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy pannu life-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy pannu life-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppothume jaipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppothume jaipa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uttande soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uttande soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Roadtaande beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadtaande beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkuda seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkuda seru"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangellam ghilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangellam ghilli"/>
</div>
<div class="lyrico-lyrics-wrapper">You don’t be silly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You don’t be silly"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongira singatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongira singatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti elippitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti elippitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Papapaan papapa papapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papapaan papapa papapaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavane yaarukitta ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane yaarukitta ah"/>
</div>
</pre>
