---
title: "kadavulae vidai song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "Vivek"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Kadavulae Vidai"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6HKKiVWljic"
type: "melody"
singers:
  - Sean Roldan
  - Pragathi Guruprasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadavule Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavule Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavule Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavule Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavule Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavule Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavule Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavule Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theyatha Muthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyatha Muthama"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhka Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhka Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna Venum Pothumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Venum Pothumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Natta Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Natta Vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetta Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetta Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver Vittu Pookkum Neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Vittu Pookkum Neramey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Thozhil Saanju Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thozhil Saanju Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhukaapa Thoongalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhukaapa Thoongalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kaiyil Serunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaiyil Serunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Thandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Poga Menmaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Menmaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkayachu Unmaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkayachu Unmaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosupatta Podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosupatta Podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayamaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayamaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogamilla Bommaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogamilla Bommaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavi Odum Pillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi Odum Pillaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Ponom Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Ponom Kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theva Vandha Thandhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Vandha Thandhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thai Paala Sindhuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Paala Sindhuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varatha Keta Saamiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varatha Keta Saamiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Thandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Thozhil Saanju Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thozhil Saanju Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhukaapa Thoongalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhukaapa Thoongalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kaiyil Serunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaiyil Serunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Thandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Vidai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Vidai "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuley Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Vidai"/>
</div>
</pre>
