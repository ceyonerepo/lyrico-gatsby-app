---
title: "jee le song lyrics"
album: "Luck"
artist: "Salim–Sulaiman - Amar Mohile"
lyricist: "Shabbir Ahmed - Anvita Dutt Guptan"
director: "Soham Shah"
path: "/albums/luck-lyrics"
song: "Jee le - Take a Chance"
image: ../../images/albumart/luck.jpg
date: 2009-07-24
lang: hindi
youtubeLink: "https://www.youtube.com/embed/IK9-2obgznM"
type: "happy"
singers:
  - Shruti Pathak
  - Naresh Kamath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Take a chance pyaar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chance pyaar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh mere bass ki baat nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh mere bass ki baat nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a chance pyaar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chance pyaar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh kal ko dega saath nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh kal ko dega saath nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zara zara kar le tu yakeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara kar le tu yakeen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh hai saza haseen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hai saza haseen"/>
</div>
<div class="lyrico-lyrics-wrapper">Zara zara darr ko tu pee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara darr ko tu pee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur marr ke tu jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur marr ke tu jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I gotcha stare at me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I gotcha stare at me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a chance in loving me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chance in loving me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Loving you is heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loving you is heavenly"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine chand ki parchaiyee mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine chand ki parchaiyee mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Haathon ki lakeeren dekhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haathon ki lakeeren dekhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And you can tell by the way i do my thing honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And you can tell by the way i do my thing honey"/>
</div>
<div class="lyrico-lyrics-wrapper">I don’t believe in fantasies & it ain’t funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I don’t believe in fantasies & it ain’t funny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine pyaar ke sajde mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine pyaar ke sajde mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhuki hui taqdeerein dekhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhuki hui taqdeerein dekhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Its not a game I want to play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its not a game I want to play"/>
</div>
<div class="lyrico-lyrics-wrapper">No winners losing all the way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No winners losing all the way"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zara zara kar le tu yakeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara kar le tu yakeen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh hai saza haseen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hai saza haseen"/>
</div>
<div class="lyrico-lyrics-wrapper">Zara zara darr ko tu pee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara darr ko tu pee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur marr ke tu jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur marr ke tu jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">I gotcha stare at me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I gotcha stare at me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a chance in loving me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chance in loving me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Loving you is heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loving you is heavenly"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jee lee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee lee"/>
</div>
<div class="lyrico-lyrics-wrapper">What you saying i ain’t buying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What you saying i ain’t buying"/>
</div>
<div class="lyrico-lyrics-wrapper">Way you saying it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Way you saying it"/>
</div>
<div class="lyrico-lyrics-wrapper">You can keep on giving me the vibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can keep on giving me the vibe"/>
</div>
<div class="lyrico-lyrics-wrapper">I aint buying it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I aint buying it"/>
</div>
<div class="lyrico-lyrics-wrapper">I m a real hustler
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I m a real hustler"/>
</div>
<div class="lyrico-lyrics-wrapper">no time to be slow in it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no time to be slow in it"/>
</div>
<div class="lyrico-lyrics-wrapper">Trun on the mircophone up &
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trun on the mircophone up &"/>
</div>
<div class="lyrico-lyrics-wrapper">Checkout the way i m flowing it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Checkout the way i m flowing it"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine pyaar ki angdyee mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine pyaar ki angdyee mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagi hui taqdeerein dekhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagi hui taqdeerein dekhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">and i aint buying about it baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and i aint buying about it baby"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m running game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m running game"/>
</div>
<div class="lyrico-lyrics-wrapper">I dont need an insecure lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I dont need an insecure lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine pyaar ke rango mein hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine pyaar ke rango mein hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangi hui tasveerein dekhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangi hui tasveerein dekhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t even try to make me stay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t even try to make me stay"/>
</div>
<div class="lyrico-lyrics-wrapper">Its safer that I walk away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its safer that I walk away"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zara zara kar le tu yakeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara kar le tu yakeen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh hai… yeh hai…saza haseen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hai… yeh hai…saza haseen"/>
</div>
<div class="lyrico-lyrics-wrapper">Zara zara darr ko tu pee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara zara darr ko tu pee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur marr ke tu jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur marr ke tu jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kar aitbaar jee le chahe ek baar jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar aitbaar jee le chahe ek baar jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudd ke naa aayegi yeh zindagi jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudd ke naa aayegi yeh zindagi jee le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I gotcha stare at me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I gotcha stare at me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a chance in loving me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chance in loving me"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Loving you is heavenly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loving you is heavenly"/>
</div>
<div class="lyrico-lyrics-wrapper">jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jee le"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le"/>
</div>
</pre>
