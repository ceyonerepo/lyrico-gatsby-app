---
title: "ey piriyame piriyame song lyrics"
album: "Ratsasan"
artist: "Ghibran"
lyricist: "Ratnakumar"
director: "Ram Kumar"
path: "/albums/ratsasan-lyrics"
song: "Ey Piriyame Piriyame"
image: ../../images/albumart/ratsasan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ljUatpqVP1Q"
type: "love"
singers:
  - Yazin Nizar
  - Anudeep Dev
  - Pragathi Guruprasad
  - Ratnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei piriyamae piriyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei piriyamae piriyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un piriyathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un piriyathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaigalum yethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaigalum yethada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivugal pirarukku thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivugal pirarukku thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nerukkathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaippaval naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaippaval naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaati vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaati vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogathae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogathae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae vazhi thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vazhi thunaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesu ethachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu ethachum"/>
</div>
<div class="lyrico-lyrics-wrapper">koocham pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocham pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae vendumae ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae vendumae ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei piriyamae piriyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei piriyamae piriyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un piriyathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un piriyathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaigalum yethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaigalum yethada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivugal pirarukku thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivugal pirarukku thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nerukkathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaippaval naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaippaval naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen etharko adimanathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen etharko adimanathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yevaraal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yevaraal "/>
</div>
<div class="lyrico-lyrics-wrapper">enum oru kelvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enum oru kelvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru muraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru muraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu pogumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu pogumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesu ethachum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu ethachum "/>
</div>
<div class="lyrico-lyrics-wrapper">koocham pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocham pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae vendumae ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae vendumae ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei piriyamae piriyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei piriyamae piriyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un piriyathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un piriyathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaigalum yethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaigalum yethadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraivathu idaiveli thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraivathu idaiveli thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nerukkathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nerukkathil "/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaippavan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaippavan naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaati vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaati vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogathae neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogathae neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae vazhi thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vazhi thunaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesu ethachum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu ethachum "/>
</div>
<div class="lyrico-lyrics-wrapper">koocham pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocham pogattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopidu koopidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopidu koopidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae vendumae ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae vendumae ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae vendum adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae vendum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda naan nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda naan nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae vendum adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae vendum adi"/>
</div>
</pre>
