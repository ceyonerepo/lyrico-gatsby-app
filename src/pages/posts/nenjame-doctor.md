---
title: "nenjame song lyrics"
album: "Doctor"
artist: "Anirudh Ravichander"
lyricist: "Mohan Rajan"
director: "Nelson Dilipkumar"
path: "/albums/doctor-lyrics"
song: "Nenjame"
image: ../../images/albumart/doctor.jpg
date: 2021-10-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rS77fDr1mhM"
type: "melody"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjame nenjame en nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjame nenjame en nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugume udaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugume udaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">Va konjame nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va konjame nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugume nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugume nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo nenjame nenjame en nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo nenjame nenjame en nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyume kalaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyume kalaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">Va konjame nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va konjame nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyume nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyume nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada kanna pinna kanvodu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kanna pinna kanvodu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna unna ninachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna unna ninachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi onna renda valiyodathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi onna renda valiyodathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo thunda thunda udanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo thunda thunda udanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavithene un ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavithene un ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudithene un pirivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudithene un pirivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thangala thangala thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thangala thangala thangalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavithene un ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavithene un ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudithene un pirivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudithene un pirivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thangala thangala thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thangala thangala thangalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna vittu nee thoram pogathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vittu nee thoram pogathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaangal rendum kanneeril thongathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaangal rendum kanneeril thongathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kudave valgintra nizhal naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kudave valgintra nizhal naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalukku thaan vaai pesa theriyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalukku thaan vaai pesa theriyathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodum illamal ennodum illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodum illamal ennodum illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazha pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazha pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerala valiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerala valiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhetho ninaivodu yen vaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhetho ninaivodu yen vaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kanna pinna kanvodu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kanna pinna kanvodu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna unna ninachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna unna ninachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi onna renda valiyodathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi onna renda valiyodathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo thunda thunda udanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo thunda thunda udanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavithene un ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavithene un ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudithene un pirivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudithene un pirivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thangala thangala thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thangala thangala thangalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavithene un ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavithene un ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudithene un pirivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudithene un pirivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali thangala thangala thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali thangala thangala thangalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjame nenjame en nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjame nenjame en nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugume udaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugume udaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">Va konjame nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va konjame nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugume nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugume nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo nenjame nenjame en nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo nenjame nenjame en nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyume kalaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyume kalaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">Va konjame nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va konjame nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyume nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyume nenjame"/>
</div>
</pre>
