---
title: "acham neeki song lyrics"
album: "Sathru"
artist: "Amresh Ganesh - Surya Prasadh R"
lyricist: "Kabilan - Karky - Sorko"
director: "Naveen Nanjundan"
path: "/albums/sathru-lyrics"
song: "Acham Neeki"
image: ../../images/albumart/sathru.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7TXa38M7vrk"
type: "mass"
singers:
  - Amresh Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Achcham Neeki Kaakkum Kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Neeki Kaakkum Kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachchu Paambai Nasukkum Kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachchu Paambai Nasukkum Kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcha Needhi Kaatum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcha Needhi Kaatum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettravan Kattravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettravan Kattravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sozhchi Seidhaal Veezhchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhchi Seidhaal Veezhchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyaale Aatchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaale Aatchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki Sattai Puratchi Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sattai Puratchi Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi Thaan Meetchi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi Thaan Meetchi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyil Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Punal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Punal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesidum Puyalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesidum Puyalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lathiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lathiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmaththin Mudichai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmaththin Mudichai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avizhppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avizhppavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Thannanthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thannanthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulavuthurai Oodagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulavuthurai Oodagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kangal Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kangal Paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poligalin Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poligalin Naadagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadayathai Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayathai Kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppi Potta Thuppaaki Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppi Potta Thuppaaki Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Neeki Kaakkum Kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Neeki Kaakkum Kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachchu Paambai Nasukkum Kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachchu Paambai Nasukkum Kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcha Needhi Kaatum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcha Needhi Kaatum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettravan Kattravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettravan Kattravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sozhchi Seidhaal Veezhchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhchi Seidhaal Veezhchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyaale Aatchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaale Aatchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki Sattai Puratchi Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sattai Puratchi Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi Thaan Meetchi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi Thaan Meetchi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koormaiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koormaiyum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nermaiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermaiyum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aani Ver Pidungidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aani Ver Pidungidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirambadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirambadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambeeramaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambeeramaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaithadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Pokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Pokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam Kaakkum Ullamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam Kaakkum Ullamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam Kandu Marmam Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Kandu Marmam Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadayathai Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayathai Kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppi Potta Thuppaaki Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppi Potta Thuppaaki Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Neeki Kaakkum Kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Neeki Kaakkum Kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachchu Paambai Nasukkum Kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachchu Paambai Nasukkum Kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcha Needhi Kaatum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcha Needhi Kaatum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettravan Kattravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettravan Kattravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sozhchi Seidhaal Veezhchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhchi Seidhaal Veezhchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyaale Aatchi Seivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaale Aatchi Seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki Sattai Puratchi Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sattai Puratchi Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchi Thaan Meetchi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi Thaan Meetchi Thaan"/>
</div>
</pre>
