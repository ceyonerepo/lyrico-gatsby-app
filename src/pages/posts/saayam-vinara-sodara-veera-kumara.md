---
title: "saayam song lyrics"
album: "Vinara Sodara Veera Kumara"
artist: "Shravan Bharadwaj"
lyricist: "Laxmi Bhupala"
director: "Sateesh Chandra Nadella"
path: "/albums/vinara-sodara-veera-kumara-lyrics"
song: "Saayam"
image: ../../images/albumart/vinara-sodara-veera-kumara.jpg
date: 2019-03-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/340g3B4o4ng"
type: "love"
singers:
  - Sri Krishna Vishnubhotla
  - Kavya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saayam chirusaayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam chirusaayam "/>
</div>
<div class="lyrico-lyrics-wrapper">vyavasaayam priyagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vyavasaayam priyagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasainapraayam bidiyaalu maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasainapraayam bidiyaalu maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadam anunaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadam anunaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">adi modam rati vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi modam rati vedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanu vaaravaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanu vaaravaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">tamakaala swedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamakaala swedam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haayi varadalaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi varadalaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nemalaaye kannula neerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nemalaaye kannula neerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudaaye gadasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudaaye gadasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Saramaaye magasiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saramaaye magasiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasamaaye sogasiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasamaaye sogasiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakha chihname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakha chihname"/>
</div>
<div class="lyrico-lyrics-wrapper">sukha sikharamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukha sikharamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayam chirusaayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam chirusaayam "/>
</div>
<div class="lyrico-lyrics-wrapper">vyavasaayam priyagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vyavasaayam priyagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasainapraayam bidiyaalu maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasainapraayam bidiyaalu maayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedalu jatagaa naliginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedalu jatagaa naliginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padulu jagalam palikenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padulu jagalam palikenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudulu tirige kadalilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudulu tirige kadalilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanuvu alalaa egiri padadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanuvu alalaa egiri padadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samayam sankellato
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samayam sankellato"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapadame teliyadelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapadame teliyadelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee samaram ye nidhiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee samaram ye nidhiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kshaname telisindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kshaname telisindaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayam chirusaayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam chirusaayam "/>
</div>
<div class="lyrico-lyrics-wrapper">vyavasaayam priyagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vyavasaayam priyagaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasainapraayam bidiyaalu maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasainapraayam bidiyaalu maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadam anunaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadam anunaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">adi modam rati vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi modam rati vedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanu vaaravaadam tamakaala swedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanu vaaravaadam tamakaala swedam"/>
</div>
</pre>
