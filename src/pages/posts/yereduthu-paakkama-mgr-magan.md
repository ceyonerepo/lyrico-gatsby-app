---
title: "yereduthu paakkama song lyrics"
album: "MGR Magan"
artist: "Anthony Daasan"
lyricist: "Muragan Manthiram"
director: "Ponram"
path: "/albums/mgr-magan-lyrics"
song: "Yereduthu Paakkama"
image: ../../images/albumart/mgr-magan.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WqxxC2nMdGU"
type: "love"
singers:
  - Anthony Daasan
  - Pooja Vaidyanath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yereduthu paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yereduthu paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu thaan kekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu thaan kekkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellaam neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellaam neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yereduthu paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yereduthu paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu thaan kekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu thaan kekkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellaam neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellaam neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi yendi en manasukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi yendi en manasukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Podura thoondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podura thoondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Una thaandi naan kadavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una thaandi naan kadavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuren vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuren vendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yereduthu paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yereduthu paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu thaan kekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu thaan kekkama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennanemmo aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanemmo aasai enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pannuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pannuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna naanum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna naanum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum kelu solluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum kelu solluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanaiyo kannu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaiyo kannu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti paakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti paakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda neeyum paakkum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda neeyum paakkum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattai verkkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattai verkkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therkku dhisai kaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therkku dhisai kaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi ennai serthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi ennai serthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyarettu naathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyarettu naathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathurukken yaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathurukken yaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaitheera vaazhanumdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaitheera vaazhanumdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae odi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae odi vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yereduthu paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yereduthu paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu thaan kekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu thaan kekkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellaam neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellaam neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalippen unnai naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalippen unnai naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru jenmamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru jenmamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamellaam kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellaam kaathiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanadi endhan vaazhvin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanadi endhan vaazhvin"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum sorgamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum sorgamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogathadi thalli neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogathadi thalli neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona naragamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona naragamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veecharuva pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veecharuva pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappu keera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappu keera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachai mala chola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai mala chola"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo unna sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo unna sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethanada vaazhkai motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanada vaazhkai motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yereduthu paakkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yereduthu paakkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu thaan kekkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu thaan kekkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellaam neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellaam neeyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi yendi en manasukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi yendi en manasukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Podura thoondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podura thoondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thaandi naan kadavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thaandi naan kadavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuren vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuren vendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mm mm"/>
</div>
</pre>
