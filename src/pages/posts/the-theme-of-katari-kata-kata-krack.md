---
title: "the theme of katari song lyrics"
album: "Krack"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Gopichand Malineni"
path: "/albums/krack-lyrics"
song: "The Theme of Katari - Kata Kata Kata"
image: ../../images/albumart/krack.jpg
date: 2021-01-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LRAVdQ5XPzI"
type: "theme song"
singers:
  - Sri Krishna
  - Sai Charan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raa raa raa ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa raa ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra raa ra ra raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra raa ra ra raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa raa ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa raa ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra raa ra ra raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra raa ra ra raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa, raaa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa, raaa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa, raaa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa, raaa raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kata kata kata katarodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kata kata kata katarodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasa kasa kasa kasayodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasa kasa kasa kasayodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karakuna konde bandarayi gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakuna konde bandarayi gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada dhada dhada puttisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada dhada dhada puttisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gada gada vanikisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada gada vanikisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi Kannupadithe keedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi Kannupadithe keedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru allallaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru allallaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaariponi padunu veta kathilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaariponi padunu veta kathilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Araachakaanike vodikadathaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araachakaanike vodikadathaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shavaala guttalekki mettu mettuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shavaala guttalekki mettu mettuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongole gadapaina thodagodathadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongole gadapaina thodagodathadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kata kata kata kata kata kata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata kata kata kata kata kata"/>
</div>
<div class="lyrico-lyrics-wrapper">kata kata kata kata kata kata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata kata kata kata kata kata"/>
</div>
<div class="lyrico-lyrics-wrapper">kata kata kata kata kata kata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata kata kata kata kata kata"/>
</div>
<div class="lyrico-lyrics-wrapper">kata kata kata kata kata kata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata kata kata kata kata kata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa raa raa ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa raa ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra raa ra ra raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra raa ra ra raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa raa ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa raa ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra raa ra ra raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra raa ra ra raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa, raaa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa, raaa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa, raaa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa, raaa raa"/>
</div>
</pre>
