---
title: 'dharala prabhu title track song lyrics'
album: 'Dharala Prabhu'
artist: 'Anirudh Ravichander'
lyricist: 'Vignesh Shivan'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Dharala Prabhu Title Track'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Anirudh Ravichander
youtubeLink: 'https://www.youtube.com/embed/3ZKf8yMfTGM'
type: 'folk'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Yeh paakku vethala
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh paakku vethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi mudichi paiyan vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathi mudichi paiyan vandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh poova thoduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh poova thoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela madichi ponnu vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sela madichi ponnu vandhaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kandathaa pesi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kandathaa pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Time waste pannaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Time waste pannaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyan thangom
<input type="checkbox" class="lyrico-select-lyric-line" value="Paiyan thangom"/>
</div>
<div class="lyrico-lyrics-wrapper">Missu pannaadhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Missu pannaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeh paakku vethala
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh paakku vethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi mudichi paiyan vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathi mudichi paiyan vandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh poova thoduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh poova thoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela madichi ponnu vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sela madichi ponnu vandhaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pee peee
<input type="checkbox" class="lyrico-select-lyric-line" value="Pee peee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeh vandhana
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh vandhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna izhudhidava sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna izhudhidava sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipdi vandhanaa nalla irundhidalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ipdi vandhanaa nalla irundhidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar weight-unnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar weight-unnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Test vachidaalam nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Test vachidaalam nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pudichidhaan kandupudichidalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai pudichidhaan kandupudichidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee onnu naa rendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee onnu naa rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee moonu naa naalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee moonu naa naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha deal-u irundhaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha deal-u irundhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life coolo coolu
<input type="checkbox" class="lyrico-select-lyric-line" value="Life coolo coolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manasellaam love oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasellaam love oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam senjaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalyaanam senjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazh naalu muzhusaavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazh naalu muzhusaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Only happy feel-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Only happy feel-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aattam poda
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neram vandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba kodukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Anba kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu kedachaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalu kedachaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeh paakku vethala
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh paakku vethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi mudichi paiyan vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathi mudichi paiyan vandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh poova thoduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh poova thoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela madichi ponnu vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sela madichi ponnu vandhaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pee peee
<input type="checkbox" class="lyrico-select-lyric-line" value="Pee peee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saaruu saaru yaarunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaruu saaru yaarunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli kudukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli alli kudukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharala prabhu doi
<input type="checkbox" class="lyrico-select-lyric-line" value="Dharala prabhu doi"/>
</div>
</pre>