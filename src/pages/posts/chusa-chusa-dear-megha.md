---
title: "chusa chusa song lyrics"
album: "Dear Megha"
artist: "Gowra Hari"
lyricist: "Krishna Kanth"
director: "A. Sushanth Reddy"
path: "/albums/dear-megha-lyrics"
song: "Chusa Chusa"
image: ../../images/albumart/dear-megha.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YZEJiwp3pJ0"
type: "love"
singers:
  - Sahithi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kante padakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kante padakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakante istanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakante istanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake nacche nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake nacche nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Novvocchaka leney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novvocchaka leney"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalanna badhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalanna badhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhute padadhe o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhute padadhe o"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kante padakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kante padakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakante istanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakante istanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello untune chesthavu gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello untune chesthavu gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougillo chereti rojemo radha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougillo chereti rojemo radha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone modhlayye poorthayye rojey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone modhlayye poorthayye rojey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppedham maatemo raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppedham maatemo raadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona nene naa o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona nene naa o"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kante padakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kante padakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa chusa chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa chusa chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakante istanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakante istanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennenni mallunna iddari madhya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni mallunna iddari madhya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhurale mincheti premundhe lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhurale mincheti premundhe lera"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello dachoddhu anutundho maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello dachoddhu anutundho maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangarey nilavanule ento o chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangarey nilavanule ento o chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolona nena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolona nena"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kante padakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kante padakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusa ninu chusa ninu chusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusa ninu chusa ninu chusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakante istanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakante istanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake nacche nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake nacche nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Novvocchaka leney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novvocchaka leney"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalanna badhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalanna badhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhute padithe o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhute padithe o"/>
</div>
</pre>
