---
title: "ninne ninne song lyrics"
album: "Aswathama"
artist: "Sricharan Pakala"
lyricist: "V N V Ramesh Kumar"
director: "Ramana Teja"
path: "/albums/aswathama-lyrics"
song: "Ninne Ninne Yadhalo Ninne"
image: ../../images/albumart/aswathama.jpg
date: 2020-01-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lMoFORb-z10"
type: "love"
singers:
  - Armaan Malik
  - Yaamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninne Ninne Yadhalo Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Yadhalo Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Neekayi Ne Vechanu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Neekayi Ne Vechanu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupe Radhe Adhupe Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupe Radhe Adhupe Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Samayam Saripodhu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Samayam Saripodhu Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adharale Madhuram Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharale Madhuram Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisaye Yekam Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisaye Yekam Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahale Dhuram Gaa Ninu Cheranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahale Dhuram Gaa Ninu Cheranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amavase Punnami Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amavase Punnami Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoche Nuvu Navanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoche Nuvu Navanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilo Nanu Choosaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilo Nanu Choosaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Nene Marichenu Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Nene Marichenu Ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Yadhalo Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Yadhalo Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Neekai Ne Vechanu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Neekai Ne Vechanu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupe Raadhe Adhupey Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupe Raadhe Adhupey Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Samayam Saripodhu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Samayam Saripodhu Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Gundelo Priya Ragale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelo Priya Ragale"/>
</div>
<div class="lyrico-lyrics-wrapper">Moge Nee Kanu Saigallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moge Nee Kanu Saigallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kannullo Cheli Andhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kannullo Cheli Andhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalige Nee Naduvompullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalige Nee Naduvompullo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalo Ilalo Prathi Oohallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo Ilalo Prathi Oohallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naa Kanupapallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naa Kanupapallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhalo Thudhalo Prathi Gadiyallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalo Thudhalo Prathi Gadiyallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Nuvve Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Nuvve Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adharale Madhuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharale Madhuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisaye Yekam’ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisaye Yekam’ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahale Dhooram’ga Ninu Cheranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahale Dhooram’ga Ninu Cheranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Amavaase Punnami’ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amavaase Punnami’ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoche Nuvu Navvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoche Nuvu Navvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Nanu Choosaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nanu Choosaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Nene Marichenu’ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Nene Marichenu’ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninney Ninney Yadhalo Ninney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninney Ninney Yadhalo Ninney"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Paniga Ninu Thalichaanu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Paniga Ninu Thalichaanu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupe Radhe Adhupe Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupe Radhe Adhupe Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Samayam Saripodhu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Samayam Saripodhu Le"/>
</div>
</pre>
