---
title: "maayya maayya song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Bhaskarabhatla"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "Maayya Maayya"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TLcp7R8Uz3M"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gaba Gaba Suryudinemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba Gaba Suryudinemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammani Pilicheddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammani Pilicheddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaba Gaba Ravvadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba Gaba Ravvadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadrudni Aaapeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadrudni Aaapeddam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasekkada Kerchief Vesimdho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasekkada Kerchief Vesimdho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Manamakkada Janda Patheddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Manamakkada Janda Patheddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Banthi Boundary Daatina Freedom Idhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banthi Boundary Daatina Freedom Idhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Chal Chal Chalchal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chal Chal Chalchal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegaa Vegaa Lagimcheddam Maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaa Vegaa Lagimcheddam Maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Ni Baaga Banthi Aadeddam Maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ni Baaga Banthi Aadeddam Maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddam Vartha Iragesaddam Maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddam Vartha Iragesaddam Maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase Railu Kootha Aripimcheddam Maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase Railu Kootha Aripimcheddam Maya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaba Gaba Suryudinemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba Gaba Suryudinemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammani Pilicheddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammani Pilicheddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaba Gaba Ravvadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba Gaba Ravvadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadrudni Aaapeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadrudni Aaapeddam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevvadani Ye Naadu Thakkuvaga Chododdu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvadani Ye Naadu Thakkuvaga Chododdu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalona Lenidhi Yedho Pakkodiki Undochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalona Lenidhi Yedho Pakkodiki Undochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani Cheyyani Gadiyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Cheyyani Gadiyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathiroju Gamanisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathiroju Gamanisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayani Rendu Sarlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayani Rendu Sarlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariga Chupisthumdhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariga Chupisthumdhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sangathi Eppudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sangathi Eppudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipettam Ganakane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipettam Ganakane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Na Sneham Inthala Aaduthu paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Na Sneham Inthala Aaduthu paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthu Thulluthunnadhi Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthu Thulluthunnadhi Gaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh mayya mayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh mayya mayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khushi La Still Iddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khushi La Still Iddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jackson La Step Veddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jackson La Step Veddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tendulkar Stickerlanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tendulkar Stickerlanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelapai Antiddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelapai Antiddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cooling Gaa Setteddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cooling Gaa Setteddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Collar Paiki Yegareddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collar Paiki Yegareddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedochina Pulsar Laa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedochina Pulsar Laa "/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantha Tirigeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantha Tirigeddam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Tirige Range Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Tirige Range Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Coloring Ye Kekaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coloring Ye Kekaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpeyyara Kallalo Vandhellaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpeyyara Kallalo Vandhellaki "/>
</div>
<div class="lyrico-lyrics-wrapper">Saripada Rangula Pandagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripada Rangula Pandagala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegaa Vegaa Lagimcheddam Maayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaa Vegaa Lagimcheddam Maayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Ni Baaga Banthi Aadeddam Maayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ni Baaga Banthi Aadeddam Maayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddam Vartha Iragesaddam Mayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddam Vartha Iragesaddam Mayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase Railu Kootha Aripimcheddam Mayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase Railu Kootha Aripimcheddam Mayya"/>
</div>
</pre>
