---
title: "nee sirichalum song lyrics"
album: "Action"
artist: "Hiphop Tamizha"
lyricist: "Pa. Vijay"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Nee Sirichalum"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7-EnFSdtRCE"
type: "love"
singers:
  - Sadhana Sargam
  - Jonita Gandhi
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Sirichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sirichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Morachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Morachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Ninachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Ninachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Erichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Erichaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Inichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Inichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Valichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Valichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Anaichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Anaichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Podhachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Podhachaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Nee Seigiraai Yukthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Nee Seigiraai Yukthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Unnodu Dhaan Suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Unnodu Dhaan Suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaana Kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaana Kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaarthaigal Kuththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaarthaigal Kuththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naano Un Paarvaiyil Sikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Un Paarvaiyil Sikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Mudiyaamalae Thikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Mudiyaamalae Thikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroodum Pesaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroodum Pesaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu Pogirom Sokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Pogirom Sokki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sirichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sirichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Morachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Morachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Ninachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Ninachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Erichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Erichaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiththom Thana Thiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththom Thana Thiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thiththom Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thiththom Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththom Thana Thiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththom Thana Thiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thiththom Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thiththom Thana Theem Theem Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiththom Thana Thiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththom Thana Thiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thiththom Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thiththom Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Theem Theem Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhaiththu Vaitha Kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhaiththu Vaitha Kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkalukkul Thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalukkul Thedavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Vandha Aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Vandha Aasaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhalukkkul Soodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhalukkkul Soodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjirukum Varaikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjirukum Varaikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivu Irukkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivu Irukkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrilaa Veliyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilaa Veliyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Kidakkiren Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kidakkiren Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mounamaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mounamaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Seidhu Kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Seidhu Kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Kaadhal Illai Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Kaadhal Illai Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi Polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Maatri Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Maatri Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Tholil Saaindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Tholil Saaindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Tholainthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Tholainthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Madiyil Thoongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Madiyil Thoongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Madinthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Madinthaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Tholil Saaindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Tholil Saaindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Tholainthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Tholainthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Madiyil Thoongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Madiyil Thoongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Madinthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Madinthaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Unna Nenachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Unna Nenachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Kanna Muzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Kanna Muzhichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Otti Anachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Otti Anachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula Katti Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula Katti Pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannu Muzhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannu Muzhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kandu Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kandu Pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Seththu Pozhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Seththu Pozhachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Veesum Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Veesum Kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thaandiyae Engu Selgiraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandiyae Engu Selgiraayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Paalai Veiyilil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Paalai Veiyilil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Indriyae Ennai Kolgiraayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Indriyae Ennai Kolgiraayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Tholil Saaindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Tholil Saaindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Tholainthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Tholainthaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Madiyil Thoongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Madiyil Thoongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Madinthaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Madinthaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sirichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sirichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Morachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Morachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Ninachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Ninachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Erichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Erichaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Inichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Inichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Valichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Valichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Anaichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Anaichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Podhachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Podhachaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Nee Seigiraai Yukthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Nee Seigiraai Yukthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Unnodu Dhaan Suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Unnodu Dhaan Suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaana Kaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaana Kaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaarthaigal Kuththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaarthaigal Kuththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naano Un Paarvaiyil Sikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Un Paarvaiyil Sikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Mudiyaamalae Thikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Mudiyaamalae Thikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroodum Pesaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroodum Pesaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu Pogirom Sokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Pogirom Sokki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiththom Thana Thiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththom Thana Thiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thiththom Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thiththom Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththom Thana Thiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththom Thana Thiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thiththom Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thiththom Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Theem Theem Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Theem Theem Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Theem Theem Thaana"/>
</div>
</pre>
