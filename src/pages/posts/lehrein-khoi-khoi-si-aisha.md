---
title: "lehrein song lyrics"
album: "Aisha"
artist: "Amit Trivedi"
lyricist: "Javed Akhtar"
director: "Rajshree Ojha"
path: "/albums/aisha-lyrics"
song: "Lehrein - Khoi Khoi si"
image: ../../images/albumart/aisha.jpg
date: 2010-08-06
lang: hindi
youtubeLink: "https://www.youtube.com/embed/WYDc1JQqduY"
type: "happy"
singers:
  - Anusha Mani
  - Neuman Pinto
  - Nikhil D'Souza
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Khoi khoi si hun main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoi khoi si hun main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun yeh dil ka haal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun yeh dil ka haal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhundli saari khwab hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhundli saari khwab hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulja har khayal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulja har khayal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari kaliyan murja gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari kaliyan murja gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang unke yaadon me reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang unke yaadon me reh gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare gharande reet ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare gharande reet ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Lehrein aayi lehron me beh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lehrein aayi lehron me beh gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rah me kal kitni chirag the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rah me kal kitni chirag the"/>
</div>
<div class="lyrico-lyrics-wrapper">Samne kal phoolon ki bhag thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samne kal phoolon ki bhag thi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss se kahun kaun hai jo sune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss se kahun kaun hai jo sune"/>
</div>
<div class="lyrico-lyrics-wrapper">Kante hi kyun maine hai chune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kante hi kyun maine hai chune"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapne mere kyun hai kho gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapne mere kyun hai kho gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jage hain kyun dil me gum mere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jage hain kyun dil me gum mere"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare kaliyan murja gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare kaliyan murja gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang unke yaadon me reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang unke yaadon me reh gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare gharande reet ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare gharande reet ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Lehrein aayi lehron me beh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lehrein aayi lehron me beh gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na na na……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na na na……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya kahun kyun ye dil udas hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya kahun kyun ye dil udas hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab koi door hai na paas hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab koi door hai na paas hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Choo le jo dil wo baatein ab kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choo le jo dil wo baatein ab kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Wo din kahan raatein ab kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wo din kahan raatein ab kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi dhakhal hai ab khawab sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi dhakhal hai ab khawab sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab dil mera hai betab sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab dil mera hai betab sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari kaliyan murja gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari kaliyan murja gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang unke yaadon me reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang unke yaadon me reh gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare gharande reet ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare gharande reet ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Lehrein aayi lehron me beh gayi beh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lehrein aayi lehron me beh gayi beh gaye"/>
</div>
</pre>
