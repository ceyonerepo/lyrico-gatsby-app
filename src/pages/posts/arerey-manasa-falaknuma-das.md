---
title: "arerey manasa song lyrics"
album: "Falaknuma Das"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Vishwak Sen"
path: "/albums/falaknuma-das-lyrics"
song: "Arerey Manasa"
image: ../../images/albumart/falaknuma-das.jpg
date: 2019-05-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VfQrntggp7E"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Emannaavo Edatho Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emannaavo Edatho Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premanukonaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premanukonaa Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodakamunde Venake Nadiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodakamunde Venake Nadiche"/>
</div>
<div class="lyrico-lyrics-wrapper">Todokatundi Kalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Todokatundi Kalisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyade Adagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyade Adagadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edurai Nuvve Dorakatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edurai Nuvve Dorakatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayano Emito Emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayano Emito Emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idanthaa Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idanthaa Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagamu Sagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamu Sagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emannaavo Edatho Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emannaavo Edatho Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premanukonaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premanukonaa Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Brathukuna Ye Roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Brathukuna Ye Roju"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Parichayamavutunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Parichayamavutunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenadiginade Lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenadiginade Lede"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadanukuni Pothunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadanukuni Pothunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallugaa Naa Venakunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallugaa Naa Venakunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvenani Teliyadu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvenani Teliyadu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellaku Ammaga Maarina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellaku Ammaga Maarina"/>
</div>
<div class="lyrico-lyrics-wrapper">Tode Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tode Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooranthaa Maharaajainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa Maharaajainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ollo Padipoyaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ollo Padipoyaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daasudanaipoyaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daasudanaipoyaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idanthaa Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idanthaa Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagamu Sagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamu Sagamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenadigina Raagaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenadigina Raagaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pranayapu Mounaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pranayapu Mounaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kurula Sameeraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kurula Sameeraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vethikina Teeraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vethikina Teeraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaalluga Naa Udayaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalluga Naa Udayaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Edurainadi Soonyamu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edurainadi Soonyamu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Tolisaarigaa Nee Mukhamannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tolisaarigaa Nee Mukhamannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vekuvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vekuvale"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanaale Ara Chethullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanaale Ara Chethullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettisthu Naa Oopiritho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettisthu Naa Oopiritho"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhakame Chesthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhakame Chesthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idanthaa Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idanthaa Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagamu Sagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamu Sagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idanthaa Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idanthaa Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagamu Sagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamu Sagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idanthaa Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idanthaa Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagamu Sagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagamu Sagamaa"/>
</div>
</pre>
