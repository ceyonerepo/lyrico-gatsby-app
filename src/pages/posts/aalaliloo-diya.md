---
title: "aalaliloo song lyrics"
album: "Diya"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/diya-song-lyrics"
song: "Aalaliloo"
image: ../../images/albumart/diya.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gj4Zdf5HpyU"
type: "melody"
singers:
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aalaliloo Aalaliloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaliloo Aalaliloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paada Maranthida Kettal Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paada Maranthida Kettal Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Yena Muthe Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Yena Muthe Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Konjatha Un Chella Thugal Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Konjatha Un Chella Thugal Ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaliloo Aalaliloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaliloo Aalaliloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paada Maranthida Kettal Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paada Maranthida Kettal Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamale Sollamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamale Sollamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Endruthaan Unnai Azhaipaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Endruthaan Unnai Azhaipaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Veesiya Punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veesiya Punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal Varutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal Varutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvi Ondrai Nenjil Yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi Ondrai Nenjil Yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Adhai Sollave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Adhai Sollave"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vendam Yenave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vendam Yenave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen Paarthirunthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen Paarthirunthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Sollutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Sollutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannil Pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaigal Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthangal Ida Paarkindratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthangal Ida Paarkindratho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingethu Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingethu Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingethu Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingethu Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illatha Vali Mullagutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illatha Vali Mullagutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Ondru Sollagutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Ondru Sollagutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pol Aval Aavadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Aval Aavadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Unara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Unara"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhavillai Endrunardhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhavillai Endrunardhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaatraagiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaatraagiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oyivena Saigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oyivena Saigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thookam Avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thookam Avale"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Kanavil Thannai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kanavil Thannai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhangal Theigiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhangal Theigiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangal Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Chinna Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Chinna Nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannodu Ula Vaa Endradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannodu Ula Vaa Endradho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Nindra Agal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Nindra Agal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhvin Nagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhvin Nagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unulle Thaanai Vidhaikudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unulle Thaanai Vidhaikudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Unnai Udhaikudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Unnai Udhaikudho"/>
</div>
</pre>
