---
title: "swathi mutyapu jallai song lyrics"
album: "Thupaki Ramudu"
artist: "T Prabhakar "
lyricist: "Abhinaya Srinivas"
director: "T Prabhakar"
path: "/albums/thupaki-ramudu-lyrics"
song: "Swathi Mutyapu Jallai"
image: ../../images/albumart/thupaki-ramudu.jpg
date: 2019-10-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sQs4oIUxBNw"
type: "love"
singers:
  - Kousalya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">swathi muthyapu jallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swathi muthyapu jallai"/>
</div>
<div class="lyrico-lyrics-wrapper">kurisinthi aanantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurisinthi aanantham"/>
</div>
<div class="lyrico-lyrics-wrapper">manase thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">alalai yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alalai yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">harivillai virise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harivillai virise"/>
</div>
<div class="lyrico-lyrics-wrapper">innallu mabbullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innallu mabbullo"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaagina aakasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaagina aakasam"/>
</div>
<div class="lyrico-lyrics-wrapper">untundha inthandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untundha inthandha"/>
</div>
<div class="lyrico-lyrics-wrapper">ipude rekkalui
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipude rekkalui"/>
</div>
<div class="lyrico-lyrics-wrapper">thodigi saageee payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodigi saageee payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">swathi muthyapu jallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swathi muthyapu jallai"/>
</div>
<div class="lyrico-lyrics-wrapper">kurisinthi aanantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurisinthi aanantham"/>
</div>
<div class="lyrico-lyrics-wrapper">manase thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase thadise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholi agudai kadhilinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi agudai kadhilinchi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee chelimini panchavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chelimini panchavu "/>
</div>
<div class="lyrico-lyrics-wrapper">nanu naake chupinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanu naake chupinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nammakamai nadipavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammakamai nadipavu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnavu nenantu kalledhute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnavu nenantu kalledhute"/>
</div>
<div class="lyrico-lyrics-wrapper">nilichavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilichavu "/>
</div>
<div class="lyrico-lyrics-wrapper">ye vedhaina naadhaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vedhaina naadhaina "/>
</div>
<div class="lyrico-lyrics-wrapper">nee badhani thalichaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee badhani thalichaavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">swathi muthyapu jallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swathi muthyapu jallai"/>
</div>
<div class="lyrico-lyrics-wrapper">kurisinthi aanantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurisinthi aanantham"/>
</div>
<div class="lyrico-lyrics-wrapper">manase thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase thadise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa nallani aakasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa nallani aakasam"/>
</div>
<div class="lyrico-lyrics-wrapper">poochindhi punnamitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poochindhi punnamitho"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">virisindhi aasalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virisindhi aasalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">viluvainadhi abhimanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viluvainadhi abhimanam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalavanchadhu otamitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalavanchadhu otamitho"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhirathanu yedhirinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhirathanu yedhirinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nilabadadha dhairayamutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilabadadha dhairayamutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">swathi muthyapu jallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swathi muthyapu jallai"/>
</div>
<div class="lyrico-lyrics-wrapper">kurisinthi aanantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurisinthi aanantham"/>
</div>
<div class="lyrico-lyrics-wrapper">manase thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">alalai yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alalai yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">harivillai virise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harivillai virise"/>
</div>
<div class="lyrico-lyrics-wrapper">innallu mabbullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innallu mabbullo"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaagina aakasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaagina aakasam"/>
</div>
<div class="lyrico-lyrics-wrapper">untundha inthandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untundha inthandha"/>
</div>
<div class="lyrico-lyrics-wrapper">ipude rekkalui
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipude rekkalui"/>
</div>
<div class="lyrico-lyrics-wrapper">thodigi saageee payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodigi saageee payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">swathi muthyapu jallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swathi muthyapu jallai"/>
</div>
<div class="lyrico-lyrics-wrapper">kurisinthi aanantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurisinthi aanantham"/>
</div>
<div class="lyrico-lyrics-wrapper">manase thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase thadise"/>
</div>
</pre>
