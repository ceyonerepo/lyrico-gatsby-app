---
title: "komma kommana song lyrics"
album: "Thupaki Ramudu"
artist: "T Prabhakar "
lyricist: "Mittapally Surender"
director: "T Prabhakar"
path: "/albums/thupaki-ramudu-lyrics"
song: "Komma Kommana"
image: ../../images/albumart/thupaki-ramudu.jpg
date: 2019-10-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8ydBfj36VLc"
type: "happy"
singers:
  - Kousalya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">komma kommana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komma kommana"/>
</div>
<div class="lyrico-lyrics-wrapper">vennela kaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennela kaase"/>
</div>
<div class="lyrico-lyrics-wrapper">amma neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">bathukamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathukamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chudalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chudalani"/>
</div>
<div class="lyrico-lyrics-wrapper">puvula aaratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvula aaratam"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla nallani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla nallani"/>
</div>
<div class="lyrico-lyrics-wrapper">mabbulu veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mabbulu veede"/>
</div>
<div class="lyrico-lyrics-wrapper">jallulu nee kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallulu nee kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">mana palle thalliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana palle thalliki"/>
</div>
<div class="lyrico-lyrics-wrapper">alle poolaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alle poolaku"/>
</div>
<div class="lyrico-lyrics-wrapper">mosina ee paanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosina ee paanam"/>
</div>
<div class="lyrico-lyrics-wrapper">katta meeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta meeda"/>
</div>
<div class="lyrico-lyrics-wrapper">katta poolu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta poolu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaristhunte neekai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaristhunte neekai"/>
</div>
<div class="lyrico-lyrics-wrapper">eduru chusthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduru chusthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chettu meeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chettu meeda"/>
</div>
<div class="lyrico-lyrics-wrapper">chemanthule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chemanthule "/>
</div>
<div class="lyrico-lyrics-wrapper">seemantha maduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seemantha maduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sigalu korithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sigalu korithe"/>
</div>
<div class="lyrico-lyrics-wrapper">neeti bottula thamarakula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeti bottula thamarakula"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu mosthanu antunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu mosthanu antunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">komma kommana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komma kommana"/>
</div>
<div class="lyrico-lyrics-wrapper">vennela kaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennela kaase"/>
</div>
<div class="lyrico-lyrics-wrapper">amma neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">bathukamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathukamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chudalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chudalani"/>
</div>
<div class="lyrico-lyrics-wrapper">puvula aaratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvula aaratam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thummi puvvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thummi puvvula"/>
</div>
<div class="lyrico-lyrics-wrapper">navvulalo thumma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvulalo thumma "/>
</div>
<div class="lyrico-lyrics-wrapper">poovula saavadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovula saavadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">ammalakkala patalani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammalakkala patalani "/>
</div>
<div class="lyrico-lyrics-wrapper">malli vineedhennadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli vineedhennadani"/>
</div>
<div class="lyrico-lyrics-wrapper">koyila eduruchustunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyila eduruchustunte"/>
</div>
<div class="lyrico-lyrics-wrapper">kaallaku gajjalu katugoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaallaku gajjalu katugoni "/>
</div>
<div class="lyrico-lyrics-wrapper">dappu dharuvu kottamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dappu dharuvu kottamani"/>
</div>
<div class="lyrico-lyrics-wrapper">baata bimba aatalaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baata bimba aatalaade"/>
</div>
<div class="lyrico-lyrics-wrapper">adugula raaka ennadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugula raaka ennadani"/>
</div>
<div class="lyrico-lyrics-wrapper">adige cheruvu dharulani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adige cheruvu dharulani"/>
</div>
<div class="lyrico-lyrics-wrapper">puttamannutho poola bommaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puttamannutho poola bommaki"/>
</div>
<div class="lyrico-lyrics-wrapper">purudosedhi ennadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purudosedhi ennadani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">komma kommana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komma kommana"/>
</div>
<div class="lyrico-lyrics-wrapper">vennela kaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennela kaase"/>
</div>
<div class="lyrico-lyrics-wrapper">amma neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">bathukamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathukamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chudalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chudalani"/>
</div>
<div class="lyrico-lyrics-wrapper">puvula aaratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvula aaratam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sattu bellam panchakuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattu bellam panchakuni"/>
</div>
<div class="lyrico-lyrics-wrapper">aathmiyathalu allukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathmiyathalu allukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">andhari kalayika kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhari kalayika kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">hrudhayam vedhika chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hrudhayam vedhika chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">vechene gori polimera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechene gori polimera"/>
</div>
<div class="lyrico-lyrics-wrapper">allah yesu biddalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allah yesu biddalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku thodabuttinolle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku thodabuttinolle"/>
</div>
<div class="lyrico-lyrics-wrapper">antha ninnu kolichevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha ninnu kolichevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">mamathale kula mathalayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamathale kula mathalayi"/>
</div>
<div class="lyrico-lyrics-wrapper">piliche ninnu rammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piliche ninnu rammani"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde gundenu thadumukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde gundenu thadumukune"/>
</div>
<div class="lyrico-lyrics-wrapper">palle palakarimpulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle palakarimpulu"/>
</div>
<div class="lyrico-lyrics-wrapper">inti intilo aadabiddala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inti intilo aadabiddala"/>
</div>
<div class="lyrico-lyrics-wrapper">navvule indra dhanassulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvule indra dhanassulu"/>
</div>
<div class="lyrico-lyrics-wrapper">yedadhi podavuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedadhi podavuna "/>
</div>
<div class="lyrico-lyrics-wrapper">palle thalli kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle thalli kallalo "/>
</div>
<div class="lyrico-lyrics-wrapper">gunde gundenu thadumukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde gundenu thadumukune"/>
</div>
<div class="lyrico-lyrics-wrapper">palle palakarimpulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle palakarimpulu"/>
</div>
<div class="lyrico-lyrics-wrapper">inti intilo aadabiddala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inti intilo aadabiddala"/>
</div>
<div class="lyrico-lyrics-wrapper">navvule indra dhanassulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvule indra dhanassulu"/>
</div>
<div class="lyrico-lyrics-wrapper">yedadhi podavuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedadhi podavuna "/>
</div>
<div class="lyrico-lyrics-wrapper">palle thalli kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle thalli kallalo "/>
</div>
<div class="lyrico-lyrics-wrapper">gunde gundenu thadumukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde gundenu thadumukune"/>
</div>
<div class="lyrico-lyrics-wrapper">palle palakarimpulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle palakarimpulu"/>
</div>
<div class="lyrico-lyrics-wrapper">inti intilo aadabiddala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inti intilo aadabiddala"/>
</div>
<div class="lyrico-lyrics-wrapper">navvule indra dhanassulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvule indra dhanassulu"/>
</div>
<div class="lyrico-lyrics-wrapper">yedadhi podavuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedadhi podavuna "/>
</div>
<div class="lyrico-lyrics-wrapper">palle thalli kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle thalli kallalo "/>
</div>
<div class="lyrico-lyrics-wrapper">gunde gundenu thadumukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde gundenu thadumukune"/>
</div>
<div class="lyrico-lyrics-wrapper">palle palakarimpulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle palakarimpulu"/>
</div>
<div class="lyrico-lyrics-wrapper">inti intilo aadabiddala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inti intilo aadabiddala"/>
</div>
<div class="lyrico-lyrics-wrapper">navvule indra dhanassulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvule indra dhanassulu"/>
</div>
<div class="lyrico-lyrics-wrapper">yedadhi podavuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedadhi podavuna "/>
</div>
<div class="lyrico-lyrics-wrapper">palle thalli kallalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle thalli kallalo "/>
</div>
<div class="lyrico-lyrics-wrapper">marapuraani guruthalakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapuraani guruthalakai"/>
</div>
<div class="lyrico-lyrics-wrapper">malli edhuru chupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli edhuru chupulu"/>
</div>
</pre>
