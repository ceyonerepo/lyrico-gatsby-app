---
title: "saayaali song lyrics"
album: "Adanga Maru"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Karthik Thangavel"
path: "/albums/adanga-maru-lyrics"
song: "Saayaali"
image: ../../images/albumart/adanga-maru.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DYFORxnDHX8"
type: "love"
singers:
  - Sathyaprakash
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm nagam vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm nagam vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol mugavari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol mugavari"/>
</div>
<div class="lyrico-lyrics-wrapper">Un udal vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udal vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen anumathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen anumathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeratha neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayatha naal mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayatha naal mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazha vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazha vehnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaratha aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaratha aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyamal thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyamal thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal neram paaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal neram paaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol saaya venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol saaya venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul nee boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul nee boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee veroodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee veroodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aali pol suzhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aali pol suzhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbillaana veedu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbillaana veedu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Or uyir polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or uyir polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal vazhum koodu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal vazhum koodu ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aali pol suzhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aali pol suzhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbillaana veedu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbillaana veedu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Or uyir polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or uyir polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal vazhum koodu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal vazhum koodu ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh thoorathu vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thoorathu vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pola santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pola santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum engal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum engal "/>
</div>
<div class="lyrico-lyrics-wrapper">veettukul kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettukul kottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallangal illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallangal illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasangal kondu ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasangal kondu ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrum ingae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrum ingae "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalattu meetkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalattu meetkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaigal ver illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigal ver illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalum vazhnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalum vazhnthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbinil vazhgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbinil vazhgirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam koodida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam koodida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithayae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithayae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee verodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee verodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethaaiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethaaiyae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaa naaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaa naaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyil vegum or nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil vegum or nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodavae nee varaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodavae nee varaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooru neeyum yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru neeyum yosanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thei nila aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thei nila aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram neeyum pogayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram neeyum pogayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ula pogalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ula pogalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodal koodum velayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodal koodum velayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannin saaralthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannin saaralthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un bimba meeralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un bimba meeralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorpikka thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorpikka thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi nenjam mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi nenjam mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anbin baarangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anbin baarangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno en nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno en nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillakka thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillakka thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen ini thaa matham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen ini thaa matham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa udan vazhnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa udan vazhnthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatkalum theerumun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkalum theerumun"/>
</div>
<div class="lyrico-lyrics-wrapper">Servom vazhnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Servom vazhnthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae boogambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithayae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithayae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee verodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee verodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethaaiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethaaiyae nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan ketkum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan ketkum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukul vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukul vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam enbaennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam enbaennaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh saayaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae en saayaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae en saayaali"/>
</div>
</pre>
