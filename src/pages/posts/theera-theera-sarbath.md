---
title: "theera theera song lyrics"
album: "Sarbath"
artist: "Ajesh"
lyricist: "Ku. Karthick"
director: "Prabhakaran"
path: "/albums/sarbath-song-lyrics"
song: "Theera Theera"
image: ../../images/albumart/sarbath.jpg
date: 2021-04-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OgPEJMVUACU"
type: "Love"
singers:
  - Ajesh
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theera theera ennai kolladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera ennai kolladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae sirikkadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae sirikkadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal endrum poigal solladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal endrum poigal solladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae nadikkadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae nadikkadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal podum naththai kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal podum naththai kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pesum vaarthai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pesum vaarthai aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal podum thaththai thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal podum thaththai thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thaedi thaavudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thaedi thaavudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Or nodiyil nuzhainthai uyiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or nodiyil nuzhainthai uyiril"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naan siraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naan siraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kaidhu seidhu pogum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kaidhu seidhu pogum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Or mazhaiyil karaindhen thuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or mazhaiyil karaindhen thuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthaai nadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthaai nadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal kaadhal sollu vaattadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal kaadhal sollu vaattadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurunthagaval thiraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunthagaval thiraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaigalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaigalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru kuru punnagaiyai thoovudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru kuru punnagaiyai thoovudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduthiraiyil thavazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduthiraiyil thavazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peruviralaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peruviralaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvizhi un azhagai theendudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvizhi un azhagai theendudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho thedum ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho thedum ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduvarai mounam valarpaaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduvarai mounam valarpaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalam pesaamal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalam pesaamal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai un kaadhalai sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai un kaadhalai sollividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodum nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungiyadhae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungiyadhae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinnaal vandhindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pinnaal vandhindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangudhu needhan ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangudhu needhan ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnalin osai minmini aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalin osai minmini aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Min thadaiyaalae ooyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min thadaiyaalae ooyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhi podum atcharam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhi podum atcharam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innisai polae paayuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innisai polae paayuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaa mazhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaa mazhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaindhae nadappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaindhae nadappomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam sarindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam sarindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukaigal sera kaadhal koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukaigal sera kaadhal koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Or nodiyil nuzhainthai uyiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or nodiyil nuzhainthai uyiril"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naan siraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naan siraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kaidhu seidhu pogum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kaidhu seidhu pogum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Or mazhaiyil karaindhen thuliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or mazhaiyil karaindhen thuliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthaai nadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthaai nadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal kaadhal sollu vaattadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal kaadhal sollu vaattadhae"/>
</div>
</pre>
