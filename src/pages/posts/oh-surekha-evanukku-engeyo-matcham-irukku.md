---
title: "oh surekha song lyrics"
album: "Evanukku Engeyo Matcham Irukku"
artist: "Natarajan Sankaran"
lyricist: "Viveka"
director: "A R Mukesh"
path: "/albums/evanukku-engeyo-matcham-irukku-lyrics"
song: "Oh Surekha"
image: ../../images/albumart/evanukku-engeyo-matcham-irukku.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9OVT9F1Zsbs"
type: "love"
singers:
  - M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella paakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella paakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paarvaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paarvaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam thaakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam thaakkuraanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam kekkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam kekkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham bodhaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham bodhaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththam thalaikkerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththam thalaikkerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada azhuchattiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada azhuchattiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba perusaagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba perusaagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye anganga ivan kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye anganga ivan kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhanjoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhanjoduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aan paavam venaannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan paavam venaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam solluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam solluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aanaalum imsai romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aanaalum imsai romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai meeri pooguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai meeri pooguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa haa aaaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa haa aaaa haaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aaaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaaa haaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa haa aaaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa haa aaaa haaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aaaa haaa haahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaaa haaa haahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh surekha surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh surekha surekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa surekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh surekha surekhaVaa surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh surekha surekhaVaa surekha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadaiyaaga pesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadaiyaaga pesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">En killaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En killaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhula sindhu paaduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhula sindhu paaduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elaam ippo kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaam ippo kekkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">En munnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En munnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna naan paathukaakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna naan paathukaakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaani thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kalavaaduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kalavaaduraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye koochaththaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye koochaththaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kodai saaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kodai saaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaarumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thaalikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thaalikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada paaththi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada paaththi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni paacha edam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni paacha edam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeduraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeduraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh surekha surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh surekha surekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa surekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh surekha surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh surekha surekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa surekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa surekha"/>
</div>
</pre>