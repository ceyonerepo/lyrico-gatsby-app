---
title: "unnai paartha naal song lyrics"
album: "Kalathil Santhippom"
artist: "Yuvan Shankar Raja"
lyricist: "Pa. Vijay"
director: "N. Rajasekar"
path: "/albums/kutty-kalathil-santhippom-lyrics"
song: "Unnai Paartha Naal"
image: ../../images/albumart/kalathil-santhippom.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8Ojg_dxMXbY"
type: "Love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai paartha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paartha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paartha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paartha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan vazhvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan vazhvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennai paartha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennai paartha naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jannalora eera saaralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jannalora eera saaralai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindru sindhi sendrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindru sindhi sendrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna naanum seivatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna naanum seivatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji pesi kolgirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji pesi kolgirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaradi yaaradi sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaradi yaaradi sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pine mara pookkalin selfie-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pine mara pookkalin selfie-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maanaseega mafia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maanaseega mafia"/>
</div>
 <div class="lyrico-lyrics-wrapper">Nee konjam kadhal solviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjam kadhal solviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaradi yaaradi sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaradi yaaradi sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pine mara pookkalin selfie-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pine mara pookkalin selfie-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maanaseega mafia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maanaseega mafia"/>
</div>
 <div class="lyrico-lyrics-wrapper">Nee konjam kadhal solviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjam kadhal solviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inippu saalaiyil erumbu pol nadakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inippu saalaiyil erumbu pol nadakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippu koodaiyil pandhu pol urulgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippu koodaiyil pandhu pol urulgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee panju pookkalal pinniya piraviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee panju pookkalal pinniya piraviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi moodi kondu thaan poga naan thuraviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi moodi kondu thaan poga naan thuraviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un veettai thaandi pogum neramthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veettai thaandi pogum neramthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum moochu vaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum moochu vaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal seigal seiyyum bothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigal seigal seiyyum bothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal megam thaanduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal megam thaanduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En swasathin vaasathai eerthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasathin vaasathai eerthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochinil moozhigai serthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moochinil moozhigai serthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvil vaanjai vaarthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvil vaanjai vaarthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En veettukaaga poothaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veettukaaga poothaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En swasathin vaasathai eerthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasathin vaasathai eerthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochinil moozhigai serthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moochinil moozhigai serthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvil vaanjai vaarthaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvil vaanjai vaarthaval"/>
</div>
<div class="lyrico-lyrics-wrapper">En veettukaaga poothaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veettukaaga poothaval"/>
</div>
</pre>
