---
title: "chalmaar song lyrics"
album: "Devi 2"
artist: "Sam C. S."
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/devi-2-lyrics"
song: "Chalmaar"
image: ../../images/albumart/devi-2.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n_fA0hU5-a4"
type: "love"
singers:
  - Benny Dayal
  - Blaaze
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chalmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalmaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu Mani Vaakkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Mani Vaakkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchayappaa’s Roadula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchayappaa’s Roadula"/>
</div>
<div class="lyrico-lyrics-wrapper">Short Skirt Oh Jennifer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Short Skirt Oh Jennifer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adithadi Nadakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithadi Nadakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettu Kuththu Vilugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu Kuththu Vilugala"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula Thaakuraa Jennifer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Thaakuraa Jennifer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellame Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellame Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamaa Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamaa Paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu Kiththu Povendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu Kiththu Povendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sari Sari Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sari Sari Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumura Sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumura Sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Polappendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Polappendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appappaa Hippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappaa Hippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Summeru Winter Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summeru Winter Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bend It Like Beckkam Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bend It Like Beckkam Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalmaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Feel Kannil Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Feel Kannil Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">No Ball Penne Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Ball Penne Edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kaadhalukku Naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kaadhalukku Naduvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Screen Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Screen Edharku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-il Tension Edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-il Tension Edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Light-ah Siri Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light-ah Siri Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sirippula Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sirippula Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Loveula Vilundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loveula Vilundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Loveula Ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loveula Ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi Viluvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi Viluvomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Penne Penne Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Penne Penne Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovers Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovers Aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil Parappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Parappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appappaa Hippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappaa Hippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Summeru Winter Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summeru Winter Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bend It Like Beckkam Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bend It Like Beckkam Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalmaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalmaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna Endhan Peruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna Endhan Peruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">God Of Love Naanuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="God Of Love Naanuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Brindhavana Thottathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Brindhavana Thottathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Queen Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Queen Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiss Me Single Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss Me Single Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch Me Mingle Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch Me Mingle Aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thananthani Desertula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thananthani Desertula"/>
</div>
<div class="lyrico-lyrics-wrapper">Rain Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rain Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Full Moon Naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moon Naduvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow Pola Azhagaa Irukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow Pola Azhagaa Irukkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Tea Kada Boiler Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Tea Kada Boiler Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Soodaa Kodhikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Soodaa Kodhikkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appappaa Hippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappaa Hippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Summeru Winter Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summeru Winter Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bend It Like Beckkam Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bend It Like Beckkam Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalmaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalmaar"/>
</div>
</pre>
