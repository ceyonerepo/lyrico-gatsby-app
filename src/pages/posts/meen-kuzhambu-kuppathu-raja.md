---
title: "meen kuzhambu song lyrics"
album: "Kuppathu Raja"
artist: "G. V. Prakash Kumar"
lyricist: "Logan"
director: "Baba Bhaskar"
path: "/albums/kuppathu-raja-lyrics"
song: "Meen Kuzhambu"
image: ../../images/albumart/kuppathu-raja.jpg
date: 2019-04-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LMH0T7tAc6o"
type: "happy"
singers:
  - Gana Guna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaru Man Avan Bloody Beggar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaru Man Avan Bloody Beggar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Vootla Meen Kuzhambu Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vootla Meen Kuzhambu Soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelithi Meenu Nadandhu Varaa Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelithi Meenu Nadandhu Varaa Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalnaa Naalnaa Etnaa Kaalam Odi Pochudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalnaa Naalnaa Etnaa Kaalam Odi Pochudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Nightla Gaandhi Coloru Maari Pochudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Nightla Gaandhi Coloru Maari Pochudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Adangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Adangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padukkumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panna Thappu Kannu Munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna Thappu Kannu Munna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Povundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Povundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Philosophy Philosophy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Philosophy Philosophy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Man Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Man Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Torture Kodukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Torture Kodukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiru Vootla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiru Vootla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dosa Kari Vaasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosa Kari Vaasana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Peratti Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Peratti Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urutti Paaka Yosana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutti Paaka Yosana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thakida Tha Thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thakida Tha Thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa Thakida Thaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Thakida Thaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thakida Tha Thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thakida Tha Thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa Thakkida Thaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Thakkida Thaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thoongumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thoongumpodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Thorandhu Thoonga Palagudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Thorandhu Thoonga Palagudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadu Kettu Kedakkudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu Kettu Kedakkudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Root Clearu Aayiruchu Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Root Clearu Aayiruchu Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appo Take Off
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Take Off"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Saltu Paperu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Saltu Paperu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Rosu Wateru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Rosu Wateru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Renduperum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Renduperum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mingle Aana Erum Meteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mingle Aana Erum Meteru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gilli Thaandi Aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Thaandi Aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karukka Deal Ah Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukka Deal Ah Poduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sungu Vacha Baanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sungu Vacha Baanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Susaram Poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Susaram Poduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Vootla Fishu Curry Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Vootla Fishu Curry Soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelitthi Meenu Walking Stylu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelitthi Meenu Walking Stylu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalnaa Naalnaa Etnaa Kaalam Odi Pochudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalnaa Naalnaa Etnaa Kaalam Odi Pochudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Nightla Gaandhi Coloru Maari Pochudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Nightla Gaandhi Coloru Maari Pochudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Adangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Adangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padukkumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panna Thappu Kannu Munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna Thappu Kannu Munna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Povundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Povundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuththu Karuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuththu Karuththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Niruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Lala Lala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Lala Lala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala Lala Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala Lala Laaa"/>
</div>
</pre>
