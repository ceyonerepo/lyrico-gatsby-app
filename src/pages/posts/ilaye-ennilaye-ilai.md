---
title: "ilaye ennilaye song lyrics"
album: "Ilai"
artist: "Vishnu V Divakaran"
lyricist: "Unknown"
director: "Bineesh Raj"
path: "/albums/ilai-lyrics"
song: "Ilaye Ennilaye"
image: ../../images/albumart/ilai.jpg
date: 2017-04-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JdCm_PeDMLU"
type: "love"
singers:
  -	unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ilaiye en ilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaiye en ilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga nan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga nan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga nan irapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga nan irapen"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavum en manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavum en manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thanadi nirainthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thanadi nirainthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thendralai enai theendi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendralai enai theendi "/>
</div>
<div class="lyrico-lyrics-wrapper">sendrathum neyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendrathum neyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">saaralai polinthu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaralai polinthu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthathum neyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthathum neyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyirodu sernthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyirodu sernthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochodu kalanthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochodu kalanthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">en paathiyai piranthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paathiyai piranthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangalil sumapathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangalil sumapathum"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjile rusipathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjile rusipathum"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkaiyil velichamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkaiyil velichamum"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavum en manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavum en manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyirum en swasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyirum en swasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakagave irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakagave irukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilaiye en ilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaiye en ilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga nan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga nan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga nan irapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga nan irapen"/>
</div>
</pre>
