---
title: "saarah saarah song lyrics"
album: "Shivalinga"
artist: "S S Thaman"
lyricist: "Viveka"
director: "P Vasu"
path: "/albums/shivalinga-lyrics"
song: "Saarah Saarah"
image: ../../images/albumart/shivalinga.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5EWuA-V8wFI"
type: "mass"
singers:
  -	Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Para Para Para Mela Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Para Para Para Mela Para"/>
</div>
<div class="lyrico-lyrics-wrapper">Parparppa Thaavi Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parparppa Thaavi Para"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasama Koovi Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasama Koovi Para"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Latchiyatha Vellum Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchiyatha Vellum Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikathan Nammaku Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikathan Nammaku Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaya Mothi Vaanam Thera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaya Mothi Vaanam Thera"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodaana Vervaikku Boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaana Vervaikku Boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedaga Sollida Yedhumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedaga Sollida Yedhumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraadi Meleri Vandhavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraadi Meleri Vandhavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorodu Noothonna Aanathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorodu Noothonna Aanathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Varuthu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Varuthu Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Yethu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Yethu Saarah Saarah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullaga Neelum Un Mookazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaga Neelum Un Mookazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathaale Aasai Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathaale Aasai Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaavi Pollula Melazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaavi Pollula Melazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullura Bothai Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullura Bothai Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minsara Sottaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsara Sottaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mc Rennett Cakekaga Meni Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mc Rennett Cakekaga Meni Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Vandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Vandhale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ennam Ederum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ennam Ederum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munneru Vegam Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munneru Vegam Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Varuthu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Varuthu Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Yethu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Yethu Saarah Saarah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillama Kollama Velai Sencha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillama Kollama Velai Sencha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama Vetri Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama Vetri Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullodum Kallodum Poradithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullodum Kallodum Poradithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamai Veedu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamai Veedu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaane Akkaala Anjal Thora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaane Akkaala Anjal Thora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanamthan Eppodhum Unthan Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamthan Eppodhum Unthan Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Undivil Kallaga Valluvan Sollaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undivil Kallaga Valluvan Sollaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara Nee Mela Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Nee Mela Para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Varuthu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Varuthu Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarah Saarah Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarah Saarah Saarah Saarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Yethu Saarah Saarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Yethu Saarah Saarah"/>
</div>
</pre>
