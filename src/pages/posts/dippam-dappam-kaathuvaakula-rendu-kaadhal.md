---
title: "dippam dappam song lyrics"
album: "Kaathuvaakula Rendu Kaadhal"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/kaathuvaakula-rendu-kaadhal-lyrics"
song: "Dippam Dappam"
image: ../../images/albumart/kaathuvaakula-rendu-kaadhal.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j64M3CACcr4"
type: "love"
singers:
  - Anthony Daasan
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amsama Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amsama Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Ponna Paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponna Paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Odane Puncture Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Odane Puncture Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama Pa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Pa!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amsama Azhaga Oru Ponna Paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amsama Azhaga Oru Ponna Paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Odane Puncture Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Odane Puncture Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Kanna Illa Current ‘ah Confusion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kanna Illa Current ‘ah Confusion"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Azhaga Pathi Paada Illa Education
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Azhaga Pathi Paada Illa Education"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Ini Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Ini Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Play Station
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Play Station"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Irukkum Idam Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irukkum Idam Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Hill Station
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hill Station"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Good Vibration Orey Sensation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good Vibration Orey Sensation"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Venamunnu Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Venamunnu Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren Meditation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren Meditation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palapalakkum Pandhurame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapalakkum Pandhurame"/>
</div>
<div class="lyrico-lyrics-wrapper">Silusilukkum Senthoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silusilukkum Senthoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daal Adikkum Rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daal Adikkum Rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuminukkum Mutharame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuminukkum Mutharame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palapalakkum Pandhurame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapalakkum Pandhurame"/>
</div>
<div class="lyrico-lyrics-wrapper">Silusilukkum Senthoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silusilukkum Senthoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daal Adikkum Rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daal Adikkum Rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuminukkum Mutharame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuminukkum Mutharame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Ninna Paatha Repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Ninna Paatha Repeat’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Pakkumbodhu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pakkumbodhu Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Appeeatt’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Appeeatt’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khadeeza Ninna Paatha Repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khadeeza Ninna Paatha Repeat’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Pakkumbodhu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pakkumbodhu Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Appeeatt’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Appeeatt’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Ethirpaarpum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Ethirpaarpum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Attraction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Attraction"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Poyi Poyi Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Poyi Poyi Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Temptation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Temptation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Boyfriend’um Irukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Boyfriend’um Irukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Complication
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Complication"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Meeri Ava Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Meeri Ava Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Satisfaction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Satisfaction"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What A Situation Venam Solution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What A Situation Venam Solution"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Club Kkulla Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Club Kkulla Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Celebration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Celebration"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippada Dippada Dippa Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippada Dippada Dippa Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palapalakkum Pandhurame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapalakkum Pandhurame"/>
</div>
<div class="lyrico-lyrics-wrapper">Silusilukkum Senthoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silusilukkum Senthoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daal Adikkum Rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daal Adikkum Rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuminukkum Mutharame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuminukkum Mutharame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palapalakkum Pandhurame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapalakkum Pandhurame"/>
</div>
<div class="lyrico-lyrics-wrapper">Silusilukkum Senthoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silusilukkum Senthoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daal Adikkum Rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daal Adikkum Rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuminukkum Mutharame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuminukkum Mutharame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippam Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippam Dippada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dippa Dappam Dippam Dappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dippa Dappam Dippam Dappam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dibbak’u Dappam Dippada Dippada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dibbak’u Dappam Dippada Dippada"/>
</div>
</pre>
