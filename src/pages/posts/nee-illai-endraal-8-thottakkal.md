---
title: "nee illai endraal song lyrics"
album: "8 Thottakkal"
artist: "KS Sundaramurthy"
lyricist: "Kutti Revathy"
director: "Sri Ganesh"
path: "/albums/8-thottakkal-lyrics"
song: "Nee Illai Endraal"
image: ../../images/albumart/8-thottakkal.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dGvKCBhZRTg"
type: "love"
singers:
  -	Haricharan
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee illai endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakena yaarum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakena yaarum illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen idhai seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen idhai seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai ena yaarumae illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai ena yaarumae illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthu pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthu pogaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu povomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu povomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena uruginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena uruginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril karaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Analena erigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analena erigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Alayaai udaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayaai udaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena varugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalai inaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalai inaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi neenguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi neenguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavilae varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae varugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandathum maraigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathum maraigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil vazhgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil vazhgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril mithakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril mithakiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etharkenai marukiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etharkenai marukiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam valikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam valikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi thanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangarai velichamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangarai velichamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaindhu ponaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaindhu ponaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalinil suzhalinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalinil suzhalinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu poven naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu poven naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaintha kai naluvinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaintha kai naluvinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aaven naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aaven naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena vazhugindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena vazhugindrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai thaangugindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai thaangugindrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakindha kobam yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakindha kobam yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena uruginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena uruginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyiril karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril karaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analena erigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analena erigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alayaai udaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayaai udaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illai endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakena yaarum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakena yaarum illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen idhai seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen idhai seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai ena yaarumae illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai ena yaarumae illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthu pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthu pogaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu povomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu povomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena uruginen Ahhaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena uruginen Ahhaahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril karaigiren Ahhaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril karaigiren Ahhaahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Analena erigiren Ahhaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analena erigiren Ahhaahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Alayaai udaigiren Ahhaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayaai udaigiren Ahhaahhh"/>
</div>
</pre>
