---
title: "yea pa yeppappa song lyrics"
album: "Iravukku Aayiram Kangal"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Mu. Maran"
path: "/albums/iravukku-aayiram-kangal-lyrics"
song: "Yea Pa Yeppappa"
image: ../../images/albumart/iravukku-aayiram-kangal.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VOQOwu23k2A"
type: "mass"
singers:
  - Sam CS
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor iravin madiyilae dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor iravin madiyilae dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam viravi thudikirom yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam viravi thudikirom yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Saer ulaga puzhudhiyil oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saer ulaga puzhudhiyil oor"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal kadatha ninaikirom naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal kadatha ninaikirom naalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla kolaiyum nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla kolaiyum nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha marana maraivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha marana maraivula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju padharum pinju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju padharum pinju "/>
</div>
<div class="lyrico-lyrics-wrapper">kozhandhaPola irulula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhandhaPola irulula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaanaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondhu nondhu saaga thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondhu nondhu saaga thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu oliyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu oliyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanja thookam thedi alaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanja thookam thedi alaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootam oyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa.thanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa.thanaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal valaigal veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal valaigal veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamellam vizhavae yesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamellam vizhavae yesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukku pogum dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukku pogum dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum vendaa dhaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum vendaa dhaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal valaigal veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal valaigal veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamellam vizhavae yesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamellam vizhavae yesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukku pogum dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukku pogum dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum vendaa dhaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum vendaa dhaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye pa yeppappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pa yeppappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukaayiram kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukaayiram kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaai kootathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaai kootathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olam kekkudhu senaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam kekkudhu senaa"/>
</div>
</pre>
