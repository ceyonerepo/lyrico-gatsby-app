---
title: "dinchak song lyrics"
album: "Red"
artist: "Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Kishore Tirumala"
path: "/albums/red-lyrics"
song: "Dinchak - Ekkadi Dhaanive"
image: ../../images/albumart/red.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/UN8ticAFu6E"
type: "happy"
singers:
  - Saketh Komanduri
  - Keerthana Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ekkadi dhaanive sakkani komali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadi dhaanive sakkani komali"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkadanivunnavendhe vasthava bheeimili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkadanivunnavendhe vasthava bheeimili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gampedu aasatho dhaatina vaakili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gampedu aasatho dhaatina vaakili"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosam chusthe mee magallantha idisina family
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosam chusthe mee magallantha idisina family"/>
</div>
<div class="lyrico-lyrics-wrapper">Are seppukunte bhadha are theeripodhe chanchitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are seppukunte bhadha are theeripodhe chanchitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Are chettantha maavodunnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are chettantha maavodunnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Settu chethadu nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settu chethadu nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedi ekkadunnadu naa kallaki kanipinchamanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedi ekkadunnadu naa kallaki kanipinchamanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee heroni koosintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee heroni koosintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannendu dabbala passengeru bandekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannendu dabbala passengeru bandekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Padakondu gantalaku podhamannadu bombai ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padakondu gantalaku podhamannadu bombai ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Padimandi soocharani saatuga vocha station ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padimandi soocharani saatuga vocha station ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thommidho number meedaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thommidho number meedaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Railoche rovvanthatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railoche rovvanthatiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sallati AC bogilo soopicchade okatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallati AC bogilo soopicchade okatiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ecchati dhuppati yesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ecchati dhuppati yesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorindammi maapatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorindammi maapatiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Koo chuk chuk koothalu thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koo chuk chuk koothalu thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothalu leve rathiriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothalu leve rathiriki"/>
</div>
<div class="lyrico-lyrics-wrapper">Engine mottham heat ekkinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engine mottham heat ekkinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jumpayyinde poddhatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jumpayyinde poddhatiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada eeda dhookake jinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada eeda dhookake jinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa beachuki raave inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa beachuki raave inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalettestha nee lanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalettestha nee lanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theega laagithe kadhile donka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theega laagithe kadhile donka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunjuthunte chenu gurunatham piliche nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunjuthunte chenu gurunatham piliche nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cut ye jesthe sceneu chennai lo thelanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cut ye jesthe sceneu chennai lo thelanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranjugundhe story yetayyndhe eesari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranjugundhe story yetayyndhe eesari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchipattu saree naligindha ledha jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchipattu saree naligindha ledha jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Englishu cinema choodham inga vaa annadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englishu cinema choodham inga vaa annadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engili mudhulante nerpisthanannadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engili mudhulante nerpisthanannadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba santhoshama naughty naancharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba santhoshama naughty naancharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambaregi poyindhemo nightu husharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambaregi poyindhemo nightu husharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lungi dance chedhamantu pongichade o beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungi dance chedhamantu pongichade o beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongunnadu gurrupetti mekki idly sambaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongunnadu gurrupetti mekki idly sambaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada eeda dhookake jinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada eeda dhookake jinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa beachuki raave inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa beachuki raave inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalettestha nee lanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalettestha nee lanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theega laagithe kadhile donka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theega laagithe kadhile donka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thippi sandhu sandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thippi sandhu sandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa valla kaadhani chandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa valla kaadhani chandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminaru mundhu thaaginchindhe mandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminaru mundhu thaaginchindhe mandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaagalanni chutti maa vizag vochava chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaagalanni chutti maa vizag vochava chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaguntaade city choosthava chemate patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaguntaade city choosthava chemate patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Light house laaga undhi bossu cut outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light house laaga undhi bossu cut outu"/>
</div>
<div class="lyrico-lyrics-wrapper">Route patti round esodham patnam soopettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route patti round esodham patnam soopettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chendu laaga methaga undhe papa nee vollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chendu laaga methaga undhe papa nee vollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ground lo dhigavante thiruguthaye kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ground lo dhigavante thiruguthaye kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu pallam yekki dhigi vachindayyo ee railu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu pallam yekki dhigi vachindayyo ee railu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satha choosi eedne unta ichavante signallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satha choosi eedne unta ichavante signallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada eeda dhookake jinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada eeda dhookake jinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa beachuki raave inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa beachuki raave inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalettestha nee lanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalettestha nee lanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theega laagithe kadhile donka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theega laagithe kadhile donka"/>
</div>
</pre>
