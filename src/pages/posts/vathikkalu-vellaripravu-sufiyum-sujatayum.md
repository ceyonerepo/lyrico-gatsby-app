---
title: "vathikkalu vellaripravu song lyrics"
album: "Sufiyum Sujatayum"
artist: "M. Jayachandran"
lyricist: "B K Harinarayanan"
director: "Naranipuzha Shanavas"
path: "/albums/sufiyum-sujatayum-lyrics"
song: "Vathikkalu Vellaripravu"
image: ../../images/albumart/sufiyum-sujatayum.jpg
date: 2020-07-03
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/H1UdC4ejBFs"
type: "happy"
singers:
  - 	Nithya Mammen Singer
  - 	Arjun Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vathikkalu Vellaripravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikkalu Vellaripravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Kondu Muttanu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Kondu Muttanu Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Hu hu Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Hu hu Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathikkalu Vellaripravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikkalu Vellaripravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Kondu Muttanu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Kondu Muttanu Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulliyamin Ullil Vannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulliyamin Ullil Vannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyam Kadalu Priyane Neeyam Kadalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyam Kadalu Priyane Neeyam Kadalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moula Moulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moula Moulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathikkalu Vellaripravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikkalu Vellaripravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Kondu Muttanu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Kondu Muttanu Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu Pole Vattam Vachhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Pole Vattam Vachhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannidayil Mutham Vachhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannidayil Mutham Vachhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwasamaake Thee Nirachhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwasamaake Thee Nirachhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenna Rooha Rooha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenna Rooha Rooha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Njaval Pazha Kannimakkunne Mayilanchi Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njaval Pazha Kannimakkunne Mayilanchi Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharinte Kuppi Thuranne Mulla Basaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharinte Kuppi Thuranne Mulla Basaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhikkaru Moolana Thathakalund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkaru Moolana Thathakalund"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukalayava Chollanath Enth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukalayava Chollanath Enth"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharamundu Othiriyundu Premathinthund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharamundu Othiriyundu Premathinthund"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyane Premathinthund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyane Premathinthund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathikkalu Vellaripravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikkalu Vellaripravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Kondu Muttanu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Kondu Muttanu Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neer Chuzhiyil Mungiyittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Chuzhiyil Mungiyittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Kolussil Vannu Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Kolussil Vannu Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli Meenaayi Minnanund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Meenaayi Minnanund"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenna Rooha Rooha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyenna Rooha Rooha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinn Palli Muttath Vanne Manja Velichham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinn Palli Muttath Vanne Manja Velichham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethanayum Then Thulliyaakum Prema Thelicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanayum Then Thulliyaakum Prema Thelicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullu Nirachoru Thaalinakathu Ha kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullu Nirachoru Thaalinakathu Ha kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enneyiduthu Kurichoru Kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enneyiduthu Kurichoru Kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannu Ninakku Onnu Thurakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannu Ninakku Onnu Thurakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Njan Innoorinu Priyane Njan Innoorinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njan Innoorinu Priyane Njan Innoorinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathikkalu Vellaripravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikkalu Vellaripravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Kondu Muttanu Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Kondu Muttanu Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moula Moulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moula Moulaa"/>
</div>
</pre>
