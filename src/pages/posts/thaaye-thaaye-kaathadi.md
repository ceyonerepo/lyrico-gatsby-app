---
title: "thaaye thaaye song lyrics"
album: "Kaathadi"
artist: "R. Pavan - Deepan B"
lyricist: "Ganesh Raja"
director: "Kalyaan"
path: "/albums/kaathadi-song-lyrics"
song: "Thaaye Thaaye"
image: ../../images/albumart/kaathadi.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jjmhwWtbvUc"
type: "sad"
singers:
  - Anu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">lala lala lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lala lala lala lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">lala lala lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lala lala lala lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">lala lala lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lala lala lala lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">lala lala lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lala lala lala lalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaaye thaaye thaai indri  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaye thaaye thaai indri  "/>
</div>
<div class="lyrico-lyrics-wrapper">nan ingu vaalvin artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ingu vaalvin artham"/>
</div>
<div class="lyrico-lyrics-wrapper">thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pogum pathai mullundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum pathai mullundu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallundu thaalam pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallundu thaalam pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">yar tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar tharuvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manathil yetho ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil yetho ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">maara nan ipothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara nan ipothum"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu aagiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu aagiren "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siragindri vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragindri vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pola nan pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pola nan pala"/>
</div>
<div class="lyrico-lyrics-wrapper">neram thani aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thani aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madi thanile thavalthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi thanile thavalthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">thudikirathe manam kanamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudikirathe manam kanamale"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalile unathu uruvam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalile unathu uruvam "/>
</div>
<div class="lyrico-lyrics-wrapper">alaigirathe moodamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigirathe moodamale"/>
</div>
</pre>
