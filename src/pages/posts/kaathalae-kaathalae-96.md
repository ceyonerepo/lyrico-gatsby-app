---
title: "kaathalae kaathalae song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Kaathalae Kaathalae"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CPMQdBmX2n4"
type: "happy"
singers:
  - Chinmayi
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum pooranamae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum pooranamae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum ezhlisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum ezhlisaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjavarna boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjavarna boodham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam niraiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam niraiyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanbathellaam kaadhaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanbathellaam kaadhaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani perunthunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani perunthunaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda vaa kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vaa kooda vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogalaam poga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogalaam poga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eee eee"/>
</div>
</pre>
