---
title: "chettekki song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "Chandrabose"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Chettekki"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EQ5xLLvO6pk"
type: "love"
singers:
  - Kaala Bhairava
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raaye Raaye Ramsilako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye Raaye Ramsilako"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranzubale Jatha Idigo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranzubale Jatha Idigo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaye Raaye Ramsilako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaye Raaye Ramsilako"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangubale Sekalivigo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangubale Sekalivigo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surukaina Sinnadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surukaina Sinnadhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarukunna Sinnodanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarukunna Sinnodanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhirindhi Ee Janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhirindhi Ee Janta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Hoye Oye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Oye Hoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ho Ho Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho Ho Hoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Hoye Oye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Oye Hoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoye Hoye Hoye Hoye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye Hoye Hoye Hoye Hoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chettekki Putta Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettekki Putta Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thechhaa Mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thechhaa Mamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lottesi Jurru Kuntaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lottesi Jurru Kuntaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buttallo Chitti Poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buttallo Chitti Poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukochhanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukochhanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhindhi Chuttukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhindhi Chuttukuntavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chukkalla Cheeranu Nesi Nesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalla Cheeranu Nesi Nesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennala Panupu Vesi Vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennala Panupu Vesi Vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula Chupe Dheepam Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chupe Dheepam Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechanu Yedhure Choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechanu Yedhure Choosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chettekki Putta Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettekki Putta Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thechhaa Mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thechhaa Mamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lottesi Jurru Kuntaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lottesi Jurru Kuntaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buttallo Chitti Poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buttallo Chitti Poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukochhanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukochhanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhindhi Chuttukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhindhi Chuttukuntavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Cherisagamai Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Cherisagamai Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Velallona Leelallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velallona Leelallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarame Naalo Chusevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarame Naalo Chusevaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravashamai Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravashamai Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharullona Theerullonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharullona Theerullonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulane Naatho Thisevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulane Naatho Thisevaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keechuralla Koothallanni Inukokaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keechuralla Koothallanni Inukokaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodikutha Kusindhemo Kaanabokaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodikutha Kusindhemo Kaanabokaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurchonika Nunchoneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurchonika Nunchoneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougittone Bajuntaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougittone Bajuntaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atta Etta Thelavari Poyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta Etta Thelavari Poyene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanalade Thaavullo Vuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanalade Thaavullo Vuntavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhellu Thappadhee Ee Se Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhellu Thappadhee Ee Se Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chettekki Putta Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettekki Putta Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thechhaa Mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thechhaa Mamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lottesi Jurru Kuntaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lottesi Jurru Kuntaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buttallo Chitti Poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buttallo Chitti Poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukochhanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukochhanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhindhi Chuttukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhindhi Chuttukuntavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chukkalla Cheeranu Nesi Nesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalla Cheeranu Nesi Nesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennala Panupu Vesi Vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennala Panupu Vesi Vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula Chupe Dheepam Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chupe Dheepam Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechanu Yedhure Choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechanu Yedhure Choosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chettekki Putta Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettekki Putta Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thechhaa Mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thechhaa Mamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lottesi Jurru Kuntaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lottesi Jurru Kuntaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Buttallo Chitti Poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Buttallo Chitti Poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukochhanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukochhanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhindhi Chuttukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhindhi Chuttukuntavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Hoye Oye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Oye Hoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Hoye Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Hoye Oye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Oye Hoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoye Hoye Hoye Hoye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye Hoye Hoye Hoye Hoye"/>
</div>
</pre>
