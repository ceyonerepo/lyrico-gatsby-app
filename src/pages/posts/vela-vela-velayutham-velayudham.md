---
title: "vela vela velayutham song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Priyan"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Vela Vela Velayutham"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ya8ojY1Uo1w"
type: "mass"
singers:
  - Vijay Antony
  - Mark
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vela Vela Vela Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vela Vela Vela Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Velayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oththa Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oththa Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Partha Podhum Noor Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha Podhum Noor Aayudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coming Down Town
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coming Down Town"/>
</div>
<div class="lyrico-lyrics-wrapper">Coming To The Sea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coming To The Sea"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is The Man Like Shiny Bee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is The Man Like Shiny Bee"/>
</div>
<div class="lyrico-lyrics-wrapper">Breaking Through The Barriers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breaking Through The Barriers"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna Come And Carry Us
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna Come And Carry Us"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Shout Out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Shout Out"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Get Some
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Get Some"/>
</div>
<div class="lyrico-lyrics-wrapper">You Cant Cut Him Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cant Cut Him Down"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is Velayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is Velayudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vela Vela Vela Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vela Vela Vela Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Velayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oththa Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oththa Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Partha Podhum Nooraayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha Podhum Nooraayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Now Dont Know Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now Dont Know Now"/>
</div>
<div class="lyrico-lyrics-wrapper">You Cant Catch Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cant Catch Him"/>
</div>
<div class="lyrico-lyrics-wrapper">He’s The Man And You’re Original Son
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He’s The Man And You’re Original Son"/>
</div>
<div class="lyrico-lyrics-wrapper">He’s Out Alone And On A Deadly Track
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He’s Out Alone And On A Deadly Track"/>
</div>
<div class="lyrico-lyrics-wrapper">Cold Heart And Broken Furious Brat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cold Heart And Broken Furious Brat"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice Cold Brand And A Killing Gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Cold Brand And A Killing Gun"/>
</div>
<div class="lyrico-lyrics-wrapper">You Gotta Look Out You Gotta Beware
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Gotta Look Out You Gotta Beware"/>
</div>
<div class="lyrico-lyrics-wrapper">We Cant Touch His Billions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Cant Touch His Billions"/>
</div>
<div class="lyrico-lyrics-wrapper">Hearts Of The Millions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hearts Of The Millions"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Shout Out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Shout Out"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Get Some
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Get Some"/>
</div>
<div class="lyrico-lyrics-wrapper">You Can’t Cut Him Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can’t Cut Him Down"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is Velayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is Velayudham"/>
</div>
</pre>
