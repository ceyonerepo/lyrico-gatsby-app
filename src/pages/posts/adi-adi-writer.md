---
title: "adi adi song lyrics"
album: "Writer"
artist: "Govind Vasantha"
lyricist: "Yugabharathi"
director: "Franklin Jacob"
path: "/albums/writer-song-lyrics"
song: "Adi Adi"
image: ../../images/albumart/writer.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dTRLLN64GV0"
type: "sad"
singers:
  - Anthakudi Ilayaraja
  - Lakshmi Chandru
  - Ananda Priyan
  - Jesurani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiye pombala pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye pombala pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye ambala pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye ambala pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi adi odambu ettadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adi odambu ettadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi paadi pozhuthum kottu adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paadi pozhuthum kottu adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiyathu adangum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyathu adangum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam podunga nalla padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam podunga nalla padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirathu poranthathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirathu poranthathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil mudivathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil mudivathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanamtha arinja manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanamtha arinja manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavadi thookkuvathu illaiyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavadi thookkuvathu illaiyathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathum vittathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathum vittathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai ponnunga kanakku vekkalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai ponnunga kanakku vekkalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaiyum pattaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaiyum pattaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyil ittume usuru thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyil ittume usuru thangalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjamum vanjamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjamum vanjamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala kolluthu padikka pogalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala kolluthu padikka pogalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Parambai uzhaipula pasiyum theeralaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parambai uzhaipula pasiyum theeralaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi adi odambu ettaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adi odambu ettaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi paadi pozhuthum kottu adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paadi pozhuthum kottu adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiyathu adangum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyathu adangum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam podunga nalla padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam podunga nalla padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirathu poranthathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirathu poranthathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil mudivathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil mudivathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanamtha arinja manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanamtha arinja manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavadi thookkuvathu illaiyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavadi thookkuvathu illaiyathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattu sarakkum seema sarakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu sarakkum seema sarakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta piragu onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta piragu onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu sirukku kundhanikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu sirukku kundhanikku"/>
</div>
<div class="lyrico-lyrics-wrapper">En mela thaan kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mela thaan kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootta veedu onbathu vaasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootta veedu onbathu vaasal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum veththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum veththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman madiyil saanji padukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman madiyil saanji padukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vehnam verai soththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vehnam verai soththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu kelama nalla irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu kelama nalla irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu nelama sonna vazhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu nelama sonna vazhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru sanamum onna irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru sanamum onna irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapparathula yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapparathula yethuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi paadhi kalladi paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi paadhi kalladi paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattathu naanga niththamum yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattathu naanga niththamum yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattunu vaanga sangadam theerka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunu vaanga sangadam theerka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru porappa neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru porappa neenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi adi odambu ettadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adi odambu ettadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi paadi pozhuthum kottu adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paadi pozhuthum kottu adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiyathu adangum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyathu adangum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam podunga nalla padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam podunga nalla padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirathu poranthathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirathu poranthathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil mudivathu eppaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil mudivathu eppaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karanamtha arinja manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanamtha arinja manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavadi thookkuvathu illaiyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavadi thookkuvathu illaiyathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punniya vaanathu pugazhu serattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punniya vaanathu pugazhu serattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungi nillungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungi nillungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaiyum kuttiyum kaalamum vaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaiyum kuttiyum kaalamum vaangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanthu pannungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanthu pannungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathu pogatum santhadhi vaazhathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathu pogatum santhadhi vaazhathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moottaiya kattungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moottaiya kattungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamurai thalaiyedukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamurai thalaiyedukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaiya maathungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaiya maathungada"/>
</div>
</pre>
