---
title: "machi open the bottle song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Machi Open the Bottle"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/68ixlbMQaY0"
type: "happy"
singers:
  - Mano
  - Premji Amaran
  - Haricharan
  - Tippu
  - Naveen Madhav
  - Suchith Suresan
  - D. Sathyaprakash
  - Rahul Nambiar
  - MK Balaji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machi Open The Bottle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Open The Bottle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Ambani Parambara Anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ambani Parambara Anjaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalamura Aanandham Valarpira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamura Aanandham Valarpira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kottunu Orumura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kottunu Orumura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnaka Palamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaka Palamura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottaadho Pananmazha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaadho Pananmazha Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Munnerum Padikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Munnerum Padikattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrachu Nam Vazhvu Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrachu Nam Vazhvu Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Onbathu Kiragamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Onbathu Kiragamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaga Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Nu Nam Jathagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Nu Nam Jathagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadama Jeichomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadama Jeichomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Meni Vadama Jeichomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Meni Vadama Jeichomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odama Run Eduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odama Run Eduthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summave Utkandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summave Utkandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Win Eduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Win Eduthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Ambani Parambara Anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ambani Parambara Anjaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalamura Aanandham Valarpira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamura Aanandham Valarpira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kottunu Orumura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kottunu Orumura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnaka Palamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaka Palamura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottaadho Pananmazha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaadho Pananmazha Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Onna Renda Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Onna Renda Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillunu Nikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillunu Nikita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chigaruthanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigaruthanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiya Oppukonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiya Oppukonda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Mela Kunthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Mela Kunthuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sola Vanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sola Vanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu Malaiyirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Malaiyirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulukkum Kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulukkum Kaasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaiyinna Kadan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyinna Kadan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kuberan Aavaan Kuselan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kuberan Aavaan Kuselan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Property Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Property Munnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Tea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Tea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endragum Sorgathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endragum Sorgathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sothukal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothukal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ullara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ullara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verkaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnala Undaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala Undaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podendi Saapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podendi Saapadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottak Podatha Koopaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottak Podatha Koopaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raman Aandalum Ravanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raman Aandalum Ravanan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandalum Enakkoru Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandalum Enakkoru Kavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nan Thaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nan Thaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasukku Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasukku Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vamgugada Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamgugada Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangathil Kooja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangathil Kooja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Keta Ketadha Kodupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Keta Ketadha Kodupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kekura Varangala Ketukoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekura Varangala Ketukoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meen Vaazha Neer Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen Vaazha Neer Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Vazha Beer Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Vazha Beer Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Konjam Oothu Oothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Konjam Oothu Oothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Ippodhum Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Ippodhum Eppodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muppodhum Veesidum Nam Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppodhum Veesidum Nam Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Kalathil Thoothikuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kalathil Thoothikuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kala Nerathil Mathikuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Nerathil Mathikuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Botha Aanalum Meeri Ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Botha Aanalum Meeri Ponalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Oor NalumEn Kalgal Maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Oor NalumEn Kalgal Maarathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pattu Vera Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pattu Vera Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum En Route u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum En Route u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Dan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Dan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Veladan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Veladan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaanu Oor Pesum Nala Dan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanu Oor Pesum Nala Dan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Ambani Parambara Anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ambani Parambara Anjaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalamura Aanandham Valarpira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamura Aanandham Valarpira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kottunu Orumura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kottunu Orumura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnaka Palamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaka Palamura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottaadho Pananmazha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaadho Pananmazha Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Munnerum Padikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Munnerum Padikattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrachu Nam Vazhvu Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrachu Nam Vazhvu Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Onbathu Kiragamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Onbathu Kiragamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaga Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaga Iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Nu Nam Jathagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Nu Nam Jathagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadama Jeichomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadama Jeichomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Meni Vadama Jeichomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Meni Vadama Jeichomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odama Run Eduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odama Run Eduthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summave Utkandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summave Utkandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Win Eduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Win Eduthom"/>
</div>
</pre>
