---
title: "oggu katha to get us visa song lyrics"
album: "Nuvvu Thopu Raa"
artist: "Suresh Bobbili"
lyricist: "Ajju Mahakali"
director: "B. Harinath Babu"
path: "/albums/nuvvu-thopu-raa-lyrics"
song: "Oggu Katha to get US VISA"
image: ../../images/albumart/nuvvu-thopu-raa.jpg
date: 2019-05-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mzz1yV3HqGo"
type: "happy"
singers:
  - Sudhakar Komakula
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ladies and gentlemen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladies and gentlemen"/>
</div>
<div class="lyrico-lyrics-wrapper">america povalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="america povalante"/>
</div>
<div class="lyrico-lyrics-wrapper">visa kavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visa kavali"/>
</div>
<div class="lyrico-lyrics-wrapper">ga visa ravalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga visa ravalante"/>
</div>
<div class="lyrico-lyrics-wrapper">masthu kasthmiethandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masthu kasthmiethandi"/>
</div>
<div class="lyrico-lyrics-wrapper">telusu gada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="telusu gada"/>
</div>
<div class="lyrico-lyrics-wrapper">aa visa easyga etla ravalno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa visa easyga etla ravalno"/>
</div>
<div class="lyrico-lyrics-wrapper">mana sudhakar anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana sudhakar anna"/>
</div>
<div class="lyrico-lyrics-wrapper">adhe mana suri anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhe mana suri anna"/>
</div>
<div class="lyrico-lyrics-wrapper">chepthadu vinuri zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepthadu vinuri zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hari hari aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hari hari aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">raghu nandana aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raghu nandana aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chall hari hari brahmandama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chall hari hari brahmandama"/>
</div>
<div class="lyrico-lyrics-wrapper">hari loka sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hari loka sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">madhela marellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhela marellu"/>
</div>
<div class="lyrico-lyrics-wrapper">marumanne gadda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marumanne gadda"/>
</div>
<div class="lyrico-lyrics-wrapper">buddhi nerpindana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buddhi nerpindana"/>
</div>
<div class="lyrico-lyrics-wrapper">bhoonachari vidhay nerpinavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhoonachari vidhay nerpinavada"/>
</div>
<div class="lyrico-lyrics-wrapper">veera hanumanuluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera hanumanuluu"/>
</div>
<div class="lyrico-lyrics-wrapper">saduvu nerpinavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saduvu nerpinavada"/>
</div>
<div class="lyrico-lyrics-wrapper">ori sambasivudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ori sambasivudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ammavaru parvathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammavaru parvathi"/>
</div>
<div class="lyrico-lyrics-wrapper">padhivelaa dandamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhivelaa dandamey"/>
</div>
<div class="lyrico-lyrics-wrapper">amma aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aga choodu aga choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aga choodu aga choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">evalu ela lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evalu ela lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">vallu elutundru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallu elutundru"/>
</div>
<div class="lyrico-lyrics-wrapper">sivudu eledi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivudu eledi"/>
</div>
<div class="lyrico-lyrics-wrapper">sivanandula kota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivanandula kota"/>
</div>
<div class="lyrico-lyrics-wrapper">brahma eledi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brahma eledi"/>
</div>
<div class="lyrico-lyrics-wrapper">satya lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satya lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">parvathi eledi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parvathi eledi"/>
</div>
<div class="lyrico-lyrics-wrapper">pancha nagarm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pancha nagarm"/>
</div>
<div class="lyrico-lyrics-wrapper">ee suri gadu eledi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee suri gadu eledi"/>
</div>
<div class="lyrico-lyrics-wrapper">saroor nagar bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saroor nagar bidda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sharanu sharanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sharanu sharanu"/>
</div>
<div class="lyrico-lyrics-wrapper">ma amba rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ma amba rani"/>
</div>
<div class="lyrico-lyrics-wrapper">shambavi rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shambavi rani"/>
</div>
<div class="lyrico-lyrics-wrapper">shambavi rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shambavi rani"/>
</div>
<div class="lyrico-lyrics-wrapper">karuna choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuna choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">o america pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o america pori"/>
</div>
<div class="lyrico-lyrics-wrapper">aa trump sir ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa trump sir ni"/>
</div>
<div class="lyrico-lyrics-wrapper">kalisosta gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalisosta gani"/>
</div>
<div class="lyrico-lyrics-wrapper">oori mallesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oori mallesha"/>
</div>
<div class="lyrico-lyrics-wrapper">ee tella tolu pilla ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee tella tolu pilla ki"/>
</div>
<div class="lyrico-lyrics-wrapper">doubtu ochindi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="doubtu ochindi "/>
</div>
<div class="lyrico-lyrics-wrapper">nannu america ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu america ki"/>
</div>
<div class="lyrico-lyrics-wrapper">enduku potunnav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enduku potunnav"/>
</div>
<div class="lyrico-lyrics-wrapper">ani adugutandi bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani adugutandi bidda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada tella tolu kanchulanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada tella tolu kanchulanta"/>
</div>
<div class="lyrico-lyrics-wrapper">sala salani manchulanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala salani manchulanta"/>
</div>
<div class="lyrico-lyrics-wrapper">sinna sinna guddalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinna sinna guddalu"/>
</div>
<div class="lyrico-lyrics-wrapper">pedda pedda middelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedda pedda middelu"/>
</div>
<div class="lyrico-lyrics-wrapper">eeda patayinchaneeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeda patayinchaneeki"/>
</div>
<div class="lyrico-lyrics-wrapper">tippale tippalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tippale tippalu"/>
</div>
<div class="lyrico-lyrics-wrapper">aada padipoye porilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada padipoye porilu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppale kuppalu bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppale kuppalu bidda"/>
</div>
<div class="lyrico-lyrics-wrapper">jiddu kare mokaponiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jiddu kare mokaponiki"/>
</div>
<div class="lyrico-lyrics-wrapper">bread mukkalanti pori ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bread mukkalanti pori ni"/>
</div>
<div class="lyrico-lyrics-wrapper">istanantay rakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istanantay rakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">unta natada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unta natada"/>
</div>
<div class="lyrico-lyrics-wrapper">istanantay rakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istanantay rakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">unta natada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unta natada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gatla nenu okkadinay kadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gatla nenu okkadinay kadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa yenaka eva levallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa yenaka eva levallu"/>
</div>
<div class="lyrico-lyrics-wrapper">vasta nantaro telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasta nantaro telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saadi annam tinetodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saadi annam tinetodu"/>
</div>
<div class="lyrico-lyrics-wrapper">sandwich tinetodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandwich tinetodu"/>
</div>
<div class="lyrico-lyrics-wrapper">computer chadivinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="computer chadivinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">saduvurani kampugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saduvurani kampugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">aragundu akbaru gadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aragundu akbaru gadu"/>
</div>
<div class="lyrico-lyrics-wrapper">angreji ranodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angreji ranodu"/>
</div>
<div class="lyrico-lyrics-wrapper">arugu meeda avva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugu meeda avva"/>
</div>
<div class="lyrico-lyrics-wrapper">ameerpet lo guvva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ameerpet lo guvva"/>
</div>
<div class="lyrico-lyrics-wrapper">busstand lo pilla dani sankalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="busstand lo pilla dani sankalo"/>
</div>
<div class="lyrico-lyrics-wrapper">bottle lo unna salla kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottle lo unna salla kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">vastanantadi bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vastanantadi bidda"/>
</div>
<div class="lyrico-lyrics-wrapper">o avva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o avva"/>
</div>
<div class="lyrico-lyrics-wrapper">nannu zara america pampinavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu zara america pampinavante"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku edi kavalante adi ippista
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku edi kavalante adi ippista"/>
</div>
<div class="lyrico-lyrics-wrapper">megastar tho selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megastar tho selfie"/>
</div>
<div class="lyrico-lyrics-wrapper">powerstar tho kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="powerstar tho kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">falknama faluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="falknama faluda"/>
</div>
<div class="lyrico-lyrics-wrapper">meenakshi lo zarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meenakshi lo zarada"/>
</div>
<div class="lyrico-lyrics-wrapper">qubaani ka meetha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="qubaani ka meetha "/>
</div>
<div class="lyrico-lyrics-wrapper">dhaba ka parota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaba ka parota"/>
</div>
<div class="lyrico-lyrics-wrapper">kukatpalli doa thethanay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kukatpalli doa thethanay"/>
</div>
<div class="lyrico-lyrics-wrapper">jai rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">nampally samosa ittanay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nampally samosa ittanay"/>
</div>
<div class="lyrico-lyrics-wrapper">jai rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">khairatabad khoorma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khairatabad khoorma"/>
</div>
<div class="lyrico-lyrics-wrapper">shamshabad shawarma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shamshabad shawarma "/>
</div>
<div class="lyrico-lyrics-wrapper">kali nuvvu adugu chalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kali nuvvu adugu chalu"/>
</div>
<div class="lyrico-lyrics-wrapper">em kavalante adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em kavalante adi"/>
</div>
<div class="lyrico-lyrics-wrapper">ippistane talli neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippistane talli neeku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okkatante okkate sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkatante okkate sari"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu visa meeda zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu visa meeda zara"/>
</div>
<div class="lyrico-lyrics-wrapper">stamp guddinchinav antay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stamp guddinchinav antay"/>
</div>
<div class="lyrico-lyrics-wrapper">maa cm kcr gari camp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa cm kcr gari camp"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kush aitade talli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kush aitade talli"/>
</div>
<div class="lyrico-lyrics-wrapper">inka evalevallu kush
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inka evalevallu kush"/>
</div>
<div class="lyrico-lyrics-wrapper">aytaro telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aytaro telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee trendu ntr u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee trendu ntr u"/>
</div>
<div class="lyrico-lyrics-wrapper">close friends ktr u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="close friends ktr u"/>
</div>
<div class="lyrico-lyrics-wrapper">srimanthudu mahesh u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="srimanthudu mahesh u"/>
</div>
<div class="lyrico-lyrics-wrapper">buddhi manthudu lokesh u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buddhi manthudu lokesh u"/>
</div>
<div class="lyrico-lyrics-wrapper">tank bund lo budhudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tank bund lo budhudu"/>
</div>
<div class="lyrico-lyrics-wrapper">racha banda lo roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="racha banda lo roja"/>
</div>
<div class="lyrico-lyrics-wrapper">vodka tagay varma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vodka tagay varma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaka hotel sharma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaka hotel sharma"/>
</div>
<div class="lyrico-lyrics-wrapper">six kotte dhoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="six kotte dhoni"/>
</div>
<div class="lyrico-lyrics-wrapper">maa galli shivara rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa galli shivara rani"/>
</div>
<div class="lyrico-lyrics-wrapper">andharu masthu kush
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharu masthu kush"/>
</div>
<div class="lyrico-lyrics-wrapper">avutaray avva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avutaray avva"/>
</div>
<div class="lyrico-lyrics-wrapper">aa painunna ntr anr ysr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa painunna ntr anr ysr"/>
</div>
<div class="lyrico-lyrics-wrapper">andharu ninnu deevistary talli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharu ninnu deevistary talli"/>
</div>
<div class="lyrico-lyrics-wrapper">oo tella tolu pori neeku dandamay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo tella tolu pori neeku dandamay"/>
</div>
<div class="lyrico-lyrics-wrapper">amma mummyy ippudu cheepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma mummyy ippudu cheepu"/>
</div>
<div class="lyrico-lyrics-wrapper">visa istavaa chastavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visa istavaa chastavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">alright your visa is approved
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alright your visa is approved"/>
</div>
</pre>
