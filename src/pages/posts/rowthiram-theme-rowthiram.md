---
title: "rowthiram theme song lyrics"
album: "Rowthiram"
artist: "Prakash Nikki"
lyricist: "Subramanya Bharathi"
director: "Gokul"
path: "/albums/rowthiram-lyrics"
song: "Rowthiram Theme"
image: ../../images/albumart/rowthiram.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3d6oWgmMY68"
type: "theme"
singers:
  - Palakkad Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Menmai Paduvaai Manamae Kel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menmai Paduvaai Manamae Kel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vin Meen Needhi Mun Vizhundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Meen Needhi Mun Vizhundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmai Thavari Nadungaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmai Thavari Nadungaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhaal Payan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhaal Payan Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menmai Paduvaai Manamae Kel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menmai Paduvaai Manamae Kel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vin Meen Needhi Mun Vizhundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Meen Needhi Mun Vizhundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmai Thavari Nadungaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmai Thavari Nadungaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhaal Payan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhaal Payan Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai Amungudhal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai Amungudhal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaanamillai Nadungudhal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaanamillai Nadungudhal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavamillai Padhungudhalillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamillai Padhungudhalillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhaal Payanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhaal Payanillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai Amungudhal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai Amungudhal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaanamillai Nadungudhal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaanamillai Nadungudhal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavamillai Padhungudhal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamillai Padhungudhal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhaal Payanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhaal Payanillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhir Veridhu Vidar Padamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir Veridhu Vidar Padamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Andam Sidharinaalum Anjamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Sidharinaalum Anjamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir Veridhu Vidar Padamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir Veridhu Vidar Padamaattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Andam Sidharinaalum Anjamaattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Sidharinaalum Anjamaattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkum Anjom Edharkum Anjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Anjom Edharkum Anjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Anjom Eppodhum Anjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Anjom Eppodhum Anjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Anjom Edharkum Anjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Anjom Edharkum Anjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Anjom Eppodhum Anjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Anjom Eppodhum Anjom"/>
</div>
</pre>
