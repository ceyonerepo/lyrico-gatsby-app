---
title: "unakkaagave song lyrics"
album: "Sila Nerangalil Sila Manidhargal"
artist: "Radhan"
lyricist: "Maathevan"
director: "Vishal Venkat"
path: "/albums/sila-nerangalil-sila-manidhargal-lyrics"
song: "Unakkaagave"
image: ../../images/albumart/sila-nerangalil-sila-manidhargal.jpg
date: 2022-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_sXbZ5PaGMc"
type: "melody"
singers:
  - Sathya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sirumega koodu thirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirumega koodu thirakkum unakkaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli theenda kaadu pirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli theenda kaadu pirakkum unakkaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirumega koodu thirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirumega koodu thirakkum unakkaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli theenda kaadu pirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli theenda kaadu pirakkum unakkaagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peroliyadhu kaar nizhalinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peroliyadhu kaar nizhalinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Or nilayilum neekka ketkkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or nilayilum neekka ketkkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar edhirile paar azhaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar edhirile paar azhaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee veruthidum neeyum indrillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veruthidum neeyum indrillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ver azhuthidum ser sagadhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver azhuthidum ser sagadhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamarai athu neekka ketkkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamarai athu neekka ketkkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazh ena unia vaazhvazhaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazh ena unia vaazhvazhaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ninaithida neelum un ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ninaithida neelum un ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Or viral or kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or viral or kural"/>
</div>
<div class="lyrico-lyrics-wrapper">Thetrida thondralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thetrida thondralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai nam paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai nam paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattrangal kaanalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattrangal kaanalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirumega koodu thirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirumega koodu thirakkum unakkaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli theenda kaadu pirakkum unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli theenda kaadu pirakkum unakkaagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai thodu mannithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thodu mannithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangalum aaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangalum aaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thodu nee anaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thodu nee anaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathin manamum poovil ezhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathin manamum poovil ezhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaazhvenna thaazhvidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhvenna thaazhvidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazh neekki meendezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazh neekki meendezhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvenna vaazhvidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvenna vaazhvidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyindri yedhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyindri yedhithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangalum kudaiyaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangalum kudaiyaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuvondru pogum paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuvondru pogum paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbile theeraa nadhi unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbile theeraa nadhi unakkaagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peroliyadhu kaar nizhalinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peroliyadhu kaar nizhalinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Or nilayilum neekka ketkkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or nilayilum neekka ketkkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar edhirile paar azhaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar edhirile paar azhaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee veruthidum neeyum indrillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veruthidum neeyum indrillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ver azhuthidum ser sagadhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver azhuthidum ser sagadhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamarai athu neekka ketkkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamarai athu neekka ketkkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazh ena unia vaazhvazhaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazh ena unia vaazhvazhaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ninaithida neelum un ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ninaithida neelum un ellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaagave unakkaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaagave unakkaagave"/>
</div>
</pre>
