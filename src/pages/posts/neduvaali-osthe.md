---
title: "neduvaali song lyrics"
album: "Osthe"
artist: "Sai Thaman"
lyricist: "Yugabharathi"
director: "S. Dharani"
path: "/albums/osthe-lyrics"
song: "Neduvaali"
image: ../../images/albumart/osthe.jpg
date: 2011-12-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/19CdwdItB3o"
type: "happy"
singers:
  - Rahul Nambiar
  - Mahathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neduvaali adiyae neduvaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvaali adiyae neduvaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Udumba udumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udumba udumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi nenja neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi nenja neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavvi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvi poriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvaali adiyae neduvaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvaali adiyae neduvaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumba arumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumba arumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyoo enna neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyoo enna neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi poriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kannula sutta kannula sutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kannula sutta kannula sutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vari variya thala mudiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vari variya thala mudiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva alagula naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva alagula naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanguromae adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanguromae adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madi madiya iru vizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi madiya iru vizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva nadaiyila neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva nadaiyila neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruvinga vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruvinga vazhiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey kuduthu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey kuduthu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonuthu pesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonuthu pesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maariduthae buthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maariduthae buthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavaniyil yeriyuromae paththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavaniyil yeriyuromae paththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha paya pula alaga ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha paya pula alaga ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukanum aarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukanum aarathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvaali adiyae neduvaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvaali adiyae neduvaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula sutta kannula sutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula sutta kannula sutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula sutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula sutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga control il oora vaipeengalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga control il oora vaipeengalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa control illama poreengalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa control illama poreengalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri thappaama nethu suttenadaIppa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri thappaama nethu suttenadaIppa "/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala soodu pattenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala soodu pattenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sema Dhillaana aalu summa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Dhillaana aalu summa illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu engeyum moola poiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu engeyum moola poiyae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pollatha case-su aanadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pollatha case-su aanadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavalu kaaka vendiya aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalu kaaka vendiya aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanala paarappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanala paarappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fir-ra odanae podappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fir-ra odanae podappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada oru sana nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada oru sana nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudana pudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudana pudikira"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa nee thedappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa nee thedappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvaali adiyae neduvaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvaali adiyae neduvaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha pennaala thookam kettarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha pennaala thookam kettarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethum unnama yekkam kondarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethum unnama yekkam kondarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu saamathil rondhu povingalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu saamathil rondhu povingalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipa roomkul rondhu poringalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipa roomkul rondhu poringalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala pala seivora pottu sertharunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pala seivora pottu sertharunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Olungilaama koothum pottarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olungilaama koothum pottarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipa ultava maari ponarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipa ultava maari ponarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalam therinju kaalayum vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam therinju kaalayum vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thrillae irukathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrillae irukathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vantha muthum kidaikaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vantha muthum kidaikaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha thunichal manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha thunichal manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukura variyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukura variyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu onnum nadakaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu onnum nadakaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada dumeeluthaan ada dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dumeeluthaan ada dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman gunnula suttaa dumeeluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman gunnula suttaa dumeeluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dammaaluthaan ada dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dammaaluthaan ada dammaaluthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mami kannula sutta dammaaluthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mami kannula sutta dammaaluthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvaali adiyae neduvaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvaali adiyae neduvaali"/>
</div>
</pre>
