---
title: "muthukutti settai song lyrics"
album: "Aelay"
artist: "Kaber Vasuki - Aruldev"
lyricist: "Halitha Shameem - Kaber Vasuki"
director: "Halitha Shameem"
path: "/albums/aelay-song-lyrics"
song: "Muthukutti Settai"
image: ../../images/albumart/aelay.jpg
date: 2021-002-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D8PG_BU_gZo"
type: "Entertainment"
singers:
  - Kaber Vasuki
  - Alexander Babu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anne ice-u vandi horn-u adikkuthu muthukutti vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne ice-u vandi horn-u adikkuthu muthukutti vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku enna vamba avan kootti vare poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku enna vamba avan kootti vare poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ice-u vandi raatham suththurathu avan koottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ice-u vandi raatham suththurathu avan koottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha koottai kulla eppothume muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha koottai kulla eppothume muthukutti settai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice-u vandi horn-u adikkuthu muthukutti vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice-u vandi horn-u adikkuthu muthukutti vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku enna vamba avan kootti vare poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku enna vamba avan kootti vare poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ice-u vandi raatham suththurathu avan koottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ice-u vandi raatham suththurathu avan koottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha koottai kulla eppothume muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha koottai kulla eppothume muthukutti settai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru koopi moodi sarayamtha kuduchi puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru koopi moodi sarayamtha kuduchi puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodikittu dappa dance-u aadi vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodikittu dappa dance-u aadi vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadankaaran veetta thedi vanthu nikkum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadankaaran veetta thedi vanthu nikkum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya pootta pottu ulla iruppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya pootta pottu ulla iruppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veda kozhi yethum sevathu mela paarthutanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veda kozhi yethum sevathu mela paarthutanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudu thanniyila sattunu thaan pottuduvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudu thanniyila sattunu thaan pottuduvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi curry koovi kitta vanthu vaasa padi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi curry koovi kitta vanthu vaasa padi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavi oothi rusi paarkkatharuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavi oothi rusi paarkkatharuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La la pakkam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la pakkam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Engayachum olinjiruppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engayachum olinjiruppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veattu pottu thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veattu pottu thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha padham paarthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha padham paarthane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan kedakkan aadu pulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kedakkan aadu pulukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Joppiyila oththa paisa illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joppiyila oththa paisa illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahna onnunga kelvi kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahna onnunga kelvi kekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana pola aalum yaarum ooril illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana pola aalum yaarum ooril illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyyo ice-u vandi horn-u adikkuthu muthukutti vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo ice-u vandi horn-u adikkuthu muthukutti vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikku enna vamba avan kootti vare poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innikku enna vamba avan kootti vare poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ice-u vandi raatham suththurathu avan koottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ice-u vandi raatham suththurathu avan koottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha koottai kulla eppothume muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha koottai kulla eppothume muthukutti settai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthukutti settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthukutti settai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aelay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aelay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Aelay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aelay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Aelay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aelay!"/>
</div>
</pre>
