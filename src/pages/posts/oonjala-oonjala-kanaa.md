---
title: "oonjala oonjala song lyrics"
album: "kanaa"
artist: "Dhibu Ninan Thomas"
lyricist: "Mohan Rajan"
director: " Arunraja Kamaraj"
path: "/albums/kanaa-lyrics"
song: "Oonjala Oonjala"
image: ../../images/albumart/kanaa.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LYHk9DvO4gU"
type: "motivational"
singers:
  - Sid Sriram
  - Niranjana Ramanan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaka Thaka Thaka Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaka Thaka Tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vittaal Unnai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittaal Unnai Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Indha Mannil Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Indha Mannil Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Solla Nee Indru Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Solla Nee Indru Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abdul Kalam Sonnaar Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abdul Kalam Sonnaar Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kaana Vendum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaana Vendum Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaakka Nee Vendru Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaakka Nee Vendru Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedalukku Thevai Nalla Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalukku Thevai Nalla Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vidu Aalam Ulla Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vidu Aalam Ulla Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedhigalai Varalaaraakkum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedhigalai Varalaaraakkum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendum Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhvigalil Karkka Seiyum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigalil Karkka Seiyum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrigalil Nirkka Seiyum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrigalil Nirkka Seiyum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalaam Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaam Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ellai Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ellai Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nee Nee Neeyaaga Iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nee Nee Neeyaaga Iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Thee Thee Theeyaaga Sudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Thee Thee Theeyaaga Sudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Naa Naa Naalodu Ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Naa Naa Naalodu Ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Madu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Madu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Eh Oh Eh Oh Eh Oh Oh Eh Oh Ahahah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Eh Oh Eh Oh Eh Oh Oh Eh Oh Ahahah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Eh Oh Eh Oh Eh Oh Oh Eh Oh Ahahah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Eh Oh Eh Oh Eh Oh Oh Eh Oh Ahahah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongi Pogum Vizhiyodu Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongi Pogum Vizhiyodu Varuvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongi Poga Irukkaamal Seivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongi Poga Irukkaamal Seivathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Paarthu Asaraamal Iruppathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Paarthu Asaraamal Iruppathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saagum Varaiyil Saagaamal Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagum Varaiyil Saagaamal Varuvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakku Solla Muyandraale Adippathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakku Solla Muyandraale Adippathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi Thaandi Podhuvaaga Iruppathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Thaandi Podhuvaaga Iruppathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ingu Oda Seiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ingu Oda Seiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnul Unnai Theda Seiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul Unnai Theda Seiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vaatti Vairam Seiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vaatti Vairam Seiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaal Ingu Maatram Vendumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Ingu Maatram Vendumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ingu Maatra Venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ingu Maatra Venduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Sollu Undhan Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Sollu Undhan Kanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Seyalil Yaavum Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Seyalil Yaavum Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nee Nee Kanneerai Thudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nee Nee Kanneerai Thudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraadu Un Vervai Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu Un Vervai Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munneru Thoolaagum Thadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munneru Thoolaagum Thadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maiya Pudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Pudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vittaal Unnai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittaal Unnai Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Indha Mannil Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Indha Mannil Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Solla Nee Indru Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Solla Nee Indru Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abdul Kalam Sonnaar Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abdul Kalam Sonnaar Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kaana Vendum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaana Vendum Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaakka Nee Vendru Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaakka Nee Vendru Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedalukku Thevai Nalla Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalukku Thevai Nalla Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vidu Aalam Ulla Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vidu Aalam Ulla Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedhigalai Varalaaraakkum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedhigalai Varalaaraakkum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendum Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhvigalil Karkka Seiyum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigalil Karkka Seiyum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrigalil Nirkka Seiyum Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrigalil Nirkka Seiyum Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalaam Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaam Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ellai Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ellai Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjala Oonjala Oonjala Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjala Oonjala Oonjala Urah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal Urah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Urah"/>
</div>
</pre>
