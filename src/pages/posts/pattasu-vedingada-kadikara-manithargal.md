---
title: "pattasu vedingada song lyrics"
album: "Kadikara Manithargal"
artist: "Sam CS"
lyricist: "Na Muthukumar"
director: "Vaigarai Balan"
path: "/albums/kadikara-manithargal-lyrics"
song: "Pattasu Vedingada"
image: ../../images/albumart/kadikara-manithargal.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GdSkSW-sWEI"
type: "happy"
singers:
  - Anthony Dhasan
  - Mukesh Mohamed
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">House ownera police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="House ownera police"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichittu poitaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichittu poitaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ini jolly thaan pa jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ini jolly thaan pa jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhaan house ownera police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaan house ownera police"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu isththunnu poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu isththunnu poyiduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini sema kalakkalu kujaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini sema kalakkalu kujaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy pattasu vedingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy pattasu vedingada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathaalam adingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathaalam adingada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosha pattonnu parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosha pattonnu parakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththaapa koluthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaapa koluthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiththaapa thiringada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiththaapa thiringada"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangaswamy rules
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangaswamy rules"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo mudinchudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo mudinchudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkalaam koothaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam koothaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aani"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadalaam kondadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaam kondadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appadi podae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadi podae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa daiyalakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa daiyalakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei agadhiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei agadhiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama irundhathu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama irundhathu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soda goli gundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soda goli gundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedanthathu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedanthathu podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithiyapola naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithiyapola naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanjathum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanjathum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama uravu marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama uravu marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya vaazhnthathu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya vaazhnthathu podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathroomu poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathroomu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tokennu thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tokennu thevaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaada kulikkalaam vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaada kulikkalaam vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nightellaam lighta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellaam lighta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pottu thaan aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pottu thaan aadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaatha thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaatha thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada vaada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">VaaParakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VaaParakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkalaam koothaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam koothaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aani"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadalaam kondadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaam kondadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangaswamykku ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangaswamykku ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangaswamykku ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangaswamykku ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa summa viduvaala sugumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa summa viduvaala sugumaari"/>
</div>
<div class="lyrico-lyrics-wrapper">therikka vudraa somaarinaanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikka vudraa somaarinaanaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaasoththukaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaasoththukaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha pallaiyae pudungratha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha pallaiyae pudungratha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kelavi athaan ootu owner poyitaarula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kelavi athaan ootu owner poyitaarula"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa kalaichingura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa kalaichingura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naanga thaan enga aattam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naanga thaan enga aattam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuma adangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuma adangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maama podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paaraa theeppori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paaraa theeppori"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikkudhu joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkudhu joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaga veetil sirippoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaga veetil sirippoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakkudhu kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkudhu kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangaswamyoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangaswamyoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanam ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocketah kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocketah kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ABC ezhuthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABC ezhuthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvattrila kirukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvattrila kirukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkatha vittu nee vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkatha vittu nee vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gilliyum aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilliyum aadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Crickettum aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crickettum aadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhosam thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosam thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dannanakka danukku nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dannanakka danukku nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Tantanakka santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tantanakka santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum ini"/>
</div>
</pre>
