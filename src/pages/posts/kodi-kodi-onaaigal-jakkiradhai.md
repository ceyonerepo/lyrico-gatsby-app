---
title: "kodi kodi song lyrics"
album: "Onaaigal Jakkiradhai"
artist: "Adheesh Uthrian"
lyricist: "Karthikeyan"
director: "JPR"
path: "/albums/onaaigal-jakkiradhai-lyrics"
song: "Kodi Kodi"
image: ../../images/albumart/onaaigal-jakkiradhai.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ex9t1pCZPrE"
type: "mass"
singers:
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnadi panam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi panam sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi vara sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi vara sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">munnadi panam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi panam sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi vara sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi vara sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">pathai mari sertha andha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathai mari sertha andha "/>
</div>
<div class="lyrico-lyrics-wrapper">paname than unai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paname than unai kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">paname than unai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paname than unai kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">serthathai ellam eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthathai ellam eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sella iraivan mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sella iraivan mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">anumathithal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anumathithal "/>
</div>
<div class="lyrico-lyrics-wrapper">serthathai ellam eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthathai ellam eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sella iraivan mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sella iraivan mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">anumathithal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anumathithal "/>
</div>
<div class="lyrico-lyrics-wrapper">kadal neerum boomiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal neerum boomiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanamal poyirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanamal poyirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulle povathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulle povathai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu manum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu manum "/>
</div>
<div class="lyrico-lyrics-wrapper">ponum serkindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponum serkindran"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulle povathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulle povathai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu manum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu manum "/>
</div>
<div class="lyrico-lyrics-wrapper">ponum serkindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponum serkindran"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai ellam jeithu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai ellam jeithu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranathidam ne thotriduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranathidam ne thotriduvai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai ellam jeithu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai ellam jeithu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranathidam ne thotriduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranathidam ne thotriduvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panju methaiyil urundalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju methaiyil urundalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paava mootaiyai sumakindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paava mootaiyai sumakindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">panju methaiyil urundalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju methaiyil urundalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paava mootaiyai sumakindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paava mootaiyai sumakindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pantha paasathai maranthu vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pantha paasathai maranthu vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugamagamai nee marugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugamagamai nee marugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagil ulla uyirinangal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagil ulla uyirinangal than"/>
</div>
<div class="lyrico-lyrics-wrapper">innathai alipathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innathai alipathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagil ulla uyirinangal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagil ulla uyirinangal than"/>
</div>
<div class="lyrico-lyrics-wrapper">innathai alipathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innathai alipathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">manithan mattum vithi vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithan mattum vithi vilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">iraiva nee athai vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraiva nee athai vilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">manithan mattum vithi vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithan mattum vithi vilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">iraiva nee athai vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraiva nee athai vilaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi kodiyai panam serka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiyai panam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi than alanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi than alanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manithar podum kanakkugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithar podum kanakkugal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam poiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba poiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba poiyaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munnadi panam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi panam sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi vara sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi vara sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">munnadi panam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi panam sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi vara sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi vara sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">pathai mari sertha andha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathai mari sertha andha "/>
</div>
<div class="lyrico-lyrics-wrapper">paname than unai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paname than unai kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">paname than unai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paname than unai kollum"/>
</div>
</pre>
