---
title: "tillu anna dj pedithe song lyrics"
album: "DJ Tillu"
artist: "Ram Miriyala"
lyricist: "Kasarla Shyam"
director: "Vimal Krishna"
path: "/albums/dj-tillu-lyrics"
song: "Tillu Anna DJ Pedithe"
image: ../../images/albumart/dj-tillu.jpg
date: 2022-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/I8c0-NEBqL0"
type: "happy"
singers:
  - Ram Miriyala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Laalaguda ambarpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laalaguda ambarpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallepalli malakpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallepalli malakpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tillu anna dj pedite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tillu anna dj pedite"/>
</div>
<div class="lyrico-lyrics-wrapper">Tilla tilla aadalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tilla tilla aadalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallesanna dawath la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallesanna dawath la"/>
</div>
<div class="lyrico-lyrics-wrapper">Bannu gaani baarath la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bannu gaani baarath la"/>
</div>
<div class="lyrico-lyrics-wrapper">Tillu anna digindu antey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tillu anna digindu antey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchak dinchak dhunkalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchak dinchak dhunkalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dj tillu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeni style ey veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeni style ey veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokemo hero teeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokemo hero teeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottedhi teenumaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottedhi teenumaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Base zara penchi kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Base zara penchi kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boxulu palige tattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boxulu palige tattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chemki shirtu aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemki shirtu aaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeni kunguru juttu ohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeni kunguru juttu ohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Atlaa yellindantey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atlaa yellindantey"/>
</div>
<div class="lyrico-lyrics-wrapper">Star ley salaam kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star ley salaam kottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Galli suttu aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galli suttu aaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatharey jallinattu oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatharey jallinattu oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Masth gaa navvindu ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masth gaa navvindu ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Porila dillu fattu addhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porila dillu fattu addhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anna photo pettukuni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna photo pettukuni "/>
</div>
<div class="lyrico-lyrics-wrapper">gym centre lanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gym centre lanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti vadi vadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti vadi vadi "/>
</div>
<div class="lyrico-lyrics-wrapper">publicity sestaa ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="publicity sestaa ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Coorperato kaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coorperato kaina "/>
</div>
<div class="lyrico-lyrics-wrapper">dirtect ga phone kodtaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dirtect ga phone kodtaadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dj tillu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeni style ey veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeni style ey veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokemo hero teeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokemo hero teeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottedhi teenumaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottedhi teenumaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Base zara penchi kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Base zara penchi kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boxulu palige tattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boxulu palige tattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj tillu kottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj tillu kottu "/>
</div>
<div class="lyrico-lyrics-wrapper">kottakuntey naa medha ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottakuntey naa medha ottu"/>
</div>
</pre>
