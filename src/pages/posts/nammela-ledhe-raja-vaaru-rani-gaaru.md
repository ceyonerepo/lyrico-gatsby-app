---
title: "nammela ledhe song lyrics"
album: "Raja Vaaru Rani Gaaru"
artist: "Jay Krish"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Ravi Kiran Kola"
path: "/albums/raja-vaaru-rani-gaaru-lyrics"
song: "Nammela Ledhe"
image: ../../images/albumart/raja-vaaru-rani-gaaru.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-gIWbi7-R5A"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nammela Ledhe Kala Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammela Ledhe Kala Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Meghamaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Meghamaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammesindhedho Oka Haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammesindhedho Oka Haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuke Bhaaramaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuke Bhaaramaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyyaalanundhi Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyyaalanundhi Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyyaalaloogeti Meghamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyyaalaloogeti Meghamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaagendhuke Saraasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaagendhuke Saraasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampinchu Chinnari Chinukunee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampinchu Chinnari Chinukunee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhaviki Kanulatho Kalahamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhaviki Kanulatho Kalahamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Mansuni Vadhalave Bidiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Mansuni Vadhalave Bidiyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seetha Choope Rama Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetha Choope Rama Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayinadhemo Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayinadhemo Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaredhee Leka Navvuthoone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaredhee Leka Navvuthoone"/>
</div>
<div class="lyrico-lyrics-wrapper">Naluguthondhi Edhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naluguthondhi Edhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasuni Kose Sutharamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasuni Kose Sutharamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepaina Aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepaina Aagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Kaalamaa Velakolamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kaalamaa Velakolamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantai Unte Neramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantai Unte Neramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhaviki Kanulatho Kalahamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhaviki Kanulatho Kalahamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Mansuni Vadhalave Bidiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Mansuni Vadhalave Bidiyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palletoore Parnashaalai Veluguthondhi Elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palletoore Parnashaalai Veluguthondhi Elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummaanni Dhaate Olipiralle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummaanni Dhaate Olipiralle"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulliraake Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulliraake Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nadakani Aape Nidhaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nadakani Aape Nidhaanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe Naadham Naatyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe Naadham Naatyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Dhooramaa Naatho Vairamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Dhooramaa Naatho Vairamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraatheeram Cherumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraatheeram Cherumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhaviki Kanulatho Kalahamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhaviki Kanulatho Kalahamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Mansuni Vadhalave Bidiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Mansuni Vadhalave Bidiyamaa"/>
</div>
</pre>
