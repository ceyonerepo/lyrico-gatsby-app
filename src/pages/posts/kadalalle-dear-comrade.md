---
title: "kadalalle song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Kadalalle"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2jzbakGCEOQ"
type: "love"
singers:
  - Sid Sriram
  - Aishwarya Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadalalle Veche Kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalalle Veche Kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilenu Nadhila Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilenu Nadhila Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalalle Veche Kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalalle Veche Kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilenu Nadhila Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilenu Nadhila Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodi Cheri Okatai Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodi Cheri Okatai Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodi Cheri Okatai Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodi Cheri Okatai Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Kore Praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Kore Praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraham Pongele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraham Pongele"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayam Oogele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayam Oogele"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharam Anchule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharam Anchule"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuram Korele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuram Korele"/>
</div>
<div class="lyrico-lyrics-wrapper">Antheleni Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antheleni Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaapam Yemitilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaapam Yemitilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhisthundhe Vesavilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhisthundhe Vesavilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chentha Cheri Sedhatheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chentha Cheri Sedhatheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Praayamilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praayamilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi chaachi Koruthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi chaachi Koruthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayamilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayamilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaalu Maarinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaalu Maarinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhyaasa Maarunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhyaasa Maarunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigindhi Mohame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigindhi Mohame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thodu Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thodu Ilaa Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraham Pongele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraham Pongele"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayam Oogele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayam Oogele"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharam Anchule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharam Anchule"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuram Korele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuram Korele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalalle Veche Kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalalle Veche Kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilenu Nadhila Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilenu Nadhila Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalalle Veche Kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalalle Veche Kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilenu Nadhila Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilenu Nadhila Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Kannulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Kannulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachaanule Lokamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachaanule Lokamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nanne Malichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nanne Malichaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga Meedha Muddhe Pette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga Meedha Muddhe Pette"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipithanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnattundi Nanne Chutte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnattundi Nanne Chutte"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduchugunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduchugunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchukunna Chinni Chinni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchukunna Chinni Chinni"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshaalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshaalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindipoye Undipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindipoye Undipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelothullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelothullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelona Cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelona Cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanunchi Verugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanunchi Verugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilindhi Praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilindhi Praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevaipu Ila Ilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevaipu Ila Ilaaa"/>
</div>
</pre>
