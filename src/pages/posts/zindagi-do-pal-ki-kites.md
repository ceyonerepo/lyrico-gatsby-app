---
title: "zindagi do pal ki song lyrics"
album: "Kites"
artist: "Rajesh Roshan"
lyricist: "Nasir Faraaz"
director: "Anurag Basu"
path: "/albums/kites-lyrics"
song: "Zindagi do pal ki"
image: ../../images/albumart/kites.jpg
date: 2010-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/vzgA7UMRbhc"
type: "love"
singers:
  - K.K
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">zindagi do pal ki, zindagi do pal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagi do pal ki, zindagi do pal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">intezar kab tak hum karenge bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intezar kab tak hum karenge bala"/>
</div>
<div class="lyrico-lyrics-wrapper">tumhe pyar kab tak na karenge bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tumhe pyar kab tak na karenge bala"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagi do pal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagi do pal ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dil mein tumhare chupa di hai maine to apni ye jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil mein tumhare chupa di hai maine to apni ye jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ab tum hi isko sambhalo hume apna hosh kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ab tum hi isko sambhalo hume apna hosh kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">bekhudi do pal ki, zindagi do pal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekhudi do pal ki, zindagi do pal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">intezar kab tak hum karenge balah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intezar kab tak hum karenge balah"/>
</div>
<div class="lyrico-lyrics-wrapper">tumhe pyar kab tak na karenge balah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tumhe pyar kab tak na karenge balah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ik chota sa vaada iss umar se zyada sachcha hai sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ik chota sa vaada iss umar se zyada sachcha hai sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">har mod par saath iss liye rehte hain ab dono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="har mod par saath iss liye rehte hain ab dono"/>
</div>
<div class="lyrico-lyrics-wrapper">dosti do pal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosti do pal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagi do pal ki, zindagi do pal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagi do pal ki, zindagi do pal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">intezar kab tak hum karenge bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intezar kab tak hum karenge bala"/>
</div>
<div class="lyrico-lyrics-wrapper">tumhe pyar kab tak na karenge bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tumhe pyar kab tak na karenge bala"/>
</div>
<div class="lyrico-lyrics-wrapper">zindagi do pal ki zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zindagi do pal ki zindagi"/>
</div>
</pre>
