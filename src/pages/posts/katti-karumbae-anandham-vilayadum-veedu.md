---
title: "katti karumbae song lyrics"
album: "Anandham Vilayadum Veedu"
artist: "Siddhu Kumar"
lyricist: "Snehan"
director: "Nandha Periyasamy"
path: "/albums/anandham-vilayadum-veedu-song-lyrics"
song: "Katti Karumbae"
image: ../../images/albumart/anandham-vilayadum-veedu.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0BI1v06bIyM"
type: "love"
singers:
  - Sasi karan
  - Vishnu priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">katti karumbe kattikkathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti karumbe kattikkathane"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti karanam pottene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti karanam pottene"/>
</div>
<div class="lyrico-lyrics-wrapper">sattunu unna muttina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattunu unna muttina "/>
</div>
<div class="lyrico-lyrics-wrapper">pinne sikki thavichene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinne sikki thavichene"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thottu palagida ketta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thottu palagida ketta "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula vittu tholachi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula vittu tholachi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kidanthida mutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kidanthida mutti "/>
</div>
<div class="lyrico-lyrics-wrapper">utharida kittakka vanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utharida kittakka vanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">o vekkathula ninna oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o vekkathula ninna oh"/>
</div>
<div class="lyrico-lyrics-wrapper">vegavachu thinna oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegavachu thinna oh"/>
</div>
<div class="lyrico-lyrics-wrapper">uthadu madippil thadukki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthadu madippil thadukki "/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthu sokki kidakkurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthu sokki kidakkurene"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu niraya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu niraya "/>
</div>
<div class="lyrico-lyrics-wrapper">vara vacha malaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara vacha malaya"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu karayura vetti neliyithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu karayura vetti neliyithu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna panna solludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna panna solludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh kathal kalavaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh kathal kalavaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal selambaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal selambaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalgal urundoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalgal urundoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">veiyil kuliruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veiyil kuliruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">verva malayaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verva malayaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kudyaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kudyaguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalaikku mela rekka mulaichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaikku mela rekka mulaichu"/>
</div>
<div class="lyrico-lyrics-wrapper">parakka sollum kathal kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakka sollum kathal kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaikkelaga thanam paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaikkelaga thanam paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakka sollum kadhal kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakka sollum kadhal kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">eh then oru vazhiyaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh then oru vazhiyaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">naan un kitta pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan un kitta pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada tholanjasu baasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada tholanjasu baasha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa ini thadaye illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa ini thadaye illa "/>
</div>
<div class="lyrico-lyrics-wrapper">namm kathala solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namm kathala solla"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu kattikka mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu kattikka mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katti karumbe kattikkathane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti karumbe kattikkathane "/>
</div>
<div class="lyrico-lyrics-wrapper">kutti karanam pottene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti karanam pottene"/>
</div>
<div class="lyrico-lyrics-wrapper">sattunu unna muttina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattunu unna muttina "/>
</div>
<div class="lyrico-lyrics-wrapper">pinne sikki thavichene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinne sikki thavichene"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thottu palagida ketta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thottu palagida ketta "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula vittu tholachi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula vittu tholachi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kidanthida mutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kidanthida mutti "/>
</div>
<div class="lyrico-lyrics-wrapper">utharida kittakka vanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utharida kittakka vanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">o vekkathula ninna oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o vekkathula ninna oh"/>
</div>
<div class="lyrico-lyrics-wrapper">vegavachu thinna oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegavachu thinna oh"/>
</div>
<div class="lyrico-lyrics-wrapper">uthadu madippil thadukki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthadu madippil thadukki "/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthu sokki kidakkurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthu sokki kidakkurene"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu niraya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu niraya "/>
</div>
<div class="lyrico-lyrics-wrapper">vara vacha malaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara vacha malaya"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu karayura vetti neliyithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu karayura vetti neliyithu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna panna solludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna panna solludaa"/>
</div>
</pre>
