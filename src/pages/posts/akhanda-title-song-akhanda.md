---
title: "akhanda title song song lyrics"
album: "Akhanda"
artist: "S. Thaman"
lyricist: "Ananta Sriram"
director: "Boyapati Srinu"
path: "/albums/akhanda-lyrics"
song: "Akhanda Title Song"
image: ../../images/albumart/akhanda.jpg
date: 2021-12-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9n1refHexDY"
type: "title track"
singers:
  - Shankar Mahadevan
  - Siddharth Mahadevan
  - Shivam Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho shambo shiva shambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho shambo shiva shambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam kam kangumandhi shankam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam kam kangumandhi shankam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadagamandi pankam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadagamandi pankam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaavukaina jankam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaavukaina jankam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham dham darmaberi shabdam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham dham darmaberi shabdam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyamandi yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyamandi yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Denikaina siddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Denikaina siddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bayankara lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bayankara lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee trayambakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee trayambakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa mayaskara nee sarike puranthakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa mayaskara nee sarike puranthakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa haromhara jatadhara jayinchara pratpara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa haromhara jatadhara jayinchara pratpara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham akhanda dham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda dham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Loka maayatho paathara bumi pai adhi jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loka maayatho paathara bumi pai adhi jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham akhanda bham bham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda bham bham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaga jwalamai paathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaga jwalamai paathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenula kalla ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenula kalla ninda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho shambo shiva shambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho shambo shiva shambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho shambo hara hara swayambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho shambo hara hara swayambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho shambo shiva shambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho shambo shiva shambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho shambo hara hara swayambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho shambo hara hara swayambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Natarajivi rajamana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natarajivi rajamana "/>
</div>
<div class="lyrico-lyrics-wrapper">kalaswarpa bhushana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaswarpa bhushana"/>
</div>
<div class="lyrico-lyrics-wrapper">Peenakapani vallabam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peenakapani vallabam "/>
</div>
<div class="lyrico-lyrics-wrapper">prachanda chanda pahimam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prachanda chanda pahimam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaativati kapuradhi paadalochana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaativati kapuradhi paadalochana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwanatha pahimam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwanatha pahimam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedevado harom hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedevado harom hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadevado harom hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadevado harom hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Peethaladhi aathaladhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peethaladhi aathaladhi "/>
</div>
<div class="lyrico-lyrics-wrapper">narakkura narakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narakkura narakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Velliricheyi shivom hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velliricheyi shivom hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallirichey shivom hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallirichey shivom hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Narakkura narakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narakkura narakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dham akhanda dham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda dham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Loka maayatho paathara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loka maayatho paathara "/>
</div>
<div class="lyrico-lyrics-wrapper">bumi pai adhi jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bumi pai adhi jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham akhanda bham bham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda bham bham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaga jwalamai paathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaga jwalamai paathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenula kalla ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenula kalla ninda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram ram paalanetra dwaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram ram paalanetra dwaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Teruchukunte goram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teruchukunte goram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaminka thimiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaminka thimiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jam jam thandavala tajyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jam jam thandavala tajyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Brovulinka dagdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brovulinka dagdham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee dharathalam hundello halahalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee dharathalam hundello halahalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa dahinchaga neeve raa mahalayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa dahinchaga neeve raa mahalayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa treeshulivai kaapalivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa treeshulivai kaapalivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yugani vai aghorivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugani vai aghorivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham akhanda dham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda dham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Loka maayatho paathara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loka maayatho paathara "/>
</div>
<div class="lyrico-lyrics-wrapper">bumi pai adhi jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bumi pai adhi jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham akhanda bham bham akhanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham akhanda bham bham akhanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaga jwalamai paathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaga jwalamai paathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenula kalla ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenula kalla ninda"/>
</div>
</pre>
