---
title: "rasikatha song lyrics"
album: "Captain Rana Prathap"
artist: "Shran"
lyricist: "Mani Lekya"
director: "Haranath Policherla"
path: "/albums/captain-rana-prathap-lyrics"
song: "Rasikatha"
image: ../../images/albumart/captain-rana-prathap.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/oULG-D6hFUQ"
type: "mass"
singers:
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikatha ekantham choopana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikatha ekantham choopana"/>
</div>
<div class="lyrico-lyrics-wrapper">sarasapu vinayasam nerpana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarasapu vinayasam nerpana"/>
</div>
<div class="lyrico-lyrics-wrapper">ragileti yathana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragileti yathana "/>
</div>
<div class="lyrico-lyrics-wrapper">usurantha penchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurantha penchana"/>
</div>
<div class="lyrico-lyrics-wrapper">thapanantha chatuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapanantha chatuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanuvantha meetana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanuvantha meetana"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thadisipovali rathi chatuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thadisipovali rathi chatuna"/>
</div>
<div class="lyrico-lyrics-wrapper">o vishapu jwalai ne nimurukona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o vishapu jwalai ne nimurukona"/>
</div>
<div class="lyrico-lyrics-wrapper">kasiga ninnu kalcheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasiga ninnu kalcheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">ohoo viru kaiputho muncheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohoo viru kaiputho muncheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andham akalithona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andham akalithona"/>
</div>
<div class="lyrico-lyrics-wrapper">allade neekena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allade neekena"/>
</div>
<div class="lyrico-lyrics-wrapper">pranayam chejarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranayam chejarena"/>
</div>
<div class="lyrico-lyrics-wrapper">magasiri vanchchalathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magasiri vanchchalathona"/>
</div>
<div class="lyrico-lyrics-wrapper">virahalla vetallo kona pantithona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virahalla vetallo kona pantithona "/>
</div>
<div class="lyrico-lyrics-wrapper">anuanuvu nettutila marcheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anuanuvu nettutila marcheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">oore yathana uduku penchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore yathana uduku penchana"/>
</div>
<div class="lyrico-lyrics-wrapper">usuru thiyana neelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuru thiyana neelona"/>
</div>
<div class="lyrico-lyrics-wrapper">yugamu kshanamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yugamu kshanamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">maruthundhana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthundhana "/>
</div>
<div class="lyrico-lyrics-wrapper">madhanuni thaapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhanuni thaapana"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valache cheekatilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valache cheekatilona"/>
</div>
<div class="lyrico-lyrics-wrapper">jare chematallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jare chematallona"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam aapesthuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam aapesthuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanuvula thakidithona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanuvula thakidithona"/>
</div>
<div class="lyrico-lyrics-wrapper">dharicheri dharpanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharicheri dharpanne"/>
</div>
<div class="lyrico-lyrics-wrapper">kougillathona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kougillathona "/>
</div>
<div class="lyrico-lyrics-wrapper">anuvanuvu naligindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anuvanuvu naligindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">magachatuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magachatuna "/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha "/>
</div>
<div class="lyrico-lyrics-wrapper">panti gatukai parithapinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panti gatukai parithapinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">valapu dhadine koregana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapu dhadine koregana"/>
</div>
<div class="lyrico-lyrics-wrapper">rathilo needila sathiga maraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathilo needila sathiga maraga"/>
</div>
<div class="lyrico-lyrics-wrapper">pedhavitho thalapadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedhavitho thalapadana"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulemusaya kunakaboyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulemusaya kunakaboyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhalo alajadinaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhalo alajadinaina"/>
</div>
<div class="lyrico-lyrics-wrapper">theere nee magasirithona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theere nee magasirithona"/>
</div>
<div class="lyrico-lyrics-wrapper">teeram cheresana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teeram cheresana"/>
</div>
<div class="lyrico-lyrics-wrapper">muddhula vanthenathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muddhula vanthenathona"/>
</div>
<div class="lyrico-lyrics-wrapper">kunukemo sadalindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunukemo sadalindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mathulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mathulona"/>
</div>
<div class="lyrico-lyrics-wrapper">paravashamai pongindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravashamai pongindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee painana haddhu dhatila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee painana haddhu dhatila"/>
</div>
<div class="lyrico-lyrics-wrapper">haayi choopina saayamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haayi choopina saayamai"/>
</div>
<div class="lyrico-lyrics-wrapper">ala cheragana thapame illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ala cheragana thapame illa"/>
</div>
<div class="lyrico-lyrics-wrapper">thanivitheeraga odilo odhigeyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanivitheeraga odilo odhigeyana"/>
</div>
</pre>
