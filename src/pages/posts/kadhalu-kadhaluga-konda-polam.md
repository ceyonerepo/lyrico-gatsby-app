---
title: "kadhalu kadhaluga song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "M. M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Kadhalu Kadhaluga"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-jBxdMLbzPM"
type: "melody"
singers:
  - Kailash Kher
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhalu kadhaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalu kadhaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalalu migileyy naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalalu migileyy naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalu kadhaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalu kadhaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalalu migileyy naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalalu migileyy naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudalu leeni aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudalu leeni aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhala gathulu adaave naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhala gathulu adaave naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhani chappave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhani chappave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam adagake manaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam adagake manaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashani bhikshaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashani bhikshaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiga ivvave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiga ivvave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalu kadhaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalu kadhaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalalu migileyy naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalalu migileyy naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudalu leeni aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudalu leeni aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhala gathulu adaave naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhala gathulu adaave naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parichayam ayinaa gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayam ayinaa gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oopiree chelisthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oopiree chelisthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavasam ayina praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavasam ayina praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanuvantha dahisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanuvantha dahisthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tegaa yedhuru chusi thadisindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tegaa yedhuru chusi thadisindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantipaape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantipaape"/>
</div>
<div class="lyrico-lyrics-wrapper">Sega raguluthunna yedha kori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sega raguluthunna yedha kori"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooradimpe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooradimpe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalu kadhaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalu kadhaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalalu migiley naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalalu migiley naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithiki chithiki aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithiki chithiki aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhala bathuka chithike naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhala bathuka chithike naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adigithe ninnu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe ninnu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dhaare yetantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhaare yetantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhuluga neeku nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhuluga neeku nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Chebutaavo rahasyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chebutaavo rahasyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye dhooli lona kalisindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dhooli lona kalisindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee prapanchaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee prapanchaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa anuvu anuvu vethakaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa anuvu anuvu vethakaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vikaasaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vikaasaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cherani gamyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherani gamyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruvai oo kashanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruvai oo kashanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppada ee nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppada ee nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammadam jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammadam jeevitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammadam jeevitham ninuu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammadam jeevitham ninuu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammadam jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammadam jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammadam jeevitham ninunuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammadam jeevitham ninunuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammadam jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammadam jeevitham"/>
</div>
</pre>
