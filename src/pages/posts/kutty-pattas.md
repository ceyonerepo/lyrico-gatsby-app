---
title: "kutty pattas"
album: "Sony Music India"
artist: "Santhosh Dhayanidhi"
lyricist: "A.Pa Raja"
director: "Venki"
path: "/albums/kutty-pattas-song-lyrics"
song: "Kutty Pattas"
image: ../../images/albumart/kutty-pattas.jpg
date: 2021-03-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KfUExOXxU5Q"
type: "album"
singers:
  - Santhosh Dhayanidhi
  - Rakshita Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyea Adiyea En Kutty Pattas’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyea Adiyea En Kutty Pattas’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniye Thaniye Vanthu Vittu Velasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Thaniye Vanthu Vittu Velasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Uyire Enna Manichu Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Uyire Enna Manichu Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal Thee Visaatha Deee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal Thee Visaatha Deee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Ree Ga Ma Pa Tha Nee Naan Solli Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ree Ga Ma Pa Tha Nee Naan Solli Tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikura Aasaiya Naan Kotti Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikura Aasaiya Naan Kotti Vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varukira Aavani Naan Veethil Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varukira Aavani Naan Veethil Sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaivezhi Yen, Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaivezhi Yen, Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Muttam Veikka Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Muttam Veikka Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum Anjadi Pafum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Anjadi Pafum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkum Kannula Kanja Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkum Kannula Kanja Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Siritcha Sinthidum Sintheni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siritcha Sinthidum Sintheni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muratcha Kiridum Kanaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muratcha Kiridum Kanaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udamba Sakkara Vazhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udamba Sakkara Vazhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasil Minura Azhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasil Minura Azhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Pakkama, Vara Vekkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Pakkama, Vara Vekkamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeladi Kannama Solladi Chellama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeladi Kannama Solladi Chellama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Munmunukkura Muthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Munmunukkura Muthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Enna Sattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Enna Sattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vezhiya Adha Sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vezhiya Adha Sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuril Inga Yuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuril Inga Yuthamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kazhi, Nenja Kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kazhi, Nenja Kizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thalli Vekkalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thalli Vekkalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja Sollu, Unna Azhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja Sollu, Unna Azhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Ghili Pola Thulli Varuvean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Ghili Pola Thulli Varuvean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thitti Thitti Sernthukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Thitti Sernthukalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Othi Othi Kattikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othi Othi Kattikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Potta Maatikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Potta Maatikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Pillai Petukalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Pillai Petukalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu Vittu Modhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vittu Modhikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Thottu Thedikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Thottu Thedikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Kurayea, Thotta Kurayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Kurayea, Thotta Kurayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattapadi Thananananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattapadi Thananananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Neeyagi Poneneayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyagi Poneneayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Azhaagi Ponaayeano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Azhaagi Ponaayeano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Kidanthu, Naatkal Idhuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Kidanthu, Naatkal Idhuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal Neer Pola Enmaatrinay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Neer Pola Enmaatrinay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Potaathu Innum Thazhi Nee Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Potaathu Innum Thazhi Nee Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Aakavea Sogam Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Aakavea Sogam Kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pola Nadakaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola Nadakaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Manitchidu Chella Kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Manitchidu Chella Kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatham Vezhi Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatham Vezhi Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Nenja Vechu Kaathu Kidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenja Vechu Kaathu Kidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Munmunukkura Muthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Munmunukkura Muthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Enna Sattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Enna Sattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vezhiya Adha Sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vezhiya Adha Sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuril Inga Yuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuril Inga Yuthamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kazhi, Nenja Kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kazhi, Nenja Kizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thalli Vekkalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thalli Vekkalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja Sollu,Unna Azhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja Sollu,Unna Azhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Ghili Pola Thulli Varuvean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Ghili Pola Thulli Varuvean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Ree Ga Ma Pa Tha Nee Naan Solli Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ree Ga Ma Pa Tha Nee Naan Solli Tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikurara Aasaiya Naan Kotti Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikurara Aasaiya Naan Kotti Vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varukira Aavani Naan Veethil Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varukira Aavani Naan Veethil Sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ideyvezhi Yen, Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ideyvezhi Yen, Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Muttam Veykka Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Muttam Veykka Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum Anjadi Pafum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Anjadi Pafum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkum Kannula Kanja Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkum Kannula Kanja Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Siritcha Sinthidum Sintheni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siritcha Sinthidum Sintheni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muratcha Kiridum Kanaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muratcha Kiridum Kanaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udamba Sakkara Vazhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udamba Sakkara Vazhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasil Minura Azhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasil Minura Azhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Pakkama, Vara Vekkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Pakkama, Vara Vekkamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Neeladi Kannama Solladi Chellama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Neeladi Kannama Solladi Chellama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munmunukkura Muthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munmunukkura Muthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Enna Sattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Enna Sattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vezhiya Adha Sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vezhiya Adha Sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuril Inga Yuthammaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuril Inga Yuthammaaa"/>
</div>
</pre>
