---
title: "un perai sonnale song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Un Perai Sonnale"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ydrE_7tQiN0"
type: "love"
singers:
  - Unnikrishnan
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Perai Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulnaakil Thithikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnaakil Thithikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathe Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathe Pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Poopookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Poopookume"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaaiyo Varaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Varaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Perai Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulnaakil Thithikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnaakil Thithikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathe Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathe Pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Poopookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Poopookume"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaaiyo Varaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Varaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondraa Irandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraa Irandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kodi Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kodi Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thinna Paarkuthe Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thinna Paarkuthe Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundai Thundai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundai Thundai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyil Vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyil Vizhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee En Kanne…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee En Kanne…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jimtaara Jimtaara Jimthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimtaara Jimtaara Jimthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimtaara Jimtaara Jimthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimtaara Jimtaara Jimthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimtaara Jimtaara Jimthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimtaara Jimtaara Jimthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimtaara Jimtaara Jimthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimtaara Jimtaara Jimthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei Ezhuthum Marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Ezhuthum Marandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ezhuthum Marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ezhuthum Marandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaiyai Naanum Aaginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyai Naanum Aaginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyasudum Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyasudum Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai Thodum Pillaipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai Thodum Pillaipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiye Meendum Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiye Meendum Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaisudum Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaisudum Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai Thodum Pillaipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai Thodum Pillaipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiye Meendum Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiye Meendum Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimel Adiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimel Adiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Melam Pol Manadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melam Pol Manadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vero Udal Vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vero Udal Vero"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyaa Vidhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyaa Vidhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedi Mel Idiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedi Mel Idiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellaathe Sellaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellaathe Sellaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Perai Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulnaakil Thithikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnaakil Thithikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Nee Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Poopookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Poopookume"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaaiyo Varaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Varaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Nee Enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondraa Irandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraa Irandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kodi Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kodi Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thinna Paarkuthe Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thinna Paarkuthe Nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundai Thundai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundai Thundai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyil Vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyil Vizhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee En Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee En Nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivillai Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivillai Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamillai Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamillai Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Solvaai Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Solvaai Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Thozhan Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thozhan Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhipokan Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhipokan Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Enna Solvaai Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Enna Solvaai Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Thozhan Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thozhan Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhipokan Enbaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhipokan Enbaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Enna Solvaai Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Enna Solvaai Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanjaadum Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjaadum Sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandhranai Azhavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhranai Azhavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Yen Solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Yen Solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthaalam Poovukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthaalam Poovukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalondru Varavaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalondru Varavaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaagum Solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaagum Solvaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Perai Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulnaakil Thithikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnaakil Thithikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Nee Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Sendraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Sendraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Poopookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Poopookume"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaaiyo Varaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaaiyo Varaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enge Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enge Nee Enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondraa Irandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraa Irandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kodi Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kodi Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thinna Paarkuthe Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thinna Paarkuthe Nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundai Thundai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundai Thundai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyil Vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyil Vizhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee En Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee En Nanbaa"/>
</div>
</pre>
