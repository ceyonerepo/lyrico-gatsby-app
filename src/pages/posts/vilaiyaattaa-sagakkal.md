---
title: "vilaiyaattaa song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Yugabharathi"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Vilaiyaattaa"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wS6iUMirQOk"
type: "happy"
singers:
  - Ranjith
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Velaiyaattaa yoasippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa yoasippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa paarthuppoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa paarthuppoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa Love-u pannuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa Love-u pannuvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">cell phone-il yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone-il yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orkut-til yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orkut-til yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebookkil koodukkattuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebookkil koodukkattuvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">yae adangaama naalellaam kokkarippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yae adangaama naalellaam kokkarippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">arasaanga perundhil sokki nirppoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasaanga perundhil sokki nirppoam"/>
</div>
<div class="lyrico-lyrics-wrapper">nerathai marandhiruppoam thoappoaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerathai marandhiruppoam thoappoaram"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkaththa thorandhiruppoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkaththa thorandhiruppoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa yoasippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa yoasippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa paarthuppoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa paarthuppoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa Love-u pannuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa Love-u pannuvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">cell phone-il yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone-il yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orkut-til yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orkut-til yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebookkil koodukkattuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebookkil koodukkattuvoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoadu kannaaga kaadhalai ennaaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoadu kannaaga kaadhalai ennaaama"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaadaa kodumai idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaadaa kodumai idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjoadu nenjaaga naesamum illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjoadu nenjaaga naesamum illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">thikkettum thirigiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikkettum thirigiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veedhikki veedhi joadiyathedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedhikki veedhi joadiyathedi"/>
</div>
<div class="lyrico-lyrics-wrapper">poaradhu kaadhal illa vayasoada tholladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaradhu kaadhal illa vayasoada tholladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalukku naalu naesamum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalukku naalu naesamum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">naesamum kooda naadagamaayiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naesamum kooda naadagamaayiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamedhummilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamedhummilladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">raaman kaadhal kondadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaman kaadhal kondadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">seethaavin sela maela illadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seethaavin sela maela illadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei raadhai nesam kondadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei raadhai nesam kondadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishnaavin leelaikkaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnaavin leelaikkaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">illadaa daa daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illadaa daa daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa yoasippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa yoasippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa paarthuppoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa paarthuppoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaattaa Love-u pannuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaattaa Love-u pannuvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">cell phone-il yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone-il yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orkut-til yaasippoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orkut-til yaasippoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebookkil koodukkattuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebookkil koodukkattuvoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meiyedhum illaadha kaadhalin kacheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyedhum illaadha kaadhalin kacheri"/>
</div>
<div class="lyrico-lyrics-wrapper">sattendru mudindhuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattendru mudindhuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thappedhum seiyaadha kaadhaley manmeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappedhum seiyaadha kaadhaley manmeedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">endrendrum thodarndhu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrendrum thodarndhu varum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbaana kaadhal ammaavappoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbaana kaadhal ammaavappoala"/>
</div>
<div class="lyrico-lyrics-wrapper">eppoadhum nenjikkulle nilaiyaaga thangumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppoadhum nenjikkulle nilaiyaaga thangumey"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyaana kaadhal thanneerappoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyaana kaadhal thanneerappoala"/>
</div>
<div class="lyrico-lyrics-wrapper">nirkkaadhu Oridathil alaippaaya ennumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirkkaadhu Oridathil alaippaaya ennumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamban paadivachadhu innaalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamban paadivachadhu innaalil "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril poana seidhidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril poana seidhidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kannathaasan sonnadhum kan munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kannathaasan sonnadhum kan munney"/>
</div>
<div class="lyrico-lyrics-wrapper">naamum kaanum poiyadaa daa daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamum kaanum poiyadaa daa daa daa"/>
</div>
</pre>
