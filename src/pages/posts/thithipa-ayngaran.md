---
title: "thithipa song lyrics"
album: "Ayngaran"
artist: "G.V. Prakash Kumar"
lyricist: "Vivek"
director: "Ravi Arasu"
path: "/albums/ayngaran-lyrics"
song: "Thithipa"
image: ../../images/albumart/ayngaran.jpg
date: 2022-01-26
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - G.V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Huu Huu Wooah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huu Huu Wooah"/>
</div>
<div class="lyrico-lyrics-wrapper">Huu Huu Woooah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huu Huu Woooah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithippaa Thithipaa Thithipaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithippaa Thithipaa Thithipaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalin Neradi Santhippaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalin Neradi Santhippaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochai Thaakki Moorchai Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochai Thaakki Moorchai Aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Thoovum Maaya Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Thoovum Maaya Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Kaatril Pesum Keetraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaatril Pesum Keetraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Sollum Neela Mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Sollum Neela Mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhin Oram Neeyum Theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhin Oram Neeyum Theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadae Yengi Thaavuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadae Yengi Thaavuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithippaa Thithipaa Thithipaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithippaa Thithipaa Thithipaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalin Neradi Santhippaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalin Neradi Santhippaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolai Thaakki Oomai Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolai Thaakki Oomai Aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Thoovum Maaya Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Thoovum Maaya Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochai Thaakki Moorchai Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochai Thaakki Moorchai Aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Engoo Kondu Serkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engoo Kondu Serkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Thaangum Thimirodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thaangum Thimirodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Paarkkum Kaal Aniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Paarkkum Kaal Aniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Paartha Siru Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Paartha Siru Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Vaazhvin Saadhanaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vaazhvin Saadhanaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Nee Nillu Hoo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Nee Nillu Hoo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Pogaadhaehoo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Pogaadhaehoo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Sendru Oorai Kooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Sendru Oorai Kooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kaatta Vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaatta Vendumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Huu Huu Wooah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Huu Huu Wooah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Huuu Huu Woooah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Huuu Huu Woooah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavizhaamal Vizhi Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavizhaamal Vizhi Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraindhunnai Paarkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraindhunnai Paarkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhil Kaatil Edhu Unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhil Kaatil Edhu Unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Inmai Ketkkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Inmai Ketkkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarai Pol Nindren Hoo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Pol Nindren Hoo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoozhai Pol Aanaen Hoo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoozhai Pol Aanaen Hoo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Naan Un Kaigal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Naan Un Kaigal Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Inbam Kaanuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Inbam Kaanuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithippaa Thithipaa Thithipaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithippaa Thithipaa Thithipaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalin Neradi Santhippaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalin Neradi Santhippaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Kaatril Pesum Keetraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaatril Pesum Keetraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Sollum Neela Mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Sollum Neela Mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochai Thaakki Moorchai Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochai Thaakki Moorchai Aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Thoovum Maaya Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Thoovum Maaya Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhin Oram Neeyum Theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhin Oram Neeyum Theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadae Yengi Thaavuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadae Yengi Thaavuthadi"/>
</div>
</pre>
