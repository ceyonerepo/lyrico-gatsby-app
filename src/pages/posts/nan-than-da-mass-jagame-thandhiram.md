---
title: "nan than da mass song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Nan Than Da Mass"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2021-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CzUslqxBwz0"
type: "mass"
singers:
  - Ofro
  - Arivu
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nan than da mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan than da mass"/>
</div>
<div class="lyrico-lyrics-wrapper">veraaru boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraaru boss"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha class"/>
</div>
<div class="lyrico-lyrics-wrapper">ukkaru pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukkaru pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">theraatha case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theraatha case"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaantha race
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaantha race"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">mannoda close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannoda close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i am the blackie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am the blackie"/>
</div>
<div class="lyrico-lyrics-wrapper">jackie thakkum eetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jackie thakkum eetti"/>
</div>
<div class="lyrico-lyrics-wrapper">paathukko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathukko "/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda potti pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda potti pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkum thought - ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkum thought - ah"/>
</div>
<div class="lyrico-lyrics-wrapper">maathikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oyaatha heat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaatha heat"/>
</div>
<div class="lyrico-lyrics-wrapper">oorellam fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorellam fight"/>
</div>
<div class="lyrico-lyrics-wrapper">aavatha pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavatha pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaaram aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaaram aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ullara hate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullara hate"/>
</div>
<div class="lyrico-lyrics-wrapper">meloda sweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meloda sweet"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnada pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnada pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnaala vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnaala vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh aalukkaaga mothikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh aalukkaaga mothikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathi koottam maandu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathi koottam maandu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha verupaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha verupaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodupodu gaali aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodupodu gaali aache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jagathai azhiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagathai azhiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">janaththai thinam pirikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janaththai thinam pirikira"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathirai kizhiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathirai kizhiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">araththai ani thirattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araththai ani thirattu"/>
</div>
<div class="lyrico-lyrics-wrapper">agaththinul veruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agaththinul veruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">velippuraththil anaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velippuraththil anaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">madaththanam maruththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madaththanam maruththu "/>
</div>
<div class="lyrico-lyrics-wrapper">sarikku samam niruththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarikku samam niruththu "/>
</div>
<div class="lyrico-lyrics-wrapper">sithari kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithari kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiri pagai athira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiri pagai athira "/>
</div>
<div class="lyrico-lyrics-wrapper">unathu kuralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu kuralai"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ezhuppidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ezhuppidu"/>
</div>
<div class="lyrico-lyrics-wrapper">paravi kidakkum pirivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravi kidakkum pirivai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai uravu ena uraithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai uravu ena uraithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i am the blackie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am the blackie"/>
</div>
<div class="lyrico-lyrics-wrapper">jackie thakkum eetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jackie thakkum eetti"/>
</div>
<div class="lyrico-lyrics-wrapper">paathukko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathukko "/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda potti pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda potti pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkum thought - ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkum thought - ah"/>
</div>
<div class="lyrico-lyrics-wrapper">maathikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aapu senju aalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aapu senju aalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu seeviaache 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu seeviaache "/>
</div>
<div class="lyrico-lyrics-wrapper">pottu thalla pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu thalla pona"/>
</div>
<div class="lyrico-lyrics-wrapper">gundu goli aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu goli aache"/>
</div>
<div class="lyrico-lyrics-wrapper">wrong-uh panna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wrong-uh panna "/>
</div>
<div class="lyrico-lyrics-wrapper">raasu kutti thaaviyaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasu kutti thaaviyaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thanda mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thanda mass"/>
</div>
<div class="lyrico-lyrics-wrapper">veraaru boss eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraaru boss eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aei vilagu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aei vilagu da"/>
</div>
<div class="lyrico-lyrics-wrapper">thee varuthu da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee varuthu da "/>
</div>
<div class="lyrico-lyrics-wrapper">naan iranguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan iranguna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee erumbu erumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee erumbu erumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aei vilagu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aei vilagu da"/>
</div>
<div class="lyrico-lyrics-wrapper">thee varuthu da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee varuthu da "/>
</div>
<div class="lyrico-lyrics-wrapper">naan iranguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan iranguna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee erumbu erumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee erumbu erumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan than da mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan than da mass"/>
</div>
<div class="lyrico-lyrics-wrapper">veraaru boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraaru boss"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha class"/>
</div>
<div class="lyrico-lyrics-wrapper">ukkaru pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukkaru pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">theraatha case
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theraatha case"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaantha race
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaantha race"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">mannoda close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannoda close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i am the blackie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am the blackie"/>
</div>
<div class="lyrico-lyrics-wrapper">jackie thakkum eetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jackie thakkum eetti"/>
</div>
<div class="lyrico-lyrics-wrapper">paathukko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathukko "/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda potti pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda potti pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkum thought - ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkum thought - ah"/>
</div>
<div class="lyrico-lyrics-wrapper">maathikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan than da mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan than da mass"/>
</div>
<div class="lyrico-lyrics-wrapper">veraaru boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraaru boss"/>
</div>
<div class="lyrico-lyrics-wrapper">nan than da mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan than da mass"/>
</div>
<div class="lyrico-lyrics-wrapper">veraaru boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraaru boss"/>
</div>
</pre>
