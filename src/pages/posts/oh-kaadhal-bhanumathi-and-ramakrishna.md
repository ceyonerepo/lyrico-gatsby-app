---
title: "oh kaadhal song lyrics"
album: "Bhanumathi & Ramakrishna"
artist: "Shravan Bharadwaj"
lyricist: "Krishna Kanth"
director: "Srikanth Nagothi"
path: "/albums/bhanumathi-and-ramakrishna-lyrics"
song: "Oh Kaadhal"
image: ../../images/albumart/bhanumathi-and-ramakrishna.jpg
date: 2020-07-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/h8uIKQK6rT0"
type: "love"
singers:
  - Shravan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey mella mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mella mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopame kose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopame kose"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga navvule poose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga navvule poose"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga mouname theesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga mouname theesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga nannu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga nannu choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga dharilo haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga dharilo haye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga dhoorame poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga dhoorame poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga ardhame ayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga ardhame ayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mellaga dhggaraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga dhggaraye"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innallu machukaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu machukaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnana lene lena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnana lene lena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudemo chuse kannulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudemo chuse kannulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Parigetthe gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parigetthe gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipoye vegamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipoye vegamemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vintu vintu pranam emayindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintu vintu pranam emayindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponu ponu madhyanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponu ponu madhyanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamedho thagge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamedho thagge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranu ranu aa maataloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranu ranu aa maataloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham ayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham ayye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paina paina oohalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paina paina oohalevo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamalle maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamalle maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggarayye dhaarilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggarayye dhaarilona"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">O kaadhal O kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kaadhal O kaadhal"/>
</div>
</pre>