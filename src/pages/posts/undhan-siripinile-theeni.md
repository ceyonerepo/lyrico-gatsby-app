---
title: "undhan siripinile song lyrics"
album: "Theeni"
artist: "Rajesh Murugesan"
lyricist: "Ko Sesha"
director: "Ani.I.V.Sasi"
path: "/albums/theeni-lyrics"
song: "Undhan Siripinile"
image: ../../images/albumart/theeni.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7hVnfCImSF4"
type: "Melody"
singers:
  - Vijay Yesudas
  - Rajesh Murugesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<div class="lyrico-lyrics-wrapper">kannin neer kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannin neer kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">irulil oli paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulil oli paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">baashai edharkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baashai edharkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">mounam pesudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam pesudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum inbam serkaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum inbam serkaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan paarvai ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan paarvai ovvondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<div class="lyrico-lyrics-wrapper">kannin neer kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannin neer kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paarkkum dhisaigal yaavilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkkum dhisaigal yaavilum "/>
</div>
<div class="lyrico-lyrics-wrapper">pani pookal pookka kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani pookal pookka kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai ninaikum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ninaikum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">uraigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uraigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai megam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai megam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhil aasai aayiram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhil aasai aayiram "/>
</div>
<div class="lyrico-lyrics-wrapper">avai theera jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avai theera jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">theerthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vazhiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vazhiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyai serndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyai serndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">en thanimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thanimai"/>
</div>
<div class="lyrico-lyrics-wrapper">naatkal oyndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatkal oyndhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<div class="lyrico-lyrics-wrapper">kannin neer kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannin neer kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan sirippinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sirippinile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuvandu pogum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuvandu pogum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivu manadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivu manadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">theatrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theatrume"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan viralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">theendum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjin kaayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjin kaayam "/>
</div>
<div class="lyrico-lyrics-wrapper">mella aarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella aarume"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan tholil saayave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan tholil saayave"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir aasai oonjal aadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir aasai oonjal aadume"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu kanavil thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu kanavil thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchiya illai enakkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchiya illai enakkul"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaai nigazhudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaai nigazhudha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam maari"/>
</div>
<div class="lyrico-lyrics-wrapper">pona podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">neengume un gnabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengume un gnabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivil tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivil tholaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivil tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivil tholaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam maari"/>
</div>
<div class="lyrico-lyrics-wrapper">pona podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">neengume un gnabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengume un gnabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivil tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivil tholaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivil tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivil tholaindhen"/>
</div>
</pre>
