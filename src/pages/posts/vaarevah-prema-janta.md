---
title: "vaarevah song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "Vaarevah"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Faa7M9uuFQg"
type: "love"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho nenu unte vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho nenu unte vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho ooshiste vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho ooshiste vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni chillibili palakula pilupula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni chillibili palakula pilupula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na parugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na parugulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni pedavulu palikina palukula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni pedavulu palikina palukula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na urakalu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na urakalu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nagavulu naligina valapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nagavulu naligina valapula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai na glupulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na glupulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nadakalu nadichina adugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nadakalu nadichina adugula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai edu adugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai edu adugulu vareva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho nenu unte vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho nenu unte vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho ooshiste vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho ooshiste vareva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholi chupu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi chupu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi navvu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi navvu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi sari ga premiste vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi sari ga premiste vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi mata vareva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi mata vareva "/>
</div>
<div class="lyrico-lyrics-wrapper">tholi ata vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi ata vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi premai kalisosthe vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi premai kalisosthe vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi sari ga nene va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi sari ga nene va va"/>
</div>
<div class="lyrico-lyrics-wrapper">nee valle va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee valle va va"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi premaku ardham ante vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi premaku ardham ante vareva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ni chillibili palakula pilupula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni chillibili palakula pilupula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na parugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na parugulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni pedavulu palikina palukula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni pedavulu palikina palukula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na urakalu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na urakalu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nagavulu naligina valapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nagavulu naligina valapula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai na glupulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na glupulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nadakalu nadichina adugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nadakalu nadichina adugula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai edu adugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai edu adugulu vareva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho nenu unte vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho nenu unte vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho ooshiste vareva va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho ooshiste vareva va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooh ee maya vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh ee maya vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ee hayee vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee hayee vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">na reyi pagalantha vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na reyi pagalantha vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">naa eedu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa eedu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jodu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jodu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">na nedu repu antha vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na nedu repu antha vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ee maikam needhe va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee maikam needhe va va"/>
</div>
<div class="lyrico-lyrics-wrapper">nee lokam va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee lokam va va"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jantai chere roju vareva va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jantai chere roju vareva va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ni chillibili palakula pilupula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni chillibili palakula pilupula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na parugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na parugulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni pedavulu palikina palukula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni pedavulu palikina palukula "/>
</div>
<div class="lyrico-lyrics-wrapper">kai na urakalu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na urakalu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nagavulu naligina valapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nagavulu naligina valapula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai na glupulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai na glupulu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nadakalu nadichina adugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nadakalu nadichina adugula"/>
</div>
<div class="lyrico-lyrics-wrapper">kai edu adugulu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai edu adugulu vareva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho nenu unte vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho nenu unte vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">vareva vare vare vaa va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vareva vare vare vaa va"/>
</div>
<div class="lyrico-lyrics-wrapper">nitho ooshiste vareva va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitho ooshiste vareva va"/>
</div>
</pre>
