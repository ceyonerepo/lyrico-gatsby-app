---
title: "hey penne penne song lyrics"
album: "Vanakkam Da Mappilei"
artist: "G V Prakash Kumar"
lyricist: "Pa Vijay"
director: "M. Rajesh"
path: "/albums/vanakkam-da-mappilei-song-lyrics"
song: "Hey Penne Penne"
image: ../../images/albumart/vanakkam-da-mappilei.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/72cTV5IdvO4"
type: "Love"
singers:
  - GV Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey penne penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey penne penne"/>
</div>
<div class="lyrico-lyrics-wrapper">naan konjam paavan thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan konjam paavan thane"/>
</div>
<div class="lyrico-lyrics-wrapper">en life ah inki panki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en life ah inki panki"/>
</div>
<div class="lyrico-lyrics-wrapper">podatha di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podatha di"/>
</div>
<div class="lyrico-lyrics-wrapper">hey adi penne penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey adi penne penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nan konjam silent type
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan konjam silent type"/>
</div>
<div class="lyrico-lyrics-wrapper">en mind voice poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mind voice poora"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathi than di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathi than di"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja koluthita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja koluthita"/>
</div>
<div class="lyrico-lyrics-wrapper">smoke ah kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smoke ah kedakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeya thanikka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeya thanikka than"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya varuvaya....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya varuvaya...."/>
</div>
<div class="lyrico-lyrics-wrapper">kanne unaku en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne unaku en"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">short ah sollanum na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="short ah sollanum na"/>
</div>
<div class="lyrico-lyrics-wrapper">ne avlo seen illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne avlo seen illa"/>
</div>
<div class="lyrico-lyrics-wrapper">i am very sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am very sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">manuchudu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manuchudu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paani poori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paani poori"/>
</div>
<div class="lyrico-lyrics-wrapper">ooti viduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooti viduma"/>
</div>
<div class="lyrico-lyrics-wrapper">adikadi heart unna parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikadi heart unna parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">TR paatu kekuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TR paatu kekuthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">life pooram unna mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life pooram unna mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">baby nu konjuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baby nu konjuven"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda friend number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda friend number"/>
</div>
<div class="lyrico-lyrics-wrapper">vanga maten di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanga maten di"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala nanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala nanum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">love boy ah paduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love boy ah paduren"/>
</div>
<div class="lyrico-lyrics-wrapper">jeans pota bharathi nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeans pota bharathi nan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi katuren di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi katuren di"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja koluthita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja koluthita"/>
</div>
<div class="lyrico-lyrics-wrapper">aama koluthita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama koluthita"/>
</div>
<div class="lyrico-lyrics-wrapper">smoke ah kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smoke ah kedakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeya anaika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeya anaika than"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya varuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya varuvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne unaku en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne unaku en"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">short ah sollanum na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="short ah sollanum na"/>
</div>
<div class="lyrico-lyrics-wrapper">ne avlo seen illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne avlo seen illa"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavula un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavula un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thinam thinam pakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thinam thinam pakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">en heart ah popcorn ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en heart ah popcorn ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ne daily korikiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne daily korikiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">en life ah bootan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en life ah bootan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ne maathi muraikiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne maathi muraikiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye..... alage.....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye..... alage....."/>
</div>
<div class="lyrico-lyrics-wrapper">unna....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna...."/>
</div>
<div class="lyrico-lyrics-wrapper">nenja koluthita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja koluthita"/>
</div>
<div class="lyrico-lyrics-wrapper">smoke ah kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smoke ah kedakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeya thanikka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeya thanikka than"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya varuvaya....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya varuvaya...."/>
</div>
<div class="lyrico-lyrics-wrapper">kanne unaku en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne unaku en"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">short ah sollanum na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="short ah sollanum na"/>
</div>
<div class="lyrico-lyrics-wrapper">ne avlo seen illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne avlo seen illa"/>
</div>
<div class="lyrico-lyrics-wrapper">i am very sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am very sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">manuchudu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manuchudu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paani poori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paani poori"/>
</div>
<div class="lyrico-lyrics-wrapper">ooti viduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooti viduma"/>
</div>
<div class="lyrico-lyrics-wrapper">adikadi heart unna parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikadi heart unna parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">TR paatu kekuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TR paatu kekuthamma"/>
</div>
</pre>
