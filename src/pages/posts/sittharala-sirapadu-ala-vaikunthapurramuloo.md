---
title: "sittharala sirapadu song lyrics"
album: "Ala Vaikunthapurramuloo"
artist: "S. Thaman"
lyricist: "Vijay Kumar Bhalla"
director: "Trivikram Srinivas"
path: "/albums/ala-vaikunthapurramuloo-lyrics"
song: "Sittharala Sirapadu"
image: ../../images/albumart/ala-vaikunthapurramuloo.jpg
date: 2020-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/w_h1GawgB-U"
type: "happy"
singers:
  - Soorranna
  - Saketh Komanduri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sitharala sirapadu Sitharala sirapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharala sirapadu Sitharala sirapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattinaadaa oggane oggadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattinaadaa oggane oggadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Petthanaalu nadipedu sitharala sirapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petthanaalu nadipedu sitharala sirapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vooruru oggesina oddhandudu oggadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vooruru oggesina oddhandudu oggadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bugathodi aambothu rankesi kummabothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugathodi aambothu rankesi kummabothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugathodi aambothu rankesi kummabothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugathodi aambothu rankesi kummabothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommuloodadeesi mari peepaloodinaduro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommuloodadeesi mari peepaloodinaduro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadalippi marri chettu dheyyala kompante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadalippi marri chettu dheyyala kompante"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadalippi marri chettu dheyyala kompante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadalippi marri chettu dheyyala kompante"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheyyamutho kayyaniki thodakotti dhigaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheyyamutho kayyaniki thodakotti dhigaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammori jaathralo onti thala ravanudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammori jaathralo onti thala ravanudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammori jaathralo onti thala ravanudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammori jaathralo onti thala ravanudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntalenta padithenu gudhi gunda sesinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntalenta padithenu gudhi gunda sesinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guntalenta padithenu gudhi gunda sesinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guntalenta padithenu gudhi gunda sesinadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnuru vasthadu dhammunte rammante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnuru vasthadu dhammunte rammante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnuru vasthadu dhammunte rammante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnuru vasthadu dhammunte rammante"/>
</div>
<div class="lyrico-lyrics-wrapper">Rommumeedokkatichi kummi kummi poyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rommumeedokkatichi kummi kummi poyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rommumeedokkatichi kummi kummi poyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rommumeedokkatichi kummi kummi poyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhi mandhi naagalnei padimoorla sorasepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhi mandhi naagalnei padimoorla sorasepa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhi mandhi naagalnei padimoorla sorasepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhi mandhi naagalnei padimoorla sorasepa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odupuga ontisettho oddukattukochinaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odupuga ontisettho oddukattukochinaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odupuga ontisettho oddukattukochinaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odupuga ontisettho oddukattukochinaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samusese kandathoti dhenikaina gattipoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samusese kandathoti dhenikaina gattipoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Samusese kandlathoti dhenikaina gattipoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samusese kandlathoti dhenikaina gattipoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugadugu yesinada adhirenu avathalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugadugu yesinada adhirenu avathalodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitharala sirapadu sitharala sirapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharala sirapadu sitharala sirapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharana oori sivara sitharala sirapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharana oori sivara sitharala sirapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandupilli soopulatho gundelona gucchaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandupilli soopulatho gundelona gucchaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkanamma yenakabadda pokirolla iragadanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkanamma yenakabadda pokirolla iragadanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkanamma yenakabadda pokirolla iragadanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkanamma yenakabadda pokirolla iragadanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkanamma kallallo yela yela sukkalocche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkanamma kallallo yela yela sukkalocche"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkanamma kallallo yela yela sukkalocche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkanamma kallallo yela yela sukkalocche"/>
</div>
</pre>
