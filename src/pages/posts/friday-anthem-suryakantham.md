---
title: "friday anthem song lyrics"
album: "Suryakantham"
artist: "Mark K Robin"
lyricist: "Krishna Kanth"
director: "Pranith Bramandapally"
path: "/albums/suryakantham-lyrics"
song: "Friday Anthem"
image: ../../images/albumart/suryakantham.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/P888n5atJcA"
type: "happy"
singers:
  - Roll Rida 
  - Anurag Kulkarni
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Welcome Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome All Teens 2019
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome All Teens 2019"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Boozy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Boozy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Hazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Hazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naachenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naachenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Shot Maarenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shot Maarenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anchu Thaakoddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anchu Thaakoddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind-Lo Data Mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind-Lo Data Mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Erase Cheseddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erase Cheseddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede Bhoome Mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede Bhoome Mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Antham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antham"/>
</div>
<div class="lyrico-lyrics-wrapper">Annatte Oogeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatte Oogeddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea Okko Shot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Okko Shot"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Bayataki Raavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Bayataki Raavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraadatledemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraadatledemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Terichey Koncham Ne Soule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terichey Koncham Ne Soule"/>
</div>
<div class="lyrico-lyrics-wrapper">Floor Ki Sweatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Floor Ki Sweatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Step-Ye Vesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step-Ye Vesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Hours
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Hours"/>
</div>
<div class="lyrico-lyrics-wrapper">Annattu Kaalaannemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annattu Kaalaannemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapeddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Boozy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Boozy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Hazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Hazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naachenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naachenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Shot Maarenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shot Maarenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anchu Thaakoddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anchu Thaakoddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Aajaa Aajaa Aajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajaa Aajaa Aajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Nachindhe Tu Peele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Nachindhe Tu Peele"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Padaku Jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Padaku Jaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathrantha Manaku Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathrantha Manaku Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Drunk And Drive Chodonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drunk And Drive Chodonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Driver Tho Phone Ghoomanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Driver Tho Phone Ghoomanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkada Oodithe Smile Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada Oodithe Smile Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Akada Oodithe Jail-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akada Oodithe Jail-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhi Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhi Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uber Ola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uber Ola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhiley Mama Aa Gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhiley Mama Aa Gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogatho Mabbulu Nirmiddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatho Mabbulu Nirmiddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vashanniinka Rappiddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vashanniinka Rappiddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Shot Shot Kee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shot Shot Kee"/>
</div>
<div class="lyrico-lyrics-wrapper">Darwin Formula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darwin Formula"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakki Mottham Thippeddaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakki Mottham Thippeddaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Go Boozy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Boozy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Go Hazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Go Hazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naachenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naachenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Shot Maarenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shot Maarenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Friday Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friday Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Night Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Night Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Anchu Thaakoddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anchu Thaakoddam"/>
</div>
</pre>
