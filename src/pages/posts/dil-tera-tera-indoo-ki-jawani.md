---
title: "dil tera tera song lyrics"
album: "Indoo Ki Jawani"
artist: "Rochak Kohli"
lyricist: "Gurpreet Saini - Gautam G Sharma"
director: "Abir Sengupta"
path: "/albums/indoo-ki-jawani-lyrics"
song: "Dil Tera Tera"
image: ../../images/albumart/indoo-ki-jawani.jpg
date: 2020-12-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/_aYmymGa9xA"
type: "love"
singers:
  - Benny Dayal
  - Neeti Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rabb ne tujhe kya khoob banaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rabb ne tujhe kya khoob banaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko tera mehboob banaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko tera mehboob banaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise farmaaye kisko samjhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise farmaaye kisko samjhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tareef teri mushqil..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tareef teri mushqil.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh rabb ne tujhe kya khoob banaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rabb ne tujhe kya khoob banaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko tera mehboob banaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko tera mehboob banaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise farmaaye kisko samjhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise farmaaye kisko samjhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tareef teri mushqil..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tareef teri mushqil.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashmir bhi dekha nahi kanyakumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashmir bhi dekha nahi kanyakumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladki mili na tere jaise pyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladki mili na tere jaise pyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhko jo paaya jabse badlaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko jo paaya jabse badlaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera brain mera heart mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera brain mera heart mera dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil tera tera dil tera tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera dil tera tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tera tera hua re hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera hua re hua re"/>
</div>
<div class="lyrico-lyrics-wrapper">Paas aake tu mil zara zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paas aake tu mil zara zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hone de jo hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hone de jo hua re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agar tujhse haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar tujhse haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar tujhse ladaaya maine ishq oh jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar tujhse ladaaya maine ishq oh jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahega uthaaya maine risk jamaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahega uthaaya maine risk jamaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Fir bhi sataaye haaye jaan le jaye haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fir bhi sataaye haaye jaan le jaye haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazre teri kaatil..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazre teri kaatil.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa haath lagaaye jo tu lage chingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haath lagaaye jo tu lage chingari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chori chori kar jaaye chorbazari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chori chori kar jaaye chorbazari"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi hai boy tujhpe hi to aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi hai boy tujhpe hi to aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera brain mera heart mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera brain mera heart mera dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil tera tera dil tera tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera dil tera tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tera tera hua re hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera hua re hua re"/>
</div>
<div class="lyrico-lyrics-wrapper">Paas aake tu mil zara zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paas aake tu mil zara zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hone de jo hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hone de jo hua re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan hua manchala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan hua manchala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise paani ko ho koyi bulbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise paani ko ho koyi bulbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwabon mein tere reh gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon mein tere reh gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera brain mera heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera brain mera heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Brain mera heart mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brain mera heart mera dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil tera tera dil tera tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera dil tera tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tera tera hua re hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera hua re hua re"/>
</div>
<div class="lyrico-lyrics-wrapper">Paas aake tu mil zara zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paas aake tu mil zara zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hone de jo hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hone de jo hua re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil tera tera dil tera tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera dil tera tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tera tera hua re hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tera tera hua re hua re"/>
</div>
<div class="lyrico-lyrics-wrapper">Paas aake tu mil zara zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paas aake tu mil zara zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hone de jo hua re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hone de jo hua re"/>
</div>
</pre>
