---
title: "dia dia dole song lyrics"
album: "Avan Ivan"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bala"
path: "/albums/avan-ivan-lyrics"
song: "Dia Dia Dole"
image: ../../images/albumart/avan-ivan.jpg
date: 2011-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/k76xtbWrbZ8"
type: "happy"
singers:
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey ha hey die die tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ha hey die die tale"/>
</div>
<div class="lyrico-lyrics-wrapper">hey ha hey ada die die tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ha hey ada die die tale"/>
</div>
<div class="lyrico-lyrics-wrapper">the dragatiku tha dragatiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the dragatiku tha dragatiku"/>
</div>
<div class="lyrico-lyrics-wrapper">tha dragatiku thnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha dragatiku thnana"/>
</div>
<div class="lyrico-lyrics-wrapper">tha dragatiku the dragatiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha dragatiku the dragatiku"/>
</div>
<div class="lyrico-lyrics-wrapper">the dragatiku tanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the dragatiku tanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey ha hey die die tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ha hey die die tale"/>
</div>
<div class="lyrico-lyrics-wrapper">hey ha hey ada die die tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey ha hey ada die die tale"/>
</div>
<div class="lyrico-lyrics-wrapper">the dragatiku tha dragatiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the dragatiku tha dragatiku"/>
</div>
<div class="lyrico-lyrics-wrapper">tha dragatiku thnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha dragatiku thnana"/>
</div>
<div class="lyrico-lyrics-wrapper">tha dragatiku the dragatiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha dragatiku the dragatiku"/>
</div>
<div class="lyrico-lyrics-wrapper">the dragatiku tanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the dragatiku tanana"/>
</div>
</pre>
