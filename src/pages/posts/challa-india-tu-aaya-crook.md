---
title: "challa song lyrics"
album: "Crook"
artist: "Pritam - Babbu Maan"
lyricist: "Kumaar"
director: "Mohit Suri"
path: "/albums/crook-lyrics"
song: "challa india tu aaya"
image: ../../images/albumart/crook.jpg
date: 2010-10-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/7CbQxyftNo4"
type: "sad"
singers:
  - Babbu Mann
  - Suzanne D'Mello
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoye! Challa india tu aaya haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye! Challa india tu aaya haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Challa India tu aaya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa India tu aaya.."/>
</div>
<div class="lyrico-lyrics-wrapper">Challa India tu aaya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa India tu aaya.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoom chik chik dhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom chik chik dhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoom chik chik dhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom chik chik dhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoom chik chik dhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom chik chik dhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoom chik chik dhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom chik chik dhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoom chik chik dhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom chik chik dhoom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Challa India tu aaya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa India tu aaya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindri nu kam te laya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindri nu kam te laya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil nu khich di maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil nu khich di maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat din taxi chalaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat din taxi chalaunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve mote Dollar kamaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve mote Dollar kamaunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye challa chad ke kamaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye challa chad ke kamaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar diyaan karda badaaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar diyaan karda badaaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kise naal akhiyan ladaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kise naal akhiyan ladaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enu koi sona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enu koi sona"/>
</div>
<div class="lyrico-lyrics-wrapper">Lageya ishq ne enu thageya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lageya ishq ne enu thageya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mitran da ae usool hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mitran da ae usool hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil dena taan fazool hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil dena taan fazool hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mar ke Majnu hazar je
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mar ke Majnu hazar je"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagdi na tu vi hun na mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagdi na tu vi hun na mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Fikraan tu chadh te saariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fikraan tu chadh te saariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal do pal kar le yaariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal do pal kar le yaariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gall maan le meri yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gall maan le meri yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kite vaade shaade pyaar waale na kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kite vaade shaade pyaar waale na kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Challa akhiyan senke goriyan memaan dekhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa akhiyan senke goriyan memaan dekhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Lab ke naina de thekke jaam rajj rajj ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lab ke naina de thekke jaam rajj rajj ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Karda pooriyan aishan karda.. haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karda pooriyan aishan karda.. haaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tell me what you thinking about now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell me what you thinking about now"/>
</div>
<div class="lyrico-lyrics-wrapper">I wanna do everything wanna do it right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna do everything wanna do it right"/>
</div>
<div class="lyrico-lyrics-wrapper">May be I can help you free your mind..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="May be I can help you free your mind.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Challa tu nai na sahi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challa tu nai na sahi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hor vi mukhde ne kayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hor vi mukhde ne kayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinna te ankh jai aa gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinna te ankh jai aa gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jaana hai taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaana hai taan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hun ja saanu ni teri parwah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hun ja saanu ni teri parwah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Te ne ne, te ne ne, te ne ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te ne ne, te ne ne, te ne ne"/>
</div>
</pre>
