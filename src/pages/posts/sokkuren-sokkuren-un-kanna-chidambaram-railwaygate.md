---
title: "sokkren sokkuren un kanna song lyrics"
album: "Chidambaram Railwaygate"
artist: "Karthik Raja"
lyricist: "Priyan"
director: "Sivabalan"
path: "/albums/chidambaram-railwaygate-lyrics"
song: "Sokkuren Sokkuren Un Kanna"
image: ../../images/albumart/chidambaram-railwaygate.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aI9ru6g8CWU"
type: "Love"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sokkuren sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkuren sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">etti nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">adi bakkunu nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi bakkunu nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sokkuren sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkuren sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">etti nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">adi bakkunu nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi bakkunu nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jannal thamaraiyai malarntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jannal thamaraiyai malarntha"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathum yenadi sirucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathum yenadi sirucha"/>
</div>
<div class="lyrico-lyrics-wrapper">mothama manasa nee paricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothama manasa nee paricha"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye thaviyai thavikirandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye thaviyai thavikirandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sokkuren sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkuren sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">etti nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">adi bakkunu nan paranthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi bakkunu nan paranthen "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thookathula kanavula sirucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookathula kanavula sirucha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavulayum pecha maracha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavulayum pecha maracha"/>
</div>
<div class="lyrico-lyrics-wrapper">pavi enna maava karacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavi enna maava karacha"/>
</div>
<div class="lyrico-lyrics-wrapper">padaai paduthurayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaai paduthurayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga vara thonala enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vara thonala enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kandu pidikuthu kiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kandu pidikuthu kiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranangal aayiram iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranangal aayiram iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">nenapula vaalurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenapula vaalurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasukulla solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukulla solla"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru vaartha irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru vaartha irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthum intha kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthum intha kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum mounam pidikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum mounam pidikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjukullara atha vaikama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukullara atha vaikama"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo unnala thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo unnala thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">polambi nan vikuren nikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polambi nan vikuren nikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">ada bakkunu nan vilunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada bakkunu nan vilunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalaiyila pogala polappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyila pogala polappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyila adanguthu thavippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyila adanguthu thavippu"/>
</div>
<div class="lyrico-lyrics-wrapper">maalaiyida theduthu kaluththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalaiyida theduthu kaluththu"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyatha unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyatha unaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en manasa epadi solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasa epadi solla"/>
</div>
<div class="lyrico-lyrics-wrapper">thayakkam vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayakkam vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">thadukuthu mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadukuthu mella"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugatha parkayil ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugatha parkayil ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thairiyam than illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thairiyam than illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">otha nodi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha nodi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pirunja valikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pirunja valikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">motha usuru inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motha usuru inga"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga kedakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga kedakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan kathula saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kathula saayum"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyila kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyila kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thathalichu thathalichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathalichu thathalichu"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkuren sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkuren sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">etti nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">adi bakkunu nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi bakkunu nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jannal thamaraiyai malarntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jannal thamaraiyai malarntha"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathum yenadi sirucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathum yenadi sirucha"/>
</div>
<div class="lyrico-lyrics-wrapper">mothama manasa nee paricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothama manasa nee paricha"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye thaviyai thavikirandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye thaviyai thavikirandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sikkuren sikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkuren sikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna pathu sokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna pathu sokkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">etti nee pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti nee pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">adi bakkunu nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi bakkunu nan paranthen"/>
</div>
</pre>
