---
title: "idhamaai unplugged song lyrics"
album: "Kolaigaran"
artist: "Simon K. King"
lyricist: "Dhamayanthi"
director: "Andrew Louis"
path: "/albums/kolaigaran-song-lyrics"
song: "Idhamaai Unplugged"
image: ../../images/albumart/kolaigaran.jpg
date: 2019-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cmNtcWvlbDM"
type: "melody"
singers:
  - 	Janani S.V. Kapil
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thoduvaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thoduvaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhazhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhazhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Nanaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Nanaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Urugum Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Urugum Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Tholaippaaiyaa Hoo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Tholaippaaiyaa Hoo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai Valaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai Valaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Ilaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Ilaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Uthirpaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Uthirpaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Udhiram Athira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Udhiram Athira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vathaipaaiyaa Ho Ooo Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vathaipaaiyaa Ho Ooo Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valai Oosai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Oosai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vaai Mooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vaai Mooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naanam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naanam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Kondaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kondaaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaaga Naanam En Udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaaga Naanam En Udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorndhu Poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorndhu Poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai Valaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai Valaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Ilaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Ilaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Uthirpaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Uthirpaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Udhiram Athira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Udhiram Athira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vathaipaaiyaa Haaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vathaipaaiyaa Haaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viyarvaigal Paambena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarvaigal Paambena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhuginil Erangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhuginil Erangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seyar Kuri Valikka Theendaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyar Kuri Valikka Theendaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ooo Hooo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo Hooo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaigal Vilagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaigal Vilagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Porkalam Pirakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkalam Pirakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungida Idaiyai Thaaraaiyoo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungida Idaiyai Thaaraaiyoo Oo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perunilaa Idhazhgalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perunilaa Idhazhgalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalinil Neendhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalinil Neendhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piraviyin Payan Athu Koodidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraviyin Payan Athu Koodidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamathai Kaamathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamathai Kaamathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthidum Munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthidum Munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooya Mounam Udainthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooya Mounam Udainthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai Valaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai Valaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Ilaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Ilaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Uthirpaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Uthirpaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Udhiram Athira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Udhiram Athira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vathaipaaiyaa Hoo Oo Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vathaipaaiyaa Hoo Oo Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai Azhaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Azhaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambugal Pulambudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambugal Pulambudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marbinil Marithu Pooven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marbinil Marithu Pooven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Oo Oo Oo Hoo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Oo Oo Oo Hoo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravinai Pirai Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinai Pirai Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Izhukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Izhukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaippinil Norungippovenooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaippinil Norungippovenooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padharidum Udhadugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharidum Udhadugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaridum Engo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaridum Engo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Mana Aadaigal Avizhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Mana Aadaigal Avizhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudugira Varai Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudugira Varai Viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam Kuduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Kuduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Mirugam Yengiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Mirugam Yengiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai Valaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai Valaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Ilaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Ilaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Uthirpaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Uthirpaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Udhiram Athira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Udhiram Athira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vathaipaaiyaa Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vathaipaaiyaa Aaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valai Oosai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Oosai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vaai Mooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vaai Mooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naanam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naanam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Kondaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kondaaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaaga Naanam En Udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaaga Naanam En Udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorndhu Poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorndhu Poguthae"/>
</div>
</pre>
