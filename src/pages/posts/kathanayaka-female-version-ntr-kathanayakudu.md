---
title: "kathanayaka female version song lyrics"
album: "NTR Kathanayakudu"
artist: "M.M. Keeravani"
lyricist: "K. Siva Datta - Dr.K. Rama Krishna"
director: "Krish Jagarlamudi"
path: "/albums/ntr-kathanayakudu-lyrics"
song: "Kathanayaka Female Version"
image: ../../images/albumart/ntr-kathanayakudu.jpg
date: 2019-01-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fW-k19Ky1Sw"
type: "happy"
singers:
  - Sreenidhi Tirumala
  - Ramya Behara
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Treshada kadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Treshada kadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaitrayatrikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaitrayatrikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanaayakaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanaayakaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Treshada kadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Treshada kadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaitrayatrikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaitrayatrikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanaayakaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanaayakaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rarirare Rarirarare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rarirare Rarirarare"/>
</div>
<div class="lyrico-lyrics-wrapper">Rarirare Rarirarareee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rarirare Rarirarareee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aharyangika Vachikapurvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aharyangika Vachikapurvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">AdhbhuthaAhulitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AdhbhuthaAhulitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Natanaa Ghatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natanaa Ghatika"/>
</div>
<div class="lyrico-lyrics-wrapper">BhimasenaVeerajuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BhimasenaVeerajuna "/>
</div>
<div class="lyrico-lyrics-wrapper">krishna Dhanakarna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna Dhanakarna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhanasuyodhana Bhishma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhanasuyodhana Bhishma "/>
</div>
<div class="lyrico-lyrics-wrapper">Bruhannala Vishvamtra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bruhannala Vishvamtra "/>
</div>
<div class="lyrico-lyrics-wrapper">Gangeshwara Dashakanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangeshwara Dashakanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Ravanasuradhipurana Purusha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravanasuradhipurana Purusha "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhumika Roshaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhumika Roshaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakshath Sakshath kaarakaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakshath Sakshath kaarakaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhira Chayachitra Chadhitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhira Chayachitra Chadhitha "/>
</div>
<div class="lyrico-lyrics-wrapper">RanjithaRangitha Chitraya Vanitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="RanjithaRangitha Chitraya Vanitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitrayavanitha Naidham Purvaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitrayavanitha Naidham Purvaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Rasodpadhaka Rasodpadakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasodpadhaka Rasodpadakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">KirthikanyakaManonaayaka Kathanayaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KirthikanyakaManonaayaka Kathanayaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanayaka Kathanayaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanayaka Kathanayaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghana Keerthi Sandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Keerthi Sandra"/>
</div>
<div class="lyrico-lyrics-wrapper">VijithaKilandhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VijithaKilandhra"/>
</div>
<div class="lyrico-lyrics-wrapper">JanathaSudendhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JanathaSudendhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhipakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhipakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Treshada kadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Treshada kadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithramaalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithramaalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaitrayatrikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaitrayatrikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanaayakaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanaayakaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jayaho Jayaho Jayaho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayaho Jayaho Jayaho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayaho Jayaho Jayaho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayaho Jayaho Jayaho"/>
</div>
</pre>
