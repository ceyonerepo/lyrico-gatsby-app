---
title: "ledhamma nyayam song lyrics"
album: "Johaar"
artist: "Priyadarshan - Balasubramanian"
lyricist: "Chaitanya Prasad"
director: "Marni Teja Chowdary"
path: "/albums/johaar-lyrics"
song: "Ledhamma Nyayam"
image: ../../images/albumart/johaar.jpg
date: 2020-08-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uzgKB0r40cs"
type: "sad"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ledhamma Nyayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhamma Nyayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhamma Saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhamma Saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashale Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashale Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thufaanulle Thufanulle Meelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thufaanulle Thufanulle Meelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shokhamulle Shokhamulle Lo Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shokhamulle Shokhamulle Lo Lo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brathakaalante Prathiroju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathakaalante Prathiroju "/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Mahaa Yaathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Mahaa Yaathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupe Leka Gathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupe Leka Gathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Leka Chese Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leka Chese Poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Shodhano Mano Vedhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Shodhano Mano Vedhano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhi Paatamo Maranampai Premo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhi Paatamo Maranampai Premo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desham Enthentho Etthe Edhigindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desham Enthentho Etthe Edhigindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Paathaalam Paalaindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Paathaalam Paalaindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jendaa Undundi Range Maarchindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jendaa Undundi Range Maarchindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundaa Thandaala Goodayyindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundaa Thandaala Goodayyindhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninge Kungenule Nelake Vangenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninge Kungenule Nelake Vangenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Panchabhoothaalave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Panchabhoothaalave "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Pancha Praanaalule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Pancha Praanaalule"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe Inthinthe Inthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe Inthinthe Inthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugisi Pothaayanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugisi Pothaayanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenu Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenu Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommallaa Choosthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommallaa Choosthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brathakaalante Prathiroju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathakaalante Prathiroju "/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Mahaa Yaathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Mahaa Yaathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupe Leka Gathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupe Leka Gathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Leka Chese Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leka Chese Poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Shodhano Mano Vedhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Shodhano Mano Vedhano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhi Paatamo Maranampai Premo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhi Paatamo Maranampai Premo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Maranampai Premo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Maranampai Premo"/>
</div>
</pre>