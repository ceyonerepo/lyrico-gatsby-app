---
title: "boom boom robo da song lyrics"
album: "Enthiran"
artist: "A.R. Rahman"
lyricist: "Madhan Karky"
director: "S. Shankar"
path: "/albums/enthiran-lyrics"
song: "Boom Boom Robo Da"
image: ../../images/albumart/enthiran.jpg
date: 2010-07-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/otB9lYNeR1Y"
type: "Entertainment"
singers:
  - Yogi B
  - Kirthi Sagathia
  - Swetha Mohan
  - Tanvi Shah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isac Asimovin Velaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isac Asimovin Velaiyo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isac Newtonin Leelaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isac Newtonin Leelaiyo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Albert Einsteinin Moolaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Albert Einsteinin Moolaiyo Robo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Robo Ye Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Robo Ye Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Inba Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Inba Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on Lets Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on Lets Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Robo Nee Agrinaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Nee Agrinaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti Nee Uyarnthinaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Nee Uyarnthinaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaaram Udalil Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaaram Udalil Raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naveena Ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naveena Ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariviyal Adhisaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariviyal Adhisaiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayundu Aanaal Vayiru Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayundu Aanaal Vayiru Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechundu Moochu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechundu Moochu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi Undu Irudhayam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Undu Irudhayam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Powerthaan Undu Thimire illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powerthaan Undu Thimire illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Mukki Aggini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Mukki Aggini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vanin Kadhalil Piranthavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vanin Kadhalil Piranthavano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ekkinile Poothavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ekkinile Poothavano"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin Kadhalai Serthavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Kadhalai Serthavano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumana Thirunaal Theriyum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumana Thirunaal Theriyum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Engal Pillaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Engal Pillaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti Chitti Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Chitti Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chutti Chutti Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chutti Chutti Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thotti Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thotti Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pattu Kuttiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pattu Kuttiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitti Chitti Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Chitti Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chutti Chutti Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chutti Chutti Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thotti Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thotti Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pattu Kuttiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pattu Kuttiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti Kutti Buttanil Vaai Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Kutti Buttanil Vaai Moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhali Ithu Pol Kidaiyatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhali Ithu Pol Kidaiyatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Solvathellaam Kettu Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Solvathellaam Kettu Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalan Ithupol Amaiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalan Ithupol Amaiyaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavamindri Varangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavamindri Varangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvathanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvathanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara Kannano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara Kannano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Auto Auto Autokkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto Auto Autokkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Automatic Kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Automatic Kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam Koottam Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam Koottam Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Autographukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Autographukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Auto Autokkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto Autokkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Automatic Kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Automatic Kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam Koottam Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam Koottam Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Autographukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Autographukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isac Asimovin Velaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isac Asimovin Velaiyo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isac Newtonin Leelaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isac Newtonin Leelaiyo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Albert Einsteinin Moolaiyo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Albert Einsteinin Moolaiyo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Robo Ye Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Robo Ye Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Inba Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Inba Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on Lets Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on Lets Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boom Boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom Zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom Zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Da Robo Da Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Da Robo Da Robo Da"/>
</div>
</pre>
