---
title: "ready ready song lyrics"
album: "Devi 2"
artist: "Sam C. S."
lyricist: "Prabhu Deva"
director: "A.L. Vijay"
path: "/albums/devi-2-lyrics"
song: "Ready Ready"
image: ../../images/albumart/devi-2.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Id7hxLSfp5I"
type: "love"
singers:
  - Nincy Vincent
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh My Darling-u Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Darling-u Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Round Round-u Jilebi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Round Round-u Jilebi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Darling-u Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Darling-u Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Round Round-u Jilebi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Round Round-u Jilebi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devi Naan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi Naan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padu Paavi Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padu Paavi Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati Naan Ethukkum Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati Naan Ethukkum Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Vappatti Ethukku Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vappatti Ethukku Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devi Naan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi Naan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padu Paavi Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padu Paavi Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati Naan Ethukkum Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati Naan Ethukkum Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Vappatti Ethukku Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vappatti Ethukku Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready Ready Ready Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready Ready Ready Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Ready Start-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Ready Start-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Bloody Bloody Bloody Bloody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bloody Bloody Bloody Bloody"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Very Hot-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Very Hot-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready Ready Ready Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready Ready Ready Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Ready Start-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Ready Start-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Bloody Bloody Bloody Bloody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bloody Bloody Bloody Bloody"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Very Hot-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Very Hot-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaan Thangam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaan Thangam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagaram Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagaram Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Annam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanthai Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanthai Ethukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjal Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavu Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavu Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkara Pongal Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkara Pongal Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnu Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnu Ethukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiya Odachi Senja Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiya Odachi Senja Intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Selaiya Paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Selaiya Paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela Mela Irukkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Mela Irukkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiya Konjam Paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiya Konjam Paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna Krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna Krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna Krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna Krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">My Dear Krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Dear Krishna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready Ready Ready Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready Ready Ready Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Ready Start-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Ready Start-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Bloody Bloody Bloody Bloody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bloody Bloody Bloody Bloody"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Very Hot-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Very Hot-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready Ready Ready Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready Ready Ready Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Ready Start-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Ready Start-u"/>
</div>
</pre>
