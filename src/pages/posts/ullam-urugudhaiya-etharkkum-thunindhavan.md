---
title: "ullam urugudhaiya song lyrics"
album: "Etharkkum Thunindhavan"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/etharkkum-thunindhavan-lyrics"
song: "Ullam Urugudhaiya"
image: ../../images/albumart/etharkkum-thunindhavan.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ubIJEDUfE68"
type: "love"
singers:
  - Pradeep Kumar
  - Vandana Srinivasan
  - Brinda Manickavasakan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagaa azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna uthu uthu paakkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna uthu uthu paakkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konji konji pesaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konji konji pesaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinna maangani naan tharavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna maangani naan tharavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnai pechena maaridavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnai pechena maaridavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkolum nee idavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkolum nee idavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyyil naan unai yenthidavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyil naan unai yenthidavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam ondralla rendalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam ondralla rendalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru thara oru nannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru thara oru nannaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaal unnaal vilaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaal unnaal vilaiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Onn uthu uthu paakkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn uthu uthu paakkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konji konji pesaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konji konji pesaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavan veesum payale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavan veesum payale"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan manadhodu maraithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan manadhodu maraithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallanthu kidappathuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallanthu kidappathuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalodu poriyai enai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalodu poriyai enai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu pisainthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu pisainthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppothum rusippathuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppothum rusippathuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi thalai mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi thalai mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivarai yenai izhuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivarai yenai izhuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham pathithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham pathithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Munaivathum yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munaivathum yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachai avizhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachai avizhndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Arupathu kalaigalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arupathu kalaigalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Katru koduthida nirainthidum poomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru koduthida nirainthidum poomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalithogaiyai iruppen naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalithogaiyai iruppen naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaimaane karam seradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaimaane karam seradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanga kadalenum sanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga kadalenum sanga "/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhil moozhgadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhil moozhgadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna uthu uthu paakkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna uthu uthu paakkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konji konji pesaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konji konji pesaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinna maangani naan tharavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna maangani naan tharavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnai pechena maaridavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnai pechena maaridavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkolum nee idavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkolum nee idavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyyil naan unai yenthidavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyil naan unai yenthidavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam ondralla rendalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam ondralla rendalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru thara oru nannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru thara oru nannaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaal unnaal vilaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaal unnaal vilaiyume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam urugudhaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam urugudhaiyya"/>
</div>
</pre>
