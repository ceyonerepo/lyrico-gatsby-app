---
title: "maane unna song lyrics"
album: "Rajavamsam"
artist: "Sam C.S"
lyricist: "Vadivelu"
director: "K.V. Kathirvelu"
path: "/albums/rajavamsam-lyrics"
song: "Maane Unna"
image: ../../images/albumart/rajavamsam.jpg
date: 2021-11-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/t6kmbzBIlRU"
type: "happy"
singers:
  - Sam CS
  - Anthakudi Ilayaraja
  - Sathish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maane unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maane unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maanae unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maanae unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi aathae aathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aathae aathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi maasa kaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi maasa kaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta malakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta malakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En cell number paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cell number paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee switch off pannum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee switch off pannum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna therinjikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna therinjikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum nalla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum nalla thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maanae unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maanae unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maanae unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maanae unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi aathae aathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aathae aathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi maasa kaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi maasa kaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta malakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta malakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En cell number paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cell number paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee switch off pannum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee switch off pannum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna therinjikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna therinjikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum nalla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum nalla thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapaan pappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapaan pappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapaan pappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapaan pappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapaan pappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapaan pappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan jolly-ana aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan jolly-ana aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo keliyaanen paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo keliyaanen paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha paazha pona paavi maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha paazha pona paavi maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saettai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saettai da"/>
</div>
<div class="lyrico-lyrics-wrapper">En siricha moonji pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En siricha moonji pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo kaduppu romba aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo kaduppu romba aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nadippukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nadippukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukuraada tholladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukuraada tholladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarukumae yaarum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarukumae yaarum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadikirathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadikirathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thaana poi ukkanthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thaana poi ukkanthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan periya thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan periya thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadiye ponaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiye ponaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini enna aagum maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna aagum maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada innoru glass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada innoru glass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothikudi sariyaagum game-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothikudi sariyaagum game-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanae unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru valaya naanum virichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru valaya naanum virichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ennanamo ninaichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ennanamo ninaichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa viricha valaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa viricha valaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthu putten naanu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthu putten naanu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada sirippula naan kavunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sirippula naan kavunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pecha nambi kedanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pecha nambi kedanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi sooniyatha vachuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi sooniyatha vachuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthaigala theduromae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigala theduromae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga unna thitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga unna thitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kailasa vaa poitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kailasa vaa poitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum nithyanandha kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum nithyanandha kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudaiya mariyathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya mariyathaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae thaandi koracha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae thaandi koracha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada soodu patta kaayathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada soodu patta kaayathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorikkathi nozhacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorikkathi nozhacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maane unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maane unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maanae unna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maanae unna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni naanae vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni naanae vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw-ah thanni pottu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw-ah thanni pottu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara pappara paaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara pappara paaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara pappara paaen"/>
</div>
</pre>
