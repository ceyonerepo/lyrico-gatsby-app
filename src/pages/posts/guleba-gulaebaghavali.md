---
title: "guleba song lyrics"
album: "Gulaebaghavali"
artist: "Vivek – Mervin"
lyricist: "Ku. Karthik"
director: "Kalyaan"
path: "/albums/gulaebaghavali-lyrics"
song: "Guleba"
image: ../../images/albumart/gulaebaghavali.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QVgSyvzmbxw"
type: "mass"
singers:
  - Anirudh Ravichander
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<div class="lyrico-lyrics-wrapper">Buenos dias
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buenos dias"/>
</div>
<div class="lyrico-lyrics-wrapper">Arriba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arriba"/>
</div>
<div class="lyrico-lyrics-wrapper">Que pasa ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Que pasa ha ha ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<div class="lyrico-lyrics-wrapper">Jio beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jio beta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ringota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringota"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey jolly lilo gimu kana thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jolly lilo gimu kana thama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama panama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama panama"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi party pannalam mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyi party pannalam mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey scene-ah oru gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey scene-ah oru gana padalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada allu nightu rawsu thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada allu nightu rawsu thama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Blockbuster song-u ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blockbuster song-u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi pottu vaangu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi pottu vaangu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Area dhan thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area dhan thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly lilo gimu kana thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly lilo gimu kana thama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey jolly lilo gimu kana thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jolly lilo gimu kana thama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama panama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama panama"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi party pannalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyi party pannalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey scene-ah oru gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey scene-ah oru gana padalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada allu nightu rawsu thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada allu nightu rawsu thama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahh ahh ahh ahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahh ahh ahh ahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahh ahh ahh ahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahh ahh ahh ahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaaOrama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaaOrama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mamacita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mamacita"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamacita lolita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamacita lolita"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa sandailaama vaazhalaama smartah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa sandailaama vaazhalaama smartah"/>
</div>
<div class="lyrico-lyrics-wrapper">Committaah ne angerukku kaatu oru tata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committaah ne angerukku kaatu oru tata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repeatah nee love ah konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeatah nee love ah konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shareu pannu neat ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shareu pannu neat ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suweeta vaa areakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suweeta vaa areakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan meetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan meetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Googleu googleu googleu panniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Googleu googleu googleu panniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkadhu en peru ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkadhu en peru ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle gapula rocketa thookura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle gapula rocketa thookura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hide and seeku aalu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hide and seeku aalu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubbleu bubbleu bubbleu bubbleu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubbleu bubbleu bubbleu bubbleu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubble gum-u body ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubble gum-u body ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jollu lilo gimu kana thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jollu lilo gimu kana thama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<div class="lyrico-lyrics-wrapper">Guleba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guleba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey jolly lilo gimu kana thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jolly lilo gimu kana thama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama panama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama panama"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyi party pannalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyi party pannalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey scene-ah oru gana padalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey scene-ah oru gana padalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada allu nightu rawsu thama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada allu nightu rawsu thama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh vikkalu vikkalu vikkalu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh vikkalu vikkalu vikkalu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kudichikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kudichikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalu sikkalu sikkalunaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalu sikkalu sikkalunaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orama othikoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama othikoma"/>
</div>
</pre>
