---
title: "chowkidar song lyrics"
album: "Gurkha"
artist: "Raj Aryan"
lyricist: "Arunraja Kamaraj"
director: "Sam Anton"
path: "/albums/gurkha-lyrics"
song: "Chowkidar"
image: ../../images/albumart/gurkha.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fgQw0e4BfCY"
type: "happy"
singers:
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rajaa Veshathil Chowkidhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Veshathil Chowkidhaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesaa Paarthaalae Paththikkuvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesaa Paarthaalae Paththikkuvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaja Podaatha Pant-ah Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaja Podaatha Pant-ah Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Rojaa Nenjukkul Kuthikkuvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rojaa Nenjukkul Kuthikkuvaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Ambaani Moonjellaam Adangungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Ambaani Moonjellaam Adangungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Bhagadhooru Baabunna Padhungungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Bhagadhooru Baabunna Padhungungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Bommailaam Odhungungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Bommailaam Odhungungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Gaandaana Mirugam Thaan Thoora Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gaandaana Mirugam Thaan Thoora Podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Am Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Back"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivandhaan Rombha Polladhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivandhaan Rombha Polladhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonam Basha Theriyaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonam Basha Theriyaadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Vambukkum Pogaathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Vambukkum Pogaathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Oor Anbathaan Thattadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oor Anbathaan Thattadhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Pathala Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Pathala Pathala Pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Pathala Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Pathala Pathala Pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Jaa Aaa Aajaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Jaa Aaa Aajaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaa Jaa Jaa Thuujaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaa Jaa Jaa Thuujaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaa Gau Kyaa Sangat Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaa Gau Kyaa Sangat Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Thum Kyaa Nafrath Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Thum Kyaa Nafrath Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Apraani Moonjellam Adangungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Apraani Moonjellam Adangungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Bhagadhooru Baabunna Padhungungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Bhagadhooru Baabunna Padhungungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Bommailaam Odhungungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Bommailaam Odhungungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Gaandaana Mirugam Thaan Thoora Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gaandaana Mirugam Thaan Thoora Podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivandhaan Rombha Polladhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivandhaan Rombha Polladhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonam Basha Theriyaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonam Basha Theriyaadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Vambukkum Pogaathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Vambukkum Pogaathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Oor Anbathaan Thattadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oor Anbathaan Thattadhavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Pathala Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Pathala Pathala Pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala Pathala Pathala Pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Pathala Pathala Pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Pathala Pathala Pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Gurkhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Gurkhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Gurkhaa"/>
</div>
</pre>
