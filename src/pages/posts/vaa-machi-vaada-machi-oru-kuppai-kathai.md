---
title: "vaa machi vaada machi song lyrics"
album: "Oru Kuppai Kathai"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Kaali Rangasamy"
path: "/albums/oru-kuppai-kathai-lyrics"
song: "Vaa Machi Vaada Machi"
image: ../../images/albumart/oru-kuppai-kathai.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T6sczOmFNRQ"
type: "gana"
singers:
  - Velumurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tottown tottown tottown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tottown tottown tottown"/>
</div>
<div class="lyrico-lyrics-wrapper">Tottudan towntu tottown tottown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tottudan towntu tottown tottown"/>
</div>
<div class="lyrico-lyrics-wrapper">Tottown tottown tottown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tottown tottown tottown"/>
</div>
<div class="lyrico-lyrics-wrapper">Tottudan towntu tottown tottown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tottudan towntu tottown tottown"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa machi vaada machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa machi vaada machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama time-u start achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama time-u start achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaaga enna paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaaga enna paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu onnu vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu onnu vandhaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppaikku mela vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaikku mela vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja poovum poothaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja poovum poothaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppathula kolam poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathula kolam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiyum vandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiyum vandhachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhu bondhu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu bondhu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava paarva vandhu modhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava paarva vandhu modhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji konji pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konji pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kolla poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kolla poraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En koora mela yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En koora mela yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava selai thuniyum kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava selai thuniyum kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo devi theatre ticket-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo devi theatre ticket-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangunu solla poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangunu solla poraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakketha jodi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakketha jodi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thank you soldren kandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thank you soldren kandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kooti poven avala kishkindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kooti poven avala kishkindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada beer-u ketta nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada beer-u ketta nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan treat-u vekkiren indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan treat-u vekkiren indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaanga matta ennoda bandhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini thaanga matta ennoda bandhaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa machi vaada machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa machi vaada machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama time-u start achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama time-u start achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei anbaaga enna paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei anbaaga enna paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu onnu vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu onnu vandhaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppaikku mela vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaikku mela vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja poovum poothaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja poovum poothaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppathula kolam poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathula kolam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiyum vandhachu eiieii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiyum vandhachu eiieii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey seth-tu kadai halwa-va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey seth-tu kadai halwa-va"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu senja kannamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu senja kannamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha podhae verthuten daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha podhae verthuten daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru poottu yedhum illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru poottu yedhum illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavi kooda podaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi kooda podaama"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja ava thorandhutaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja ava thorandhutaa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey cheri paarthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey cheri paarthathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha city paartha thilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha city paartha thilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava thanga neram pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava thanga neram pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkeedae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkeedae illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaasu panam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaasu panam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aanaa enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aanaa enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhavaippen queen-ah poladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhavaippen queen-ah poladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakketha jodi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakketha jodi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thank you soldren kandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thank you soldren kandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kooti poven avala kishkindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kooti poven avala kishkindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada beer-u ketta nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada beer-u ketta nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan treat-u vekkiren indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan treat-u vekkiren indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaanga matta ennoda bandhaaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini thaanga matta ennoda bandhaaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaa aaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaa aaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey koovam aathu pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey koovam aathu pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppa kosu naathathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppa kosu naathathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Senta pola poosippen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senta pola poosippen daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thanni lorry koottathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thanni lorry koottathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundi modhi nikkaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundi modhi nikkaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna pola paarthuppen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna pola paarthuppen daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha beach-u karaiyoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha beach-u karaiyoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam kaathu vaanga povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam kaathu vaanga povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava pechu kural ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pechu kural ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu podhum podhummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu podhum podhummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava serthu vacha aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava serthu vacha aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha alli alli veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha alli alli veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhavaippen queen-ah poladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhavaippen queen-ah poladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakketha jodi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakketha jodi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thank you soldren kandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thank you soldren kandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kooti poven avala kishkindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kooti poven avala kishkindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada beer-u ketta nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada beer-u ketta nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan treat-u vekkiren indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan treat-u vekkiren indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaanga matta ennoda bandhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini thaanga matta ennoda bandhaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakketha jodi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakketha jodi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thank you soldren kandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thank you soldren kandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kooti poven avala kishkindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kooti poven avala kishkindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada beer-u ketta nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada beer-u ketta nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan treat-u vekkiren indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan treat-u vekkiren indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini thaanga matta ennoda bandhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini thaanga matta ennoda bandhaaa"/>
</div>
</pre>
