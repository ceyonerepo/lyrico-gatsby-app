---
title: "oda mudiyadhu song lyrics"
album: "Monster"
artist: "Justin Prabhakaran"
lyricist: "Sankardaas"
director: "Nelson Venkatesan"
path: "/albums/monster-lyrics"
song: "Oda Mudiyadhu"
image: ../../images/albumart/monster.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Bz4h6pF5x08"
type: "mass"
singers:
  - S.J. Surya
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You Think You Can Catch Me Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Think You Can Catch Me Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Hide Even In Thoon Thurumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Hide Even In Thoon Thurumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goiyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliya Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliya Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pudichu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pudichu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adippen Nachunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adippen Nachunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliya Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliya Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pudichu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pudichu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adippen Nachunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adippen Nachunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaalu Pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaalu Pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Thara-nnu Izhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Thara-nnu Izhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya Pichu Kaadha Thirugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya Pichu Kaadha Thirugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Katti Kanna Nondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Katti Kanna Nondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odadha Odadha Odadha Odadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odadha Odadha Odadha Odadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliya Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m A Monster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m A Monster"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Gangster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Gangster"/>
</div>
<div class="lyrico-lyrics-wrapper">You Think You Can Catch Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Think You Can Catch Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Funny Guy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funny Guy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaduppethuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduppethuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Veruppethuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Veruppethuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apdiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjom Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjom Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Risku Eduthu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Risku Eduthu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ruska Edukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ruska Edukkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aang"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu En Rusku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu En Rusku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Askku Buskku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Askku Buskku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vara Vara Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Vara Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbu Thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbu Thaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thona Thona Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thona Thona Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Edukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Edukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara Kara Kara Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara Kara Kara Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha Arukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Arukkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arukkuriye Arukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arukkuriye Arukkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arukkuriye Arukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arukkuriye Arukkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Vandha Sethan Sekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandha Sethan Sekar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vandha Sethan Sekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandha Sethan Sekar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vandha Sethan Sekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandha Sethan Sekar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vandha Sethan Sekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandha Sethan Sekar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Podaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podaangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odapporane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odapporane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyapporene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyapporene"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ippo Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ippo Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikka Mudiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikka Mudiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odapporane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odapporane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyapporane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyapporane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ippo Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ippo Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikka Mudiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikka Mudiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaalu Pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaalu Pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Tharannu Izhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Tharannu Izhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya Pichu Kaadha Thirugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya Pichu Kaadha Thirugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Katti Kanna Nondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Katti Kanna Nondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odadha Odadha Odadha Odadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odadha Odadha Odadha Odadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Better Luck Next Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Better Luck Next Time"/>
</div>
</pre>
