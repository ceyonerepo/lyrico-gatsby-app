---
title: "athuva athuva naduvasal song lyrics"
album: "Nadodigal 2"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-2-lyrics"
song: "Athuva Athuva Naduvasal"
image: ../../images/albumart/nadodigal-2.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e0Tqa_STY7M"
type: "Love"
singers:
  - Sooraj Santhosh
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enakku Oru Thadava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Oru Thadava "/>
</div>
<div class="lyrico-lyrics-wrapper">Thonuchu Eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonuchu Eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athuva Athuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuva Athuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvasal Koothula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvasal Koothula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Paricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Paricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thoga Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thoga Pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukum Thunichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukum Thunichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Pora Pokkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Pora Pokkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathum Unaipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathum Unaipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Aazha Pakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aazha Pakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkum Oru Thadava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum Oru Thadava "/>
</div>
<div class="lyrico-lyrics-wrapper">Thonuchu Eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonuchu Eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athuva Athuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuva Athuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalu Roatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalu Roatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mariyal Mudinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mariyal Mudinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Una Naanum Veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Naanum Veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidavum Erandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidavum Erandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyoda Veechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyoda Veechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethaiyo Tholachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyo Tholachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyama Aambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyama Aambala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engirundhu Vanthadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirundhu Vanthadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Thozhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thozhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Ingu Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Ingu Yaarum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Thozhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Thozhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhu Kaadhal Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhu Kaadhal Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugathai Kaatudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathai Kaatudhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ondru Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ondru Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Malarai Neetudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Malarai Neetudho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhaiyoo Nee Pesa Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyoo Nee Pesa Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Kekka Nadaiye Maariduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Kekka Nadaiye Maariduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maname Kaana Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maname Kaana Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga Usire Ooriduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga Usire Ooriduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum Un Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Un Nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooralai Thoovuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralai Thoovuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyillum Neeyena Kadhai Pesuthe…he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyillum Neeyena Kadhai Pesuthe…he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanoli Yaavilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanoli Yaavilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kural Kekkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kural Kekkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Neramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Neramum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Sera Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Sera Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuvaa Medhuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaa Medhuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhanchaaye Moochila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhanchaaye Moochila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhancha Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhancha Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraichaaiye Kaichala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koraichaaiye Kaichala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthum Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthum Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Mela Neechala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Mela Neechala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkum Una Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkum Una Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichene Paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichene Paatula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engirundhu Vanthadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirundhu Vanthadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Thozhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thozhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Ingu Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Ingu Yaarum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Thozhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Thozhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhu Kaadhal Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhu Kaadhal Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugathai Kaatudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathai Kaatudhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ondru Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ondru Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Malarai Neetudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Malarai Neetudhoo"/>
</div>
</pre>
