---
title: "freedom song lyrics"
album: "Cheppina Evaru Nammaru"
artist: "Jagdeesh Vemula"
lyricist: "Bhaskarabhatla"
director: "Aaryan Krishna"
path: "/albums/cheppina-evaru-nammaru-lyrics"
song: "Freedom Freedom"
image: ../../images/albumart/cheppina-evaru-nammaru.jpg
date: 2021-01-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Cf7uTF8hIV0"
type: "happy"
singers:
  - Ashwin Iyer
  - Aditya Tadepalli 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey"/>
</div>
<div class="lyrico-lyrics-wrapper">what can i do for you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what can i do for you"/>
</div>
<div class="lyrico-lyrics-wrapper">aai adagadu evvadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aai adagadu evvadu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve ra neeku thoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve ra neeku thoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey"/>
</div>
<div class="lyrico-lyrics-wrapper">gundello ni bharuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundello ni bharuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">evadochi dhirchadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evadochi dhirchadu"/>
</div>
<div class="lyrico-lyrics-wrapper">evadatha khali leedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evadatha khali leedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nilo frustration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilo frustration"/>
</div>
<div class="lyrico-lyrics-wrapper">nidey dhariki solution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidey dhariki solution"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvey apudapudu ilagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvey apudapudu ilagey"/>
</div>
<div class="lyrico-lyrics-wrapper">chalorey chaloorey chaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalorey chaloorey chaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">freedom freedom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="freedom freedom "/>
</div>
<div class="lyrico-lyrics-wrapper">freedom freedom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="freedom freedom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallu therichi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu therichi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamentha baagundhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamentha baagundhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">ouu marivhipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ouu marivhipo"/>
</div>
<div class="lyrico-lyrics-wrapper">andhamina dhaarulloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhamina dhaarulloo"/>
</div>
<div class="lyrico-lyrics-wrapper">columbus ravara enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="columbus ravara enti"/>
</div>
<div class="lyrico-lyrics-wrapper">kotha dheevi kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotha dheevi kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">arthaku mindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arthaku mindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">arudhina dheevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arudhina dheevi"/>
</div>
<div class="lyrico-lyrics-wrapper">choodham google map
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choodham google map"/>
</div>
<div class="lyrico-lyrics-wrapper">choopinchaledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choopinchaledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">swargamkurna dwaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swargamkurna dwaram"/>
</div>
<div class="lyrico-lyrics-wrapper">edhokiga ethicesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhokiga ethicesi"/>
</div>
<div class="lyrico-lyrics-wrapper">pattedhamm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattedhamm"/>
</div>
<div class="lyrico-lyrics-wrapper">eee gali rekkaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eee gali rekkaley"/>
</div>
<div class="lyrico-lyrics-wrapper">sayam thisukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sayam thisukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">meghala chettupi kurchndham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meghala chettupi kurchndham"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gunde dhositto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gunde dhositto"/>
</div>
<div class="lyrico-lyrics-wrapper">haayi nimpukundham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haayi nimpukundham"/>
</div>
<div class="lyrico-lyrics-wrapper">aanandham ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanandham ki "/>
</div>
<div class="lyrico-lyrics-wrapper">inko ardham untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inko ardham untey"/>
</div>
<div class="lyrico-lyrics-wrapper">mamamey andhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamamey andhaam"/>
</div>
</pre>
