---
title: "waka waka song lyrics"
album: "Gaddalakonda Ganesh"
artist: "Mickey J. Meyer"
lyricist: "Chandrabose"
director: "Harish Shankar"
path: "/albums/gaddalakonda-ganesh-lyrics"
song: "Waka Waka"
image: ../../images/albumart/gaddalakonda-ganesh.jpg
date: 2019-09-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/INuLC2IVAEA"
type: "mass"
singers:
  - Anurag Kulkarni
  - Mickey J Meyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhada Dhada Dhada Dhanchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Dhada Dhanchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Pidi Dhinchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Pidi Dhinchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam Ochhinodni Sampude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam Ochhinodni Sampude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhu Paddhulanni Simpude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhu Paddhulanni Simpude"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthaloni Kallu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthaloni Kallu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaguthunte Ekkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaguthunte Ekkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Seesaloni Saaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seesaloni Saaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Laaguthunte Ekkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaguthunte Ekkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudumbainaa Baaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudumbainaa Baaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunjuthunte Ekkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunjuthunte Ekkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvannaina Guddhithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvannaina Guddhithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Kikkenaaku Ekkuddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kikkenaaku Ekkuddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeloni Vanuke Chicken Tikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeloni Vanuke Chicken Tikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Praanam Ney Peelche Hukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Praanam Ney Peelche Hukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundela Sochhi Guchhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundela Sochhi Guchhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayame Nene Ekki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayame Nene Ekki "/>
</div>
<div class="lyrico-lyrics-wrapper">Koosunde Kursi Leraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosunde Kursi Leraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting Antene Comedy Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting Antene Comedy Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paanaale Yentika Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paanaale Yentika Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Naaku Dhandam Pedathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Naaku Dhandam Pedathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevuni Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevuni Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasko Pakkaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasko Pakkaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada Dhada Dhada Dhanchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Dhada Dhanchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Pidi Dhinchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Pidi Dhinchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam Ochhinodni Sampude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam Ochhinodni Sampude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhu Paddhulanni Simpude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhu Paddhulanni Simpude"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada Dhada Dhada Dhanchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Dhada Dhanchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Pidi Dhinchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Pidi Dhinchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam Ochhinodni Sampude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam Ochhinodni Sampude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhu Paddhulanni Simpude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhu Paddhulanni Simpude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemro Vintunnaavraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemro Vintunnaavraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Eeda Kaadhu Bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Eeda Kaadhu Bidda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundela Meenne Undi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundela Meenne Undi "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Adda Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Adda Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sachhaa Ledhu Jootaa Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sachhaa Ledhu Jootaa Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nen Seppindhe Maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen Seppindhe Maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Aage Ledhu Peeche Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage Ledhu Peeche Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nen Nadisindhe Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen Nadisindhe Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotaa Ledhu Motaa Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotaa Ledhu Motaa Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nen Pelchindhe Thootaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen Pelchindhe Thootaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeena Marna Lene Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena Marna Lene Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhagi Anthaa Vetaa Vetaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhagi Anthaa Vetaa Vetaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kochhaa Kochhaa Meesam Thoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kochhaa Kochhaa Meesam Thoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuri Teesesi Oopiri Aapesthav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuri Teesesi Oopiri Aapesthav"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Vasthe Savaanni Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Vasthe Savaanni Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Baitiki Teesi Mallaa Sampesthaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baitiki Teesi Mallaa Sampesthaav"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeloni Vanuke Chicken Tikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeloni Vanuke Chicken Tikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Praanam Ney Peelche Hukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Praanam Ney Peelche Hukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gundela Sochhi Guchhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gundela Sochhi Guchhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayame Nene Ekki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayame Nene Ekki "/>
</div>
<div class="lyrico-lyrics-wrapper">Koosunde Kursi Leraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosunde Kursi Leraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting Antene Comedy Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting Antene Comedy Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paanaale Yentika Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paanaale Yentika Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakka Wakka Wakka Wakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakka Wakka Wakka Wakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Naaku Dhandam Pedathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Naaku Dhandam Pedathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevuni Lekkaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevuni Lekkaaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada Dhada Dhada Dhanchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Dhada Dhanchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Pidi Dhinchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Pidi Dhinchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam Ochhinodni Sampude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam Ochhinodni Sampude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhu Paddhulanni Simpude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhu Paddhulanni Simpude"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada Dhada Dhada Dhanchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Dhada Dhanchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelloki Pidi Dhinchude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelloki Pidi Dhinchude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addam Ochhinodni Sampude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addam Ochhinodni Sampude"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhu Paddhulanni Simpude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhu Paddhulanni Simpude"/>
</div>
</pre>
