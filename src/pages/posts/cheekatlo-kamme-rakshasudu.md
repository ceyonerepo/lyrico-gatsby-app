---
title: "cheekatlo kamme song lyrics"
album: "Rakshasudu"
artist: "Ghibran"
lyricist: "Rakendu Mouli"
director: "Ramesh Varma"
path: "/albums/rakshasudu-lyrics"
song: "Cheekatlo Kamme"
image: ../../images/albumart/rakshasudu.jpg
date: 2019-08-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X4_1yAZeOQA"
type: "happy"
singers:
  - Shabir
  - Ghibran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheekatle Kamme Vela Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatle Kamme Vela Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pramadham Paisachikanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pramadham Paisachikanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham Pondhuthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham Pondhuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gola Dhurmargam Adharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gola Dhurmargam Adharmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathai Mundhukocchadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathai Mundhukocchadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Assal Sissal Asurudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assal Sissal Asurudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oopire Ummadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Ummadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithadu Chaavuku Parichayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithadu Chaavuku Parichayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayamunu Vijrumbinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayamunu Vijrumbinchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidwamsame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidwamsame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamo Narakam Chuputhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamo Narakam Chuputhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Thisesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Thisesthade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charmanni Olichesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charmanni Olichesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullasam Ullasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullasam Ullasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naralu Chitluthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naralu Chitluthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Utsaham Utsaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utsaham Utsaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemukalni Virichesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemukalni Virichesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham Aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham Aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Akrandhanalu Vinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akrandhanalu Vinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahladham Ahladham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahladham Ahladham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakasila Aatma Harinchuku Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakasila Aatma Harinchuku Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakthanni Ruchiga Jurreseinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakthanni Ruchiga Jurreseinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhammunte Pattukora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammunte Pattukora "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannapa Levu Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannapa Levu Lera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthunna Vasthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vasthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthunna Vasthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vasthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seranam Antunna Vadhaladika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seranam Antunna Vadhaladika"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Ventadu Ninne Tappanisari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Ventadu Ninne Tappanisari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Oppu Teliyadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Oppu Teliyadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Japincham Yerugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japincham Yerugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishamule Thanu Anuvanuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishamule Thanu Anuvanuvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Kanikaramu Ledhule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kanikaramu Ledhule "/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna Chupadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna Chupadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Pradheya Paduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pradheya Paduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Proyajanam Undadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Proyajanam Undadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali Dhaya Ravvanthaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali Dhaya Ravvanthaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Lene Leni Vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lene Leni Vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanisam Manava Jathi Ki Chendhade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanisam Manava Jathi Ki Chendhade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gathame Tharime Prathi Kshaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathame Tharime Prathi Kshaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaragani Guruthulu Kasirepi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaragani Guruthulu Kasirepi"/>
</div>
<div class="lyrico-lyrics-wrapper">Melukundhi Mrugame Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukundhi Mrugame Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">G0llanni Peekuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="G0llanni Peekuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Maayo Maayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Maayo Maayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillalni Cut Chesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillalni Cut Chesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Haayo Em Haayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Haayo Em Haayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalni Kosasthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalni Kosasthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Haayo Em Haayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Haayo Em Haayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradhaaga Champesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradhaaga Champesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Haayo Em Haayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Haayo Em Haayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakasila Aatma Harinchuku Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakasila Aatma Harinchuku Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakthanni Ruchiga Jurreseinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakthanni Ruchiga Jurreseinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhammunte Pattukora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammunte Pattukora "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannapa Levu Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannapa Levu Lera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthunna Vasthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vasthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthunna Vasthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthunna Vasthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuham Ghoham Himsa Suradhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuham Ghoham Himsa Suradhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Nene Rakshasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Nene Rakshasudu"/>
</div>
</pre>
