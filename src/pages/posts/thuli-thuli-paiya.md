---
title: 'thuli thuli song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'Thuli Thuli Thuli'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
lang: tamil
singers: 
- Haricharan
- Tanvi
youtubeLink: "https://www.youtube.com/embed/v-hL3sks2qI"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thuli thuli thuli mazhayaai vandhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli thuli thuli mazhayaai vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda suda suda maraindhae ponaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Suda suda suda maraindhae ponaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthal parka thondrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthal parka thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai ketka thondrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Perai ketka thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopol sirikum podhu kaatrai parandhida thondrum .
<input type="checkbox" class="lyrico-select-lyric-line" value="Poopol sirikum podhu kaatrai parandhida thondrum ."/>
</div>
<div class="lyrico-lyrics-wrapper">Sel sel avalidam sel endrae kalgal solludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sel sel avalidam sel endrae kalgal solludhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol avalidam sol endrae nenjam kolludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sol sol avalidam sol endrae nenjam kolludhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagai manadhai parithuvitaalae …
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagai manadhai parithuvitaalae …"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuli thuli thuli mazhayaai vandhalae..ehhhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli thuli thuli mazhayaai vandhalae..ehhhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda suda suda maraindhae ponaalae..ehhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Suda suda suda maraindhae ponaalae..ehhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Devadhai aval oru devadhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Devadhai aval oru devadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya poomugam kanavae aayul thaan podhumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagiya poomugam kanavae aayul thaan podhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrilae avaladhu vaasanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Katrilae avaladhu vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalidam yosannai ketuthan pookalum pookumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Avalidam yosannai ketuthan pookalum pookumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri mela otraimudi aadum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Netri mela otraimudi aadum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulale minnal pookum parvai aalai thookum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukulale minnal pookum parvai aalai thookum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanam parthal muthangalal theenda thondrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanam parthal muthangalal theenda thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padham rendu parkum podhu golusai marathondrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Padham rendu parkum podhu golusai marathondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Azhagai manadhai parithuvitaalae …
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagai manadhai parithuvitaalae …"/>
</div>
<div class="lyrico-lyrics-wrapper">Sel sel avalidam sel endrae kalgal solludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sel sel avalidam sel endrae kalgal solludhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol avalidam sol endrae nenjam kolludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sol sol avalidam sol endrae nenjam kolludhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Salayil azhagiya mazhayil
<input type="checkbox" class="lyrico-select-lyric-line" value="Salayil azhagiya mazhayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaludan pogavae yenguven thozgalil saayuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Avaludan pogavae yenguven thozgalil saayuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil vizhugira velayil
<input type="checkbox" class="lyrico-select-lyric-line" value="Boomiyil vizhugira velayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalayum odipoi yendhuven nenjilae thanguven
<input type="checkbox" class="lyrico-select-lyric-line" value="Nizhalayum odipoi yendhuven nenjilae thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanum podhae kanaal yennai katti potal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanum podhae kanaal yennai katti potal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayamindri vetti potal uyirai edho seidhal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kayamindri vetti potal uyirai edho seidhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamaga ullukulae pesum podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mounamaga ullukulae pesum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae vandhu ottu ketal kanvil koochal potal
<input type="checkbox" class="lyrico-select-lyric-line" value="Angae vandhu ottu ketal kanvil koochal potal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Azhagai manadhai parithuvitaalae …
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagai manadhai parithuvitaalae …"/>
</div>
<div class="lyrico-lyrics-wrapper">Sel sel avalidam sel endrae kalgal solludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sel sel avalidam sel endrae kalgal solludhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol avalidam sol endrae nenjam kolludhada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sol sol avalidam sol endrae nenjam kolludhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuli thuli thuli mazhayaai vandhalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli thuli thuli mazhayaai vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda suda suda maraindhae ponaalae (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Suda suda suda maraindhae ponaalae"/></div>
</div>
</pre>