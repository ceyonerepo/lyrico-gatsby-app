---
title: "heli song lyrics"
album: "Sebastian PC 524"
artist: "Ghibran"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Balaji Sayyapureddy"
path: "/albums/sebastian-pc-524-lyrics"
song: "Heli"
image: ../../images/albumart/sebastian-pc-524.jpg
date: 2022-03-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HyEjuDMRXIc"
type: "happy"
singers:
  - Kapil Kapilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Maata Vinte Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maata Vinte Raadha Maimarape"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Peru Ante Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Peru Ante Raadha Maimarape"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Evaro Evaro Telisindhi Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Evaro Evaro Telisindhi Nee Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Lenu Nenu Khaali Kaadhule Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Lenu Nenu Khaali Kaadhule Kalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kalale Madhilo Medhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalale Madhilo Medhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Valane Sarada Modalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Valane Sarada Modalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maata Vinte Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maata Vinte Raadha Maimarape"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Peru Ante Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Peru Ante Raadha Maimarape"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Prapancham Heli Ane Naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prapancham Heli Ane Naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalemouthondho Chuttu Teliyadhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalemouthondho Chuttu Teliyadhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Chiraake Undhi Paraarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Chiraake Undhi Paraarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tega Sambarapaduthu Untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tega Sambarapaduthu Untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Kanabadagaa Kadhulanugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Kanabadagaa Kadhulanugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhalanugaa Vadhalanugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhalanugaa Vadhalanugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkadidhe Ee Velugantha Naa Kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadidhe Ee Velugantha Naa Kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaalalaa Merise Nee Navvulandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaalalaa Merise Nee Navvulandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppatiki Nanu Bathikinche Oopirive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppatiki Nanu Bathikinche Oopirive"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalaadadhe Kshanamaina Nuvvu Lenidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalaadadhe Kshanamaina Nuvvu Lenidhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvochhaakane Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvochhaakane Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevitham Ante Telisindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevitham Ante Telisindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avastha Unna Sare Naa Paniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avastha Unna Sare Naa Paniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhyatha Perigindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhyatha Perigindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Levaa Nenasalu Emouthaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Levaa Nenasalu Emouthaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithamu Emouno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithamu Emouno"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudaithe Neetho Untaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudaithe Neetho Untaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandadigaa Untaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandadigaa Untaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamu Telupanaa Ledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamu Telupanaa Ledhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Manasulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Manasulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapadu Adugu Adugunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapadu Adugu Adugunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudurugaa Manasu Nilavadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudurugaa Manasu Nilavadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidividine Madagadadhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidividine Madagadadhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadivadigaa Mudipadadhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadivadigaa Mudipadadhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa, Mudipadadhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa, Mudipadadhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidividine Madagadadhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidividine Madagadadhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maata Vinte Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maata Vinte Raadha Maimarape"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Peru Ante Raadha Maimarape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Peru Ante Raadha Maimarape"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Evaro Evaro Telisindhi Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Evaro Evaro Telisindhi Nee Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Lenu Nenu Khaali Kaadhule Kallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Lenu Nenu Khaali Kaadhule Kallaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kalale Madhilo Medhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalale Madhilo Medhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Valane Saradaa Modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Valane Saradaa Modhalu"/>
</div>
</pre>
