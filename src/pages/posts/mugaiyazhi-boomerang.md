---
title: "mughaiyazhi song lyrics"
album: "Boomerang"
artist: "Radhan"
lyricist: "Vivek"
director: "R. Kannan"
path: "/albums/boomerang-lyrics"
song: "Mughaiyazhi"
image: ../../images/albumart/boomerang.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2uE0I1ExusM"
type: "love"
singers:
  - Anand Aravindakshan
  - Radhika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mughaiyazhi Pennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mughaiyazhi Pennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaadi Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaadi Pogindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalodu Nizhalaai Selgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalodu Nizhalaai Selgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaaram Sollaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram Sollaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodineram Undaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodineram Undaakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhileri Kaadhal Solgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhileri Kaadhal Solgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaipaarthaal Anil Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaipaarthaal Anil Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhaiyaada Manal Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaiyaada Manal Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamey Ariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamey Ariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paadhi Vaalibam Kadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paadhi Vaalibam Kadanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhazhin Mazhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazhin Mazhaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Paavam Yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Paavam Yaavaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mughaiyazhi Pennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mughaiyazhi Pennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaadi Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaadi Pogindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalodu Nizhalaai Selgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalodu Nizhalaai Selgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Uraiyaadum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Uraiyaadum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endrey Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endrey Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetil Unnai Bommaiaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetil Unnai Bommaiaakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaigalkorkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaigalkorkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum Un Moochilithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Un Moochilithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vaazhapaarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhapaarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kondaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kondaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sol Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sol Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhimoodi Vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhimoodi Vizhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaadhey Unthan Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhey Unthan Nyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum Un Kaal Thadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Un Kaal Thadangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvondraai Erinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondraai Erinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Ovvondrin Meedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Ovvondrin Meedhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nimidam Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nimidam Vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyaai En Paer Udhirthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaai En Paer Udhirthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondaadi Theerkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadi Theerkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerai Un Thol Kodhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerai Un Thol Kodhikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandraadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandraadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Moodi Vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Moodi Vizhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaadhey Unthan Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhey Unthan Nyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaiye"/>
</div>
</pre>
