---
title: "yeppa mama mama treatu song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Viveka"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Yeppa Mama Mama Treatu"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qFYcMSQcOvQ"
type: "Love"
singers:
  - A. V. Pooja
  - D. Imman
  - Snigdha Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maama Maama Yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Maama Yeppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama Maama Yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Maama Yeppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Enna Marakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Enna Marakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vinnil Parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vinnil Parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Treatu Dhaane Routu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Treatu Dhaane Routu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa Maama Maama Treat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Maama Maama Treat"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Moodu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Moodu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Password Unakku Tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Password Unakku Tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Ellam Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Ellam Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Oda Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Oda Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovume Hotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovume Hotu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikilla Gate tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikilla Gate tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangikodi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangikodi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sachinu Na Batu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sachinu Na Batu"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaki Na Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaki Na Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivaji Na Actu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivaji Na Actu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Naale Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Naale Treatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inoru Dhaaba Innoru Innoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inoru Dhaaba Innoru Innoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Suspense Yethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Suspense Yethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Quick Ah Sollu Enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quick Ah Sollu Enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Increase Aachu Heartu Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Increase Aachu Heartu Beat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Maama Maama Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Maama Maama Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Simple Hugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Simple Hugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Single Kissula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Single Kissula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Oda Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Oda Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovume Hotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovume Hotu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikilla Gate tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikilla Gate tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangikodi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangikodi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sachinu Na Batu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sachinu Na Batu"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaki Na Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaki Na Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivaji Na Actu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivaji Na Actu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Naale Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Naale Treatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru Dhabba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Dhabba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get Up And Aadu Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get Up And Aadu Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Vera Maari Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Vera Maari Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Freeya Vai Date
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Freeya Vai Date"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nondi Ketaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nondi Ketaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulla Solla Maatendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulla Solla Maatendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman Romba Romba Smartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Romba Romba Smartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vena Paaru Maiyya Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Paaru Maiyya Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Oda Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Oda Treatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovume Hotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovume Hotu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikilla Gate tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikilla Gate tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangikodi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangikodi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sachinu Na Batu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sachinu Na Batu"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaki Na Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaki Na Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivaji Na Actu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivaji Na Actu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Naale Treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Naale Treatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru Dhaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Dhaba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru Innoru Innoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Innoru Innoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Maama Treattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Maama Treattu"/>
</div>
</pre>
