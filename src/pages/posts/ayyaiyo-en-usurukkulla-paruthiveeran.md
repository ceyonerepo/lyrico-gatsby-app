---
title: "ayyaiyo en usurukkulla song lyrics"
album: "Paruthiveeran"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Ameer"
path: "/albums/paruthiveeran-lyrics"
song: "Ayyaiyo En Usurukkulla"
image: ../../images/albumart/paruthiveeran.jpg
date: 2007-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zKBY8iffWl0"
type: "Love"
singers:
  - Krishna Raj
  - Manika Vinayagam
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeleyyy yeleleleyyy yeleyyy yeleleleyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleyyy yeleleleyyy yeleyyy yeleleleyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha pana marathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha pana marathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethaneram un madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethaneram un madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala vachi saanjikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala vachi saanjikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathiya sollithaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathiya sollithaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu kannu paalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu kannu paalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maechalukku kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maechalukku kaathiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaichaloda vaadi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaichaloda vaadi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koocham keecham thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koocham keecham thevaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nee vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeleyyy yeleleleyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleyyy yeleleleyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeleyyy yeleleleyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleyyy yeleleleyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvelani chinna kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvelani chinna kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna seraiyedukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna seraiyedukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaiyyo en usurukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaiyyo en usurukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya vachan ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya vachan ayyaiyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Noiya thachan ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noiya thachan ayyaiyyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaali un paasathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaali un paasathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanum sundeliyaa aanen pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanum sundeliyaa aanen pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee konnaa kooda kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konnaa kooda kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonnaa saagum indha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonnaa saagum indha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaiyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En vetkam pathee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vetkam pathee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegurathae ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegurathae ayyaiyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En samanja dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En samanja dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaiyirathae ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaiyirathae ayyaiyyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arali vedha vaasakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arali vedha vaasakkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala kollum paasakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala kollum paasakkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">En udambu nenja keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udambu nenja keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ulla vandha getti kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ulla vandha getti kaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaiyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En iduppu vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En iduppu vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi pochae ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi pochae ayyaiyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En meesa murukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meesa murukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madangi pochae ayyaiyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madangi pochae ayyaiyyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallukulla therai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukulla therai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanjirukkum thaadi kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanjirukkum thaadi kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Olinjikka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinjikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala suthum nizhala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala suthum nizhala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta kaatil unkoodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil unkoodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyanaara paathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyanaara paathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammi kallu poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammi kallu poo pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari pochu yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari pochu yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaadaa malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaadaa malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podaa alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podaa alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoratti kannu karuvaachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoratti kannu karuvaachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thotta aruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thotta aruvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumbaaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumbaaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoratti kannu karuvaachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoratti kannu karuvaachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thotta aruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thotta aruvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumbaaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumbaaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaali un paasathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaali un paasathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanum sundeliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanum sundeliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee konnaa kooda kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konnaa kooda kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonnaa saagum indhapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonnaa saagum indhapulla"/>
</div>
</pre>
