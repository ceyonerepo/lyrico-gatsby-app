---
title: "capmaari anthem song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "logan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Capmaari Anthem"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DFRNb-7Ootg"
type: "anthem"
singers:
  - MC Vicky
  - Syed Abu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Mothanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Mothanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kenjunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kenjunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagulla Kuthanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagulla Kuthanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu Veedhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Veedhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naekka Vettanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naekka Vettanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Police Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Thallanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Thallanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Maattikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasama Vandhu Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasama Vandhu Maattikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Maattikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasama Vasama Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasama Vasama Maattikittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu Kitta Sikkikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Kitta Sikkikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinna Pinna Aagipu Sang-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinna Pinna Aagipu Sang-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae Oodhi Thattivuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae Oodhi Thattivuttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Orae Kallula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Orae Kallula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Maanga Adikka Paathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Maanga Adikka Paathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Miss-aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Miss-aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri Maari Vazhi Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Maari Vazhi Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu Beeru Mela Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Beeru Mela Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama Andha Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Andha Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Maari Molla Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Maari Molla Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Cap Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Cap Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Capmaari Hot-ah Adippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Hot-ah Adippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Sight-um Adippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Sight-um Adippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Maattikittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Maattikittan"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Capmaari Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Capmaari Capmaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedi Kedi Kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedi Kedi Kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Pakka Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Pakka Capmaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Paathen Kottankuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Paathen Kottankuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae Kannae Kammachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Kannae Kammachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mild-ula Mayakkiputta Meenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mild-ula Mayakkiputta Meenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka Aiyoo Pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka Aiyoo Pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhula Sindhu Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhula Sindhu Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyoo Edhuku Imaa Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Edhuku Imaa Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make It One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make It One"/>
</div>
<div class="lyrico-lyrics-wrapper">Take It One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take It One"/>
</div>
<div class="lyrico-lyrics-wrapper">Simple One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simple One"/>
</div>
<div class="lyrico-lyrics-wrapper">One By Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One By Two"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venamaa Aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venamaa Aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhumae Flop-u Aagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhumae Flop-u Aagadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saniyana Baniyan-la Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saniyana Baniyan-la Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Oda Vidudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Oda Vidudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanae Thaenae Neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae Thaenae Neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu Malli Kuduthu Kavuthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu Malli Kuduthu Kavuthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Rendu Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Rendu Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo Bodhai Ulla Pona Vaathiyaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Bodhai Ulla Pona Vaathiyaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyoo Rendu Figure Aala Aanaaru Apettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Rendu Figure Aala Aanaaru Apettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyoo Viratti Vittaaru Sila Sila Pala Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Viratti Vittaaru Sila Sila Pala Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyoo Ivan Capmaari Thalaiyila Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyoo Ivan Capmaari Thalaiyila Kottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Mothanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Mothanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kenjunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kenjunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagulla Kuthanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagulla Kuthanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu Veedhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Veedhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naekka Vettanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naekka Vettanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Police Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Thallanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Thallanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Novama Nungu Thinna Comali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novama Nungu Thinna Comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaan Da Arivula Yemaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaan Da Arivula Yemaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bemani Bejaarana Somaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bemani Bejaarana Somaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Mollamari Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mollamari Capmaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Kadhai Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kadhai Yemaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Plan Panni Sila Pala Bitt-ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Sila Pala Bitt-ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Vachu Kalutha Arutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Vachu Kalutha Arutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mollamari Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mollamari Capmaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Doi Doi Doi Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doi Doi Doi Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kaluvura Meenula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kaluvura Meenula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naluvura Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naluvura Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonjila Kaiya Vai Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonjila Kaiya Vai Vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Doi Doi Doi Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doi Doi Doi Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Tyre-ukku Adiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Tyre-ukku Adiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkuna Lemon-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuna Lemon-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Pochu Maama Life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Pochu Maama Life-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikittaan Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikittaan Maattikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikittaan Maattikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikittaan Maattikittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Gap-u Kedachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Gap-u Kedachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Cap-ah Maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Cap-ah Maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Colour-ah Maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Colour-ah Maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Angattum Ingattum Aattam Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angattum Ingattum Aattam Kaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Thaan Namba Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaan Namba Capmaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhava Thora Kaathu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhava Thora Kaathu Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyani Orthan Sonnan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyani Orthan Sonnan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kadhava Thorandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kadhava Thorandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyani Ippo Bemaani Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyani Ippo Bemaani Aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Capmaari Aana Deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Capmaari Aana Deii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Capmaari Ivan Fraud-u Mangan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Ivan Fraud-u Mangan"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Ivan Lucha Payan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Ivan Lucha Payan"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Thadi Adi Vaanga Poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Thadi Adi Vaanga Poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Capmaari Capmaari Capmaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Capmaari Capmaari Capmaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Mothanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Mothanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adei Edu Patta Payale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adei Edu Patta Payale"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kenjunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kenjunaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bagulla Kuthanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagulla Kuthanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Serupala Aduchi Oda Oda Verattanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serupala Aduchi Oda Oda Verattanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu Veedhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Veedhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naekka Vettanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naekka Vettanum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Police La Pudichi Kudukka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police La Pudichi Kudukka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Police Ivana Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Ivana Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Thallanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Thallanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porukki Rascal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukki Rascal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadi Adi Deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Adi Deii"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadi Adi Deiia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Adi Deiia"/>
</div>
</pre>
