---
title: "adiye ivale song lyrics"
album: "Azhagarsamiyin Kuthirai"
artist: "Ilaiyaraaja"
lyricist: "Snehan"
director: "Suseenthiran"
path: "/albums/azhagarsamiyin-kuthirai-lyrics"
song: "Adiye Ivale"
image: ../../images/albumart/azhagarsamiyin-kuthirai.jpg
date: 2011-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lgK8twKclag"
type: "happy"
singers:
  - Thanjai Selvi
  - Snehan
  - Lenin Bharathi
  - Hemambika
  - Murugan
  - Iyyappan
  - Master Regan
  - Senthil Das
  - Anita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adiye ivale oorukulla thiruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye ivale oorukulla thiruvizhavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye ivale oorukulla thiruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye ivale oorukulla thiruvizhavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagar saamiku indha maasam theruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagar saamiku indha maasam theruvizhavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye ivale oorukulla thiruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye ivale oorukulla thiruvizhavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagar saamiku indha maasam theruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagar saamiku indha maasam theruvizhavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ettu pattikum kaappathaana katti puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu pattikum kaappathaana katti puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thambattatha aduthu aduthu saati puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thambattatha aduthu aduthu saati puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu pattikum kaappathaana katti puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu pattikum kaappathaana katti puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thambattatha aduthu aduthu saati puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thambattatha aduthu aduthu saati puttanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maavilayum thoranamum katti puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maavilayum thoranamum katti puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">othumaia kodithaana yethi puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othumaia kodithaana yethi puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">maavilayum thoranamum katti puttanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maavilayum thoranamum katti puttanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ada othumaia kodithaana yethi puttainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada othumaia kodithaana yethi puttainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye ivale adiye ivale oorukulla thiruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye ivale adiye ivale oorukulla thiruvizhavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagar saamiku indha maasam theruvizhavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagar saamiku indha maasam theruvizhavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi kaattukulla koyila katti saamia vachanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kaattukulla koyila katti saamia vachanga"/>
</div>
<div class="lyrico-lyrics-wrapper">adha othayila nikka vachi vedika paathaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha othayila nikka vachi vedika paathaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi kaattukulla koyila katti saamia vachanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kaattukulla koyila katti saamia vachanga"/>
</div>
<div class="lyrico-lyrics-wrapper">adha othayila nikka vachi vedika paathaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha othayila nikka vachi vedika paathaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vela vetti venum'unu vaendikuvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela vetti venum'unu vaendikuvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">vevagaaram panniputta velakku vepaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevagaaram panniputta velakku vepaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vela vetti venum'unu vaendikuvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela vetti venum'unu vaendikuvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">vevagaaram panniputta velakku vepaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevagaaram panniputta velakku vepaainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada saami irundha moonu varusham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada saami irundha moonu varusham "/>
</div>
<div class="lyrico-lyrics-wrapper">yenda nikum thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda nikum thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ahhn evan kelviya paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ahhn evan kelviya paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaya adichi pattaya potta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaya adichi pattaya potta "/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi nadakkum thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi nadakkum thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangu sakkara vediya vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangu sakkara vediya vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara pongal ponga vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara pongal ponga vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">sangu sakkara saamiku katti pangu vachikuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangu sakkara saamiku katti pangu vachikuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenda kangu thiruvizhavum thodangiyachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda kangu thiruvizhavum thodangiyachi"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiku erunadhum thaanaga erangi pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiku erunadhum thaanaga erangi pochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koyil kaala pola nikkum minor ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyil kaala pola nikkum minor ah paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">koyil kaala pola nikkum minor ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyil kaala pola nikkum minor ah paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada vaala thookkum kaalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vaala thookkum kaalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">pola collar ah thookuraru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola collar ah thookuraru"/>
</div>
<div class="lyrico-lyrics-wrapper">ada vaala thookkum kaalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vaala thookkum kaalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">pola collar ah thookuraru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola collar ah thookuraru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannadipaar paathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadipaar paathuko"/>
</div>
<div class="lyrico-lyrics-wrapper">hey chikkan eh chikan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey chikkan eh chikan "/>
</div>
<div class="lyrico-lyrics-wrapper">eh chikan chikan chikan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh chikan chikan chikan"/>
</div>
<div class="lyrico-lyrics-wrapper">kai asaippaar paathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai asaippaar paathuko"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnalathaan povoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnalathaan povoma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyum kalavuma pudipoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyum kalavuma pudipoma"/>
</div>
<div class="lyrico-lyrics-wrapper">ada yenda namaku indha ooru vambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada yenda namaku indha ooru vambu"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukulla thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukulla thiruvizha"/>
</div>
</pre>
