---
title: "para para song lyrics"
album: "Ettuthikkum Para"
artist: "M. S. Sreekanth"
lyricist: "Saavee"
director: "Keera"
path: "/albums/ettuthikkum-para-song-lyrics"
song: "Para Para Para"
image: ../../images/albumart/ettuthikkum-para.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cRO6PW_zPUU"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">adichu noruku para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichu noruku para"/>
</div>
<div class="lyrico-lyrics-wrapper">achamindri para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achamindri para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu katti para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu katti para"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga maruthu para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga maruthu para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">asai patta kadhal vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asai patta kadhal vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">andha kadhal kooda vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha kadhal kooda vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">aanavathil kolai nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanavathil kolai nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyathai aduchu putu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyathai aduchu putu"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalai nimirnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai nimirnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhanaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhanaga "/>
</div>
<div class="lyrico-lyrics-wrapper">para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai nimirnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai nimirnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thamizhanaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thamizhanaga "/>
</div>
<div class="lyrico-lyrics-wrapper">para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yethamillai kaadumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethamillai kaadumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu katti para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu katti para"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga maruthu para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga maruthu para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sathi ingu irupathala vaazhura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathi ingu irupathala vaazhura "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyil nimathi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyil nimathi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi pera solli solliputu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi pera solli solliputu"/>
</div>
<div class="lyrico-lyrics-wrapper">sandayala romba tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandayala romba tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeyum naanum serthu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum serthu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum vera onnum theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum vera onnum theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sathi matham pathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathi matham pathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai ethayum kodupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai ethayum kodupathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeyum naanum kondadathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum kondadathane"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam boomi undanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam boomi undanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">valum bothu undana theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valum bothu undana theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">katrum neerum rendanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrum neerum rendanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey sontham bantham enna sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey sontham bantham enna sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu pathu enna seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu pathu enna seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thimiri elunthu ninnu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimiri elunthu ninnu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">thana varum kana thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana varum kana thanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu noruku para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu noruku para para"/>
</div>
<div class="lyrico-lyrics-wrapper">achamindri para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achamindri para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu katti para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu katti para"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga maruthu para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga maruthu para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sathi ingu irupathala vaazhura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathi ingu irupathala vaazhura "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyil nimathi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyil nimathi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi pera solli solliputu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi pera solli solliputu"/>
</div>
<div class="lyrico-lyrics-wrapper">sandayala romba tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandayala romba tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum naanum serthu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum serthu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum vera onnum theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum vera onnum theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sathi matham pathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathi matham pathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai ethayum kodupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai ethayum kodupathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mutta mutta kannala thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutta mutta kannala thane"/>
</div>
<div class="lyrico-lyrics-wrapper">morachu paakuthu ponnu onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morachu paakuthu ponnu onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">kitta poi sangathi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta poi sangathi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">sirichu poguthu kanna thinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichu poguthu kanna thinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">eh vada macha kuthu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh vada macha kuthu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">valthu sollu paatu padu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valthu sollu paatu padu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalichu kai puducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalichu kai puducha"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala ellam thana marum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala ellam thana marum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu noruku para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu noruku para para"/>
</div>
<div class="lyrico-lyrics-wrapper">achamindri para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achamindri para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu katti para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu katti para"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga maruthu para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga maruthu para"/>
</div>
</pre>
