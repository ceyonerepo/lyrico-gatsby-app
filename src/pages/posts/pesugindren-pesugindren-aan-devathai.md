---
title: "pesugindren pesugindren song lyrics"
album: "Aan Devathai"
artist: "Ghibran"
lyricist: "Karthick Netha"
director: "Thamira"
path: "/albums/aan-devathai-lyrics"
song: "Pesugindren Pesugindren"
image: ../../images/albumart/aan-devathai.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zeMynnXVKq0"
type: "happy"
singers:
  - Chaitra Ambadipudi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesugindren pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugindren pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechai thaandi vandhu pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechai thaandi vandhu pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Osaiyellam vesham endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osaiyellam vesham endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhae ullam pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhae ullam pesugindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nerathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nerathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nermai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nermai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorathil kaankindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathil kaankindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathin bramaandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathin bramaandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiaagi vizhum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiaagi vizhum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaai maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugindren pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugindren pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechai thaandi vandhu pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechai thaandi vandhu pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhae ullam pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhae ullam pesugindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanakena piditha ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakena piditha ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Salanathil thavara vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salanathil thavara vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palangkadhai pulidhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palangkadhai pulidhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indha moodhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen indha moodhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irupathai rasithu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupathai rasithu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaipathail uyarvu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaipathail uyarvu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhchiyil thilaithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhchiyil thilaithirunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhae kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhae kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu vaazhthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu vaazhthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalin neer aatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalin neer aatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaanum ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanum ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyae serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyae serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatadha kaatrukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatadha kaatrukku"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavugal thoondil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavugal thoondil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum yen intha komaali thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum yen intha komaali thedal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada muyalaamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada muyalaamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaiyaachu ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaiyaachu ulagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Salanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhumai kuda maranamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhumai kuda maranamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugindren pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugindren pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechai thaandi vandhu pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechai thaandi vandhu pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Osaiyellam vesham endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osaiyellam vesham endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhae ullam pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhae ullam pesugindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nerathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nerathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkindra nermai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkindra nermai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorathil kaankindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathil kaankindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Megathin bramaandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathin bramaandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiaagi vizhum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiaagi vizhum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaai maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesugindren pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugindren pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechai thaandi vandhu pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechai thaandi vandhu pesugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirundhae ullam pesugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirundhae ullam pesugindren"/>
</div>
</pre>
