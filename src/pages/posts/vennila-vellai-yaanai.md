---
title: "vennila song lyrics"
album: "Vellai Yaanai"
artist: "Santhosh Narayanan"
lyricist: "Uma Devi"
director: "Subramaniam Siva"
path: "/albums/vellai-yaanai-lyrics"
song: "Vennila"
image: ../../images/albumart/vellai-yaanai.jpg
date: 2021-07-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EjQHpUNH3T8"
type: "happy"
singers:
  - Vijaynarain
  - Sangeetha Karuppiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thanthana Thanthana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana Thanthana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Thandhana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Thandhana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana Thanthana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana Thanthana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Thandhana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Thandhana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana Thanthana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana Thanthana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Thandhana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Thandhana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Thandhanana Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Thandhanana Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Thandhanana Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Thandhanana Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Thandhanana Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Thandhanana Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhanana Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhanana Hoi Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallukulla Oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukulla Oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vellam Pola Nee Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vellam Pola Nee Aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannin Paavaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannin Paavaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerikara Ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerikara Ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerikkara Ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerikkara Ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ninna Podhum Yaandherae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ninna Podhum Yaandherae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Mothamaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Mothamaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullam Saayudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullam Saayudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennila Vinnula Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Vinnula Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn Nila Mannula Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Nila Mannula Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollula Kandhagam Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollula Kandhagam Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaama Thaan Konna Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaama Thaan Konna Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkatha Adakki Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkatha Adakki Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillunu Thavikka Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillunu Thavikka Vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamma Rasikka Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamma Rasikka Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendaadidum Vandaaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendaadidum Vandaaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethu Vacha Naathuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Vacha Naathuuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hoo Hoo Hoo Hooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hoo Hoo Hoo Hooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethu Vacha Naathu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Vacha Naathu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula Thaan Poothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula Thaan Poothaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli Kolusu Pol Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Kolusu Pol Aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Manasum Serndhaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Manasum Serndhaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Vazhi Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Vazhi Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kooda Vaaren Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kooda Vaaren Maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinju Mozhiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinju Mozhiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji Konji Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Konji Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nellum Neerum Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nellum Neerum Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Pasi Aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Pasi Aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju Viralaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Viralaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Thottu Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Thottu Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli Pani Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Pani Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Vazhi Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Vazhi Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ullam Ippo Ponni Nellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullam Ippo Ponni Nellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaguthadi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaguthadi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannu Ennai Kattu Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannu Ennai Kattu Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthadi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthadi Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennila Vinnula Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Vinnula Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn Nila Mannula Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Nila Mannula Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollula Kandhagam Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollula Kandhagam Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaama Thaan Konna Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaama Thaan Konna Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkatha Adakki Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkatha Adakki Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillunu Thavikka Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillunu Thavikka Vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamma Rasikka Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamma Rasikka Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendaadidum Vandaaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendaadidum Vandaaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Kulam Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Kulam Serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Kulamdhaan Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Kulamdhaan Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathu Thanni Neethaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathu Thanni Neethaanyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaahoooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaahoooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Kuthichu Neeroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Kuthichu Neeroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Meenum Serndhaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Meenum Serndhaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Meena Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Meena Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kooda Vaaren Maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kooda Vaaren Maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththum Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththum Kadal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Unnai Thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Unnai Thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha Sondham Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha Sondham Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitham Uravaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitham Uravaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi Malai Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi Malai Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pugazhum Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pugazhum Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thodum Kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thodum Kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Uyir Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Uyir Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Nenjam Ippo Pandhal Vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Nenjam Ippo Pandhal Vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanuthadi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanuthadi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Pillai Nadhi Anbil Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Pillai Nadhi Anbil Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Serudhaiyaahoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serudhaiyaahoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennila Vinnula Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Vinnula Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn Nila Mannula Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Nila Mannula Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollula Kandhagam Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollula Kandhagam Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaama Thaan Konna Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaama Thaan Konna Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkatha Adakki Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkatha Adakki Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillunu Thavikka Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillunu Thavikka Vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamma Rasikka Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamma Rasikka Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendaadidum Vandaaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendaadidum Vandaaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
</pre>
