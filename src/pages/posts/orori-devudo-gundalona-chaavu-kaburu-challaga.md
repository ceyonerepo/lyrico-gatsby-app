---
title: "orori devudo song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Karunakar Adigarla"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "Orori Devudo - Gundelonaa Savvadundhe"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/S5Dzg8fWyGo"
type: "sad"
singers:
 - Anirudh Suswaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gundelonaa savvadundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelonaa savvadundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulonaa praanamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulonaa praanamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Guvvalonaa savvadundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guvvalonaa savvadundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulonaa praanamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulonaa praanamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri maathram unnapalamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri maathram unnapalamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothunnattundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothunnattundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkiri bikkiri chese baadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkiri bikkiri chese baadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttumuttindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttumuttindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orori devudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori devudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni sittharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni sittharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthaavu nee raathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthaavu nee raathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sethi bommalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sethi bommalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nelameeda memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nelameeda memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaali enni aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaali enni aatalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orori devudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori devudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni sittharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni sittharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthaavu nee raathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthaavu nee raathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sethi bommalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sethi bommalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nelameeda memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nelameeda memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaali enni aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaali enni aatalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raayi rappalni theesukochhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raayi rappalni theesukochhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gullo devatha setthaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullo devatha setthaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raktha maamsaalu maaku posi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raktha maamsaalu maaku posi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattipaalu kammantaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattipaalu kammantaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaa aali bandhaalichhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaa aali bandhaalichhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthalone thenchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthalone thenchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamlona yedhi ledhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamlona yedhi ledhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venta theesukupothaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venta theesukupothaavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orori devudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori devudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni sittharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni sittharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthaavu nee raathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthaavu nee raathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sethi bommalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sethi bommalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nelameeda memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nelameeda memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaali enni aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaali enni aatalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orori devudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orori devudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni sittharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni sittharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthaavu nee raathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthaavu nee raathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sethi bommalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sethi bommalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nelameeda memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nelameeda memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaali enni aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaali enni aatalo"/>
</div>
</pre>
