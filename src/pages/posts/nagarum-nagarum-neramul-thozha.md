---
title: "nagarum nagarum nera mul song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Nagarum Nagarum Nera Mul"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7x4V-N9JyQg"
type: "Motivational"
singers:
  - Shanker Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nagarum Nagarum Nera Mul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum Nagarum Nera Mul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namaiyum Nagara Solludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaiyum Nagara Solludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamo Pinne Selludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo Pinne Selludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen Idhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen Idhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarum Uravum Unmaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarum Uravum Unmaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigazhum Pirivum Unmaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhum Pirivum Unmaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhin Ganamum Unmaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhin Ganamum Unmaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Yen Adhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Yen Adhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Vandhaai Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vandhaai Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ponaal Na Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponaal Na Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nakarum Nakarum Naera Mul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakarum Nakarum Naera Mul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namayum Nagara Solludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namayum Nagara Solludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamo Pinne Selludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo Pinne Selludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Yen Ithu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Yen Ithu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena Oru Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Oru Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunthathai Marakirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthathai Marakirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarndhadhum Thirakirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarndhadhum Thirakirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Nenje Yen Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenje Yen Nenje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Oru Vali Thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Oru Vali Thadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaithida Thudikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithida Thudikkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaipathai Marukirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaipathai Marukirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Nenje Yen Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenje Yen Nenje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen Nanbane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen Nanbane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarum Nagarum Nerra Mul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum Nagarum Nerra Mul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namayum Nagara Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namayum Nagara Solluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamo Pinne Selluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo Pinne Selluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yaen Idhu Yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yaen Idhu Yaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasappugal Inippathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasappugal Inippathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udainthathai Inaipathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthathai Inaipathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruthathai Anaipathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthathai Anaipathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Nenje Yen Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenje Yen Nenje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivilae Vasipathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivilae Vasipathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil Pasipathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Pasipathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil Thavippathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil Thavippathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Nenje Yen Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenje Yen Nenje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen Nanbane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen Nanbane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarum Uravum Unmaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarum Uravum Unmaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigazhum Pirivum Unmaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhum Pirivum Unmaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhin Ganamum Unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhin Ganamum Unmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Yen Idhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yen Idhu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Vanthaai Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vanthaai Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ponaal Na Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponaal Na Yaaro"/>
</div>
</pre>
