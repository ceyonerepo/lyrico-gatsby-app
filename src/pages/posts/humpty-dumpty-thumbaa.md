---
title: "humpty dumpty song lyrics"
album: "Thumbaa"
artist: "Santhosh Dhayanidhi"
lyricist: "Ku. Karthik"
director: "Harish Ram L.H."
path: "/albums/thumbaa-lyrics"
song: "Humpty Dumpty"
image: ../../images/albumart/thumbaa.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jcFVfw5e93Y"
type: "happy"
singers:
  - Sivakarthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Humpty Dumpty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humpty Dumpty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Aadu Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aadu Kabaddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inky Pinky Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Pinky Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podalam Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podalam Vaa Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hide And Seekula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hide And Seekula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Pudi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pudi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monkey Tailula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkey Tailula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattalaam Tholi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalaam Tholi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Paakkura Yaanai Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Paakkura Yaanai Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polaam Kaattula Yaanai Ambari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polaam Kaattula Yaanai Ambari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutty Anilukku Looka Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Anilukku Looka Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Sernthu Nee Suththu Savari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu Nee Suththu Savari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulli Maanukku Kolam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Maanukku Kolam Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makeup Podalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makeup Podalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Raajvukku Hair Style
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Raajvukku Hair Style"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maathi Paakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathi Paakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichchu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichchu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuru Kuru Kichchu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru Kuru Kichchu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakkura Kokku Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkura Kokku Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poduthu Kummalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduthu Kummalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pechu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala Pechu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Pechu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkira Kaadellam Kekkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira Kaadellam Kekkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Monkey Un Torture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Monkey Un Torture"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Thaangama Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thaangama Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coolaana Tiger Gandaavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coolaana Tiger Gandaavathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Birds Ellaam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Birds Ellaam Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Dance Aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Dance Aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Sernthu Friend Aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Sernthu Friend Aanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulla Narikku Complain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla Narikku Complain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gift Pannalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gift Pannalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bison Brotherukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bison Brotherukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facepack Pottu Paakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facepack Pottu Paakkalaam"/>
</div>
</pre>
