---
title: "mission to the sydney song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Hari - Harris Jayaraj"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Mission to the Sydney"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TetT4RIr2B4"
type: "Mission"
singers:
  - Lady Kash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ena gana gana smack up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena gana gana smack up"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini nava nava match up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nava nava match up"/>
</div>
<div class="lyrico-lyrics-wrapper">Eva thava thava trap up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eva thava thava trap up"/>
</div>
<div class="lyrico-lyrics-wrapper">Ema gama gama got up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ema gama gama got up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roaring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roaring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Daring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Soaring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soaring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocking lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocking lion"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wanna wanna waanna higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanna wanna waanna higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna gonna gonna fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna gonna gonna fire"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta gotta gotta near
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gotta gotta gotta near"/>
</div>
<div class="lyrico-lyrics-wrapper">Never never never fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never never never fear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sydney mission
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sydney mission"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Own decision
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Own decision"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Has the vision
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Has the vision"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">For the nation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For the nation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal thaandi vanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thaandi vanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattraaga sendraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattraaga sendraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gana neram nindraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gana neram nindraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirmaai velvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirmaai velvaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena gana gana roaring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena gana gana roaring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini nava nava daring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nava nava daring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Eva thava thava soaring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eva thava thava soaring lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Ema gama gama rocking lion ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ema gama gama rocking lion ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re re re reloading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re re re reloading"/>
</div>
<div class="lyrico-lyrics-wrapper">Re re reloading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re re reloading"/>
</div>
<div class="lyrico-lyrics-wrapper">Big badder boss boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big badder boss boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet on a zoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet on a zoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Flew in to sydney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flew in to sydney"/>
</div>
<div class="lyrico-lyrics-wrapper">A new direction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A new direction"/>
</div>
<div class="lyrico-lyrics-wrapper">Got it all locked down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got it all locked down"/>
</div>
<div class="lyrico-lyrics-wrapper">None to question
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="None to question"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Didgeridoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Didgeridoo"/>
</div>
<div class="lyrico-lyrics-wrapper">If you know who
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you know who"/>
</div>
<div class="lyrico-lyrics-wrapper">He could do it solo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He could do it solo"/>
</div>
<div class="lyrico-lyrics-wrapper">What it takes two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What it takes two"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna leave a trail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna leave a trail"/>
</div>
<div class="lyrico-lyrics-wrapper">In new south wales
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In new south wales"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaroo kicks this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaroo kicks this"/>
</div>
<div class="lyrico-lyrics-wrapper">Man is so sleek
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man is so sleek"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragha dhaga dhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragha dhaga dhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragha dha dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragha dha dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragha dhaga dhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragha dhaga dhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragha dha dha ((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragha dha dha ((4) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Roaring lion daring lion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roaring lion daring lion"/>
</div>
</pre>
