---
title: "ra ra ra song lyrics"
album: "Dora"
artist: "Vivek–Mervin"
lyricist: "Ku Karthik"
director: "Doss Ramasamy"
path: "/albums/dora-lyrics"
song: "Ra Ra Ra"
image: ../../images/albumart/dora.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YeAsCUK-KQw"
type: "mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkavae vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkavae vaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi vaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Speed-u kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speed-u kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooda vida poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooda vida poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi vaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Speed-u kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speed-u kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooda vida poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooda vida poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kaadhu munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaadhu munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa paaru kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa paaru kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kanna light-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kanna light-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Night-a konjam bright-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night-a konjam bright-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kanna light-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kanna light-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Night-a konjam bright-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night-a konjam bright-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayuradhu dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayuradhu dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nikkaama vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa nikkaama vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhirukkaa dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkaa dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa munnaadi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa munnaadi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayuradhu dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayuradhu dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nikkaama vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa nikkaama vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhirukkaa dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkaa dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa munnaadi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa munnaadi vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhirukkaa dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkaa dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa munnaadi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa munnaadi vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkavae vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkavae vaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi vaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi vaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Speed-u kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speed-u kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooda vida poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooda vida poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kaadhu munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaadhu munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa paaru kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa paaru kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kanna light-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kanna light-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Night-a konjam bright-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night-a konjam bright-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kanna light-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kanna light-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Night-a konjam bright-aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night-a konjam bright-aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi varum pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi varum pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mudikka"/>
</div>
</pre>
