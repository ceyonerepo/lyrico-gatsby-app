---
title: "vidaikodu vidaikodu vizhiye song lyrics"
album: "Pririyadha Varam Vendum"
artist: "S. A. Rajkumar"
lyricist: "Pazhani Bharathi"
director: "Kamal"
path: "/albums/pririyadha-varam-vendum-lyrics"
song: "Vidaikodu Vidaikodu Vizhiye"
image: ../../images/albumart/piriyadha-varam-vendum.jpg
date: 2001-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AXym19zTHZg"
type: "sad"
singers:
  - P. Unnikrishnan
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vidai Kodu Vidai Kodu Vizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Vidai Kodu Vizhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerin Payanam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin Payanam Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Vazhi Vidu Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Vazhi Vidu Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Mattum Pogirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Mattum Pogirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Sunai Ootrile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Sunai Ootrile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppinai Ootrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppinai Ootrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pournami Koppayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pournami Koppayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Kuditthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Kuditthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullang Kaiyil Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullang Kaiyil Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Ootri Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Ootri Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Povathaai Varuguraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathaai Varuguraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Murai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Murai Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inndre Vidai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inndre Vidai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrunai Ketkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrunai Ketkindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthayai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounathil Idarugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil Idarugindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulle Nadaiperum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Nadaiperum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadagam Thiraivilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam Thiraivilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velayil Medaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayil Medaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thondrugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Thani Kaayamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Thani Kaayamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranapada Thonuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranapada Thonuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaikale Kelviyai Aagirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaikale Kelviyai Aagirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Kodu Vidai Kodu Viliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Vidai Kodu Viliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannerin Payanam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerin Payanam Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Pechai Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Pechai Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhiyai Pirindhu Korthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyai Pirindhu Korthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhthinen Maraigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthinen Maraigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gniyaabagathai Korthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gniyaabagathai Korthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Kaadhalai Natpil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kaadhalai Natpil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodiya Idhayathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiya Idhayathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Murai Veliyil Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Murai Veliyil Edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Saalaigal Nedungil Poovidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Saalaigal Nedungil Poovidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargalai Valarthida Urimai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargalai Valarthida Urimai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerkumil Meethile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerkumil Meethile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Sumai Yetrinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Sumai Yetrinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhir Disai Thooramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir Disai Thooramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaikkiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaikodu Vidaikodu Vizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaikodu Vidaikodu Vizhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannerin Payanam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerin Payanam Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Vazhi Vidu Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Vazhi Vidu Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Mattum Pogirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Mattum Pogirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Sunai Ootrile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Sunai Ootrile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppinai Ootrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppinai Ootrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pournami Koppayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pournami Koppayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Kudiththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Kudiththai"/>
</div>
</pre>
