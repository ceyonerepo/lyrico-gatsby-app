---
title: "who am i song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "Who Am I"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vsvxfuYi_3I"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">One Two Three Lets Walk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Two Three Lets Walk"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Marudhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Marudhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadinam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadinam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Sirikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Sirikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunodi Azghukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunodi Azghukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkathil Kidakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkathil Kidakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupinil Pizhaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupinil Pizhaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum Virupathil Kodhikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Virupathil Kodhikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannavugal Vithaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannavugal Vithaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaindhadhum Thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaindhadhum Thavikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulai Ketkiren Kulambudey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai Ketkiren Kulambudey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirappadhum Irappadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirappadhum Irappadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuvukkum Nadakkumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuvukkum Nadakkumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhanaigal Seidhu Vidu Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanaigal Seidhu Vidu Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinasari Kavalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinasari Kavalaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Thurathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Thurathida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththanaiyum Enakkulley Pudhaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanaiyum Enakkulley Pudhaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthamendru Solvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamendru Solvena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Enna Solvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Enna Solvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Enna Solveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enna Solveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkondrum Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkondrum Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Nanbargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Nanbargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaiyendru Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyendru Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Nanbanaai Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Nanbanaai Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Edhirigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Edhirigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumillai Endru Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillai Endru Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Edhiriyaai Verukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Edhiriyaai Verukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Velvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Velvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Kolvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Kolvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhappakkam Selveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhappakkam Selveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkondrum Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkondrum Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Marudhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Marudhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadinam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadinam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Sirikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Sirikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunodi Azghukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunodi Azghukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkathil Kidakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkathil Kidakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupinil Pizhaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupinil Pizhaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum Virupathil Kodhikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Virupathil Kodhikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannavugal Vithaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannavugal Vithaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaindhadhum Thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaindhadhum Thavikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulai Ketkiren Kulambudey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai Ketkiren Kulambudey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who Am I Who Am I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who Am I Who Am I"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whoooo Am Ii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whoooo Am Ii"/>
</div>
</pre>
