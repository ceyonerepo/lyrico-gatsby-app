---
title: "paayum puli song lyrics"
album: "Enkitta Mothathe"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Ramu Chellappa"
path: "/albums/enkitta-mothathe-lyrics"
song: "Paayum Puli"
image: ../../images/albumart/enkitta-mothathe.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OE1HL_1A5IM"
type: "happy"
singers:
  - Hariharasudan
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paayum Puli Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Puli Naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthiduvom Vaanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthiduvom Vaanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeporiyum Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeporiyum Naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Theethruvom Ponga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theethruvom Ponga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayum Puli Naanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Puli Naanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga Da Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga Da Naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthiduvom Vaanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthiduvom Vaanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanga Da Vaanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanga Da Vaanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeporiyum Naanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeporiyum Naanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Theethruvom Ponga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theethruvom Ponga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">ponga Da Ponga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponga Da Ponga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Akku Akka Umma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Akku Akka Umma "/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Pekka Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Pekka Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothu Kotha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothu Kotha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kopurane Thookava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopurane Thookava"/>
</div>
<div class="lyrico-lyrics-wrapper">Akku Akka Umma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akku Akka Umma "/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Pekka Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Pekka Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothu Kotha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothu Kotha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kopurane Thookava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopurane Thookava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirthu Vandhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthu Vandhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Midhichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhichiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulaiyum Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulaiyum Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhichiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki Sata Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sata Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Setta Yedhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setta Yedhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Murattu Kaala Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu Kaala Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala Surutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Surutu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayum Puli Naanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Puli Naanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga Da Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga Da Naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthiduvom Vaanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthiduvom Vaanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanga Da Vaanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanga Da Vaanga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayaganum Naanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayaganum Naanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga Da Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga Da Naanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Saachiruvom Vaanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saachiruvom Vaanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Da Vaanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Da Vaanga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Solapori Neenga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solapori Neenga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">neenga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothiruvom Ponga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiruvom Ponga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">ponga Da Ponga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponga Da Ponga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetu Onnu Thundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetu Onnu Thundu "/>
</div>
<div class="lyrico-lyrics-wrapper">Renda Aakava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renda Aakava"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Kandam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Kandam "/>
</div>
<div class="lyrico-lyrics-wrapper">Potu Usi Kokava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu Usi Kokava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetu Onnu Thundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetu Onnu Thundu "/>
</div>
<div class="lyrico-lyrics-wrapper">Renda Aakava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renda Aakava"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Kandam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Kandam "/>
</div>
<div class="lyrico-lyrics-wrapper">Potu Usi Kokava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potu Usi Kokava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egura Vandha Erichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egura Vandha Erichiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogaraiyathaan Urichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaraiyathaan Urichiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki Sata Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sata Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Setta Yedhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setta Yedhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Murattu Kaala Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu Kaala Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala Surutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Surutu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Murattu Kaala Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu Kaala Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala Surutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Surutu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayum Puli Naanga Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Puli Naanga Da "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga Da Naanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga Da Naanga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki Sata Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Sata Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Setta Yedhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setta Yedhuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Murattu Kaala Naangadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murattu Kaala Naangadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala Surutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Surutu"/>
</div>
</pre>
