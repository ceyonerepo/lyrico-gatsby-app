---
title: "jaalame song lyrics"
album: "Trance"
artist: "Jackson Vijayan - Vinayakan"
lyricist: "Vinayak Sasikumar"
director: "Anwar Rasheed"
path: "/albums/trance-lyrics"
song: "Jaalame"
image: ../../images/albumart/trance.jpg
date: 2020-02-20
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/1X7Fi_hTI8k"
type: "melody"
singers:
  - Rex Vijayan
  - Lal Krishna
  - Divya S. Menon
  - Namitha Raju
  - Gagul Joseph
  - Jackson Vijayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaalame Thiruvelichathin Naalame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalame Thiruvelichathin Naalame "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthiduka Ningal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthiduka Ningal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhanaam Avan Karangalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhanaam Avan Karangalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Saandhwanam Kezhnniduka Ningal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saandhwanam Kezhnniduka Ningal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avane Nin Abhayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avane Nin Abhayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Avane Nin Ulagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avane Nin Ulagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Avanilaa navasaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanilaa navasaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Avanilaa nini-ninyaathrayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanilaa nini-ninyaathrayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaalame Thiruvelichathin Naalame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalame Thiruvelichathin Naalame "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthiduka Ningal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthiduka Ningal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhanaam Avan Karangalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhanaam Avan Karangalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Saandhwanam Kezhnniduka Ningal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saandhwanam Kezhnniduka Ningal "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegamagamavan Ninneriyum Athmaavin  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamagamavan Ninneriyum Athmaavin  "/>
</div>
<div class="lyrico-lyrics-wrapper">Shaanthi Nilayamavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaanthi Nilayamavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sneharoopanavan Nivyathakalonnake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneharoopanavan Nivyathakalonnake "/>
</div>
<div class="lyrico-lyrics-wrapper">Theerkkum Idayanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerkkum Idayanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Kaikal Chumbippin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kaikal Chumbippin "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Paadham Vandippin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Paadham Vandippin "/>
</div>
<div class="lyrico-lyrics-wrapper">Swargathe Prapikkuvin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargathe Prapikkuvin "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapathin Chenthiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapathin Chenthiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Neerunna Ninne  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerunna Ninne  "/>
</div>
<div class="lyrico-lyrics-wrapper">Moksham Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moksham Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaachikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaachikkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Shaapabhaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Shaapabhaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuvaangum Salputhran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuvaangum Salputhran "/>
</div>
<div class="lyrico-lyrics-wrapper">Ekasathyamavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekasathyamavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninte Vyathikalthan Nithya Mukthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Vyathikalthan Nithya Mukthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nin Shudhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin Shudhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkum Karunayavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkum Karunayavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Kaikal Chumbippin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kaikal Chumbippin "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Paadham Vandippin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Paadham Vandippin "/>
</div>
<div class="lyrico-lyrics-wrapper">Swargathe Prapikkuvin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargathe Prapikkuvin "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapathin Chenthiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapathin Chenthiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Neerunna Ninte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerunna Ninte "/>
</div>
<div class="lyrico-lyrics-wrapper">Moksham Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moksham Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaachikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaachikkume"/>
</div>
</pre>
