---
title: "pedda puli song lyrics"
album: "Chal Mohan Ranga"
artist: "S S Thaman"
lyricist: "Sahithi"
director: "Krishna Chaitanya"
path: "/albums/chal-mohan-ranga-lyrics"
song: "Pedda Puli"
image: ../../images/albumart/chal-mohan-ranga.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6Um_Pkl8qiY"
type: "happy"
singers:
  -	Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arey Inkora Bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Inkora Bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku America Eesa Vachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku America Eesa Vachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gee Manchi Gadiyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gee Manchi Gadiyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cigarettu Aggitho Aaratichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cigarettu Aggitho Aaratichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Surapanamtho Nee Sopathiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surapanamtho Nee Sopathiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Davathivvalani Pabbati Pattinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davathivvalani Pabbati Pattinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara Bhadram Ga Elli Ra Bidoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara Bhadram Ga Elli Ra Bidoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindeyyi Saami Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindeyyi Saami Ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivamettu Subaranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivamettu Subaranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chal Mohan Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chal Mohan Ranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ranga Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ranga Ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindeyyi Saami Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindeyyi Saami Ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivamettu Subaranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivamettu Subaranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chal Mohan Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chal Mohan Ranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Podugale Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Podugale Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bonam Yetti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bonam Yetti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandi Maisaki Mokkellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandi Maisaki Mokkellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali Moter Yekkellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Moter Yekkellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Nuvvu Peddapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Nuvvu Peddapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Lekka Goditiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Lekka Goditiviro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bemmandam Baddalu Gotteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bemmandam Baddalu Gotteyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Lekka Goditiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Lekka Goditiviro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bemmandam Baddalu Gotteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bemmandam Baddalu Gotteyro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Podugale Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Podugale Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bonam Yetti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bonam Yetti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandi Maisaki Mokkellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandi Maisaki Mokkellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali Moter Yekkellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Moter Yekkellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Nuvvu Peddapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Nuvvu Peddapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Lekka Goditiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Lekka Goditiviro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bemmandam Baddalu Gotteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bemmandam Baddalu Gotteyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Peddapuli Lekka Goditiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Peddapuli Lekka Goditiviro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bemmandam Baddalu Gotteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bemmandam Baddalu Gotteyro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ranga Chindeyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ranga Chindeyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivamettu Shivamettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivamettu Shivamettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivamettu Subbaranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivamettu Subbaranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindeyi Chindeyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindeyi Chindeyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal Mohan Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Mohan Ranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti Gotti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti Gotti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satri Vatti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satri Vatti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damkaistu Daftarlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damkaistu Daftarlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamainchu Dollerlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamainchu Dollerlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Areyre re re re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Areyre re re re"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti Gotti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti Gotti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satri Vatti Bailellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satri Vatti Bailellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damkaistu Daftarlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damkaistu Daftarlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamainchu Dollerlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamainchu Dollerlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Peddapuli Nuvvu Peddapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Peddapuli Nuvvu Peddapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Peddapuli Yeshamgatteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Peddapuli Yeshamgatteyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chapanna Desam Chutteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chapanna Desam Chutteyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Peddapuli Yeshamgatteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Peddapuli Yeshamgatteyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chapanna Desam Chutteyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chapanna Desam Chutteyro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">U S Koluvu Needaite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U S Koluvu Needaite"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladikilanta Neevente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladikilanta Neevente"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyenadu Nee Pelle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyenadu Nee Pelle"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Neeku Attille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Neeku Attille"/>
</div>
<div class="lyrico-lyrics-wrapper">Areyy re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Areyy re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">U S Koluvu Needaite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U S Koluvu Needaite"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladikilanta Neevente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladikilanta Neevente"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyenadu Nee Pelle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyenadu Nee Pelle"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Neeku Attille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Neeku Attille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Neeku Peddapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Neeku Peddapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Pattam Gadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Pattam Gadataruro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaneeki Poti Vadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaneeki Poti Vadataruro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Neeku Peddapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Neeku Peddapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Pattam Gadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Pattam Gadataruro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaneeki Poti Vadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaneeki Poti Vadataruro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Chal Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chal Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal Chal Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chal Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohan Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohan Ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Pattam Gadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Pattam Gadataruro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaneeki Poti Vadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaneeki Poti Vadataruro"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Peddapuli Pattam Gadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Peddapuli Pattam Gadataruro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaneeki Poti Vadataruro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaneeki Poti Vadataruro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Ranga Seendeyi Shivamettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Ranga Seendeyi Shivamettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivamettu Shivamettu Subaranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivamettu Shivamettu Subaranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindeyi Chindeyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindeyi Chindeyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Chal Mohan Ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Chal Mohan Ranga"/>
</div>
</pre>
