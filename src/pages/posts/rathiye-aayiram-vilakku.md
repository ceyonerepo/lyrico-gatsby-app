---
title: "rathiye song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Rathiye"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2EiGr8TQKGk"
type: "love"
singers:
  - Harish Raghavendra
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenji kenjiyae oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenji kenjiyae oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji konjiyae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konjiyae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai paarkkum munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkkum munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulkooda poi endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulkooda poi endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannai paartha pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannai paartha pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul undu engindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul undu engindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha mani maadam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha mani maadam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aimbulangalin paadam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimbulangalin paadam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukku oru yeni nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku oru yeni nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti anaikkaiyil vallal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti anaikkaiyil vallal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai thodugaiyil minnal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai thodugaiyil minnal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudavaikkul oru thaeni nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudavaikkul oru thaeni nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodhal maadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodhal maadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan koondhal porvaiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan koondhal porvaiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veiyil maadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veiyil maadham"/>
</div>
<div class="lyrico-lyrics-wrapper">andha koondhal visiriyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha koondhal visiriyaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhai sollumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai sollumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaivaanar neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaivaanar neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karppukkul nuzhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppukkul nuzhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaani neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paal mugam paarkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paal mugam paarkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththanaai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththanaai aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un baakkiyai paarkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un baakkiyai paarkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththanaai pogiren pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththanaai pogiren pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm..mmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm..mmmmmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambukkul siru pooppookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambukkul siru pooppookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbukkul adhu thaen vaarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbukkul adhu thaen vaarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam vidaikkondu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam vidaikkondu pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu mudhuginil nandoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu mudhuginil nandoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasukkul oru vandoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasukkul oru vandoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nilaigalum maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan nilaigalum maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu pookkal vendum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu pookkal vendum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai ellaam sondham kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai ellaam sondham kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai seidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai seidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan megam veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan megam veezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Men kaatru podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Men kaatru podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan yaanai veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan yaanai veezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthaanaippodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthaanaippodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ven megangal pogaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven megangal pogaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha oviyam yaavilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha oviyam yaavilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarkkiren paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam paarkkiren paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhiyae en radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhiyae en radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En agilam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agilam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae en sagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagiyae en sagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sagalam naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sagalam naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenji kenjiyae oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenji kenjiyae oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji konjiyae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konjiyae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai paarkkum munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkkum munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulkooda poi endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulkooda poi endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannai paartha pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannai paartha pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul undu engindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul undu engindren"/>
</div>
</pre>
