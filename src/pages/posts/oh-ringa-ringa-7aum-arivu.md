---
title: "oh ringa ringa song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Pa. Vijay"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Oh Ringa Ringa"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/It3lEzmSVUo"
type: "mass"
singers:
  - Roshan
  - Jerry John
  - Benny Dayal
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Ringa Ringa Jamaikalaam Gang Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ringa Ringa Jamaikalaam Gang Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Binga Binga Hiphopula Song Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Binga Binga Hiphopula Song Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On Andra Indra Natpendrume Neengaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Andra Indra Natpendrume Neengaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ondra Ondra Naam Aayiram Poonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ondra Ondra Naam Aayiram Poonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Vanaa Ovanaa Onnonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Vanaa Ovanaa Onnonnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otamum Aattamum Inithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamum Aattamum Inithaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Naalumey Thaenthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Naalumey Thaenthaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanin Nanbanum Naanthanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanin Nanbanum Naanthanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nenjadangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nenjadangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nanachatha Nadathiko Nadathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nanachatha Nadathiko Nadathiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Urangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Urangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Eduthukko Eduthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Eduthukko Eduthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nenjadangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nenjadangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenachatha Nadathiko Nadathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenachatha Nadathiko Nadathiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Eduthukko Eduthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Eduthukko Eduthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Binga Binga Jamaikalaam Gang Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Binga Binga Jamaikalaam Gang Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Ringa Ringa Hiphopula Song Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Ringa Ringa Hiphopula Song Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On Andra Indra Natpendrumey Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Andra Indra Natpendrumey Neenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ondra Ondra Naam Aayiram Poonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ondra Ondra Naam Aayiram Poonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Aila Aila Ae Ailey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Aila Aila Ae Ailey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Life Kooda Oru Railey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Life Kooda Oru Railey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Oda Oda Oru Styley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Oda Oda Oru Styley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkaathey Ninnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaathey Ninnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Oiley Oiley O Oiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Oiley Oiley O Oiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasam Motham Namma Kaile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasam Motham Namma Kaile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaatha Vaazhvu Verum Jaille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaatha Vaazhvu Verum Jaille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagengum Ullaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagengum Ullaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niraiya Niraiyave Thullikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraiya Niraiyave Thullikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuraiya Kuraiyave Allikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiya Kuraiyave Allikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theliya Theliyave Kathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliya Theliyave Kathuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therintha Thavarugal Othuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therintha Thavarugal Othuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nenjadanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nenjadanguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nanachatha Nadathiko Nadathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nanachatha Nadathiko Nadathiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Eduthukko Eduthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Eduthukko Eduthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nenjadanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nenjadanguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nanachatha Nadathiko Nadathiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nanachatha Nadathiko Nadathiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Eduthukko Eduthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Eduthukko Eduthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Taachu Taachu Thotachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Taachu Taachu Thotachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Serthu Serthu Koottaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Serthu Serthu Koottaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpodu Paathu Potaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpodu Paathu Potaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasellam MotTaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellam MotTaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Aachu Aachu Puthusaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Aachu Aachu Puthusaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Pona Nimisam Palasaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Pona Nimisam Palasaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinanthorum Thorum Thinusaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinanthorum Thorum Thinusaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellame Namakaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Namakaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Layika Layikave Aattamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Layika Layikave Aattamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeikka Jeyikkave Koottamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikka Jeyikkave Koottamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyara Uyarave Megamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyara Uyarave Megamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarum Pothu Vegamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarum Pothu Vegamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ringa Ringa Jamaikalaam Gang Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ringa Ringa Jamaikalaam Gang Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Binga Binga Hiphopula Song Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Binga Binga Hiphopula Song Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On Andra Indra Natpendrume Neengaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Andra Indra Natpendrume Neengaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ondra Ondra Naam Aayiram Poonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ondra Ondra Naam Aayiram Poonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Vanaa Ovanaa Onnonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Vanaa Ovanaa Onnonnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otamum Aattamum Inithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamum Aattamum Inithaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Naalumey Thaenthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Naalumey Thaenthaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanin Nanbanum Naanthanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanin Nanbanum Naanthanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nenjadanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nenjadanguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nanachatha Blackamofy Balckamofy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nanachatha Blackamofy Balckamofy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Mekomofy Mekomofy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Mekomofy Mekomofy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gama Gama Nengadangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gama Gama Nengadangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nanchatha Blackamofy Balckamofy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nanchatha Blackamofy Balckamofy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Guma Guma Kan Uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Guma Guma Kan Uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kedachatha Mekomofy Mekomofy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kedachatha Mekomofy Mekomofy"/>
</div>
</pre>
