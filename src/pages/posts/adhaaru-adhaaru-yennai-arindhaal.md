---
title: "adhaaru adhaaru song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Vignesh Shivan"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Adhaaru Adhaaru"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/misuAYV7Z2s"
type: "Celebration"
singers:
  - Vijay Prakash
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Raja Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Raja Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Itha Un Daavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Itha Un Daavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ellaarukku No Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ellaarukku No Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Valikkuthu Slow Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Valikkuthu Slow Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Raja Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Raja Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Itha Un Daavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Itha Un Daavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ellaarukku No Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ellaarukku No Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Valikkuthu Slow Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Valikkuthu Slow Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanathu Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathu Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kaiya Meeri Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kaiya Meeri Pochchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yen Vetti Pechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yen Vetti Pechchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Sokkaakkuthu Matchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Sokkaakkuthu Matchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungiyathaan Thookki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungiyathaan Thookki Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dustu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustu Paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selaiyathaan Yeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyathaan Yeththi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hippu Therithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hippu Therithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungiyathaan Thookki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungiyathaan Thookki Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dustu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustu Paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selaiyathaan Yeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyathaan Yeththi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hippu Therithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hippu Therithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangi Pona Moonji Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangi Pona Moonji Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daaladikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Songi Pona Namma Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Songi Pona Namma Janam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothadikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaamey Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamey Inimel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaathaan Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaathaan Nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaasum Summaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasum Summaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koluthaama Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthaama Vedikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaamey Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamey Inimel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaathaan Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaathaan Nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaasum Summaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasum Summaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koluthaama Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthaama Vedikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasa Malli Vaasam Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasa Malli Vaasam Veesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasal Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Variyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aasai Ellaam Yeththukathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Ellaam Yeththukathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasa Malli Vaasam Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasa Malli Vaasam Veesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasal Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Variyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aasai Ellaam Yeththukathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Ellaam Yeththukathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Collarrathaan Thookkamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Collarrathaan Thookkamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aada Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Variyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigil Aduchchu Ragala Seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigil Aduchchu Ragala Seiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Ready Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ready Ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Collarrathaan Thookkamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Collarrathaan Thookkamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aada Variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Variyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigil Aduchchu Ragala Seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigil Aduchchu Ragala Seiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Ready Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ready Ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhai Kattum Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhai Kattum Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Master of Kola Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master of Kola Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lighta Kattum Vichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lighta Kattum Vichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Poduvaanda Sketchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Poduvaanda Sketchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Galeeju Raaidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galeeju Raaidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parisuththamaana Fradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisuththamaana Fradu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Haiyo oho Haiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Haiyo oho Haiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhala Kattum Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhala Kattum Mayilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Porandha Idam Jailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Porandha Idam Jailu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhiya Paakkura Baala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhiya Paakkura Baala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaikku Arsi Poduvaan Dhoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaikku Arsi Poduvaan Dhoolaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panneera Thelikkum Jhon nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panneera Thelikkum Jhon nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Sumaara Poduvaan Scenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Sumaara Poduvaan Scenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Haiyo oho Haiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Haiyo oho Haiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ellaam Onna Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ellaam Onna Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Orey Thattula Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Orey Thattula Soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ellaaroda Natpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ellaaroda Natpu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Kaaththula Kalandha Uppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Kaaththula Kalandha Uppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaaru Adhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaaru Adhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhaaru Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaaru Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaathe Udhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaathe Udhaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungiyathaan Thookki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungiyathaan Thookki Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dustu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustu Paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seliyathaan Yeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seliyathaan Yeththi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hippu Therithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hippu Therithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangi Pona Moonji Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangi Pona Moonji Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daaladikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Songi Pona Namma Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Songi Pona Namma Janam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothadikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangi Pona Moonji Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangi Pona Moonji Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daaladikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Songi Pona Namma Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Songi Pona Namma Janam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothadikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Raja Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Raja Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Itha Un Daavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Itha Un Daavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ellaarukku No Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ellaarukku No Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Valikkuthu Slow Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Valikkuthu Slow Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Raja Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Raja Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Itha Un Daavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Itha Un Daavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Ellaarukku No Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ellaarukku No Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Valikkuthu Slow Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Valikkuthu Slow Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanathu Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathu Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kaiya Meeri Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kaiya Meeri Pochchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yen Vetti Pechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yen Vetti Pechchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Sokkaakkuthu Matchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Sokkaakkuthu Matchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungiyathaan Thookki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungiyathaan Thookki Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dustu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustu Paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seliyathaan Yeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seliyathaan Yeththi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hippu Therithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hippu Therithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungiyathaan Thookki Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungiyathaan Thookki Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dustu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustu Paduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seliyathaan Yeththi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seliyathaan Yeththi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hippu Therithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hippu Therithu"/>
</div>
</pre>
