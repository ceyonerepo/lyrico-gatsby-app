---
title: "kaasuda song lyrics"
album: "Onaaigal Jakkiradhai"
artist: "Adheesh Uthrian"
lyricist: "Sirkazhi Sirpi"
director: "JPR"
path: "/albums/onaaigal-jakkiradhai-lyrics"
song: "Kaasuda"
image: ../../images/albumart/onaaigal-jakkiradhai.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gYhWtWsBUIw"
type: "mass"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu illama pona vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu illama pona vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">romba waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu illama pona vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu illama pona vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">romba waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaya notu ipo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaya notu ipo "/>
</div>
<div class="lyrico-lyrics-wrapper">waste da puthiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da puthiya "/>
</div>
<div class="lyrico-lyrics-wrapper">notuku than mouse da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="notuku than mouse da"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaya notu ipo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaya notu ipo "/>
</div>
<div class="lyrico-lyrics-wrapper">waste da puthiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da puthiya "/>
</div>
<div class="lyrico-lyrics-wrapper">notuku than mouse da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="notuku than mouse da"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu illama pona vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu illama pona vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">romba waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba waste da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naai kutti vanga kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai kutti vanga kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbana piripathum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbana piripathum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam vangavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam vangavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">andha sattam pesavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha sattam pesavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">seetu aadavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seetu aadavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">mla seetu vangavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mla seetu vangavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kovilukulla kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovilukulla kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">komali vesam potalum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komali vesam potalum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kovilukulla kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovilukulla kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">komali vesam potalum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komali vesam potalum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kaasu illatha vaalkai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kaasu illatha vaalkai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">valvathu than waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvathu than waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kaasu illatha vaalkai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kaasu illatha vaalkai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">valvathu than waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvathu than waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vasthu meenukum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthu meenukum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadagai thaaikum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadagai thaaikum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">mela parakavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela parakavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">onna keelz puthaikavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna keelz puthaikavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">chatting pannavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chatting pannavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee datin pogavum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee datin pogavum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ambani aakuvathum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambani aakuvathum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">unna ambonu aakuvathum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ambonu aakuvathum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ambani aakuvathum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambani aakuvathum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">unna ambonu aakuvathum kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ambonu aakuvathum kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kaasu illatha vaalkai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kaasu illatha vaalkai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">valvathu than waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvathu than waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kaasu illatha vaalkai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kaasu illatha vaalkai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">valvathu than waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvathu than waste da"/>
</div>
<div class="lyrico-lyrics-wrapper">waste da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasuda nanba kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasuda nanba kaasuda"/>
</div>
</pre>
