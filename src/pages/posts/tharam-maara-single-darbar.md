---
title: 'tharam maara single song lyrics'
album: 'Darbar'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'A R Murugadoss'
path: '/albums/darbar-song-lyrics'
song: 'Tharam Maara Single'
image: ../../images/albumart/darbar.jpg
date: 2020-01-09
lang: tamil
singers:
- Arjun Chandy
- Anirudh Ravichander Ravichander
youtubeLink: "https://www.youtube.com/embed/QUoJIknUk-A"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ohoo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohoo ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance-ae indha face la
<input type="checkbox" class="lyrico-select-lyric-line" value="Romance-ae indha face la"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Varlanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodaama nippiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodaama nippiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda neeyum
<input type="checkbox" class="lyrico-select-lyric-line" value="En kooda neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh tharam maara
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh tharam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Single naanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Single naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En paavaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En paavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha star-um
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha star-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Twinkle aagavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Twinkle aagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thevayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thevayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Purchase panna
<input type="checkbox" class="lyrico-select-lyric-line" value="Purchase panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Market ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Market ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En mullaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En mullaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga kuda
<input type="checkbox" class="lyrico-select-lyric-line" value="Anga kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladies finger-eh
<input type="checkbox" class="lyrico-select-lyric-line" value="Ladies finger-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enga veetu calender
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga veetu calender"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla date-ah kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla date-ah kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhakkam pola lovers day
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhakkam pola lovers day"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku mattum april one-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku mattum april one-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Movie kootti povenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Movie kootti povenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchiyatha servenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Latchiyatha servenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Couple seat-ah paappenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Couple seat-ah paappenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadasi varaikkum only naana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadasi varaikkum only naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Romance-ae indha face la
<input type="checkbox" class="lyrico-select-lyric-line" value="Romance-ae indha face la"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Varlanaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella le bella ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella le bella ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un accountil vaazhuvan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un accountil vaazhuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum sethukodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanaalum sethukodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Flow illaama paaduven
<input type="checkbox" class="lyrico-select-lyric-line" value="Flow illaama paaduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittaama ketukodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thittaama ketukodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaal illaadha vaanaram
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaal illaadha vaanaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendaaga yethukodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendaaga yethukodi"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannoda sogamae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kannoda sogamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaama paathukodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Illaama paathukodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjodu set aaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu set aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Fit aaga yaaradi
<input type="checkbox" class="lyrico-select-lyric-line" value="Fit aaga yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda bloododa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennoda bloododa"/>
</div>
<div class="lyrico-lyrics-wrapper">Red aaga yaaradi
<input type="checkbox" class="lyrico-select-lyric-line" value="Red aaga yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Zero dhaan en kaadhal
<input type="checkbox" class="lyrico-select-lyric-line" value="Zero dhaan en kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Balance paaradi
<input type="checkbox" class="lyrico-select-lyric-line" value="Balance paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Beer owner kattaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Beer owner kattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Loan aaga neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Loan aaga neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh tharam maara
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh tharam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Single naanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Single naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En paavaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En paavaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha star-um
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha star-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Twinkle aagavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Twinkle aagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thevayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thevayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Purchase panna
<input type="checkbox" class="lyrico-select-lyric-line" value="Purchase panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Market ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Market ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En mullaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En mullaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga kuda
<input type="checkbox" class="lyrico-select-lyric-line" value="Anga kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladies finger-eh
<input type="checkbox" class="lyrico-select-lyric-line" value="Ladies finger-eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enga veetu calender
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga veetu calender"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla date-ah kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla date-ah kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhakkam pola lovers day
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhakkam pola lovers day"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku mattum april one-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku mattum april one-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Movie kootti povenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Movie kootti povenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchiyatha servenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Latchiyatha servenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Couple seat-ah paappenaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Couple seat-ah paappenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadasi varaikkum naan mattum thaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadasi varaikkum naan mattum thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Romance-ae indha face la
<input type="checkbox" class="lyrico-select-lyric-line" value="Romance-ae indha face la"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Varlanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodaama nippiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodaama nippiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda neeyum
<input type="checkbox" class="lyrico-select-lyric-line" value="En kooda neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Luba luba le bella le bella ((<div class="lyrico-lyrics-wrapper">4) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Luba luba le bella le bella ((<div class="lyrico-lyrics-wrapper">4) times)"/></div>
</div>
</pre>