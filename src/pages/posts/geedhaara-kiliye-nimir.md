---
title: "geedhaara kiliye song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Mohan Rajan"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Geedhaara Kiliye"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cdWOKvJPMf8"
type: "love"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ni sa ga ri gaa ree nee saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni sa ga ri gaa ree nee saa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ga ma pa dha ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ga ma pa dha ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha ri dha ri ni pa sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha ri dha ri ni pa sa"/>
</div>
<div class="lyrico-lyrics-wrapper">dha ri ni dha ri ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dha ri ni dha ri ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiro udalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiro udalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai viyakkum peralaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai viyakkum peralaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanavo ninaivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanavo ninaivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir urukkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir urukkidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai pozhudhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai pozhudhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhai neengadhiruppaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhai neengadhiruppaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai vidiyum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai vidiyum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhai thoongaadhiruppaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhai thoongaadhiruppaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochodu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochodu serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un swasam azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un swasam azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naalodu theeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naalodu theeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mogam azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mogam azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvizhiyaal pesaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvizhiyaal pesaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvizhiyaal pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvizhiyaal pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idigalai nee veesaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idigalai nee veesaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maazhaiyai veesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maazhaiyai veesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam nanaindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam nanaindhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu neeUyiro udalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu neeUyiro udalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai viyakkum peralaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai viyakkum peralaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanavo ninaivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanavo ninaivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir urukkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir urukkidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya karuvizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya karuvizhiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalai velvaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalai velvaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum sirumozhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum sirumozhiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal solvaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal solvaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhara theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhara theeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvai azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvai azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru sedharam aagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru sedharam aagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perugiyadhae kaadhal dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugiyadhae kaadhal dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal alaiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal alaiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhikiradhae thedal dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhikiradhae thedal dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ninaikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ninaikkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiro udalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiro udalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai viyakkum peralaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai viyakkum peralaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanavo ninaivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanavo ninaivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo Ninaivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo Ninaivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo Ninaivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo Ninaivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivoo Ninaivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivoo Ninaivoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaageedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaageedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanin geedhaara kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanin geedhaara kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevaara thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevaara thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjidum dhevaara thamizhaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjidum dhevaara thamizhaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhaeeeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhaeeeae ae"/>
</div>
</pre>
