---
title: "unakulla olichu song lyrics"
album: "Thodraa"
artist: "RN Uthamaraja - Navin Shander"
lyricist: "Seerkazhi Sirpi"
director: "Madhuraj"
path: "/albums/thodraa-lyrics"
song: "Unakulla Olichu"
image: ../../images/albumart/thodraa.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NHkHLTrjC_Q"
type: "love"
singers:
  - Anthakudi Ilayaraja
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adi unakkulla olichuvecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unakkulla olichuvecha"/>
</div>
<div class="lyrico-lyrics-wrapper">un ottumotha kadhalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ottumotha kadhalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">otha nodiyila kandupuducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha nodiyila kandupuducha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu rekka katti nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu rekka katti nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sooraavali kaatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooraavali kaatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thakura"/>
</div>
<div class="lyrico-lyrics-wrapper">soorathenga pola ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorathenga pola ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">sillu silla pekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillu silla pekura"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai partha naal muthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai partha naal muthala"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha maranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada enakkulla olichu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada enakkulla olichu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">en ottu motha kathala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ottu motha kathala than"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyila kandu puducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyila kandu puducha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu rekka katti nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu rekka katti nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennaremum un pera than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaremum un pera than"/>
</div>
<div class="lyrico-lyrics-wrapper">ucharikka nenjam nacharikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ucharikka nenjam nacharikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ottamathan thalli ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ottamathan thalli ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">mutha pasi vanthu sutreikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutha pasi vanthu sutreikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatatha jaada enththatha mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatatha jaada enththatha mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">nethikadan theerka varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethikadan theerka varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha vayasu ethaiyo theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha vayasu ethaiyo theduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasu athaiye naaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasu athaiye naaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi unakkulla olichuvecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unakkulla olichuvecha"/>
</div>
<div class="lyrico-lyrics-wrapper">un ottumotha kadhalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ottumotha kadhalathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sontha mena vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontha mena vanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyellam poo paiyidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyellam poo paiyidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonthamellam intha vanjikuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthamellam intha vanjikuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">vanjamilla unthan nenjam pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjamilla unthan nenjam pothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellama nee than velladu naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellama nee than velladu naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">appadiye thinga porendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appadiye thinga porendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam porumai unaku venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam porumai unaku venume"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kodumai enakum venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kodumai enakum venume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi unakkulla olichuvecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unakkulla olichuvecha"/>
</div>
<div class="lyrico-lyrics-wrapper">un ottumotha kadhalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ottumotha kadhalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">otha nodiyila kandupuducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha nodiyila kandupuducha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu rekka katti nan paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu rekka katti nan paranthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sooraavali kaatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooraavali kaatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi suthi thakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi suthi thakura"/>
</div>
<div class="lyrico-lyrics-wrapper">soorathenga pola ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorathenga pola ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">sillu silla pekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillu silla pekura"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai partha naal muthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai partha naal muthala"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha maranthen"/>
</div>
</pre>
