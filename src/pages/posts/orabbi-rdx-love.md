---
title: "orabbi song lyrics"
album: "RDX Love"
artist: "Radhan"
lyricist: "Bhaskar Bhatla"
director: "Bhanu Shankar"
path: "/albums/rdx-love-lyrics"
song: "Orabbi"
image: ../../images/albumart/rdx-love.jpg
date: 2019-10-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/W2BKIY7die8"
type: "love"
singers:
  - Yogi Sekar
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orabbi Konge Jarindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orabbi Konge Jarindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Gunde Jaranodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Gunde Jaranodu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yevadaina Vuntada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevadaina Vuntada "/>
</div>
<div class="lyrico-lyrics-wrapper">Jadatho Jalle Kottindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadatho Jalle Kottindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Jada Padardhame Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jada Padardhame Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kottey Kuntaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kottey Kuntaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedave Mithayilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedave Mithayilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadume Batanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadume Batanilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakale Puttistunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakale Puttistunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Tassa Diyaa Aagedelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tassa Diyaa Aagedelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuluke Bathayilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuluke Bathayilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikey Ante Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikey Ante Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadike Dada Dada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadike Dada Dada"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiri Adiri Podaa Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiri Adiri Podaa Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Najooku Kundaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Najooku Kundaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Vanti Nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Vanti Nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayyaaram Nimpukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayyaaram Nimpukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vampesthunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vampesthunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Norettakundaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norettakundaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkulne Choodakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkulne Choodakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chettantha Magaadainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettantha Magaadainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Longaalsinde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Longaalsinde "/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitaa Thadimee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitaa Thadimee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumegudu Digudulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumegudu Digudulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Kunuku Padadule Pillo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Kunuku Padadule Pillo "/>
</div>
<div class="lyrico-lyrics-wrapper">Hattheri Andam Mothham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hattheri Andam Mothham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Thala Laadagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thala Laadagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthree Pettaipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthree Pettaipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Body Motham Kaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body Motham Kaaladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muggulo Laagesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muggulo Laagesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Paruvaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Paruvaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase Maggamlaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase Maggamlaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaripoka Vuntaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaripoka Vuntaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tikke Reginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikke Reginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimmeerekkinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimmeerekkinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Agge Rajukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agge Rajukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajukundaa Baanise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajukundaa Baanise"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gupthamgaa Konni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gupthamgaa Konni"/>
</div>
<div class="lyrico-lyrics-wrapper">Klupthamgaa Konni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Klupthamgaa Konni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaallaku Chinna Chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaallaku Chinna Chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasalu Untai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasalu Untai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avi Teerutunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avi Teerutunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Swarganne Yelinanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swarganne Yelinanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandam Mogudimeede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandam Mogudimeede"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuripisthuntai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuripisthuntai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhigo Ippude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhigo Ippude "/>
</div>
<div class="lyrico-lyrics-wrapper">Telisindi Maradalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisindi Maradalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padataa Padataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padataa Padataa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvanni Jalagalaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvanni Jalagalaa Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koppulo Guppu Guppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppulo Guppu Guppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallepoolu Undaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallepoolu Undaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Appude Teesichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appude Teesichina"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthakallu Dandaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthakallu Dandaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilaka Saraakottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilaka Saraakottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedi Kottu Bandikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedi Kottu Bandikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Roju Roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Roju Roju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivarathri Pandaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivarathri Pandaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambe Reginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambe Reginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Rambhe Rammande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambhe Rammande"/>
</div>
<div class="lyrico-lyrics-wrapper">Githa Rechipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Githa Rechipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerra Koka Voopithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerra Koka Voopithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadalla Chethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadalla Chethike"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggaalu Vachakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggaalu Vachakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mamoolugaa Vuntadeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mamoolugaa Vuntadeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Choodu Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodu Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Talam Guthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Talam Guthule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Bodlo Dopaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Bodlo Dopaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Tellarlu Sayyaatalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Tellarlu Sayyaatalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Kottaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Kottaaram"/>
</div>
</pre>
