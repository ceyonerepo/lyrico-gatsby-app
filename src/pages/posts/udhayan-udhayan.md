---
title: "udhayan song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Udhayan"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Manikanth Kadri
  - Timmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Udhithaan pudhu sooriyanaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhithaan pudhu sooriyanaai "/>
</div>
<div class="lyrico-lyrics-wrapper">indha udhayan udhayan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha udhayan udhayan"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagil oru aayirathil ivan oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagil oru aayirathil ivan oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">engum serkkum kodikattum ilavattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum serkkum kodikattum ilavattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai yedhum avanukkeedu kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai yedhum avanukkeedu kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">avan vettupattaal vattappaarai oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan vettupattaal vattappaarai oor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathangarai kaathu vaangum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathangarai kaathu vaangum "/>
</div>
<div class="lyrico-lyrics-wrapper">aiyanaarai poayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyanaarai poayi"/>
</div>
<div class="lyrico-lyrics-wrapper">raakket poale kannungannaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakket poale kannungannaai "/>
</div>
<div class="lyrico-lyrics-wrapper">vaithirupaan aruvaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithirupaan aruvaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhithaan pudhu sooriyanaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhithaan pudhu sooriyanaai "/>
</div>
<div class="lyrico-lyrics-wrapper">indha udhayan udhayan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha udhayan udhayan"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagil oru aayirathil ivan oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagil oru aayirathil ivan oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">adichaa adhil adhiradithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichaa adhil adhiradithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">engal annan adithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal annan adithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">pidichaa oor pidichidanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidichaa oor pidichidanum "/>
</div>
<div class="lyrico-lyrics-wrapper">enga annan kodithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga annan kodithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO OhO OhO hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO OhO OhO hO hO hO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Osaipoal adangaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Osaipoal adangaadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">siru poochipoal mudangaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru poochipoal mudangaadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan thandhaikku thalaipillaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thandhaikku thalaipillaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">palar pillaiyo kilippillaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palar pillaiyo kilippillaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">oru theemai mun ivan theemaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru theemai mun ivan theemaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">oru oomai mun ivan oomaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru oomai mun ivan oomaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vaangidum pizhai seidhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vaangidum pizhai seidhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">payir vaadinaal mazhai peibavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payir vaadinaal mazhai peibavan"/>
</div>
</pre>
