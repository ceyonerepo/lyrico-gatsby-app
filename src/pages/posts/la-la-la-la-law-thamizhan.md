---
title: "la la la la law song lyrics"
album: "Thamizhan"
artist: "D. Imman"
lyricist: "Vairamuthu"
director: "Majith"
path: "/albums/thamizhan-lyrics"
song: "La La La La Law"
image: ../../images/albumart/thamizhan.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ecuCZeDwCrs"
type: "happy"
singers:
  - Shankar Mahadevan
  - Lavanya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">La La La La Law Mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Law Mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala La La La Love Padippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala La La La Love Padippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchiyai Aadharippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchiyai Aadharippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Vaangum Vayasu Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Vaangum Vayasu Namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Vidathaan Manasu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Vidathaan Manasu Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Koottil Sila Naal Vaazhthiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Koottil Sila Naal Vaazhthiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ada College ah Mudinjathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ada College ah Mudinjathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kaal Age gu Mudinthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaal Age gu Mudinthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Vayathodu Sinthu Paadaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Vayathodu Sinthu Paadaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Vayathodu Vaazhvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Vayathodu Vaazhvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Examsum Theernthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Examsum Theernthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Engagement Theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Engagement Theduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">License Illaatha Yaara Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="License Illaatha Yaara Paarthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lover Ival Endru Thondruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover Ival Endru Thondruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La La La La Law Mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Law Mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala La La La Love Padippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala La La La Love Padippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchiyai Aadharippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchiyai Aadharippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangu Thisaigal Aanaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangu Thisaigal Aanaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naapathunaayiram Vazhiyirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapathunaayiram Vazhiyirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ainthu Pulangal Aanaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthu Pulangal Aanaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Inbam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Inbam Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamey Siragaa Aanavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamey Siragaa Aanavanukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanum Mannum Thiranthirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum Mannum Thiranthirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettriyil Moolai Ullavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettriyil Moolai Ullavanukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavil Veedu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavil Veedu Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Mannil Ulla Porul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Mannil Ulla Porul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Ennavendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ennavendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seyarkai Kolgal Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyarkai Kolgal Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pennil Ulla Porul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pennil Ulla Porul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Ennavendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ennavendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Kanngal Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Kanngal Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salvaarai Kandavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salvaarai Kandavudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakkam Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkam Vendaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salute Adithuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Adithuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangaliley Siruthaenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaliley Siruthaenum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Therinthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Therinthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalil Vizhunthividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil Vizhunthividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La La La La Law Mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Law Mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala La La La Love Padippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala La La La Love Padippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchiyai Aadharippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchiyai Aadharippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja Konja Kuraigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja Konja Kuraigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pizhaiyillaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaiyillaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithan Vaazhvu Niraiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan Vaazhvu Niraiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokka Thangam Athiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Thangam Athiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sembu Seraamal Adhisayam Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sembu Seraamal Adhisayam Mudiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinna Athumeeral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Athumeeral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaiyil Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyil Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathiley Kolai Kuttram Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathiley Kolai Kuttram Kidaiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannikkira Velai Mattum Illaamal Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannikkira Velai Mattum Illaamal Ponaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Kidaiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Konjam Malar Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Konjam Malar Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Color Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Color Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhnthaal Enna Paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthaal Enna Paavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Vandu Theendaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Vandu Theendaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Thaandaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Thaandaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovukkenna Laabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukkenna Laabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Vaanavillai Thoranam Aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Vaanavillai Thoranam Aakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasalil Sirikkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasalil Sirikkirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallavaney Indru Mudhal Vendridu Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallavaney Indru Mudhal Vendridu Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Azhaikkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Azhaikkirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La La La La Law Mudichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Law Mudichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala La La La Love Padippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala La La La Love Padippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchiyai Aadharippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchiyai Aadharippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Vaangum Vayasu Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Vaangum Vayasu Namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Vidathaan Manasu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Vidathaan Manasu Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Koottil Sila Naal Vaazhthiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Koottil Sila Naal Vaazhthiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ada College ah Mudinjathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ada College ah Mudinjathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kaal Age gu Mudinthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaal Age gu Mudinthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Vayathodu Sinthu Paadaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Vayathodu Sinthu Paadaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Vayathodu Vaazhvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Vayathodu Vaazhvathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Exams-um Theernthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Exams-um Theernthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Engagement Theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Engagement Theduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">License Illaatha Yaara Paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="License Illaatha Yaara Paarthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lover Ival Endru Thondruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover Ival Endru Thondruthu"/>
</div>
</pre>
