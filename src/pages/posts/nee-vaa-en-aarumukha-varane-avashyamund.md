---
title: "nee vaa en aarumukha song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma - Dr. Kritaya"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Nee Vaa En Aarumukha"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/pVqA-A78psY"
type: "devotional"
singers:
  - K. S. Chithra
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee vaa en aarumukhaa poovazhakaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaa en aarumukhaa poovazhakaana"/>
</div>
<div class="lyrico-lyrics-wrapper">maal murukaa un thaay nencham eakkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maal murukaa un thaay nencham eakkathey"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paadavaa arul vaay karunai murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paadavaa arul vaay karunai murugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paramasivan pottum thiru makanentraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paramasivan pottum thiru makanentraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paranthaaman kondaadum marumakanentraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthaaman kondaadum marumakanentraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvathi naan verum thaayllavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvathi naan verum thaayllavo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaila mala kadanthe kaila malai kadanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaila mala kadanthe kaila malai kadanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhani mala ezhunthoy marathaka mayileri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhani mala ezhunthoy marathaka mayileri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thirukumarane guruguhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirukumarane guruguhane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vaa en aarumukhaa poovazhakaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaa en aarumukhaa poovazhakaana"/>
</div>
<div class="lyrico-lyrics-wrapper">maal murukaa un thaay nencham eakkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maal murukaa un thaay nencham eakkathey"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paadavaa arul vaay karunai murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paadavaa arul vaay karunai murugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadyaabharabgal orukki vilaa ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyaabharabgal orukki vilaa ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathirunne namma kaanuvanaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunne namma kaanuvanaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadyaabharabgal orukki vilaa ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyaabharabgal orukki vilaa ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathirunne namma kaanuvanaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirunne namma kaanuvanaay"/>
</div>
<div class="lyrico-lyrics-wrapper">aninjorungi nee varumenkil lathumathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aninjorungi nee varumenkil lathumathi"/>
</div>
<div class="lyrico-lyrics-wrapper">njanoru ponmayilaay ninnaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="njanoru ponmayilaay ninnaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini nee mizhikalil varaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini nee mizhikalil varaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">marakalil olichu kalikkukil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakalil olichu kalikkukil "/>
</div>
<div class="lyrico-lyrics-wrapper">manassile theeyaalum murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manassile theeyaalum murugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vaa en aarumukhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaa en aarumukhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaa en aarumukhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaa en aarumukhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poovazhakaana maal murukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovazhakaana maal murukaa"/>
</div>
</pre>
