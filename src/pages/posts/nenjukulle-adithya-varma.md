---
title: "nenjukulle song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Mohan Rajan"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Nenjukulle"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gRpcB3bjmCc"
type: "melody"
singers:
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjukullae Nee Vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae Nee Vizhundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Konjum Korththu Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Konjum Korththu Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukullae Nee Karainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukullae Nee Karainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Ennil Moodi Vaiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Ennil Moodi Vaiththen"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollada Sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollada Sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Vilagi Dhooramaai Ponathaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Vilagi Dhooramaai Ponathaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillada Nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillada Nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Anbul Vaazhnthu Kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Anbul Vaazhnthu Kolgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukullae Nee Vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae Nee Vizhundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Konjum Korththu Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Konjum Korththu Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iruntha Podhum Izhantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iruntha Podhum Izhantha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvai Thedinaen Thenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvai Thedinaen Thenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irudhi Varaiyil Endhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irudhi Varaiyil Endhan "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil Unnai Thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Unnai Thaanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaa Unadhu Iravum Enadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaa Unadhu Iravum Enadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Serumae Serumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Serumae Serumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Arugil Vaazhum Intha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Arugil Vaazhum Intha "/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Naalum Vendmae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Naalum Vendmae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulum Athu Vilagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulum Athu Vilagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Velicham Ondru Pookkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Velicham Ondru Pookkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Adhu Thudupatharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Adhu Thudupatharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Konjam Kooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Konjam Kooduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urugineno Naan Udaigireno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugineno Naan Udaigireno"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Anbinaalae Karaigireno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Anbinaalae Karaigireno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Unnai Servathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Unnai Servathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Indru Kooda Pirindhu Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indru Kooda Pirindhu Poven"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Unnai Pirivathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Unnai Pirivathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Indha Nodiyae Irandhu Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indha Nodiyae Irandhu Poven"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothumae Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumae Pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Bantham Ondru Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bantham Ondru Pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumae Vaazhumae Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumae Vaazhumae Vaazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Jenmam Endhan Jeevan Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Jenmam Endhan Jeevan Vaazhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Unnai Servathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Unnai Servathendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Indru Kooda Pirindhu Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indru Kooda Pirindhu Poven"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravasam Paravasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam Paravasam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae Paravuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae Paravuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Dhegam Unthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Dhegam Unthan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Veesikindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Veesikindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Sugam Pudhu Sugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Sugam Pudhu Sugam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vanthu Soozhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vanthu Soozhuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Paarvai Ondril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Paarvai Ondril "/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vizhugindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vizhugindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaiyo Thaalattu Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyo Thaalattu Paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Karai Sernthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Karai Sernthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Nee Unnai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Nee Unnai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai uruvanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai uruvanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Sonnaal Verenna Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Sonnaal Verenna Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Moochil Undhan Swasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Moochil Undhan Swasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukullae Nee Vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae Nee Vizhundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Konjum Korththu Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Konjum Korththu Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothumae Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumae Pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Bantham Ondru Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Bantham Ondru Pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumae Vaazhumae Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumae Vaazhumae Vaazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Jenmam Endhan Jeevan Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Jenmam Endhan Jeevan Vaazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumae"/>
</div>
</pre>
