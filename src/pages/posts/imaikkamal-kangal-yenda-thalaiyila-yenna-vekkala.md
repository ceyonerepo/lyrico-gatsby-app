---
title: "imaikkamal kangal song lyrics"
album: "Yenda Thalaiyila Yenna Vekkala"
artist: "A.R. Reihana"
lyricist: "Thava"
director: "Vignesh Karthick"
path: "/albums/yenda-thalaiyila-yenna-vekkala-song-lyrics"
song: "Imaikkamal Kangal"
image: ../../images/albumart/yenda-thalaiyila-yenna-vekkala.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E2PP3B36zEE"
type: "love"
singers:
  - Abhay Jodhpurkar
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">imaikaamal kangal nindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikaamal kangal nindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thano sontham endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thano sontham endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumarum kangal rendile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumarum kangal rendile"/>
</div>
<div class="lyrico-lyrics-wrapper">avan paarvai maiyal kondathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan paarvai maiyal kondathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirpaaramal inainthathu nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirpaaramal inainthathu nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir ulla varai unnidam thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir ulla varai unnidam thanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini naan pogum paathai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini naan pogum paathai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai neengamal thunai vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai neengamal thunai vara vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaikaamal kangal nindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikaamal kangal nindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thano sontham endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thano sontham endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumarum kangal rendile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumarum kangal rendile"/>
</div>
<div class="lyrico-lyrics-wrapper">avan paarvai maiyal kondathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan paarvai maiyal kondathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aval siripinil malai yena aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval siripinil malai yena aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada da athil nanaigiren nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada da athil nanaigiren nane"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan parvaiyil oru nilavu aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan parvaiyil oru nilavu aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">un velichathil vaalthiduvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un velichathil vaalthiduvene"/>
</div>
<div class="lyrico-lyrics-wrapper">konja neram ithamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja neram ithamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">un madiyil idam kudu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madiyil idam kudu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">iravendrum pagalendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravendrum pagalendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyamal or nilai vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyamal or nilai vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaikaamal kangal nindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikaamal kangal nindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thano sontham endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thano sontham endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumarum kangal rendile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumarum kangal rendile"/>
</div>
<div class="lyrico-lyrics-wrapper">avan paarvai maiyal kondathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan paarvai maiyal kondathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thol saainthu kadha solla vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol saainthu kadha solla vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavodu karainthida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavodu karainthida vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinthalum aruginil vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinthalum aruginil vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">mulu naalum thodarnthida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulu naalum thodarnthida vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam pirakuthu ennodu engu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam pirakuthu ennodu engu"/>
</div>
<div class="lyrico-lyrics-wrapper">sendru marainthida naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendru marainthida naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">maru jenman eduthalum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru jenman eduthalum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">serum varam naan ketpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serum varam naan ketpene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaikaamal kangal nindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikaamal kangal nindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thano sontham endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thano sontham endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumarum kangal rendile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumarum kangal rendile"/>
</div>
<div class="lyrico-lyrics-wrapper">athil paarvai maiyal kondathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil paarvai maiyal kondathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethirpaaramal inainthathu nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirpaaramal inainthathu nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir ulla varai unnidam thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir ulla varai unnidam thanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini naan pogum paathai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini naan pogum paathai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai neengamal thunai vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai neengamal thunai vara vendum"/>
</div>
</pre>
