---
title: "nakentho nachinde song lyrics"
album: "Nuvvu Thopu Raa"
artist: "Suresh Bobbili"
lyricist: "Kasarla Shyam"
director: "B. Harinath Babu "
path: "/albums/nuvvu-thopu-raa-lyrics"
song: "Nakentho Nachinde"
image: ../../images/albumart/nuvvu-thopu-raa.jpg
date: 2019-05-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ug4pN1obvUo"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hey nakentho nalhinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey nakentho nalhinde"/>
</div>
<div class="lyrico-lyrics-wrapper">neelaga unna needa kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelaga unna needa kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">naaventa vachinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaventa vachinde"/>
</div>
<div class="lyrico-lyrics-wrapper">ye chota nannu veedakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye chota nannu veedakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee navvu jallu  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee navvu jallu  "/>
</div>
<div class="lyrico-lyrics-wrapper">naapai raale puvvulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naapai raale puvvulla"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vella daari musthaabayyelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vella daari musthaabayyelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa ooha rivvumantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa ooha rivvumantu"/>
</div>
<div class="lyrico-lyrics-wrapper">eaire guvvalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eaire guvvalla"/>
</div>
<div class="lyrico-lyrics-wrapper">neevaipe saagegaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neevaipe saagegaa "/>
</div>
<div class="lyrico-lyrics-wrapper">o pilla nee valla ne malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o pilla nee valla ne malli"/>
</div>
<div class="lyrico-lyrics-wrapper">malli pudutunna kshanamoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli pudutunna kshanamoka"/>
</div>
<div class="lyrico-lyrics-wrapper">janmagaa nee kalla vaakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janmagaa nee kalla vaakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukalle jaari dooram inaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukalle jaari dooram inaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nimishame thalavagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimishame thalavagaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oka choopu oka maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka choopu oka maata"/>
</div>
<div class="lyrico-lyrics-wrapper">oke adugulai ela kaliseno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oke adugulai ela kaliseno"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve cheragaa edo varamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve cheragaa edo varamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jathagaa thirigina rojuloo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jathagaa thirigina rojuloo "/>
</div>
<div class="lyrico-lyrics-wrapper">nee gnapakam manasu thaakithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gnapakam manasu thaakithe"/>
</div>
<div class="lyrico-lyrics-wrapper">rangulenno challenanta kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangulenno challenanta kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ela ninnu kasirinado hrudayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela ninnu kasirinado hrudayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o pilla nee valla ne malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o pilla nee valla ne malli"/>
</div>
<div class="lyrico-lyrics-wrapper">malli pudutunna kshanamoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli pudutunna kshanamoka"/>
</div>
<div class="lyrico-lyrics-wrapper">janmagaa nee kalla vaakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janmagaa nee kalla vaakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukalle jaari dooram inaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukalle jaari dooram inaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nimishame thalavagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimishame thalavagaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oo chiguraasa visiresaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo chiguraasa visiresaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve nenani nijam teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve nenani nijam teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">kale kaadani kalatha niduralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kale kaadani kalatha niduralo"/>
</div>
<div class="lyrico-lyrics-wrapper">vethikesthunna velugulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethikesthunna velugulo"/>
</div>
<div class="lyrico-lyrics-wrapper">aakasame leni nelani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakasame leni nelani"/>
</div>
<div class="lyrico-lyrics-wrapper">o vinthaga choosenanta lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o vinthaga choosenanta lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">gaalai ninnu chutteyenaa udayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaalai ninnu chutteyenaa udayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">o pilla nee valla ne malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o pilla nee valla ne malli"/>
</div>
<div class="lyrico-lyrics-wrapper">malli pudutunna kshanamoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli pudutunna kshanamoka"/>
</div>
<div class="lyrico-lyrics-wrapper">janmagaa nee kalla vaakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janmagaa nee kalla vaakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukalle jaari dooram inaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukalle jaari dooram inaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nimishame thalavagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimishame thalavagaa "/>
</div>
</pre>
