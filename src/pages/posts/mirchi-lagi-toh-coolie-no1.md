---
title: "mirchi lagi toh song lyrics"
album: "Coolie No1"
artist: "Lijo George - DJ Chetas - Anand-Milind"
lyricist: "Sameer"
director: "David Dhawan"
path: "/albums/coolie-no1-lyrics"
song: "Mirchi Lagi toh"
image: ../../images/albumart/coolie-no1.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/EQlYzDdOLxI"
type: "love"
singers:
  - Kumar Sanu
  - Alka Yagnik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">back to the 90's
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="back to the 90's"/>
</div>
<div class="lyrico-lyrics-wrapper">arey wah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey wah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">main toh raste se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh raste se"/>
</div>
<div class="lyrico-lyrics-wrapper">ja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh bhel puri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh bhel puri"/>
</div>
<div class="lyrico-lyrics-wrapper">kha raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kha raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh ladki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh ladki"/>
</div>
<div class="lyrico-lyrics-wrapper">ghuma raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghuma raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">ho raste se ja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho raste se ja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">bhel puri kha raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhel puri kha raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">ladki ghuma raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladki ghuma raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha tha tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">toh main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toh main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">toh main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toh main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh raste se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh raste se"/>
</div>
<div class="lyrico-lyrics-wrapper">ja rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ja rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh ice-cream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh ice-cream"/>
</div>
<div class="lyrico-lyrics-wrapper">kha rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kha rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">oh main toh naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh main toh naina"/>
</div>
<div class="lyrico-lyrics-wrapper">lada rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lada rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">raste se ja rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raste se ja rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">ice-cream kha rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice-cream kha rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">naina lada rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naina lada rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">thi thi thi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thi thi thi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">wo puraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wo puraane"/>
</div>
<div class="lyrico-lyrics-wrapper">yaadgaar geet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaadgaar geet"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jale chahe saara zamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jale chahe saara zamana"/>
</div>
<div class="lyrico-lyrics-wrapper">chahe tujhe tera deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chahe tujhe tera deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">jale chahe saara zamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jale chahe saara zamana"/>
</div>
<div class="lyrico-lyrics-wrapper">chahe tujhe tera deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chahe tujhe tera deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">sang tere main bhag jaaun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sang tere main bhag jaaun"/>
</div>
<div class="lyrico-lyrics-wrapper">nazar kisi ko bhi na aaun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nazar kisi ko bhi na aaun"/>
</div>
<div class="lyrico-lyrics-wrapper">arey log dilwalon se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey log dilwalon se"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar jalte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar jalte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaise bataun kya kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaise bataun kya kya"/>
</div>
<div class="lyrico-lyrics-wrapper">chal chalte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chal chalte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">ae ae ae ae haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae ae ae ae haye"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh gaadi se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh gaadi se"/>
</div>
<div class="lyrico-lyrics-wrapper">ja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh seeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh seeti"/>
</div>
<div class="lyrico-lyrics-wrapper">baja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh topi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh topi"/>
</div>
<div class="lyrico-lyrics-wrapper">phira raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phira raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">are gaadi se ja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="are gaadi se ja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">seeti baja raha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeti baja raha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">topi phira raha tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="topi phira raha tha "/>
</div>
<div class="lyrico-lyrics-wrapper">tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha tha tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko dhakka laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko dhakka laga"/>
</div>
<div class="lyrico-lyrics-wrapper">toh main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toh main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko dhakka laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko dhakka laga"/>
</div>
<div class="lyrico-lyrics-wrapper">toh main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toh main kya karoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hello?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hello?"/>
</div>
<div class="lyrico-lyrics-wrapper">aap jis number ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aap jis number ko"/>
</div>
<div class="lyrico-lyrics-wrapper">call kar rahein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="call kar rahein hai"/>
</div>
<div class="lyrico-lyrics-wrapper">wo abhi pyaar mein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wo abhi pyaar mein hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nayi koyi picture dikha de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayi koyi picture dikha de"/>
</div>
<div class="lyrico-lyrics-wrapper">mujhe kahin khana khila de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mujhe kahin khana khila de"/>
</div>
<div class="lyrico-lyrics-wrapper">nayi koyi picture dikha de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayi koyi picture dikha de"/>
</div>
<div class="lyrico-lyrics-wrapper">mujhe kahin khana khila de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mujhe kahin khana khila de"/>
</div>
<div class="lyrico-lyrics-wrapper">zara nigahon se pila de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zara nigahon se pila de"/>
</div>
<div class="lyrico-lyrics-wrapper">pyaas mere dil ki bujha de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pyaas mere dil ki bujha de"/>
</div>
<div class="lyrico-lyrics-wrapper">aaj tujhe jee bharke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaj tujhe jee bharke"/>
</div>
<div class="lyrico-lyrics-wrapper">pyar karna hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pyar karna hai"/>
</div>
<div class="lyrico-lyrics-wrapper">teri nigahon se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teri nigahon se"/>
</div>
<div class="lyrico-lyrics-wrapper">deedaar karna hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deedaar karna hai"/>
</div>
<div class="lyrico-lyrics-wrapper">ae ae ae ae haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae ae ae ae haye"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh thumka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh thumka "/>
</div>
<div class="lyrico-lyrics-wrapper">laga rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laga rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">main toh geet koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main toh geet koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">gaa rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaa rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">ho main toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho main toh"/>
</div>
<div class="lyrico-lyrics-wrapper">chakkar chala rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chakkar chala rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">thumka laga rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thumka laga rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">geet koyi gaa rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geet koyi gaa rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">chakkar chala rahi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chakkar chala rahi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">thi thi thi thi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thi thi thi thi"/>
</div>
<div class="lyrico-lyrics-wrapper">teri nani mari toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teri nani mari toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">teri nani mari toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teri nani mari toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">aye coolie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye coolie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">teri nani mari toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teri nani mari toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko mirchi lagi toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko mirchi lagi toh"/>
</div>
<div class="lyrico-lyrics-wrapper">main kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kya karoon"/>
</div>
</pre>
