---
title: 'friendship anthem song lyrics'
album: 'Oh My Kadavule'
artist: 'Leon James'
lyricist: 'Ko Sesha'
director: 'Ashwath Marimuthu'
path: '/albums/oh-my-kadavule-song-lyrics'
song: 'Friendship Anthem'
image: ../../images/albumart/oh-my-kadavule.jpg
date: 2020-02-14
lang: tamil
singers: 
- Anirudh Ravichander Ravichander
- Leon James
- M M Manasi
youtubeLink: 'https://www.youtube.com/embed/iYoLVTsJvWY'
type: 'friendship'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Natpukku vayadhillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Natpukku vayadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru oru gnyaani
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru oru gnyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaanae oh sonnaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sonnaanae oh sonnaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaana natpukingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Meiyaana natpukingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivillai endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirivillai endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaanae oh sonnaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sonnaanae oh sonnaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Almost diaper kattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Almost diaper kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thodangi naamum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam thodangi naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhgal   Urasi nadanthom
<input type="checkbox" class="lyrico-select-lyric-line" value="Thozhgal   Urasi nadanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kusthi fight-um pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kusthi fight-um pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai mustafa-vum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraai mustafa-vum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi   Vaazhvai kadanthom
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadi   Vaazhvai kadanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naam ezhil nadhigalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam ezhil nadhigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadiya oodam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadiya oodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatraadha kadal indha ragasiya paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vatraadha kadal indha ragasiya paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkul irundhadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namakkul irundhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai veli vesham thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Illai veli vesham thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku…uu…uu…uu..
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku…uu…uu…uu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram uravu vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram uravu vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban thaan geththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanban thaan geththu namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hu hu hu hu hu hu…hoo hoo hoo hooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hu hu hu hu hu hu…hoo hoo hoo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hu hu hu hu hu hu…hoo hoo hoo hooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hu hu hu hu hu hu…hoo hoo hoo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hu hu hu hu hu hu…hoo hoo hoo hooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hu hu hu hu hu hu…hoo hoo hoo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hu hu hu hu hu hu…hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hu hu hu hu hu hu…hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Collegeu cut adichi thala padam ponom
<input type="checkbox" class="lyrico-select-lyric-line" value="Collegeu cut adichi thala padam ponom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ponom…ponom
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponom…ponom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Exam-il bit adichum justu failu aanom
<input type="checkbox" class="lyrico-select-lyric-line" value="Exam-il bit adichum justu failu aanom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aanom…aanom
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanom…aanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sothil kooda pangu undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sothil kooda pangu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samosa-vil pangillaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Samosa-vil pangillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapadam edhumilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalapadam edhumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam andha kaalam thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam andha kaalam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku..uu…uu…uuu…uu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku..uu…uu…uuu…uu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku…uu…uu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku…uu…uu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram uravu vandhaalum…mm…mm…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram uravu vandhaalum…mm…mm…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban thaan geththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanban thaan geththu namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Natpukku vayadhillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Natpukku vayadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru oru gnyaani
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru oru gnyaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaanae oh sonnaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sonnaanae oh sonnaanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Meiyaana natpukingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Meiyaana natpukingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivillai endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirivillai endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaanae oh sonnaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sonnaanae oh sonnaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kalla dham adichi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalla dham adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootta sight adichi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootta sight adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike-il suttri thirindhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Bike-il suttri thirindhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Motta maadi mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Motta maadi mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti kadhai adichi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti kadhai adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi aandu kalithom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi aandu kalithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All  Naam ezhil nadhigalil
<input type="checkbox" class="lyrico-select-lyric-line" value="All  Naam ezhil nadhigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadiya oodam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadiya oodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatraadha kadal indha ragasiya paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vatraadha kadal indha ragasiya paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkul irundhadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namakkul irundhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai veli vesham thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Illai veli vesham thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku..uu…uu…uuu…uu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku..uu…uu…uuu…uu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku…uu…uu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku…uu…uu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram uravu vandhaalum…mm…mm…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram uravu vandhaalum…mm…mm…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban thaan geththu namakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanban thaan geththu namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Friendship thaan soththu namakku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Friendship thaan soththu namakku…"/>
</div>
</pre>