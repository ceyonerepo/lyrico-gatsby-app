---
title: 'thappa dhan theriyum song lyrics'
album: 'Maari'
artist: 'Anirudh Ravichander'
lyricist: 'Vignesh Shivan'
director: 'Balaji Mohan'
path: '/albums/maari-song-lyrics'
song: 'Thappa Dhan Theriyum'
image: ../../images/albumart/Maari.jpg
date: 2015-07-17
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/OViH68fJUhM'
type: 'philosophy'
singers: 
- Dhanush
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thappa dhaan theriyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappa dhaan theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma route
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma route"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyana payalaa get outu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sariyana payalaa get outu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham wrongathan irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koncham wrongathan irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaana rightu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaana rightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadha varaikkum apeettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puriyaadha varaikkum apeettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Intha ooroda light-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha ooroda light-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari the great-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari the great-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Police adichathu da salute-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Police adichathu da salute-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee irunthathaan correct-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee irunthathaan correct-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Illana reveett-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Illana reveett-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimeluthaan naala neram isstart-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimeluthaan naala neram isstart-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Guniyumbothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Guniyumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthum oorukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuthum oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Niminthethaan nadakanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Niminthethaan nadakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyae illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai thedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Venai thaedum olagathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Venai thaedum olagathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyavae irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyavae irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappae illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Summa ethachum
<input type="checkbox" class="lyrico-select-lyric-line" value="Summa ethachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollatha kannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollatha kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae irukkum ellorum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae irukkum ellorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum onnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppothum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku rite-nnu pattatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku rite-nnu pattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum pannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattum pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo vanthu en vootla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ippo vanthu en vootla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayirara thunnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vayirara thunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniya vanthen thaniya poven
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya vanthen thaniya poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha bantham theva illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sontha bantham theva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaena pesi thaana vantha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaena pesi thaana vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena pogum yenda tholla
<input type="checkbox" class="lyrico-select-lyric-line" value="Veena pogum yenda tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thappa dhaan theriyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappa dhaan theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma route
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma route"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyana payalaa get outu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sariyana payalaa get outu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham wrongathan irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koncham wrongathan irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaana rightu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaana rightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadha varaikkum apeettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puriyaadha varaikkum apeettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Intha ooroda light-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha ooroda light-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari the great-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Maari the great-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Police adichathu da salute-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Police adichathu da salute-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee irunthathaan correct-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee irunthathaan correct-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Illana reveett-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Illana reveett-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimeluthaan naala neram isstart-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimeluthaan naala neram isstart-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaah haaah haaah haa haa hahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaah haaah haaah haa haa hahaha"/>
</div>
</pre>