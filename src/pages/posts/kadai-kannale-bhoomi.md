---
title: 'Kadai Kannaaley lyrics'
album: 'Bhoomi'
artist: 'D Imman'
lyricist: 'Thamarai'
director: 'Lakshman'
path: '/albums/bhoomi-song-lyrics'
song: 'Kadai Kannaaley'
image: ../../images/albumart/bhoomi.jpg
date: 2020-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MFfaonhrxqY"
type: 'love'
singers: 
- Shreya Ghoshal
- Varun Parandhaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Annalum nookinaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Annalum nookinaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalum nookinaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Avalum nookinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha nodi yugamaai neela
<input type="checkbox" class="lyrico-select-lyric-line" value="Paartha nodi yugamaai neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaipadhaithu kann thirappen
<input type="checkbox" class="lyrico-select-lyric-line" value="Padhaipadhaithu kann thirappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnum aatraamaiyaal allal uttru
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinnum aatraamaiyaal allal uttru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavan veesinaal kadai kannaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavan veesinaal kadai kannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadai kannaalae rasithaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadai kannaalae rasithaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinpoovae kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavinpoovae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaikulaadum mazhaikaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudaikulaadum mazhaikaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir parthean innaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhir parthean innaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadai kannaalae rasithaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadai kannaalae rasithaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinpoovae kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavinpoovae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaikulaadum mazhaikaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudaikulaadum mazhaikaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir parthean innaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhir parthean innaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tharai nillaa nillaa iru kaalodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharai nillaa nillaa iru kaalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai kollaa kollaa alai neeroodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karai kollaa kollaa alai neeroodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangaamalae thodarum unn nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangaamalae thodarum unn nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thedalil idarum un poo mugam
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir thedalil idarum un poo mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannalanae….kannalanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannalanae….kannalanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidamae… en manamae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidamae… en manamae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadai kannaalae rasitheanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadai kannaalae rasitheanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinpoovae kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavinpoovae kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Annalum nokinaan…avalum nokinaaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Annalum nokinaan…avalum nokinaaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Olinthaen marainthaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Olinthaen marainthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai paarthum naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhai paarthum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai serntha pinbu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai serntha pinbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayam neenginaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhayam neenginaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Padaranthaen alainthaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Padaranthaen alainthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi pola naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi pola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani maarpil saaindhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mani maarpil saaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar serkkiraen
<input type="checkbox" class="lyrico-select-lyric-line" value="Malar serkkiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vizhiyai imaiyai virithaen unai en
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhiyai imaiyai virithaen unai en"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyin aranmanai varaverkkuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilamaiyin aranmanai varaverkkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Viralai nagaththai kadithae ezhuthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Viralai nagaththai kadithae ezhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaiyai idhalgalum arangettruthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavithaiyai idhalgalum arangettruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Indha nodi podhum thaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha nodi podhum thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhi urainthaenae naanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sindhi urainthaenae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalum uyirum mezhugaai urugum
<input type="checkbox" class="lyrico-select-lyric-line" value="Udalum uyirum mezhugaai urugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannalanae….kannalanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannalanae….kannalanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidamae… en manamae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidamae… en manamae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadai kannaalae rasithaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadai kannaalae rasithaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinpoovae kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavinpoovae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaikulaadum mazhaikaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudaikulaadum mazhaikaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir parthean innaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhir parthean innaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tharai nillaa nillaa iru kaalodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharai nillaa nillaa iru kaalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai kollaa kollaa alai neeroodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karai kollaa kollaa alai neeroodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangaamalae thodarum unn nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Urangaamalae thodarum unn nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thedalil idarum un poo mugam
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir thedalil idarum un poo mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannalanae….kannalanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannalanae….kannalanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidamae… en manamae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidamae… en manamae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadai kannaalae rasithaenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadai kannaalae rasithaenae"/>
</div>
</pre>