---
title: "vilayadu mankatha song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Vilayadu Mankatha"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fhdLTvl9a74"
type: "mass"
singers:
  - Yuvan Shankar Raja
  - Ranjith
  - Anitha Karthikeyan
  - Premgi Amaren
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadava Arangetri Pada Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadava Arangetri Pada Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaargal Koodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaargal Koodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Pottu Thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Pottu Thedavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomiyil Puthithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Puthithaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhane Pugalkoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhane Pugalkoorum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seedane Ne Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seedane Ne Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheerane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathinai Maatrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathinai Maatrada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magizhchiyai Yetrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhchiyai Yetrada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuraigalai Neekada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraigalai Neekada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thdaigalai Thookki Potu Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thdaigalai Thookki Potu Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalukkul Neruppada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukkul Neruppada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvugal Kothipada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvugal Kothipada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vidhi Ezhuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vidhi Ezhuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yea Yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Yea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puratchi Seithu Katta Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratchi Seithu Katta Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theenda Vaa Ennai Thottu Thoondavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda Vaa Ennai Thottu Thoondavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Thannai Thaandavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thannai Thaandavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunai Aanai Aandava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Aanai Aandava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulu Moga Thoothuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulu Moga Thoothuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam Jothi Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Jothi Allava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhi Indri Solla Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Indri Solla Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthi Enbadhe Sakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthi Enbadhe Sakthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enbathai Katrukollada En Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathai Katrukollada En Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakthi Enbathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakthi Enbathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhilil Vaithu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhilil Vaithu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nitham Vetridhan En Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitham Vetridhan En Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Puthukural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Puthukural"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirukural  Dhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirukural  Dhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhai Purinthapin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai Purinthapin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaiyethu Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyethu Munne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Porupinai Etru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Porupinai Etru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu Pani Aatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Pani Aatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Vendum Mele Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Vendum Mele Munneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithanai Vizhikka Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai Vizhikka Vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivinai Thuvaithu Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivinai Thuvaithu Vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavinai Jeikavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinai Jeikavai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavanathai Thozhilil Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanathai Thozhilil Vaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravinai Perukivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinai Perukivai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyarvinil Paninthu Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarvinil Paninthu Vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyai Nilaikka Vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyai Nilaikka Vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathai Thirumbi Parka Vai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai Thirumbi Parka Vai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilayadu Mankatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayadu Mankatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidamata Enkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamata Enkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velivesam Podaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesam Podaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vetri Kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vetri Kita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaadha"/>
</div>
</pre>
