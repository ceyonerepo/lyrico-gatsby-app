---
title: "kannamma kanvizhi song lyrics"
album: "Ratsasan"
artist: "Ghibran"
lyricist: "Ram kumar"
director: "Ram Kumar"
path: "/albums/ratsasan-lyrics"
song: "Kannamma Kanvizhi"
image: ../../images/albumart/ratsasan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N7RwcoLGOk4"
type: "sad"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamma kanvizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kanvizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thaan en mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thaan en mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illa vaanathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa vaanathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam thaan en vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam thaan en vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu irupaai angu varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu irupaai angu varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru enna vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru enna vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma kanvizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kanvizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thaan en mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thaan en mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illa vaanathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa vaanathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam thaan en vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam thaan en vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu irupaai angu varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu irupaai angu varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru enna vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru enna vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangi sellum veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi sellum veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venn iragil katti vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venn iragil katti vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indraanathu irul vaazhum kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indraanathu irul vaazhum kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai soozhntha gangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai soozhntha gangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Annadhiyil moochadaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annadhiyil moochadaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhkai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhkai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aaven kaatrizhandha yakkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aaven kaatrizhandha yakkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu kanavaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu kanavaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena dhinam dhinam vizhipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena dhinam dhinam vizhipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu varai ivvazhvu adarvanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu varai ivvazhvu adarvanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini eppothu ippbhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini eppothu ippbhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir perumo sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir perumo sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma kanvizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kanvizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thaan en mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thaan en mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illa vaanathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa vaanathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam thaan en vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam thaan en vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu irupaai angu varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu irupaai angu varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru enna vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru enna vendum"/>
</div>
</pre>
