---
title: "mun andhi song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Na. Muthukumar"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Mun Andhi"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7fFFbFp8nOg"
type: "love"
singers:
  - Karthik
  - Megha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mun Andhi Chaaral Nee Mun Jenma Thedal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Andhi Chaaral Nee Mun Jenma Thedal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoongum Nerathil Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoongum Nerathil Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Paadal Nee Poo Pootha Saalai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Paadal Nee Poo Pootha Saalai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha Kaalai Nee Vidinthaalum Thookathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha Kaalai Nee Vidinthaalum Thookathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyorathil Varum Kanavu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyorathil Varum Kanavu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaal Ulle Ulle Uruguthu Nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaal Ulle Ulle Uruguthu Nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaai Inbam Solla Vaarthaigal Konjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaai Inbam Solla Vaarthaigal Konjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mun Andhi Chaaral Nee Mun Jenma Thedal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Andhi Chaaral Nee Mun Jenma Thedal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoongum Nerathil Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoongum Nerathil Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Paadal Nee Poo Pootha Saalai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Paadal Nee Poo Pootha Saalai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha Kaalai Nee Vidinthalum Thookathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha Kaalai Nee Vidinthalum Thookathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyorathil Varum Kanavu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyorathil Varum Kanavu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Azhagey Oh Imai Azhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Azhagey Oh Imai Azhagey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea Kalainthaalum Unthan Koondhal Oar Azhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Kalainthaalum Unthan Koondhal Oar Azhagey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthaalum Undhan Nizhalum Perazhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaalum Undhan Nizhalum Perazhagey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Unnai Theendathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Unnai Theendathaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Thaagam Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Thaagam Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyaai Thuvaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyaai Thuvaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Unnai Thotta Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Unnai Thotta Pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagam Theernthathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theernthathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil Seraatho Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil Seraatho Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaal Ulle Ulle Uruguthu Nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaal Ulle Ulle Uruguthu Nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Ho Ho Vaa Vaa Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ho Ho Vaa Vaa Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaai Inbam Solla Vaarthaigal Konjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaai Inbam Solla Vaarthaigal Konjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaalai Oh Andhi Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai Oh Andhi Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mm Unnai Thedi Paarka Solli Poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mm Unnai Thedi Paarka Solli Poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Pinbe Enthan Naal Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Pinbe Enthan Naal Odum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Pambarathai Pole Ennai Sutra Vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Pambarathai Pole Ennai Sutra Vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Nillaamal Dhinam Antharathin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Nillaamal Dhinam Antharathin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thoonga Vaithaai Kadhal Sollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thoonga Vaithaai Kadhal Sollamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaal Ulle Ulle Uruguthu Nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaal Ulle Ulle Uruguthu Nenjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Penne Penne Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Penne Penne Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Munne Munne Munne Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Munne Munne Munne Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaai Inbam Solla Vaarthaigal Konjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaai Inbam Solla Vaarthaigal Konjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mun Andhi Chaaral Nee Mun Jenma Thedal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Andhi Chaaral Nee Mun Jenma Thedal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoongum Nerathil Tholai Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoongum Nerathil Tholai Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Paadal Nee Poo Pootha Saalai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Paadal Nee Poo Pootha Saalai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha Kaalai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha Kaalai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinthalum Thookathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinthalum Thookathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyorathil Varum Kanavu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyorathil Varum Kanavu Nee"/>
</div>
</pre>
