---
title: "komuram beemano song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Komuram Beemano"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VyXiuDPlSLg"
type: "melody"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bheema usuru thandha nila thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheema usuru thandha nila thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moocha kudutha kaattu maranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moocha kudutha kaattu maranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vacha goondu jaadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vacha goondu jaadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda pesurangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda pesurangada"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu unakku ketkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu unakku ketkudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondralum un kaalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondralum un kaalil "/>
</div>
<div class="lyrico-lyrics-wrapper">mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandi iduvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandi iduvaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivaana chooriyanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaana chooriyanai "/>
</div>
<div class="lyrico-lyrics-wrapper">tharaiyil vizhuvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaiyil vizhuvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil vizhuvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil vizhuvaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan pondra maanathinai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan pondra maanathinai "/>
</div>
<div class="lyrico-lyrics-wrapper">un kaalil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaalil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaduyinam naam nanmaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaduyinam naam nanmaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai aavaano pullai aavaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai aavaano pullai aavaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru mumtham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mumtham "/>
</div>
<div class="lyrico-lyrics-wrapper">erikuralai kondranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erikuralai kondranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanal oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanal oru "/>
</div>
<div class="lyrico-lyrics-wrapper">miththaayin mahanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miththaayin mahanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai kolvaano 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai kolvaano "/>
</div>
<div class="lyrico-lyrics-wrapper">peyarai kolvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyarai kolvaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondralum un kaalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondralum un kaalil "/>
</div>
<div class="lyrico-lyrics-wrapper">mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandi iduvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandi iduvaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanmel pozhiyum inmazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanmel pozhiyum inmazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">thumbam yendraanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thumbam yendraanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidhari raththamum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidhari raththamum "/>
</div>
<div class="lyrico-lyrics-wrapper">dhoorida sirikkathu ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorida sirikkathu ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyendru kanneero 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyendru kanneero "/>
</div>
<div class="lyrico-lyrics-wrapper">vezhiyeri veezhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vezhiyeri veezhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi thaai thaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi thaai thaai "/>
</div>
<div class="lyrico-lyrics-wrapper">paalai undaan enbaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalai undaan enbaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaan enbaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaan enbaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondralum un kaalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondralum un kaalil "/>
</div>
<div class="lyrico-lyrics-wrapper">mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandi iduvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandi iduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandi iduvaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarondrai polae than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarondrai polae than "/>
</div>
<div class="lyrico-lyrics-wrapper">kurudhi kandaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurudhi kandaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarondrai polae than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarondrai polae than "/>
</div>
<div class="lyrico-lyrics-wrapper">kurudhi kandaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurudhi kandaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai mannin udhalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai mannin udhalil "/>
</div>
<div class="lyrico-lyrics-wrapper">pottaai aagindraano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottaai aagindraano"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma kaalil marudhaniyaindraano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kaalil marudhaniyaindraano"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin punnagai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin punnagai "/>
</div>
<div class="lyrico-lyrics-wrapper">chevvai minnal endravaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chevvai minnal endravaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi thaaiku than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi thaaiku than "/>
</div>
<div class="lyrico-lyrics-wrapper">udambai thiruppi tharuvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambai thiruppi tharuvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Komuram bheemano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komuram bheemano"/>
</div>
</pre>
