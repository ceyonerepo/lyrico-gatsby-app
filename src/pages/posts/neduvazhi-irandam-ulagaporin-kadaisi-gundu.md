---
title: "neduvazhi song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Muthuvel"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Neduvazhi"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-Yq_DvbhyMw"
type: "sad"
singers:
  - Vignesh Iswar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neduvazhi Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvazhi Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil Anainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil Anainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Pirinthenae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pirinthenae Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suduvazhi Manalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suduvazhi Manalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthidum Uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthidum Uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan Vanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Vanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravu Bandham Ellam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu Bandham Ellam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Dhaanae Vandhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Dhaanae Vandhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbu Idhayam Ithanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu Idhayam Ithanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhanappa Thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhanappa Thanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvazhi Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvazhi Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil Anainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil Anainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Pirinthenae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pirinthenae Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadaruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadaruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vantha Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vantha Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Maraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Maraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam Kalainthu Thaediya Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam Kalainthu Thaediya Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thavippu Marakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thavippu Marakala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panangaavil Lorry Senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panangaavil Lorry Senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaa Paasam Marakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaa Paasam Marakala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodathil Nee Vittu Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodathil Nee Vittu Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Serikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Serikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravu Bandham Ellam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu Bandham Ellam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Dhaanae Vandhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Dhaanae Vandhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbu Idhayam Ithanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu Idhayam Ithanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhanappa Thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhanappa Thanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvazhi Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvazhi Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil Anainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil Anainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Pirinthenae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pirinthenae Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illanu Innum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illanu Innum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaa Nambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa Nambala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Seruppula Potha Mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Seruppula Potha Mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Poda Manasilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Poda Manasilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soreduthu Nee Vandha Vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soreduthu Nee Vandha Vellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappil Dhaanae Methakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappil Dhaanae Methakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaraya Mutham Tholayaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaraya Mutham Tholayaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Mookkil Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Mookkil Nikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravu Bandham Ellam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu Bandham Ellam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Dhaanae Vandhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Dhaanae Vandhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbu Idhayam Ithanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu Idhayam Ithanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhanappa Thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhanappa Thanthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduvazhi Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvazhi Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil Anainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil Anainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Pirinthenae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pirinthenae Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suduvazhi Manalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suduvazhi Manalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthidum Uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthidum Uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan Vanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Vanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thunaiyaaga"/>
</div>
</pre>
