---
title: "mehabooba song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Ramajogayya Sastry"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Mehabooba"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/V4omWx5yEZk"
type: "melody"
singers:
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mande gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirujallai vastunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirujallai vastunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindu kaugililo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindu kaugililo"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumallelu pustunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumallelu pustunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey alajadi velanaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey alajadi velanaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Talanimire chelinai lena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talanimire chelinai lena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee alasata tircalena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee alasata tircalena"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa mamatala odilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa mamatala odilona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba O main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba O main teri mehabooba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chanuvaina vennello challarani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanuvaina vennello challarani"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalaina davanalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalaina davanalam"/>
</div>
<div class="lyrico-lyrics-wrapper">uppenai egasina swasa pavanalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppenai egasina swasa pavanalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jata kavali andala cheli parimalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jata kavali andala cheli parimalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Replay muyani vippu kanudoyiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Replay muyani vippu kanudoyiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Iali padali paruvala gamadavanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iali padali paruvala gamadavanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viradhi veerudivaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viradhi veerudivaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasivadiga ninnu chustunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasivadiga ninnu chustunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ekantala velite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ekantala velite"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorista ikapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorista ikapaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba Main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba Main teri mehabooba"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehabooba O main teri mehabooba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehabooba O main teri mehabooba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">huhu hu Hm hu hu hu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="huhu hu Hm hu hu hu"/>
</div>
<div class="lyrico-lyrics-wrapper">huhu hu Hm  uhumm humm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="huhu hu Hm  uhumm humm"/>
</div>
</pre>
