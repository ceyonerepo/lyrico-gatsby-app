---
title: "petthaapuram song lyrics"
album: "RK Nagar"
artist: "Premgi Amaren"
lyricist: "Karpaga Rajan"
director: "Saravana Rajan"
path: "/albums/rk-nagar-lyrics"
song: "Petthaapuram"
image: ../../images/albumart/rk-nagar.jpg
date: 2019-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TLpXB6UTxE8"
type: "happy"
singers:
  - Malathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Peththaapuram Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peththaapuram Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thottu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thottu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Rk Nagar Beat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rk Nagar Beat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vittu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vittu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Renda Aayiram Note-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Renda Aayiram Note-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Enni Kodu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Enni Kodu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaikka Porom Treat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaikka Porom Treat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pinni Pudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pinni Pudu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kanna Pinnaa Rate-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanna Pinnaa Rate-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Alli Kudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Alli Kudu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Enna Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Enna Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Sollitharen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Sollitharen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Koovathuru Koota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Koovathuru Koota"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Katti Vaiyen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Katti Vaiyen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Tharmakolu Saet-Ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tharmakolu Saet-Ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Pannipudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Pannipudu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Peththaapuram Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peththaapuram Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thottu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thottu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Rk Nagar Beat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rk Nagar Beat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vittu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vittu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Renda Aayiram Note-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Renda Aayiram Note-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Enni Kodu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Enni Kodu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaikka Porom Treat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaikka Porom Treat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pinni Pudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pinni Pudu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Patta Patti Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Patta Patti Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mitta Miraasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mitta Miraasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kattil Mela Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kattil Mela Beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottu Oraasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottu Oraasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Katchi Karai Vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Katchi Karai Vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Putham Pudhusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Putham Pudhusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ishta Paadi Vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ishta Paadi Vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vittu Velaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vittu Velaasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaasukaaga Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaasukaaga Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Maanam Vikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Maanam Vikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kaasa Vachu Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaasa Vachu Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Ootta Vikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Ootta Vikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Rendum Onnu Thaan Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rendum Onnu Thaan Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Senju Nikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Senju Nikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Paavathoda Paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Paavathoda Paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Paadi Vaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Paadi Vaikkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Peththaapuram Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peththaapuram Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thottu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thottu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Rk Nagar Beat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rk Nagar Beat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vittu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vittu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Renda Aayiram Note-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Renda Aayiram Note-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Enni Kodu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Enni Kodu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaikka Porom Treat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaikka Porom Treat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pinni Pudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pinni Pudu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sittan Jittan Sinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sittan Jittan Sinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Seeni Pattaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Seeni Pattaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kummanguthu Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kummanguthu Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Ellaam Thammasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Ellaam Thammasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thillalangadi Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thillalangadi Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae Onnum Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae Onnum Thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam Kummalamthan Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Kummalamthan Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thotta Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thotta Thappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaalam Maari Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaalam Maari Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Patta Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Patta Thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kettu Pochu Vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kettu Pochu Vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ketta Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ketta Thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vazhkai Vandhu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vazhkai Vandhu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Onnum Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Onnum Thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Onna Sernthu Senjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Onna Sernthu Senjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yethum Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yethum Thappilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Peththaapuram Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peththaapuram Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thottu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thottu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Rk Nagar Beat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rk Nagar Beat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vittu Pudi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vittu Pudi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Renda Aayiram Note-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Renda Aayiram Note-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Enni Kodu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Enni Kodu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaikka Porom Treat-Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaikka Porom Treat-Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pinni Pudu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pinni Pudu Daa"/>
</div>
</pre>
