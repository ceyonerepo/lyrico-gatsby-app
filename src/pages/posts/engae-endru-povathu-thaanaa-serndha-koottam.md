---
title: "engae endru povathu song lyrics"
album: "Thaanaa Serndha Koottam"
artist: "Anirudh Ravichander"
lyricist: "Thamarai"
director: "Vignesh Shivan"
path: "/albums/thaanaa-serndha-koottam-lyrics"
song: "Engae Endru Povathu"
image: ../../images/albumart/thaanaa-serndha-koottam.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U6t4TO--zUE"
type: "sad"
singers:
  - Anirudh Ravichander
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engae endru povadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae endru povadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai solli novadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai solli novadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho konjam vazhumbothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho konjam vazhumbothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotru thotru saavadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotru thotru saavadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratham ketkum peyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham ketkum peyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiri pagalaai maayidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri pagalaai maayidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyvae illai ovvondragaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyvae illai ovvondragaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooru potu kolludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru potu kolludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirappadhae pilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirappadhae pilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enum ili nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enum ili nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai illa naattil thavarudhae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai illa naattil thavarudhae mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam varum pagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam varum pagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolgugindrom setril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolgugindrom setril"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor uyirukku ingae vilai enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor uyirukku ingae vilai enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum kanneer sindhi payan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum kanneer sindhi payan enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam naanum neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam naanum neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karugi pogum nilai enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karugi pogum nilai enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thiramai irundhal podhadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thiramai irundhal podhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam thedi kondu vaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam thedi kondu vaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha lanjam oolal rendum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha lanjam oolal rendum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta vaarthai aagathaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta vaarthai aagathaaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi thedi alaigindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thedi alaigindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga valargindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga valargindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikeezhaai thirigindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikeezhaai thirigindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai theriyaamal thinarugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai theriyaamal thinarugirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi thedi alaigindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thedi alaigindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga valargindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga valargindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikeezhaai thirigindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikeezhaai thirigindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai theriyaamal thinarugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai theriyaamal thinarugirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satchigal maaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satchigal maaruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchigal maaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchigal maaruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Soolnilai maaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soolnilai maaruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Soolchigal maaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soolchigal maaruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini naamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dhaayam veesiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru dhaayam veesiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenii yeranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenii yeranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiri adi vaangi vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiri adi vaangi vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi poganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu valiyaal vaadiya kootamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu valiyaal vaadiya kootamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pudhithaana porattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pudhithaana porattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thedi sertha kootam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thedi sertha kootam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana serndha koottamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana serndha koottamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theedhum nandrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theedhum nandrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae vaazhum oril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae vaazhum oril"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeemai mattum oongi nirkum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeemai mattum oongi nirkum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum kooda kaasai ketkum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum kooda kaasai ketkum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaal enna naamum seiya koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal enna naamum seiya koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu thaana serndha koottamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaana serndha koottamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaana serndha koottamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaana serndha koottamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thedi sertha kootam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thedi sertha kootam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana serndha koottam ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana serndha koottam ada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhu valiyaal vaadiya kootamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhu valiyaal vaadiya kootamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pudhithaana porattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pudhithaana porattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thedi sertha kootam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thedi sertha kootam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana serndha koottamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana serndha koottamada"/>
</div>
</pre>
