---
title: "aayiram pournamigal song lyrics"
album: "Traffic Ramasamy"
artist: "Balamurali Balu"
lyricist: "Pa Vijay"
director: "Vicky"
path: "/albums/traffic-ramasamy-lyrics"
song: "Aayiram Pournamigal"
image: ../../images/albumart/traffic-ramasamy.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/l6uFxy_w1XQ"
type: "happy"
singers:
  - Aishwarya Ravichandran
  - Srinidhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aayiram Pournamigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Pournamigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Jolikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Jolikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaan Pooththupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaan Pooththupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Sirikkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Sirikkithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigal Vizhuntha Vayathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal Vizhuntha Vayathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kavithai Padikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kavithai Padikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellippokkal Kannethirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellippokkal Kannethirey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Thanthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Thanthathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Pournamigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Pournamigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Jolikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Jolikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaan Pooththupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaan Pooththupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Sirikkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Sirikkithey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Ga Ri Ga Ma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ga Ri Ga Ma "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri Ga Ri Ni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Ga Ri Ni "/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ga Ri Ga Ma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ga Ri Ga Ma "/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ga Ri Ga Ma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ga Ri Ga Ma "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri Ga Ri Ni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Ga Ri Ni "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Polavandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Polavandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaiyin Paasam Thangumadiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaiyin Paasam Thangumadiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammavin Paasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavin Paasam Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin Veettu Samayal Arayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Veettu Samayal Arayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaiyal Serthuvaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaiyal Serthuvaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmai Sogam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmai Sogam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thai Thanthai Poley Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thanthai Poley Ingey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmedhu Deivam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmedhu Deivam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendunilavu Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendunilavu Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Oongal Aadum Naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Oongal Aadum Naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai Pokkal Kannethurey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Pokkal Kannethurey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Thanthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Thanthathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Pournamigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Pournamigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Jolikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Jolikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaan Pooththupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaan Pooththupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Sirikkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Sirikkithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigal Vizhuntha Vayathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal Vizhuntha Vayathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kavithai Padikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kavithai Padikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellippokkal Kannethirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellippokkal Kannethirey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Thanthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Thanthathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Pournamigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Pournamigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Jolikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Jolikkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaan Pooththupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaan Pooththupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Sirikkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Sirikkithey"/>
</div>
</pre>
