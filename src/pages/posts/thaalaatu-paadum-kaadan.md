---
title: "thaalaatu paadum song lyrics"
album: "Kaadan"
artist: "Shantanu Moitra"
lyricist: "Vanamali"
director: "Prabhu Solomon"
path: "/albums/kaadan-song-lyrics"
song: "Thaalaatu Paadum"
image: ../../images/albumart/kaadan.jpg
date: 2021-03-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4zj9fMAfMe8"
type: "Motivational"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalaattu paadum thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattu paadum thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesa paattu paadum kuyilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesa paattu paadum kuyilgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thaalam podum kiligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thaalam podum kiligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha kettu aadum mayilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha kettu aadum mayilgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai thatti kaadellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thatti kaadellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhodu thozh sehruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhodu thozh sehruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam thaan paaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam thaan paaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvom aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam thaan paaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam thaan paaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalaattu paadum thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattu paadum thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesa paattu paadum kuyilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesa paattu paadum kuyilgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thatti kaadellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thatti kaadellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhodu thozh sehruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhodu thozh sehruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduvom aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam thaan paaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam thaan paaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduvom aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduvom aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam thaan paaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam thaan paaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulli maanin koottangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanin koottangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaattin pillaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaattin pillaigale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli thaavi oodungale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli thaavi oodungale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam illai vaazhungale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam illai vaazhungale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu poo vaasam vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu poo vaasam vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhthu solli pesum pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthu solli pesum pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootha kaathu veesum veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootha kaathu veesum veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai konji pesum pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai konji pesum pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum paathaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum paathaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvana veel theettuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvana veel theettuvom"/>
</div>
</pre>
