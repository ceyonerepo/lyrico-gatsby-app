---
title: "a aa e ee song lyrics"
album: "Kobbari Matta"
artist: "Kamran"
lyricist: "Steven Shankar"
director: "Rupak Ronaldson"
path: "/albums/kobbari-matta-lyrics"
song: "A Aa E Ee"
image: ../../images/albumart/kobbari-matta.jpg
date: 2019-08-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FJaHSEt7Lyw"
type: "happy"
singers:
  - Rahul Sipligunj
  - Lipsika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">A..Aa..E..Ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A..Aa..E..Ee"/>
</div>
<div class="lyrico-lyrics-wrapper">A..Aa..E..Ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A..Aa..E..Ee"/>
</div>
<div class="lyrico-lyrics-wrapper">U..Uu..Ru..Ruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U..Uu..Ru..Ruu"/>
</div>
<div class="lyrico-lyrics-wrapper">U..Uu..Ru..Ruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U..Uu..Ru..Ruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh..Eeh..I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh..Eeh..I"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh..Ooh..Au
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh..Ooh..Au"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh..Ooh..Au
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh..Ooh..Au"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh..Eeh..I
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh..Eeh..I"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh..Ooh..Au
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh..Ooh..Au"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh..Ooh..Au
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh..Ooh..Au"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Am..Aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am..Aha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka..Kha..Ga..Gha..Gna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka..Kha..Ga..Gha..Gna"/>
</div>
<div class="lyrico-lyrics-wrapper">Cha..Ccha..Ja..Jha..Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cha..Ccha..Ja..Jha..Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta..Tha..Da..Dha..Nya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta..Tha..Da..Dha..Nya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha..Tthha..Da..Dha..Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha..Tthha..Da..Dha..Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka..Kha..Ga..Gha..Gna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka..Kha..Ga..Gha..Gna"/>
</div>
<div class="lyrico-lyrics-wrapper">Cha..Ccha..Ja..Jha..Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cha..Ccha..Ja..Jha..Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta..Tha..Da..Dha..Nya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta..Tha..Da..Dha..Nya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha..Tthha..Da..Dha..Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha..Tthha..Da..Dha..Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa..Pha..Ba..Bha..Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa..Pha..Ba..Bha..Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa..Pha..Ba..Bha..Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa..Pha..Ba..Bha..Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya..Ra..La..Va..Sya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya..Ra..La..Va..Sya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sha..Sa..Ha..La..Ksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sha..Sa..Ha..La..Ksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandira..Bandira..Bandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandira..Bandira..Bandira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka..Kaa..Ki..Kee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka..Kaa..Ki..Kee "/>
</div>
<div class="lyrico-lyrics-wrapper">Ka..Kaa..Ki..Kee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka..Kaa..Ki..Kee "/>
</div>
<div class="lyrico-lyrics-wrapper">Ku..Koo..Kru..Kroo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku..Koo..Kru..Kroo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ku..Koo..Kru..Kroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku..Koo..Kru..Kroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke..Kae..Kai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke..Kae..Kai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ko..Koa..Kow 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko..Koa..Kow "/>
</div>
<div class="lyrico-lyrics-wrapper">Ko..Koa..Kow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko..Koa..Kow"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke..Kae..Kai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke..Kae..Kai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ko..Koa..Kow 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko..Koa..Kow "/>
</div>
<div class="lyrico-lyrics-wrapper">Ko..Koa..Kow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko..Koa..Kow"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kam..Kaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kam..Kaha"/>
</div>
</pre>
