---
title: "naanaagiya nadhimoolamae song lyrics"
album: "Vishwaroopam 2"
artist: "Ghibran Muhammad"
lyricist: "Kamal Haasan"
director: "Kamal Haasan"
path: "/albums/vishwaroopam-2-lyrics"
song: "Naanaagiya Nadhimoolamae"
image: ../../images/albumart/vishwaroopam-2.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FVJELQX3-AQ"
type: "mass"
singers:
  - Kamal Haasan
  - Kaushiki Chakraborty
  - Master Karthik Suresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhage Thitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhage Thitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhage Thituku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhage Thituku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanu Thathak Thakku Dhigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanu Thathak Thakku Dhigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhanu Dhigi Tita Dhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanu Dhigi Tita Dhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitiku Dhanu Dhinak Dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitiku Dhanu Dhinak Dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakita Than Thalanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakita Than Thalanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Girikita Tharikita Girikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girikita Tharikita Girikita"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Girikita Tharikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Girikita Tharikita"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha Dha Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha Dha Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalanna Kita Thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalanna Kita Thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikita Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikita Dha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaagiya Nadhi Moolamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaagiya Nadhi Moolamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayagiya Aatharamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagiya Aatharamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaangiya Karukkudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaangiya Karukkudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyaeyila Thiruththalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyaeyila Thiruththalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinam Unai Ninainthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Unai Ninainthirukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinam Unai Ninainthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Unai Ninainthirukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pola Naan Uyiranathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pola Naan Uyiranathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Endra Naan Thaaiyanathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Endra Naan Thaaiyanathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha Payanaai Unai Perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha Payanaai Unai Perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirantha Perumai Nigazhnthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirantha Perumai Nigazhnthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinamum Naan Ninainthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinamum Naan Ninainthirukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinamum Naan Ninainthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinamum Naan Ninainthirukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhage Thita Thaga Dhikkina Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhage Thita Thaga Dhikkina Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatha Ka Thaka Dikita Thaan Thikithita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatha Ka Thaka Dikita Thaan Thikithita"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaga Thitiku Dhan Thinaku Dhan Thakita Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga Thitiku Dhan Thinaku Dhan Thakita Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalan Nakitathaka Tharikita Kita Thaka Tharikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalan Nakitathaka Tharikita Kita Thaka Tharikita"/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thaka Tharikita Thita Kata Dhadha Gina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thaka Tharikita Thita Kata Dhadha Gina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadhidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadhidha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavum Nee Appavum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavum Nee Appavum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaal Enai Aandaalum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaal Enai Aandaalum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha Payanaai Unai Perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha Payanaai Unai Perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirantha Perumai Nigazhnthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirantha Perumai Nigazhnthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinamum Naan Ninainthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinamum Naan Ninainthirukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manathin Saayal Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manathin Saayal Ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Uruvai Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Uruvai Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhangkanavai Kanalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhangkanavai Kanalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankalanga Kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankalanga Kaangiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhaya Padi Ninaivugal Thirumbidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaya Padi Ninaivugal Thirumbidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha Madi Sainthida Kidaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha Madi Sainthida Kidaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Varummo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Varummo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru Naal Varummo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Naal Varummo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Ni Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ni Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Pa Ma Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Pa Ma Ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa Ni Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ni Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Sa Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Sa Ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ni"/>
</div>
 & <div class="lyrico-lyrics-wrapper">Aaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaagiya Nadhi Moolamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaagiya Nadhi Moolamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayagiya Aatharamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagiya Aatharamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaangiya Karukkudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaangiya Karukkudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyaeyila Thiruththalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyaeyila Thiruththalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuthinam Unai Ninanthirukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Unai Ninanthirukkiren"/>
</div>
</pre>
