---
title: "kadalula penja mazha song lyrics"
album: "Koottali"
artist: "Britto Michael"
lyricist: "Kurinchi Prabha"
director: "SK Mathi"
path: "/albums/koottali-song-lyrics"
song: "Kadalula Penja Mazha"
image: ../../images/albumart/koottali.jpg
date: 2018-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HKWK8d241qE"
type: "sad"
singers:
  - Saavaniee Ravinder
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kadalula penja mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula penja mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalula penja mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula penja mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyila inipathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyila inipathila"/>
</div>
<div class="lyrico-lyrics-wrapper">malarula veesum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarula veesum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">maru naal irupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru naal irupathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha irupen unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha irupen unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pirinja irappen mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pirinja irappen mannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadalula penja mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula penja mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyila inipathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyila inipathila"/>
</div>
<div class="lyrico-lyrics-wrapper">malarula veesum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarula veesum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">maru naal irupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru naal irupathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha irupen unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha irupen unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pirinja irappen mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pirinja irappen mannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">narampil paayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narampil paayum "/>
</div>
<div class="lyrico-lyrics-wrapper">nathi nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">nathiyai thedum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathiyai thedum "/>
</div>
<div class="lyrico-lyrics-wrapper">kadalena nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalena nan"/>
</div>
<div class="lyrico-lyrics-wrapper">iruvar idaiyil idaivela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvar idaiyil idaivela "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu oru naal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu oru naal "/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyum athikaalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum athikaalai "/>
</div>
<div class="lyrico-lyrics-wrapper">swasamai nenjukul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swasamai nenjukul "/>
</div>
<div class="lyrico-lyrics-wrapper">nee nulainthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nulainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil varnangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil varnangal "/>
</div>
<div class="lyrico-lyrics-wrapper">yen varanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen varanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalin kaikalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalin kaikalil "/>
</div>
<div class="lyrico-lyrics-wrapper">iru malar naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru malar naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathin siluvaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathin siluvaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">yaar arindhaar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar arindhaar "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangathey kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangathey kanney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kalangathey kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kalangathey kanney"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil uppa vadikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil uppa vadikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju kuzhi kumaiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju kuzhi kumaiyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla vachu vedhumpathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla vachu vedhumpathey "/>
</div>
<div class="lyrico-lyrics-wrapper">kanne nee kannurangu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne nee kannurangu "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangathey kanney nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangathey kanney nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kalangathey kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangathey kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en thol mela sanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thol mela sanju"/>
</div>
<div class="lyrico-lyrics-wrapper">un thuyaram eru poyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thuyaram eru poyen"/>
</div>
<div class="lyrico-lyrics-wrapper">un thai naaney kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thai naaney kanney"/>
</div>
<div class="lyrico-lyrics-wrapper">en madiyil mela vaayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en madiyil mela vaayen"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangathey ne kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangathey ne kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kannurangu kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kannurangu kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kalangathey kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kalangathey kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadalula penja mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula penja mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyila inipathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyila inipathila"/>
</div>
<div class="lyrico-lyrics-wrapper">malarula veesum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarula veesum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">maru naal irupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru naal irupathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha irupen unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha irupen unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pirinja irappen mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pirinja irappen mannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadalula penja mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula penja mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyila inipathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyila inipathila"/>
</div>
<div class="lyrico-lyrics-wrapper">malarula veesum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarula veesum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">maru naal irupathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru naal irupathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruntha irupen unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruntha irupen unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pirinja irappen mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pirinja irappen mannodu"/>
</div>
</pre>
