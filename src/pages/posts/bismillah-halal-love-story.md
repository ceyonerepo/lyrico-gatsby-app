---
title: "bismillah song lyrics"
album: "Halal Love Story"
artist: "Bijibal - Shahabaz Aman"
lyricist: "Muhsin Parari"
director: "Zakariya Mohammed"
path: "/albums/halal-love-story-lyrics"
song: "Bismillah"
image: ../../images/albumart/halal-love-story.jpg
date: 2020-10-15
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/rEEeXo-Td_w"
type: "happy"
singers:
  - Shahabaz Aman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veezhaathe Vazhuthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaathe Vazhuthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Pokaame Karuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokaame Karuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choraathe Kaviyaathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choraathe Kaviyaathe "/>
</div>
<div class="lyrico-lyrics-wrapper">Neraa Neram Manamaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraa Neram Manamaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Bismillaah Bismillaah Bismillaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaah Bismillaah Bismillaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheythaaya Cheythellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheythaaya Cheythellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnil Ninnuravaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnil Ninnuravaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninniloodozhukunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninniloodozhukunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnilekkanayunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnilekkanayunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nin Naamamaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin Naamamaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Aalam Nirayunnoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam Nirayunnoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bismillaah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaah "/>
</div>
<div class="lyrico-lyrics-wrapper">Hikbamaa Irbakkee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hikbamaa Irbakkee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nin Naamamaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin Naamamaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Aalam Nirayunnoonin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam Nirayunnoonin"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamamaale Aalam Nirayunnoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamamaale Aalam Nirayunnoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bismillaah Bismillaah Bismillaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaah Bismillaah Bismillaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamaake Muzhuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamaake Muzhuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaathe Vazhuthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaathe Vazhuthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Pokaame Karuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokaame Karuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neraa Neram Manamaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraa Neram Manamaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Bismillaah Bismillaah Bismillaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaah Bismillaah Bismillaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bismillaah Bismillaah Bismillaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaah Bismillaah Bismillaah"/>
</div>
</pre>
