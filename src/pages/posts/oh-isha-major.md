---
title: "oh isha song lyrics"
album: "Major"
artist: "Sricharan Pakala"
lyricist: "Rajiv Bharadwaj"
director: "Sashi Kiran Tikka"
path: "/albums/major-lyrics"
song: "Oh Isha"
image: ../../images/albumart/major.jpg
date: 2022-06-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HQHlvmpMDDs"
type: "love"
singers:
  -	Armaan Malik
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haayi Haayi Haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Haayi Haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">E Maya Emitoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Maya Emitoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Aagi Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Aagi Aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguruthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguruthunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chikkulanni Kurchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkulanni Kurchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Lekkanevo Nerchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Lekkanevo Nerchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ankelata Ledho Aaduthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ankelata Ledho Aaduthunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Antarantu Oh Nestamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Antarantu Oh Nestamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Kotha Lokam Cheranila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kotha Lokam Cheranila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sankhya Visirina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sankhya Visirina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankellu Darichi Cherava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankellu Darichi Cherava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Isha Haan Oh Isha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Isha Haan Oh Isha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Isha Haan Oh Isha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Isha Haan Oh Isha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhurane Lakkoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhurane Lakkoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalo Dakkoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalo Dakkoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosari Kosari Kougilindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosari Kosari Kougilindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Livvamakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Livvamakala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuruga Nilabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuruga Nilabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulo Alajadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo Alajadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penchi Penchi Premalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchi Penchi Premalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchite Yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchite Yela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kotha Darilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kotha Darilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Prema Page Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Prema Page Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Kadhani Raadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kadhani Raadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Ila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sankhya Visirina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sankhya Visirina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankellu Darichi Cherava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankellu Darichi Cherava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Isha Haan Oh Isha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Isha Haan Oh Isha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Isha Haan Oh Isha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Isha Haan Oh Isha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Isha Haan Oh Isha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Isha Haan Oh Isha"/>
</div>
</pre>
