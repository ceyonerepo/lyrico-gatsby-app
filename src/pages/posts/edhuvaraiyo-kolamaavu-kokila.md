---
title: "edhuvaraiyo song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Vivek"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Edhuvaraiyo"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VIsYnSD2odc"
type: "sad"
singers:
  - Sean Roldan
  - Gautham Vasudev Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edhuvaraiyo edhuvaraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvaraiyo edhuvaraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha valiyae edhuvaraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha valiyae edhuvaraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul anaiyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul anaiyaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhiyo thalai vidhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyo thalai vidhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha gadhiyae thalai vidhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha gadhiyae thalai vidhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyar maraiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyar maraiyaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marubadi nila pozhiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi nila pozhiyaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi nila pozhiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi nila pozhiyaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraiyaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal tharum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal tharum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viriyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viriyaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal tharum kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal tharum kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaatho viriyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaatho viriyaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Avlo easy ah irukkum nenachiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avlo easy ah irukkum nenachiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippidi thaan irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippidi thaan irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvanunga pottu thakkuvanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvanunga pottu thakkuvanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kavalayae padatha poitae iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kavalayae padatha poitae iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oru ambalaikku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru ambalaikku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu ambalaikku samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu ambalaikku samam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyudha unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyudha unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyudha unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyudha unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla paaru konja thooram innum ponum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla paaru konja thooram innum ponum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum velicham theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum velicham theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">There is light at the end of the tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There is light at the end of the tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Go for it girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go for it girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaram vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram vanthu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram vanthu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illa yaarum illa koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illa yaarum illa koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyilae ulavugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyilae ulavugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthidavae pazhagugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthidavae pazhagugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha vendum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha vendum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai thondrum aasai thondrum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai thondrum aasai thondrum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal naduvae thathumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal naduvae thathumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai varumaa irangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai varumaa irangugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalin vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyaatho mudiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyaatho mudiyaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalin vinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin vinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">udaiyaatho mudiyaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiyaatho mudiyaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaipadum pura nagaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaipadum pura nagaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaipadum pura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaipadum pura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaraatho uyaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaraatho uyaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga kuzhiyil vaazha viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga kuzhiyil vaazha viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya padiyil kaadhal ezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya padiyil kaadhal ezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru ambalaikku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru ambalaikku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu ambalaikku samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu ambalaikku samam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyudha unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyudha unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyudha unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyudha unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla paaru konja thooram innum ponum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla paaru konja thooram innum ponum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum velicham theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum velicham theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">There is light at the end of the tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There is light at the end of the tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Go for it girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go for it girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvaraiyo edhuvaraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvaraiyo edhuvaraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha valiyae edhuvaraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha valiyae edhuvaraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul anaiyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul anaiyaadho"/>
</div>
</pre>
