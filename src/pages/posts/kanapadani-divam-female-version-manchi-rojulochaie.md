---
title: "kanapadani divam female version song lyrics"
album: "Manchi Rojulochaie"
artist: "Anup Rubens"
lyricist: "Kasarla Shyam"
director: "Maruthi"
path: "/albums/manchi-rojulochaie-lyrics"
song: "Kanapadani Divam Female Version"
image: ../../images/albumart/manchi-rojulochaie.jpg
date: 2021-11-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0yeXgKY0dEk?start=35"
type: "melody"
singers:
  - Sahithi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa chinni paadham nee gunde paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa chinni paadham nee gunde paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatadathunte mosavu nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatadathunte mosavu nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelone pranam naalone daachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone pranam naalone daachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne naalo chusavu o nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne naalo chusavu o nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle premante telisindhi o nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle premante telisindhi o nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vente santosham kalisindhile nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vente santosham kalisindhile nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chethullo unte bayamedhi o nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethullo unte bayamedhi o nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontariga nenunna naa dhairyam nuvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontariga nenunna naa dhairyam nuvvega"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa needala naa venuke untune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa needala naa venuke untune"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipavule naa mundu daarulne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipavule naa mundu daarulne"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa navvule nee lokam antune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa navvule nee lokam antune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa chuttu allave bandhale o nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa chuttu allave bandhale o nanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanipinche dhaivam nuvvainavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinche dhaivam nuvvainavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchi prathiroju pujinchinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchi prathiroju pujinchinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kalla mundhu nuvvunte chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kalla mundhu nuvvunte chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne chusthu brathikestha o nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chusthu brathikestha o nanna"/>
</div>
</pre>
