---
title: "karadu muradu male version song lyrics"
album: "Bakrid"
artist: "D. Imman"
lyricist: "Gnanakaravel"
director: "Jagadeesan Subu"
path: "/albums/bakrid-lyrics"
song: "Karadu Muradu Male Version"
image: ../../images/albumart/bakrid.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u9SUo5f5Xc4"
type: "happy"
singers:
  - Narayanan Ravishankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therandu Vazhiyum Thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therandu Vazhiyum Thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nadakkum Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nadakkum Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya Iruppen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya Iruppen Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anba Unarthidum Bashai Nazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Unarthidum Bashai Nazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Vazhigira Vaarthai Aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vazhigira Vaarthai Aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthae Enakku Usura Uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuthae Enakku Usura Uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Tholachadhum Enna Tholachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Tholachadhum Enna Tholachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Dhesaiyilum Kanna Vedhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Dhesaiyilum Kanna Vedhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaviyaa Thavichu Thangaththa Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaviyaa Thavichu Thangaththa Pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Nodi Pirivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Nodi Pirivula"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththavana Thudichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththavana Thudichenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therandu Vazhiyum Thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therandu Vazhiyum Thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nadakkum Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nadakkum Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya Iruppen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya Iruppen Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Posukkunnu Kannukkulla Terinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posukkunnu Kannukkulla Terinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhaiyin Uruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhaiyin Uruvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona Senmam Namakkulla Ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Senmam Namakkulla Ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul Kodi Urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul Kodi Urava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillaiyena Nee Aagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyena Nee Aagiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Mudhal En Aaguvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Mudhal En Aaguvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kootilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kootilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr Aandilae Thanthaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr Aandilae Thanthaaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Noor Aandugal Neengathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noor Aandugal Neengathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nyaabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenju Kitta Vandhu Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kitta Vandhu Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Vidu Pathamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Vidu Pathamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Veppam Idhayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Veppam Idhayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadavidum Edhamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadavidum Edhamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Alaiyum Medhappaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Alaiyum Medhappaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therandu Vazhiyum Thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therandu Vazhiyum Thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nadakkum Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nadakkum Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya Iruppen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya Iruppen Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pooththu Kulungira Paalaivanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooththu Kulungira Paalaivanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Kaalu Konda Nandhavanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kaalu Konda Nandhavanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan Nee Iruntha Valiyum Sugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Nee Iruntha Valiyum Sugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Alaiyaama Vantha Varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Alaiyaama Vantha Varamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Vida Illai Manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Vida Illai Manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivu Terinja Payanam Ganamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivu Terinja Payanam Ganamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulila Oliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulila Oliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugila Iru Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugila Iru Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therandu Vazhiyum Thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therandu Vazhiyum Thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nadakkum Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nadakkum Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya Iruppen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya Iruppen Naanae"/>
</div>
</pre>
