---
title: "inayae song lyrics"
album: "Thadam"
artist: "Arun Raj"
lyricist: "Madhan Karky"
director: "Magizh Thirumeni"
path: "/albums/thadam-lyrics"
song: "Inayae"
image: ../../images/albumart/thadam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YlMAhUrXFiM"
type: "love"
singers:
  - Sid Sriram
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inayae En Uyir Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inayae En Uyir Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Imai Thiranthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Imai Thiranthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Uraivathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Uraivathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage En Muzhu Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage En Muzhu Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhigaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigaliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Uranguthu Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Uranguthu Paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugey Nee irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugey Nee irunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kai Pesi Vaai Moodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Pesi Vaai Moodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Saaithu Nee Sirithaaiyenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saaithu Nee Sirithaaiyenil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thaeneeril Thean Koodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaeneeril Thean Koodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaiye En Uyir Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiye En Uyir Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Inimaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Inimaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Karaivathu Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Karaivathu Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugamaai Kai Viral Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugamaai Kai Viral Pidithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nadappathu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nadappathu Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unarvathu Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unarvathu Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maiyal Kadhalaai Maariya Pulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyal Kadhalaai Maariya Pulli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endro Manam Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endro Manam Ketkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kaamamaai Uru Konda Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kaamamaai Uru Konda Tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaikkaiyil Uyir Verkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkaiyil Uyir Verkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Mel Pookkum Neerodu Neeraatiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Mel Pookkum Neerodu Neeraatiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Naal Ennai Suththam Seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naal Ennai Suththam Seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Sevaigal Ellaame Paaraatiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Sevaigal Ellaame Paaraatiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Aadaigal Meendum Thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Aadaigal Meendum Thanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inayae En Uyir Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inayae En Uyir Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Inimaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Inimaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Karaivathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Karaivathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugamaai Kai Viral Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugamaai Kai Viral Pidithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nadappathu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nadappathu Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unarvathu Yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unarvathu Yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugey Nee irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugey Nee irunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kai Pesi Vaai Moodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Pesi Vaai Moodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Saaithu Nee Sirithaaiyenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saaithu Nee Sirithaaiyenil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Theaneeril Thean Koodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theaneeril Thean Koodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaiye En Uyir Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiye En Uyir Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Inimaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Inimaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Karaivathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Karaivathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugamaai Kai Viral Pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugamaai Kai Viral Pidithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nadappathu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nadappathu Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unarvathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unarvathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiye"/>
</div>
</pre>
