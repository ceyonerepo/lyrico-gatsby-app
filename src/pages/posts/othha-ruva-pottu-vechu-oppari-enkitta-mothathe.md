---
title: "othha ruva pottu vechu - oppari song lyrics"
album: "Enkitta Mothathe"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Ramu Chellappa"
path: "/albums/enkitta-mothathe-lyrics"
song: "Othha Ruva Pottu Vechu - Oppari"
image: ../../images/albumart/enkitta-mothathe.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T6Aikbao7wo"
type: "sad"
singers:
  - Prabhu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ettooru Seemaiyila Oviyama Irundhavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettooru Seemaiyila Oviyama Irundhavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathuru Koodi Nika Paadaiyila Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathuru Koodi Nika Paadaiyila Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Singara Kannazhagil Sedhi Pala Sonavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singara Kannazhagil Sedhi Pala Sonavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnuthaiyum Pesaama Ummunudhaan Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnuthaiyum Pesaama Ummunudhaan Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Ruva Pottu Vechu Ooru Sanam Kottadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Ruva Pottu Vechu Ooru Sanam Kottadichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Methaiyila Mella Mella Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methaiyila Mella Mella Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Kattayila Kanna Moodi Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Kattayila Kanna Moodi Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Sothu Sugam Serthu Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Sothu Sugam Serthu Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangala Paaka Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhangala Paaka Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamave Numma Vitu Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamave Numma Vitu Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Ettu Kaalu Theril Yeri Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Ettu Kaalu Theril Yeri Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaka Neram Pola Karupaana Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaka Neram Pola Karupaana Thaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Poraaru Sooravali Kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Poraaru Sooravali Kaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaka Neram Pola Karupaana Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaka Neram Pola Karupaana Thaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Poraaru Sooravali Kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Poraaru Sooravali Kaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nekka Irundharu Nethu Vara Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekka Irundharu Nethu Vara Thaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakama Poraaru Pacha Ola Keethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakama Poraaru Pacha Ola Keethaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha Ola Keethaa Pacha Ola Keethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Ola Keethaa Pacha Ola Keethaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha Ola Keethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Ola Keethaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru Thaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Poga Pola Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Poga Pola Poraaru Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arasaanda Periyoru Ambonu Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasaanda Periyoru Ambonu Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Pera Sethavaru Thaniaala Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pera Sethavaru Thaniaala Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaka Vazhndhavaru Kanneera Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaka Vazhndhavaru Kanneera Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Serukaa Thirinjavaru Sevanenu Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serukaa Thirinjavaru Sevanenu Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu Thinna Veedu Katti Nallairundharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Thinna Veedu Katti Nallairundharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Kaiya Katti Ipo Kamunudhaan Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Kaiya Katti Ipo Kamunudhaan Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonadhellam Senju Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonadhellam Senju Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana Anga Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana Anga Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandadhellam Kaiya Vecha Nethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandadhellam Kaiya Vecha Nethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadasiyil Kaanamathaan Poraaraiya Thorthu Thorthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadasiyil Kaanamathaan Poraaraiya Thorthu Thorthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru Thaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Poga Pola Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Poga Pola Poraaru Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodhuvathi Vaasam Adha Pola Avaru Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhuvathi Vaasam Adha Pola Avaru Paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaiya Pothi Pesa Avaru Vartha Pochu Mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaiya Pothi Pesa Avaru Vartha Pochu Mosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu Maadu Vaangi Vecha Ammanukum Poosa Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Maadu Vaangi Vecha Ammanukum Poosa Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona Edam Theriyalaya Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Edam Theriyalaya Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Theki Vechi Nelachidhinga Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Theki Vechi Nelachidhinga Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Rendu Katti Vecha Vedhanaiya Yetti Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Rendu Katti Vecha Vedhanaiya Yetti Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu Poora Suthi Vandha Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu Poora Suthi Vandha Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nummala Vitu Poga Oothavenum Paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nummala Vitu Poga Oothavenum Paalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru Thaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru Thaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Poraaru Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Ruva Pottu Vechu Ooru Sanam Kottadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Ruva Pottu Vechu Ooru Sanam Kottadichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Methaiyila Mella Mella Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methaiyila Mella Mella Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Kattayila Kanna Moodi Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Kattayila Kanna Moodi Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Sothu Sugam Serthu Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Sothu Sugam Serthu Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangala Paaka Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhangala Paaka Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamave Numma Vitu Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamave Numma Vitu Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Ettu Kaalu Theril Yeri Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Ettu Kaalu Theril Yeri Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaru Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaru Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Poga Pola Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Poga Pola Poraaru Poraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha Poga Pola Poraaru Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha Poga Pola Poraaru Poraaru"/>
</div>
</pre>
