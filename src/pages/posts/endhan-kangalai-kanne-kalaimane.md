---
title: "endhan kangalai song lyrics"
album: "Kanne Kalaimane"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Seenu Ramasamy"
path: "/albums/kanne-kalaimane-lyrics"
song: "Endhan Kangalai"
image: ../../images/albumart/kanne-kalaimane.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iwgEHHXQUjQ"
type: "sad"
singers:
  - Yuvan Shankar Raja
  - Sooraj Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endhan Kangalai Kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kangalai Kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kangalil Kangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalil Kangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaithenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kangalai Kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kangalai Kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kangalil Ini Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalil Ini Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhipenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neril Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen En Nenjil Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen En Nenjil Vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Koottukkul Pugunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Koottukkul Pugunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevvaru Marappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvaru Marappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Marippathu Nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Marippathu Nandru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Vaarthai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Vaarthai Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kalagapoochikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kalagapoochikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirappathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirappathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi Kande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Kande"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Thondrum Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thondrum Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Patshi Vilanku Saathikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patshi Vilanku Saathikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathagam Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathagam Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin Ethiri Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin Ethiri Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam Thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Thevaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaiyum Ennaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum Ennaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirikkum Perum Pallaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirikkum Perum Pallaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Konde Moodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Konde Moodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kangalai Kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kangalai Kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kangalai Kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kangalai Kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kangalil Kangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalil Kangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaithenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kangalai Kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kangalai Kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kangalil Ini Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalil Ini Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhipenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neril Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen En Nenjil Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen En Nenjil Vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Koottukkul Pugunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Koottukkul Pugunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevvaru Marappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvaru Marappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Marippathu Nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Marippathu Nandru"/>
</div>
</pre>
