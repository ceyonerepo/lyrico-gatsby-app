---
title: "kodi kodiyai ethanai song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Kodi Kodiyai Ethanai"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dEL1f8qWHkY"
type: "happy"
singers:
  - Vijay Yesudas
  - Vaishali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kodi Kodiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Kidakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Kidakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullin Thalai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin Thalai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam Sootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairam Sootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyin Thuli Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin Thuli Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theendum Kaalidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum Kaalidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaigal Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigal Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Varugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Varugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenda Pidi Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Pidi Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodaigal Neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaigal Neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marangal Pola Edhu Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangal Pola Edhu Inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrile Vaazhvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrile Vaazhvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Perum Thunbame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Perum Thunbame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Intha Naal Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Intha Naal Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Per Inbame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Per Inbame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaazhndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaazhndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parpome Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parpome Naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Kodiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Kidakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Kidakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullin Thalai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin Thalai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam Sootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairam Sootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyin Thuli Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin Thuli Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vizhi Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vizhi Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kuppai Thottiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kuppai Thottiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaiyodu Athu Odaintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaiyodu Athu Odaintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kutti Chedikku Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kutti Chedikku Athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oramaagaum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oramaagaum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yadhum Ooru Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadhum Ooru Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnavarin Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnavarin Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondra Engalin Varamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondra Engalin Varamaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Muzhuvadhum Unmayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Muzhuvadhum Unmayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Anbai Vedhaithaal Nalamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Anbai Vedhaithaal Nalamaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malayil Nee Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayil Nee Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil Valaivellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil Valaivellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Bayam Kaatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Bayam Kaatume"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Thunithu Nee Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Thunithu Nee Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Ellaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna Poo Meetume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Poo Meetume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Kondaatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Kondaatame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Pogum Neerottame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Pogum Neerottame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Kodiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Kidakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Kidakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullin Thalai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin Thalai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam Sootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairam Sootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyin Thuli Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin Thuli Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Ooru Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Ooru Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Entha Paravaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Entha Paravaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Kadalukku Serum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Kadalukku Serum Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Ninaithu Mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Ninaithu Mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Kudhipathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Kudhipathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Kaayam Athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Kaayam Athai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithu Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathir Naalai Vinnil Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathir Naalai Vinnil Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetai Thaandi Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetai Thaandi Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Pokkan Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Pokkan Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootin Verumaigalai Ninaipathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootin Verumaigalai Ninaipathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhakku Athu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakku Athu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Merkku Athu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merkku Athu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaigal Namadhaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaigal Namadhaagume"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Inbam Athu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Inbam Athu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam Athu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Athu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Ondraagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Ondraagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Kondaatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Kondaatame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Pogum Neerottame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Pogum Neerottame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Kodiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Kidakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Kidakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullin Thalai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin Thalai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam Sootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairam Sootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyin Thuli Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin Thuli Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Per Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Inbam"/>
</div>
</pre>
