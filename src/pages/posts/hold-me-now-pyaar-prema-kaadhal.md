---
title: "hold me now song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Hold Me Now"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HrrkN3EREYI"
type: "love"
singers:
  - Sanjith Hegde
  - Thurga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pothum di un minnal thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum di un minnal thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaisaiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaisaiyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi antha vazhthu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi antha vazhthu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa un kannam pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa un kannam pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam theyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam theyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayama vanthu veesum kaathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayama vanthu veesum kaathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalai paaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalai paaiyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobama naan kenjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobama naan kenjuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka solla unna konjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka solla unna konjuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirattu di oru kozhndhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirattu di oru kozhndhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan anjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan anjuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaachala kai koorkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaachala kai koorkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu mulichu unnai paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu mulichu unnai paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir koduthu thaan unnakaga naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir koduthu thaan unnakaga naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thookuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thookuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizha kizha kizha tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizha kizha kizha tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil vizhanthathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil vizhanthathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizha kizha kizha tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizha kizha kizha tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil vizhanthathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil vizhanthathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hold me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me little closer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me little closer"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me little closer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me little closer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna da nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna da nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panni naan maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panni naan maaruren"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla da naan ennai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla da naan ennai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga poguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamaai vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamaai vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollaiyodu vilaiyadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaiyodu vilaiyadura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookittu en ellaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookittu en ellaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan vaazhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan vaazhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madiyil naan otti thoonguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil naan otti thoonguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kotti kodai thaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kotti kodai thaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi nee vanthu nadakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi nee vanthu nadakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasikka thaan konjom neenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka thaan konjom neenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna mutham koda vanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna mutham koda vanguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathisiyam onnu kadaika thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathisiyam onnu kadaika thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thaanguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala maarukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala maarukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaathil vittathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaathil vittathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala maarukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala maarukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaathil vittathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaathil vittathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hold me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me little closer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me little closer"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Hold me little closer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hold me little closer"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oohoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala kaala kaala maarukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala kaala kaala maarukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaathil vittathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaathil vittathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela mela mela paarakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela mela paarakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal pattathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal pattathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam kuthikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam kuthikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizha kizha kizha tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizha kizha kizha tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil vizhanthathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil vizhanthathaal"/>
</div>
</pre>
