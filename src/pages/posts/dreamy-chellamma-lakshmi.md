---
title: "dreamy chellamma song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Dreamy Chellamma"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LnZpWXS6R3g"
type: "happy"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hohoh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hohoh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dreamy little chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dreamy little chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un chinna kanna konjam thoranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un chinna kanna konjam thoranthukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chubby kutty chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chubby kutty chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rekka rendu katti paranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rekka rendu katti paranthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu kanda kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu kanda kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai aana kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai aana kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinjida virinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinjida virinjida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dreamy little chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dreamy little chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un chinna kanna konjam thoranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un chinna kanna konjam thoranthukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chubby kutty chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chubby kutty chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rekka rendu katti paranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rekka rendu katti paranthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkae unakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkae unakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae thorakkuthu azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae thorakkuthu azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukkae athukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukkae athukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragae mulaikuthu azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragae mulaikuthu azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu kanda kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu kanda kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai aana kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai aana kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinjida virinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinjida virinjida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu kanda kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu kanda kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai aana kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai aana kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinjida virinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinjida virinjida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavodu nee pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavodu nee pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanmoodi pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanmoodi pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaimoodi pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaimoodi pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarenna sonnaalum kekkama pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarenna sonnaalum kekkama pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavodu nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavodu nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kai korthu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kai korthu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalellaam aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalellaam aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un boomi ninnaalum nikkathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un boomi ninnaalum nikkathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un dream-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dream-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un friend-ah maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un friend-ah maathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un path-ah maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un path-ah maathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu kanda kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu kanda kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai aana kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai aana kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinjida virinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinjida virinjida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu kanda kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu kanda kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai aana kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai aana kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinjida virinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinjida virinjida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dreamy little chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dreamy little chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un chinna kanna konjam thoranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un chinna kanna konjam thoranthukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chubby kutty chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chubby kutty chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rekka rendu katti paranthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rekka rendu katti paranthukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana nana"/>
</div>
</pre>
