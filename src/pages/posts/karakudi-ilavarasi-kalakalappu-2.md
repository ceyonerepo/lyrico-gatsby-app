---
title: "karakudi ilavarasi song lyrics"
album: "Kalakalappu 2"
artist: "Hiphop Tamizha"
lyricist: "Mohan Rajan"
director: "Sundar C"
path: "/albums/kalakalappu-2-lyrics"
song: "Karakudi Ilavarasi"
image: ../../images/albumart/kalakalappu-2.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aCDKULGwsps"
type: "love"
singers:
  - Jassie Gift
  - Sudharshan Ashok
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karakudi ilavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakudi ilavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja thaakura mavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thaakura mavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi varagarisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi varagarisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kayapodura enna alassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kayapodura enna alassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu gun-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu gun-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bun-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bun-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu gin-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu gin-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodha yethudhu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodha yethudhu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kulfi madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kulfi madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu barfi madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu barfi madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva verai madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva verai madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna konna da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna konna da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masakiyae masakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakiyae masakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi naanum porennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukiyae sirukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukiyae sirukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethari naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari naanum porennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masakiyae masakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakiyae masakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi naanum porennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukiyae sirukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukiyae sirukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethari naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari naanum porennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen di yen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen di yen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu motha ulaga azhaga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu motha ulaga azhaga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthi nee vechirikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi nee vechirikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa kaima panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa kaima panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruma pol kodhika nee vittuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruma pol kodhika nee vittuputtiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen di yen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen di yen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu motha ulaga azhaga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu motha ulaga azhaga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthi nee vechirikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi nee vechirikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa kaima panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa kaima panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruma pol kodhika nee vittuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruma pol kodhika nee vittuputtiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All in all azhagu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All in all azhagu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathan ammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathan ammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasa repair-aka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manasa repair-aka"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandha killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandha killadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha odanae pulse-a yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha odanae pulse-a yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poralae yemma yemma di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poralae yemma yemma di"/>
</div>
<div class="lyrico-lyrics-wrapper">GPS illamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GPS illamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae na un pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae na un pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakudi ilavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakudi ilavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja thaakura mavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thaakura mavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi varagarisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi varagarisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kayapodura enna alassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kayapodura enna alassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakudi ilavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakudi ilavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja thaakura mavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thaakura mavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi varagarisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi varagarisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kayapodura enna alassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kayapodura enna alassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masakiyae masakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakiyae masakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi naanum porennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukiyae sirukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukiyae sirukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethari naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari naanum porennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masakiyae masakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakiyae masakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi naanum porennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukiyae sirukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukiyae sirukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethari naanum porennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari naanum porennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen di yen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen di yen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu motha ulaga azhaga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu motha ulaga azhaga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthi nee vechirikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi nee vechirikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa kaima panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa kaima panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruma pol kodhika nee vittuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruma pol kodhika nee vittuputtiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen di yen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen di yen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu motha ulaga azhaga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu motha ulaga azhaga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthi nee vechirikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi nee vechirikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa kaima panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa kaima panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruma pol kodhika nee vittuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruma pol kodhika nee vittuputtiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye yeyae yeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye yeyae yeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye yeyae yeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye yeyae yeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye yeyae yeye yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye yeyae yeye yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu gun-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu gun-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bun-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bun-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu gin-u madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu gin-u madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodha yethudhu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodha yethudhu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kulfi madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kulfi madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu barfi madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu barfi madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva verai madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva verai madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna konna da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna konna da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakudi ilavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakudi ilavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja thaakura mavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thaakura mavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi varagarisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi varagarisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kayapodura enna alassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kayapodura enna alassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakudi ilavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakudi ilavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja thaakura mavarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja thaakura mavarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi varagarisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi varagarisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kayapodura enna alassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kayapodura enna alassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masakiyae masakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakiyae masakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukkiyae sirukkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkiyae sirukkiyae"/>
</div>
</pre>
