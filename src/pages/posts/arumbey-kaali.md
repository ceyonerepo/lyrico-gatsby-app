---
title: "arumbey song lyrics"
album: "Kaali"
artist: "Vijay Antony"
lyricist: "Vivek"
director: "Kiruthiga Udhayanidhi"
path: "/albums/kaali-lyrics"
song: "Arumbey"
image: ../../images/albumart/kaali.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Osao-zv7iH0"
type: "love"
singers:
  - Nivas
  - Janaki Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arumbae arumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbae arumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kadathi poo karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kadathi poo karumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbae thalumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbae thalumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kedathi poo kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kedathi poo kurumbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugamaiyae veragaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugamaiyae veragaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaramalae uyir poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaramalae uyir poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edham ooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edham ooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkamum kuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkamum kuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbae kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbae kurumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kadathi poo kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kadathi poo kurumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbae thalumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbae thalumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kedathi poo karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kedathi poo karumbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patthiyama ninna vaalibam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patthiyama ninna vaalibam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parthu than viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parthu than viduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Batthirama vecha aanavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batthirama vecha aanavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoolaagi thoo viduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoolaagi thoo viduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha neram senja ooviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha neram senja ooviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal kuda koosidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal kuda koosidudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa.pathu viral pottai kaatukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa.pathu viral pottai kaatukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookolam poosidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookolam poosidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannam kuliyodu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam kuliyodu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vedhai pottuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vedhai pottuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu karaiyodu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu karaiyodu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ala pottu taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ala pottu taan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharum devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharum devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha vaasam kaati puttaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaasam kaati puttaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumbae arumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbae arumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kadathi poo karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kadathi poo karumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbae thalumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbae thalumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kedathi poo kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kedathi poo kurumbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netthi mudi suthum paambu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthi mudi suthum paambu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai seendi paakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai seendi paakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaachinna pulla seiyum veemba pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaachinna pulla seiyum veemba pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyi theendi paakuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyi theendi paakuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kungumam poo kottum megama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumam poo kottum megama"/>
</div>
<div class="lyrico-lyrics-wrapper">Panji vagam thoovuthudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panji vagam thoovuthudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa manmadha thee patthum baanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa manmadha thee patthum baanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mogam yevuthuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mogam yevuthuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam palathandi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam palathandi than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen thada podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen thada podatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji uravada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji uravada than"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru valai podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru valai podatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha jodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha jodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vazha kupiduthaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vazha kupiduthaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumbae arumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbae arumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kadathi poo karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kadathi poo karumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbae thalumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbae thalumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kedathi poo kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kedathi poo kurumbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugamaiyae veragaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugamaiyae veragaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaramalae uyir poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaramalae uyir poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edham ooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edham ooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkamum kuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkamum kuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbae kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbae kurumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kadathi poo kurumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kadathi poo kurumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbae thalumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbae thalumbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kedathi poo karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kedathi poo karumbae"/>
</div>
</pre>
