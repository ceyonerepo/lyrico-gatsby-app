---
title: "Kalava song lyrics"
album: "Amaram Akhilam Prema"
artist: "Radhan"
lyricist: "Rehman"
director: "Jonathan Edwards"
path: "/albums/amaram-akhilam-prema-lyrics"
song: "Kalava"
image: ../../images/albumart/amaram-akhilam-prema.jpg
date: 2020-09-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yiVsr8BXnoU"
type: "love"
singers:
  - Satya Yamini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Piliche swaramaa valache varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piliche swaramaa valache varamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadilinchavu nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadilinchavu nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhalo sodhalaa modhalai ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo sodhalaa modhalai ippude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalupe theriche preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalupe theriche preme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaa naa kalavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaa naa kalavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kalalaku dhorikina nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kalalaku dhorikina nijamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadivaa yedha sadivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadivaa yedha sadivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenidhivarakerugani mahimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenidhivarakerugani mahimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jathava naa kathavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathava naa kathavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa madhigelichina pasithanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa madhigelichina pasithanama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivaa alajadivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivaa alajadivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa anumathinadagani ranamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa anumathinadagani ranamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa dhaarine mallinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhaarine mallinche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo theepi panthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo theepi panthame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa theerune maarchese aaratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa theerune maarchese aaratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo needalaa ventaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo needalaa ventaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee kotha bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kotha bandhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopindhi nuvenantu naa gamyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopindhi nuvenantu naa gamyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preme naa manasanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme naa manasanthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preme ee pulakintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme ee pulakintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preme ee jagamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme ee jagamanthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preme preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme preme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piliche swaramaa valache varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piliche swaramaa valache varamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadilinchavu nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadilinchavu nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhalo sodhalaa modhalai ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo sodhalaa modhalai ippude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chigure thodige preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigure thodige preme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenevaranu prashnaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenevaranu prashnaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo badhuluvu nuvvenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo badhuluvu nuvvenani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo rujuvai niliche lolona praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo rujuvai niliche lolona praaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kotha janmake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kotha janmake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo arthaanni korithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo arthaanni korithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee lokame ila ninu choopele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee lokame ila ninu choopele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa pedavula paina nuvve navai cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pedavula paina nuvve navai cheragaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa velugula lonaa ninne choosaa kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa velugula lonaa ninne choosaa kothagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve naa prethimaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa prethimaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve naa prethibaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa prethibaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve naa prethichota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa prethichota"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa dhaarine mallinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhaarine mallinche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo theepi panthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo theepi panthame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa theerune maarchese aaratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa theerune maarchese aaratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo needalaa ventaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo needalaa ventaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee kotha bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kotha bandhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopindhi nuvvenantu naa gamyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopindhi nuvvenantu naa gamyame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaa naa kalavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaa naa kalavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na kalalaku dhorikina nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na kalalaku dhorikina nijamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadivaa yedha sadivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadivaa yedha sadivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenidhivarakerugani mahimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenidhivarakerugani mahimaa"/>
</div>
</pre>