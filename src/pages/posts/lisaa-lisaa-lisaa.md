---
title: "lisaa lisaa song lyrics"
album: "Lisaa"
artist: "Santhosh Dhayanidhi"
lyricist: "Mani Amuthavan"
director: "Raju Viswanath"
path: "/albums/lisaa-lyrics"
song: "Lisaa Lisaa"
image: ../../images/albumart/lisaa.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D3z6Ktw9Mxs"
type: "happy"
singers:
  - Varun Parandhaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoo Oo Hoowow Ooh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Hoowow Ooh Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blackaana Rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blackaana Rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devil Aana Face-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devil Aana Face-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hell Made Pizza Pizza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hell Made Pizza Pizza"/>
</div>
<div class="lyrico-lyrics-wrapper">Lisaa Lisaa Monalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lisaa Lisaa Monalisaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Helo Helo Helo Helo Dragula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helo Helo Helo Helo Dragula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vila Vila Vila Vila Evil -Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vila Vila Vila Vila Evil -Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bella Bella Bella Bella Anabella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bella Bella Bella Bella Anabella"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ghostu Villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ghostu Villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Horror-Ah Horror-Ah Kaattadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horror-Ah Horror-Ah Kaattadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Fever-Ah Fever-Ah Yethaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fever-Ah Fever-Ah Yethaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hyper Tension Aakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyper Tension Aakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Scary Pannaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scary Pannaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Varudhae Bayam Varudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Varudhae Bayam Varudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Pada Pada Padagirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Pada Pada Padagirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Uruvam Thodargirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Uruvam Thodargirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Saavai Kaattudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Saavai Kaattudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Nila Oh Nila Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Nila Oh Nila Nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Killing Killing Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Killing Killing Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela Ela Oh Ela Ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela Ela Oh Ela Ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dying Missed Call Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dying Missed Call Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blackaana Rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blackaana Rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devil Aana Face-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devil Aana Face-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hell Made Pizza Pizza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hell Made Pizza Pizza"/>
</div>
<div class="lyrico-lyrics-wrapper">Lisaa Lisaa Monalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lisaa Lisaa Monalisaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Urunden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Urunden"/>
</div>
<div class="lyrico-lyrics-wrapper">Urunden Puranden Miranden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urunden Puranden Miranden"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Olinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Olinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Olinjae Marainjae Thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinjae Marainjae Thirinjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laka Laka-Nnu Sirikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka Laka-Nnu Sirikaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Muzhiya Muzhiya Uruttaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Muzhiya Muzhiya Uruttaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mora Mora-Nnu Muraikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mora Mora-Nnu Muraikaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vayathula Puliya Karaikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vayathula Puliya Karaikaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukka Illaiya Theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka Illaiya Theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppa Vellaiya Theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppa Vellaiya Theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanum Paarthathu Kedaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum Paarthathu Kedaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukku Thaan Ulagamae Bayapaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukku Thaan Ulagamae Bayapaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blackaana Rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blackaana Rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devil Aana Face-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devil Aana Face-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hell Made Pizza Pizza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hell Made Pizza Pizza"/>
</div>
<div class="lyrico-lyrics-wrapper">Lisaa Lisaa Monalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lisaa Lisaa Monalisaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Helo Helo Helo Helo Dragula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helo Helo Helo Helo Dragula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vila Vila Vila Vila Evil -Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vila Vila Vila Vila Evil -Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bella Bella Bella Bella Anabella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bella Bella Bella Bella Anabella"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ghostu Villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ghostu Villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blackaana Rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blackaana Rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Devil Aana Face-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devil Aana Face-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hell Made Pizza Pizza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hell Made Pizza Pizza"/>
</div>
<div class="lyrico-lyrics-wrapper">Lisaa Lisaa Monalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lisaa Lisaa Monalisaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananaana Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananaana Naana Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Naana Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananaana Thana Naanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Thana Naanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananaana Naana Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Naana Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananaana Thaana Thaana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Thaana Thaana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananaana Thana Naanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana Thana Naanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah Yeah"/>
</div>
</pre>
