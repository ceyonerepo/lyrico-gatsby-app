---
title: "nooraai song lyrics"
album: "Kaali"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Kiruthiga Udhayanidhi"
path: "/albums/kaali-lyrics"
song: "Nooraai"
image: ../../images/albumart/kaali.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UqIIpc1jIjg"
type: "love"
singers:
  - Vedala Hemachandra
  - Sangeetha Rajeshwaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaali kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorai yugam noorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorai yugam noorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai pirappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin imai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin imai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyai iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyai iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedaai unakku eedaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedaai unakku eedaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai naan koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naan koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethum unakku endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum unakku endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiraal thadupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiraal thadupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kadavulin parisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadavulin parisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Karangalil vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karangalil vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam enna purinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam enna purinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ingu pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ingu pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamena kidaithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamena kidaithaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalukkul irangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukkul irangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorai yugam noorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorai yugam noorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai pirappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin imai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin imai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyai iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyai iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallam serum vellam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam serum vellam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ninaithiduven thozhiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ninaithiduven thozhiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellam konjum undhan thimirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam konjum undhan thimirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti anaithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti anaithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathazhlum udalin meedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathazhlum udalin meedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai sumandhiduven thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai sumandhiduven thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam azhiyum bodho naanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam azhiyum bodho naanoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ninaithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ninaithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathin maiya paguthiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin maiya paguthiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkai virithu amarnthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkai virithu amarnthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathin motha magizhchiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathin motha magizhchiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthi vadivil koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi vadivil koduthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahaaa aaahaa nan nan nan nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaa aaahaa nan nan nan nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaa aaahaa nan nan nan nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaa aaahaa nan nan nan nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaahaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaahaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaahaaaaaa dhi rara rara naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaahaaaaaa dhi rara rara naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan madiyil nirantharamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan madiyil nirantharamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku idam koduthaai thozhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku idam koduthaai thozhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam vanthu nindraal koda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam vanthu nindraal koda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbida maruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbida maruthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku mattum thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku mattum thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan irudhayam thudikkum thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan irudhayam thudikkum thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangum botho enthan udhado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum botho enthan udhado"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan per azhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan per azhaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkullae ennai pudhaipadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkullae ennai pudhaipadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba thirumbi pirapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba thirumbi pirapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi thunai nee irupadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thunai nee irupadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhi varaikkum nadappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhi varaikkum nadappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorai yugam noorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorai yugam noorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai pirappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin imai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin imai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyai iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyai iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedaai unakku eedaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedaai unakku eedaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai naan koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naan koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethum unakku endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum unakku endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiraal thadupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiraal thadupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kadavulin parisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadavulin parisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Karangalil vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karangalil vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam enna purinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam enna purinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai ingu pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ingu pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamena kidaithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamena kidaithaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalukkul irangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukkul irangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorai yugam noorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorai yugam noorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaai pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaai pirappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannin imai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin imai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyai iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyai iruppen"/>
</div>
</pre>
