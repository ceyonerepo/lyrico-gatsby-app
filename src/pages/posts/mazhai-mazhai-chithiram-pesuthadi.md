---
title: "mazhai mazhai song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Vairamuthu"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Mazhai Mazhai"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nb0zuxyF7U0"
type: "love"
singers:
  - Afsal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mazhai mazhai pudhu mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai mazhai pudhu mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul thooruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul thooruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil nanaindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil nanaindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">alai alai pudhu alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai alai pudhu alai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukku adikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukku adikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil nanaindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil nanaindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin karuvinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin karuvinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyin ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyin ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mazhai mazhai pudhu mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai mazhai pudhu mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul thooruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul thooruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil nanaindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil nanaindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">alai alai pudhu alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai alai pudhu alai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukku adikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukku adikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil nanaindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil nanaindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin karuvinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin karuvinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyin ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyin ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pani pani ven pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani pani ven pani"/>
</div>
<div class="lyrico-lyrics-wrapper">ullaththil peiyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullaththil peiyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kulirndhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kulirndhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">maeniyil urasuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maeniyil urasuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kulirndhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kulirndhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin avasthayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin avasthayai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkayin avasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkayin avasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pani pani ven pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani pani ven pani"/>
</div>
<div class="lyrico-lyrics-wrapper">ullaththil peiyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullaththil peiyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kulirndhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kulirndhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">maeniyil urasuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maeniyil urasuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kulirndhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kulirndhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin avasthayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin avasthayai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkayin avasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkayin avasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minmini minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalil parakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalil parakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kalandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kalandhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kulir kuliril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kulir kuliril"/>
</div>
<div class="lyrico-lyrics-wrapper">irudhayam surunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irudhayam surunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kalandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kalandhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin perumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin perumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkayin muzhumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkayin muzhumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minmini minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalil parakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalil parakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kalandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kalandhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kulir kuliril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kulir kuliril"/>
</div>
<div class="lyrico-lyrics-wrapper">irudhayam surunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irudhayam surunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalil kalandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalil kalandhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin perumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin perumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkayin muzhumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkayin muzhumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai arindhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai arindhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
<div class="lyrico-lyrics-wrapper">wou wooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wou wooo"/>
</div>
</pre>
