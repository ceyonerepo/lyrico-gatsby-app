---
title: "yaar vazhiyil song lyrics"
album: "Sila Nerangalil Sila Manidhargal"
artist: "Radhan"
lyricist: "Snehan"
director: "Vishal Venkat"
path: "/albums/sila-nerangalil-sila-manidhargal-lyrics"
song: "Yaar Vazhiyil"
image: ../../images/albumart/sila-nerangalil-sila-manidhargal.jpg
date: 2022-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zHo0pg-4OJM"
type: "mass"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar vazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar mozhiyil yaar thunaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar mozhiyil yaar thunaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal vandhu serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal vandhu serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar vidhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vidhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sadhiyil yaar madhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sadhiyil yaar madhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal vandhu nerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal vandhu nerumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theervugal indriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervugal indriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam dhaan neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam dhaan neelumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarndhidum thunbathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarndhidum thunbathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam dhaan thaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam dhaan thaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etthisai emakkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthisai emakkena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal thanai kaatumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal thanai kaatumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvazhi nambikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvazhi nambikai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai vandhu serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai vandhu serumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvum kadandhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum kadandhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena ninaithom yaavum poidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena ninaithom yaavum poidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiye vendru theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiye vendru theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena padithom yaavum meidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena padithom yaavum meidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaridam muraiyida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam muraiyida"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumillai vidaiyai sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillai vidaiyai sollida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro thodarndha vazhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro thodarndha vazhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyaldhanai tharuma namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaldhanai tharuma namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam namadhu ilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam namadhu ilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai thalarndhu mudamaai kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai thalarndhu mudamaai kidakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munvinai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munvinai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinvinai thedumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinvinai thedumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvazhi paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvazhi paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaidhaan odumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaidhaan odumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaranam yaarendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam yaarendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulai ketpadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai ketpadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathai ketpadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathai ketpadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ingu pesuvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ingu pesuvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam polathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam polathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaadha kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaadha kelvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaitheda thedathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaitheda thedathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Virisalgal nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virisalgal nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palaneram vaazhkaiku baliyaagum maanudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaneram vaazhkaiku baliyaagum maanudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paripona yaavaiyum kidaikaadhu meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paripona yaavaiyum kidaikaadhu meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaigal enbadhu edhuvaraiyil neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaigal enbadhu edhuvaraiyil neelumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai keetrugal adhuvaraiyil vaazhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai keetrugal adhuvaraiyil vaazhumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silandhiyin koodena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silandhiyin koodena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhanaigal yaavumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhanaigal yaavumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalgalil aadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalgalil aadudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidharidhaan pogudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidharidhaan pogudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhudhaadha naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhudhaadha naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyakkuvadhu yaaringe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyakkuvadhu yaaringe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivenna enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivenna enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar solvaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar solvaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyaadha paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaadha paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadha thedalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaadha thedalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengo pogudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengo pogudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingu illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingu illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvum kadandhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum kadandhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena ninaithom yaavum poidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena ninaithom yaavum poidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiye vendru theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiye vendru theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena padithom yaavum meidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena padithom yaavum meidhaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaridam muraiyida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam muraiyida"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumillai vidaiyai sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillai vidaiyai sollida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro thodarndha vazhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro thodarndha vazhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyaldhanai tharuma namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaldhanai tharuma namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam namadhu ilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam namadhu ilakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai thalarndhu mudamaai kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai thalarndhu mudamaai kidakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar mozhiyil yaar thunaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar mozhiyil yaar thunaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal vandhu serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal vandhu serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar vidhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vidhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sadhiyil yaar madhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sadhiyil yaar madhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal vandhu nerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal vandhu nerumo"/>
</div>
</pre>
