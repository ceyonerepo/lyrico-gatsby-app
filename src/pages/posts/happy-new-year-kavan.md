---
title: "happy new year song lyrics"
album: "Kavan"
artist: "Hiphop Tamizha"
lyricist: "Arunraja Kamaraj"
director: "K V Anand"
path: "/albums/kavan-lyrics"
song: "Happy New Year"
image: ../../images/albumart/kavan.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mnBUaFo9VQA"
type: "happy"
singers:
  - T Rajendar
  - Hiphop Tamizha
  - Madonna Sebastian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Happy happy new year-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy new year-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai ellam over-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai ellam over-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyama velai senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyama velai senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinjidum da drawer-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjidum da drawer-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy happy new year-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy new year-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai venam poyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai venam poyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yar enna sonna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar enna sonna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtam pola nee iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam pola nee iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heiadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heiadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkakka rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkakka rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkakka rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkakka rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkakka rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkakka rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara rekkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara rekkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaiya marakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiya marakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkaiya virikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkaiya virikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiya pola than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaiya pola than"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky la parakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky la parakkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palasa erikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palasa erikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa porakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa porakkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraikura alu kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraikura alu kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilichi kattalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilichi kattalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai seiyum soolchi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai seiyum soolchi than"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamma illa poochi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamma illa poochi than"/>
</div>
<div class="lyrico-lyrics-wrapper">Saguni aatam adi paapoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saguni aatam adi paapoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varusham porakkumbothu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varusham porakkumbothu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa marum neram than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa marum neram than"/>
</div>
<div class="lyrico-lyrics-wrapper">Elasum podisum onnaa servooma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elasum podisum onnaa servooma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada attack u pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada attack u pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Attract pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attract pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Assault pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assault pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae vendravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae vendravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panbalae nindravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbalae nindravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen naalum yengha thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen naalum yengha thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Happy happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Happy happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happyhappy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happyhappy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy happy new year-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy new year-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai ellam over-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai ellam over-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyama velai senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyama velai senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinjidum da drawer-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjidum da drawer-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy happy new year-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy new year-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai venam poyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai venam poyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yar enna sonna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar enna sonna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtam pola nee iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam pola nee iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Are u ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Are u ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Get on the dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get on the dance floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One shot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One shot"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan ippo party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan ippo party"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam nadakutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam nadakutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Two shots
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two shots"/>
</div>
<div class="lyrico-lyrics-wrapper">You dont know me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You dont know me"/>
</div>
<div class="lyrico-lyrics-wrapper">Now ellam ennakuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now ellam ennakuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Three shots
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Three shots"/>
</div>
<div class="lyrico-lyrics-wrapper">Boys dancing all around me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys dancing all around me"/>
</div>
<div class="lyrico-lyrics-wrapper">I got four shots
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got four shots"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody gotta get it in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody gotta get it in"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on come on girls
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on girls"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on come on girls
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on girls"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada machii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada machii"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduren paaru switch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduren paaru switch-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivurai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivurai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyadhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyadhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakora moolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakora moolai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaiyadhu unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaiyadhu unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai podum dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai podum dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha adakki anduputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha adakki anduputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhu right-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu right-u "/>
</div>
<div class="lyrico-lyrics-wrapper">yedhu wrong-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedhu wrong-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikkamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosikkamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un heart-u adhu beat-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un heart-u adhu beat-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum nikkamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum nikkamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam work-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam work-u "/>
</div>
<div class="lyrico-lyrics-wrapper">adhil tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena sikkamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena sikkamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaiya maranga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiya maranga da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalaiyaa irunghadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalaiyaa irunghadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada attack u pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada attack u pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Attract pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attract pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Assault pandravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assault pandravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae vendravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae vendravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panbalae nindravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbalae nindravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen naalum yengha thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen naalum yengha thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year-u ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year-u ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan avannuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan avannuku "/>
</div>
<div class="lyrico-lyrics-wrapper">yegirum bp-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegirum bp-ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu podu podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varen paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varen paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Varen paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varen paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Varen paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varen paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Varen paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varen paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduren paaru switchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduren paaru switchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkara rekkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkara rekkara raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Really happy ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Really happy ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy new year ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year ya"/>
</div>
</pre>
