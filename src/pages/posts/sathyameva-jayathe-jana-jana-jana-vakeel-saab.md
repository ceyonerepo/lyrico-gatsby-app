---
title: "sathyameva jayathe song lyrics"
album: "Vakeel Saab"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Venu Sriram"
path: "/albums/vakeel-saab-lyrics"
song: "Sathyameva Jayathe - Jana Jana Jana"
image: ../../images/albumart/vakeel-saab.jpg
date: 2021-04-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tQk20LlPw5M"
type: "mass"
singers:
  - Shankar Mahadevan
  - Prudhvi Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jana jana jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana jana jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaganamuna kalagalisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaganamuna kalagalisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Janam manishi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam manishi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishi musirina kalalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishi musirina kalalanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana velugutho gelipinchu ghanudu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana velugutho gelipinchu ghanudu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi naligina bathukulakoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi naligina bathukulakoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Balamagu bujamivvagaladuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balamagu bujamivvagaladuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalane vadhaladu edurugaa thappu jarigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalane vadhaladu edurugaa thappu jarigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanilaa o galam mana vennu dhannai poradithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanilaa o galam mana vennu dhannai poradithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyameva jayathe sathyameva jayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyameva jayathe sathyameva jayathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyameva jayathe sathyameva jayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyameva jayathe sathyameva jayathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana jana jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana jana jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaganamuna kalagalisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaganamuna kalagalisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Janam manishi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam manishi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishi musirina kalalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishi musirina kalalanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana velugutho gelipinchu ghanudu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana velugutho gelipinchu ghanudu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi naligina bathukulakoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi naligina bathukulakoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Balamagu bujamivvagaladuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balamagu bujamivvagaladuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundetho spandhisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundetho spandhisthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaga cheyandhisthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaga cheyandhisthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ila chempa jaaredi akari asruvunaa pedi varaku anunityam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila chempa jaaredi akari asruvunaa pedi varaku anunityam"/>
</div>
<div class="lyrico-lyrics-wrapper">Balahinulandariki ummadi gonthugaa poratame thana karthavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balahinulandariki ummadi gonthugaa poratame thana karthavyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakaal thapuchukuni vaadhinche vakilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakaal thapuchukuni vaadhinche vakilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedholla pakkanundi kattisthadu bakilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedholla pakkanundi kattisthadu bakilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Betthamlaa churrumani kakkisthadu nijalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Betthamlaa churrumani kakkisthadu nijalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motthanga nyayaniki pettisthadu dhandalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motthanga nyayaniki pettisthadu dhandalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittanti okkadunte anthe chaalanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittanti okkadunte anthe chaalanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthetthi prashninchado antha nichhinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthetthi prashninchado antha nichhinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettanti anyayalu thaletthavanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettanti anyayalu thaletthavanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Moretthey mosagalla pattha gallanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moretthey mosagalla pattha gallanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyameva jayathe sathyameva jayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyameva jayathe sathyameva jayathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyameva jayathe sathyameva jayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyameva jayathe sathyameva jayathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyameva jayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyameva jayathe"/>
</div>
</pre>
