---
title: "universal cop song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "	Viveka - Dinesh Kanagaratnam"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Universal cop"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/On87jKwiKJY"
type: "Mass"
singers:
  - Christopher Stanley
  - Dinesh Kanagaratnam
  - Krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kakki sattai potaan.. paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakki sattai potaan.. paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thekki vacha kobam… nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekki vacha kobam… nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakka poran thaaru… maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakka poran thaaru… maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga pora aalu… yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga pora aalu… yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inai kidaiyaathu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai kidaiyaathu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop.."/>
</div>
<div class="lyrico-lyrics-wrapper">Very very sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very sharp"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ongi ivan potta.. pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi ivan potta.. pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai tonnu weight ah.. maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai tonnu weight ah.. maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eri ivan adicha… pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eri ivan adicha… pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevuganai pola … thakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevuganai pola … thakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inai illai yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai illai yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop…mm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop…mm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Very very sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very sharp"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udai pottavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai pottavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithana oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithana oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai pottu varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai pottu varuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae paarvayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae paarvayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu aalaiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu aalaiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edai pottu viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edai pottu viduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udai pottavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai pottavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithana oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithana oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai pottu varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai pottu varuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae paarvayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae paarvayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu aalaiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu aalaiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edai pottu viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edai pottu viduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arai onnu vitta kan patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arai onnu vitta kan patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kalla ullam ellaam odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kalla ullam ellaam odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu naadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu naadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaikku yetta oru vanda..aa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaikku yetta oru vanda..aa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan theervukaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan theervukaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollaigalum unda unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaigalum unda unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo.. ivan romba sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo.. ivan romba sharp"/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehe..very very sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe..very very sharp"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai athi oru thadai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai athi oru thadai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Va da thaandalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da thaandalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on rappa tap tap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on rappa tap tap"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vidha mana routai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vidha mana routai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatalaam irangi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatalaam irangi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka rappa tap tap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka rappa tap tap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai athi oru thadai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai athi oru thadai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Va da thaandalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da thaandalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on rappa tap tap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on rappa tap tap"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vidha mana routai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vidha mana routai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatalaam irangi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatalaam irangi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka rappa tap tap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka rappa tap tap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaba gaba ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba gaba ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalai vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalai vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada pada ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaigal parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigarangal thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigarangal thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan vizhi sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan vizhi sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinil ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinil ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithathu nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathu nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap pap pap pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap pap pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Pabapap papap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapap papap"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra sakka kadrra rekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra sakka kadrra rekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pap pap pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pap pap pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Pabapap papap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pabapap papap"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra sakka kadrra rekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra sakka kadrra rekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulaikindra oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulaikindra oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudipodu ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipodu ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaakkal irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaakkal irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappanavanai kandaalae athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappanavanai kandaalae athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagipodu kuthikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagipodu kuthikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sabalangal theenda …yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabalangal theenda …yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru aaluda…oohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aaluda…oohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan sattam kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan sattam kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaiyum thaan thaandu vaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiyum thaan thaandu vaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarachum kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarachum kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru punnagaiyai veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru punnagaiyai veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai thallu vaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai thallu vaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo.. ivan romba sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo.. ivan romba sharp"/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehe..very very sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe..very very sharp"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kakki sattai potaan.. paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakki sattai potaan.. paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thekki vacha kobam.. nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekki vacha kobam.. nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakka poran thaaru… maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakka poran thaaru… maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga pora aalu.. yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga pora aalu.. yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inai kidayaathu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai kidayaathu.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very sharp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very sharp"/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra sakka kadrra rekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra sakka kadrra rekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah reba reba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah reba reba"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah reba reba raah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah reba reba raah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Universal cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Universal cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra sakka kadrra rekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra sakka kadrra rekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah reba reba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah reba reba"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah reba reba reba reba raba raba raah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah reba reba reba reba raba raba raah"/>
</div>
</pre>
