---
title: "maayangal song lyrics"
album: "Kutty Story"
artist: "Karthik"
lyricist: "Madhan Karky"
director: "Gautham Vasudev Menon - Vijay - Venkat Prabhu - Nalan Kumarasamy"
path: "/albums/kutty-story-song-lyrics"
song: "Maayangal"
image: ../../images/albumart/kutty-story.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cIse3U3o3ME"
type: "Love"
singers:
  - Krishna K
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anginga naalubadi anaithizhukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anginga naalubadi anaithizhukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana illathabadi ennai izhakka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana illathabadi ennai izhakka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum sellathabadi vasithirikka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum sellathabadi vasithirikka poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaiye kekkuren kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiye kekkuren kekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalai thekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai thekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayamum yekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayamum yekkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaiye kekkuren kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiye kekkuren kekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam serkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam serkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thoda paarkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thoda paarkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyila karaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyila karaiyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayangal manmadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal manmadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathangal vanjagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathangal vanjagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil naanum en arugil neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil naanum en arugil neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral ennaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reengaram silmisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reengaram silmisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal kandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal kandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil naanum en arugil neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil naanum en arugil neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral ennaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaroda yaar sikki poraada poromo inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroda yaar sikki poraada poromo inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroda bodhaikku thalladi theepomo inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroda bodhaikku thalladi theepomo inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pal illa meen onnu painthodum kaattara thinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pal illa meen onnu painthodum kaattara thinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaiye kekkuren kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiye kekkuren kekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam serkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam serkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thoda paarkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thoda paarkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyila karaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyila karaiyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boogambam poo mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogambam poo mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean kinnam theipirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean kinnam theipirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil naanum en arugil neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil naanum en arugil neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral ennaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevai idhu thandanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai idhu thandanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamangal niththirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamangal niththirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil naanum en arugil neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil naanum en arugil neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral ennaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayangal manmadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal manmadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathangal vanjagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathangal vanjagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil un arugil un arugil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil un arugil un arugil naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayangal manmadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal manmadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathangal vanjagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathangal vanjagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral nindral nindral ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral nindral nindral ennaagum"/>
</div>
</pre>
