---
title: "ithu jaikkapora song lyrics"
album: "Dhoni Kabadi Kuzhu"
artist: "Roshan Joseph"
lyricist: "N Rasa"
director: "P Iyyappan"
path: "/albums/dhoni-kabadi-kuzhu-lyrics"
song: "Ithu Jaikkapora"
image: ../../images/albumart/dhoni-kabadi-kuzhu.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aaASayzq4_w"
type: "mass"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ithu jaikapora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu jaikapora "/>
</div>
<div class="lyrico-lyrics-wrapper">jaikapora kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaikapora kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalaka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalaka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaka pora aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaka pora aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu jaikapora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu jaikapora "/>
</div>
<div class="lyrico-lyrics-wrapper">jaikapora kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaikapora kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalaka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalaka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaka pora aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaka pora aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kannula illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kannula illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula illa thookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula illa thookam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu latchiyam vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu latchiyam vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyam vellum nokkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyam vellum nokkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulaikkamal nee ulaikkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikkamal nee ulaikkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum kidaikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum kidaikathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unaraamal unai unaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaraamal unai unaraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum nadakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum nadakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee muyarchi seithu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee muyarchi seithu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mugavariyum kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mugavariyum kidaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee muyarchi seithu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee muyarchi seithu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mugavariyum kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mugavariyum kidaikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi kodi panam irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodi panam irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu uthava maatan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu uthava maatan da"/>
</div>
<div class="lyrico-lyrics-wrapper">karunan pola kudupavan than da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunan pola kudupavan than da"/>
</div>
<div class="lyrico-lyrics-wrapper">irantha pinnum nilaipan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irantha pinnum nilaipan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muyarchi seithu parkum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muyarchi seithu parkum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">mutu katta podum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutu katta podum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">jaitha piragu thaane unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaitha piragu thaane unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai vanthu thatum thatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai vanthu thatum thatum"/>
</div>
<div class="lyrico-lyrics-wrapper">oora koodi thaduthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora koodi thaduthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kootam pathungaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kootam pathungaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalipam than engal kotam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalipam than engal kotam"/>
</div>
<div class="lyrico-lyrics-wrapper">vairakiyam engal nokkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vairakiyam engal nokkam"/>
</div>
<div class="lyrico-lyrics-wrapper">sura meena pola neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sura meena pola neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">epoluthum neenthi vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epoluthum neenthi vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu jaikapora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu jaikapora "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu jaikapora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu jaikapora "/>
</div>
<div class="lyrico-lyrics-wrapper">jaikapora kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaikapora kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kalaka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kalaka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaka pora aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaka pora aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kannula illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kannula illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula illa thookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula illa thookam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu latchiyam vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu latchiyam vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyam vellum nokkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyam vellum nokkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulaikkamal nee ulaikkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikkamal nee ulaikkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum kidaikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum kidaikathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unaraamal unai unaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaraamal unai unaraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum nadakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum nadakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee muyarchi seithu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee muyarchi seithu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mugavariyum kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mugavariyum kidaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee muyarchi seithu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee muyarchi seithu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mugavariyum kidaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mugavariyum kidaikum"/>
</div>
</pre>