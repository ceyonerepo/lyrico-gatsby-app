---
title: "adi pappali palame song lyrics"
album: "Maniyaar Kudumbam"
artist: "Thambi Ramaiah"
lyricist: "Thambi Ramaiah"
director: "Thambi Ramaiah"
path: "/albums/maniyaar-kudumbam-lyrics"
song: "Adi Pappali Palame"
image: ../../images/albumart/maniyaar-kudumbam.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9wHBZ1OZ85k"
type: "happy"
singers:
  - Jithin Raj
  - Sooraj Santhosh
  - Surmukhi Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaathadikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakalangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalliyambatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalliyambatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neru nerungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neru nerungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallika poovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallika poovu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottathukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovula ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovula ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenolugudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenolugudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandugal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandugal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi parakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi parakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olugura thaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olugura thaena"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhatula sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatula sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappudhu sappudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappudhu sappudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi pappali palamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pappali palamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thakkaali rasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thakkaali rasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta enna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta enna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadi sollalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadi sollalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela thaandi jollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela thaandi jollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi pappali palamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pappali palamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thakkaali rasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thakkaali rasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta enna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta enna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadi sollalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadi sollalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela thaandi jollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela thaandi jollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi urula kalangu varuvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi urula kalangu varuvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna kelangu thuvayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna kelangu thuvayala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyilae kachiriyae di kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyilae kachiriyae di kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athula thoththu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athula thoththu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Foriegn la vikira fullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foriegn la vikira fullu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada moodumandhira raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada moodumandhira raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">En mundhiri saaraaya seesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mundhiri saaraaya seesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Side la thaan podiriyae route -uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side la thaan podiriyae route -uh"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasula thaan yerudhada beat-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasula thaan yerudhada beat-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pongi paayum aaru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pongi paayum aaru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha potta kozhi jodi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha potta kozhi jodi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambu podachu paaduriyae paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambu podachu paaduriyae paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varamba odachi unna enakku kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varamba odachi unna enakku kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi pappali palamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pappali palamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thakkaali rasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thakkaali rasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta enna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta enna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadi sollalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadi sollalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela thaandi jollu hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela thaandi jollu hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaadhoram aadu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhoram aadu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagam paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagam paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku madhiyaana neram kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku madhiyaana neram kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogham kuduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogham kuduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un velanji kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un velanji kedakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamba paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamba paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram porakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram porakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha samanja poovu aruvadaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha samanja poovu aruvadaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram sumakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram sumakudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanum ponnum paathi paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum ponnum paathi paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seta seiyura boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seta seiyura boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna allikittu poren naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna allikittu poren naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna pasikkum thinna serikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pasikkum thinna serikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu oru unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu oru unavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu munnala kaathu kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu munnala kaathu kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya vechi pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya vechi pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malayil oru kuruvi kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayil oru kuruvi kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya thaan sikkikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya thaan sikkikichaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedan kan paaka paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedan kan paaka paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil thee pathikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil thee pathikichaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikichaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikichaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi pappali palamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pappali palamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thakkaali rasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thakkaali rasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta Hahhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta Hahhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakka kitta enna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakka kitta enna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadi sollalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadi sollalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela thaandi jollu hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela thaandi jollu hoi"/>
</div>
</pre>
