---
title: "chittuku chittuku song lyrics"
album: "A1"
artist: "Santhosh Narayanan"
lyricist: "Rokesh"
director: "Johnson K"
path: "/albums/a1-lyrics"
song: "Chittuku Chittuku"
image: ../../images/albumart/a1.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7YL8hc55dWQ"
type: "love"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Majiliya Suththa Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliya Suththa Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga Raattinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Raattinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engamma Vanda Pondattinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engamma Vanda Pondattinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kaattanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaattanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-oda Rasagullava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-oda Rasagullava"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Oottanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Oottanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thottil Katti En Kozhandhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thottil Katti En Kozhandhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aattanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aattanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lippulathaan Kissu Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lippulathaan Kissu Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valichinnu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valichinnu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyya Molchama Munnadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya Molchama Munnadithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Podura Scene Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podura Scene Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkala Pudichitenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkala Pudichitenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappeesu Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappeesu Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu Kirukkala Aakitada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu Kirukkala Aakitada"/>
</div>
<div class="lyrico-lyrics-wrapper">Locallu Donna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locallu Donna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Mittaava Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mittaava Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutta Rocketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutta Rocketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutta Bulletta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Bulletta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Achcha Asaltta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcha Asaltta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen Clean Out Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Clean Out Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu Ulla Poondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Ulla Poondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene Ah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Ah Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innamaa Pesuramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innamaa Pesuramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innadhamaa Un Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innadhamaa Un Kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innila Irunthu Nee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innila Irunthu Nee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesathukkum Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesathukkum Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fittaaga Kattu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fittaaga Kattu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavudhama En Sadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavudhama En Sadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennikkum Summa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennikkum Summa Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vuttu Povatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vuttu Povatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adagu Vekka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagu Vekka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Pithala Kodaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Pithala Kodaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Un Kuda Oora Suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Un Kuda Oora Suththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanguven Kadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanguven Kadatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jora Bejaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jora Bejaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Love-ah Kudutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Love-ah Kudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendi Thanthu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Thanthu Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuluru Joraththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuluru Joraththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Flow-vaana Un Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flow-vaana Un Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Slow-va Therithu Ethurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slow-va Therithu Ethurula"/>
</div>
<div class="lyrico-lyrics-wrapper">Showvaaga Sokka Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showvaaga Sokka Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Soljavaana Nadaiyula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soljavaana Nadaiyula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaavi Kattikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi Kattikadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Ennikkum Thadaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Ennikkum Thadaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaviye Mayanguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviye Mayanguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalthikamma Madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalthikamma Madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattivuda Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattivuda Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Leasu Patta Figure Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leasu Patta Figure Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attivudum Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attivudum Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku BP Sugar Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku BP Sugar Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethira Nikkura Maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethira Nikkura Maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum Koosum Colourulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Koosum Colourulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mama Unakku Naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mama Unakku Naina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Tighta Kusttu Olarula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Tighta Kusttu Olarula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Majiliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Majiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Majiliya Majiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliya Majiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Majiliya Majiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliya Majiliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittukku Chittukku Chittukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittukku Chittukku Chittukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Majiliya Majiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliya Majiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Majiliya Majiliyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliya Majiliyaaaa"/>
</div>
</pre>
