---
title: "oru malai pozhuthil song lyrics"
album: "Theneer Viduthi"
artist: "S.S. Kumaran"
lyricist: "Na. Muthukumar"
director: "S.S. Kumaran"
path: "/albums/theneer-viduthi-lyrics"
song: "Oru Malai Pozhuthil"
image: ../../images/albumart/theneer-viduthi.jpg
date: 2011-07-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TUp6cvCkyXc"
type: "sad"
singers:
  - Sona Mohapatra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru maalaipozhudhil naan unaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maalaipozhudhil naan unaippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai paarthen anbey naan enaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai paarthen anbey naan enaippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">urangaamal unaikkaangiren O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaamal unaikkaangiren O"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkaaga uyi vaazhgiren O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaaga uyi vaazhgiren O O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru maalaipozhudhil naan unaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maalaipozhudhil naan unaippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai paarthen anbey naan enaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai paarthen anbey naan enaippaarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaalai ninaivugal azhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai ninaivugal azhaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">anaiyaamal neruppena kodhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaiyaamal neruppena kodhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaadhal yaar pagal mazhai vandhu anaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaadhal yaar pagal mazhai vandhu anaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaadha aasaigal irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaadha aasaigal irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaalum mounangal thadukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaalum mounangal thadukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhil adiyinil ilaiyellaam thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhil adiyinil ilaiyellaam thavikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhupoal uyirkkaadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhupoal uyirkkaadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">iru uyiril varum moadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru uyiril varum moadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">neney naan ullavaraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neney naan ullavaraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">unai sumappen idhayathin araiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai sumappen idhayathin araiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru maalaipozhudhil naan unaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maalaipozhudhil naan unaippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai paarthen anbey naan enaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai paarthen anbey naan enaippaarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thedi thaer vandha velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi thaer vandha velai"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhimaari poanadhu paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhimaari poanadhu paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyaamal kannanai pirindhaal raadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyaamal kannanai pirindhaal raadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">idimoadhi idhayathil paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idimoadhi idhayathil paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu thaano kaadhalin geedhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu thaano kaadhalin geedhai"/>
</div>
<div class="lyrico-lyrics-wrapper">varavenum koakula thoattathil naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavenum koakula thoattathil naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">medhuvaai ennai varudavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medhuvaai ennai varudavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu muzhuthaai ennai thirudavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu muzhuthaai ennai thirudavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than en uyir nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than en uyir nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini nee than en uyir nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini nee than en uyir nee than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru maalaipozhudhil naan unaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maalaipozhudhil naan unaippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unai paarthen anbey naan enaippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai paarthen anbey naan enaippaarthen"/>
</div>
</pre>
