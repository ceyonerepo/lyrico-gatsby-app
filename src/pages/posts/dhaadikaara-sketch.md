---
title: "dhaadikaara song lyrics"
album: "Sketch"
artist: "S. Thaman"
lyricist: "Vivek"
director: "Vijay Chandar"
path: "/albums/sketch-lyrics"
song: "Dhaadikaara"
image: ../../images/albumart/sketch.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NE8wjr0DNEE"
type: "love"
singers:
  - S. Thaman
  - Sudha Ragunathan
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikaara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikaara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Sella Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Sella Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Nenjil Pooti Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nenjil Pooti Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikkara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikkara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Thedi Mutham Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Thedi Mutham Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kodi Artham Thaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kodi Artham Thaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mellathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vida Unnai Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Unnai Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Nerungida Parpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Nerungida Parpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamale Ulle Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamale Ulle Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellodu En Unarvugal Korpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellodu En Unarvugal Korpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Kondadida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Kondadida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraayiram Iruvagal Serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraayiram Iruvagal Serpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa En Uyire Aruge Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa En Uyire Aruge Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikaara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikaara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Sella Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Sella Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Nenjil Pooti Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nenjil Pooti Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikkara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikkara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Thedi Mutham Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Thedi Mutham Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kodi Artham Thaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kodi Artham Thaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mellathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Siraginal Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Siraginal Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sinugalil Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sinugalil Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Udhadinil Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Udhadinil Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Neeye Neeye Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Neeye Neeye Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kanavugal Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavugal Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thavuragal Neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thavuragal Neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Urasalil Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Urasalil Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Neeye Neeye Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Neeye Neeye Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pani Vilum Malar Vanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Vilum Malar Vanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithatume Anaithum Marenthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithatume Anaithum Marenthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenadi Yenadi Yenadi Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadi Yenadi Yenadi Yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Vilum Pani Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Vilum Pani Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Nadhiyaai Mithanthen Magizhthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Nadhiyaai Mithanthen Magizhthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanadi Naanadi Naanadi Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanadi Naanadi Naanadi Naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikaara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikaara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vittu Sella Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vittu Sella Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Nenjil Pooti Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nenjil Pooti Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadikkara Dhaadikaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadikkara Dhaadikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Thedi Mutham Vaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Thedi Mutham Vaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kodi Artham Thaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kodi Artham Thaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mellathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vida Unnai Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Unnai Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Nerungida Parpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Nerungida Parpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamale Ulle Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamale Ulle Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellodu En Unarvugal Korpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellodu En Unarvugal Korpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Kondadida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Kondadida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraayiram Iruvagal Serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraayiram Iruvagal Serpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Va En Uyire Aruge Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va En Uyire Aruge Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi Adiyaathi Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi Adiyaathi Adiyaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaathi"/>
</div>
</pre>
