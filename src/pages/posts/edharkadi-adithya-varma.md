---
title: "edharkadi song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Vivek - Dhruv Vikram"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Edharkadi"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Y1JCAXAxoMg"
type: "sad"
singers:
  - Dhruv Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edharkadi Vali Thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharkadi Vali Thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Thollaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Thollaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkku Mel Vali Ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkku Mel Vali Ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivin Thaeniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivin Thaeniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Neerin Theeniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Neerin Theeniyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kondru Saaikavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kondru Saaikavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Vandhu Poi Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vandhu Poi Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Swaasam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Artham Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Artham Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thunbam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thunbam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathin Theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathin Theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann Marainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann Marainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjal Endru Naan Nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjal Endru Naan Nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nodi Kann Thiranthu Parkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nodi Kann Thiranthu Parkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyilae Kidakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyilae Kidakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thavira Edhuvumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thavira Edhuvumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Pona Vedhanaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Pona Vedhanaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam Pottu Ennai Nerukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam Pottu Ennai Nerukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Theeyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Theeyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Kangal Thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kangal Thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Swaasam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Artham Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Artham Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thunbam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thunbam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathin Theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathin Theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Swaasam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Artham Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Artham Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thunbam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thunbam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathin Theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathin Theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Distance Makes U Distant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Distance Makes U Distant"/>
</div>
<div class="lyrico-lyrics-wrapper">B Coz Existence
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B Coz Existence"/>
</div>
<div class="lyrico-lyrics-wrapper">Got Me Thinking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got Me Thinking"/>
</div>
<div class="lyrico-lyrics-wrapper">Now For Instance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now For Instance"/>
</div>
<div class="lyrico-lyrics-wrapper">If I Insisted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If I Insisted"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">An Answer To These Questions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An Answer To These Questions"/>
</div>
<div class="lyrico-lyrics-wrapper">What Were You On A Leash?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Were You On A Leash?"/>
</div>
<div class="lyrico-lyrics-wrapper">Which Made Me A Stray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Which Made Me A Stray"/>
</div>
<div class="lyrico-lyrics-wrapper">From The Street?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="From The Street?"/>
</div>
<div class="lyrico-lyrics-wrapper">Is This What They Call “defeat”
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is This What They Call “defeat”"/>
</div>
<div class="lyrico-lyrics-wrapper">Coz, Baby Then This Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coz, Baby Then This Time"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll Bring A Fuc..ing “fleet”
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll Bring A Fuc..ing “fleet”"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edharkadi Vali Thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharkadi Vali Thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Thollaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Thollaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkku Mel Vali Ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkku Mel Vali Ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Illaiyae"/>
</div>
</pre>
