---
title: "title track song lyrics"
album: "Trance"
artist: "Jackson Vijayan - Vinayakan"
lyricist: "Vinayak Sasikumar"
director: "Anwar Rasheed"
path: "/albums/trance-lyrics"
song: "Title Track"
image: ../../images/albumart/trance.jpg
date: 2020-02-20
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/u-DdeAPPq-Y"
type: "title track"
singers:
  - Neha Nair
  - Lee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sathyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Divyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maargam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Divyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maargam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhayathil Avan Kithaykilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayathil Avan Kithaykilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayathe Avan Jayikunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayathe Avan Jayikunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Avan Pirakunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Avan Pirakunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Avan Pirakunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Avan Pirakunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shoonyathe Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shoonyathe Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhathe Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhathe Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhakaarame Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhakaarame Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shoonyathe Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shoonyathe Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhathe Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhathe Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhakaarame Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhakaarame Po"/>
</div>
</pre>
