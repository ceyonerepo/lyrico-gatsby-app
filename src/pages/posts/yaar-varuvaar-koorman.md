---
title: "yaar varuvaar song lyrics"
album: "Koorman"
artist: "Tony Britto"
lyricist: "Bryan B. George "
director: "Bryan B. George"
path: "/albums/koorman-lyrics"
song: "Yaar Varuvaar"
image: ../../images/albumart/koorman.jpg
date: 2022-02-11
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaar varuvaar yaar iruppar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar varuvaar yaar iruppar"/>
</div>
<div class="lyrico-lyrics-wrapper">yennodu ini kathaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennodu ini kathaika"/>
</div>
<div class="lyrico-lyrics-wrapper">en veetil enai sirika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veetil enai sirika"/>
</div>
<div class="lyrico-lyrics-wrapper">otrai siragal parakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai siragal parakum "/>
</div>
<div class="lyrico-lyrics-wrapper">paravaiyai nanum parakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyai nanum parakiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vetru udalal alaiyum pinamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetru udalal alaiyum pinamai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thirigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thirigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaaaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaaaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thisai engu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai engu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pin thodarum nilamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin thodarum nilamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">unai nanum paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai nanum paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyatha en vaanin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyatha en vaanin"/>
</div>
<div class="lyrico-lyrics-wrapper">suriyanin kathiraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suriyanin kathiraga"/>
</div>
<div class="lyrico-lyrics-wrapper">ennul padargirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennul padargirai"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai polivor ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai polivor ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadum valiyai tharuvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadum valiyai tharuvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">theevam anainthathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theevam anainthathume"/>
</div>
<div class="lyrico-lyrics-wrapper">oli maayam aavathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli maayam aavathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaar varuvarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaar varuvarooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nesipavar ellam nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesipavar ellam nammodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalthal pirivin vali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalthal pirivin vali "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyathu paarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyathu paarum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbirkaga yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbirkaga yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">yasagan naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yasagan naano"/>
</div>
<div class="lyrico-lyrics-wrapper">innum pirivai thanguvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum pirivai thanguvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimai theervanal pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimai theervanal pala"/>
</div>
<div class="lyrico-lyrics-wrapper">kathai sollume ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathai sollume ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">rathaiku kaathu irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathaiku kaathu irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">kannan naanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannan naanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar varuvaar yaar iruppar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar varuvaar yaar iruppar"/>
</div>
<div class="lyrico-lyrics-wrapper">yennodu ini kathaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennodu ini kathaika"/>
</div>
</pre>
