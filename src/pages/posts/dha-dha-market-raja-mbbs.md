---
title: "dha dha song lyrics"
album: "Market Raja MBBS"
artist: "Simon K. King"
lyricist: "Rokesh"
director: "Saran"
path: "/albums/market-raja-mbbs-lyrics"
song: "Dha Dha"
image: ../../images/albumart/market-raja-mbbs.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YqedRIOslww"
type: "mass"
singers:
  - Simon K. King
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Kaalathula Koovam Aathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaalathula Koovam Aathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Medhakkinu Vanthen Boat-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Medhakkinu Vanthen Boat-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhanginae Vantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhanginae Vantaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasae Illaama Suthunen Road-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasae Illaama Suthunen Road-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Appathaan Thirudunaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appathaan Thirudunaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Biscuitu Bottle-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biscuitu Bottle-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appo En Vayasu Paththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo En Vayasu Paththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appovae Annan Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appovae Annan Gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraamoottu Porulellaam Vithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraamoottu Porulellaam Vithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Sethaaru Neraiya Sothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Sethaaru Neraiya Sothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deii Appo Annan Saadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deii Appo Annan Saadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaa Dhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaa Dhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaa Dhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaa Dhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaa Dhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaa Dhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Vayasu 18
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Vayasu 18"/>
</div>
<div class="lyrico-lyrics-wrapper">Appothaan Vaangunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appothaan Vaangunen"/>
</div>
<div class="lyrico-lyrics-wrapper">First-u Vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaraiyum Thatti Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaraiyum Thatti Vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vetta Vandhavanungellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vetta Vandhavanungellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbi Paatha Oru Sittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Paatha Oru Sittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Pattunu Potaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Pattunu Potaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku Oru Bittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Oru Bittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Vanta Ava Purusanaiyae Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Vanta Ava Purusanaiyae Vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Appo Rombo Saadha Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Appo Rombo Saadha Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Ippo Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Ippo Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Watchala Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchala Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
21 <div class="lyrico-lyrics-wrapper">Vayasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kola Pandrathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Pandrathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Perusilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Perusilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Panam Velaaduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Panam Velaaduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Purse-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Purse-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Thaan Oru Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Thaan Oru Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa En Manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa En Manasula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Voottu Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Voottu Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaavaana Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaavaana Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkaetha Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaetha Beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Aanjinu Irundha Botti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Aanjinu Irundha Botti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Panrennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Panrennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthen Rose-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthen Rose-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Vanginnu Saroja Devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Vanginnu Saroja Devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadhiri Kodutha Oru Posu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadhiri Kodutha Oru Posu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thideerunnu Voonanttaaya Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thideerunnu Voonanttaaya Loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annailenthu Naan Devadasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annailenthu Naan Devadasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegapatta Case-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegapatta Case-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangukku Naan Bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangukku Naan Bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyila Jailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyila Jailu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saingaalam Bailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saingaalam Bailu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Kodutha Vettuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Kodutha Vettuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch Pottu Thattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch Pottu Thattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhaingana Kottuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhaingana Kottuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalla Vizhundha Eh Chei Pehh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalla Vizhundha Eh Chei Pehh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesavae Maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesavae Maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Pera Potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pera Potten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pola Oruthana Kaattae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pola Oruthana Kaattae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiyila Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiyila Sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettuvenda Veriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettuvenda Veriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaiyil Serndhadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaiyil Serndhadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa Karaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Karaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Noo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Noo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhu Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhu Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae Nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae Nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri Enna Bayathula Nerunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri Enna Bayathula Nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam Potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Potten"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhiyum Adanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhiyum Adanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambichadhu En Raajiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambichadhu En Raajiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarambichadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambichadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajiyam Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajiyam Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambichadhu Raajiyam Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambichadhu Raajiyam Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajiyam Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajiyam Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajiyam Thodanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajiyam Thodanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Dhaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Dhaadhaa"/>
</div>
</pre>
