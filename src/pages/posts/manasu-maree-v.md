---
title: "manasu maree song lyrics"
album: "V"
artist: "Amit Trivedi"
lyricist: "Sirivennela Seetharama Sastry"
director: "Mohana Krishna - Indraganti"
path: "/albums/v-lyrics"
song: "Manasu Maree"
image: ../../images/albumart/v.jpg
date: 2020-09-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Pal0uollc4E"
type: "love"
singers:
  - Amit Trivedi
  - Shashaa Tirupati
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasu maree matthugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu maree matthugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thugipo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugipo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu maree vinthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu maree vinthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">visthubo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visthubo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede ee leela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede ee leela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthaga kavisthavem gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaga kavisthavem gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke bandhincheiyi nunmali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke bandhincheiyi nunmali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiladi komali guleba kawali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiladi komali guleba kawali"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukala jawali vinali kougili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukala jawali vinali kougili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu maree matthugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu maree matthugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thugipo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugipo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu maree vinthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu maree vinthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">visthubo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visthubo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede ee leela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede ee leela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho adugulo aduguvai ila ra natho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho adugulo aduguvai ila ra natho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyam varanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyam varanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan bathukulo bathukunai nivedhista
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan bathukulo bathukunai nivedhista"/>
</div>
<div class="lyrico-lyrics-wrapper">Na sarvam jahapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na sarvam jahapana"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola nawa galithowa haylo hayleso
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola nawa galithowa haylo hayleso"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheraneewa cheyaneewa sevalevewo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheraneewa cheyaneewa sevalevewo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu maree matthugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu maree matthugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thugipo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugipo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho vayasu maree vinthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho vayasu maree vinthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">visthubo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visthubo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede ee leela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede ee leela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasulo alalaye rahasyalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo alalaye rahasyalevo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe kshanam idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe kshanam idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manuvuthu modhalaye maro janmannayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manuvuthu modhalaye maro janmannayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Putte varamidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putte varamidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo vuncha na pranani chusi polchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo vuncha na pranani chusi polchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo pencha nee kalalani ugani uyallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo pencha nee kalalani ugani uyallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu maree matthugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu maree matthugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thugipo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugipo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu maree vinthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu maree vinthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">visthubo thunadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visthubo thunadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede ee leela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede ee leela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthaga kavisthavem gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaga kavisthavem gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke bandhincheiyi nunmali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke bandhincheiyi nunmali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiladi komali guleba kawali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiladi komali guleba kawali"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukala jawali vinali kougili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukala jawali vinali kougili"/>
</div>
</pre>
