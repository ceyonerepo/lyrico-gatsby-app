---
title: "jai balayya song lyrics"
album: "Akhanda"
artist: "S. Thaman"
lyricist: "Ananta Sriram"
director: "Boyapati Srinu"
path: "/albums/akhanda-lyrics"
song: "Jai Balayya"
image: ../../images/albumart/akhanda.jpg
date: 2021-12-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fHW8BjAK-2k"
type: "love"
singers:
  - Geetha Madhuri
  - Sahithi Chaganti
  - Satya Yamini
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kiya Kiya Jaadoo Kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya Kiya Jaadoo Kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Diya Diya Dil De Diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diya Diya Dil De Diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya Maiya Mama Miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Maiya Mama Miya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyya Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya Balayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiyya Thiyya Kaaralayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyya Thiyya Kaaralayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayya Rayya Maaralayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayya Rayya Maaralayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayya Thayya Thayyarayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayya Thayya Thayyarayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyya Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya Balayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiya Kiya Jaadoo Kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya Kiya Jaadoo Kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Diya Diya Dil De Diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diya Diya Dil De Diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya Maiya Mama Miya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Maiya Mama Miya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyya Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya Balayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiyya Thiyya Kaaralayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyya Thiyya Kaaralayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayya Rayya Maaralayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayya Rayya Maaralayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayya Thayya Thayyarayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayya Thayya Thayyarayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyya Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya Balayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoiyyare Hoiyya Muddhula Mavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiyyare Hoiyya Muddhula Mavayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobbari Kaaya Kottana Baavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobbari Kaaya Kottana Baavayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoiyyare Hoiyya Muddhula Mavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiyyare Hoiyya Muddhula Mavayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobbari Kaaya Kottana Baavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobbari Kaaya Kottana Baavayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katthule Dhoose Krishna Devaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthule Dhoose Krishna Devaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallatho Sesse Krishnudanti Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallatho Sesse Krishnudanti Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthuga Sootthe Poyinadhi Soya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthuga Sootthe Poyinadhi Soya"/>
</div>
<div class="lyrico-lyrics-wrapper">Motthanga Neeke Nennu Padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motthanga Neeke Nennu Padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kaala Gajja Kankalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kaala Gajja Kankalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veggu Sukkai Yelagalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veggu Sukkai Yelagalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Kalipi Step Ey Abbaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Kalipi Step Ey Abbaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaa Yaa Yaa Yaa Jai Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa Yaa Yaa Yaa Jai Balayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama Kirre Kuthandento Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama Kirre Kuthandento Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa Yaa Yaa Yaa Jai Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa Yaa Yaa Yaa Jai Balayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamo Gurrale Kaalemo Sayya Sayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamo Gurrale Kaalemo Sayya Sayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaa Yaa Yaa Yaa Jai Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa Yaa Yaa Yaa Jai Balayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama Kirre Kuthandento Thassadiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama Kirre Kuthandento Thassadiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa Yaa Yaa Yaa Jai Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa Yaa Yaa Yaa Jai Balayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamo Gurrale Kaalemo Sayya Sayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamo Gurrale Kaalemo Sayya Sayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balayya Balayya Balayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balayya Balayya Balayya"/>
</div>
</pre>
