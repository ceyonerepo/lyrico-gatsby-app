---
title: "yeano song lyrics"
album: "Kadikara Manithargal"
artist: "Sam CS"
lyricist: "Na Muthukumar"
director: "Vaigarai Balan"
path: "/albums/kadikara-manithargal-lyrics"
song: "Pattasu Vedingada"
image: ../../images/albumart/kadikara-manithargal.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OTgRmoIlzzQ"
type: "sad"
singers:
  - Sam CS
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeno oru veedu vaasal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno oru veedu vaasal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal thinam alaiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal thinam alaiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yeno naam kaanum mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yeno naam kaanum mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhchi valaiyin nizhal theriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhchi valaiyin nizhal theriyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno oru veedu vaasal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno oru veedu vaasal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal thinam alaiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal thinam alaiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yeno naam kaanum mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yeno naam kaanum mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhchi valaiyin nizhal theriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhchi valaiyin nizhal theriyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu enna soodhu nagarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu enna soodhu nagarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam thedum kora nagaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam thedum kora nagaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu enna soodhu nagarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu enna soodhu nagarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam thedum kora nagaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam thedum kora nagaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idu kaattil kooda idam illaa naragamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idu kaattil kooda idam illaa naragamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaikkum erumbukkum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikkum erumbukkum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu vaadagai thandhadhundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu vaadagai thandhadhundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu paarkkum idangal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu paarkkum idangal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakku endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakku endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani veligal pottadhundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani veligal pottadhundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aararivu manidhan yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aararivu manidhan yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigiraan thavikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigiraan thavikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiyum thanakkendrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum thanakkendrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagilae ninaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagilae ninaikkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyil irandhu pogaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil irandhu pogaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idu kaattil idam illaamal saambalaagiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idu kaattil idam illaamal saambalaagiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenooru veedu vaasal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenooru veedu vaasal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal thinam alaiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal thinam alaiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yen… naam kaanum mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yen… naam kaanum mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhchi valaiyin nizhal theriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhchi valaiyin nizhal theriyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno oru veedu vaasal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno oru veedu vaasal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal thinam alaiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal thinam alaiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yeno naam kaanum mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yeno naam kaanum mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhchi valaiyin nizhal theriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhchi valaiyin nizhal theriyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu enna soodhu nagarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu enna soodhu nagarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam thedum kora nagaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam thedum kora nagaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu enna soodhu nagarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu enna soodhu nagarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam thedum kora nagaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam thedum kora nagaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idu kaattil kooda idam illaa naragamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idu kaattil kooda idam illaa naragamaa"/>
</div>
</pre>
