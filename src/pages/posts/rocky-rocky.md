---
title: "rocky song lyrics"
album: "Rocky"
artist: "Darbuka Siva"
lyricist: "Kaber Vasuki"
director: "Arun Matheswaran"
path: "/albums/rocky-song-lyrics"
song: "Rocky"
image: ../../images/albumart/rocky.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8vh16CJODDk"
type: "mass"
singers:
  - James Thakara
  - Kaber Vasuki
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ullathil arakan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathil arakan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarathil iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarathil iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">arakanidam thapikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakanidam thapikka"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivanidam adaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivanidam adaikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">keten savada endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keten savada endru"/>
</div>
<div class="lyrico-lyrics-wrapper">sabithu vitan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabithu vitan"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarathil nirka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarathil nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">arakan vanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakan vanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaal thanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaal thanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhada endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhada endru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazthi sendraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazthi sendraan"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivan kodiyavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan kodiyavan"/>
</div>
<div class="lyrico-lyrics-wrapper">arakan meylone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakan meylone"/>
</div>
<div class="lyrico-lyrics-wrapper">yellai illa vallamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellai illa vallamai"/>
</div>
<div class="lyrico-lyrics-wrapper">konda iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konda iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">intha aathi puthiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha aathi puthiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">unaal sumarka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaal sumarka "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyatha baarathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyatha baarathai"/>
</div>
 <div class="lyrico-lyrics-wrapper">unaal padaika iyaluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaal padaika iyaluma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanma pathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanma pathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaduki vizhuntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaduki vizhuntha"/>
</div>
<div class="lyrico-lyrics-wrapper">veerar ellam sathuvakium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerar ellam sathuvakium"/>
</div>
<div class="lyrico-lyrics-wrapper">amaithi kanpathilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amaithi kanpathilai"/>
</div>
<div class="lyrico-lyrics-wrapper">sulchi sualal sulla vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulchi sualal sulla vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">aravali adithadi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aravali adithadi than"/>
</div>
<div class="lyrico-lyrics-wrapper">ayuthangal yenthi vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayuthangal yenthi vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">vidar athan vithapadi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidar athan vithapadi than"/>
</div>
<div class="lyrico-lyrics-wrapper">puvi sutri varum achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvi sutri varum achi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vanmurai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vanmurai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">palzithadum pagi nachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palzithadum pagi nachu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu neram pakaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu neram pakaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valzum varam vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valzum varam vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbilla boomi ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbilla boomi ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam vendru edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam vendru edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanjanai vanthadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjanai vanthadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">rowthiram kan mudumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rowthiram kan mudumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ayutham kai serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayutham kai serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ratchasan thindadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratchasan thindadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">porin neethiyil izhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porin neethiyil izhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum illai endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum illai endral"/>
</div>
<div class="lyrico-lyrics-wrapper">vagai sudium vetriku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagai sudium vetriku"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi illai kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi illai kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">theerka veeramundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerka veeramundu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalathila varipuli ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalathila varipuli ya"/>
</div>
<div class="lyrico-lyrics-wrapper">pavam theerka veekkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavam theerka veekkam "/>
</div>
<div class="lyrico-lyrics-wrapper">undu atharku ingu vazhi illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undu atharku ingu vazhi illaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puvi sutri varum achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvi sutri varum achi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vanmurai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vanmurai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">palzithadum pagi nachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palzithadum pagi nachu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu neram pakaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu neram pakaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valzum varam vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valzum varam vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbilla boomi ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbilla boomi ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam vendru edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam vendru edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanjanai vanthadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjanai vanthadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">rowthiram kan mudumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rowthiram kan mudumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ayutham kai serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayutham kai serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ratchasan thindadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratchasan thindadumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthiya thukikitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthiya thukikitu"/>
</div>
<div class="lyrico-lyrics-wrapper">thisaigal yettilum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisaigal yettilum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">parvaiyil anaithume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parvaiyil anaithume"/>
</div>
<div class="lyrico-lyrics-wrapper">anigalai ayudhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anigalai ayudhame"/>
</div>
<div class="lyrico-lyrics-wrapper">kalathiley kalainthapin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalathiley kalainthapin"/>
</div>
<div class="lyrico-lyrics-wrapper">karunai parpathu pavamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunai parpathu pavamada"/>
</div>
<div class="lyrico-lyrics-wrapper">valimaiyin attam ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valimaiyin attam ethu"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyai katinaal kayamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyai katinaal kayamada"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruthi kohika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruthi kohika"/>
</div>
<div class="lyrico-lyrics-wrapper">yzhumbu murika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yzhumbu murika"/>
</div>
<div class="lyrico-lyrics-wrapper">padaigal sithaika va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaigal sithaika va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puvi sutri varum achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvi sutri varum achi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vanmurai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vanmurai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">palzithadum pagi nachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palzithadum pagi nachu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu neram pakaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu neram pakaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valzum varam vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valzum varam vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbilla boomi ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbilla boomi ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam vendru edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam vendru edu"/>
</div>
</pre>
