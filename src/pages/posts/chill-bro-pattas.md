---
title: 'chill bro song lyrics'
album: 'Pattas'
artist: 'Vivek - Mervin'
lyricist: 'Mervin Solomon, Vivek Siva'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Chill bro'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Dhanush
youtubeLink: 'https://www.youtube.com/embed/pHxz8l1iWU4'
type: 'folk'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey petta boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey petta boy"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a setta boy
<input type="checkbox" class="lyrico-select-lyric-line" value="I am a setta boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Build up illaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Build up illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass kaattum boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Mass kaattum boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paasam boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Paasam boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam mosam boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam mosam boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu illaati
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasu illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda happy boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuda happy boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dummaangi nammakitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Dummaangi nammakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam seenagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Venaam seenagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poidunda vandhu friend aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poidunda vandhu friend aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnukulla onna fittaagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnukulla onna fittaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarumaara hit aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaarumaara hit aagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Chill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Rombha dhill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Rombha dhill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Go for kill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Go for kill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">We are winner winner winner
<input type="checkbox" class="lyrico-select-lyric-line" value="We are winner winner winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken dinner (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Chicken dinner"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chill bro…
<input type="checkbox" class="lyrico-select-lyric-line" value="Chill bro…"/>
</div>
<div class="lyrico-lyrics-wrapper">Winner winner
<input type="checkbox" class="lyrico-select-lyric-line" value="Winner winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken dinner
<input type="checkbox" class="lyrico-select-lyric-line" value="Chicken dinner"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bro chill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Bro chill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bro chill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Bro chill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bro….bro….chill bro….
<input type="checkbox" class="lyrico-select-lyric-line" value="Bro….bro….chill bro…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey petrol velai
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey petrol velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeripochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeripochu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Car-ey illa problem illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Car-ey illa problem illa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Chennaila thanni illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Chennaila thanni illa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan kulikavaemaaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kulikavaemaaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavalai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naalaiku ennaanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naalaiku ennaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikka maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Yosikka maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Secret sonnaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Secret sonnaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pesikka maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan pesikka maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi vandhakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambi vandhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaivuda maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaivuda maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri kettaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandri kettaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukka maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Kandukka maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dummaangi nammakitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Dummaangi nammakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam seenagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Venaam seenagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poidunda vandhu friend aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Poidunda vandhu friend aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnukulla onna fittaagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnukulla onna fittaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarumaara hit aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaarumaara hit aagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Chill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Rombha dhill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Rombha dhill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Go for kill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Go for kill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">We are winner winner winner
<input type="checkbox" class="lyrico-select-lyric-line" value="We are winner winner winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken dinner (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Chicken dinner"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey petta boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey petta boy"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a setta boy
<input type="checkbox" class="lyrico-select-lyric-line" value="I am a setta boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Build up illaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Build up illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass kaattum boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Mass kaattum boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paasam boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Paasam boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam mosam boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam mosam boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu illaati
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasu illaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda happy boy
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuda happy boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vaa (3  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey vaa"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Hey..hey..hey..hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey..hey..hey..hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Chill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Rombha dhill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Rombha dhill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Go for kill bro
<input type="checkbox" class="lyrico-select-lyric-line" value="Go for kill bro"/>
</div>
<div class="lyrico-lyrics-wrapper">We are winner winner winner
<input type="checkbox" class="lyrico-select-lyric-line" value="We are winner winner winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken dinner (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Chicken dinner"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Winner winner
<input type="checkbox" class="lyrico-select-lyric-line" value="Winner winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken dinner (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Chicken dinner"/></div>
</div>
</pre>