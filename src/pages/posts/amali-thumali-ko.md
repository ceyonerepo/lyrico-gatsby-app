---
title: "amali thumali song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Viveka"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Amali Thumali"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8IQXqcdhlLA"
type: "love"
singers:
  - Hariharan
  - Shweta Mohan
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amali Thumali Neliyum Valley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amali Thumali Neliyum Valley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavvi Kondathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavvi Kondathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Iduppin Oru Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Iduppin Oru Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Alli Sendrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Alli Sendrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Columbus Kanavilum Ninaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Columbus Kanavilum Ninaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Desam Azhaikutehy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Desam Azhaikutehy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyililum Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyililum Enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Kaatrum Veesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Kaatrum Veesuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mullil Pookkum Ena Arivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mullil Pookkum Ena Arivene"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenda Mullil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda Mullil"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Poovum Poorpathoru Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Poovum Poorpathoru Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarkka Solli Vizhi Kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkka Solli Vizhi Kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Nenjodu Nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Nenjodu Nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Pongi Varuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Pongi Varuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amali Thumali Neliyum Valley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amali Thumali Neliyum Valley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavvi Kondathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavvi Kondathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Iduppin Oru Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Iduppin Oru Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Alli Sendrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Alli Sendrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Columbus Kanavilum Ninaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Columbus Kanavilum Ninaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Desam Azhaikutehy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Desam Azhaikutehy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyililum Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyililum Enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Kaatrum Veesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Kaatrum Veesuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ena Solla Thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ena Solla Thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Po Ena Solla Marukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Po Ena Solla Marukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Kadhalin Paathaiyil Anaithum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Kadhalin Paathaiyil Anaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Perum Kolappam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Perum Kolappam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarugal Aruginil Irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarugal Aruginil Irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai Mazhai Athu Sovena Pozhinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Mazhai Athu Sovena Pozhinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nee Maattum Thooraththil Irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nee Maattum Thooraththil Irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Varandu Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Varandu Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Koova Koova Koova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Koova Koova Koova"/>
</div>
<div class="lyrico-lyrics-wrapper">Koova Kuyilethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koova Kuyilethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thavva Thavva Thavva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thavva Thavva Thavva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavva Manamethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavva Manamethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Muthal Mazhai Nanaithathai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Muthal Mazhai Nanaithathai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Thunai Anaithathai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Thunai Anaithathai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikkiren Kuthikkiren Mele Aaruyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikkiren Kuthikkiren Mele Aaruyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Unnai Kuduthathu Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Unnai Kuduthathu Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai Thoda Marukkuthu Paatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai Thoda Marukkuthu Paatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkini Urakkamum Thooram Devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkini Urakkamum Thooram Devathaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amali Thumali Neliyum Valley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amali Thumali Neliyum Valley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavvi Kondathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavvi Kondathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Iduppin Oru Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Iduppin Oru Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Alli Sendrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Alli Sendrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Columbus Kanavilum Ninaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Columbus Kanavilum Ninaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Desam Azhaikutehy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Desam Azhaikutehy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyililum Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyililum Enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Kaatrum Veesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Kaatrum Veesuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgalil Aadidum Kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalil Aadidum Kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan Osaigal Bhoomikku Puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan Osaigal Bhoomikku Puthusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Kaathugal Kettidum Poluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kaathugal Kettidum Poluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaviyarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaviyarasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merkilum Suriyan Uthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merkilum Suriyan Uthikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Minmini Sutrilum Kothikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Minmini Sutrilum Kothikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Aruginil Nee Ulla Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aruginil Nee Ulla Varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Miga Manamanakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga Manamanakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Poova Poova Poova Sirippaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Poova Poova Poova Sirippaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Avva Avva Ava Ava Theerthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Avva Avva Ava Ava Theerthaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Soodamale Anikalan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Soodamale Anikalan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodamale Udal Palan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodamale Udal Palan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamale Manathinil Thollai Kadhaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamale Manathinil Thollai Kadhaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda Thoda inithathey illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda inithathey illai"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveli Miga Perum Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveli Miga Perum Thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiyuma Magizhchiyin Ellai Oodalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyuma Magizhchiyin Ellai Oodalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amali Thumali Neliyum Valley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amali Thumali Neliyum Valley"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavvi Kondathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavvi Kondathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Iduppin Oru Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Iduppin Oru Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Alli Sendrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Alli Sendrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Columbus Kanavilum Ninaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Columbus Kanavilum Ninaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Desam Azhaikutehy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Desam Azhaikutehy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyililum Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyililum Enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Kaatrum Veesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Kaatrum Veesuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mullil Pookkum Ena Arivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mullil Pookkum Ena Arivene"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenda Mullil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda Mullil"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Poovum Poorpathoru Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Poovum Poorpathoru Maayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maari Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarkka Solli Vizhi Kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkka Solli Vizhi Kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Nenjodu Nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Nenjodu Nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Pongi Varuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Pongi Varuthey"/>
</div>
</pre>
