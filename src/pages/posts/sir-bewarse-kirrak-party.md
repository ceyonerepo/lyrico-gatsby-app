---
title: "sir bewarse song lyrics"
album: "Kirrak Party"
artist: "B Ajaneesh Loknath"
lyricist: "Rakendu Mouli"
director: "Sharan Koppisetty"
path: "/albums/kirrak-party-lyrics"
song: "Sir Bewarse"
image: ../../images/albumart/kirrak-party.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qlr3fB31xgs"
type: "happy"
singers:
  -	Shashank Sheshagiri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sir Bèwars Batch 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir Bèwars Batch "/>
</div>
<div class="lyrico-lyrics-wrapper">Maadi Kaanè Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi Kaanè Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Lifè A Option Ivvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Lifè A Option Ivvalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Lona Bèèr Taagè 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Lona Bèèr Taagè "/>
</div>
<div class="lyrico-lyrics-wrapper">Gang Kaanè Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gang Kaanè Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Vèrè Daari Tochalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Vèrè Daari Tochalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last Bènch Ni Nèwton’S 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last Bènch Ni Nèwton’S "/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Nuisancè Antu Tarimitè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Nuisancè Antu Tarimitè"/>
</div>
<div class="lyrico-lyrics-wrapper">Collègè Paruvu Poddi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collègè Paruvu Poddi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mèmanta Roddu èkkitè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèmanta Roddu èkkitè"/>
</div>
<div class="lyrico-lyrics-wrapper">Scam Schèmè Laina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scam Schèmè Laina "/>
</div>
<div class="lyrico-lyrics-wrapper">Dummu Duluputaamulè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummu Duluputaamulè"/>
</div>
<div class="lyrico-lyrics-wrapper">Friènd Krishna Gaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friènd Krishna Gaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Blèssings Untè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blèssings Untè"/>
</div>
<div class="lyrico-lyrics-wrapper">Hostèl Iyna Janata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hostèl Iyna Janata "/>
</div>
<div class="lyrico-lyrics-wrapper">Baaru Chèsè Talènt Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaru Chèsè Talènt Unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Month ènd Pockèt Monèy Undadè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Month ènd Pockèt Monèy Undadè"/>
</div>
<div class="lyrico-lyrics-wrapper">Plan-U Lè Bolèdunnaayilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan-U Lè Bolèdunnaayilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chèyyagaa Crèzy Things è
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chèyyagaa Crèzy Things è"/>
</div>
<div class="lyrico-lyrics-wrapper">Wath To Do Maaku Timè 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wath To Do Maaku Timè "/>
</div>
<div class="lyrico-lyrics-wrapper">Lèdulè Kaalamè Kaatu Vèsè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lèdulè Kaalamè Kaatu Vèsè"/>
</div>
<div class="lyrico-lyrics-wrapper">Inta Agè Lona Anta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inta Agè Lona Anta "/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamaa Studènt Lifè Nèramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamaa Studènt Lifè Nèramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir Bèwars Batch Maadi Kaanè Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir Bèwars Batch Maadi Kaanè Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Lifè A Option Ivvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Lifè A Option Ivvalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Car Lona Bèèr Taagè 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car Lona Bèèr Taagè "/>
</div>
<div class="lyrico-lyrics-wrapper">Gang Kaanè Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gang Kaanè Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Vèrè Daari Tochalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Vèrè Daari Tochalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last Bènch Ni Nèwton’S 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last Bènch Ni Nèwton’S "/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Nuisancè Antu Tarimitè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Nuisancè Antu Tarimitè"/>
</div>
<div class="lyrico-lyrics-wrapper">Collègè Paruvu Poddi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collègè Paruvu Poddi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mèmanta Roddu èkkitè
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèmanta Roddu èkkitè"/>
</div>
</pre>
