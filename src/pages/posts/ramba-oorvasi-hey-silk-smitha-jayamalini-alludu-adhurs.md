---
title: "ramba oorvasi song lyrics"
album: "Alludu Adhurs"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Santosh Srinivas"
path: "/albums/alludu-adhurs-lyrics"
song: "RAmba Oorvasi - Hey Silk Smitha"
image: ../../images/albumart/alludu-adhurs.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YGnji9aIyWE"
type: "happy"
singers:
  - Mangli
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Silk Smitha Jayamalini Jyothi Lakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Silk Smitha Jayamalini Jyothi Lakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamlo Chandamlo Related To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamlo Chandamlo Related To Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Katrina Kareena Sunny Leoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Katrina Kareena Sunny Leoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaru Naa Sisters Ye Please Believe Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaru Naa Sisters Ye Please Believe Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Koka Raika Nenesaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Koka Raika Nenesaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Saati Raaledu Ye Taaraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Saati Raaledu Ye Taaraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurraallina Eelleyyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurraallina Eelleyyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaaga Aagedi Nenochaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaaga Aagedi Nenochaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramba Ooravasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramba Ooravasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Rambha Urvasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Rambha Urvasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Silk Smitha Jayamalini Jyothi Lakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Silk Smitha Jayamalini Jyothi Lakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamlo Chandamlo Related To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamlo Chandamlo Related To Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrina Kareena Sunny Leoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrina Kareena Sunny Leoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaru Naa Sisters Ye Please Believe Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaru Naa Sisters Ye Please Believe Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First First Aadabomma Chekkinodiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First First Aadabomma Chekkinodiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Role Modele’dante Nee Picture Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Role Modele’dante Nee Picture Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Soanpapdi Laanti Soyaaganiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Soanpapdi Laanti Soyaaganiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Suitable Match Ante Nee Structure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suitable Match Ante Nee Structure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pori Naa Shirt Paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pori Naa Shirt Paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola Print Nee Valle Sentalle Maarinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola Print Nee Valle Sentalle Maarinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Smile Ishtayilu Gochi Thaakagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Smile Ishtayilu Gochi Thaakagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Olle Kade Oodipoyindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Olle Kade Oodipoyindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rambha Urvasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rambha Urvasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Rambha Oorvasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rambha Oorvasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Likkarante Nee Pedaallo Nakki Untade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Likkarante Nee Pedaallo Nakki Untade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkerante Nee Padaallo Chikki Untade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkerante Nee Padaallo Chikki Untade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ukku Laanti Onti Theeru Greeky Shilpame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ukku Laanti Onti Theeru Greeky Shilpame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Thene Poosukunna Katthi Vaatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Thene Poosukunna Katthi Vaatame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Shape Kothandaalake Best Shop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Shape Kothandaalake Best Shop"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Street’e Nee Care Of Address
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Street’e Nee Care Of Address"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pere Nuvvu Just Cheppu Chaalu Baasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pere Nuvvu Just Cheppu Chaalu Baasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Oolla Nenu Chaala Famous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Oolla Nenu Chaala Famous"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramba Ooravasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramba Ooravasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan Rambha Oorvasi Menaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan Rambha Oorvasi Menaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarnee Kalipesthe Nenika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarnee Kalipesthe Nenika"/>
</div>
</pre>
