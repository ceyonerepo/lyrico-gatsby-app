---
title: "naan romba romba song lyrics"
album: "Siruthai"
artist: "Vidyasagar"
lyricist: "Na. Muthukumar"
director: "Siva"
path: "/albums/siruthai-lyrics"
song: "Naan Romba Romba"
image: ../../images/albumart/siruthai.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cThvjkWIJzs"
type: "mass"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jintha Thak Thak Jintha Jintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jintha Thak Thak Jintha Jintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jintha Thak Thak Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jintha Thak Thak Thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jintha Thak Thak Jintha Jintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jintha Thak Thak Jintha Jintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jintha Thak Thak Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jintha Thak Thak Thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Romba Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Romba Romba Romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Powered by Ad.Plus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powered by Ad.Plus"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Nalla Pullaikku Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Pullaikku Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chella Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chella Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Romba Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Romba Romba Romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Nalla Pullaikku Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Pullaikku Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chella Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chella Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friend Ellam Poriki Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Ellam Poriki Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Echchikala Thiruttu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echchikala Thiruttu Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalukku Oorum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Oorum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Permananentu Perum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Permananentu Perum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattiketka Aalum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattiketka Aalum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daav Vadika Neram Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daav Vadika Neram Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhaminnu Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhaminnu Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sentimentu Yaethum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sentimentu Yaethum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Rocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pick Pocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pick Pocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Romba Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Romba Romba Romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Pullai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pullai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Nalla Pullaikku Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Pullaikku Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chella Pullai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chella Pullai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Am Classu Padikum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am Classu Padikum Podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattaiya Pottavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattaiya Pottavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
6 <div class="lyrico-lyrics-wrapper">Am Clasu Padikum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am Clasu Padikum Podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Blade Pottavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blade Pottavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Head Master Bike Ah Thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Head Master Bike Ah Thirudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edaikku Pottavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaikku Pottavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Central Jailil 100 Thadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Central Jailil 100 Thadava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tenttu Pottavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tenttu Pottavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemaandha Emaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaandha Emaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Envelai Pammaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Envelai Pammaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluradhellaam Suthamaana Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluradhellaam Suthamaana Poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vakkiradhellaam Pockettula Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkiradhellaam Pockettula Kai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraangallil Kooda Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraangallil Kooda Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththiduven Nei Neyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththiduven Nei Neyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Rocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pick Pocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pick Pocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Rompa Rompa Rompa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rompa Rompa Rompa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Nalla Pullaikku Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Pullaikku Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chella Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chella Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambi Enni Kambi Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambi Enni Kambi Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakku Padichchavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku Padichchavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Naalu Vayasil Nambiyaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naalu Vayasil Nambiyaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pola Suttavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Suttavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanji Kotta Vaalibana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Kotta Vaalibana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazha Nenachavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha Nenachavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pinjulaiyae Palluthavanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pinjulaiyae Palluthavanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paeru Eduthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paeru Eduthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Roottu Thani Rootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Roottu Thani Rootu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vettu Adhir Vaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vettu Adhir Vaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluradhellaam Suthamaana Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluradhellaam Suthamaana Poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vakkiradhellaam Pockettula Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkiradhellaam Pockettula Kai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraangallil Kooda Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraangallil Kooda Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththiduven Nei Neyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththiduven Nei Neyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Rocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pick Pocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pick Pocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Romba Romba Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Romba Romba Romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Nalla Pullaikku Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Pullaikku Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chella Pulla Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chella Pulla Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friend Ellam Poriki Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Ellam Poriki Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Echchikala Thiruttu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echchikala Thiruttu Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalukku Oorum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Oorum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Permananentu Perum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Permananentu Perum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattiketka Aalum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattiketka Aalum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daavadika Neram Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavadika Neram Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhaminnu Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhaminnu Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sentimentu Yaethum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sentimentu Yaethum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Rocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rocket Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Raja Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Raja Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pick Pocket Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pick Pocket Raja"/>
</div>
</pre>
