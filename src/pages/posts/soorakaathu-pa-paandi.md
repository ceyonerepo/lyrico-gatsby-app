---
title: "soorakaathu song lyrics"
album: "Pa Paandi"
artist: "Sean Roldan"
lyricist: "Dhanush"
director: "Dhanush"
path: "/albums/pa-paandi-lyrics"
song: "Soorakaathu"
image: ../../images/albumart/pa-paandi.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YBYzlXCLeS0"
type: "happy"
singers:
  -	Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora paathu poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora paathu poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasellaam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paathu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paathu yerudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora paathu poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora paathu poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasellaam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paathu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paathu yerudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala kuyilu koovudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kuyilu koovudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mayilu aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mayilu aaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaanavillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaanavillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Powder poosuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder poosuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kadhavu thorakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kadhavu thorakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vazhigal porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vazhigal porakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasaana singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaana singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeans pottu dance aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeans pottu dance aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada naadi thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naadi thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vegam porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vegam porakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada palaiya raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada palaiya raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi yeriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi yeriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora paathu poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora paathu poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasellaam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paathu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paathu yerudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor ulagam kaaladiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ulagam kaaladiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan adanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan adanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Per sollida por muzhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per sollida por muzhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan muzhanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan muzhanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveliyil ennudaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveliyil ennudaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Face theriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face theriyudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini baarangal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini baarangal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada en baaram thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada en baaram thaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazhathu kellaam aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhathu kellaam aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaanda sorgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaanda sorgam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu ippo en pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu ippo en pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pona pinnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pona pinnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En per nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En per nikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha star-um naanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha star-um naanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha sun-um naandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha sun-um naandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha sun-oda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha sun-oda "/>
</div>
<div class="lyrico-lyrics-wrapper">vinnum naandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnum naandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora kaathu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora kaathu kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru soora kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru soora kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora paathu poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora paathu poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasellaam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam paathu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam paathu yerudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala kuyilu koovudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kuyilu koovudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mayilu aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mayilu aaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaanavillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaanavillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Powder poosuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder poosuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kadhavu thorakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kadhavu thorakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vazhigal porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vazhigal porakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasaana singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaana singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeans pottu dance aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeans pottu dance aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada naadi thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naadi thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vegam porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vegam porakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada palaiya raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada palaiya raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi yeriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi yeriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanna nanna nanna naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna nanna nanna naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna nanna naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna nanna naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna nanna nanna naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna nanna nanna naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna nanna naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna nanna naa"/>
</div>
</pre>
