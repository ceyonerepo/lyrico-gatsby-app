---
title: "vettai singam 3 theme song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Hari"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Vettai singam 3 Theme Song"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p774zBfdf7M"
type: "Mass"
singers:
  - M. C. Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakka vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Durai singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durai singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Durai singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durai singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda thoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Durai singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durai singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Durai singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durai singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai ..Vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai ..Vettai"/>
</div>
</pre>
