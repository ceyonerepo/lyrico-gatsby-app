---
title: "dauda dauda song lyrics"
album: "Suraj pe mangal bhari"
artist: "Javed - Mohsin"
lyricist: "Danish Sabri"
director: "Abhishek Sharma"
path: "/albums/suraj-pe-mangal-bhari-lyrics"
song: "Dauda Dauda"
image: ../../images/albumart/suraj-pe-mangal-bhari.jpg
date: 2020-11-15
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jh407047cNQ"
type: "happy"
singers:
  - Divya Kumar
  - Javed-Mohsin
  - Mohsin Shaikh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Suno Suno Duniya Walo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno Suno Duniya Walo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Rishta Bada Nirala Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Rishta Bada Nirala Hai"/>
</div>
2 <div class="lyrico-lyrics-wrapper">Bando Ki Story Hai Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bando Ki Story Hai Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik Jija Ik Saala Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik Jija Ik Saala Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya Bataye Hum Kitna Pareshan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya Bataye Hum Kitna Pareshan Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokha Dadhi Se Meri Leli Haye Jaan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokha Dadhi Se Meri Leli Haye Jaan Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya Bataye Hum Kitna Pareshan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya Bataye Hum Kitna Pareshan Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokha Dadhi Se Meri Leli Haye Jaan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokha Dadhi Se Meri Leli Haye Jaan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi Tu Aage Main Piche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi Tu Aage Main Piche"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik Duje Ko Hum Khinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik Duje Ko Hum Khinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Tom And Jerry Ke Jaisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tom And Jerry Ke Jaisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna Yaarana Hai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apna Yaarana Hai Thoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pad Gaya Mere Piche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pad Gaya Mere Piche"/>
</div>
<div class="lyrico-lyrics-wrapper">Jasoos Nigoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jasoos Nigoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Doom Daba Ke Dekho Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doom Daba Ke Dekho Dauda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lag Gaya Mere Piche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lag Gaya Mere Piche"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Baawala Ghoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Baawala Ghoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Doom Daba Ke Dekho Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doom Daba Ke Dekho Dauda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apni Alag Hi Shan Hai Pyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni Alag Hi Shan Hai Pyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna Alag Style Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apna Alag Style Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaan Fatte To Fat Jaane Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaan Fatte To Fat Jaane Do"/>
</div>
<div class="lyrico-lyrics-wrapper">Par Hothon Pe Smile Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par Hothon Pe Smile Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jab Jab Apni Chalti Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab Jab Apni Chalti Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Kyun Itni Jalti Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Kyun Itni Jalti Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Apni Ki Tareef Karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Apni Ki Tareef Karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab Mujhme Talent Multi Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab Mujhme Talent Multi Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main Tujhpe Blame Nahi Karta Hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Tujhpe Blame Nahi Karta Hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyunki Tu Tere Dad Ki Galti Hai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyunki Tu Tere Dad Ki Galti Hai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Doom Daba Ke Dekho Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doom Daba Ke Dekho Dauda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lag Gaya Mere Piche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lag Gaya Mere Piche"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Baawala Ghoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Baawala Ghoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Doom Daba Ke Dekho Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doom Daba Ke Dekho Dauda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pad Gaya Mere Piche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pad Gaya Mere Piche"/>
</div>
<div class="lyrico-lyrics-wrapper">Jasoos Nigoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jasoos Nigoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dauda Dauda Dauda Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dauda Dauda Dauda Dauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Doom Daba Ke Dekho Dauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doom Daba Ke Dekho Dauda"/>
</div>
</pre>
