---
title: "vazhkaiyea oru olivilakku song lyrics"
album: "Kuppathu Raja"
artist: "G. V. Prakash Kumar"
lyricist: "Logan"
director: "Baba Bhaskar"
path: "/albums/kuppathu-raja-lyrics"
song: "Vazhkaiyea Oru Olivilakku"
image: ../../images/albumart/kuppathu-raja.jpg
date: 2019-04-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mDfnFyzDnT4"
type: "sad"
singers:
  - Gana Bala
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Oru Oli Vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Oru Oli Vilakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Pottadhu Yeman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Pottadhu Yeman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thalaiyila Idi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thalaiyila Idi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilundhiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilundhiduchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Gopuram Tharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Gopuram Tharaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanjiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjiduchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paasathukku Ulagam Sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paasathukku Ulagam Sirusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Patta Kadan Enakko Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Patta Kadan Enakko Perusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idha Enga Poi Soluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Enga Poi Soluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarukitta Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarukitta Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Ippo Paakanumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Ippo Paakanumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Poi Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poi Theduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Suththi Muththi Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Suththi Muththi Paakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Kekkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochi Illa Unkitta Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi Illa Unkitta Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ipo Vandhu Pesuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipo Vandhu Pesuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Ennai Peththaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Ennai Peththaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Ennai Thookuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Ennai Thookuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ullangaiyil Vechuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullangaiyil Vechuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Neeyum Thaanguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Neeyum Thaanguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marandhuteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marandhuteney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Appana Elandhuteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Appana Elandhuteney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paasathukku Ulagam Sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paasathukku Ulagam Sirusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Patta Kadan Enakko Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Patta Kadan Enakko Perusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idha Enga Poi Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Enga Poi Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarukitta Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarukitta Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Ippo Paakanumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Ippo Paakanumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Poi Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poi Theduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Suththi Muththi Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Suththi Muththi Paakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Kekkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochi Illa Unkitta Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi Illa Unkitta Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ipo Vandhu Pesuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipo Vandhu Pesuren"/>
</div>
</pre>
