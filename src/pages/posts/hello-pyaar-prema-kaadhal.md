---
title: "hello song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Hello"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PQVWYQzYvY0"
type: "happy"
singers:
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paa pap paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap paa paaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa paaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paa pap paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap paa paaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa paaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Are you looking for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are you looking for me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paa pap paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap paa paaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa paaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dupa dupa dupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dupa dupa dupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dupa dupa dupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dupa dupa dupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dupa dupa dupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dupa dupa dupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dupa dupa dupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dupa dupa dupaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paa pap paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap paa paaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa paaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heloo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heloo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Helooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paa pap paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap paa paaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap paa paaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap dooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap dooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa pap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paa pap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello hello oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello oooo"/>
</div>
</pre>
