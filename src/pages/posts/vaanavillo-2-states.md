---
title: "vaanavillo song lyrics"
album: "2 States"
artist: "Jakes Bejoy"
lyricist: "Jyothish T Kasi"
director: "Jacky S. Kumar"
path: "/albums/2-states-lyrics"
song: "Vaanavillo"
image: ../../images/albumart/2-states.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/Y-7RozC9h0o"
type: "happy"
singers:
  - M G Sreekumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manchaadi kunnum maambully thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchaadi kunnum maambully thodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payyaaram chollana neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payyaaram chollana neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalo kaattaadi paattukal paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalo kaattaadi paattukal paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Doore chankile thaalam kettee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doore chankile thaalam kettee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillo panthalitte mizhivaathil padiyolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillo panthalitte mizhivaathil padiyolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongalitte chembadayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongalitte chembadayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaithaalamitte naruchiri thookana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithaalamitte naruchiri thookana"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthumuthi kammalitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumuthi kammalitte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellinilaa thullikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellinilaa thullikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellalinju aattirambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellalinju aattirambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattilaadi veenavare kannu vachathaarithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattilaadi veenavare kannu vachathaarithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkilumee veeranivan kacha ketti vannirangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkilumee veeranivan kacha ketti vannirangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerinju ninnavaro nenchakathil aarum kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerinju ninnavaro nenchakathil aarum kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnale konderinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnale konderinje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillo panthalitte mizhivaathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillo panthalitte mizhivaathil "/>
</div>
<div class="lyrico-lyrics-wrapper">padiyolam Thongalitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padiyolam Thongalitte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruthe ninnaalum veyilin naalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthe ninnaalum veyilin naalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathivaay nenchoram kaniyaay pookkunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathivaay nenchoram kaniyaay pookkunne"/>
</div>
<div class="lyrico-lyrics-wrapper">Mizhikal mindaathe marayunnennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mizhikal mindaathe marayunnennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin koodaake kuliraay neeyunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin koodaake kuliraay neeyunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Katturumbukal nera nerakkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katturumbukal nera nerakkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattumukkile peedika thinnelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattumukkile peedika thinnelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram pokkinu keniyorukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram pokkinu keniyorukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathivaanivide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathivaanivide"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaalakkedin vidhamorukkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaalakkedin vidhamorukkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Veembadichoru poocha kuttykku paattilaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembadichoru poocha kuttykku paattilaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orthathilloru naanakkedithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orthathilloru naanakkedithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkilumenkilu onnaanennidanenchinakam neraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkilumenkilu onnaanennidanenchinakam neraye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenginilaneerin chellanniniyaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenginilaneerin chellanniniyaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Melle kaatil moolidunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melle kaatil moolidunne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillo panthalitte mizhivaathil padiyolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillo panthalitte mizhivaathil padiyolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongalitte chembadayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongalitte chembadayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaithaalamitte naruchiri thookana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithaalamitte naruchiri thookana"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthumuthi kammalitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumuthi kammalitte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellinilaa thullikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellinilaa thullikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellalinju aattirambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellalinju aattirambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattilaadi veenavare kannu vachathaarithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattilaadi veenavare kannu vachathaarithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkilumee veeranivan kacha ketti vannirangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkilumee veeranivan kacha ketti vannirangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerinju ninnavaro nenchakathil aarum kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerinju ninnavaro nenchakathil aarum kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnale konderinje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnale konderinje"/>
</div>
</pre>
