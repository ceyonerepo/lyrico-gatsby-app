---
title: "unnaivandhu song lyrics"
album: "Kuthiraivaal"
artist: "Pradeep Kumar"
lyricist: "Uma Devi"
director: "Manoj Leonel Jahson - Shyam Sunder"
path: "/albums/kuthiraivaal-lyrics"
song: "Unnaivandhu"
image: ../../images/albumart/kuthiraivaal.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FN-YoholbKY"
type: "melody"
singers:
  - Sean Roldan
  - Priyanka NK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unnai vanthu adaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vanthu adaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">siragena virinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragena virinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">paravaiyaai parakkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyaai parakkava"/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthu elunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthu elunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pidiyinil thodarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidiyinil thodarava"/>
</div>
<div class="lyrico-lyrics-wrapper">nijangalil asainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijangalil asainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilalinil pirakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilalinil pirakkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavile uraiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavile uraiyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanavile nirayavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanavile nirayavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nirangalil oliravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirangalil oliravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irulil milirnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulil milirnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannalil tholaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannalil tholaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchiyil thodargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchiyil thodargiren"/>
</div>
<div class="lyrico-lyrics-wrapper">pilaiyinil malargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilaiyinil malargiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">murangalil mulaithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murangalil mulaithavan"/>
</div>
<div class="lyrico-lyrics-wrapper">puluvai pol thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluvai pol thavikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">paranile erinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranile erinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">paluthai pol aligiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paluthai pol aligiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaipinai thuranthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaipinai thuranthida"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaigiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaigiren "/>
</div>
<div class="lyrico-lyrics-wrapper">nilaigalai kadanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaigalai kadanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">suyambu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suyambu naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai vanthu adaiyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vanthu adaiyava"/>
</div>
<div class="lyrico-lyrics-wrapper">siragena virinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragena virinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">paravaiyaai parakkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyaai parakkava"/>
</div>
</pre>
