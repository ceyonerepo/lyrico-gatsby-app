---
title: "hey amaicha song lyrics"
album: "Nota"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "Anand Shankar"
path: "/albums/nota-lyrics"
song: "Hey Amaicha"
image: ../../images/albumart/nota.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8ZijQiza1Nc"
type: "happy"
singers:
  - Arvind Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga da da daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga da da daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dak dak dagada dagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dak dak dagada dagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey amaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey amaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharma koyil kottai katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharma koyil kottai katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa amaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa amaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey amaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey amaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam aadi sattam poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam aadi sattam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa amaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa amaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Left-tu koppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left-tu koppai"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-tu cup-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-tu cup-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sign-eh vaida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sign-eh vaida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa naina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga da da daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga da da daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dak dak dagada dagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dak dak dagada dagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga da da daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga da da daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dak dak daga dak dak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dak dak daga dak dak"/>
</div>
<div class="lyrico-lyrics-wrapper">Daga dak dak dagada dagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daga dak dak dagada dagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey amaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey amaicha"/>
</div>
</pre>
