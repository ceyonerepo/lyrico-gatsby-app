---
title: "thaai nilame meendum varuvom song lyrics"
album: "Methagu"
artist: "Praveen Kumar"
lyricist: "Kittu"
director: "T. Kittu"
path: "/albums/methagu-lyrics"
song: "Thaai Nilame Meendum Varuvom"
image: ../../images/albumart/methagu.jpg
date: 2021-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q3NZ56_Wp94"
type: "motivational"
singers:
  - Dhivakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thaai nilame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai nilame"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">em thamile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em thamile "/>
</div>
<div class="lyrico-lyrics-wrapper">puliyai eluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyai eluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thadaigalai thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai thoduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai thoduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai nimirve nam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai nimirve nam"/>
</div>
<div class="lyrico-lyrics-wrapper">ilakai selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilakai selluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adimaiyenum mathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaiyenum mathil"/>
</div>
<div class="lyrico-lyrics-wrapper">suvargalai thagarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvargalai thagarka"/>
</div>
<div class="lyrico-lyrics-wrapper">aayutham pala seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayutham pala seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">viduthalai peruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduthalai peruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thadaigalai thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai thoduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai thoduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai nimirve nam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai nimirve nam"/>
</div>
<div class="lyrico-lyrics-wrapper">ilakai selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilakai selluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adimaiyenum mathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaiyenum mathil"/>
</div>
<div class="lyrico-lyrics-wrapper">suvargalai thagarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvargalai thagarka"/>
</div>
<div class="lyrico-lyrics-wrapper">aayutham pala seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayutham pala seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">viduthalai peruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduthalai peruvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unarve uyire uravadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarve uyire uravadum"/>
</div>
<div class="lyrico-lyrics-wrapper">magilve vaalve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magilve vaalve"/>
</div>
<div class="lyrico-lyrics-wrapper">puviyaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puviyaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanave sudare enai aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanave sudare enai aalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alage thamile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage thamile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathirai kanavai ennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathirai kanavai ennul"/>
</div>
<div class="lyrico-lyrics-wrapper">karvamai thondrinaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karvamai thondrinaye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai uramai tharuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai uramai tharuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">maraivom nilame engal thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraivom nilame engal thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyai vendi naangal nirpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyai vendi naangal nirpom"/>
</div>
<div class="lyrico-lyrics-wrapper">thuve engal suvasam enbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuve engal suvasam enbom"/>
</div>
<div class="lyrico-lyrics-wrapper">thuroga kanaigal soolthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuroga kanaigal soolthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthal thagartherivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthal thagartherivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unarve uyire uravadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarve uyire uravadum"/>
</div>
<div class="lyrico-lyrics-wrapper">magilve vaalve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magilve vaalve"/>
</div>
<div class="lyrico-lyrics-wrapper">puviyaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puviyaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanave sudare enai aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanave sudare enai aalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alage thamile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage thamile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaai nilame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai nilame"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">em thamile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em thamile "/>
</div>
<div class="lyrico-lyrics-wrapper">puliyai eluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyai eluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennul saratheeyai paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennul saratheeyai paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">em thamile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em thamile"/>
</div>
<div class="lyrico-lyrics-wrapper">kannul unai poruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannul unai poruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapen ennalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapen ennalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanmam uyirthelum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanmam uyirthelum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanamai irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanamai irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">thinnam oru pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinnam oru pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">odi oliya maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi oliya maten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanai noki nimirven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanai noki nimirven"/>
</div>
<div class="lyrico-lyrics-wrapper">en maaril veeram thanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maaril veeram thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">endrenum oru naal pali theerpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrenum oru naal pali theerpen"/>
</div>
<div class="lyrico-lyrics-wrapper">thurogam kalaiven viduthalai vetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurogam kalaiven viduthalai vetri"/>
</div>
<div class="lyrico-lyrics-wrapper">theepathai un kaaladiyil etruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theepathai un kaaladiyil etruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unarve uyire uravadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarve uyire uravadum"/>
</div>
<div class="lyrico-lyrics-wrapper">magilve vaalve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magilve vaalve"/>
</div>
<div class="lyrico-lyrics-wrapper">puviyaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puviyaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanave sudare enai aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanave sudare enai aalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alage thamile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage thamile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udale engal aayuthamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udale engal aayuthamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">inavatham engal theekiraiyagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inavatham engal theekiraiyagum"/>
</div>
<div class="lyrico-lyrics-wrapper">padaiyathai thirati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaiyathai thirati "/>
</div>
<div class="lyrico-lyrics-wrapper">porkalam pugunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalam pugunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">narigal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narigal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">thisai maari sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai maari sitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">irulum vidiyum kalangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulum vidiyum kalangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum maraiyum thayangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum maraiyum thayangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paravum nerupai irunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravum nerupai irunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">malarum eelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarum eelam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irulum vidiyum kalangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulum vidiyum kalangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum maraiyum thayangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum maraiyum thayangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paravum nerupai irunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravum nerupai irunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">malarum eelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarum eelam"/>
</div>
</pre>
