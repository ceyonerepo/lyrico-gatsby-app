---
title: "adiye song lyrics"
album: "RK Nagar"
artist: "Premgi Amaren"
lyricist: "Gangai Amaran"
director: "Saravana Rajan"
path: "/albums/rk-nagar-lyrics"
song: "Adiye"
image: ../../images/albumart/rk-nagar.jpg
date: 2019-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/irvuMkGUWy8"
type: "happy"
singers:
  - Ranjith
  - Bhavatharini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyae Vasamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Vasamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rootu Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rootu Vidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vusupethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vusupethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootividavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangadi Kattithaangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangadi Kattithaangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchi Kulirum Varaikkum Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchi Kulirum Varaikkum Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Doopudi Ippo Toppudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Doopudi Ippo Toppudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaatu Kaatavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaatu Kaatavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Appothaikkum Eppothaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Appothaikkum Eppothaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappethadiee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappethadiee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Vasamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Vasamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rootu Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rootu Vidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vusupethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vusupethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootividavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vetpaalar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vetpaalar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mattum Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mattum Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakkaalar Yeraalam Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakkaalar Yeraalam Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Otta Mattum Pottu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Mattum Pottu Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kelammaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kelammaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoguthikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoguthikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamum Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamum Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paguthikku Paniyaatru Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paguthikku Paniyaatru Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakku Tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku Tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalamaa Thaaralamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalamaa Thaaralamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kattayanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kattayanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathuthara Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathuthara Yaarum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa Itha Sollithara Thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa Itha Sollithara Thevaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorulagam Pora Vazhi Poyipaapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorulagam Pora Vazhi Poyipaapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallam Kuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam Kuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamumae Nerappunum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamumae Nerappunum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu Pattu Veetukkulla Magizhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu Pattu Veetukkulla Magizhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyasonna Velaiyellam Senjiranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyasonna Velaiyellam Senjiranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimae Pudhu Maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae Pudhu Maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapom Pala Raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapom Pala Raathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Record Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Record Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck Odu Yei Allikkudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Odu Yei Allikkudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Vasamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Vasamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rootu Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rootu Vidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vusupethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vusupethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootividavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangadi Kattithaangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangadi Kattithaangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchi Kulirum Varaikkum Aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchi Kulirum Varaikkum Aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Doopuda Ippo Toppuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Doopuda Ippo Toppuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaatu Kaatavaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaatu Kaatavaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Appothaikkum Eppothaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Appothaikkum Eppothaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappethadiee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappethadiee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Vasamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Vasamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rootu Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rootu Vidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vusupethavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vusupethavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootividavaa"/>
</div>
</pre>
