---
title: "kaattu vazhi song lyrics"
album: "Mambattiyan"
artist: "S Thaman"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/mambattiyan-lyrics"
song: "Kaattu Vazhi"
image: ../../images/albumart/mambattiyan.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lzR6he5txIM"
type: "love"
singers:
  - Thiagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthalai Vetti Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthalai Vetti Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhaiyila Kotti Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhaiyila Kotti Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodumaiya Kattivachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodumaiya Kattivachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna Micham Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Micham Vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullumela Thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullumela Thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaikkellaam Poo Virichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaikkellaam Poo Virichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththimala Paarai Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththimala Paarai Innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvadhavan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvadhavan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Aththimala Paarai Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Aththimala Paarai Innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvadhavan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvadhavan Pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaaththaa Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaaththaa Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Kooda Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Kooda Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalikkava Aasappattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalikkava Aasappattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbathukku Vaakkappattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbathukku Vaakkappattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgathukku Serndhu Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathukku Serndhu Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Vaangipputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Vaangipputtaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kadhaiyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadhaiyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Iduppodinja Naaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Iduppodinja Naaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Emmaa Enna Kadhaiyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmaa Enna Kadhaiyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Iduppodinja Naaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Iduppodinja Naaththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam Pola Varnamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam Pola Varnamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullikotta Sornamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullikotta Sornamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathupera Paatha Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathupera Paatha Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathini Thaan Mathapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathini Thaan Mathapadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambatiyaan Saagayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambatiyaan Saagayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Usura Vittavadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Usura Vittavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadhaviya Porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadhaviya Porantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kannagiya Iranthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kannagiya Iranthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadhaviya Porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadhaviya Porantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kannagiya Iranthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kannagiya Iranthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Peru Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Peru Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavu Konjam Koosuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavu Konjam Koosuthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju Mara Thoppu Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Mara Thoppu Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavi Vanthu Pesuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavi Vanthu Pesuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Veeram Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Veeram Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathi Sanam Kaakumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathi Sanam Kaakumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Seema Varapochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Seema Varapochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Seema Varapochaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Seema Varapochaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Vazhipora Ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Vazhipora Ponnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalappadaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalappadaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattupuli Vazhimarikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattupuli Vazhimarikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Nirkkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Nirkkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mambattiyaan Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mambattiyaan Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chonnaa Puli Udhavum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chonnaa Puli Udhavum Paaru"/>
</div>
</pre>
