---
title: "anukoledhey song lyrics"
album: "90 ML"
artist: "Anup Rubens"
lyricist: "Chandrabose"
director: "SekharReddy Yerra"
path: "/albums/90ml-lyrics"
song: "Anukoledhey"
image: ../../images/albumart/90ml.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LDoZc-YEGvY"
type: "sad"
singers:
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">anukoledhey anukoledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukoledhey anukoledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo sagame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo sagame "/>
</div>
<div class="lyrico-lyrics-wrapper">segalai kalchestundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="segalai kalchestundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anukoledhey anukoledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukoledhey anukoledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo sagame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo sagame "/>
</div>
<div class="lyrico-lyrics-wrapper">segalai kalchestundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="segalai kalchestundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anukoledhey anukoledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukoledhey anukoledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">kannulu erugani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulu erugani "/>
</div>
<div class="lyrico-lyrics-wrapper">sokam nadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokam nadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gundeku teliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundeku teliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">vedana nadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedana nadey"/>
</div>
<div class="lyrico-lyrics-wrapper">premanu minchina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premanu minchina "/>
</div>
<div class="lyrico-lyrics-wrapper">prasne eduraitey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prasne eduraitey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manase moyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase moyani"/>
</div>
<div class="lyrico-lyrics-wrapper">bharam nadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharam nadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">mataga marani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mataga marani"/>
</div>
<div class="lyrico-lyrics-wrapper">bhavam nadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhavam nadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gontuku edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gontuku edho"/>
</div>
<div class="lyrico-lyrics-wrapper">addam paduthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="addam paduthundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anukoledhey anukoledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukoledhey anukoledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo sagame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo sagame "/>
</div>
<div class="lyrico-lyrics-wrapper">segalai kalchestundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="segalai kalchestundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sirulu sampadalimmannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirulu sampadalimmannana"/>
</div>
<div class="lyrico-lyrics-wrapper">bahumathulevo temannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bahumathulevo temannana"/>
</div>
<div class="lyrico-lyrics-wrapper">korikalenno adigesana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korikalenno adigesana"/>
</div>
<div class="lyrico-lyrics-wrapper">saradalevo aasinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradalevo aasinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">badhyathalenno bodinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhyathalenno bodinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">baruvulu nepai nemopana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baruvulu nepai nemopana"/>
</div>
<div class="lyrico-lyrics-wrapper">iddaramokate ayyetanduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iddaramokate ayyetanduku"/>
</div>
<div class="lyrico-lyrics-wrapper">okkati ninnu vadilemannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkati ninnu vadilemannane"/>
</div>
</pre>
