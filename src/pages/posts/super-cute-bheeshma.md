---
title: "super cute song lyrics"
album: "Bheeshma"
artist: "Mahati Swara Sagar"
lyricist: "Sri Mani"
director: "Venky Kudumula"
path: "/albums/bheeshma-lyrics"
song: "Super Cute"
image: ../../images/albumart/bheeshma.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_xbsBq0STFM"
type: "love"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho aai aai yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aai aai yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho aai aai yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aai aai yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nee navvemo super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nee navvemo super cute ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee white chunni super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee white chunni super cute ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho look thoti penchave naalo heartbeat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho look thoti penchave naalo heartbeat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nadakento super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadakento super cute ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee out look ae super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee out look ae super cute ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallake nuvvu miss world kanna opthete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallake nuvvu miss world kanna opthete"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tsunami laaga pilla andhale illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tsunami laaga pilla andhale illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Allesthe yella naa dilu fatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allesthe yella naa dilu fatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Orachupu ala padesthe ala padunta nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orachupu ala padesthe ala padunta nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaali vente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaali vente"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu oppukunte pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu oppukunte pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenilla lavadesi na life sette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenilla lavadesi na life sette"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kassu mante ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kassu mante ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopamlo kuda anthandhente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopamlo kuda anthandhente"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa maate vinavente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maate vinavente"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho aai aai yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aai aai yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jante super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jante super cute ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho aai aai yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aai aai yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho aai aai yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aai aai yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kosam entha chesina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosam entha chesina "/>
</div>
<div class="lyrico-lyrics-wrapper">na life raasi icchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na life raasi icchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasthaina kanikarinchave oh pisinari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasthaina kanikarinchave oh pisinari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvventho vethiki chusina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvventho vethiki chusina"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokale jalledesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokale jalledesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Na lanti vaade dorakade oh sukumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na lanti vaade dorakade oh sukumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho unde felling ae super ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho unde felling ae super ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho konchem playing ae apeyyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho konchem playing ae apeyyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naughty beauty torture ae pettake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naughty beauty torture ae pettake"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila mana futureni set cheyyve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila mana futureni set cheyyve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa maate vinavente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maate vinavente"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jante super cute ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jante super cute ae"/>
</div>
</pre>
