---
title: "uyirinum uyarndha song lyrics"
album: "Ayngaran"
artist: "G.V. Prakash Kumar"
lyricist: "Madhan Karky"
director: "Ravi Arasu"
path: "/albums/ayngaran-lyrics"
song: "Uyirinum Uyarndha"
image: ../../images/albumart/ayngaran.jpg
date: 2022-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WwoYvMOUkA0"
type: "happy"
singers:
  - Hariharan
  - Sukhwinder Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirinum Uyarndhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinum Uyarndhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Enum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Enum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginil Ulaginil Oli Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginil Ulaginil Oli Kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhanai Manidhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhanai Manidhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhungidum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhungidum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Uyir Pera Vazhi Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Uyir Pera Vazhi Kidaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keezhae Miga Keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezhae Miga Keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Pudhaiyudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Pudhaiyudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melae Adhan Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melae Adhan Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae Chidhaiyudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae Chidhaiyudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirarukku Varandharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirarukku Varandharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivenna Arivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivenna Arivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulinai Niruthidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulinai Niruthidu Aingaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karungarunguzhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karungarunguzhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Surungudhu Manidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surungudhu Manidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Karam Koduthidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Karam Koduthidu Aingaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Idhu Yen?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Idhu Yen?"/>
</div>
<div class="lyrico-lyrics-wrapper">Emai Naamae Azhippadhu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emai Naamae Azhippadhu Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Adhu Yen?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Adhu Yen?"/>
</div>
<div class="lyrico-lyrics-wrapper">Piragunnai Pazhippadhu Yen?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piragunnai Pazhippadhu Yen?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poigalin Punniya Vaedathai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poigalin Punniya Vaedathai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Posukkida Vaa Vaa Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posukkida Vaa Vaa Aingaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanmaiyai Midhithidum Narigalai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanmaiyai Midhithidum Narigalai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasukkida Vaa Vaa Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasukkida Vaa Vaa Aingaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaahaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaahaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Oru Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Oru Thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Mulaikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Mulaikkudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Serndhae Anaikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Serndhae Anaikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudindhidum Mudindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudindhidum Mudindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudindhidum Endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudindhidum Endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urudhiyai Koduthidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urudhiyai Koduthidu Aingaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidindhidum Vidindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhidum Vidindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidindhidum Endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhidum Endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravinai Mudithidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinai Mudithidu Aingaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirinum Uyarndhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinum Uyarndhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Enum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Enum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginil Ulaginil Oli Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginil Ulaginil Oli Kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhanai Manidhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhanai Manidhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhungidum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhungidum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Uyir Pera Vazhi Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Uyir Pera Vazhi Kidaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keezhae Miga Keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezhae Miga Keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Pudhaiyudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Pudhaiyudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melae Adhan Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melae Adhan Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae Chidhaiyudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae Chidhaiyudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirarukku Varandharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirarukku Varandharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivenna Arivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivenna Arivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulinai Niruthidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulinai Niruthidu Aingaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karungarunguzhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karungarunguzhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Surungudhu Manidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surungudhu Manidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Karam Koduthidu Aingaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Karam Koduthidu Aingaranae"/>
</div>
</pre>
