---
title: "yaaradhu song lyrics"
album: "Kalari"
artist: "VV Prasanna"
lyricist: "Dinesh Krishnamurthy"
director: "Kiran C"
path: "/albums/kalari-lyrics"
song: "Yaaradhu"
image: ../../images/albumart/kalari.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dEvm1D6qXWU"
type: "love"
singers:
  - Hariharan
  - MM Monisha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaradhu yaaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradhu yaaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae vandhu ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae yenadhaanadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae yenadhaanadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattrangal vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattrangal vandhu ponadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu porvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu porvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugilae ini neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugilae ini neeyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayamae thadumaarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayamae thadumaarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu nilavai polae theyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu nilavai polae theyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal mazhaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal mazhaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu nanaindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu nanaindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai ooriyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai ooriyadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradhu yaaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradhu yaaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae vandhu ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae yenadhaanadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae yenadhaanadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattrangal vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattrangal vandhu ponadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae nee kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae nee kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai kadavula ena paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai kadavula ena paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalae ingu nee ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae ingu nee ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Enneramum unai parkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enneramum unai parkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavae kanavae podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavae kanavae podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugilae nee vendumae ini vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugilae nee vendumae ini vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai yeno unnil theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai yeno unnil theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yeno ennil theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai yeno ennil theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indha pudhu vidha boogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen indha pudhu vidha boogambam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradhu yaaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradhu yaaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae vandhu ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae yenadhaanadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae yenadhaanadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattrangal vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattrangal vandhu ponadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae oru poovai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae oru poovai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugiren un madiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugiren un madiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugaiyil unai thaangi pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugaiyil unai thaangi pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaigiren un tholilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaigiren un tholilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal nijamaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal nijamaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaigalum konjam pesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigalum konjam pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji pesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji pesudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaiyoram neerum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyoram neerum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikkindra kangal neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkindra kangal neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya thudipum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya thudipum neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradhu yaaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradhu yaaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilae vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae vandhu ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae yenadhaanadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae yenadhaanadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattrangal vandhu ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattrangal vandhu ponadhu"/>
</div>
</pre>
