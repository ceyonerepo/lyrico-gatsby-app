---
title: "niyayam dharmam song lyrics"
album: "Utharavu Maharaja"
artist: "Naren Balakumar"
lyricist: "Na Muthukumar"
director: "Asif Kuraishi"
path: "/albums/utharavu-maharaja-lyrics"
song: "Niyayam Dharmam"
image: ../../images/albumart/utharavu-maharaja.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PErMGAN5-gg"
type: "happy"
singers:
  - Deepak
  - MC Ganee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">i am a free bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am a free bird"/>
</div>
<div class="lyrico-lyrics-wrapper">i am a bad bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am a bad bird"/>
</div>
<div class="lyrico-lyrics-wrapper">i am a free bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am a free bird"/>
</div>
<div class="lyrico-lyrics-wrapper">i am a bad bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am a bad bird"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">niyayam dharmam neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayam dharmam neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">nermai sothukku uthavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermai sothukku uthavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai pidithu kattida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai pidithu kattida "/>
</div>
<div class="lyrico-lyrics-wrapper">engum kayirugal kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum kayirugal kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sutri engum kettavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutri engum kettavan"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamaai vaazhum thesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamaai vaazhum thesam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasai eduthu veesi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasai eduthu veesi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kallarai kooda pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kallarai kooda pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavanaaga irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavanaaga irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oore unna esum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore unna esum"/>
</div>
<div class="lyrico-lyrics-wrapper">kettavanaga irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettavanaga irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oore pugazhthu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore pugazhthu pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">neraga sellathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neraga sellathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">entha tharmamum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha tharmamum "/>
</div>
<div class="lyrico-lyrics-wrapper">illathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">neraga sellathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neraga sellathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">entha tharmamum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha tharmamum "/>
</div>
<div class="lyrico-lyrics-wrapper">illathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha ulagathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagathula"/>
</div>
<div class="lyrico-lyrics-wrapper">panam panam panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam panam panam"/>
</div>
<div class="lyrico-lyrics-wrapper">panam mattum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam mattum than"/>
</div>
<div class="lyrico-lyrics-wrapper">romba perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha panam illana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha panam illana"/>
</div>
<div class="lyrico-lyrics-wrapper">nee aduthavan kaaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee aduthavan kaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaladi kaaladi thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaladi kaaladi thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha panathai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha panathai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee use panlanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee use panlanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee rumba periya loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee rumba periya loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">loosu loosu loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loosu loosu loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">loosu loosu loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loosu loosu loosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panamenbathu oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panamenbathu oru"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakitha maayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakitha maayai"/>
</div>
<div class="lyrico-lyrics-wrapper">therinthavan purinthavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinthavan purinthavar"/>
</div>
<div class="lyrico-lyrics-wrapper">mutrilum methai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutrilum methai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulakathil vazthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulakathil vazthida"/>
</div>
<div class="lyrico-lyrics-wrapper">panam thaan perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam thaan perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">illati nee kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illati nee kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">kaaladi thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaladi thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">panam irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">payan padutha theriyathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payan padutha theriyathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaanda periya loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaanda periya loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">looseee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="looseee"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu kaasu packetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu kaasu packetil"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthaa thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthaa thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">manusanum mathipanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusanum mathipanda"/>
</div>
<div class="lyrico-lyrics-wrapper">panjathil vazhthu panjathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjathil vazhthu panjathil"/>
</div>
<div class="lyrico-lyrics-wrapper">setha un mavane uthapanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setha un mavane uthapanda"/>
</div>
<div class="lyrico-lyrics-wrapper">keththa vaazha kathukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keththa vaazha kathukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">kathukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum thappillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum thappillada"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttukku mele ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttukku mele ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">mele ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mele ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">vella adichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella adichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pakalaa maarumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakalaa maarumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru yutham seiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru yutham seiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum ponaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum ponaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vetti thalli munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti thalli munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kollaiyadiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kollaiyadiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">sertha kaasil ratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sertha kaasil ratha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam theriyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa enna venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa enna venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">enna venaa pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna venaa pannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">niyayam dharmam neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayam dharmam neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">nermai udane jeyikkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermai udane jeyikkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">adai mazhai vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adai mazhai vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">athanai neruppum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanai neruppum "/>
</div>
<div class="lyrico-lyrics-wrapper">anainthida marukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anainthida marukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sutri engum kettavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutri engum kettavan"/>
</div>
<div class="lyrico-lyrics-wrapper">padu sugamaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padu sugamaai "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhum thesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhum thesam"/>
</div>
<div class="lyrico-lyrics-wrapper">patri eriyum vayirukal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patri eriyum vayirukal"/>
</div>
<div class="lyrico-lyrics-wrapper">sabiththal aiyoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabiththal aiyoo "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhve mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhve mosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nallavanaaga irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavanaaga irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">sarithiram unnai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarithiram unnai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kettavanaaga irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kettavanaaga irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">sarvam naasamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarvam naasamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">ho ho ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho ho ho ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">pala paavam seythanivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala paavam seythanivan"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam maayai  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam maayai  "/>
</div>
<div class="lyrico-lyrics-wrapper">ivan pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba pollathavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">pala paavam seythanivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala paavam seythanivan"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam maayai  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam maayai  "/>
</div>
<div class="lyrico-lyrics-wrapper">ivan pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan pollathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba pollathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba pollathavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana nallavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana nallavana"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ni nalla irukkanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni nalla irukkanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana nallavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana nallavana"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavana irudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavana irudaa"/>
</div>
</pre>
