---
title: "thinna thinna song lyrics"
album: "Pen Vilai Verum 999 Rubai Mattume"
artist: "Judah Sandhy"
lyricist: "Chandra Shekar"
director: "Varadha Raj G"
path: "/albums/pen-vilai-verum-999-rubai-mattume-lyrics"
song: "Thinna Thinna"
image: ../../images/albumart/pen-vilai-verum-999-rubai-mattume.jpg
date: 2022-01-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5IOVClFXics"
type: "happy"
singers:
  - Ashwini Sai Prakash
  - Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thinna thinna taste aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna taste aana"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi pease naan pichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi pease naan pichu"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha innum konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha innum konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">koodum kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodum kaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">broking heart parota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="broking heart parota"/>
</div>
<div class="lyrico-lyrics-wrapper">open your mouth 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="open your mouth "/>
</div>
<div class="lyrico-lyrics-wrapper">oh ada ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ada ada"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna taste aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna taste aana"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi pease naan pichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi pease naan pichu"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha innum konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha innum konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">koodum kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodum kaasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">matuna aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matuna aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">aapala kunfu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aapala kunfu"/>
</div>
<div class="lyrico-lyrics-wrapper">slow slow sema heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="slow slow sema heart"/>
</div>
<div class="lyrico-lyrics-wrapper">thaara nayan thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaara nayan thaara"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pola iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pola iva "/>
</div>
<div class="lyrico-lyrics-wrapper">frash ah irupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="frash ah irupa"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna aalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna aalathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kuruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kuruma"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa kallu vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa kallu vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">parota varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parota varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">sayangalam nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sayangalam nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppa mooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppa mooti"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalu dosa oothama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalu dosa oothama"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thinna thinna taste aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna taste aana"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi pease naan pichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi pease naan pichu"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha innum konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha innum konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">koodum kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodum kaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaara nayan thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaara nayan thaara"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pola iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pola iva "/>
</div>
<div class="lyrico-lyrics-wrapper">frash ah irupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="frash ah irupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thinna thinna aalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna aalathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kuruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kuruma"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa kallu vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa kallu vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">parota varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parota varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">sayangalam nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sayangalam nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppa mooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppa mooti"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalu dosa oothama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalu dosa oothama"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeli kari veeti vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeli kari veeti vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaara kozhi itha naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaara kozhi itha naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">patta jivvu nu than aerum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta jivvu nu than aerum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">piduchu kathu nethu nethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piduchu kathu nethu nethu"/>
</div>
<div class="lyrico-lyrics-wrapper">pola vettu oorugava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola vettu oorugava"/>
</div>
<div class="lyrico-lyrics-wrapper">mutta kari thotukita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutta kari thotukita"/>
</div>
<div class="lyrico-lyrics-wrapper">muttum paru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttum paru aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum konjam thotukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum konjam thotukada"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkum ooru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkum ooru "/>
</div>
<div class="lyrico-lyrics-wrapper">piduchu pola tea iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piduchu pola tea iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">vadicha sothu panai uruti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadicha sothu panai uruti"/>
</div>
<div class="lyrico-lyrics-wrapper">michan kecham vaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="michan kecham vaika"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu sotha fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu sotha fulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enake ezhuthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake ezhuthi "/>
</div>
<div class="lyrico-lyrics-wrapper">vachu putanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu putanta"/>
</div>
<div class="lyrico-lyrics-wrapper">kothu kari vachu putanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothu kari vachu putanta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaada kari da kidacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaada kari da kidacha"/>
</div>
<div class="lyrico-lyrics-wrapper">vera maari da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera maari da"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi matham ethu theriyaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi matham ethu theriyaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vaithu matter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vaithu matter"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayi oru vaayi atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayi oru vaayi atha"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iva side la irupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iva side la irupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thinna thinna aalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna aalathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kuruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kuruma"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa kallu vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa kallu vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">parota varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parota varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">sayangalam nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sayangalam nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppa mooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppa mooti"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalu dosa oothama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalu dosa oothama"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam nandu kaalu rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nandu kaalu rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">thattu mela kidapathu kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattu mela kidapathu kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">rusiya pathu viluntha aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rusiya pathu viluntha aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku mela enna ketpan doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku mela enna ketpan doi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu iruken da kotha malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu iruken da kotha malli"/>
</div>
<div class="lyrico-lyrics-wrapper">serthu iruken da vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu iruken da vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">pidikum thutu thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikum thutu thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">paasam pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasam pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu iruken da kotha malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu iruken da kotha malli"/>
</div>
<div class="lyrico-lyrics-wrapper">serthu iruken da vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu iruken da vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">pidikum thutu thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikum thutu thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">paasam pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasam pidikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaara nayan thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaara nayan thaara"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pola iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pola iva "/>
</div>
<div class="lyrico-lyrics-wrapper">frash ah irupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="frash ah irupa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaara nayan thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaara nayan thaara"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pola iva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pola iva "/>
</div>
<div class="lyrico-lyrics-wrapper">frash ah irupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="frash ah irupa"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna aalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna aalathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhi kuruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhi kuruma"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa kallu vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa kallu vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">parota varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parota varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">sayangalam nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sayangalam nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppa mooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppa mooti"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalu dosa oothama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalu dosa oothama"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">akkava sutkava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkava sutkava "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">makka nee kikkagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka nee kikkagi"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha karuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha karuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ei maama varatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ei maama varatta"/>
</div>
</pre>
