---
title: 'hero title track song lyrics'
album: 'Hero'
artist: 'Yuvan Shankar Raja'
lyricist: 'Pa.Vijay'
director: 'P.S.Mithran'
path: '/albums/hero'
song: 'Hero Title Track'
image: ../../images/albumart/hero.jpg
date: 2019-12-20
lang: tamil
singers: 
- Yuvan Shankar Raja
youtubeLink: "https://www.youtube.com/embed/qsfEvzBaGR0"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Saga manidhanai madhikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Saga manidhanai madhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha manidhanum hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha manidhanum hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Suya sindhanaigal irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Suya sindhanaigal irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoruthanumae hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Ovvoruthanumae hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kan munn nadakkum thatti thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan munn nadakkum thatti thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkiravan medai yerinaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkiravan medai yerinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai mattumae pesubavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai mattumae pesubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh vandha pinnum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pugazh vandha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai meedhudhaan nirkkiravan
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharai meedhudhaan nirkkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin munnae thottru nirpavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbin munnae thottru nirpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Medai yeruda vaanam thoduda
<input type="checkbox" class="lyrico-select-lyric-line" value="Medai yeruda vaanam thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduda
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezha thalli thaan kottam sirikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezha thalli thaan kottam sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum ezhuda suthi paaruda
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum ezhuda suthi paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam munna nee jeyichu kaatudaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottam munna nee jeyichu kaatudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Netrin valigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Netrin valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku kaattum vazhigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku kaattum vazhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Selvom vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Selvom vaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom vaa… hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Seivom vaa… hey…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Engal kanavum engal aasaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Engal kanavum engal aasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paritchai paper-il karaigiradhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paritchai paper-il karaigiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinna siragugal <div class="lyrico-lyrics-wrapper">3llaam</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Chinna chinna siragugal <div class="lyrico-lyrics-wrapper">3llaam"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Puthagam sumandhae udaigiradhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthagam sumandhae udaigiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagupugal ellaam siraigalum alla
<input type="checkbox" class="lyrico-select-lyric-line" value="Vagupugal ellaam siraigalum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugira kanavugal thavarugal alla
<input type="checkbox" class="lyrico-select-lyric-line" value="Varugira kanavugal thavarugal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar….badhil tharuvaar ?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar….badhil tharuvaar ?…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mark-ku kaana ottam podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mark-ku kaana ottam podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Markkai thaandi odu
<input type="checkbox" class="lyrico-select-lyric-line" value="Markkai thaandi odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvikku bathilai ezhudhum anba
<input type="checkbox" class="lyrico-select-lyric-line" value="Kelvikku bathilai ezhudhum anba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelviya neeyum thedu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kelviya neeyum thedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chemistry-la fail aanavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Chemistry-la fail aanavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttal ondrum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttal ondrum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maths-il neeyum medhai aalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Maths-il neeyum medhai aalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Melae vaada vella
<input type="checkbox" class="lyrico-select-lyric-line" value="Melae vaada vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thiramaiyai pattai theettum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiramaiyai pattai theettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam dhaanae kalvi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thittam dhaanae kalvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthagatha mug up pannum
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthagatha mug up pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvi sariyaa thambi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalvi sariyaa thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivukku filter podum
<input type="checkbox" class="lyrico-select-lyric-line" value="Arivukku filter podum"/>
</div>
<div class="lyrico-lyrics-wrapper">System dhaanae neet-u
<input type="checkbox" class="lyrico-select-lyric-line" value="System dhaanae neet-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhaiyum meeri talent-il
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhaiyum meeri talent-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaarunu dhaan kaattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee yaarunu dhaan kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thervil thorpadhu tholvi illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thervil thorpadhu tholvi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thiramaiyai thervugal ketpadhillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani thiramaiyai thervugal ketpadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I felt the wave
<input type="checkbox" class="lyrico-select-lyric-line" value="I felt the wave"/>
</div>
<div class="lyrico-lyrics-wrapper">On my hair
<input type="checkbox" class="lyrico-select-lyric-line" value="On my hair"/>
</div>
<div class="lyrico-lyrics-wrapper">When i saw
<input type="checkbox" class="lyrico-select-lyric-line" value="When i saw"/>
</div>
<div class="lyrico-lyrics-wrapper">You across the hall
<input type="checkbox" class="lyrico-select-lyric-line" value="You across the hall"/>
</div>
<div class="lyrico-lyrics-wrapper">Wearing a coat
<input type="checkbox" class="lyrico-select-lyric-line" value="Wearing a coat"/>
</div>
<div class="lyrico-lyrics-wrapper">That spells H..E..R…O..
<input type="checkbox" class="lyrico-select-lyric-line" value="That spells H..E..R…O.."/>
</div>
<div class="lyrico-lyrics-wrapper">Fireball
<input type="checkbox" class="lyrico-select-lyric-line" value="Fireball"/>
</div>
<div class="lyrico-lyrics-wrapper">Every Time
<input type="checkbox" class="lyrico-select-lyric-line" value="Every Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Enter in my space
<input type="checkbox" class="lyrico-select-lyric-line" value="Enter in my space"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m always blessed
<input type="checkbox" class="lyrico-select-lyric-line" value="I’m always blessed"/>
</div>
<div class="lyrico-lyrics-wrapper">With your ever everlasting grace
<input type="checkbox" class="lyrico-select-lyric-line" value="With your ever everlasting grace"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Boy you got me good
<input type="checkbox" class="lyrico-select-lyric-line" value="Boy you got me good"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re a king
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re a king"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re a saint
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re a saint"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re a knight hood
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re a knight hood"/>
</div>
<div class="lyrico-lyrics-wrapper">Can’t take you down
<input type="checkbox" class="lyrico-select-lyric-line" value="Can’t take you down"/>
</div>
<div class="lyrico-lyrics-wrapper">With a smother
<input type="checkbox" class="lyrico-select-lyric-line" value="With a smother"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna hold you tight daylight
<input type="checkbox" class="lyrico-select-lyric-line" value="Gonna hold you tight daylight"/>
</div>
<div class="lyrico-lyrics-wrapper">Firefight
<input type="checkbox" class="lyrico-select-lyric-line" value="Firefight"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re my kryptonite
<input type="checkbox" class="lyrico-select-lyric-line" value="You’re my kryptonite"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll never trade you for another
<input type="checkbox" class="lyrico-select-lyric-line" value="I’ll never trade you for another"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurai solvadhai thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurai solvadhai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai sari seibavanae hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhai sari seibavanae hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattravan vetriyai paarthu nija
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattravan vetriyai paarthu nija"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhchi kolbavan hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Magizhchi kolbavan hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kan mun nadakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan mun nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappai thatti thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappai thatti thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkiravan medai yerinaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkiravan medai yerinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai mattumae pesubavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai mattumae pesubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh vandha pinnum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pugazh vandha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai meedhudhaan nirkkiravan
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharai meedhudhaan nirkkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin munnae thottru nirpavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbin munnae thottru nirpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero…..hero
<input type="checkbox" class="lyrico-select-lyric-line" value="Hero…..hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Medai yeruda vaanam thoduda
<input type="checkbox" class="lyrico-select-lyric-line" value="Medai yeruda vaanam thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduda
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezha thalli thaan kottam sirikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezha thalli thaan kottam sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum ezhuda suthi paaruda
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum ezhuda suthi paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam munna nee jeyichu kaatudaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottam munna nee jeyichu kaatudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattudaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaattudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I know that you never gonna fight alone
<input type="checkbox" class="lyrico-select-lyric-line" value="I know that you never gonna fight alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Standing so tall like a firestone
<input type="checkbox" class="lyrico-select-lyric-line" value="Standing so tall like a firestone"/>
</div>
<div class="lyrico-lyrics-wrapper">I know that you never gonna fight alone
<input type="checkbox" class="lyrico-select-lyric-line" value="I know that you never gonna fight alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Standing so tall like a firestone….
<input type="checkbox" class="lyrico-select-lyric-line" value="Standing so tall like a firestone…."/>
</div>
</pre>