---
title: "paga paga paga song lyrics"
album: "Nadodigal 2"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-2-lyrics"
song: "Paga Paga Paga"
image: ../../images/albumart/nadodigal-2.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UzrjVvX1lgA"
type: "Mass"
singers:
  - Mark Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thoonilum Irupaan Thurumbilum Irupaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonilum Irupaan Thurumbilum Irupaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonilum Irupaan Thurumbilum Irupaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonilum Irupaan Thurumbilum Irupaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Pattadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Pattadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrida Pattrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrida Pattrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Kettadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Kettadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrida Katrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrida Katrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattamum Thittamum Raththam Urinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattamum Thittamum Raththam Urinjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutrida Mutrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutrida Mutrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottathu Vittadhum Vittadhu Suttadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathu Vittadhum Vittadhu Suttadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotrida Thotrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotrida Thotrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthavane Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavane Vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sei Puratchi Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Puratchi Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Puratchi Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Puratchi Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu Sei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Neruppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Neruppena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagalamum Namakkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagalamum Namakkena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nada Nada Kizhakkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada Nada Kizhakkena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadapadhu Sivappena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadapadhu Sivappena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthavane Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavane Vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom Poraaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraaduvom Poraaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraaduvom Poraaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Pattadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Pattadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrida Pattrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrida Pattrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Kettadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Kettadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrida Katrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrida Katrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattamum Thittamum Raththam Urinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattamum Thittamum Raththam Urinjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutrida Mutrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutrida Mutrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottathu Vittadhum Vittadhu Suttadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathu Vittadhum Vittadhu Suttadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotrida Thotrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotrida Thotrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthavane Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavane Vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Aadum Aattathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Aadum Aattathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vazhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vazhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh Thaai Vaazha Por Seidhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh Thaai Vaazha Por Seidhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theengaaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengaaguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathaanin Kootathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Kootathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Yokkiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yokkiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saayaamal Porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saayaamal Porada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Seekkiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Seekkiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sei Puratchi Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Puratchi Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Sei Puratchi Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sei Puratchi Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu Sei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaveri Neer Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaveri Neer Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Varandomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varandomada"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Yeraamal Kaneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Yeraamal Kaneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhandhomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduthavan Adikaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthavan Adikaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahimsaiyai Uraipadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahimsaiyai Uraipadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadippinil Namai Silar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadippinil Namai Silar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkida Ninaipatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkida Ninaipatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Pattadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Pattadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrida Pattrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrida Pattrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Ethanai Kettadhu Ethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Ethanai Kettadhu Ethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrida Katrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrida Katrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattamum Thittamum Raththam Urinjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattamum Thittamum Raththam Urinjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutrida Mutrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutrida Mutrida Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottathu Vittadhum Vittadhu Suttadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathu Vittadhum Vittadhu Suttadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotrida Thotrida Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotrida Thotrida Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthavane Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavane Vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthavane Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthavane Vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga Paga Paga Paga Paga Paga Paga Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga Paga Paga Paga Paga Paga Paga Paga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paga Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga Paga"/>
</div>
</pre>
