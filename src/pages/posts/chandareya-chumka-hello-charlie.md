---
title: "chandareya chumka song lyrics"
album: "Hello Charlie"
artist: "Rishi Rich - Kiranee - Don D Marley"
lyricist: "Kumaar"
director: "Pankaj Saraswat"
path: "/albums/hello-charlie-lyrics"
song: "Chandareya chumka"
image: ../../images/albumart/hello-charlie.jpg
date: 2021-04-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/1iUKWBBSdZA"
type: "Enjoy"
singers:
  - Kiranee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Awaazan marda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaazan marda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Awaazan marda, chandareya chumka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaazan marda, chandareya chumka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandareya chumka main tera intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandareya chumka main tera intezaar kardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal sun le tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal sun le tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mona kinna pyaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mona kinna pyaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh jawani ki phere nahiyon aani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh jawani ki phere nahiyon aani"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kyun ni 2 2 drink marda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kyun ni 2 2 drink marda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari jawaan main tere uthe saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari jawaan main tere uthe saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kyun nahi uthe dil haarda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kyun nahi uthe dil haarda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ajj mili ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj mili ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ajj mili ve tu kal bhul jaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ajj mili ve tu kal bhul jaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kal bhul jaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kal bhul jaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa waada koyi yaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa waada koyi yaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Awaazan marda, chandareya chumka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaazan marda, chandareya chumka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandareya chumka main tera intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandareya chumka main tera intezaar kardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal sun le tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal sun le tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mona kinna pyaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mona kinna pyaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Awaazan marda, chandareya chumka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaazan marda, chandareya chumka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandareya chumka main tera intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandareya chumka main tera intezaar kardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal sun le tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal sun le tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu nehde nehre aake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu nehde nehre aake"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mona kinna pyaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mona kinna pyaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaandi jaandi aundi jaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaandi jaandi aundi jaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeke jadd muskandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeke jadd muskandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaandi jaandi aundi jaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaandi jaandi aundi jaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeke jadd muskandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeke jadd muskandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaandi jaandi aundi jaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaandi jaandi aundi jaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeke jadd muskandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeke jadd muskandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Munde kehnde meri billori blank ho gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde kehnde meri billori blank ho gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main tera intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tera intezaar kardi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mona kinna pyaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mona kinna pyaar kardi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Awaazan marda, chandareya chumka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaazan marda, chandareya chumka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandareya chumka main tera intezaar kardi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandareya chumka main tera intezaar kardi"/>
</div>
</pre>
