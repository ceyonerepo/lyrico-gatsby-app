---
title: "allari chese kala song lyrics"
album: "1945"
artist: "Yuvan Shankar Raja"
lyricist: "Ananth Sriram"
director: "Sathyasiva"
path: "/albums/1945-lyrics"
song: "Allari Chese Kala"
image: ../../images/albumart/1945.jpg
date: 2022-01-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yjYPJ0oVQ_E"
type: "happy"
singers:
  - Haricharan
  - Priya Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allari chese kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari chese kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Allandha kokala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allandha kokala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapade manasutho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapade manasutho "/>
</div>
<div class="lyrico-lyrics-wrapper">aata neekela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aata neekela"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukupoye vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukupoye vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallunadhokya vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallunadhokya vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudipade gadiyakai aagalevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudipade gadiyakai aagalevela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velluvalanti praname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velluvalanti praname "/>
</div>
<div class="lyrico-lyrics-wrapper">vechi choodala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi choodala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yedha nene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yedha nene "/>
</div>
<div class="lyrico-lyrics-wrapper">cherutha ninnu edhola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherutha ninnu edhola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayithe adhedho ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayithe adhedho ee "/>
</div>
<div class="lyrico-lyrics-wrapper">kshanamulone cherithe pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanamulone cherithe pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Allari chese kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari chese kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Allandha kokala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allandha kokala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapade manasutho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapade manasutho "/>
</div>
<div class="lyrico-lyrics-wrapper">aata neekela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aata neekela"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukupoye vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukupoye vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallunadhokya vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallunadhokya vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudipade gadiyakai aagalevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudipade gadiyakai aagalevela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nemmadhiga nemmadhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nemmadhiga nemmadhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Musurula kamminadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musurula kamminadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maikamedho marupu nerchela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maikamedho marupu nerchela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa nammadhuga nammavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nammadhuga nammavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">mahimedho rammandhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahimedho rammandhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu neetho kanulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu neetho kanulu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaluputhu aadhamarichena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluputhu aadhamarichena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavipaina pedhavila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavipaina pedhavila "/>
</div>
<div class="lyrico-lyrics-wrapper">haayiga pavalinchada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haayiga pavalinchada"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalalona kathalane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalalona kathalane "/>
</div>
<div class="lyrico-lyrics-wrapper">mouname telapalira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mouname telapalira"/>
</div>
<div class="lyrico-lyrics-wrapper">Allari chese kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari chese kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Allandha kokala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allandha kokala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapade manasutho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapade manasutho "/>
</div>
<div class="lyrico-lyrics-wrapper">aata neekela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aata neekela"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukupoye vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukupoye vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallunadhokya vala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallunadhokya vala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudipade gadiyakai aagalevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudipade gadiyakai aagalevela"/>
</div>
</pre>
