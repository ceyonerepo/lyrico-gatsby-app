---
title: "unga appakitta pesala song lyrics"
album: "Nagesh Thiraiyarangam"
artist: "Srikanth Deva"
lyricist: "Jegan Set"
director: "Mohamad Issack"
path: "/albums/nagesh-thiraiyarangam-lyrics"
song: "Unga Appakitta Pesala"
image: ../../images/albumart/nagesh-thiraiyarangam.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A5S1D0-GckY"
type: "happy"
singers:
  - Jithesh
  - Remya Nambeesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unga appa kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga appa kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">unga amma kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga amma kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthuputen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthuputen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga anna kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga anna kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">unga akka kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga akka kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthu puten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un friend kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un friend kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi naan suthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi naan suthala"/>
</div>
<div class="lyrico-lyrics-wrapper">un friend kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un friend kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi naan suthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi naan suthala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalaum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalaum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthu puten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">sulakki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulakki "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dance podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dance podu"/>
</div>
<div class="lyrico-lyrics-wrapper">dance podu bulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance podu bulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee summa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee summa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">summa summa sulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa summa sulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee chance kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chance kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">chance kodu lolaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chance kodu lolaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan vaadi vasal mapila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaadi vasal mapila"/>
</div>
<div class="lyrico-lyrics-wrapper">nan aadi neyum pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan aadi neyum pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaadi vasal mapila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaadi vasal mapila"/>
</div>
<div class="lyrico-lyrics-wrapper">nan aadi neyum pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan aadi neyum pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pancha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pancha than"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum kavunthuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum kavunthuduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan otha naadi pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan otha naadi pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">othikaiyum pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othikaiyum pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">vithaiyila unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithaiyila unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthuduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mallu katta mama varan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallu katta mama varan"/>
</div>
<div class="lyrico-lyrics-wrapper">pullu katta thinga poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullu katta thinga poran"/>
</div>
<div class="lyrico-lyrics-wrapper">jalli kattu kaala varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jalli kattu kaala varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">othungi nillungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi nillungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam pathingu nillungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam pathingu nillungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa yen paama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa yen paama"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala aagurendi koma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala aagurendi koma"/>
</div>
<div class="lyrico-lyrics-wrapper">vada vada yen maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vada vada yen maama"/>
</div>
<div class="lyrico-lyrics-wrapper">ne ennoda vascodagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne ennoda vascodagama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oorasutha polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorasutha polama"/>
</div>
<div class="lyrico-lyrics-wrapper">come come come daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come come come daa"/>
</div>
<div class="lyrico-lyrics-wrapper">umma onnu thayemaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umma onnu thayemaa"/>
</div>
<div class="lyrico-lyrics-wrapper">go go go daaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="go go go daaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oorasutha polama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorasutha polama"/>
</div>
<div class="lyrico-lyrics-wrapper">come come come daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come come come daa"/>
</div>
<div class="lyrico-lyrics-wrapper">umma onnu thayemaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="umma onnu thayemaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chee chee chee poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chee chee chee poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bubble gummu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bubble gummu "/>
</div>
<div class="lyrico-lyrics-wrapper">bubble gummu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bubble gummu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadatha double game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadatha double game"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">sulakki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulakki "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dance podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dance podu"/>
</div>
<div class="lyrico-lyrics-wrapper">dance podu bulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance podu bulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee summa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee summa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">summa summa sulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa summa sulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee chance kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chance kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">chance kodu lolaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chance kodu lolaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga appa kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga appa kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">unga amma kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga amma kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthuputen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthuputen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga anna kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga anna kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">unga akka kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga akka kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthu puten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un friend kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un friend kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi naan suthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi naan suthala"/>
</div>
<div class="lyrico-lyrics-wrapper">un friend kitta pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un friend kitta pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadi naan suthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadi naan suthala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalaum unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalaum unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuthu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuthu puten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">dammal dummal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammal dummal"/>
</div>
<div class="lyrico-lyrics-wrapper">sulakki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulakki "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dance podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dance podu"/>
</div>
<div class="lyrico-lyrics-wrapper">dance podu bulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance podu bulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee summa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee summa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">summa summa sulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa summa sulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee chance kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chance kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">chance kodu lolaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chance kodu lolaki"/>
</div>
</pre>
