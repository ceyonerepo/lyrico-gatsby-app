---
title: "pappara pappaa song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Pappara Pappaa"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XOg1QpXBLBw"
type: "happy"
singers:
  - Praniti
  - Riyaz
  - Sri Vishnu
  - Pranav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puliyaattam pappara pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyaattam pappara pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilaattam jinukku jikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaattam jinukku jikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaattam dandakkudanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaattam dandakkudanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagaattam thaiyakuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagaattam thaiyakuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puliyaattam pappara pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyaattam pappara pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilaattam jinukku jikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaattam jinukku jikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaattam dandakkudanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaattam dandakkudanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagaattam thaiyakuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagaattam thaiyakuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu poikaal kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu poikaal kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku dhillu irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku dhillu irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhundhu vaa nee edhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhundhu vaa nee edhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru pudicha kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru pudicha kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Inainju aadu kudhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainju aadu kudhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha boomi athira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha boomi athira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varipuli onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varipuli onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja karuppu kodu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja karuppu kodu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varanju thaan puliyaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varanju thaan puliyaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumi aada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumi aada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala pala thoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pala thoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhiya muzhiya parikka thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhiya muzhiya parikka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanju thaan mayilaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanju thaan mayilaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Virichu aada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virichu aada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadu nammoda thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu nammoda thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamil Nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil Nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukintha mannil illa eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukintha mannil illa eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panpaada paadikitta aadu aadu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panpaada paadikitta aadu aadu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhanin nadanathai kaatu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhanin nadanathai kaatu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida thakida thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida thakida thakida thakida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
</pre>
