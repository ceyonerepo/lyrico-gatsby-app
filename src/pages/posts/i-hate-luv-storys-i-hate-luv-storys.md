---
title: "i hate luv storys song lyrics"
album: "I Hate Luv Storys"
artist: "Vishal Shekhar"
lyricist: "Kumaar"
director: "Punit Malhotra"
path: "/albums/i-hate-luv-storys-lyrics"
song: "I Hate Luv Storys"
image: ../../images/albumart/i-hate-luv-storys.jpg
date: 2010-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/gEvpZoVS2AY"
type: "title track"
singers:
  - Vishal Dadlani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I know you like me, you know I like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you like me, you know I like you"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's get together girl, you know you want to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's get together girl, you know you want to"/>
</div>
<div class="lyrico-lyrics-wrapper">I know you like me, you know I like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you like me, you know I like you"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's get together girl, you know you want to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's get together girl, you know you want to"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raat din aate jaate, hoti hain sau mulakatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat din aate jaate, hoti hain sau mulakatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aage badhti hain baatein, peechhe chhod de..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage badhti hain baatein, peechhe chhod de.."/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste mein hain thehare, aur bhi kitne chehre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste mein hain thehare, aur bhi kitne chehre"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj phir apni aankhein unse jod le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj phir apni aankhein unse jod le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kho jaayega, lamha yeh kho jaayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kho jaayega, lamha yeh kho jaayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudke na phir aayega, raahon mein usko rok le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudke na phir aayega, raahon mein usko rok le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jaayega, hoga jo ho jaayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jaayega, hoga jo ho jaayega"/>
</div>
<div class="lyrico-lyrics-wrapper">You know so hot, you on fire 1 2 3 go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know so hot, you on fire 1 2 3 go"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">I know you like me, you know I like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you like me, you know I like you"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's get together girl, you know you want to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's get together girl, you know you want to"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koi aas paas ho, samjhe jo dil ki baat ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi aas paas ho, samjhe jo dil ki baat ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Soche na aaj yaar woh kal ki baatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soche na aaj yaar woh kal ki baatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal do pal ka sath ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal do pal ka sath ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir na haathon mein haath ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir na haathon mein haath ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoole na yaad aaye phir beeti raatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoole na yaad aaye phir beeti raatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon se keh ke yeh sapna gaya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon se keh ke yeh sapna gaya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Subah ko jaage to rasta naya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subah ko jaage to rasta naya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a man, who's got a plan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a man, who's got a plan"/>
</div>
<div class="lyrico-lyrics-wrapper">Need a girl who will understand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Need a girl who will understand"/>
</div>
<div class="lyrico-lyrics-wrapper">You could be the one for sure 1 2 3 go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You could be the one for sure 1 2 3 go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi karna yaar kar duniya ka na khayal kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi karna yaar kar duniya ka na khayal kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kehti hai jo bhi isko kehne de na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehti hai jo bhi isko kehne de na"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadon se jo bandhkar rakhna hai apne paas to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadon se jo bandhkar rakhna hai apne paas to"/>
</div>
<div class="lyrico-lyrics-wrapper">To jaane de phir yeh dooriya rehne de na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To jaane de phir yeh dooriya rehne de na"/>
</div>
<div class="lyrico-lyrics-wrapper">Marzi se chalti hain jaise hawayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marzi se chalti hain jaise hawayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Waise hi hai meri saari adaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waise hi hai meri saari adaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll be your and you will be mine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll be your and you will be mine"/>
</div>
<div class="lyrico-lyrics-wrapper">Love is such a waste of time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love is such a waste of time"/>
</div>
<div class="lyrico-lyrics-wrapper">You could be my girl for sure 1 2 3 go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You could be my girl for sure 1 2 3 go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I know you like me, you know I like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you like me, you know I like you"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's get together girl, you know you want to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's get together girl, you know you want to"/>
</div>
<div class="lyrico-lyrics-wrapper">I know you like me, you know I like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you like me, you know I like you"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's get together girl, you know you want to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's get together girl, you know you want to"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raat din aate jaate, hoti hain sau mulakatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat din aate jaate, hoti hain sau mulakatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aage badhti hain baatein, peechhe chhod de..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage badhti hain baatein, peechhe chhod de.."/>
</div>
<div class="lyrico-lyrics-wrapper">Raaste mein hain thehare, aur bhi kitne chehre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaste mein hain thehare, aur bhi kitne chehre"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj phir apni aankhein unse jod le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj phir apni aankhein unse jod le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kho jaayega, lamha yeh kho jaayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kho jaayega, lamha yeh kho jaayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudke na phir aayega, raahon mein usko rok le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudke na phir aayega, raahon mein usko rok le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jaayega, hoga jo ho jaayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jaayega, hoga jo ho jaayega"/>
</div>
<div class="lyrico-lyrics-wrapper">You know so hot, you on fire 1 2 3 go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know so hot, you on fire 1 2 3 go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
<div class="lyrico-lyrics-wrapper">Mil gaye jo chhora chhori, hui masti thodi thodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mil gaye jo chhora chhori, hui masti thodi thodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas pyaar ka naam na lena, I hate luv storys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas pyaar ka naam na lena, I hate luv storys"/>
</div>
</pre>
