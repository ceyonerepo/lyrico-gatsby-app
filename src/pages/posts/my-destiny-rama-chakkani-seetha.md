---
title: "my destiny song lyrics"
album: "Rama Chakkani Seetha"
artist: "Kesava Kiran"
lyricist: "Chaitanya Prasad"
director: "Sriharsha Manda"
path: "/albums/rama-chakkani-seetha-lyrics"
song: "My Destiny"
image: ../../images/albumart/rama-chakkani-seetha.jpg
date: 2019-09-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kaUmDamKKKk"
type: "melody"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanulerugani Swapnamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulerugani Swapnamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaserugani Mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaserugani Mounamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Tharagani Shoonyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Tharagani Shoonyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhaya Ghoshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhaya Ghoshaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi Valapulaloni Oogisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi Valapulaloni Oogisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhee Destiny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhee Destiny"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhee Best Anee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhee Best Anee"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhoo Mystery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhoo Mystery"/>
</div>
<div class="lyrico-lyrics-wrapper">Avthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaage Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaage Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha Chedharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha Chedharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyakthithvam Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyakthithvam Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhurochina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhurochina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vyakthithathvamunu Oppukovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyakthithathvamunu Oppukovaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi Yemannadhoo Vinavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi Yemannadhoo Vinavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulloo Unna Katha Kanavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulloo Unna Katha Kanavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Ninneegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Ninneegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Ilaa Che Jaaruthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Ilaa Che Jaaruthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhee Vidhamgaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhee Vidhamgaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchuthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchuthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepini Chedhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepini Chedhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalee Kanneerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalee Kanneerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaruthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaruthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praana Deepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praana Deepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaruthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaruthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhee Vidhamgaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhee Vidhamgaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Kasi Vedukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Kasi Vedukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okaru Nenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaru Nenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaru Verani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaru Verani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhramasi Unnaavuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhramasi Unnaavuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okati Laalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okati Laalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Okati Shoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okati Shoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelusukoney Levuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelusukoney Levuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yedhee Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yedhee Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Madhee Neenuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Madhee Neenuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Ninneegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Ninneegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Ilaa Che Jaaruthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Ilaa Che Jaaruthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhee Vidhamgaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhee Vidhamgaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchuthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchuthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepini Chedhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepini Chedhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Ninnee Yetepoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Ninnee Yetepoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannee Yitepe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannee Yitepe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaguthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaguthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhee Vidhamgaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhee Vidhamgaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Kasi Vedukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Kasi Vedukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Dha Pa Pa Pa Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Dha Pa Pa Pa Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Pa Pa Ma Pa Pa Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Pa Pa Ma Pa Pa Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Pa Ma Pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Pa Ma Pa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Pa Ma Pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Pa Ma Pa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Dha Pa Pa Pa Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Dha Pa Pa Pa Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Pa Pa Pa Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Pa Pa Pa Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Dha Pa Ma Pa Ma Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Dha Pa Ma Pa Ma Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ma Ma Pa Pa Dha Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ma Ma Pa Pa Dha Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha Pa Ma Ma Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha Pa Ma Ma Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Ma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Ma "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ma Ma Ga Ga Ri Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ma Ma Ga Ga Ri Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedabaatee Mana Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedabaatee Mana Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherigenaa Thala Raathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherigenaa Thala Raathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedabaatee Mana Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedabaatee Mana Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherigenaa Thala Raathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherigenaa Thala Raathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedabaatee Mana Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedabaatee Mana Baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherigenaa Thala Raathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherigenaa Thala Raathaa"/>
</div>
</pre>
