---
title: "kalyanam kalyanam song lyrics"
album: "Thoonga Nagaram"
artist: "Sundar C Babu"
lyricist: "Narayana Kavi"
director: "Gaurav Narayanan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Kalyanam Kalyanam"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uUu5C7t3TZ8"
type: "happy"
singers:
  - Chandrababu
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalyaanam kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">venum vaazhvil kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum vaazhvil kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam ha ha ha kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha kalyaanam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasamaagavey ulagathil vaazhavey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasamaagavey ulagathil vaazhavey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasamaagavey ulagathil vaazhavey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasamaagavey ulagathil vaazhavey "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">venum vaazhvil kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum vaazhvil kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam ha ha ha kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha kalyaanam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saroja, Girija, Jalaja, Malaja 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saroja, Girija, Jalaja, Malaja "/>
</div>
<div class="lyrico-lyrics-wrapper">Saroja, Girija, Jalaja, Malaja 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saroja, Girija, Jalaja, Malaja "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaarai inbamey manaiviyinaaley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaarai inbamey manaiviyinaaley "/>
</div>
<div class="lyrico-lyrics-wrapper">maamiyaar veedey sorkkathai poaley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamiyaar veedey sorkkathai poaley "/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhaarai inbamey manaiviyinaaley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaarai inbamey manaiviyinaaley "/>
</div>
<div class="lyrico-lyrics-wrapper">maamiyaar veedey sorkkathai poaley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamiyaar veedey sorkkathai poaley "/>
</div>
<div class="lyrico-lyrics-wrapper">aanukku pennum, pennukku aanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanukku pennum, pennukku aanum "/>
</div>
<div class="lyrico-lyrics-wrapper">aanukku pennum, pennukku aanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanukku pennum, pennukku aanum "/>
</div>
<div class="lyrico-lyrics-wrapper">venum kattaayam vaazhviley kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum kattaayam vaazhviley kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">venum kattaayam vaazhviley kalyaana vaibavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum kattaayam vaazhviley kalyaana vaibavam "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">venum vaazhvil kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum vaazhvil kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam ha ha ha kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha kalyaanam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavan enakkey arul purindhaarey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan enakkey arul purindhaarey "/>
</div>
<div class="lyrico-lyrics-wrapper">aagum en manamey andraiya dhinamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagum en manamey andraiya dhinamey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavan enakkey arul purindhaarey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan enakkey arul purindhaarey "/>
</div>
<div class="lyrico-lyrics-wrapper">aagum en manamey andraiya dhinamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagum en manamey andraiya dhinamey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalukku saadhi illai bedhamillai edhumillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalukku saadhi illai bedhamillai edhumillai "/>
</div>
<div class="lyrico-lyrics-wrapper">yaarukkum thegamillai kattuthaari kayirumillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukkum thegamillai kattuthaari kayirumillai "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maagini roshan Manjula vaagini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maagini roshan Manjula vaagini "/>
</div>
<div class="lyrico-lyrics-wrapper">Maagini roshan Manjula vaagini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maagini roshan Manjula vaagini "/>
</div>
<div class="lyrico-lyrics-wrapper">yaaro oru penmani avaley un kanmani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaro oru penmani avaley un kanmani "/>
</div>
<div class="lyrico-lyrics-wrapper">yaaro oru penmani avaley un kanmani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaro oru penmani avaley un kanmani "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum "/>
</div>
<div class="lyrico-lyrics-wrapper">maappillaiyaagi aanandhamaaga manamaalai soodidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maappillaiyaagi aanandhamaaga manamaalai soodidum "/>
</div>
<div class="lyrico-lyrics-wrapper">thanakku thanakku dhina kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanakku thanakku dhina kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">tharigidathoam thoam kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigidathoam thoam kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">thanakku thanakku dhina kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanakku thanakku dhina kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">tharigidathoam thoam kalyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigidathoam thoam kalyaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">venum vaazhvil kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum vaazhvil kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam ha ha ha kalyaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha kalyaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha kalyaanam "/>
</div>
</pre>
