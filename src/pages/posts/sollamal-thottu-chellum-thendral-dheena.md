---
title: "sollamal thottu chellum thendral song lyrics"
album: "Dheena"
artist: "Yuvan Shankar Raja"
lyricist: "Vijaysagar"
director: "A.R. Murugadoss"
path: "/albums/dheena-lyrics"
song: "Sollamal Thottu Chellum Thendral"
image: ../../images/albumart/dheena.jpg
date: 2001-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A66iM3ySR68"
type: "sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sollamal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalaikulle Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalaikulle Mella Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mounam Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mounam Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaadhalinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaadhalinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamal Thottu Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalaikulle Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalaikulle Mella Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mounam Ennai Kolla Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mounam Ennai Kolla Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaadhalinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaadhalinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellum Thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellum Thendral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Kaadhalin Avasthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Kaadhalin Avasthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirikkum Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirikkum Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naraga Sugam Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraga Sugam Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhungi Vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhungi Vitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Amilam Arundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Amilam Arundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noyaai Nenjil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noyaai Nenjil Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuzhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaindhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marundhai Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marundhai Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Maranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Maranthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalibathin Solaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibathin Solaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiyamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Parithaval Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Parithaval Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellum Thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellum Thendral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pengalin Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pengalin Ullam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padu Kuzhi Enbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padu Kuzhi Enbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhunthavan Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthavan Yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazham Alandhavan Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham Alandhavan Yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Karaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Karaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadandhavan Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhavan Yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukkum Bayathinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukkum Bayathinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Bhoomiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Bhoomiku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meeri Avan Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Avan Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaal Dhaadiyudan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal Dhaadiyudan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaivaan Veedhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaivaan Veedhiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalaikulle Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalaikulle Mella Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mounam Ennai Kolla Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mounam Ennai Kolla Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaadhalinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaadhalinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamal Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamal Thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral En Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devadhaiyin Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadhaiyin Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjathil Kotti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathil Kotti Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannoram Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kaadhal"/>
</div>
</pre>
