---
title: "dheewana song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "Dheewana"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Rvom_bNXzd0"
type: "love"
singers:
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Deewaana Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Nanaithavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Nanaithavan Neethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Deewaana Oh Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Oh Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Pizhindhavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Pizhindhavan Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veetu Kolathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetu Kolathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullikul Olinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullikul Olinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Than Kaadhal Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kaadhal Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakannanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakannanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deewaana Oh Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Oh Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Nanaithavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Nanaithavan Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellikizhamayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellikizhamayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetu Thotahil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetu Thotahil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal Mulaitha Chittu Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Mulaitha Chittu Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Vittathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Vittathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keechu Keechendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keechu Keechendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Koochal Poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochal Poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Kootil Koodukatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kootil Koodukatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallu Kattuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallu Kattuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi Sellumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi Sellumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kothi Sellumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kothi Sellumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Thanthaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Thanthaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Thoazhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Thoazhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanumbodhu Naanam Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanumbodhu Naanam Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Koosuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Koosuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookam Vitruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam Vitruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Endhan Penmai Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Endhan Penmai Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Keli Pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keli Pesuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavaahumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavaahumoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Aadai Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Aadai Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Pogumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Pogumoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Ek Ajnabi Kese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Ek Ajnabi Kese"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi Aage Na Peeche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi Aage Na Peeche"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum He Ke Ho Kya Baathuhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum He Ke Ho Kya Baathuhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deewaana Oh Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Oh Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Nanaithavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Nanaithavan Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Enname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Enname"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Asingame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Asingame"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Kangal Kaadhu Mooku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Kangal Kaadhu Mooku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothi Kondaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothi Kondaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Kaaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Kaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai Ketathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai Ketathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Vaasal Kathavu Jannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Vaasal Kathavu Jannal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranthu Kondanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranthu Kondanal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuvittathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuvittathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliye Sendruvittathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliye Sendruvittathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhi Oruthiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Oruthiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neril Niruthi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril Niruthi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaluttra Sethi Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaluttra Sethi Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Muttuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Muttuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaal Naavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Naavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa Aavanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa Aavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendezhulathai Thavira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendezhulathai Thavira"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Vaarthai Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Vaarthai Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathalikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathalikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Baashai Maatri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Baashai Maatri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucharikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucharikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Ek Ajnabi Kese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Ek Ajnabi Kese"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi Aage Na Peeche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi Aage Na Peeche"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum He Ke Ho Kya Baathuhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum He Ke Ho Kya Baathuhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deewaana Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Nanaithavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Nanaithavan Neethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Deewaana Oh Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Oh Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Pizhindhavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Pizhindhavan Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veetu Kolathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetu Kolathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullikul Olinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullikul Olinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Than Kaadhal Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kaadhal Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakannanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakannanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deewaana Deewaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deewaana Deewaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Nanaithavan Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Nanaithavan Neethaanaa"/>
</div>
</pre>
