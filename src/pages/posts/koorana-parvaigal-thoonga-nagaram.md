---
title: "koorana parvaigal song lyrics"
album: "Thoonga Nagaram"
artist: "Sundar C Babu"
lyricist: "Thamarai"
director: "Gaurav Narayanan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Koorana Parvaigal"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OuyOSWNEbfk"
type: "love"
singers:
  - Hariharan
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koorana paarvaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorana paarvaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">kurumbaana vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbaana vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En thookkam ponathe anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookkam ponathe anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani peiyum iravile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani peiyum iravile "/>
</div>
<div class="lyrico-lyrics-wrapper">pala naatkal kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala naatkal kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un tholil thoonginen en anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholil thoonginen en anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En veedu varugiraai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veedu varugiraai "/>
</div>
<div class="lyrico-lyrics-wrapper">en kaiyil sergiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaiyil sergiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaalai enniye vazhnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaalai enniye vazhnthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai andri yaaridam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai andri yaaridam "/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal poi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal poi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai enni enniye theinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai enni enniye theinthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorana paarvaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorana paarvaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">kurumbaana vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbaana vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En thookkam ponathe anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookkam ponathe anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un viralathu asaiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viralathu asaiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dharisanam kidaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru dharisanam kidaikkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena ena un vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena ena un vaasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam naan nirkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam naan nirkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada sada ena vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada sada ena vaasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhum vizhum mazhaith thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum vizhum mazhaith thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam manam idhu maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam manam idhu maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivinil unaip paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil unaip paarthathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyaaga nee nuraiyaaga naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyaaga nee nuraiyaaga naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vellam ulla mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vellam ulla mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam thathumbidap ponguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam thathumbidap ponguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyaaga nee alaiyaaga naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyaaga nee alaiyaaga naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara vendaam endra podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara vendaam endra podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi vanthu theenduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi vanthu theenduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraana paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraana paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaana vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaana vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En thookkam ponathe anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookkam ponathe anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena udai uduthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena udai uduthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena dhinam samaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena dhinam samaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyinil saayanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyinil saayanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam varai vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam varai vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhigalil neerenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigalil neerenil"/>
</div>
<div class="lyrico-lyrics-wrapper">En manam adhaith thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manam adhaith thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil naan irukkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil naan irukkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam ena neer thaenguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam ena neer thaenguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatrin neeril kallai veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrin neeril kallai veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyalgal seithu thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyalgal seithu thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam ena adhai vaanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam ena adhai vaanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum podhe kannai veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum podhe kannai veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pechai kondru vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pechai kondru vittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi ini pesuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi ini pesuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraana paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraana paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaana vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaana vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En thookkam ponathe anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookkam ponathe anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai andri yaaridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai andri yaaridam"/>
</div>
<div class="lyrico-lyrics-wrapper">En kangal poi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kangal poi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai enni enniye theinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai enni enniye theinthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraana paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraana paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaana vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaana vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En thookkam ponathe anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookkam ponathe anbe"/>
</div>
</pre>
