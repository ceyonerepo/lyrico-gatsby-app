---
title: "kalavani kanulani song lyrics"
album: "Romantic Criminals"
artist: "Sudhakar Mario"
lyricist: "Bala Vardhan"
director: "P Suneel Kumar Reddy"
path: "/albums/romantic-criminals-lyrics"
song: "Kalavani Kanulani"
image: ../../images/albumart/romantic-criminals.jpg
date: 2019-05-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YhzzYJQt9zw"
type: "love"
singers:
  - Lipsika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kalavani kanulani nee yadhanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani kanulani nee yadhanu "/>
</div>
<div class="lyrico-lyrics-wrapper">thaaki nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaki nilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">valapule valachina ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapule valachina ee "/>
</div>
<div class="lyrico-lyrics-wrapper">pasi thanaale vidichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi thanaale vidichina"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamga varamga ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamga varamga ee "/>
</div>
<div class="lyrico-lyrics-wrapper">kalayikalakai vethikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayikalakai vethikina"/>
</div>
<div class="lyrico-lyrics-wrapper">ila ee kshanam lo nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ila ee kshanam lo nee "/>
</div>
<div class="lyrico-lyrics-wrapper">vodini viduvaku adhi chaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vodini viduvaku adhi chaala"/>
</div>
<div class="lyrico-lyrics-wrapper">pedhale kalisthe bigisthe dhahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedhale kalisthe bigisthe dhahame"/>
</div>
<div class="lyrico-lyrics-wrapper">varaale kuristhe parusthaa dhehame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaale kuristhe parusthaa dhehame"/>
</div>
<div class="lyrico-lyrics-wrapper">padhalai pilisthe japistha thapassai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhalai pilisthe japistha thapassai"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasse gelisthe phalistha pudamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasse gelisthe phalistha pudamila"/>
</div>
<div class="lyrico-lyrics-wrapper">ubiki egasina korike uniki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ubiki egasina korike uniki "/>
</div>
<div class="lyrico-lyrics-wrapper">thelipene usuruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelipene usuruke"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavani kanulani nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani kanulani nee "/>
</div>
<div class="lyrico-lyrics-wrapper">yadhanu thaaki nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yadhanu thaaki nilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">valapule valachina ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapule valachina ee "/>
</div>
<div class="lyrico-lyrics-wrapper">pasi thanaale vidichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi thanaale vidichina"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamga varamga ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamga varamga ee "/>
</div>
<div class="lyrico-lyrics-wrapper">kalayikalakai vethikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayikalakai vethikina"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvukochina mohame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvukochina mohame "/>
</div>
<div class="lyrico-lyrics-wrapper">urakalesenu gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakalesenu gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">jathallo pramodham thelusukovaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jathallo pramodham thelusukovaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavani kanulani nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani kanulani nee "/>
</div>
<div class="lyrico-lyrics-wrapper">yadhanu thaaki nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yadhanu thaaki nilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">valapule valachina ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapule valachina ee "/>
</div>
<div class="lyrico-lyrics-wrapper">pasi thanaale vidichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi thanaale vidichina"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamga varamga ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamga varamga ee "/>
</div>
<div class="lyrico-lyrics-wrapper">kalayikalakai vethikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayikalakai vethikina"/>
</div>
<div class="lyrico-lyrics-wrapper">ila ee kshanam lo nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ila ee kshanam lo nee "/>
</div>
<div class="lyrico-lyrics-wrapper">vodini viduvaku adhi chaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vodini viduvaku adhi chaala"/>
</div>
</pre>
