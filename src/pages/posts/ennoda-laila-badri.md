---
title: "ennoda laila song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Ennoda Laila"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dlC6A3MplMo"
type: "love"
singers:
  - Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennoda laila varalae maila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda laila varalae maila"/>
</div>
<div class="lyrico-lyrics-wrapper">Signallae kidaikala kidaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signallae kidaikala kidaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula coola oothudhu cola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula coola oothudhu cola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagamae adangala adangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagamae adangala adangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pappa nee konjam nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pappa nee konjam nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku unakku ithainai lollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku unakku ithainai lollu"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjila kuthathae mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjila kuthathae mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaya pazhama sollu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya pazhama sollu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda laila varalae maila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda laila varalae maila"/>
</div>
<div class="lyrico-lyrics-wrapper">Signallae kidaikala kidaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signallae kidaikala kidaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula coola oothudhu cola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula coola oothudhu cola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagamae adangala adangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagamae adangala adangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada.. manasa valaiya virichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada.. manasa valaiya virichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha maina maatalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha maina maatalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru manjaa thadaviyum parthen parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru manjaa thadaviyum parthen parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealae kedaikalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealae kedaikalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammu inga varavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammu inga varavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhila kettu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhila kettu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava iduppula madippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava iduppula madippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasangudhu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasangudhu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Quick-ah vara sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quick-ah vara sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Why doesn’t she talk to me Vaavaa chinnakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she talk to me Vaavaa chinnakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love u konjam sollukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love u konjam sollukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Why doesn’t she walk with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she walk with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivandhan unnakku Pakkam konjam vaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivandhan unnakku Pakkam konjam vaakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda laila varalae maila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda laila varalae maila"/>
</div>
<div class="lyrico-lyrics-wrapper">Signallae kidaikala kidaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signallae kidaikala kidaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula coola oothudhu cola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula coola oothudhu cola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagamae adangala adangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagamae adangala adangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thethi singara chittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thethi singara chittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai parthu thannaala varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai parthu thannaala varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Allungi kulungum thala thala udumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allungi kulungum thala thala udumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo vandhu engitta tharuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo vandhu engitta tharuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figura konjam oram katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figura konjam oram katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Shortu route irundha sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shortu route irundha sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pannum bejarai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pannum bejarai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Medaththa parthu message sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medaththa parthu message sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En height idhu podhadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En height idhu podhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enda alluthukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enda alluthukura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ellam therinja allukku munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ellam therinja allukku munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba alatikiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba alatikiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasu payanai moraika vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu payanai moraika vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada konjam sirikka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada konjam sirikka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava ora kannil partha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava ora kannil partha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Looku kuduka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looku kuduka sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Why doesn’t she look at me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she look at me"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru parvai paradi Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru parvai paradi Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Why doesn’t she care for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she care for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Chee chee endru Sonna vambae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chee chee endru Sonna vambae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Why doesn’t she starve for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she starve for me"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala varuva poruda nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala varuva poruda nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Why doesn’t she just love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she just love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona pottum love you sollumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pottum love you sollumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Why doesn’t she just love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she just love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam pozhaikattum Love you sollumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam pozhaikattum Love you sollumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Why doesn’t she just kiss me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she just kiss me"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada paduthara Umma kudumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada paduthara Umma kudumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Why doesn’t she just love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she just love me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjuraan thalaivan Love you sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjuraan thalaivan Love you sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Why doesn’t she just love me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why doesn’t she just love me"/>
</div>
</pre>
