---
title: "kekkatha vathiyam song lyrics"
album: "Merku Thodarchi Malai"
artist: "Ilaiyaraaja"
lyricist: "unknown"
director: "Lenin Bharathi"
path: "/albums/merku-thodarchi-malai-lyrics"
song: "Kekkatha Vathiyam"
image: ../../images/albumart/merku-thodarchi-malai.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nAtALQZk2Wg"
type: "happy"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">poo onnu manja saratuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo onnu manja saratuku"/>
</div>
<div class="lyrico-lyrics-wrapper">saaijaadap pakuthu pakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaijaadap pakuthu pakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathagam pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathagam pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">sangathi ketkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangathi ketkala"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi sanathidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi sanathidam "/>
</div>
<div class="lyrico-lyrics-wrapper">samatham vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samatham vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">aen saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aen saami"/>
</div>
<div class="lyrico-lyrics-wrapper">vali nee kaami thunaiya va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali nee kaami thunaiya va"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanathukku nalathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanathukku nalathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yar yar senja thavathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar yar senja thavathula"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum vanthirukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum vanthirukom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadatha katula kootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadatha katula kootula"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaga sernthu irukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaga sernthu irukom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam podum pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam podum pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum pogath thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum pogath thane"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum poga"/>
</div>
<div class="lyrico-lyrics-wrapper">thanaa namakup pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaa namakup pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">varungaala vaalvukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varungaala vaalvukku"/>
</div>
<div class="lyrico-lyrics-wrapper">aatharam manasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatharam manasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai endru aengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai endru aengi"/>
</div>
<div class="lyrico-lyrics-wrapper">kidakka venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidakka venaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">poo onnu manja saratuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo onnu manja saratuku"/>
</div>
<div class="lyrico-lyrics-wrapper">saaijaadap pakuthu pakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaijaadap pakuthu pakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathagam pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathagam pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">sangathi ketkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangathi ketkala"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi sanathidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi sanathidam "/>
</div>
<div class="lyrico-lyrics-wrapper">samatham vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samatham vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">aen saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aen saami"/>
</div>
<div class="lyrico-lyrics-wrapper">vali nee kaami thunaiya va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali nee kaami thunaiya va"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanathukku nalathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanathukku nalathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aathaalum appanum mamanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathaalum appanum mamanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vethaalu illai yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethaalu illai yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">paakatha pakathu vetilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakatha pakathu vetilum"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkaatha pasamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha pasamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">munna pina irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munna pina irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">onna irukum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna irukum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagip pogum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagip pogum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam kitta pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam kitta pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethukaaga aenganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethukaaga aenganum"/>
</div>
<div class="lyrico-lyrics-wrapper">namakaaga naama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakaaga naama than"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu vitu neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu vitu neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">oda aagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda aagathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">poo onnu manja saratuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo onnu manja saratuku"/>
</div>
<div class="lyrico-lyrics-wrapper">saaijaadap pakuthu pakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaijaadap pakuthu pakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathagam pakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathagam pakala"/>
</div>
<div class="lyrico-lyrics-wrapper">sangathi ketkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangathi ketkala"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi sanathidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi sanathidam "/>
</div>
<div class="lyrico-lyrics-wrapper">samatham vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samatham vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">aen saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aen saami"/>
</div>
<div class="lyrico-lyrics-wrapper">vali nee kaami thunaiya va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali nee kaami thunaiya va"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru sanathukku nalathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru sanathukku nalathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketkaatha vaathiyam ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkaatha vaathiyam ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraana oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraana oorukulla"/>
</div>
</pre>
