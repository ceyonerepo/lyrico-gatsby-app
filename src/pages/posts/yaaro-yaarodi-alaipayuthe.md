---
title: 'yaaro yaarodi song lyrics'
album: 'Alaipayuthey'
artist: 'A R Rahman'
lyricist: 'Vairamuthu'
director: 'Maniratnam'
path: '/albums/Alaipayuthey-song-lyrics'
song: 'Yaaro Yaarodi'
image: ../../images/albumart/Alaipayuthey.jpg
date: 2000-04-14
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/wkB69vKHUi8'
type: 'sad'
singers: 
- Mahalakshmi Iyer
- Richa Sharma
- Vaishali Samant
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey dum dum dam dammakuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey dum dum dam dammakuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dam damakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dum dam damakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dum hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dum dum hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dum dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey dum dum dum dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dammakku dum dammukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dammakku dum dammukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dammakku dum dammakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dammakku dum dammakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dum dum dum dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dum dum dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dammakku dum dammukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dammakku dum dammukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dammakku dum dammakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dammakku dum dammakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dum dum dum dum dum…aaaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Dum dum dum dum dum dum…aaaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha..ha..ha..ha…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ha..ha..ha..ha…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoda purusan
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnoda purusan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thimirukku Arasan (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thimirukku Arasan"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eekki pola nilaavadikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Eekki pola nilaavadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhiranaar pandhadikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Indhiranaar pandhadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha pandhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha pandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerththadippavano sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Theerththadippavano sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sandhana pottazhagai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhana pottazhagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanja nadaiyazhagai
<input type="checkbox" class="lyrico-select-lyric-line" value="Saanja nadaiyazhagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli vetti
<input type="checkbox" class="lyrico-select-lyric-line" value="Velli vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiyevano sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattiyevano sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoda purusan
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnoda purusan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thimirukku Arasan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thimirukku Arasan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thangaththukku verkkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thangaththukku verkkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarunga paarunga..
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarunga paarunga.."/>
</div>
<div class="lyrico-lyrics-wrapper">Saandhu kannum
<input type="checkbox" class="lyrico-select-lyric-line" value="Saandhu kannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangudhu enunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayangudhu enunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo…ooooo…aaaaa…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohooo…ooooo…aaaaa….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Muththazhagi ingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththazhagi ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illeenga sollunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Illeenga sollunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththamittu engae thodunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththamittu engae thodunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththamaaga solli kudunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththamaaga solli kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli kudunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Solli kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudunga kudunga kudunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudunga kudunga kudunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanni ponnu nallaa nadippaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanni ponnu nallaa nadippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nadippaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava nadippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattilukku paattu padippaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattilukku paattu padippaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoda purusan
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnoda purusan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thimirukku Arasan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thimirukku Arasan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanaee….eee…ga vaaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanaee….eee…ga vaaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalam kannaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaalam kannaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongodikku kannaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongodikku kannaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongodikku kannaalam (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongodikku kannaalam"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalam…kannaalam…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaalam…kannaalam…"/>
</div>
<div class="lyrico-lyrics-wrapper">poongodikku kannaalam…
<input type="checkbox" class="lyrico-select-lyric-line" value="poongodikku kannaalam…"/>
</div>
<div class="lyrico-lyrics-wrapper">poongodikku kannaalam..
<input type="checkbox" class="lyrico-select-lyric-line" value="poongodikku kannaalam.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pon thaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Pon thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnukkedhukku edhukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnukkedhukku edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu mudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Moonu mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduvadhedhukku…aaaaa…aaaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Poduvadhedhukku…aaaaa…aaaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Urimaikaaga oththa mudichchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Urimaikaaga oththa mudichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaikaaga oththa mudichchu adiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Urimaikaaga oththa mudichchu adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravukkaaga rendaam mudichchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravukkaaga rendaam mudichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkaaga moonaam mudichchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorukkaaga moonaam mudichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudichchu…mudichchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudichchu…mudichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudichchu mudichchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudichchu mudichchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pon thaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Pon thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnukkedhukku edhukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnukkedhukku edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu mudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Moonu mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduvadhedhukku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Poduvadhedhukku…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoda purusan
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnoda purusan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thimirukku Arasan (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thimirukku Arasan"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Un thimirukku Arasan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thimirukku Arasan…"/>
</div>
</pre>