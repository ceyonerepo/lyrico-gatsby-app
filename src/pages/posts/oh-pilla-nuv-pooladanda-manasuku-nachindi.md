---
title: "oh pilla nuv pooladanda song lyrics"
album: "Manasuku Nachindi"
artist: "Radhan"
lyricist: "Ananta Sriram"
director: "Manjula Ghattamaneni"
path: "/albums/manasuku-nachindi-lyrics"
song: "Oh Pilla Nuv Pooladanda"
image: ../../images/albumart/manasuku-nachindi.jpg
date: 2018-02-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vUBA05NGQvg"
type: "happy"
singers:
  -	Naresh Iyer
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladanda "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Vaadu Pranam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Vaadu Pranam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Vaadi Pranam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Vaadi Pranam "/>
</div>
<div class="lyrico-lyrics-wrapper">Yendukinta Mounam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendukinta Mounam "/>
</div>
<div class="lyrico-lyrics-wrapper">Maaranivvu Vainam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaranivvu Vainam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Choopu Banam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Choopu Banam "/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyave Nuvu Ee Kshanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyave Nuvu Ee Kshanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Niku Vadi Thodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niku Vadi Thodai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Vaadi Needai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Vaadi Needai "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvutunte Andam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvutunte Andam "/>
</div>
<div class="lyrico-lyrics-wrapper">Allukunte Andam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukunte Andam "/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakante Andam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakante Andam "/>
</div>
<div class="lyrico-lyrics-wrapper">Undadhee Bandhame Varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undadhee Bandhame Varam "/>
</div>
<div class="lyrico-lyrics-wrapper">Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenni Rojulina Meerem Veru Karu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenni Rojulina Meerem Veru Karu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yevvaraina Neeti Mate Nammutharu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvaraina Neeti Mate Nammutharu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chethilona Cheyyi Vesukuntu Veyyi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethilona Cheyyi Vesukuntu Veyyi "/>
</div>
<div class="lyrico-lyrics-wrapper">Janmalaina Veedare Nijam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janmalaina Veedare Nijam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Pilla Neelo Yedo Unde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Neelo Yedo Unde "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Marechestundee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Marechestundee "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Alochanalu Nannu Vachi Vatestunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Alochanalu Nannu Vachi Vatestunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanam Dooramaithe Gunde Lagesthunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanam Dooramaithe Gunde Lagesthunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Kalalu Uliki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Kalalu Uliki "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Paliki Chelimestunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Paliki Chelimestunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paine Dyasantha Neethone Rojuantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paine Dyasantha Neethone Rojuantha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi Melisi Vayassu Chilipi Nalo Ponge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Melisi Vayassu Chilipi Nalo Ponge "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maate Naa Paatai Vaanalle Vaateste 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maate Naa Paatai Vaanalle Vaateste "/>
</div>
<div class="lyrico-lyrics-wrapper">Taadisipothonde Pilla Murisipothade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taadisipothonde Pilla Murisipothade "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Kalalu Yedu Janmalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Kalalu Yedu Janmalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Deepalu Deevistoo Unnayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Deepalu Deevistoo Unnayi "/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru Okkatai Okkachota Niluchunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru Okkatai Okkachota Niluchunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Okkatai Sambaralu Chestade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Okkatai Sambaralu Chestade "/>
</div>
<div class="lyrico-lyrics-wrapper">Made For Each Other Anna Maata Meekene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made For Each Other Anna Maata Meekene "/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Nijam Nijam Nijam Aneelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Nijam Nijam Nijam Aneelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Palapuntha Kooda Meekai Gantulesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapuntha Kooda Meekai Gantulesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nela Paiki Rada Neelo Prema Choosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela Paiki Rada Neelo Prema Choosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru Iddaraina Oopiri Okkatega 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru Iddaraina Oopiri Okkatega "/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Sandehamenduke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Sandehamenduke "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pilla Nuv Puladandavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pilla Nuv Puladandavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baboo Nuv Neeti Kundavoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baboo Nuv Neeti Kundavoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Yededadaaginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Yededadaaginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Neetibottu Poola Chettuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Neetibottu Poola Chettuke"/>
</div>
</pre>
