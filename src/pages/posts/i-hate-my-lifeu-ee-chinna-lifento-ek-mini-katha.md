---
title: "i hate my life'u song lyrics"
album: "Ek Mini Katha"
artist: "Pravin Lakkaraju"
lyricist: "Bhaskarabhatla"
director: "Karthik Rapolu"
path: "/albums/ek-mini-katha-lyrics"
song: "I Hate My Life'u - Ee Chinna Lifento"
image: ../../images/albumart/ek-mini-katha.jpg
date: 2021-05-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/5abUJpaao2g"
type: "sad"
singers:
  - Hema Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Chi Chi Ee Chinna Lifento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chi Chi Ee Chinna Lifento"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraaku Theppisthunnaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraaku Theppisthunnaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnappatinundhi Inthegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnappatinundhi Inthegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchainaa Changevakunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchainaa Changevakunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KingU Size Cigarette Laa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KingU Size Cigarette Laa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dharjaaga Undaalanukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharjaaga Undaalanukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Small Size Beedilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Small Size Beedilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke Ussoorumantundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke Ussoorumantundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Hate My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Hate My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Kashtaalake Care Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Kashtaalake Care Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">My Life’u Life’u Life’u Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’u Life’u Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathee Kshanam Mood Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathee Kshanam Mood Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipothunnadhi Light Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipothunnadhi Light Off-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Enaatiko Take Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Enaatiko Take Off-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life’u I Hate You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u I Hate You"/>
</div>
<div class="lyrico-lyrics-wrapper">My Life’u I Hate You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u I Hate You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andariki Happinessu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andariki Happinessu "/>
</div>
<div class="lyrico-lyrics-wrapper">Full Bottle Size Lo Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Bottle Size Lo Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakemo Quarter Size 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakemo Quarter Size "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Eppatinuncho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Eppatinuncho"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkokkadi Future Choosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkokkadi Future Choosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Silver Screen Rangelo Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silver Screen Rangelo Unde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakepudu Cell Phone Screen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakepudu Cell Phone Screen "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Em Cheyyaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Em Cheyyaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Arachethi Rekha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Arachethi Rekha "/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanthainaa Peragatledhayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanthainaa Peragatledhayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Adrushtamaithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Adrushtamaithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Angulamu Podugavvatledhayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angulamu Podugavvatledhayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Hate My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Hate My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Kashtaalake Care Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Kashtaalake Care Off-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life’u Life’u Life’u Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’u Life’u Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathee Kshanam Mood Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathee Kshanam Mood Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipothunnadhi Light Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipothunnadhi Light Off-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Enaatiko Take Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Enaatiko Take Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Thintemo Ekkatledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thintemo Ekkatledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidharemo Pattatledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidharemo Pattatledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenimeedha Undatledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenimeedha Undatledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Concentration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Concentration"/>
</div>
<div class="lyrico-lyrics-wrapper">Allopathy Ayurvedam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allopathy Ayurvedam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhante Homeo Vaidhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhante Homeo Vaidhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Mandhutho Nayamavuthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Mandhutho Nayamavuthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Frustration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Frustration"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanu Aa Brhamma Chekke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Aa Brhamma Chekke "/>
</div>
<div class="lyrico-lyrics-wrapper">Time Ki Matti Saripoledhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Ki Matti Saripoledhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poni Edhotile Ani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poni Edhotile Ani "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnaga Sardhesaademo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnaga Sardhesaademo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Hate My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Hate My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Kashtaalake Care Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Kashtaalake Care Off-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life’u Life’u Life’u Chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’u Life’u Chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathee Kshanam Mood Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathee Kshanam Mood Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Life’u Life’uLife’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ipothunnadhi Light Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipothunnadhi Light Off-U"/>
</div>
<div class="lyrico-lyrics-wrapper">My Life’u Life’uLife’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life’u Life’uLife’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Enaatiko Take Off-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Enaatiko Take Off-U"/>
</div>
</pre>
