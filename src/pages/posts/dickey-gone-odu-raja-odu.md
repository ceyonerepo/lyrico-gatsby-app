---
title: "dickey gone song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Ken Royson"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Dickey Gone"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/heXQUFa0kdE"
type: "happy"
singers:
  - Aryan Dinesh
  - Ken Royson
  - Tosh Nanda
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gone Gone Gone Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gone Gone Gone Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gone Gone Gone Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gone Gone Gone Gone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gone Gone Gone Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gone Gone Gone Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gone Gone Gone Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gone Gone Gone Gone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ama Sami Muttuna Muttu'la 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama Sami Muttuna Muttu'la "/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey'lona Bush'ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey'lona Bush'ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pogum Idam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogum Idam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Naanum Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tv'la Josiyakaaren Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tv'la Josiyakaaren Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Red-la Gandanu Waarnu Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Red-la Gandanu Waarnu Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthatu Poyidumnu Saabam Utta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthatu Poyidumnu Saabam Utta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatite Mulinu Ezhamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatite Mulinu Ezhamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Peter Vanthan Kavuthuuttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peter Vanthan Kavuthuuttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Matteru Kaathula Parakka Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matteru Kaathula Parakka Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiya Ippo Thorathauttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiya Ippo Thorathauttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Redula Underware Potu Uttaaaaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Redula Underware Potu Uttaaaaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All You Good People
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All You Good People"/>
</div>
<div class="lyrico-lyrics-wrapper">Listen To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Listen To Me"/>
</div>
<div class="lyrico-lyrics-wrapper">This Is The Story Of My Dickey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Is The Story Of My Dickey"/>
</div>
<div class="lyrico-lyrics-wrapper">And How I Lost It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And How I Lost It"/>
</div>
<div class="lyrico-lyrics-wrapper">Here We Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Here We Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wanted-Ah Maatuna Maanu Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted-Ah Maatuna Maanu Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Netula Sikkina Meen Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netula Sikkina Meen Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothoda Kotha Alla Poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothoda Kotha Alla Poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnuta Oorellam Solla Poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnuta Oorellam Solla Poran"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapikka Let Me Try Try
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapikka Let Me Try Try"/>
</div>
<div class="lyrico-lyrics-wrapper">Else Vazhkai Fulla Cry Cry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Else Vazhkai Fulla Cry Cry"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatuna Mutton Fry Fry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatuna Mutton Fry Fry"/>
</div>
<div class="lyrico-lyrics-wrapper">My Aavi Going Sky Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Aavi Going Sky Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Intha Torture Why Why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Intha Torture Why Why"/>
</div>
<div class="lyrico-lyrics-wrapper">BP Raising High High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BP Raising High High"/>
</div>
<div class="lyrico-lyrics-wrapper">Basica Na Romba Shy Shy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basica Na Romba Shy Shy"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Life Ku Tata Bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Life Ku Tata Bye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bye Bye Bye Bye Bye Bye Bye Bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye Bye Bye Bye Bye Bye Bye Bye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bye Bye Bye Bye Bye Bye Bye Bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye Bye Bye Bye Bye Bye Bye Bye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dickey Dickey Dickey Gone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dickey Dickey Dickey Gone"/>
</div>
</pre>
