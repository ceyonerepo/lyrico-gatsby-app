---
title: "mayam seidhayo song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Viveka"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Mayam Seidhayo"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h4DOt79AjAs"
type: "love"
singers:
  - Sangeetha Rajeshwaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkanakku Thakka Dhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkanakku Thakka Dhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaari Sendrai Pennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari Sendrai Pennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Nindren Kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Nindren Kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu Seithai Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu Seithai Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Nindren Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Nindren Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooohoooohhoohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooohoooohhoohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooohoooohhoohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooohoooohhoohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woahuuahhhohhwoahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woahuuahhhohhwoahh"/>
</div>
<div class="lyrico-lyrics-wrapper">Uhhhoo Woahhuahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uhhhoo Woahhuahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana Chedi Valarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Chedi Valarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottam Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaanai Vandhu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai Vandhu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Karai Purandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Karai Purandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooda Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooda Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil Mul Nuniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil Mul Nuniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Korthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Korthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Sevi Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Sevi Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Vegu Thooram Vizhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Vegu Thooram Vizhundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Peyarai Naan Maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peyarai Naan Maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal Pola Kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Pola Kidanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solla Vandhaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Thuli Mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Thuli Mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaira Karkkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaira Karkkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagai Koora Tamilil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagai Koora Tamilil"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Sorkkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Sorkkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesai Mudi Kariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Mudi Kariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugam Purkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugam Purkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavi Mella Kadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi Mella Kadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengum Parkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengum Parkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarugil Mul Chediyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarugil Mul Chediyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaga Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaga Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Ethiril Thondurugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Ethiril Thondurugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurumbagum Malaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbagum Malaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamma Hamma Hammammmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamma Hamma Hammammmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaari Sendrai Pennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari Sendrai Pennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Nindren Kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Nindren Kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu Seithai Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu Seithai Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Nindren Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Nindren Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kayam Seidhayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kayam Seidhayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Solla Vandhaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Solla Vandhaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooohoooohhoohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooohoooohhoohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooohoooohhoohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooohoooohhoohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">U Got To Do It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Got To Do It"/>
</div>
</pre>
