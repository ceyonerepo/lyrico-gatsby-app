---
title: "pollatha  madhana song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Vaali - Jagdish Khebudkar"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Pollatha Madhana Paanam"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nsv_Iinjfds"
type: "happy"
singers:
  - Mahalakshmi Iyer
  - Anupama Deshpande
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ippiriki Salo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippiriki Salo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu Kushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Kushi"/>
</div>
<div class="lyrico-lyrics-wrapper">Me Vagu Kushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Me Vagu Kushi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Sagu Kushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Sagu Kushi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Me Nahalabas Ye Paanapath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Me Nahalabas Ye Paanapath"/>
</div>
<div class="lyrico-lyrics-wrapper">So Koruna Ala Rama Path
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Koruna Ala Rama Path"/>
</div>
<div class="lyrico-lyrics-wrapper">Keli Da Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keli Da Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondi Ke Sadi Ke Liye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondi Ke Sadi Ke Liye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhere Dhere Dhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhere Dhere Dhere"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadha Koth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadha Koth"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu Dhadu Dhadu Dhadu Dhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu Dhadu Dhadu Dhadu Dhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poora Karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poora Karo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Bis Laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Bis Laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh Has Laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh Has Laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Me Lago Chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Me Lago Chale"/>
</div>
<div class="lyrico-lyrics-wrapper">Bekar Bekar Bekar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekar Bekar Bekar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uttaanpaadaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttaanpaadaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Devan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Devan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aago Baiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aago Baiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Che Pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Che Pe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjidichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara Ooduruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara Ooduruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenjidichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambal Techchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambal Techchaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thechcha Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thechcha Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaa Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnaa Pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththa Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththa Pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooosi Poda Vaiththiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooosi Poda Vaiththiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjidichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara Ooduruvi Neenjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara Ooduruvi Neenjidichchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakku Na Dhin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakku Na Dhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakku Na Dhin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakku Na Dhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakina Thakina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakina Thakina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakina Thakkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakina Thakkina"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha Kida Thirukida Thirukida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha Kida Thirukida Thirukida"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha Kida Thirukida Thirukida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha Kida Thirukida Thirukida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodaiyile Avan Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyile Avan Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha Thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha Thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaiyile Avan Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaiyile Avan Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venni Thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venni Thanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naala Vara Ennaavumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naala Vara Ennaavumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichchaaba Pichchedukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichchaaba Pichchedukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaala Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaala Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Vala Kazhandiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Vala Kazhandiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kachcha Kooda Koranjiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachcha Kooda Koranjiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambu Thechcha Kaaranamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambu Thechcha Kaaranamthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangiduchchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangiduchchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Summa Iruppaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Summa Iruppaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Devan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Devan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjidichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara Ooduruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara Ooduruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenjidichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambal Techchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambal Techchaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thechcha Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thechcha Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaa Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnaa Pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththa Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththa Pochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooosi Poda Vaiththiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooosi Poda Vaiththiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollatha Madana Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollatha Madana BaaNam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Madana BaaNam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjidichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjidichchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil Vantha Kaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Vantha Kaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalil Kooda Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalil Kooda Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravil Sonna Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Sonna Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Koota Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Koota Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatti Eduththu Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Eduththu Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitta Swaram Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitta Swaram Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol Thattai Kaathil Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Thattai Kaathil Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malligai Mottodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligai Mottodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Meni Vadivellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meni Vadivellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gama Gama Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama Gama Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Aanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Aanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagina Thaginanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagina Thaginanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagina Thaginanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagina Thaginanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagina Thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagina Thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagina Thagina Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagina Thagina Na Na"/>
</div>
</pre>
