---
title: 'pathu kaasu song lyrics'
album: 'Jail'
artist: 'G V Prakash Kumar'
lyricist: 'Arivu'
director: 'G Vasanthabalan'
path: '/albums/jail-song-lyrics'
song: 'Pathu Kaasu'
image: ../../images/albumart/jail.jpg
date: 2021-12-09
lang: tamil
singers: 
- G.V. Prakash Kumar
youtubeLink: "https://www.youtube.com/embed/gr-3-YZiYOY"
type: 'friendship'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Pathu kaasu illanaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu kaasu illanaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">panakkaran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panakkaran da"/>
</div>
<div class="lyrico-lyrics-wrapper">En soththu sogam ellaamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En soththu sogam ellaamae "/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban thaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban thaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththa thaaiya pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa thaaiya pola "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu thudippaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu thudippaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">En pakkathula eppodhumae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pakkathula eppodhumae "/>
</div>
<div class="lyrico-lyrics-wrapper">iruppaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruppaana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lappu tappu lappu tappu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lappu tappu lappu tappu "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu natpu natpu natpunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu natpu natpu natpunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">thaan eppodhum ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan eppodhum ninaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Lappu tappu lappu tappu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lappu tappu lappu tappu "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu natpu natpu natpunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu natpu natpu natpunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">thaan eppodhum ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan eppodhum ninaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthaiyum thaaiyayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaiyum thaaiyayum "/>
</div>
<div class="lyrico-lyrics-wrapper">indha oorae aalu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha oorae aalu da"/>
</div>
<div class="lyrico-lyrics-wrapper">En nanban pol oru sondham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nanban pol oru sondham "/>
</div>
<div class="lyrico-lyrics-wrapper">inga yaaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga yaaru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu kaasu illanaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu kaasu illanaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">panakkaran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panakkaran da"/>
</div>
<div class="lyrico-lyrics-wrapper">En soththu sogam ellaamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En soththu sogam ellaamae "/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban thaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban thaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththa thaaiya pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa thaaiya pola "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu thudippaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu thudippaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">En pakkathula eppodhumae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pakkathula eppodhumae "/>
</div>
<div class="lyrico-lyrics-wrapper">iruppaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruppaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu phone-ah potta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu phone-ah potta "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu nippan munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu nippan munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-u peru thaanae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u peru thaanae "/>
</div>
<div class="lyrico-lyrics-wrapper">onna number killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna number killadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethuna gethungo en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethuna gethungo en "/>
</div>
<div class="lyrico-lyrics-wrapper">nanban varaan oththungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban varaan oththungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaama nikkurapo vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaama nikkurapo vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya kuththaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya kuththaango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu kaiya potta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu kaiya potta "/>
</div>
<div class="lyrico-lyrics-wrapper">titanic nikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="titanic nikkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai vandhuputta machaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai vandhuputta machaan "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae bejaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthaiyum thaaiyayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaiyum thaaiyayum "/>
</div>
<div class="lyrico-lyrics-wrapper">indha oorae aalu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha oorae aalu da"/>
</div>
<div class="lyrico-lyrics-wrapper">En nanban pol oru sondham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nanban pol oru sondham "/>
</div>
<div class="lyrico-lyrics-wrapper">inga yaaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga yaaru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu kaasu illanaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu kaasu illanaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">panakkaran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panakkaran da"/>
</div>
<div class="lyrico-lyrics-wrapper">En soththu sogam ellaamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En soththu sogam ellaamae "/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban thaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban thaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththa thaaiya pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa thaaiya pola "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu thudippaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu thudippaana da"/>
</div>
<div class="lyrico-lyrics-wrapper">En pakkathula eppodhumae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pakkathula eppodhumae "/>
</div>
<div class="lyrico-lyrics-wrapper">iruppaana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruppaana da"/>
</div>
</pre>