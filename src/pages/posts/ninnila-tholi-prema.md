---
title: "ninnila song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Ninnila"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sNKD014uRj0"
type: "love"
singers:
  -	Armaan Malik
  - S Thaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninnila Ninnila Chusane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnila Ninnila Chusane "/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo Kallalo Daachane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo Kallalo Daachane "/>
</div>
<div class="lyrico-lyrics-wrapper">Reppale Veyyananthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppale Veyyananthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula Pandage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Pandage "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnila Ninnila Chusane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnila Ninnila Chusane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugule Thadabade Nivalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugule Thadabade Nivalle "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo Vinapadindhiga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo Vinapadindhiga "/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Chappude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Chappude"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Cheri Poye Naa Pranam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Cheri Poye Naa Pranam "/>
</div>
<div class="lyrico-lyrics-wrapper">Korenemo Ninne Ee Hrudayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korenemo Ninne Ee Hrudayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Mundhundhe Andham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Mundhundhe Andham "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Aanandham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Aanandham "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nene Marchipoyela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nene Marchipoyela "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kshanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kshanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Varshaniki Sparshunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Varshaniki Sparshunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manase Thakenugaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manase Thakenugaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yadhalo Nee Pere 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yadhalo Nee Pere "/>
</div>
<div class="lyrico-lyrics-wrapper">Palikele Ivale Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikele Ivale Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Varshaniki Sparshunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Varshaniki Sparshunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manase Thakenugaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manase Thakenugaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yadhalo Nee Pere 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yadhalo Nee Pere "/>
</div>
<div class="lyrico-lyrics-wrapper">Palikele Ivale Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikele Ivale Ila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi Tholi Preme Dacheyikalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi Tholi Preme Dacheyikalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Chiru Navve Aapeyikila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Chiru Navve Aapeyikila"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali Chali Gaale Veechenthala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Chali Gaale Veechenthala "/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Mari Nanne Cherenthala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Mari Nanne Cherenthala "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Neenunchi Nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Neenunchi Nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Baitaku Ranivvuu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baitaku Ranivvuu "/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbu Theralu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbu Theralu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thenchukunna Jabilammala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenchukunna Jabilammala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Varshaniki Sparshunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Varshaniki Sparshunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manase Thakenugaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manase Thakenugaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yadhalo Nee Pere 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yadhalo Nee Pere "/>
</div>
<div class="lyrico-lyrics-wrapper">Palikele Ivale Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikele Ivale Ila"/>
</div>
</pre>
