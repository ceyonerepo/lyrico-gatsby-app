---
title: "marana matta song lyrics"
album: "90 ML"
artist: "Silambarasan"
lyricist: "Mirchi Vijay - STR"
director: "Anita Udeep"
path: "/albums/90-ml-lyrics"
song: "Marana Matta"
image: ../../images/albumart/90-ml.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nuq3qkIcLIA"
type: "happy"
singers:
  - Oviya
  - Silambarasan TR
  - Harish Kalyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Nirthu Nirthu Nirthu Nirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nirthu Nirthu Nirthu Nirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Naal Than New Year'ku Ithey Paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Naal Than New Year'ku Ithey Paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Pudhu Paatu Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pudhu Paatu Vehnum"/>
</div>
<div class="lyrico-lyrics-wrapper">New Year Night'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Year Night'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Aava Lamaa Tight'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aava Lamaa Tight'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniya Tamizh, Telugu, Malayalam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniya Tamizh, Telugu, Malayalam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannada, Hindi,English
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannada, Hindi,English"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Ulaga Makkalaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Ulaga Makkalaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Wish You Happy New Year
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wish You Happy New Year"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's Get Marana Mattaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's Get Marana Mattaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">New Year Night'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Year Night'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavalama Tight'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavalama Tight'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku Night'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku Night'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae Right'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae Right'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">New Year Night'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Year Night'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavalama Tight'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavalama Tight'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku Night'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku Night'u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae Right'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae Right'u"/>
</div>
<div class="lyrico-lyrics-wrapper">On The Rocks Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On The Rocks Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittha Ellam Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittha Ellam Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan Oda Paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan Oda Paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikume Weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikume Weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight'u Weight'u Weight'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight'u Weight'u Weight'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona Kaathula Manasu Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Kaathula Manasu Innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka Villai Villai Villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka Villai Villai Villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambala Kaasu Konjam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambala Kaasu Konjam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yera Villai Villai Villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yera Villai Villai Villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle'ah Thaan Thorakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle'ah Thaan Thorakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhasellam Marakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhasellam Marakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Varusham Namma Varusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Varusham Namma Varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rebirth'u Edukkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rebirth'u Edukkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Da Nadakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Da Nadakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmamaa Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmamaa Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukulla Varusham Odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukulla Varusham Odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Varusham Porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Varusham Porakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love'eh Vena'nu Sollu'vom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love'eh Vena'nu Sollu'vom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku Dum'eh Niruthu'vom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Dum'eh Niruthu'vom"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadiyum Pona Varusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadiyum Pona Varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaddhu Than Pannuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaddhu Than Pannuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Avalo Thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Avalo Thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta Matta Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta Matta Matta Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Before Sarakku Paatu Mudijirichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Before Sarakku Paatu Mudijirichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku After Sarakku Paattu Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku After Sarakku Paattu Vehnum"/>
</div>
</pre>
