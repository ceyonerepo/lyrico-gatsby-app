---
title: "pasappukkalli song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Vivek"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Pasappukkalli"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dxe9OyoyCJM"
type: "love"
singers:
  - Nivas K. Prasanna
  - Vijay Antony
  - Alex Samuel Jenito
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Achchu Asalu Vennilaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchu Asalu Vennilaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyila Paathathundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyila Paathathundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha Pasum Pullu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha Pasum Pullu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradiyil Valarnthathundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradiyil Valarnthathundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illennu Solla Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illennu Solla Mudiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Paaththa Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Paaththa Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Solla Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Solla Mudiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Paaththa Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Paaththa Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Solla Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Solla Mudiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pasappukkalli En Pasappukkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pasappukkalli En Pasappukkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thandhupputtaa Ivan Manasa Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thandhupputtaa Ivan Manasa Alli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Erukkanjulli En Erukkanjulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Erukkanjulli En Erukkanjulli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Paththa Vachaa Ivan Nenappa Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Paththa Vachaa Ivan Nenappa Killi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pasappukkalli En Pasappukkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pasappukkalli En Pasappukkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thandhupputtaa Ivan Manasa Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thandhupputtaa Ivan Manasa Alli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Erukkanjulli En Erukkanjulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Erukkanjulli En Erukkanjulli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Paththa Vachaa Ivan Nenappa Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Paththa Vachaa Ivan Nenappa Killi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keelanelli Kannu Nenjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keelanelli Kannu Nenjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keelaththalli Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keelaththalli Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaaamalli Vaasam Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaaamalli Vaasam Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Solli Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Solli Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nesamalli En Nesamalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nesamalli En Nesamalli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paasavilli En Paasavilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paasavilli En Paasavilli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nesamalli En Nesamalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nesamalli En Nesamalli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paasavilli En Paasavilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paasavilli En Paasavilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nongu Kondai Pinnalagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nongu Kondai Pinnalagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundi Vachi Solattuthappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundi Vachi Solattuthappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adengappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adengappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvelani Munnalagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvelani Munnalagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondi Aada Vaikkuthappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondi Aada Vaikkuthappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikkuthappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkuthappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Panamaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Panamaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennamaramaa Theriyaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennamaramaa Theriyaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Naanum Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Naanum Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Puriyaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Puriyaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Naanum Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Naanum Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Puriyaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Puriyaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aasa Rossaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasa Rossaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Rossaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Rossaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Aanen Loosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aanen Loosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen Loosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Loosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ponen Thoosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ponen Thoosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen Thoosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen Thoosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Odanjen Peesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Odanjen Peesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanjen Peesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odanjen Peesaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aasa Rossaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasa Rossaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen Loosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Loosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen Thoosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen Thoosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanjen Peesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odanjen Peesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosaa Rosaa Rosaa Rosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosaa Rosaa Rosaa Rosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ro Ro Ro Ro Ro Ro Roo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ro Ro Ro Ro Ro Ro Roo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulla Pootta Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Pootta Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechchikkuven Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechchikkuven Vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnodaiyaa Nizhala Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnodaiyaa Nizhala Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachchikkuven Naandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachchikkuven Naandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onn Kosuvaththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn Kosuvaththula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mayangurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mayangurendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kosuvaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kosuvaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Mulikkurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Mulikkurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onn Kosuvaththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn Kosuvaththula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mayangurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mayangurendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kosuvaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kosuvaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Mulikkurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Mulikkurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aasa Madhuve Nee Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasa Madhuve Nee Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Paatha Piragu Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paatha Piragu Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aanen Paaru Thean Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aanen Paaru Thean Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vilagi Pona Veen Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vilagi Pona Veen Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththu Parkkum Kannu Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu Parkkum Kannu Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Vella Gundumalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Vella Gundumalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Kaathu Enna Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Kaathu Enna Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthadaa Pera Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthadaa Pera Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Pogum Varthai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Pogum Varthai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkippodum Kodukaapuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkippodum Kodukaapuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Pogum Paarvai Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Pogum Paarvai Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththa Vaikkum Kirukaa Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththa Vaikkum Kirukaa Thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Vecha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vecha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vattamittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vattamittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Suttu Thalli Poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Suttu Thalli Poguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaviyaa Thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaviyaa Thavichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Maiyiruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Maiyiruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Machcha Mottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Machcha Mottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottu Killi Ooduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottu Killi Ooduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaa Sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaa Sirichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaveli Oththavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaveli Oththavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa Vali Pona Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa Vali Pona Vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum Kili Kollum Uli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Kili Kollum Uli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Muli Kannakkuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Muli Kannakkuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Retta Suzhi Pinju Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Suzhi Pinju Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Kili Anbu Thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Kili Anbu Thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solli Yethu Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solli Yethu Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Piththukkuli Polamburendiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththukkuli Polamburendiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Salamburendiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salamburendiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappaaala Vaanathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappaaala Vaanathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelamburen Naaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelamburen Naaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Di Di Di Di Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Di Di Di Di Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasappukkalli Pasappukkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasappukkalli Pasappukkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasappukkalli Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasappukkalli Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasappukkalli Pasappukkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasappukkalli Pasappukkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasappukkalli Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasappukkalli Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pasappukkalli En Pasappukkalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pasappukkalli En Pasappukkalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Aadiputta Oru Aadupuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Aadiputta Oru Aadupuli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Yerukkanjuli En Yerukkanjuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Yerukkanjuli En Yerukkanjuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Maattikkiten Oru Veettu Eli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Maattikkiten Oru Veettu Eli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye"/>
</div>
</pre>
