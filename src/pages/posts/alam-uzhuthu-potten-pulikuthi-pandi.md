---
title: "alam uzhuthu potten song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Araikudi Bharathi Ganeshan"
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Alam Uzhuthu Potten"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iYXETilGmlA"
type: "Motivational"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">alam uzhuthu potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alam uzhuthu potten"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu uramum nalla potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu uramum nalla potten"/>
</div>
<div class="lyrico-lyrics-wrapper">alam uzhuthu potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alam uzhuthu potten"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu uramum nalla potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu uramum nalla potten"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathula paruva mala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathula paruva mala"/>
</div>
<div class="lyrico-lyrics-wrapper">peiyavum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyavum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathula paruva mala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathula paruva mala"/>
</div>
<div class="lyrico-lyrics-wrapper">peiyavum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyavum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kastam paathu kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kastam paathu kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">kuda kanna thorakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuda kanna thorakala"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kastam paathu kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kastam paathu kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">kuda kanna thorakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuda kanna thorakala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alam uzhuthu potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alam uzhuthu potten"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu uramum nalla potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu uramum nalla potten"/>
</div>
<div class="lyrico-lyrics-wrapper">alam uzhuthu potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alam uzhuthu potten"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu uramum nalla potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu uramum nalla potten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onnuku onnu kadana vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnuku onnu kadana vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnuku onnu kadana vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnuku onnu kadana vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhuva rendu maadu vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhuva rendu maadu vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnuku onnu kadana vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnuku onnu kadana vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhuva rendu maadu vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhuva rendu maadu vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">vatti panam katta kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatti panam katta kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nellu velaiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nellu velaiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">vatti panam katta kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatti panam katta kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nellu velaiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nellu velaiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">enga shati muti samanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga shati muti samanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam vatti kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam vatti kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">enga shati muti samanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga shati muti samanam "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam vatti kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam vatti kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">arasanga loan vanga aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasanga loan vanga aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">maasam alanju paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasam alanju paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">arasanga loan vanga aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasanga loan vanga aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">maasam alanju paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasam alanju paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">appava ippava nu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appava ippava nu "/>
</div>
<div class="lyrico-lyrics-wrapper">alaya vittanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaya vittanga"/>
</div>
<div class="lyrico-lyrics-wrapper">appava ippava nu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appava ippava nu "/>
</div>
<div class="lyrico-lyrics-wrapper">alaya vittanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaya vittanga"/>
</div>
<div class="lyrico-lyrics-wrapper">en manavari boomiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manavari boomiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaya vittanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaya vittanga"/>
</div>
<div class="lyrico-lyrics-wrapper">en manavari boomiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manavari boomiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaya vittanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaya vittanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">milaga paluthuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milaga paluthuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">milaga paluthuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milaga paluthuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ven paruthi veduchuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ven paruthi veduchuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">milaga paluthuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milaga paluthuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ven paruthi veduchuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ven paruthi veduchuruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathuvatti kaaran vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathuvatti kaaran vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">veeta thaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeta thaturan"/>
</div>
<div class="lyrico-lyrics-wrapper">kathuvatti kaaran vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathuvatti kaaran vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">veeta thaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeta thaturan"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kadan patta vivasayinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kadan patta vivasayinga"/>
</div>
<div class="lyrico-lyrics-wrapper">thookula thonguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookula thonguran"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kadan patta vivasayinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kadan patta vivasayinga"/>
</div>
<div class="lyrico-lyrics-wrapper">thookula thonguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookula thonguran"/>
</div>
<div class="lyrico-lyrics-wrapper">kanniyam thavarama nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanniyam thavarama nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">vendiya kaaval thuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendiya kaaval thuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanniyam thavarama nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanniyam thavarama nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">vendiya kaaval thuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendiya kaaval thuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanthuvatti kaarana than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanthuvatti kaarana than"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka pidikuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka pidikuran"/>
</div>
<div class="lyrico-lyrics-wrapper">kanthuvatti kaarana than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanthuvatti kaarana than"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka pidikuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka pidikuran"/>
</div>
<div class="lyrico-lyrics-wrapper">inga andra kaatchi makkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga andra kaatchi makkal"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneer vadikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneer vadikiran"/>
</div>
</pre>
