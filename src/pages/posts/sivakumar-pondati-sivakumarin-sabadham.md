---
title: "sivakumar pondati song lyrics"
album: "Sivakumarin Sabadham"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "Hiphop Tamizha"
path: "/albums/sivakumarin-sabadham-lyrics"
song: "Sivakumar Pondati"
image: ../../images/albumart/sivakumarin-sabadham.jpg
date: 2021-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AWESYHH6kPI"
type: "happy"
singers:
  - Gana Michael
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ahaan Ho Hoo Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaan Ho Hoo Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Pattani Moonji Odi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattani Moonji Odi Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhumakapuka Rapukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhumakapuka Rapukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhumakapuka Rapukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhumakapuka Rapukutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhumakapuka Rapukutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhumakapuka Rapukutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinakku Thathakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Thathakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Thathakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Thathakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Thathakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Thathakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Thathakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Thathakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peteru Aaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peteru Aaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkira Sirikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira Sirikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Majava Irukkudha Irukkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majava Irukkudha Irukkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarukkuran Sarukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarukkuran Sarukkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Side Duka Sarukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Side Duka Sarukkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkira Sirikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira Sirikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Majava Irukkudh Irukkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majava Irukkudh Irukkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkuran Parakkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkuran Parakkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Flight Ah Pol Parakkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Flight Ah Pol Parakkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan Pili Pili Pili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Pili Pili Pili "/>
</div>
<div class="lyrico-lyrics-wrapper">Pili Pili Pili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pili Pili Pili"/>
</div>
<div class="lyrico-lyrics-wrapper">Adho Paaru Pachchai Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho Paaru Pachchai Kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Gili Gili Gili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Gili Gili Gili "/>
</div>
<div class="lyrico-lyrics-wrapper">Gili Gili Gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili Gili Gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Machanukku Pudcha Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machanukku Pudcha Kili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan Pili Pili Pili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Pili Pili Pili "/>
</div>
<div class="lyrico-lyrics-wrapper">Pili Pili Pili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pili Pili Pili"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarda Avan Side La Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarda Avan Side La Vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Gili Gili Gili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Gili Gili Gili "/>
</div>
<div class="lyrico-lyrics-wrapper">Gili Gili Gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili Gili Gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Tu Vutta Vaiya Kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Tu Vutta Vaiya Kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achocho Thuppaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achocho Thuppaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakumar Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakumar Pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Achocho Thuppaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achocho Thuppaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakumar Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakumar Pondati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urala Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urala Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerla Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerla Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallu Oru Juice Eh Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallu Oru Juice Eh Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Jula Kavalai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Jula Kavalai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi Benchula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi Benchula "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavundhu Kedappen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavundhu Kedappen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyila Canteen Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyila Canteen Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Mayila Pudippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayila Pudippen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyila Ezhundhuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyila Ezhundhuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann Muzhuchi Coffee Kudippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann Muzhuchi Coffee Kudippen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aproma Busula Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aproma Busula Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Football Adippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Football Adippen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pullainga Kaaga Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pullainga Kaaga Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Kuduppom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Kuduppom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi Perachanainu Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Perachanainu Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Sangula Midhippom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Sangula Midhippom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Runga Munnadi Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Runga Munnadi Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene Ah Iruppom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Ah Iruppom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahna En Figure Mattum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahna En Figure Mattum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sightu Adippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sightu Adippen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tightu Kuduppen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tightu Kuduppen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Fightu Kuduppen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightu Kuduppen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Davlathaga Suththunaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davlathaga Suththunaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti Othaippen Da Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti Othaippen Da Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Quite Ah Iruppen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quite Ah Iruppen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Weightaah Iruppen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightaah Iruppen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">En Figure Ru Ennai Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Figure Ru Ennai Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lightaah Sirippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lightaah Sirippen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu Thiguthigu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu Thiguthigu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu Thiguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu Thiguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhrigidaku Thakkdhina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrigidaku Thakkdhina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu Thiguthigu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu Thiguthigu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu Thiguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu Thiguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achocho Thuppaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achocho Thuppaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakumar Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakumar Pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">Achocho Thuppaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achocho Thuppaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakumar Pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakumar Pondati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urala Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urala Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerla Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerla Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Achakka Achakka Achakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Achakka Achakka Achakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Achakka Achakka Achakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achakka Achakka Achakka"/>
</div>
</pre>
