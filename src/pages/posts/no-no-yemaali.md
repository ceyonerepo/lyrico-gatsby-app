---
title: "no no song lyrics"
album: "Yemaali"
artist: "Sam D. Raj"
lyricist: "V.Z. Durai"
director: "V.Z. Durai"
path: "/albums/yemaali-lyrics"
song: "No No"
image: ../../images/albumart/yemaali.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TnorMeBP04w"
type: "happy"
singers:
  - V.Z. Durai
  - Cliffy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kena Poona Pola Enna Aakiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kena Poona Pola Enna Aakiputaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallaa Senjaale Vachi Senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Senjaale Vachi Senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena Pona Ponna Nambi Mokai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Pona Ponna Nambi Mokai Aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovunu Sonnaale Dash Adichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovunu Sonnaale Dash Adichaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No No No No No Namaku Girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No Namaku Girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No So And So Dating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No So And So Dating"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maama Miss Pannalaamaa Sokkaa Oru Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Miss Pannalaamaa Sokkaa Oru Party"/>
</div>
<div class="lyrico-lyrics-wrapper">It'S B R E A K Up Break Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It'S B R E A K Up Break Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No No No No No Namaku Girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No Namaku Girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No So And So Dating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No So And So Dating"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fb Illa Twitter Illa Whatsapp Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fb Illa Twitter Illa Whatsapp Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Block'Udhaan Melu Melu Feelu Melu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Block'Udhaan Melu Melu Feelu Melu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reelu Reelu Girls'Udhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reelu Reelu Girls'Udhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mike'U Illaamale Kathi Kadupethuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mike'U Illaamale Kathi Kadupethuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Extra Fitting'Ula Vayasa Kurachiduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Extra Fitting'Ula Vayasa Kurachiduvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongadi Neengalum Unga Bongu Kadhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongadi Neengalum Unga Bongu Kadhalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No No No No No Namaku Girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No Namaku Girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No So And So Dating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No So And So Dating"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalu Mela Kaalu Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Mela Kaalu Potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Night'U Full'Ah Torcher Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night'U Full'Ah Torcher Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rules Mela Rules'U Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rules Mela Rules'U Potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosu Aakum Lovsu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosu Aakum Lovsu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matter Illaamale Meter Potuduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter Illaamale Meter Potuduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Warning Illaamale Aala Maathiduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Warning Illaamale Aala Maathiduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Waranty Guaranty Unadhu Lovil Ketkaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waranty Guaranty Unadhu Lovil Ketkaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No No No No No Namaku Girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No Namaku Girlfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No So And So Dating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No So And So Dating"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maama Miss Pannalaamaa Sokkaa Oru Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Miss Pannalaamaa Sokkaa Oru Party"/>
</div>
<div class="lyrico-lyrics-wrapper">It'S B R E A K Up Break Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It'S B R E A K Up Break Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kena Poona Pola Enna Aakiputaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kena Poona Pola Enna Aakiputaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallaa Senjaale Vachi Senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Senjaale Vachi Senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena Pona Ponna Nambi Mokai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Pona Ponna Nambi Mokai Aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovunu Sonnaale Dash Adichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovunu Sonnaale Dash Adichaale"/>
</div>
</pre>
