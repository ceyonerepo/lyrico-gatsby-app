---
title: "yah allah song lyrics"
album: "FIR"
artist: "Ashwath"
lyricist: "Mashook Rahman"
director: "Manu Anand"
path: "/albums/fir-lyrics"
song: "Yah Allah"
image: ../../images/albumart/fir.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hNjBWTwkj1g"
type: "happy"
singers:
  - Deepak Blue
  - Vishnu Vishal
  - Sugandh Shekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thodarpadu idar tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarpadu idar tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adar irul sirayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adar irul sirayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudar thara yengidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudar thara yengidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pori naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pori naano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaigalin irulinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigalin irulinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthirgalai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirgalai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalai kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiravan naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiravan naano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai thaandi irulilae meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai thaandi irulilae meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irupathu indru thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupathu indru thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvarai thandha moochaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai thandha moochaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru izhakiren siraiyil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru izhakiren siraiyil naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu sadhigalin ulaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sadhigalin ulaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu silandhiyin valaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu silandhiyin valaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiranu udaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiranu udaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kanthaga siraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kanthaga siraiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En udhirathai pirithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udhirathai pirithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil amilathai vidhaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil amilathai vidhaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">En siragugal erithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En siragugal erithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu nyaayam thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu nyaayam thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagai seigiraan sirai seigiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai seigiraan sirai seigiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhai seigiraan yah allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhai seigiraan yah allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal seigiraan nijam poigiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal seigiraan nijam poigiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadham seigiraan yah allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadham seigiraan yah allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karam yenthiyae siram saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam yenthiyae siram saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aram vellumo yah allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram vellumo yah allah"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam yenthiyae siram saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam yenthiyae siram saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan naatam ini yah allah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan naatam ini yah allah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodarpadu idar tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarpadu idar tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adar irul sirayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adar irul sirayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudar thara yengidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudar thara yengidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pori naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pori naano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidaigalin irulinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigalin irulinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthirgalai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirgalai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalai kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiravan naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiravan naano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu yugangalin pagaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu yugangalin pagaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thagithidum veriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thagithidum veriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Emai karuvaruthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emai karuvaruthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam korthidum padaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam korthidum padaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manithathai udaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manithathai udaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai mathathirkul adaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai mathathirkul adaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhesiyam parithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhesiyam parithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu nyaayam thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu nyaayam thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugamadhai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugamadhai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agamadhai verukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agamadhai verukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidiyilae indru naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiyilae indru naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharavugal seidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharavugal seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarugal indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarugal indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerpai sollum naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerpai sollum naal"/>
</div>
</pre>
