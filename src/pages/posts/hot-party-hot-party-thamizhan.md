---
title: "hot party hot party song lyrics"
album: "Thamizhan"
artist: "D. Imman"
lyricist: "Vaali"
director: "Majith"
path: "/albums/thamizhan-lyrics"
song: "Hot Party Hot Party"
image: ../../images/albumart/thamizhan.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vGr4g6PVhpI"
type: "love"
singers:
  - Tippu
  - Mathangi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Look Shimla Ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Look Shimla Ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vella Yedhu Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vella Yedhu Potti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Look Shimla Ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Look Shimla Ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vella Yedhu Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vella Yedhu Potti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Oorai Kollum Anthrax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Oorai Kollum Anthrax"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannil Irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Irukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandaal En Udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandaal En Udambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andraadam Erakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam Erakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lappu Dappu Endrudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lappu Dappu Endrudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungsu Moochu Vaangi Nindru Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungsu Moochu Vaangi Nindru Tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Look Shimla Ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Look Shimla Ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vella Yedhu Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vella Yedhu Potti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeahu Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeahu Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boohambam Endru Varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boohambam Endru Varudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Unnai Paarthaal Nitham Varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Unnai Paarthaal Nitham Varudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Attraction Unn Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Attraction Unn Mel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vaithen Enn Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vaithen Enn Mel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Engum Undhan Sticker
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Engum Undhan Sticker"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otti Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Irukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenjai Pilandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjai Pilandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Unmai Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Unmai Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Avasthaikku Mudivenna Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Avasthaikku Mudivenna Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Look Shimla Ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Look Shimla Ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veetin Internetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetin Internetil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veetin Internetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetin Internetil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Website Undhan Peril Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Website Undhan Peril Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Seivoma Chatting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Seivoma Chatting"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyey Sollu Dating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyey Sollu Dating"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Reiki Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Reiki Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sootta Kilappuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sootta Kilappuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Temperature Paarkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Temperature Paarkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thermometer Therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thermometer Therikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Jurathukku Marundhu Ennammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Jurathukku Marundhu Ennammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Look Shimla Ooty Ee Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Look Shimla Ooty Ee Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Vella Yedhu Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vella Yedhu Potti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Party Hot Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Party Hot Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Look Shimla Ooty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Look Shimla Ooty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">World Beauty World Beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Beauty World Beauty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vella Yedhu Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vella Yedhu Potti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Oorai Kollum Anthrax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Oorai Kollum Anthrax"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannil Irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Irukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandaal En Udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandaal En Udambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andraadam Erakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam Erakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lappu Dappu Endrudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lappu Dappu Endrudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lungsu Moochu Vaangi Nindru Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungsu Moochu Vaangi Nindru Tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
</pre>
