---
title: 'come on girls song lyrics'
album: '3'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Soundharya Rajinikanth'
path: '/albums/3-song-lyrics'
song: 'Come on Girls'
image: ../../images/albumart/3.jpg
date: 2012-03-30
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/ptB_6oJ3vGY'
type: 'pub'
singers: 
- Anirudh Ravichander
- Nadisha Thomas
- Maalavika Manoj
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Life-ula wife-u vandhuta
<input type="checkbox" class="lyrico-select-lyric-line" value="Life-ula wife-u vandhuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight-adhaan irukanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Tight-adhaan irukanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight-aana ponna parthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Weight-aana ponna parthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-aadhaan nadakanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Right-aadhaan nadakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veetuku friends-alaam vandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Veetuku friends-alaam vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Guest-adhaan nadathanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Guest-adhaan nadathanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmela thappillatiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmela thappillatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silent-ah irukkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Silent-ah irukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">La la la la la la la…la
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la…la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la la ..common girls
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la ..common girls"/>
</div>
<div class="lyrico-lyrics-wrapper">s  La la la la la la la.. .la
<input type="checkbox" class="lyrico-select-lyric-line" value="s  La la la la la la la.. .la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la la
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy enga daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy enga daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">La la la la la la la…la
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la…la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Onnoda onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnoda onnu serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendaaga aayaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Rendaaga aayaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend ippa girl friend aachu..uu
<input type="checkbox" class="lyrico-select-lyric-line" value="Friend ippa girl friend aachu..uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanoda ponnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanoda ponnu serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Couple-ah aayaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Couple-ah aayaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Titanic kappal aachu..
<input type="checkbox" class="lyrico-select-lyric-line" value="Titanic kappal aachu.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Janani nee kanmani
<input type="checkbox" class="lyrico-select-lyric-line" value="Janani nee kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">En uryir neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="En uryir neeyadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Bore adikaama ne aadudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Bore adikaama ne aadudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">La la la la la la la…la
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la…la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la la ..common girls
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la ..common girls"/>
</div>
<div class="lyrico-lyrics-wrapper">s  La la la la la la la.. .la
<input type="checkbox" class="lyrico-select-lyric-line" value="s  La la la la la la la.. .la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la la
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la la la la la"/>
</div>
  <div class="lyrico-lyrics-wrapper">Common girls..
<input type="checkbox" class="lyrico-select-lyric-line" value="Common girls.."/>
</div>
</pre>