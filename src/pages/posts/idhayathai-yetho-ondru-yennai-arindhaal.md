---
title: "idhayathai yetho ondru song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Idhayathai Yetho Ondru"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JpmYuSJhlYY"
type: "Love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayathai Yedho Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Yedho Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhukkuthu Konjam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkuthu Konjam Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Idhu Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Idhu Pole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal alai pole vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal alai pole vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaigalai allum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigalai allum ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhugida manathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhugida manathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pin vaangavillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin vaangavillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruppathu oru manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppathu oru manathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhuvarai athu enathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuvarai athu enathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu methuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu methuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu poga kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu poga kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu oru kanavu nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu oru kanavu nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaithida virumbavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaithida virumbavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavukkul kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukkul kanavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai naane kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai naane kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkenna venum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkenna venum indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vaarthai kelu nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaarthai kelu nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini neeyum naanum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neeyum naanum ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena sollum naalum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena sollum naalum endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkenna venum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkenna venum indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vaarthai kelu nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaarthai kelu nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini neeyum naanum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neeyum naanum ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena sollum naalum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena sollum naalum endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargalai alli vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargalai alli vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magizhvudan kaiyil thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhvudan kaiyil thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathinai pagirndhidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathinai pagirndhidave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai kolgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai kolgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaduppathu enna endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppathu enna endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkuthu nenjam indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkuthu nenjam indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyinil ilai ena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyinil ilai ena naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thointhu selgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thointhu selgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumbugal poovaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbugal poovaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya maatram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram aandaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram aandaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagiya thotram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya thotram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru velli kolusu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velli kolusu pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha manasu sinungum keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manasu sinungum keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaatha vairam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaatha vairam pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu naanam minungum mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu naanam minungum mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru velli kolusu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velli kolusu pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha manasu sinungum keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manasu sinungum keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaatha vairam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaatha vairam pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu naanam minungum mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu naanam minungum mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathai Yedho Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Yedho Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhukkuthu Konjam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkuthu Konjam Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Idhu Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Idhu Pole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal alai pole vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal alai pole vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaigalai allum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigalai allum ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhugida manathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhugida manathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pin vaangavillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin vaangavillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruppathu oru manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppathu oru manathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhuvarai athu enathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuvarai athu enathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu methuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu methuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu poga kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu poga kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu oru kanavu nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu oru kanavu nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaithida virumbavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaithida virumbavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavukkul kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukkul kanavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai naane kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai naane kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru velli kolusu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velli kolusu pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha manasu sinungum keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manasu sinungum keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyaatha vairam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaatha vairam pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu naanam minungum mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu naanam minungum mela"/>
</div>
</pre>
