---
title: "yuvaraani song lyrics"
album: "Khyla"
artist: "Shravan"
lyricist: "Vadivarasu "
director: "Baskar Sinouvassane"
path: "/albums/khyla-lyrics"
song: "Yuvaraani"
image: ../../images/albumart/khyla.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n-Hzm6KDghw"
type: "happy"
singers:
  - Sarath Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yuvarani indha ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yuvarani indha ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">ival rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival rani"/>
</div>
<div class="lyrico-lyrics-wrapper">ival pesum mozhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival pesum mozhi "/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum navathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum navathani"/>
</div>
<div class="lyrico-lyrics-wrapper">nathi pola mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi pola mella"/>
</div>
<div class="lyrico-lyrics-wrapper">sirithidum ivan gnani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirithidum ivan gnani"/>
</div>
<div class="lyrico-lyrics-wrapper">madhi kooda azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhi kooda azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">thotridum maruthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotridum maruthani"/>
</div>
<div class="lyrico-lyrics-wrapper">viral asaivil vendriduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral asaivil vendriduval"/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai pola nindriduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai pola nindriduval"/>
</div>
<div class="lyrico-lyrics-wrapper">ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">men siripil veezhthiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="men siripil veezhthiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyinai pola viyathiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyinai pola viyathiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">valaradha annai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaradha annai "/>
</div>
<div class="lyrico-lyrics-wrapper">ival valarndha siru thennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival valarndha siru thennai"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaradha thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaradha thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">ival tharndhidaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival tharndhidaa "/>
</div>
<div class="lyrico-lyrics-wrapper">perum paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum paadal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katril kai pidithu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katril kai pidithu than"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanthiduval nadathiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanthiduval nadathiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil kuchi eduthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil kuchi eduthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">miratiduval midhandiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miratiduval midhandiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">bommaigal soozh ulagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bommaigal soozh ulagil"/>
</div>
<div class="lyrico-lyrics-wrapper">bommai pondra uyiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bommai pondra uyiri"/>
</div>
<div class="lyrico-lyrics-wrapper">ival anumathi ketu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival anumathi ketu than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha suriyan kooda udhikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha suriyan kooda udhikum"/>
</div>
<div class="lyrico-lyrics-wrapper">poimaigal nirai mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poimaigal nirai mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai naadum thirami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai naadum thirami"/>
</div>
<div class="lyrico-lyrics-wrapper">ival asaivinai parthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival asaivinai parthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vaanam kooda velukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vaanam kooda velukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bigilu bigilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bigilu bigilu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi agil mugili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi agil mugili"/>
</div>
<div class="lyrico-lyrics-wrapper">oodi naisa saisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi naisa saisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thulli thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">vidulu padalu paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidulu padalu paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vadalu vidalu naadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadalu vidalu naadi"/>
</div>
<div class="lyrico-lyrics-wrapper">alavaa milavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavaa milavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">alli alli koodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli alli koodi "/>
</div>
<div class="lyrico-lyrics-wrapper">samathithana kanavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samathithana kanavi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kaila voda peyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kaila voda peyari"/>
</div>
<div class="lyrico-lyrics-wrapper">pottiyinna muthali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottiyinna muthali"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu kaila voda pugazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu kaila voda pugazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">harry potter padithu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harry potter padithu than"/>
</div>
<div class="lyrico-lyrics-wrapper">sirithiduval silirthiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirithiduval silirthiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">googlukul pugundhu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="googlukul pugundhu than"/>
</div>
<div class="lyrico-lyrics-wrapper">asathiduval adhatiduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asathiduval adhatiduval"/>
</div>
<div class="lyrico-lyrics-wrapper">super kid ahi maaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="super kid ahi maaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">viththai arindha vithagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viththai arindha vithagi"/>
</div>
<div class="lyrico-lyrics-wrapper">ival nizhalinil nanaindhu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival nizhalinil nanaindhu than"/>
</div>
<div class="lyrico-lyrics-wrapper">indha malargal poothu kuthikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha malargal poothu kuthikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kurumbugal pala seidhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbugal pala seidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">greedam soodha thalaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="greedam soodha thalaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">ival kuralinai thirudi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival kuralinai thirudi than"/>
</div>
<div class="lyrico-lyrics-wrapper">endha kuyilum paadi kalikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha kuyilum paadi kalikum"/>
</div>
</pre>
