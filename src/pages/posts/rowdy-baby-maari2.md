---
title: 'rowdy baby song lyrics'
album: 'Maari 2'
artist: 'Yuvan Shankar Raja'
lyricist: 'Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari2-song-lyrics'
song: 'Rowdy Baby'
image: ../../images/albumart/Maari-2.jpg
date: 2018-12-21
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/x6Q7c9RyMzk'
type: 'duet'
singers: 
- Dhanush
- Dhee
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey en goli sodavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey en goli sodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kari kozhambae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kari kozhambae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kutty puppy naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kutty puppy naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Take me
<input type="checkbox" class="lyrico-select-lyric-line" value="Take me"/>
</div>
<div class="lyrico-lyrics-wrapper">Take me
<input type="checkbox" class="lyrico-select-lyric-line" value="Take me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey en silku satta
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey en silku satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee weightu katta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee weightu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Loveu sotta sotta
<input type="checkbox" class="lyrico-select-lyric-line" value="Loveu sotta sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Talk me
<input type="checkbox" class="lyrico-select-lyric-line" value="Talk me"/>
</div>
<div class="lyrico-lyrics-wrapper">Talk me
<input type="checkbox" class="lyrico-select-lyric-line" value="Talk me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey my dear machaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey my dear machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu vechaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee manasu vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma orasikkalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma orasikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju jigu jigu jaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenju jigu jigu jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey my dear raani
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey my dear raani"/>
</div>
<div class="lyrico-lyrics-wrapper">En dreamula vaa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="En dreamula vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma onna sera
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma onna sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire pathikkiruchaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Fire pathikkiruchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raa… namma beachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa… namma beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam pothaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkam pothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dappang kuthu vesthaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru dappang kuthu vesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennudaiya rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ennudaiya rowdy baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raa…you are my
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa…you are my"/>
</div>
<div class="lyrico-lyrics-wrapper">Only girlfriendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Only girlfriendu"/>
</div>
<div class="lyrico-lyrics-wrapper">I will give you poochendu
<input type="checkbox" class="lyrico-select-lyric-line" value="I will give you poochendu"/>
</div>
<div class="lyrico-lyrics-wrapper">We will make us new trendu baby
<input type="checkbox" class="lyrico-select-lyric-line" value="We will make us new trendu baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pothaam…
<input type="checkbox" class="lyrico-select-lyric-line" value="Pothaam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesthaam…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vesthaam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy baby…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Girlfriendu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Girlfriendu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Poochendu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Poochendu…"/>
</div>
<div class="lyrico-lyrics-wrapper">New trendu baby
<input type="checkbox" class="lyrico-select-lyric-line" value="New trendu baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra ra rowdy baby…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ra ra ra ra rowdy baby…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnala yei moodaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnala yei moodaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">My hormoneu balanceu damageu
<input type="checkbox" class="lyrico-select-lyric-line" value="My hormoneu balanceu damageu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yei kamaatchi en meenatchi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei kamaatchi en meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha maarikkum unmela kannaachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha maarikkum unmela kannaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">One plus onnu
<input type="checkbox" class="lyrico-select-lyric-line" value="One plus onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Two maamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Two maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You plus me three maamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="You plus me three maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaadi jhansi raani
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaadi jhansi raani"/>
</div>
<div class="lyrico-lyrics-wrapper">En krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line" value="En krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">I will buy you pony
<input type="checkbox" class="lyrico-select-lyric-line" value="I will buy you pony"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa ottinnu vaa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Aththa ottinnu vaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En mandhiravaadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="En mandhiravaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kedikku kedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kedikku kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnulla paadhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan unnulla paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma semma jodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma semma jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yei rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei rowdy baby"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy rowdy rowdy rowdy
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy rowdy rowdy rowdy"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaaan (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaaan"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey en goli sodavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey en goli sodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kari kozhambae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kari kozhambae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kutty puppy naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kutty puppy naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Take me
<input type="checkbox" class="lyrico-select-lyric-line" value="Take me"/>
</div>
<div class="lyrico-lyrics-wrapper">Take me
<input type="checkbox" class="lyrico-select-lyric-line" value="Take me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey en silku satta
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey en silku satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee weightu katta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee weightu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Loveu sotta sotta
<input type="checkbox" class="lyrico-select-lyric-line" value="Loveu sotta sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Talk me
<input type="checkbox" class="lyrico-select-lyric-line" value="Talk me"/>
</div>
<div class="lyrico-lyrics-wrapper">Talk me
<input type="checkbox" class="lyrico-select-lyric-line" value="Talk me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey my dear machaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey my dear machaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee manasu vechaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee manasu vechaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Namma orasikkalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma orasikkalaam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nenju jigu jigu jaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenju jigu jigu jaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey my dear raani
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey my dear raani"/>
</div>
<div class="lyrico-lyrics-wrapper">En dreamula vaa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="En dreamula vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma onna sera
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma onna sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire pathikkiruchaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Fire pathikkiruchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raa… namma beachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa… namma beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam pothaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkam pothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dappang kuthu vesthaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru dappang kuthu vesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennudaiya rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ennudaiya rowdy baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raa…you are my
<input type="checkbox" class="lyrico-select-lyric-line" value="Raa…you are my"/>
</div>
<div class="lyrico-lyrics-wrapper">Only girlfriendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Only girlfriendu"/>
</div>
<div class="lyrico-lyrics-wrapper">I will give you poochendu
<input type="checkbox" class="lyrico-select-lyric-line" value="I will give you poochendu"/>
</div>
<div class="lyrico-lyrics-wrapper">We will make us new trendu baby
<input type="checkbox" class="lyrico-select-lyric-line" value="We will make us new trendu baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Child  Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Child  Rowdy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaan (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaan"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hahaan hahaan (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahaan hahaan"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rowdy baby
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy baby"/>
</div>
</pre>