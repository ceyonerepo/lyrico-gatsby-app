---
title: "amma kutti amma kutti song lyrics"
album: "Jai Simha"
artist: "Chirantan Bhatt"
lyricist: "Bhaskarbhatla"
director: "K S Ravikumar"
path: "/albums/jai-simha-lyrics"
song: "Amma Kutti Amma Kutti"
image: ../../images/albumart/jai-simha.jpg
date: 2018-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rUWIZgMtZw0"
type: "love"
singers:
  - Jaspreet Jasz
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ammakutti Ammakutti Andamanthaa Vompakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammakutti Ammakutti Andamanthaa Vompakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aggipetti Guggipetti Aataloki Dhimpakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggipetti Guggipetti Aataloki Dhimpakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammakutti Ammakutti Andamanthaa Vompakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammakutti Ammakutti Andamanthaa Vompakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aggipetti Guggipetti Aataloki Dhimpakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggipetti Guggipetti Aataloki Dhimpakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Navvu Tandaa Gurthaina Raadu Yendaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Navvu Tandaa Gurthaina Raadu Yendaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Oogindi Jendaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Oogindi Jendaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkupetti Ekkupetti Kanti Choopu Dhinchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkupetti Ekkupetti Kanti Choopu Dhinchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Gukkapetti Gukkapetti Ukkapotha Penchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukkapetti Gukkapetti Ukkapotha Penchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkupetti Ekkupetti Kanti Choopu Dhinchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkupetti Ekkupetti Kanti Choopu Dhinchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Gukkapetti Gukkapetti Ukkapotha Penchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukkapetti Gukkapetti Ukkapotha Penchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvemo Dhandha Nenemo Rajineegandhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvemo Dhandha Nenemo Rajineegandhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nannu Aapedhi Undhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nannu Aapedhi Undhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Chaliga Undhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Chaliga Undhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilla Duppatilaa Kaapadanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilla Duppatilaa Kaapadanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Segalaa Undhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Segalaa Undhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaraa Muddhulatho Thadipeyanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaraa Muddhulatho Thadipeyanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Paddathiga Undutelaa Timmiriney Thattukuni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paddathiga Undutelaa Timmiriney Thattukuni "/>
</div>
<div class="lyrico-lyrics-wrapper">Andhukaney Undakalaa Chethulaney Kattukuni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhukaney Undakalaa Chethulaney Kattukuni "/>
</div>
<div class="lyrico-lyrics-wrapper">Aithey Alagaithey Meedha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithey Alagaithey Meedha "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyesi Cheseyyi Karantuney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyesi Cheseyyi Karantuney "/>
</div>
<div class="lyrico-lyrics-wrapper">Sarapharaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarapharaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammakutti Ammakutti Andamanthaa Vompakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammakutti Ammakutti Andamanthaa Vompakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aggipetti Guggipetti Aataloki Dhimpakey  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggipetti Guggipetti Aataloki Dhimpakey  "/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkupetti Ekkupetti Kanti Choopu Dhinchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkupetti Ekkupetti Kanti Choopu Dhinchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Gukkapetti Gukkapetti Ukkapotha Penchakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukkapetti Gukkapetti Ukkapotha Penchakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Navvu Tandaa Gurthaina Raadu Yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Navvu Tandaa Gurthaina Raadu Yendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Oogindi Jendaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Oogindi Jendaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baboi Bhayamestundey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baboi Bhayamestundey "/>
</div>
<div class="lyrico-lyrics-wrapper">Undoddhu Ontarigaa Daggarikochchey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undoddhu Ontarigaa Daggarikochchey "/>
</div>
<div class="lyrico-lyrics-wrapper">Baavoy Siggestundhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baavoy Siggestundhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepey Untadhiley Kalley Moosey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepey Untadhiley Kalley Moosey "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudilaa Ledhu Kadhaa Ippudilaa Endhukani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudilaa Ledhu Kadhaa Ippudilaa Endhukani "/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthakani Untadhiley Vayasu Thaley Dhinchukunee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthakani Untadhiley Vayasu Thaley Dhinchukunee "/>
</div>
<div class="lyrico-lyrics-wrapper">Aunaa Aunavna Padhaa Eerojey Theercheddham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunaa Aunavna Padhaa Eerojey Theercheddham "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayassulo Gara Garaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayassulo Gara Garaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammakutti Ammakutti Andamanthaa Vompakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammakutti Ammakutti Andamanthaa Vompakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Aggipetti Guggipetti Aataloki Dhimpakey  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggipetti Guggipetti Aataloki Dhimpakey  "/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkupetti Ekkupetti Kanti Choopu Dhinchakey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkupetti Ekkupetti Kanti Choopu Dhinchakey "/>
</div>
<div class="lyrico-lyrics-wrapper">Gukkapetti Gukkapetti Ukkapotha Penchakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukkapetti Gukkapetti Ukkapotha Penchakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Navvu Tandaa Gurthaina Raadu Yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Navvu Tandaa Gurthaina Raadu Yendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Oogindi Jendaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Oogindi Jendaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama "/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Jajjanakaa Ma Ma Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Jajjanakaa Ma Ma Mama Mama"/>
</div>
</pre>
