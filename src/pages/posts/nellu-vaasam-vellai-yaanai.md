---
title: "nellu vaasam song lyrics"
album: "Vellai Yaanai"
artist: "Santhosh Narayanan"
lyricist: "Rajumurugan"
director: "Subramaniam Siva"
path: "/albums/vellai-yaanai-lyrics"
song: "Nellu Vaasam"
image: ../../images/albumart/vellai-yaanai.jpg
date: 2021-07-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6O0FnJ89jKM"
type: "happy"
singers:
  - Santhosh Narayanan
  - Shreenitha
  - Dharshan B M
  - Sidharth D Sekar
  - Idhazhiga I
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannulathan Kanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulathan Kanneeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulathan Kannerum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulathan Kannerum Theva Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannulathan Illadha Edhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannulathan Illadha Edhum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallidu Un Sorva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallidu Un Sorva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnidum Nam Verva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnidum Nam Verva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana Naala Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana Naala Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sogamellam Moodathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sogamellam Moodathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyyoda Pathu Veral Moolathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyoda Pathu Veral Moolathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottura Nam Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottura Nam Naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthura Mann Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura Mann Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum Vazhva Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum Vazhva Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil Yeridum Yeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Yeridum Yeril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Maaridum Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Maaridum Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerum Illamai Thaane Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerum Illamai Thaane Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seril Vazhndhidum Kaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seril Vazhndhidum Kaalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhum Oorpasi Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum Oorpasi Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarum Ellame Thaane Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum Ellame Thaane Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Pesidum Naatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Pesidum Naatrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Nam Vidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Nam Vidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatrum Kaakum Dheivangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrum Kaakum Dheivangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann Thaane Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Thaane Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilam Yendrana Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Yendrana Thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Un Maarbil Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Un Maarbil Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulai Thandhaye Em Pasi Theerave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulai Thandhaye Em Pasi Theerave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uram Endragum Saanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uram Endragum Saanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Nellagum Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Nellagum Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam Thandhaye Em Vidhi Maarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Thandhaye Em Vidhi Maarave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil Yeridum Yeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Yeridum Yeril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha Maaridum Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha Maaridum Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil Yeridum Yeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Yeridum Yeril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulathan Kannerum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulathan Kannerum Theva Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannulathan Illadha Edhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannulathan Illadha Edhum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallidu Un Sorva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallidu Un Sorva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnidum Nam Verva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnidum Nam Verva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana Naala Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana Naala Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sogamellam Moodathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sogamellam Moodathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyyoda Pathu Veral Moolathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyoda Pathu Veral Moolathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottura Nam Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottura Nam Naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthura Mann Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura Mann Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum Vazhva Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum Vazhva Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setril Pootha Panbadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setril Pootha Panbadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaathan Kaatha Mann Maedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaathan Kaatha Mann Maedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seevan Intha Natrodu Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seevan Intha Natrodu Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerum Sorum Mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerum Sorum Mannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathi Antham Mannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi Antham Mannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Intha Mannodu Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Intha Mannodu Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Nilamena Poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Nilamena Poothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimuraigalai Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimuraigalai Kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravena Unnai Aatum Em Nilame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravena Unnai Aatum Em Nilame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyil Mazhaigalai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Mazhaigalai Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathirena Thalai Saaythu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirena Thalai Saaythu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulaginuk Uyir Ootum Em Nilame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginuk Uyir Ootum Em Nilame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulathan Kannerum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulathan Kannerum Theva Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannulathan Illadha Edhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannulathan Illadha Edhum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallidu Un Sorva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallidu Un Sorva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnidum Nam Verva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnidum Nam Verva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana Naala Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana Naala Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sogamellam Moodathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sogamellam Moodathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyyoda Pathu Veral Moolathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyoda Pathu Veral Moolathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottura Nam Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottura Nam Naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthura Mann Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura Mann Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum Vazhva Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum Vazhva Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulathan Kannerum Theva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulathan Kannerum Theva Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannulathan Illadha Edhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannulathan Illadha Edhum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallidu Un Sorva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallidu Un Sorva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnidum Nam Verva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnidum Nam Verva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana Naala Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana Naala Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sogamellam Moodathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sogamellam Moodathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyyoda Pathu Veral Moolathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyyoda Pathu Veral Moolathanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottura Nam Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottura Nam Naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthura Mann Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura Mann Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum Vazhva Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum Vazhva Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjulathan Pacha Kuthum Nellu Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjulathan Pacha Kuthum Nellu Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooo Nenjulathan Pacha Kuthum Nellu Vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Nenjulathan Pacha Kuthum Nellu Vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooo Nenjulathan Pacha Kuthum Nellu Vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Nenjulathan Pacha Kuthum Nellu Vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjulathan Pacha Kuthum Nellu Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjulathan Pacha Kuthum Nellu Vaasam"/>
</div>
</pre>
