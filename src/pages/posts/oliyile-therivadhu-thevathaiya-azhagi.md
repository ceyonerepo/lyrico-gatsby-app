---
title: "oliyile therivadhu thevathaiya song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Oliyile Therivadhu Thevathaiya"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zo82B_-HPRA"
type: "love"
singers:
  - Karthik
  - Bhavatharini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Dhevadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Dhevadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirile Kalandhadhu Nee Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirile Kalandhadhu Nee Illaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nesama Nesam Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nesama Nesam Illaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenavukku Theriyalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenavukku Theriyalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavile Nadakkutha Kangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavile Nadakkutha Kangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangiratha Kangiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangiratha Kangiratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Dhevadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhaiya Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiya Dhevadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Manasukku Velangavillye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Manasukku Velangavillye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadapathu Ennenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadapathu Ennenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Enniyum Puriyavillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enniyum Puriyavillaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthadhu Ennenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthadhu Ennenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovil Maniya Yaaru Adikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovil Maniya Yaaru Adikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Velakka Yaaru Ethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Velakka Yaaru Ethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Pothum Anaiyama Nindru Oliranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pothum Anaiyama Nindru Oliranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Nee Ilaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Nee Ilaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ilaiya Nee Ilaiyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilaiya Nee Ilaiyaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Pudhiyathor Ponnu Silaionnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Pudhiyathor Ponnu Silaionnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkudhu Manjalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkudhu Manjalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poova Pola Orr Chinna Meniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Pola Orr Chinna Meniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhadhu Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhadhu Poovukkulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ariya Vayasu Kelvi Ezhuppudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariya Vayasu Kelvi Ezhuppudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadantha Theriyum Ezhuthi Vachadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadantha Theriyum Ezhuthi Vachadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthiyathai Padichalum Edhuvum Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthiyathai Padichalum Edhuvum Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Nee Ilaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Nee Ilaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirile Kalandhadhu Nee Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirile Kalandhadhu Nee Illaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nesama Nesam Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nesama Nesam Illaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenavukku Theriyalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenavukku Theriyalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavile Nadakkutha Kangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavile Nadakkutha Kangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangiratha Kangirathaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangiratha Kangirathaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyile Therivadhu Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyile Therivadhu Dhevadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhaiya Dhevadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiya Dhevadhaiya"/>
</div>
</pre>
