---
title: "neeli neeli song lyrics"
album: "30 Rojullo Preminchadam Ela"
artist: "Anup Rubens"
lyricist: "Chandrabose"
director: "Dhulipudi Phani Pradeep"
path: "/albums/30-rojullo-preminchadam-ela-lyrics"
song: "Neeli Neeli Aakasham"
image: ../../images/albumart/30-rojullo-preminchadam-ela.jpg
date: 2021-01-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XjJTtKTbR84"
type: "love"
singers:
  - Sid Sriram
  - Sunitha Upadrashta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeli Neeli Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Neeli Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddamanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddamanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulu Ninne Kammesthayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulu Ninne Kammesthayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Manesthoo Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manesthoo Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavankanu Iddamanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavankanu Iddamanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oho Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oho Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuku Saripodhantunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuku Saripodhantunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve Nadicheti Theeruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Nadicheti Theeruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaralu Molichaayi Nelake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaralu Molichaayi Nelake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Vadhileti Swaasake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Vadhileti Swaasake"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalulu Brathikaayi Choodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalulu Brathikaayi Choodave"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Goppa Andhagatthekemi Ivvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Goppa Andhagatthekemi Ivvane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeli Neeli Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Neeli Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddamanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddamanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulu Ninne Kammesthayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulu Ninne Kammesthayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Manesthoo Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manesthoo Vunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Vaanavillulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Vaanavillulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Undani Rangu Nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undani Rangu Nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Rangula Cheeranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Rangula Cheeranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Neyyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Neyyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Mabbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Mabbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Merise Kallu Neevile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise Kallu Neevile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kallaku Kaatuka Endhukettaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kallaku Kaatuka Endhukettaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chekkilipai Chukkagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekkilipai Chukkagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishte Pedathaarule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishte Pedathaarule"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekaithe Thanuvanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekaithe Thanuvanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chukkanu Pettaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chukkanu Pettaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Ivvaali Kaanuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Ivvaali Kaanuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho Vethikaanu Aasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho Vethikaanu Aasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edi Nee Saati Raadhika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edi Nee Saati Raadhika"/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo Odaanu Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo Odaanu Poorthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke Praanamantha Thaali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke Praanamantha Thaali "/>
</div>
<div class="lyrico-lyrics-wrapper">Chesi Neeku Kattanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesi Neeku Kattanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeli Neeli Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Neeli Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddamanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddamanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Hrudhayam Mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hrudhayam Mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasam Chinnadhi Antunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasam Chinnadhi Antunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Amma Choopulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Amma Choopulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Olike Jaali Nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olike Jaali Nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Jaaliki Maaruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Jaaliki Maaruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Ivvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Ivvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanna Velitho Nadipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanna Velitho Nadipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhairyame Neede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhairyame Neede"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paapanai Pasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paapanai Pasi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paapanai Emi Ivvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapanai Emi Ivvale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaya Kaligina Devude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaya Kaligina Devude"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalanu Kalipaadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalanu Kalipaadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamosige Devudike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamosige Devudike"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenem Thirigivvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenem Thirigivvaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo Ivvaali Kaanuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Ivvaali Kaanuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho Vethikaanu Aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho Vethikaanu Aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Edi Nee Saati Raadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edi Nee Saati Raadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo Alishaanu Poorthiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo Alishaanu Poorthiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke Malli Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke Malli Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Janmanetthi Ninnu Cherana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janmanetthi Ninnu Cherana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeli Neeli Aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli Neeli Aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddhaamanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddhaamanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulu Ninne Kammesthayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulu Ninne Kammesthayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Manesthoo Vunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manesthoo Vunnaa"/>
</div>
</pre>
