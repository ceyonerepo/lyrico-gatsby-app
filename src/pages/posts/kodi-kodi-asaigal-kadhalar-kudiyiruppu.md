---
title: "kodi kodi asaigal song lyrics"
album: "Kadhalar Kudiyiruppu"
artist: "James Vasanthan"
lyricist: "Yugabharathi"
director: "AMR Ramesh"
path: "/albums/kadhalar-kudiyiruppu-lyrics"
song: "Kodi Kodi Asaigal"
image: ../../images/albumart/kadhalar-kudiyiruppu.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J0C0Lrt-jXY"
type: "love"
singers:
  - Sharath
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koadikkoadi aasaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koadikkoadi aasaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanum vaelaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanum vaelaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil azhagey arindheney naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil azhagey arindheney naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum edhuvum unaippoalavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum edhuvum unaippoalavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal thoongappoavadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal thoongappoavadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnaikkaanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnaikkaanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravo pagalo enakkedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravo pagalo enakkedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhen ulagil unnai cheravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhen ulagil unnai cheravey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhviley koadi minnal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhviley koadi minnal "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kandappinbu thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kandappinbu thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosinaai vannam ennil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosinaai vannam ennil "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillai poala naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillai poala naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalillaadha pattam poalavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalillaadha pattam poalavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum indru maaripponakkoalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum indru maaripponakkoalam "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyil naan mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyil naan mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">eeramaakkum aaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeramaakkum aaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koadikkoadi aasaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koadikkoadi aasaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanum vaelaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanum vaelaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil azhagey arindheney naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil azhagey arindheney naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum edhuvum unaippoalavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum edhuvum unaippoalavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan thevaiyillai kaalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan thevaiyillai kaalai "/>
</div>
<div class="lyrico-lyrics-wrapper">naeram neeyum vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naeram neeyum vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiri thaevai illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri thaevai illai "/>
</div>
<div class="lyrico-lyrics-wrapper">undhan maarbil saindhukkondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan maarbil saindhukkondaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oivillaadha boomi poalavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oivillaadha boomi poalavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarum unnai aasai sutra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarum unnai aasai sutra "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal koodum nenjiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal koodum nenjiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal thoondililey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal thoondililey "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum neendhavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum neendhavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal thoongappoavadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal thoongappoavadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnaikkaanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnaikkaanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravo pagalo enakkedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravo pagalo enakkedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhen ulagil unnai cheravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhen ulagil unnai cheravey"/>
</div>
</pre>
