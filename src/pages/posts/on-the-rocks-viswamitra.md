---
title: "on the rocks song lyrics"
album: "Viswamitra"
artist: "Anup Rubens"
lyricist: "Pranav Chaganti"
director: "Rajaa Kiran"
path: "/albums/viswamitra-lyrics"
song: "On The Rocks"
image: ../../images/albumart/viswamitra.jpg
date: 2019-06-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/a27hw0Yxn4k"
type: "happy"
singers:
  - Pranav Chaganti
  - Mounika Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">namasthe namasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namasthe namasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">randi randi andaru randi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="randi randi andaru randi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye teena meena leena angelina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye teena meena leena angelina "/>
</div>
<div class="lyrico-lyrics-wrapper">karenge gaana bhajana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karenge gaana bhajana"/>
</div>
<div class="lyrico-lyrics-wrapper">ye party shuru karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye party shuru karo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arey on the rocks pegge kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey on the rocks pegge kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">dance floor pai adugu pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance floor pai adugu pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">ladies ey join avutharu ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladies ey join avutharu ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">move your body move it now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="move your body move it now"/>
</div>
<div class="lyrico-lyrics-wrapper">mummy daddy ki phone kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mummy daddy ki phone kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">night letani cheepi pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night letani cheepi pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">pareshani pakkana pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pareshani pakkana pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">shake your body shake it now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shake your body shake it now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">welcome miss leena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="welcome miss leena"/>
</div>
<div class="lyrico-lyrics-wrapper">arey sunlo sunaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey sunlo sunaina"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam vanga meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam vanga meena"/>
</div>
<div class="lyrico-lyrics-wrapper">da da dance with me teena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="da da dance with me teena"/>
</div>
<div class="lyrico-lyrics-wrapper">arey chuttu antha angelsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey chuttu antha angelsey"/>
</div>
<div class="lyrico-lyrics-wrapper">malli radhu ee chancey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli radhu ee chancey"/>
</div>
<div class="lyrico-lyrics-wrapper">top lepey le dancey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="top lepey le dancey"/>
</div>
<div class="lyrico-lyrics-wrapper">party with me i say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party with me i say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa peru kanakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa peru kanakam"/>
</div>
<div class="lyrico-lyrics-wrapper">andariki welcome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andariki welcome"/>
</div>
<div class="lyrico-lyrics-wrapper">naa peru kanakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa peru kanakam"/>
</div>
<div class="lyrico-lyrics-wrapper">andariki welcome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andariki welcome"/>
</div>
<div class="lyrico-lyrics-wrapper">nenentho famoussu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenentho famoussu"/>
</div>
<div class="lyrico-lyrics-wrapper">oorantha naa fansu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorantha naa fansu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenokka steppey esthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenokka steppey esthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kurrallantha kallasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurrallantha kallasu"/>
</div>
<div class="lyrico-lyrics-wrapper">neneley princess
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neneley princess"/>
</div>
<div class="lyrico-lyrics-wrapper">queen of masses 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="queen of masses "/>
</div>
<div class="lyrico-lyrics-wrapper">osari kanne kodithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osari kanne kodithe"/>
</div>
<div class="lyrico-lyrics-wrapper">lokam motham riversuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokam motham riversuu"/>
</div>
<div class="lyrico-lyrics-wrapper">my name is kanakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my name is kanakam"/>
</div>
<div class="lyrico-lyrics-wrapper">my name is kanakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my name is kanakam"/>
</div>
<div class="lyrico-lyrics-wrapper">welcome welcome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="welcome welcome"/>
</div>
<div class="lyrico-lyrics-wrapper">welcome welcome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="welcome welcome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye angrezi beat oddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye angrezi beat oddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dappe muddhu under
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dappe muddhu under"/>
</div>
<div class="lyrico-lyrics-wrapper">world lo dappe muddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world lo dappe muddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">life la lolliki pub ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life la lolliki pub ey"/>
</div>
<div class="lyrico-lyrics-wrapper">mandhu come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandhu come on come on"/>
</div>
<div class="lyrico-lyrics-wrapper">lets pary party party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lets pary party party"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tam abb abb abb aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tam abb abb abb aa"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">anthe tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthe tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">naacho tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naacho tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">yesko tatara tatara tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesko tatara tatara tam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">night mottham chillu maro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mottham chillu maro"/>
</div>
<div class="lyrico-lyrics-wrapper">dillu petti naach yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dillu petti naach yaro"/>
</div>
<div class="lyrico-lyrics-wrapper">only today no tomorrow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="only today no tomorrow"/>
</div>
<div class="lyrico-lyrics-wrapper">move your body move it now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="move your body move it now"/>
</div>
<div class="lyrico-lyrics-wrapper">khaki chokka aithe enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khaki chokka aithe enti"/>
</div>
<div class="lyrico-lyrics-wrapper">salam kotti nikal jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salam kotti nikal jaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">sallani peggey raa bammardhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sallani peggey raa bammardhi"/>
</div>
<div class="lyrico-lyrics-wrapper">shake your bodt shake it now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shake your bodt shake it now"/>
</div>
<div class="lyrico-lyrics-wrapper">your are my laila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your are my laila"/>
</div>
<div class="lyrico-lyrics-wrapper">wah arey kirrak tere naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wah arey kirrak tere naina"/>
</div>
<div class="lyrico-lyrics-wrapper">abb abb arey nacho jane jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abb abb arey nacho jane jana"/>
</div>
<div class="lyrico-lyrics-wrapper">full hopes eh nee paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="full hopes eh nee paina"/>
</div>
<div class="lyrico-lyrics-wrapper">bar mottham inka kallasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bar mottham inka kallasu"/>
</div>
<div class="lyrico-lyrics-wrapper">aagamaitharu ika bouncersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagamaitharu ika bouncersu"/>
</div>
<div class="lyrico-lyrics-wrapper">dj babu jarra beatu marchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dj babu jarra beatu marchara"/>
</div>
<div class="lyrico-lyrics-wrapper">eeda andaru oora maassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeda andaru oora maassu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oy adhi kottu vareva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oy adhi kottu vareva"/>
</div>
<div class="lyrico-lyrics-wrapper">vunnadhoka lifura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vunnadhoka lifura "/>
</div>
<div class="lyrico-lyrics-wrapper">masthi cheyyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masthi cheyyara"/>
</div>
<div class="lyrico-lyrics-wrapper">badhalanni bayatapetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhalanni bayatapetti"/>
</div>
<div class="lyrico-lyrics-wrapper">chindheseyyara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chindheseyyara "/>
</div>
<div class="lyrico-lyrics-wrapper">ninna nedu radhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna nedu radhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nedu repu radhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedu repu radhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neekunna timene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekunna timene"/>
</div>
<div class="lyrico-lyrics-wrapper">enjoy cheyyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enjoy cheyyara"/>
</div>
<div class="lyrico-lyrics-wrapper">phone appu petti steppeyyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phone appu petti steppeyyara"/>
</div>
<div class="lyrico-lyrics-wrapper">mundhu sollu aapi chilled
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundhu sollu aapi chilled"/>
</div>
<div class="lyrico-lyrics-wrapper">peggeyyara mundu billu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peggeyyara mundu billu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhi bey fikarugundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhi bey fikarugundu"/>
</div>
<div class="lyrico-lyrics-wrapper">come on come on lets party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come on come on lets party"/>
</div>
<div class="lyrico-lyrics-wrapper">party party party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party party party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tam "/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tanku tattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tanku tattam"/>
</div>
<div class="lyrico-lyrics-wrapper">tatara tatara tam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatara tatara tam"/>
</div>
</pre>
