---
title: "tuck song song lyrics"
album: "Tuck Jagadish"
artist: "Gopi Sundar"
lyricist: "Shiva Nirvana"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Tuck Song"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FD4_wiWUImA"
type: "happy"
singers:
  - Shiva Nirvana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sallaati kunda loo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallaati kunda loo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sallaa sakkaa manasuvaaduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallaa sakkaa manasuvaaduuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu gilli gicchi recchegodite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gilli gicchi recchegodite"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchi dhanchutaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchi dhanchutaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Najooku navvulonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Najooku navvulonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jharukatthi padunu sooduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jharukatthi padunu sooduuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhummu dhuluputhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhummu dhuluputhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaraakunda kalla joduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaraakunda kalla joduuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theetekki takku jolikocchinavooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theetekki takku jolikocchinavooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarra sarrekki dokka dolu simphuthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarra sarrekki dokka dolu simphuthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Khirrekki kaallu thagi sindhuletthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khirrekki kaallu thagi sindhuletthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku booretti bhattaletti pamphuthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku booretti bhattaletti pamphuthadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttameedura nuvvu sakkagundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttameedura nuvvu sakkagundara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikka rhegithe rada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikka rhegithe rada"/>
</div>
<div class="lyrico-lyrics-wrapper">Raada raada sivudu saamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raada raada sivudu saamila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sallaati kunda loo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallaati kunda loo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sallaa sakkaa manasuvaaduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallaa sakkaa manasuvaaduuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu gilli gicchi recchegodite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gilli gicchi recchegodite"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchi dhanchutaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchi dhanchutaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Najooku navvulonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Najooku navvulonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jharukatthi padunu sooduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jharukatthi padunu sooduuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhummu dhuluputhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhummu dhuluputhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaraakunda kalla joduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaraakunda kalla joduuu"/>
</div>
</pre>
