---
title: "kavalayodum song lyrics"
album: "Thimiru Pudichavan"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Ganeshaa"
path: "/albums/thimiru-pudichavan-lyrics"
song: "Kavalayodum"
image: ../../images/albumart/thimiru-pudichavan.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z8O5uKgUIFg"
type: "melody"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kavalaiyodum kuzhappathodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyodum kuzhappathodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En manathil nooru vazhigalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manathil nooru vazhigalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithum naalai mudiyum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithum naalai mudiyum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thadikki vizhuntha kaayathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thadikki vizhuntha kaayathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridam solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara ini naan nambuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara ini naan nambuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul mattum kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul mattum kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithaal kolluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithaal kolluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam indri thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam indri thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Seitha thavarai ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seitha thavarai ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pasithum unavai verukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pasithum unavai verukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkiren naan thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiren naan thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappathai enni thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappathai enni thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkullae naan azhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkullae naan azhugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga veliyilae mattum nadikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga veliyilae mattum nadikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaiyodum kuzhappathodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyodum kuzhappathodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En manathil nooru vazhigalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manathil nooru vazhigalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril kanneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril kanneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodum enn odam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodum enn odam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai ennaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae poraattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae poraattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakka mudiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakka mudiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanga theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandukkum naduvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandukkum naduvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam vilangavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam vilangavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethavum sariyumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethavum sariyumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkullae thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkullae thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennai vathaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennai vathaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Veithu sithaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veithu sithaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam indri thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam indri thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Seitha thavarai ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seitha thavarai ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pasithum unavai verukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pasithum unavai verukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkiren naan thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiren naan thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappathai enni thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappathai enni thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkullae naan azhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkullae naan azhugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga veliyilae mattum nadikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga veliyilae mattum nadikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaiyodum kuzhappathodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyodum kuzhappathodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En manathil nooru vazhigalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manathil nooru vazhigalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithum naalai mudiyum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithum naalai mudiyum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thadikki vizhuntha kaayathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thadikki vizhuntha kaayathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridam solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara ini naan nambuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara ini naan nambuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul mattum kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul mattum kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithal kolluven nnnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithal kolluven nnnn"/>
</div>
</pre>
