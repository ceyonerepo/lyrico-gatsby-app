---
title: "otha parvayil song lyrics"
album: "Vennila Kabaddi Kuzhu 2"
artist: "V. Selvaganesh"
lyricist: "Vijayasagar"
director: "Selva Sekaran"
path: "/albums/vennila-kabaddi-kuzhu-2-lyrics"
song: "Otha Parvayil"
image: ../../images/albumart/vennila-kabaddi-kuzhu-2.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eQf1tcEVetE"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Otha Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viddha Kaattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viddha Kaattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavuthittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavuthittiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Kuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vizhunthittanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vizhunthittanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo Manasathaan Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo Manasathaan Kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathan Pannuven Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathan Pannuven Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichae Muraichae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichae Muraichae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viddha Kaattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viddha Kaattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavuthittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavuthittiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhigalil Vazhuki Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigalil Vazhuki Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhadhum Thaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhadhum Thaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mugavari Ninaivinil Marandhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mugavari Ninaivinil Marandhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Edhazhgalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Edhazhgalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkira Oligalai Veenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkira Oligalai Veenai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soramena Thirigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soramena Thirigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Madurai-ah Aatipadaikirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai-ah Aatipadaikirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagara Kudhiraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagara Kudhiraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Parakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichae Muraichae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichae Muraichae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viddha Kaattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viddha Kaattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavuthittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavuthittiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Kuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vizhunthittanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vizhunthittanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iduppula Nadathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iduppula Nadathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulukkalil Thodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulukkalil Thodhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Adikkadi Kalandhuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Adikkadi Kalandhuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sirippula Iraikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sirippula Iraikira"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhiya Soodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhiya Soodai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Porikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Porikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhirshtatha Pakkanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhirshtatha Pakkanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththudha Ranga Raatinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththudha Ranga Raatinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Naan Seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Naan Seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Seekiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Seekiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichae Muraichae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichae Muraichae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkan Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viddha Kaattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viddha Kaattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavuthittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavuthittiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjum Sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum Sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Kuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vizhunthittanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vizhunthittanae"/>
</div>
</pre>
