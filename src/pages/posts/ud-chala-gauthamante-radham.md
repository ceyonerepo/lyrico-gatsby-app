---
title: "ud chala song lyrics"
album: "Gauthamante Radham"
artist: "Ankit Menon - Anuraj O. B"
lyricist: "Sushant Sudhakaran"
director: "Anand Menon"
path: "/albums/gauthamante-radham-lyrics"
song: "Ud Chala"
image: ../../images/albumart/gauthamante-radham.jpg
date: 2020-01-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/Q-m2Qhd7dCk"
type: "happy"
singers:
  - Ankit Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">shukhar hai safar meh asar yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shukhar hai safar meh asar yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">nikhar ke hai bah raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikhar ke hai bah raha"/>
</div>
<div class="lyrico-lyrics-wrapper">yeh keh raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh keh raha"/>
</div>
<div class="lyrico-lyrics-wrapper">kahaani par yeh vo safeene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kahaani par yeh vo safeene"/>
</div>
<div class="lyrico-lyrics-wrapper">sunane hei iss tharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunane hei iss tharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">bina wajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bina wajah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">main kuch bhi chaahtha nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kuch bhi chaahtha nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">dua main maangta nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dua main maangta nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">hawaao ko yu thaamke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hawaao ko yu thaamke"/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala"/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jo iss pal hai hal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jo iss pal hai hal chal"/>
</div>
<div class="lyrico-lyrics-wrapper">wahi kal vo salsal tu mil sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wahi kal vo salsal tu mil sara"/>
</div>
<div class="lyrico-lyrics-wrapper">de dil sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="de dil sara"/>
</div>
<div class="lyrico-lyrics-wrapper">meh tujhme khulu aur tu meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meh tujhme khulu aur tu meri"/>
</div>
<div class="lyrico-lyrics-wrapper">ragho main aa gul sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragho main aa gul sara"/>
</div>
<div class="lyrico-lyrics-wrapper">bina wajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bina wajah"/>
</div>
<div class="lyrico-lyrics-wrapper">main tujhes poochta nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main tujhes poochta nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">paheli poochta nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paheli poochta nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hawaao ko bas thaamke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hawaao ko bas thaamke "/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala"/>
</div>
<div class="lyrico-lyrics-wrapper">oooh main ud chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooh main ud chala"/>
</div>
<div class="lyrico-lyrics-wrapper">ooh bas ud chala aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh bas ud chala aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">main ud chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main ud chala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mmmmmm hhhhhmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmmmm hhhhhmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">mmmmmm hhhhhmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmmmm hhhhhmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">mmmmmm hhhhhmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmmmm hhhhhmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">mmmmmm hhhhhmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmmmm hhhhhmmmmm"/>
</div>
</pre>
