---
title: "kaalangathale song lyrics"
album: "Venghai"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/venghai-lyrics"
song: "Kaalangathale"
image: ../../images/albumart/venghai.jpg
date: 2011-07-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o7rXVEK2om8"
type: "love"
singers:
  - 	Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei Kaalangathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Kaalangathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjirukum Vennilaa Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjirukum Vennilaa Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Olinjikiriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Olinjikiriyae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hm Hmm Hm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hm Hmm Hm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Kaalangathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Kaalangathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjirukum Vennilaa Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjirukum Vennilaa Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Olinjikiriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Olinjikiriyae Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pozhudhu Saanjaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Saanjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Guniyum Thaamara Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Guniyum Thaamara Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Vetkapaduriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Vetkapaduriyae Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naan Paarthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Paarthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Rasithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rasithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thindaadinaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thindaadinaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naan Thodarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Thodarndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnardhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnardhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaadhal Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vazhkayin Vaasalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhkayin Vaasalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Kaalangathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Kaalangathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjirukum Vennilaa Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjirukum Vennilaa Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Olinjikiriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Olinjikiriyae Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hallalaayi Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hallalaayi Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hallalaayi Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hallalaayi Lae Lae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhattai Suzhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattai Suzhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril Vedi Vaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril Vedi Vaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum Pozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum Pozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edharkku Podi Vaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharkku Podi Vaikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollu Bommai Pol Irukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollu Bommai Pol Irukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kodi Mullai Pol Nadakkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kodi Mullai Pol Nadakkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikadi Nagam Kadikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Nagam Kadikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Mayakki Maayam Seidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mayakki Maayam Seidhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Rathiri Paarthidum Vaanavil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rathiri Paarthidum Vaanavil Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjirukum Vennilaa Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjirukum Vennilaa Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Olinjikiriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Olinjikiriyae Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhasai Maraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhasai Maraikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikum Unnaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikum Unnaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikka Varavillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikka Varavillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvam Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puruvam Viriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puruvam Viriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruvan Naanillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruvan Naanillayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edharku Nee Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edharku Nee Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavirkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavirkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Edhirilae Mugam Sivakkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Edhirilae Mugam Sivakkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agamellam Poi Poosiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agamellam Poi Poosiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Arugil Serkka Maruthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Arugil Serkka Maruthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aaviyai Thaakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aaviyai Thaakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyae Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyae Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranjirukum Vennilaa Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranjirukum Vennilaa Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Olinjikiriyae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Olinjikiriyae Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pozhudhu Saanjaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Saanjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Guniyum Thaamara Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Guniyum Thaamara Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Paathaalae Vetkapaduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paathaalae Vetkapaduriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hae Haepennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Haepennae"/>
</div>
</pre>
