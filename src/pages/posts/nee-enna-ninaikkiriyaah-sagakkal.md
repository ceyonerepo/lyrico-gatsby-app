---
title: "nee enna ninaikkiriyaah song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Yugabharathi"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Nee Enna Ninaikkiriyaah"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Ranjith
  - Srimathumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">O vattamidum munnazhagu vambuseiyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O vattamidum munnazhagu vambuseiyudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">thangividu ennudaney endru solludhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangividu ennudaney endru solludhey"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli irundhum kollugindradhey kollai azhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli irundhum kollugindradhey kollai azhagey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan ketkum paadal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketkum paadal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaaney neeye thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaaney neeye thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">un pechiley naan ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pechiley naan ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sangeedha saaralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangeedha saaralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan vaangum moochukkaatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaangum moochukkaatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaaney neeye thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaaney neeye thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">un kangalai naan paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangalai naan paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">dheiveega thedalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheiveega thedalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marubadiyum mannil pirandhadhupoal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadiyum mannil pirandhadhupoal"/>
</div>
<div class="lyrico-lyrics-wrapper">ularugiren sinnakuzhandhaiyai poal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ularugiren sinnakuzhandhaiyai poal"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyil sirippadhum thavithida ninaippadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyil sirippadhum thavithida ninaippadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">edharkkena theriyaadha ennavendru puriyaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edharkkena theriyaadha ennavendru puriyaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee soodum poovukkellaam vaasangal koodacheidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee soodum poovukkellaam vaasangal koodacheidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaasanai en meesaiyil neengaamal neelavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaasanai en meesaiyil neengaamal neelavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeppoadum aadaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeppoadum aadaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ennai noolaai neiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ennai noolaai neiven"/>
</div>
<div class="lyrico-lyrics-wrapper">un aadaiyil en meniyum moodaamal poadumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aadaiyil en meniyum moodaamal poadumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhadugalaal unnai nagaleduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadugalaal unnai nagaleduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">uravugalaal thottu alangarippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravugalaal thottu alangarippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravenna pagalenna ini endha thuyarenna nerukkathiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravenna pagalenna ini endha thuyarenna nerukkathiley"/>
</div>
<div class="lyrico-lyrics-wrapper">hO vaa urakkaththai marappoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO vaa urakkaththai marappoamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna nenaikkiriyaa poraiyerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nenaikkiriyaa poraiyerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai nenaikkaiyila thadumaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai nenaikkaiyila thadumaarudhu"/>
</div>
</pre>
