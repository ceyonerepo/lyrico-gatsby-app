---
title: "adugadugo action hero song lyrics"
album: "Ruler"
artist: "Chirantan Bhatt"
lyricist: "Ramajogayya Sastry"
director: "K S Ravikumar"
path: "/albums/ruler-lyrics"
song: "Adugadugo Action Hero"
image: ../../images/albumart/ruler.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yO-buCmun4U"
type: "happy"
singers:
  - Saicharan Bhaskaruni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adugadugo Action Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugadugo Action Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Dekho Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Dekho Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugadugu Thanadem Pero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugadugu Thanadem Pero"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Thanadem Ooro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Thanadem Ooro"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulalo Adi Yem Fire O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulalo Adi Yem Fire O"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Salute Chey Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Salute Chey Ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Koduthu Seeti Maaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Koduthu Seeti Maaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie Le Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Le Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">King Of The Jungle La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Of The Jungle La"/>
</div>
<div class="lyrico-lyrics-wrapper">Angry Avenger La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angry Avenger La"/>
</div>
<div class="lyrico-lyrics-wrapper">Return Of The Dragon La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Return Of The Dragon La"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchadhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchadhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopullone Veedu Classu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopullone Veedu Classu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Bc Center Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Bc Center Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka White Collar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka White Collar"/>
</div>
<div class="lyrico-lyrics-wrapper">Corporate Leaderu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corporate Leaderu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahanaale Eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahanaale Eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Vasthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Osthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Osthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikothha Charithalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikothha Charithalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahanaale Eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahanaale Eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Vasthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Osthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Osthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikothha Charithalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikothha Charithalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushisthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokaale Thirigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaale Thirigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Ehulloki Yedhigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ehulloki Yedhigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Puttina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Puttina"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattini Vadaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattini Vadaladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nelabaludu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nelabaludu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Rajyaa Elina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Rajyaa Elina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Shikharaale Shaasinchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Shikharaale Shaasinchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Janmichina Thalli Ki Eppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janmichina Thalli Ki Eppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">O Chanti Paapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Chanti Paapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Maatalo Gunavanthudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Maatalo Gunavanthudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Baata Lo Thalavanchadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Baata Lo Thalavanchadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pratiaatalo Prati Vetalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pratiaatalo Prati Vetalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Upper Hand Veedide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upper Hand Veedide"/>
</div>
<div class="lyrico-lyrics-wrapper">Success Sound Veedide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Success Sound Veedide"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahanaale Eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahanaale Eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Vasthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Osthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Osthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikothha Charithalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikothha Charithalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushisthaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Aa Glamouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Aa Glamouru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Handsomeness Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Handsomeness Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Grammaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grammaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Jara Chupinchaado Teaseru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara Chupinchaado Teaseru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Chupu Thipparu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Chupu Thipparu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammailu Yevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammailu Yevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Company Isthe Vadalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Company Isthe Vadalaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Thappadu Kada Ee Dangeru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Thappadu Kada Ee Dangeru "/>
</div>
<div class="lyrico-lyrics-wrapper">Marchaali Numberu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchaali Numberu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaalake Saradaa Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaalake Saradaa Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradaa Ante Asalaagadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaa Ante Asalaagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaalalo Shruti Minchadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaalalo Shruti Minchadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun Time Krishnudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun Time Krishnudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Time Raamudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Time Raamudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahanaale Eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahanaale Eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Vasthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Osthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Osthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikothha Charithalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikothha Charithalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Mande Suryudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Mande Suryudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Grahanaale Eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahanaale Eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Vasthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Vasthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Osthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Osthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikothha Charithalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikothha Charithalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushisthaadu"/>
</div>
</pre>
