---
title: "yennai thalaathum female song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "P. Vijay"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Yennai Thalaathum female"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DlyLDCuJABo"
type: "sad"
singers:
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaaa aah aaaa aah aaaa aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aah aaaa aah aaaa aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aah aaaa aah aaaa aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aah aaaa aah aaaa aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmm mmm mmmm mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmm mmm mmmm mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thalaatum sangetham nee allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thalaatum sangetham nee allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai seeratum ponoonjal naan allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai seeratum ponoonjal naan allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai mazhai enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mazhai enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai thee enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai thee enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha aagayam nizham kaatru nee enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha aagayam nizham kaatru nee enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan enbathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thalaatum sangetham nee allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thalaatum sangetham nee allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai seeratum ponoonjal naan allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai seeratum ponoonjal naan allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo oooh oooo ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo oooh oooo ooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathiyaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathiyaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaalae naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalae naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irukum dhooram varai karai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irukum dhooram varai karai aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irukum neram varai uyir vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irukum neram varai uyir vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal naal en manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal naal en manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhaiyai nee irunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiyai nee irunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru naal paarkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru naal paarkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanamaai maari vitaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanamaai maari vitaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi thudipodu nadamadi nee vaazhgirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi thudipodu nadamadi nee vaazhgirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil nee vaazhgirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil nee vaazhgirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thalaatum sangetham nee allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thalaatum sangetham nee allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai seeratum ponoonjal naan allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai seeratum ponoonjal naan allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na na na naa na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na na na naa na na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boologam oar naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologam oar naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrindri ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrindri ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan uyir unthan moochu kaatraagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan uyir unthan moochu kaatraagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam oar naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam oar naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan jeevan unthan kaiyil vilakaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan jeevan unthan kaiyil vilakaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae naan irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae naan irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai kaagithamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai kaagithamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil nee vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil nee vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum oviyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum oviyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepam nee endraal athil naanae thiriyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepam nee endraal athil naanae thiriyaagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam thiriyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam thiriyaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thalaatum sangetham nee allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thalaatum sangetham nee allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai seeratum ponoonjal naan allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai seeratum ponoonjal naan allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai mazhai enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mazhai enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai thee enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai thee enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha aagayam nizham kaatru nee enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha aagayam nizham kaatru nee enbathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan enbathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan enbathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thalaatum sangetham nee allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thalaatum sangetham nee allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai seeratum ponoonjal naan allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai seeratum ponoonjal naan allavaa"/>
</div>
</pre>
