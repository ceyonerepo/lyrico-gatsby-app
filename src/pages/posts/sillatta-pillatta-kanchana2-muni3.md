---
title: "sillatta pillatta song lyrics"
album: "Kanchana2-Muni3"
artist: "S. Thaman - Leon James - C. Sathya"
lyricist: "Logan"
director: "Raghava Lawrence"
path: "/albums/kanchana2-muni3-lyrics"
song: "Sillatta Pillatta"
image: ../../images/albumart/kanchana2-muni3.jpg
date: 2015-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YL7BFAt6WfA"
type: "Mass"
singers:
  - Jagadeesh Kumar
  - C. Sathya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Everybody Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Everybody Hello"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raghava Lawrence Is Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghava Lawrence Is Back"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adrasakkai Muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adrasakkai Muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillatta Pillatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillatta Pillatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Pannum Galatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Pannum Galatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urtaadha Mertaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaadha Mertaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Night La Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night La Vandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vertaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vertaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrunnum Durrunnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrunnum Durrunnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Vandha Girrunum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Vandha Girrunum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achacho Ichaccho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achacho Ichaccho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peingala Puchaacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peingala Puchaacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alardhu Olarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alardhu Olarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thogardhu Nagardhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thogardhu Nagardhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaangu Idaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaangu Idaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baimaakkeedhu Kiyamaakeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baimaakkeedhu Kiyamaakeedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angaakeedhu Ingaakeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaakeedhu Ingaakeedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engakeedhu Ullakeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engakeedhu Ullakeedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satnnukeedhu Pattnukeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satnnukeedhu Pattnukeedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatnukeedhu Otnukeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatnukeedhu Otnukeedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Arsalu Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Arsalu Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyavutta Ersalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyavutta Ersalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koundhadichi Paduthukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koundhadichi Paduthukkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Varaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundivittaa Sillara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundivittaa Sillara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perlavittaa Kallara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perlavittaa Kallara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathikinu Pothikadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathikinu Pothikadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Varaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Vyaasarpaadi Kaasima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Vyaasarpaadi Kaasima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Uruvampodhu Paathumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Uruvampodhu Paathumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Veetta Nallaa Pootumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veetta Nallaa Pootumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Photo Photo Photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Photo Photo Photo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangi Maatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Maatumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arusalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyayyayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayyayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyayyayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayyayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arusalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyayyayoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayyayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo Aiyo Aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Aiyo Aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Arsalu Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Arsalu Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyavutta Ersalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyavutta Ersalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koundhadichi Paduthukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koundhadichi Paduthukkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Varaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Padam Paakkapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Padam Paakkapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Vendikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Vendikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koindhagellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koindhagellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayappadapodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayappadapodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Moodikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Pei Indha Pei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Pei Indha Pei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bulba Kaattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulba Kaattudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Pei Nightu Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Pei Nightu Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viththa Kaatudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa Kaatudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeeyo Betta Pei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeyo Betta Pei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayandhuttaaru Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhuttaaru Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga Inga Oondhu Endhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Inga Oondhu Endhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkudhadaa Thaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkudhadaa Thaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akka Bukka Sikkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Bukka Sikkaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Mela Okkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Mela Okkaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nightu Neram Thoongumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightu Neram Thoongumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannuvan Naa Makkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuvan Naa Makkaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allah Jesus Karumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allah Jesus Karumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Vandhu Nikkudhu Urumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Vandhu Nikkudhu Urumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyoo Iruttaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyoo Iruttaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarumaniyaana Muni Kaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumaniyaana Muni Kaatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethuvudaadha Kothuvudaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuvudaadha Kothuvudaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peikitta Enna Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peikitta Enna Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maativudaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maativudaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Neram Thaarumaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Neram Thaarumaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanda Poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Poduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathirila Uchaa Pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathirila Uchaa Pova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammava Koopuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammava Koopuduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padukkumbodhu Thodappam Seruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkumbodhu Thodappam Seruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu Vechukkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Vechukkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Kanavu Mertti Edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Kanavu Mertti Edukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaava Puchukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaava Puchukuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachamoocha Pei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachamoocha Pei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaikkuthunga Naai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikkuthunga Naai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dargaavula Maativuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dargaavula Maativuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olinjikichi Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinjikichi Thaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peigalellaam Mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peigalellaam Mosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattaadhunga Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaadhunga Paasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erinchipona Kovathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erinchipona Kovathellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Mela Kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Mela Kaatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evano Oruthan Saavadipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evano Oruthan Saavadipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Nammala Pudchinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Nammala Pudchinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaliyaruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliyaruppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayatha Kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayatha Kekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalillayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei Kovatha Koraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei Kovatha Koraikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morillaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethi Vudaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethi Vudaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooki Vudaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Vudaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhula Pondhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhula Pondhula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Kaatti Maativudaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kaatti Maativudaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arsalu Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arsalu Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arsalu Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arsalu Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Arsalu Orsalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Arsalu Orsalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyavutta Ersalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyavutta Ersalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koundhadichi Paduthukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koundhadichi Paduthukkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Varaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundivittaa Sillara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundivittaa Sillara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perlavittaa Kallara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perlavittaa Kallara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathikinu Pothikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathikinu Pothikkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni Varaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vyaasarpaadi Kaasimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyaasarpaadi Kaasimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Uruvampodhu Paathumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Uruvampodhu Paathumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Veetta Nallaa Pootumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veetta Nallaa Pootumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Photo Photo Photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Photo Photo Photo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangi Maatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Maatumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Photo Photo Photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Photo Photo Photo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangi Maatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Maatumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silpaa Sillaatti Sevulkaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpaa Sillaatti Sevulkaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apdinaa Seekiram Veetukku Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdinaa Seekiram Veetukku Poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serungadaa Nu Artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serungadaa Nu Artham"/>
</div>
</pre>
