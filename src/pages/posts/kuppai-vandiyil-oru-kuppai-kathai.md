---
title: "kuppai vandiyil song lyrics"
album: "Oru Kuppai Kathai"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Kaali Rangasamy"
path: "/albums/oru-kuppai-kathai-lyrics"
song: "Kuppai Vandiyil"
image: ../../images/albumart/oru-kuppai-kathai.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TOPoUPQ7G5Y"
type: "melody"
singers:
  - Saicharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuppai vandiyil selgirom payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppai vandiyil selgirom payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Allum kaigalil illaiyae salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allum kaigalil illaiyae salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai orangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai orangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyum kuppai koolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyum kuppai koolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal ullangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal ullangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththam oorai athu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththam oorai athu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom naangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada adada adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada adada adada"/>
</div>
<div class="lyrico-lyrics-wrapper">Drainage danger paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drainage danger paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakkadayil sewage alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkadayil sewage alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai tharuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai tharuvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha boomiyin kuppai manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomiyin kuppai manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul innum allalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul innum allalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan podum plastic kuppaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan podum plastic kuppaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha boomi thaangalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomi thaangalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everestil garbage-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everestil garbage-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkai aaguthae damage-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkai aaguthae damage-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukkazhiva kootturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukkazhiva kootturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha ketta thitturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha ketta thitturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagara nachukkaatrinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagara nachukkaatrinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha ozone thiraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha ozone thiraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhichukatta paarkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhichukatta paarkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppai vandiyil selgirom payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppai vandiyil selgirom payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Allum kaigalil illaiyae salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allum kaigalil illaiyae salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai orangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai orangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyum kuppai koolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyum kuppai koolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal ullangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal ullangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththam oorai athu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththam oorai athu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom naangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada adada adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada adada adada"/>
</div>
<div class="lyrico-lyrics-wrapper">Drainage danger paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drainage danger paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakkadayil sewage alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkadayil sewage alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai tharuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai tharuvomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha boomiyin kuppai manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomiyin kuppai manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul innum allalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul innum allalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan podum plastic kuppaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan podum plastic kuppaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha boomi thaangalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomi thaangalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangalaiyae thaangalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangalaiyae thaangalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana nanananaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nanananaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana naana naa nana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naana naa nana naanaa"/>
</div>
</pre>
