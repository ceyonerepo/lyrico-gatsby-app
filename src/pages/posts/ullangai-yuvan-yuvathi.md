---
title: "ullangai song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Ullangai"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/slgi6g2KFEQ"
type: "love"
singers:
  - Sangeetha Rajeshwaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullangaiyai Udharumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyai Udharumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Vilagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Vilagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Kaalam Kadandhu Pogumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Kaalam Kadandhu Pogumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Tholaiyumo Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Tholaiyumo Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vizhi Thiraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhi Thiraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Uruvam Azhiyaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uruvam Azhiyaadhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niththiraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niththiraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavai Kalaikkaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavai Kalaikkaadhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaaladiyil Nee Kidandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaaladiyil Nee Kidandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Sendrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Sendrene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Dhoorathai Naan Kadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Dhoorathai Naan Kadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedugindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedugindrene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangaiyai Udharumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyai Udharumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Vilagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Vilagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Kaalam Kadandhu Pogumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Kaalam Kadandhu Pogumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Tholaiyumo Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Tholaiyumo Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vizhi Thiraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhi Thiraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Uruvam Azhiyaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uruvam Azhiyaadhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niththiraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niththiraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavai Kalaikkaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavai Kalaikkaadhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaaladiyil Nee Kidandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaaladiyil Nee Kidandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Sendrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Sendrene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Dhoorathai Naan Kadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Dhoorathai Naan Kadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedugindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedugindrene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangaiyai Udharumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyai Udharumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Vilagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Vilagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Kaalam Kadandhu Pogumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Kaalam Kadandhu Pogumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Tholaiyumo Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Tholaiyumo Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vizhi Thiraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhi Thiraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Uruvam Azhiyaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uruvam Azhiyaadhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niththiraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niththiraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavai Kalaikkaadhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavai Kalaikkaadhadhu"/>
</div>
</pre>
