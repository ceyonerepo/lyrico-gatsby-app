---
title: "theera kadhalae"
album: "Media Masons"
artist: "Santhosh Balaji"
lyricist: "Alisa"
director: "Sasikumar Jayaraman"
path: "/albums/theera-kadhalae-song-lyrics"
song: "Theera Kadhalae"
image: ../../images/albumart/theera-kadhalae.jpg
date: 2022-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Cu0S3-6ndcU"
type: "album"
singers:
  - Sam Vishal 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theera thee aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera thee aval"/>
</div>
<div class="lyrico-lyrics-wrapper">paathai maatrinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathai maatrinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">karainthu pogiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karainthu pogiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vali thorum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thorum un "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">megangal moodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megangal moodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vida kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">paathaigal pala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaigal pala "/>
</div>
<div class="lyrico-lyrics-wrapper">nooru ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai adaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai adaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyamal nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyamal nindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vali thorum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thorum un "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">megangal moodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megangal moodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vida kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">paathaigal pala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaigal pala "/>
</div>
<div class="lyrico-lyrics-wrapper">nooru ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai adaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai adaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyamal nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyamal nindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un ithaludan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ithaludan"/>
</div>
<div class="lyrico-lyrics-wrapper">inaintha naal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaintha naal "/>
</div>
<div class="lyrico-lyrics-wrapper">maraven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraven"/>
</div>
<div class="lyrico-lyrics-wrapper">un madiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">mayangi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalai paniyin un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai paniyin un"/>
</div>
<div class="lyrico-lyrics-wrapper">muththangal enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththangal enge"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai irulil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai irulil "/>
</div>
<div class="lyrico-lyrics-wrapper">naan yengugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan yengugindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirinthen udainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirinthen udainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">manam thiranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam thiranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">arinthen sarinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arinthen sarinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan saagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kulir kaatril en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaatril en"/>
</div>
<div class="lyrico-lyrics-wrapper">udal uraiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal uraiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">un sirapil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sirapil "/>
</div>
<div class="lyrico-lyrics-wrapper">soodagi kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodagi kidanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam inaikkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam inaikkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan neeril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan neeril "/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai mithanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai mithanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kulir kaatril en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaatril en"/>
</div>
<div class="lyrico-lyrics-wrapper">udal uraiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal uraiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">un sirapil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sirapil "/>
</div>
<div class="lyrico-lyrics-wrapper">soodagi kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodagi kidanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam inaikkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam inaikkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan neeril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan neeril "/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai mithanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai mithanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theera thee aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera thee aval"/>
</div>
<div class="lyrico-lyrics-wrapper">paathai maatrinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathai maatrinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kanavile"/>
</div>
<div class="lyrico-lyrics-wrapper">karainthu pogiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karainthu pogiraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">theera kathale theera kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera kathale theera kathale"/>
</div>
</pre>
