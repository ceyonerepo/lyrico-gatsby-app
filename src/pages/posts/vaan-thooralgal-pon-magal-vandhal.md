---
title: 'vaan thooralgal song lyrics'
album: 'Pon Magal Vandhal'
artist: 'Govind Vasantha'
lyricist: 'Uma Devi'
director: 'J J Fredrick'
path: '/albums/pon-magal-vandhal-song-lyrics'
song: 'Vaan Thooralgal'
image: ../../images/albumart/ponmagal-vandhal.jpg
date: 2020-05-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Oabst6IuSGc"
type: 'parenting'
singers: 
- Chinmayi
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Vaan thooralgal
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaan thooralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthukkal paada
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhthukkal paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan meendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan meendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Pookkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Needhaanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Needhaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini needhaanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini needhaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaayae vaazh naatkalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanthaayae vaazh naatkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai naanadi thandhai naanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaai naanadi thandhai naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyaagiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyaagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa en vaagaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa en vaagaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Parandhu pogalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Parandhu pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandha vaanilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirandha vaanilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyaai naan iruppaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Oliyaai naan iruppaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae… vaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirae… vaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanamaai naanagiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanamaai naanagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru vaadhai theendidamal kaappaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru vaadhai theendidamal kaappaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi pol naanagiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thozhi pol naanagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaevai thaedi thaedi serppaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thaevai thaedi thaedi serppaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaerillaa silai naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaerillaa silai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerilaa nilam naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerilaa nilam naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaa pizhai mozhiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sariyaa pizhai mozhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yedhu naan anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu yedhu naan anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marangal pookkuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marangal pookkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirangal kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaai… nee kidaithaayae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagaai… nee kidaithaayae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Magalaai naan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Magalaai naan…"/>
</div>
</pre>