---
title: "dope track song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Rajan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Dope Track"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SSrdUo0IrAc"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Katrae un kaal adiyai naan thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrae un kaal adiyai naan thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae naan kaathurinthen kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae naan kaathurinthen kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu enna pudhu kaalam ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enna pudhu kaalam ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirodu anal veesum nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirodu anal veesum nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu polae irunthathillai eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu polae irunthathillai eppothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katrae un kaal adiyai naan thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrae un kaal adiyai naan thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae naan kaathurinthen kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae naan kaathurinthen kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu enna pudhu kaalam ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enna pudhu kaalam ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirodu anal veesum nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirodu anal veesum nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhangarai vizhakena vazhgiren naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhangarai vizhakena vazhgiren naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyum uravena theigiren naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyum uravena theigiren naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhigalin nilavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhigalin nilavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam thinam nanaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam thinam nanaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthum karaigal neengathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthum karaigal neengathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal karai suvadagal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal karai suvadagal "/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya nimidangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya nimidangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaipathu yethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaipathu yethadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal athu than pirigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal athu than pirigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam athu than maraigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam athu than maraigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum kannum thavikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum kannum thavikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai ondru thozhaikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai ondru thozhaikirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollathadi kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollathadi kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum enna devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum enna devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraginai virikka maranthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraginai virikka maranthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilladi ponmani neeyum enna penno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladi ponmani neeyum enna penno"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan manam thavikka paranthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan manam thavikka paranthaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhigalin nilavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhigalin nilavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam thinam nanaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam thinam nanaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthum karaigal neengathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthum karaigal neengathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal karai suvadagal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal karai suvadagal "/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya nimidangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya nimidangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaipathu yethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaipathu yethadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal"/>
</div>
</pre>
