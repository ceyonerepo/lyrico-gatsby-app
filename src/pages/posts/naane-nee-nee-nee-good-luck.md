---
title: "naane nee nee nee song lyrics"
album: "Good Luck"
artist: "Manoj Bhatnaghar"
lyricist: "Vairamuthu"
director: "Manoj Bhatnaghar"
path: "/albums/good-luck-song-lyrics"
song: "Naane Nee Nee Nee"
image: ../../images/albumart/good-luck.jpg
date: 2000-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CEu_CgXZ4Zw"
type: "love"
singers:
  - P. Unnikrishnan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naane nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethane naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethane naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhangal yaen yaen yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhangal yaen yaen yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondranoom vaa……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondranoom vaa……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya maegangal maegangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya maegangal maegangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maelam kottattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelam kottattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe oru pookkaadu pookkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe oru pookkaadu pookkaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyagattm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyagattm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavil medaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil medaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaezhuthaan vannangal…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaezhuthaan vannangal…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaezhaiyum meeriye minnuthey kannangal….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaezhaiyum meeriye minnuthey kannangal…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veiyl padaa idathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veiyl padaa idathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathin sinnangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin sinnangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanai aasaigal ennamooo pannungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai aasaigal ennamooo pannungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil enna valam engu ullathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil enna valam engu ullathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu thoondraathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu thoondraathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennil enna valam engu ullathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennil enna valam engu ullathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">En kangal sollum vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kangal sollum vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thedatha paagam yaeralam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedatha paagam yaeralam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal solli tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal solli tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naane nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethane naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethane naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhangal yaen yaen yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhangal yaen yaen yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondranoom vaa……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondranoom vaa……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm….kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm….kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalai kangalal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai kangalal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavinal oorinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavinal oorinbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalal kadhalai thoondinal perinpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalal kadhalai thoondinal perinpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvidhzal rendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvidhzal rendume"/>
</div>
<div class="lyrico-lyrics-wrapper">Saervathoo sitrinpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saervathoo sitrinpam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevanum jeevanum saervathoo perinpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevanum jeevanum saervathoo perinpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal udhadu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal udhadu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal pesugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal pesugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu inbam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu inbam sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nimidangalil intha udhadugalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nimidangalil intha udhadugalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodana mutham sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodana mutham sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pennage neeum aanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pennage neeum aanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaa ennenna kadhal sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaa ennenna kadhal sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethanae naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhangal yaen yaen yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhangal yaen yaen yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondranoom vaa……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondranoom vaa……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya maegangal maegangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya maegangal maegangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maelam kottattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelam kottattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe oru pookkaadu pookkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe oru pookkaadu pookkaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyagattm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyagattm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kaanpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kaanpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katcheri kaanpom…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katcheri kaanpom….."/>
</div>
</pre>
