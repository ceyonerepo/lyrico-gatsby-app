---
title: "andam ankitam song lyrics"
album: "Sye Raa Narasimha Reddy"
artist: "Amit Trivedi"
lyricist: "Ananta Sriram"
director: "Surender Reddy"
path: "/albums/sye-raa-narasimha-reddy-lyrics"
song: "Andam Ankitam"
image: ../../images/albumart/sye-raa-narasimha-reddy.jpg
date: 2019-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EljjO6f5DO4"
type: "happy"
singers:
  - Vijay Prakash
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andam Ankitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Ankitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Arpitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Arpitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham Shaswtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham Shaswtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Meere Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meere Jeevitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu Padhmanivai Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Padhmanivai Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravitejam Nenautha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravitejam Nenautha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulai Nuvu Vechunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulai Nuvu Vechunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelarajai Ney Vastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelarajai Ney Vastha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varichuthaa Tharinchuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varichuthaa Tharinchuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andam Ankitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Ankitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Arpitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Arpitam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Oo Chakkorayaanam Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Oo Chakkorayaanam Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheraa Nennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheraa Nennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhe Nadivennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhe Nadivennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkorahasyam Virichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkorahasyam Virichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Virisaa Puvvulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virisaa Puvvulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Niliche Navvulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Niliche Navvulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarasowthanu Neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasowthanu Neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ituraave Raayencha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ituraave Raayencha"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Naadhaina Santhonsham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Naadhaina Santhonsham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Neeke Raasuncha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Neeke Raasuncha"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyayachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyayachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Layaayachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Layaayachaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andam Ankitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Ankitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Arpitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Arpitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham Shaswtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham Shaswtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Meere Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meere Jeevitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakarina Thakadhina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakarina Thakadhina "/>
</div>
<div class="lyrico-lyrics-wrapper">Thakarina Dhinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakarina Dhinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thidiyana Thadiyana Dhillana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thidiyana Thadiyana Dhillana"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamulu Kalisenu Madhuvidhugona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamulu Kalisenu Madhuvidhugona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaghanisa Padhumusa Dheejaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaghanisa Padhumusa Dheejaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakarina Thakadhina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakarina Thakadhina "/>
</div>
<div class="lyrico-lyrics-wrapper">Thakarina Dhinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakarina Dhinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thidiyana Thadiyana Dhillana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thidiyana Thadiyana Dhillana"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayamu Adhirenu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayamu Adhirenu "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura Kshanaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura Kshanaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramu Kurisenu Thandhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramu Kurisenu Thandhana"/>
</div>
</pre>
