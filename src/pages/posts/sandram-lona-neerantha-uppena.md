---
title: "sandram lona song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Devi Sri Prasad"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Sandram Lona Neerantha"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/f9zE9O8eO4s"
type: "sad"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandramlona neeranthaa kanneeraayene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandramlona neeranthaa kanneeraayene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundellonaa prathimoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellonaa prathimoolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gonthe mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gonthe mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu gaali nippai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu gaali nippai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne kaalchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne kaalchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sudigaali ninnetthukelli nanne koolchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sudigaali ninnetthukelli nanne koolchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ele ele ele le, ye ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ele ele ele le, ye ye ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ele ele le, ye ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ele ele le, ye ye ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandramlona neeranthaa kanneeraayene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandramlona neeranthaa kanneeraayene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundellonaa prathimoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellonaa prathimoolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gonthe mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gonthe mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu gaali nippai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu gaali nippai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne kaalchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne kaalchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sudigaali ninnetthukelli nanne koolchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sudigaali ninnetthukelli nanne koolchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ele ele ele le, ye ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ele ele ele le, ye ye ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ele ele le, ye ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ele ele le, ye ye ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaalilo nee maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo nee maate"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalapai nee paate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalapai nee paate"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthagaalisthunnaa nuvvu leve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthagaalisthunnaa nuvvu leve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammavai prathimuddha thinipinchi penchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavai prathimuddha thinipinchi penchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakore aakalunnaa nuvvu raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakore aakalunnaa nuvvu raave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enno maatalu inkaa neetho cheppaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno maatalu inkaa neetho cheppaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachunchaanani vaatikemi cheppedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachunchaanani vaatikemi cheppedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno rangulu pooseti nee chirunavvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno rangulu pooseti nee chirunavvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallee nene eppudu choosedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallee nene eppudu choosedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijame cheppaali ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijame cheppaali ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku cheppe nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku cheppe nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadu naatho untaanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadu naatho untaanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Abaddham cheppaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abaddham cheppaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandramlona neeranthaa kanneeraayene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandramlona neeranthaa kanneeraayene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundellonaa prathimoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellonaa prathimoolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gonthe mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gonthe mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu gaali nippai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu gaali nippai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne kaalchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne kaalchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sudigaali ninnetthukelli nanne koolchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sudigaali ninnetthukelli nanne koolchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ele ele ele le, ye ye ye, ele ele le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ele ele ele le, ye ye ye, ele ele le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ele ele ele le, ye ye ye, ele ele le ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ele ele ele le, ye ye ye, ele ele le ye"/>
</div>
</pre>
