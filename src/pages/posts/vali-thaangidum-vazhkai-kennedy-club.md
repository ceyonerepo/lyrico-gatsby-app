---
title: "vali thaangidum vazhkai song lyrics"
album: "Kennedy Club"
artist: "D. Imman"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kennedy-club-lyrics"
song: "Vali Thaangidum Vazhkai"
image: ../../images/albumart/kennedy-club.jpg
date: 2019-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kveIHtrpxYM"
type: "mass"
singers:
  - Keerthi Sagathia
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaahaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaahaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaahaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali Thaangidum Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Thaangidum Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhondrum Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhondrum Kidaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idithaangiya Kambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idithaangiya Kambam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragaalae Udaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragaalae Udaiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Illa Vetrigal Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Illa Vetrigal Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilum Kidaiyadhuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum Kidaiyadhuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Veesi Kaatrai Oonam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Veesi Kaatrai Oonam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyavum Mudiyaathuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyavum Mudiyaathuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Kettum Thooram Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kettum Thooram Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Koppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Koppai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanadha Inbam Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha Inbam Kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaada Paarppaar Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaada Paarppaar Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaippai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaippai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraadi Neeyae Ezhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraadi Neeyae Ezhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Theerppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Theerppai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivoduuuuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivoduuuuuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaahaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaahaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaahaaaaaaaaaaaa"/>
</div>
</pre>
