---
title: "mayakka oosi song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Kalai Kumar"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Mayakka Oosi"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h0qPeWwd9YI"
type: "love"
singers:
  - Vijay Prakash
  - Srimathumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayakka Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakka Oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Paarvai Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Paarvai Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaakki Thaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaakki Thaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moorchai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorchai Aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">Marugi Marugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marugi Marugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Urugi Urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Urugi Urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaangi Thaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaangi Thaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham Ponene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uh Uh Uh Uh Uh Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uh Uh Uh Uh Uh Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaal Irukka Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaal Irukka Mudiyaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Uh Uh Uh Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Uh Uh Uh Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Aachchu Yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachchu Yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku Theriyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakku Theriyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakka Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakka Oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Paarvai Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Paarvai Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaakki Thaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaakki Thaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moorchai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorchai Aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Ulla Azhagai Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Ulla Azhagai Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumin Nilayam Ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumin Nilayam Ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Kannil Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kannil Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Ariyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ariyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Kaadhal Kondene Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Kaadhal Kondene Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silayai Meettum Uliyai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silayai Meettum Uliyai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Seiyum Viralaal Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Seiyum Viralaal Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Seidhaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Seidhaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyai Kalaiyaamal Valiyai Unaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyai Kalaiyaamal Valiyai Unaraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaaga Kondraaye Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaaga Kondraaye Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Maru Nodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Maru Nodiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhayam Valapuram Maaridudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam Valapuram Maaridudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kai Theendum Oru Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Theendum Oru Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambhugal Enakkulle Vedikindradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambhugal Enakkulle Vedikindradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakka Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakka Oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Paarvai Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Paarvai Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaakki Thaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaakki Thaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moorchai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorchai Aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye Naamum Kaanum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Naamum Kaanum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Nirkkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Nirkkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyaa Vidaiyai Sonna Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyaa Vidaiyai Sonna Pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Suttrattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Suttrattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Virikkaamal Uyare Parakkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Virikkaamal Uyare Parakkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Vinnai Thaandattum Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Vinnai Thaandattum Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Munne Thongum Thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Munne Thongum Thottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottru Pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottru Pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Soodaa Pookkal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Soodaa Pookkal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenji Ketkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenji Ketkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Neeyaaga Puzhalum Naanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Neeyaaga Puzhalum Naanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Maiyam Kollattum Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Maiyam Kollattum Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Endra Vaarthaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endra Vaarthaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Kavidhaihal Therigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kavidhaihal Therigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal Thaakki Idhayangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Thaakki Idhayangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Podi Podiyana Udaigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Podi Podiyana Udaigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakka Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakka Oosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Paarvai Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Paarvai Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaakki Thaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaakki Thaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moorchai Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moorchai Aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">Marugi Marugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marugi Marugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Urugi Urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Urugi Urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaangi Thaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaangi Thaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham Ponene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaal Irukka Mudiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaal Irukka Mudiyaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Aachchu Yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aachchu Yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku Theriyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakku Theriyaadhe"/>
</div>
</pre>
