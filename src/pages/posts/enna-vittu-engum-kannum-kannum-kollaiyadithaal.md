---
title: "ennai vittu engum song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Vignesh Shivan"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Ennai Vittu Engum"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nAP7esBta10"
type: "Love"
singers:
  - Ranjith	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai Kavuthu Pottutiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavuthu Pottutiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanimozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanimozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idha Kekka Yaarum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Kekka Yaarum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Sonna Pechaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Sonna Pechaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Eppovume Kekaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Eppovume Kekaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannu En Kanna Kollai Adithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannu En Kanna Kollai Adithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagale Paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale Paniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piraiye Nudhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraiye Nudhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Endrum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Endrum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Endrum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Endrum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Midham Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Midham Azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Velicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Thuliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Thuliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Enai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Enai Thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanaithidave Vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaithidave Vanthavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Endrum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Endrum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaththukulla Aayiram Meen Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththukulla Aayiram Meen Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Meenu Namakkunnu Ezhuthurikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Meenu Namakkunnu Ezhuthurikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Meenu Thaana Maattirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Meenu Thaana Maattirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yosikkaama Appudiya Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosikkaama Appudiya Amukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nidham Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidham Thondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaigal Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Nooru Aasaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nooru Aasaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Saarndhavai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Saarndhavai Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Vaitha Poove Nee Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Vaitha Poove Nee Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vaadi Poga Vidave Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vaadi Poga Vidave Maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatpa Veppam Maaridalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatpa Veppam Maaridalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjile En Nenjiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile En Nenjiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thottil Unakkaga Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thottil Unakkaga Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nithirai Kalaiyamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithirai Kalaiyamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalattidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalattidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Midham Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Midham Azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Velicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Thuliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Thuliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Enai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Enai Thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjile En Nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile En Nenjile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Oonjal Unakkaaga Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Oonjal Unakkaaga Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mellamaai Konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamaai Konjamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamaai Thaalattidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamaai Thaalattidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Endrum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Endrum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Endrum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Endrum Pogaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Engum Pogaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Engum Pogaathey"/>
</div>
</pre>
