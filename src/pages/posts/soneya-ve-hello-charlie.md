---
title: "soneya ve song lyrics"
album: "Hello Charlie"
artist: "Kanika Kapoor - Jasbir Jassi"
lyricist: "Kumaar"
director: "Pankaj Saraswat"
path: "/albums/hello-charlie-lyrics"
song: "Soneya Ve"
image: ../../images/albumart/hello-charlie.jpg
date: 2021-04-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/8ZzDuMUx2p4"
type: "Item Song"
singers:
  - Kanika Kapoor
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meri ankhiyon ke samajh ishaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri ankhiyon ke samajh ishaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine tod dene border saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine tod dene border saare"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri ankhiyon ke samajh ishaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri ankhiyon ke samajh ishaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine tod dene border saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine tod dene border saare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas time hai thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas time hai thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ankh mere utte utte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ankh mere utte utte"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye rakh le soneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye rakh le soneya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal hona ae farara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal hona ae farara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj takk le ve takk le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj takk le ve takk le"/>
</div>
<div class="lyrico-lyrics-wrapper">Takk le soneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takk le soneya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Love shuv da swad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love shuv da swad"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj chakh le ve chakh le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj chakh le ve chakh le"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakh le sohneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakh le sohneya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal hona ae farara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal hona ae farara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj takk le ve takk le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj takk le ve takk le"/>
</div>
<div class="lyrico-lyrics-wrapper">Takk le sohneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takk le sohneya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Love shuv da swad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love shuv da swad"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj chakh le ve chakh le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj chakh le ve chakh le"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakh le sohneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakh le sohneya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meri ankhiyon ka branded kajal ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri ankhiyon ka branded kajal ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri ankhiyon ko message karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri ankhiyon ko message karta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan uthe mere seene mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan uthe mere seene mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jab jab usko padhta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jab jab usko padhta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main cheez hi kuch aisi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main cheez hi kuch aisi hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabka dil mujhpe hi adta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabka dil mujhpe hi adta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai zameen ke aashiq ek taraf
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai zameen ke aashiq ek taraf"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh chaand bhi mujhpe marta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh chaand bhi mujhpe marta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine dil tera toda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine dil tera toda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal tote tote tote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal tote tote tote"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye chakk le soneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye chakk le soneya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal hona ae farara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal hona ae farara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj takk le ve takk le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj takk le ve takk le"/>
</div>
<div class="lyrico-lyrics-wrapper">Love shuv da swaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love shuv da swaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj chakh le ve chakh le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj chakh le ve chakh le"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakh le soneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakh le soneya ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal hona ae farara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal hona ae farara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj takk le ve takk le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj takk le ve takk le"/>
</div>
<div class="lyrico-lyrics-wrapper">Takk le sohneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takk le sohneya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Love shuv da swad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love shuv da swad"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajj chakh le ve chakh le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj chakh le ve chakh le"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakh le sohneya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakh le sohneya ve"/>
</div>
</pre>
