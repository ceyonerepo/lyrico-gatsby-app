---
title: "vittu thallu song lyrics"
album: "Seyal"
artist: "Siddharth Vipin"
lyricist: "Siddharth Vipin"
director: "Ravi Abbulu"
path: "/albums/seyal-lyrics"
song: "Vittu Thallu"
image: ../../images/albumart/seyal.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zyshik5lHuM"
type: "happy"
singers:
  - Jagadeesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maama dai vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama dai vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku yenda ivlo dullu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku yenda ivlo dullu "/>
</div>
<div class="lyrico-lyrics-wrapper">asalta vanthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asalta vanthu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">friend oda iruntha dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friend oda iruntha dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnunga vidanam jollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnunga vidanam jollu"/>
</div>
<div class="lyrico-lyrics-wrapper">poyita tata sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyita tata sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">sogatha aduche kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sogatha aduche kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">yethuku da allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethuku da allu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maama dai vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama dai vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku yenda ivlo dullu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku yenda ivlo dullu "/>
</div>
<div class="lyrico-lyrics-wrapper">asalta vanthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asalta vanthu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">friend oda iruntha dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friend oda iruntha dhillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanjara meen kedaikala na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjara meen kedaikala na"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvattu kolambu vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvattu kolambu vai"/>
</div>
<div class="lyrico-lyrics-wrapper">itha yarum othukalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha yarum othukalana"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga ooru ennoore 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru ennoore "/>
</div>
<div class="lyrico-lyrics-wrapper">kulathure poruru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathure poruru"/>
</div>
<div class="lyrico-lyrics-wrapper">egmore perambure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egmore perambure"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma oor"/>
</div>
<div class="lyrico-lyrics-wrapper">ambathuru adaiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambathuru adaiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kottor kuratur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottor kuratur"/>
</div>
<div class="lyrico-lyrics-wrapper">mudichuru meenjuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudichuru meenjuru"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">facebook il fake id
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="facebook il fake id"/>
</div>
<div class="lyrico-lyrics-wrapper">love sonna vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love sonna vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">girl friend una whatsup
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="girl friend una whatsup"/>
</div>
<div class="lyrico-lyrics-wrapper">il block panna vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="il block panna vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">maamu pursu pitpocket
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamu pursu pitpocket"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchalum vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchalum vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu empty thane da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu empty thane da"/>
</div>
<div class="lyrico-lyrics-wrapper">feel panamo naama da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feel panamo naama da"/>
</div>
<div class="lyrico-lyrics-wrapper">bike race il vilunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bike race il vilunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">hospitalu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hospitalu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">alave venam siruchu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alave venam siruchu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga nikira nurse ah paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga nikira nurse ah paru"/>
</div>
<div class="lyrico-lyrics-wrapper">veli naatil velai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veli naatil velai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">nambatha vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambatha vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe ooril enna illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe ooril enna illa "/>
</div>
<div class="lyrico-lyrics-wrapper">oora suthi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora suthi paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga ooru ennoore 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru ennoore "/>
</div>
<div class="lyrico-lyrics-wrapper">kulathure poruru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathure poruru"/>
</div>
<div class="lyrico-lyrics-wrapper">egmore perambure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egmore perambure"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma oor"/>
</div>
<div class="lyrico-lyrics-wrapper">ambathuru adaiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambathuru adaiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kottor kuratur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottor kuratur"/>
</div>
<div class="lyrico-lyrics-wrapper">mudichuru meenjuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudichuru meenjuru"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhoni bating last ball
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoni bating last ball"/>
</div>
<div class="lyrico-lyrics-wrapper">six venum pattununa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="six venum pattununa"/>
</div>
<div class="lyrico-lyrics-wrapper">power cut na sattunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power cut na sattunu"/>
</div>
<div class="lyrico-lyrics-wrapper">than tension aavatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than tension aavatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vena feeling enna aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vena feeling enna aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">semma match highlights
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma match highlights"/>
</div>
<div class="lyrico-lyrics-wrapper">than night undu remote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than night undu remote"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pottu udachudatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pottu udachudatha"/>
</div>
<div class="lyrico-lyrics-wrapper">exam il fail aana feel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="exam il fail aana feel "/>
</div>
<div class="lyrico-lyrics-wrapper">panatha bilgates ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panatha bilgates ah "/>
</div>
<div class="lyrico-lyrics-wrapper">business man aagalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="business man aagalan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veli naatil velai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veli naatil velai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">nambatha vittu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambatha vittu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe ooril enna illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe ooril enna illa "/>
</div>
<div class="lyrico-lyrics-wrapper">oora suthi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora suthi paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga ooru ennoore 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru ennoore "/>
</div>
<div class="lyrico-lyrics-wrapper">kulathure poruru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathure poruru"/>
</div>
<div class="lyrico-lyrics-wrapper">egmore perambure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egmore perambure"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma oor"/>
</div>
<div class="lyrico-lyrics-wrapper">ambathuru adaiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambathuru adaiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kottor kuratur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottor kuratur"/>
</div>
<div class="lyrico-lyrics-wrapper">mudichuru meenjuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudichuru meenjuru"/>
</div>
<div class="lyrico-lyrics-wrapper">madras namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madras namma ooru"/>
</div>
</pre>
