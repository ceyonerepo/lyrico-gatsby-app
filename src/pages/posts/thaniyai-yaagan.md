---
title: "thaniyai song lyrics"
album: "Yaagan"
artist: "Niro Pirabakaran"
lyricist: "Kabilan"
director: "Vinoth Thangavel"
path: "/albums/yaagan-lyrics"
song: "Thaniyai"
image: ../../images/albumart/yaagan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fnMrDvpfutQ"
type: "love"
singers:
  - Sathya Prakash
  - Jessica Judes
  - Niro
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thaniyai thananth thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyai thananth thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thunai thedi alainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thunai thedi alainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvai adi methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvai adi methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyiril indru tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyiril indru tholainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaniyai thananth thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyai thananth thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thunai thedi alainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thunai thedi alainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvai adi methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvai adi methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyiril indru tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyiril indru tholainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal paarvai paarthayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal paarvai paarthayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil nilaithu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil nilaithu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sonnal unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sonnal unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai tholaithu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai tholaithu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thottu ponai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thottu ponai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethugai monai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethugai monai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil mella nulainthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil mella nulainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi madiyil veenai aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi madiyil veenai aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaniyai thananth thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyai thananth thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thunai thedi alainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thunai thedi alainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvai ada methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvai ada methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyiril indru tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyiril indru tholainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathaiyai vidu kathaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiyai vidu kathaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ena thayakathodu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena thayakathodu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanada ivana thodar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanada ivana thodar "/>
</div>
<div class="lyrico-lyrics-wrapper">kathai pola valarnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathai pola valarnthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagiya vithai pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya vithai pole"/>
</div>
<div class="lyrico-lyrics-wrapper">avalukule puthainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avalukule puthainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi irandinil nano indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi irandinil nano indru"/>
</div>
<div class="lyrico-lyrics-wrapper">virucham pola aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virucham pola aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">pani thuli mazhai nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani thuli mazhai nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">nanainthu poren alagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanainthu poren alagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai thuvatidu neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai thuvatidu neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thanimai venam thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thanimai venam thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un koonthal en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koonthal en "/>
</div>
<div class="lyrico-lyrics-wrapper">meesai uravadume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesai uravadume "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirinil un paadal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirinil un paadal "/>
</div>
<div class="lyrico-lyrics-wrapper">en raagam isai aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en raagam isai aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">kannale nee enthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale nee enthan "/>
</div>
<div class="lyrico-lyrics-wrapper">thalai kothinai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kothinai "/>
</div>
<div class="lyrico-lyrics-wrapper">viral pada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral pada "/>
</div>
<div class="lyrico-lyrics-wrapper">unnale en kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale en kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">muthal thethi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal thethi thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enathu paathi unathu paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu paathi unathu paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu than oru kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu than oru kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku mele ivanai kettal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku mele ivanai kettal"/>
</div>
<div class="lyrico-lyrics-wrapper">inimel solla vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel solla vaarthai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">poyaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan imaigalali korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan imaigalali korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pola paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pola paranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaivinil paranthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaivinil paranthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un tholil vanthu serven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un tholil vanthu serven"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini vilaketri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini vilaketri"/>
</div>
<div class="lyrico-lyrics-wrapper">velichamaga irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velichamaga irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">iravinil ivalodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravinil ivalodu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kaaya vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaaya vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi thiranthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi thiranthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">avan naabagam ini naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan naabagam ini naam"/>
</div>
<div class="lyrico-lyrics-wrapper">ondraaga sernthaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondraaga sernthaale "/>
</div>
<div class="lyrico-lyrics-wrapper">oru kaaviyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kaaviyam "/>
</div>
<div class="lyrico-lyrics-wrapper">en nenju idamillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenju idamillai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu naal varai ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu naal varai ada "/>
</div>
<div class="lyrico-lyrics-wrapper">indru nee vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru nee vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">sonnaye un kathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnaye un kathalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaniyai thananth thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyai thananth thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thunai thedi alainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thunai thedi alainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvai adi methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvai adi methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uyiril indru tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uyiril indru tholainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal paarvai paarthayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal paarvai paarthayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil nilaithu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil nilaithu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai sonnal unnaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai sonnal unnaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai tholaithu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai tholaithu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thottu ponai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thottu ponai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethugai monai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethugai monai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil mella nulainthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil mella nulainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi madiyil veenai aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi madiyil veenai aanene"/>
</div>
</pre>
