---
title: "laalijo laalijo song lyrics"
album: "KS100"
artist: "Navaneeth Chari"
lyricist: "Bhashya Sree"
director: "Sher"
path: "/albums/ks100-lyrics"
song: "Laalijo Laalijo"
image: ../../images/albumart/ks100.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GKPKRtfRT2Y"
type: "happy"
singers:
  - Anjana Sowmya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">laalijo laalijo laalijo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laalijo laalijo laalijo "/>
</div>
<div class="lyrico-lyrics-wrapper">chitti kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitti kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">navvuthu aaduko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvuthu aaduko "/>
</div>
<div class="lyrico-lyrics-wrapper">brathukane aata chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brathukane aata chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">adharaku bedharaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adharaku bedharaku "/>
</div>
<div class="lyrico-lyrics-wrapper">edhuruko baadhalunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruko baadhalunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalalo raanila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalalo raanila "/>
</div>
<div class="lyrico-lyrics-wrapper">geluchuko manasu nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geluchuko manasu nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">padi padi lechi alavai naduv 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi padi lechi alavai naduv "/>
</div>
<div class="lyrico-lyrics-wrapper">budi budi nadakalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="budi budi nadakalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">chiru chiru oo chillakai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chiru chiru oo chillakai"/>
</div>
<div class="lyrico-lyrics-wrapper">nadichi viduvaku navvulunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadichi viduvaku navvulunu"/>
</div>
<div class="lyrico-lyrics-wrapper">bangaru bujjayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bangaru bujjayi "/>
</div>
<div class="lyrico-lyrics-wrapper">prathi badhaku good bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi badhaku good bye"/>
</div>
<div class="lyrico-lyrics-wrapper">chepesi nuv kadhalali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chepesi nuv kadhalali"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaiyani choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaiyani choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi yedupu navvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi yedupu navvali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanentho andham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanentho andham "/>
</div>
<div class="lyrico-lyrics-wrapper">toofanu gandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toofanu gandam"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu raalechedhi mabbe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu raalechedhi mabbe "/>
</div>
<div class="lyrico-lyrics-wrapper">aanandha baashpam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanandha baashpam "/>
</div>
<div class="lyrico-lyrics-wrapper">kanneeti kaashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneeti kaashtam"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu icchedi kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu icchedi kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">koosantha navvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koosantha navvu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasepu baadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasepu baadha"/>
</div>
<div class="lyrico-lyrics-wrapper">ee rendu panchedhe jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee rendu panchedhe jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">toofanuni baadanuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toofanuni baadanuko"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi chinukuki edhurelipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi chinukuki edhurelipo"/>
</div>
<div class="lyrico-lyrics-wrapper">aa urumoka swaramanuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa urumoka swaramanuko"/>
</div>
<div class="lyrico-lyrics-wrapper">ee pidugoka goduganuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee pidugoka goduganuko"/>
</div>
<div class="lyrico-lyrics-wrapper">jo jo jo laali jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jo jo jo laali jo jo jo"/>
</div>
<div class="lyrico-lyrics-wrapper">jo jo jo laali jo jo jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jo jo jo laali jo jo jo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa malle theeganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa malle theeganu"/>
</div>
<div class="lyrico-lyrics-wrapper">allukupommani cheppindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allukupommani cheppindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru cheepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru cheepu"/>
</div>
<div class="lyrico-lyrics-wrapper">ye guruu nerpina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye guruu nerpina"/>
</div>
<div class="lyrico-lyrics-wrapper">patani koyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patani koyila"/>
</div>
<div class="lyrico-lyrics-wrapper">paadesi pondhindhi meppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadesi pondhindhi meppu"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevithame nerpunule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevithame nerpunule"/>
</div>
<div class="lyrico-lyrics-wrapper">ennenno paataalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenno paataalu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi gadiyanu chadhivesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi gadiyanu chadhivesi"/>
</div>
<div class="lyrico-lyrics-wrapper">payaninchu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payaninchu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee gelupoka sambaramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gelupoka sambaramu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee otami anubhavamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee otami anubhavamu"/>
</div>
<div class="lyrico-lyrics-wrapper">chirunavvule aayudhamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chirunavvule aayudhamu"/>
</div>
<div class="lyrico-lyrics-wrapper">sandhinchu prathi kshanamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandhinchu prathi kshanamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">laalijo laalijo laalijo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laalijo laalijo laalijo "/>
</div>
<div class="lyrico-lyrics-wrapper">chitti kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitti kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">navvuthu aaduko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvuthu aaduko "/>
</div>
<div class="lyrico-lyrics-wrapper">brathukane aata chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brathukane aata chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">adharaku bedharaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adharaku bedharaku "/>
</div>
<div class="lyrico-lyrics-wrapper">edhuruko baadhalunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruko baadhalunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalalo raanila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalalo raanila "/>
</div>
<div class="lyrico-lyrics-wrapper">geluchuko manasu nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geluchuko manasu nanna"/>
</div>
</pre>
