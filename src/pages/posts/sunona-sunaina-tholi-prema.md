---
title: "sunona sunaina song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Sunona Sunaina"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dGuEm4RMZvE"
type: "love"
singers:
  -	Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sunonaa Sunainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunonaa Sunainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Hyper Heart-e 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hyper Heart-e "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Kollagottanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Kollagottanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sunonaa Sunainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunonaa Sunainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vente Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vente Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhe Laaga Cheyyanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhe Laaga Cheyyanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Twinkle Twinkle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Twinkle Twinkle "/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaki Linkavanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaki Linkavanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Single Single 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Single Single "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasutho Mingle Avvanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasutho Mingle Avvanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Simple Simple 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Simple Simple "/>
</div>
<div class="lyrico-lyrics-wrapper">Life Lo Wonder Avvanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Lo Wonder Avvanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sunainaa Neetho Raanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunainaa Neetho Raanaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunonaa Sunainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunonaa Sunainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Hyper Heart-e 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hyper Heart-e "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Kollagottanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Kollagottanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sunonaa Sunainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunonaa Sunainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vente Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vente Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhe Laaga Cheyyanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhe Laaga Cheyyanaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Age-e Pothe Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Age-e Pothe Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Raade Nuvvem Chesinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raade Nuvvem Chesinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Open Chesina Bottle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Open Chesina Bottle "/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Kaali Cheseynaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Kaali Cheseynaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Lovely Lady Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Lovely Lady Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Bet-e Chesinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Bet-e Chesinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Iddari Madhyana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Iddari Madhyana "/>
</div>
<div class="lyrico-lyrics-wrapper">London Bridge-e Nene Daateynaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="London Bridge-e Nene Daateynaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Sarigama Neelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Sarigama Neelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Padanisa Kalapavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padanisa Kalapavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvv Palakavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvv Palakavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Paatugaa Naatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Paatugaa Naatho "/>
</div>
<div class="lyrico-lyrics-wrapper">Paatagaa Maaravaa Nuvv Paadavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatagaa Maaravaa Nuvv Paadavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Twinkle Twinkle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Twinkle Twinkle "/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaloki Linkavanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaloki Linkavanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Single Single 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Single Single "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasutho Mingle Avvanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasutho Mingle Avvanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Simple Simple 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Simple Simple "/>
</div>
<div class="lyrico-lyrics-wrapper">Life Lo Wonder Avvanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Lo Wonder Avvanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sunainaa Neetho Raanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunainaa Neetho Raanaa"/>
</div>
</pre>
