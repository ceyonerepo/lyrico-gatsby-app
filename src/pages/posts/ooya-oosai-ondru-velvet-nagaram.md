---
title: "ooya oosai ondru song lyrics"
album: "Velvet Nagaram"
artist: "Achu Rajamani"
lyricist: "Kaber Vasuki"
director: "Manojkumar Natarajan"
path: "/albums/velvet-nagaram-lyrics"
song: "Ooya Oosai Ondru"
image: ../../images/albumart/velvet-nagaram.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GFPZ8imiGeo"
type: "melody"
singers:
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooya oosai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooya oosai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal ketkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhin manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhin manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanamum kanakirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanamum kanakirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal adhae kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal adhae kelvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvai ulava udhiram uraiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvai ulava udhiram uraiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi kaatti pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaatti pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan vaazhkaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan vaazhkaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai indri thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai indri thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpin naangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpin naangalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravae un manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae un manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yenthiyadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yenthiyadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unara mudiyaadha…aa…aa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unara mudiyaadha…aa…aa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno engo engo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno engo engo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedalai thuvanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedalai thuvanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyum thadaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyum thadaiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae purigiradhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae purigiradhae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedi thaembi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi thaembi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeindhu paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeindhu paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiyum vidiya pudhirum pirakkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiyum vidiya pudhirum pirakkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi kaatti pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaatti pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai thaedi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai thaedi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpin naangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpin naangalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravae un manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae un manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yenthiyadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yenthiyadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unara mudiyaadha…aa…aa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unara mudiyaadha…aa…aa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooya oosai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooya oosai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal ketkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhin manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhin manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanamum kanakirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanamum kanakirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal adhae kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal adhae kelvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvai ulava udhiram uraiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvai ulava udhiram uraiyudhae"/>
</div>
</pre>
