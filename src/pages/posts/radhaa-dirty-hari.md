---
title: "radhaa song lyrics"
album: "Dirty Hari"
artist: "Mark K. Robin"
lyricist: "Shresta"
director: "M. S. Raju"
path: "/albums/dirty-hari-lyrics"
song: "Radhaa"
image: ../../images/albumart/dirty-hari.jpg
date: 2020-12-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rs8keWqyxsQ"
type: "love"
singers:
  - Harini Ivaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhaa Radhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhaa Radhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee radha andhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee radha andhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Varninchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varninchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye bhava bhashyaalu Saripovunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye bhava bhashyaalu Saripovunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye raaga varnaale Vinipinchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye raaga varnaale Vinipinchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venu gaanalu…Saripovunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venu gaanalu…Saripovunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venu madhava melukonara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venu madhava melukonara"/>
</div>
<div class="lyrico-lyrics-wrapper">Melime kanara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melime kanara"/>
</div>
<div class="lyrico-lyrics-wrapper">Swarna renuvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swarna renuvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Soudhame kanaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soudhame kanaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa krishnayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa krishnayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa krishnayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa krishnayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasa sallaapala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasa sallaapala"/>
</div>
<div class="lyrico-lyrics-wrapper">Swapna theeram chera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapna theeram chera"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa krishnayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa krishnayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa raa krishnayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa krishnayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhane chera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhane chera"/>
</div>
<div class="lyrico-lyrics-wrapper">Radha gopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radha gopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuninchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuninchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee radha kannulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee radha kannulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhusudhana manavi vinara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhusudhana manavi vinara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegi"/>
</div>
</pre>
