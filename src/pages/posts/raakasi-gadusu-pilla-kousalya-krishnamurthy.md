---
title: "raakasi gadusu pilla song lyrics"
album: "Kousalya Krishnamurthy"
artist: "Dhibu Ninan Thomas"
lyricist: "Rambabu Gosala"
director: "Bhimaneni Srinivasa Rao"
path: "/albums/kousalya-krishnamurthy-lyrics"
song: "Raakasi Gadusu Pilla"
image: ../../images/albumart/kousalya-krishnamurthy.jpg
date: 2019-08-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/x_MK-g2-4e4"
type: "happy"
singers:
  - Ananya Nair
  - Rahul Sipligunj
  - Roshitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raakasi Gadusu Pillaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Gadusu Pillaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakaasi Saruki Pilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakaasi Saruki Pilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridhi Evaridhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridhi Evaridhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Allari Cheshtala Ammaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari Cheshtala Ammaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Aatallo Geliche Killadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatallo Geliche Killadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Jinkalaa Chenguna Chindhadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinkalaa Chenguna Chindhadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Jorugaa Vacchesthundhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorugaa Vacchesthundhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridhi Evaridhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridhi Evaridhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Inti Devatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Inti Devatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Mandharaa Pulatha Urantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandharaa Pulatha Urantha "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jatha Rendu Jella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jatha Rendu Jella "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Sitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Sitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaru Picchuka Chinnari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaru Picchuka Chinnari "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandrika Maa Bujji 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandrika Maa Bujji "/>
</div>
<div class="lyrico-lyrics-wrapper">Gopika Sakkangunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopika Sakkangunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti Chilaka Penki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Chilaka Penki "/>
</div>
<div class="lyrico-lyrics-wrapper">Pillave Konte Pillave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillave Konte Pillave "/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Polike Vacchinaadhile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Polike Vacchinaadhile "/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Pillave Gatti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pillave Gatti "/>
</div>
<div class="lyrico-lyrics-wrapper">Pillave Nanna Kucchilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillave Nanna Kucchilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Puttinavule Nannakanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttinavule Nannakanna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Nijamayyela Neekunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Nijamayyela Neekunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtame Theerelaa Kannamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtame Theerelaa Kannamma "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalabadi Ee Aata Nercheaivaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalabadi Ee Aata Nercheaivaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Venakadugu Veyani Vyuhamlaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakadugu Veyani Vyuhamlaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Bariloki Nuvve Dhukala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bariloki Nuvve Dhukala "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnammaa Nilabadi Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnammaa Nilabadi Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Churuku Chupeyvaa Bullipitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Churuku Chupeyvaa Bullipitta "/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjipitta Pasi Paalapitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjipitta Pasi Paalapitta "/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvulitta Kurise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvulitta Kurise "/>
</div>
<div class="lyrico-lyrics-wrapper">Pulabutta Paidigutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulabutta Paidigutta "/>
</div>
<div class="lyrico-lyrics-wrapper">Putta Thenepatta Vendi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putta Thenepatta Vendi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelinta Meghamalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelinta Meghamalle "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Merise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Merise "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Inti Devatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Inti Devatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa Naana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa Naana "/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaana Naanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana Naanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mandharaa Pulatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandharaa Pulatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Billi Jabili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Billi Jabili "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvamma Jajimalli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvamma Jajimalli "/>
</div>
<div class="lyrico-lyrics-wrapper">Kommaku Chellemma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommaku Chellemma "/>
</div>
<div class="lyrico-lyrics-wrapper">Chura Churaa Chudagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura Churaa Chudagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Suryude Parugamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryude Parugamma "/>
</div>
<div class="lyrico-lyrics-wrapper">Kondapalli Bomma Kousamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondapalli Bomma Kousamma "/>
</div>
<div class="lyrico-lyrics-wrapper">Palle Gunde Savvadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Gunde Savvadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvamma Pudamike Rangule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvamma Pudamike Rangule "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Letha Navvulamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Letha Navvulamma "/>
</div>
<div class="lyrico-lyrics-wrapper">Bulli Buggalunna Thalli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulli Buggalunna Thalli "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Palavelli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Palavelli "/>
</div>
<div class="lyrico-lyrics-wrapper">Indhradhanassumalle Virise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhradhanassumalle Virise "/>
</div>
<div class="lyrico-lyrics-wrapper">Palle Pairagaali Keli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Pairagaali Keli "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanaala Holi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanaala Holi "/>
</div>
<div class="lyrico-lyrics-wrapper">Challagundamantu Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challagundamantu Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Deevenallo Munche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deevenallo Munche "/>
</div>
<div class="lyrico-lyrics-wrapper">Raakasi Gadusu Pillaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Gadusu Pillaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakaasi Saruki Pilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakaasi Saruki Pilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridhi Evaridhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridhi Evaridhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Allari Cheshtala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari Cheshtala "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaadi Aatallo Geliche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaadi Aatallo Geliche "/>
</div>
<div class="lyrico-lyrics-wrapper">Killadi Jinkalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Jinkalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chenguna Chindhadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chenguna Chindhadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Jorugaa Vacchesthundhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorugaa Vacchesthundhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridhi Evaridhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridhi Evaridhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Inti Devatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Inti Devatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Mandharaa Pulatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandharaa Pulatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Urantha Nee Jatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urantha Nee Jatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Jella Chinni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Jella Chinni "/>
</div>
<div class="lyrico-lyrics-wrapper">Sitha Bangaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitha Bangaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Picchuka Chinnari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchuka Chinnari "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandrika Maa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandrika Maa "/>
</div>
<div class="lyrico-lyrics-wrapper">Bujji Gopika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji Gopika "/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkangunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkangunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti Chilaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Chilaka "/>
</div>
</pre>
