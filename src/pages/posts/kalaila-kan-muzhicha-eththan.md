---
title: "kalaila kan muzhicha song lyrics"
album: "Eththan"
artist: "Taj Noor"
lyricist: "Aandal Priyadharshini"
director: "L Suresh"
path: "/albums/eththan-lyrics"
song: "Kalaila Kan Muzhicha"
image: ../../images/albumart/eththan.jpg
date: 2011-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N7EEPQ3aqdQ"
type: "sad"
singers:
  - Vel Murugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaalaiyila kanmuzhichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyila kanmuzhichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kadan koduthavan thorathuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kadan koduthavan thorathuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyila kanmuzhichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyila kanmuzhichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kadan koduthavan thorathuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kadan koduthavan thorathuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">ingoruthan angoruthan enna naalaapakkamum veratturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingoruthan angoruthan enna naalaapakkamum veratturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naanum seiyuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naanum seiyuven"/>
</div>
<div class="lyrico-lyrics-wrapper">indha boomikkulla naan ozhiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha boomikkulla naan ozhiyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">illa kaaththukkulla than karaiyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa kaaththukkulla than karaiyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ammaavoda karuvaraikkulla ozhinjikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ammaavoda karuvaraikkulla ozhinjikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ammaavoda karuvaraikkulla ozhinjikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ammaavoda karuvaraikkulla ozhinjikkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyila kanmuzhichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyila kanmuzhichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kadan koduthavan thorathuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kadan koduthavan thorathuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyila kanmuzhichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyila kanmuzhichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kadan koduthavan thorathuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kadan koduthavan thorathuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kadanaiyellaam theerthupputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadanaiyellaam theerthupputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">eppo oruvaa thingappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppo oruvaa thingappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppo nimmadhiyaa thoongappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppo nimmadhiyaa thoongappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">eppo oruvaa thingappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppo oruvaa thingappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppo nimmadhiyaa thoongappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppo nimmadhiyaa thoongappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppo nimmadhiyaa thoongappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppo nimmadhiyaa thoongappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppo nimmadhiyaa thoongappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppo nimmadhiyaa thoongappoaren"/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppo nimmadhiyaa thoongappoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppo nimmadhiyaa thoongappoaren"/>
</div>
</pre>
