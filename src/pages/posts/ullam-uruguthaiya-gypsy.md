---
title: "ullam uruguthaiya song lyrics"
album: "Gypsy"
artist: "Susheela Raman - Sam Mills"
lyricist: "Andavan Pichai"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Ullam Uruguthaiya"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j5syRVDA4_s"
type: "melody"
singers:
  - Susheela Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullam Urugudhayyaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Urugudhayyaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Urugudhayyaa Muruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Urugudhayyaa Muruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadi Kaangaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadi Kaangaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Urugudhaiyaa Muruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Urugudhaiyaa Muruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadi Kaangaiyile..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadi Kaangaiyile.."/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Anaiththidave..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Anaiththidave.."/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Anaiththidave..ee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Anaiththidave..ee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Anaiththidave Enakkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Anaiththidave Enakkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Asai Perugudhappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asai Perugudhappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruga Ullam Urugudhayyaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruga Ullam Urugudhayyaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadi Paravasamaai Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Paravasamaai Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthida Thonudhayya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthida Thonudhayya.."/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Paravasamai Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Paravasamai Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarththidath Thonudhaiya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarththidath Thonudhaiya.."/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Paravasamaai Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Paravasamaai Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthida Thonudhayya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthida Thonudhayya.."/>
</div>
<div class="lyrico-lyrics-wrapper">Adum Mayileri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adum Mayileri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adum Mayileri..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adum Mayileri.."/>
</div>
<div class="lyrico-lyrics-wrapper">aasai peruguthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai peruguthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Uruguthaiya …
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Uruguthaiya …"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruga Unnadi Kangaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruga Unnadi Kangaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Uruguthaiya …
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Uruguthaiya …"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruga Unnadi Kangaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruga Unnadi Kangaiyile"/>
</div>
</pre>
