---
title: "missing me song lyrics"
album: "Mahaan"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Karthik Subbaraj"
path: "/albums/mahaan-lyrics"
song: "Missing Me"
image: ../../images/albumart/mahaan.jpg
date: 2022-02-10
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Dhruv Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Vanthamar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vanthamar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Muradan Enpaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Muradan Enpaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu En Mudhal Nilai Enpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu En Mudhal Nilai Enpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pookkal Thuranthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pookkal Thuranthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthi Thintru Vandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi Thintru Vandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puligal Ennai Pusippathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puligal Ennai Pusippathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungal Peru Azhivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungal Peru Azhivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Varugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Naane Arasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Naane Arasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Thattum Asuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Thattum Asuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Surukku Kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Surukku Kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Narampugalaal Pinnappattathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Narampugalaal Pinnappattathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aano Penno Sisuvo Sirippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aano Penno Sisuvo Sirippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhanaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arame Kollum Arakkan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arame Kollum Arakkan Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paavangal Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paavangal Marappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Azhithu Marappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Azhithu Marappen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Kalavu Ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Kalavu Ponadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iravu Azhugaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iravu Azhugaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakkiyar Thaaippaal Anaithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkiyar Thaaippaal Anaithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sogam Savakkuzhi Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sogam Savakkuzhi Irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thintru Uyripithen Enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thintru Uyripithen Enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan En Aayutham Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan En Aayutham Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuditha Un Sathai Katrai Nalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuditha Un Sathai Katrai Nalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">There Was Once A Beast Inside Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There Was Once A Beast Inside Me"/>
</div>
<div class="lyrico-lyrics-wrapper">But He Lives Here No More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But He Lives Here No More"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Him, He Is Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Him, He Is Me"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are One Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are One Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I See A Animal Lurking In My Shadows
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I See A Animal Lurking In My Shadows"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t Ask Me Why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Ask Me Why"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Him, He Is Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Him, He Is Me"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are One Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are One Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Kadanthidum Enpaal Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Kadanthidum Enpaal Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithiru Entrum Enpaal Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithiru Entrum Enpaal Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Ethirtha Thalaigalai Kadanthy Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Ethirtha Thalaigalai Kadanthy Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Ketta Sirippu Intru En Ithazhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Ketta Sirippu Intru En Ithazhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kontavar Kanakku Ninaivillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kontavar Kanakku Ninaivillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannitharula Manamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannitharula Manamillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanma Vitru Raatchasi Ontru Vaangi Vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanma Vitru Raatchasi Ontru Vaangi Vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kangal Thintru Paranthu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangal Thintru Paranthu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Ponathu Iyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Ponathu Iyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veesiya வாளால் Yaar Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veesiya வாளால் Yaar Yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethargalo Iyyo Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethargalo Iyyo Haa Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhanthathu Maayum Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhanthathu Maayum Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithathu Karanangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithathu Karanangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kutram Sollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kutram Sollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaalil Mothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaalil Mothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Kaayamura Seithavargalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kaayamura Seithavargalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandippayaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandippayaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandippayaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandippayaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumillaiya Iyyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillaiya Iyyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">There Was Once A Beast Inside Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There Was Once A Beast Inside Me"/>
</div>
<div class="lyrico-lyrics-wrapper">But He Lives Here No More
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But He Lives Here No More"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Him, He Is Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Him, He Is Me"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are One Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are One Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I See A Animal Lurking In My Shadows
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I See A Animal Lurking In My Shadows"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t Ask Me Why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Ask Me Why"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Him, He Is Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Him, He Is Me"/>
</div>
<div class="lyrico-lyrics-wrapper">We Are One Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Are One Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Therkaalil Un Uyirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Therkaalil Un Uyirudan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thottaavil Un Peyarudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thottaavil Un Peyarudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vithi En Chathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vithi En Chathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ganam Un Ranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ganam Un Ranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontru Irandu Moontru Naangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontru Irandu Moontru Naangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisaptham"/>
</div>
</pre>
