---
title: "oxygen song lyrics"
album: "Kavan"
artist: "Hiphop Tamizha"
lyricist: "Kabilan Vairamuthu"
director: "K V Anand"
path: "/albums/kavan-lyrics"
song: "Oxygen"
image: ../../images/albumart/kavan.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7M9hc_PC_Vg"
type: "love"
singers:
  - Hiphop Tamizha
  - Sudarshan Ashok 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oxygen thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnoru pozhudhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru pozhudhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatrai moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatrai moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi ponaai edhanaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum nadhiyil ilaiyai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum nadhiyil ilaiyai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal nagargirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal nagargirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyin meedhu nilavin oliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyin meedhu nilavin oliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaai pozhigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaai pozhigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaabamae enai keerinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaabamae enai keerinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai meghamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai meghamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai aaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai aaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vaasalil suvar aaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaasalil suvar aaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum oru thoondil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum oru thoondil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idavaa thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idavaa thondrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oxygen thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnoru pozhudhinilae ehh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru pozhudhinilae ehh"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatrai moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatrai moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi ponaai edhanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaai edhanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaai edhanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanaalae ohooo hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanaalae ohooo hmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ula ula kalloori mannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ula ula kalloori mannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un theendal ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theendal ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai koiyum thendralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai koiyum thendralaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muyal idai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyal idai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirai neengum podhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai neengum podhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru mogam vandhadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru mogam vandhadhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai cindrellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai cindrellaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaveli vaanam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaveli vaanam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta mugam kanden kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta mugam kanden kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta nadu nenjil nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta nadu nenjil nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yutham idum kadhal konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutham idum kadhal konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam adhu theerndhaal kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam adhu theerndhaal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal adhu vazhum endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal adhu vazhum endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai ne piriyum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai ne piriyum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil kanavai kondren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil kanavai kondren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oxygen thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnoru pozhudhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru pozhudhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">ehhMoochu kaatrai moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ehhMoochu kaatrai moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi ponaai edhanaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaaga nadamaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga nadamaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidivaadham unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidivaadham unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalodum urasaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalodum urasaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanmaanam enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanmaanam enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edai illaa porul alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edai illaa porul alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kaadhal manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kaadhal manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agalaadha oru ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agalaadha oru ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu malaiyin alavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu malaiyin alavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalattra araiyil kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalattra araiyil kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aniyaaya thooram thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyaaya thooram thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayam ariyadhazhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayam ariyadhazhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam ezhudhum sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhayam ezhudhum sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaai thooram nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaai thooram nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyilae baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyilae baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum oru kadhal seiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum oru kadhal seiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil eeram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil eeram illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oxygen thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnoru pozhudhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru pozhudhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaatrai moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatrai moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi ponaai edhanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi ponaai edhanaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum nadhiyil ilaiyai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum nadhiyil ilaiyai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal nagargirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal nagargirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyin meedhu nilavin oliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyin meedhu nilavin oliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaai pozhigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaai pozhigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaabamae enai keerinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaabamae enai keerinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai meghamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai meghamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai aaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai aaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vaasalil suvar aaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaasalil suvar aaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum oru thoondil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum oru thoondil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idavaa thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idavaa thondrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmm mmm"/>
</div>
</pre>
