---
title: "naane indhiran naane chandhiran song lyrics"
album: "Singam"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-lyrics"
song: "Naane Indhiran Naane Chandhiran"
image: ../../images/albumart/singam.jpg
date: 2010-05-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UvTkASGOz_A"
type: "Mass Intro"
singers:
  - Benny Dayal
  - Manicka Vinayagam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalu Kaalu Pachalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kaalu Pachalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Kannu Mechalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kannu Mechalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Thisai Kuchalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thisai Kuchalile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kia Thattugira Osaiyille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kia Thattugira Osaiyille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthivaraan Sulanduvaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthivaraan Sulanduvaraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyalpola Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalpola Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Paanchuvaraan Paranthuvaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Paanchuvaraan Paranthuvaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naane Indhiran Naane Chandhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Indhiran Naane Chandhiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porantha Oorukkula Sooriyani Pol Suthi Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porantha Oorukkula Sooriyani Pol Suthi Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathi Nallavan Meethi Vallavan Motha Vandhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi Nallavan Meethi Vallavan Motha Vandhavana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Mithipen Mutti Udaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Mithipen Mutti Udaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakki Satta Naataama Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Satta Naataama Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithu Pannum Vellai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithu Pannum Vellai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anna Thambi Sandaikku Veenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna Thambi Sandaikku Veenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Case Su Poda Thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Case Su Poda Thevai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerathi Veeran Ellam Eppothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathi Veeran Ellam Eppothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verraappa Thirivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verraappa Thirivathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sollitharava Hey Allividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sollitharava Hey Allividava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sollitharava Hey Allividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sollitharava Hey Allividava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ammavin Kaiyil Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ammavin Kaiyil Soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athil Ulla Rusiye Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Ulla Rusiye Veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Thorum Thinnu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Thorum Thinnu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Aayul Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Aayul Nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Bandhangal Kuda Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bandhangal Kuda Iruntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vantha Thunbangal Thura Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Thunbangal Thura Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamirabharaniyila Moozhgi Kulicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamirabharaniyila Moozhgi Kulicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bharani Aalugira Thembu Kidaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharani Aalugira Thembu Kidaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooroda Irukanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooroda Irukanumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Pola Peroda Irukkanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Peroda Irukkanumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kaththutharavaa Othukidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kaththutharavaa Othukidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththutharavaa Vaa Vaa Othukidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththutharavaa Vaa Vaa Othukidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naane Inthiran Naane Chanthiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Inthiran Naane Chanthiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porantha Oorukkulla Suriyanai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porantha Oorukkulla Suriyanai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sirivarum Kaala Kuda Othungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sirivarum Kaala Kuda Othungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Perai Sonna Vanmuraiyum Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perai Sonna Vanmuraiyum Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallooril Porantha Oru Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallooril Porantha Oru Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kaakki Sattai Pottaa Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kaakki Sattai Pottaa Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karukku Vel Aiyannaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukku Vel Aiyannaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaiyathaan Nikkuraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyathaan Nikkuraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaani Yaarum Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaani Yaarum Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavanga Vidamataaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavanga Vidamataaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Ooril Oru Ketta Palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Ooril Oru Ketta Palakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Kettaalum Alli Kodupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Kettaalum Alli Kodupom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiri Vanthaalum Naangal Mathipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri Vanthaalum Naangal Mathipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethan Nilamaiyilum Melai iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan Nilamaiyilum Melai iruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kula Theivam Aarumugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kula Theivam Aarumugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalukuu Eppothum Aezhumugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukuu Eppothum Aezhumugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendikidava Vetri Tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendikidava Vetri Tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendikidava Vetri Tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendikidava Vetri Tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naane Inthiran Naane Chanthiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Inthiran Naane Chanthiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porantha Oorukkula Sooriyanai Pol Suththi Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porantha Oorukkula Sooriyanai Pol Suththi Varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathi Nallavan Meethi Vallavan Motha Vanthavaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathi Nallavan Meethi Vallavan Motha Vanthavaanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Mithipen Mutti Udaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Mithipen Mutti Udaippen"/>
</div>
</pre>
