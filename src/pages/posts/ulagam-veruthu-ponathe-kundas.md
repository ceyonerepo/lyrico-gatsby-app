---
title: "ulagam veruthu ponathe song lyrics"
album: "Kundas"
artist: "Chandra sathyaraj"
lyricist: "Chellabalu"
director: "RA Ananth"
path: "/albums/kundas-lyrics"
song: "Ulagam Veruthu Ponathe"
image: ../../images/albumart/kundas.jpg
date: 2022-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1qqm8WnbD6A"
type: "mass"
singers:
  - velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ulagam veruthu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam veruthu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam marathu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam marathu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha vazhkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha vazhkai "/>
</div>
<div class="lyrico-lyrics-wrapper">nilai illathathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai illathathe"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilai aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagam veruthu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam veruthu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam marathu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam marathu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha vazhkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha vazhkai "/>
</div>
<div class="lyrico-lyrics-wrapper">nilai illathathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai illathathe"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilai aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pirapum irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirapum irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">irapum irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irapum irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiyinile ivan seyalal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiyinile ivan seyalal"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayamum norunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayamum norunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna than nadanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna than nadanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">uravugal irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravugal irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">manathinile kavalaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathinile kavalaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">alaikave mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaikave mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nan seivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nan seivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai ponathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai polave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal seitha pinnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal seitha pinnale"/>
</div>
<div class="lyrico-lyrics-wrapper">siraiyil maati thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siraiyil maati thavikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagam veruthu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam veruthu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam marathu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam marathu ponathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanum irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanum irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyum irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyum irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulle aaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulle aaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">valigalum irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigalum irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna than sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna than sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam thudithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam thudithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">innalum than theeravilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innalum than theeravilai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamaiyum enaku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamaiyum enaku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbama nan irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbama nan irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">koondodu aluchu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koondodu aluchu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">kovam thalai keri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovam thalai keri"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaiyum seithu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaiyum seithu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">phoenix paravai polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phoenix paravai polave"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagil vaala thonuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagil vaala thonuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagam veruthu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam veruthu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam marathu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam marathu ponathe"/>
</div>
</pre>
