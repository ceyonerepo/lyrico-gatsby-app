---
title: "un idhya song lyrics"
album: "Nisabdham"
artist: "Shawn Jazeel"
lyricist: "Na Muthukumar"
director: "Michael Arun"
path: "/albums/nisabdham-lyrics"
song: "Un Idhya"
image: ../../images/albumart/nisabdham.jpg
date: 2017-03-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/K_gpRYa-PyA"
type: "melody"
singers:
  -	Shawn Jazeel
  - Shakina Shawn
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un idhayathin kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhayathin kural"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyathai keetu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyathai keetu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">irul neekidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul neekidu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam unathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam unathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilavil karai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilavil karai"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavin perilo pilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavin perilo pilai"/>
</div>
<div class="lyrico-lyrics-wrapper">netrai mara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrai mara penne"/>
</div>
<div class="lyrico-lyrics-wrapper">indrai ninai en poo magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrai ninai en poo magale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un vizhi eeramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhi eeramo"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyum illamalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyum illamalo"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pathil sollu penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathil sollu penne"/>
</div>
<div class="lyrico-lyrics-wrapper">un vazhi maarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vazhi maarave"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiya vaanam kaanalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiya vaanam kaanalam"/>
</div>
<div class="lyrico-lyrics-wrapper">netrai mara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrai mara penne"/>
</div>
<div class="lyrico-lyrics-wrapper">indrai ninai en poo magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrai ninai en poo magale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa mothalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa mothalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pathaiyil kal irukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathaiyil kal irukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ver polave nam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ver polave nam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">mothi vellalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi vellalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">devathaiye va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathaiye va va"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irupom"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ennalum en magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennalum en magale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar vaalvilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar vaalvilum"/>
</div>
<div class="lyrico-lyrics-wrapper">vethanai than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanai than "/>
</div>
<div class="lyrico-lyrics-wrapper">nilaikumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaikumo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa naalaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa naalaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">oviyam naam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oviyam naam "/>
</div>
<div class="lyrico-lyrics-wrapper">theetalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">devathaiye vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathaiye vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irupom"/>
</div>
<div class="lyrico-lyrics-wrapper">boomi meethu thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomi meethu thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">ennalum en magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennalum en magale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en uyir uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en uyir uyir "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un idhayathin kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhayathin kural"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyathai keetu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyathai keetu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">irul neekidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul neekidu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam unathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam unathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilavil karai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilavil karai"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavin perilo pilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavin perilo pilai"/>
</div>
<div class="lyrico-lyrics-wrapper">netrai mara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrai mara penne"/>
</div>
<div class="lyrico-lyrics-wrapper">indrai ninai en poo magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrai ninai en poo magale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un vizhi eeramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhi eeramo"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyum illamalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyum illamalo"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pathil sollu penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathil sollu penne"/>
</div>
<div class="lyrico-lyrics-wrapper">un vazhi maarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vazhi maarave"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiya vaanam kaanalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiya vaanam kaanalam"/>
</div>
<div class="lyrico-lyrics-wrapper">netrai mara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netrai mara penne"/>
</div>
<div class="lyrico-lyrics-wrapper">indrai ninai en poo magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrai ninai en poo magale"/>
</div>
</pre>
