---
title: "neee song lyrics"
album: "Yaakkai"
artist: "Yuvan Shankar Raja"
lyricist: "Na Muthukumar"
director: "Kuzhandai Velappan"
path: "/albums/yaakkai-lyrics"
song: "Neee"
image: ../../images/albumart/yaakkai.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s84XNv_ik2w"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neee en kangal naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neee en kangal naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkum dhevadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum dhevadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu naanum vaazha yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu naanum vaazha yenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamal kaadhal thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal kaadhal thaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kangal unnai theduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kangal unnai theduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi polae keeruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi polae keeruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En aaval ellai meeruthaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aaval ellai meeruthaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neee eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pagal kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pagal kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kollum ninaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kollum ninaivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kuzhambugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kuzhambugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru pada padappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pada padappil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan thudi thudippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan thudi thudippil"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam norungugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam norungugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi sitharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sitharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru alai polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru alai polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholilae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En tholilae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayum nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayum nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaagi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaagi pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan pinnae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan pinnae "/>
</div>
<div class="lyrico-lyrics-wrapper">nadakindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidakindren un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidakindren un "/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalai polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalai polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandaal "/>
</div>
<div class="lyrico-lyrics-wrapper">endhan nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naai kutty 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naai kutty "/>
</div>
<div class="lyrico-lyrics-wrapper">pola thaavuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola thaavuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un swasa kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un swasa kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai theendumo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai theendumo "/>
</div>
<div class="lyrico-lyrics-wrapper">ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru siru oonjaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru siru oonjaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu un dhisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu un dhisaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam aaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam aaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam alaipaainthae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam alaipaainthae "/>
</div>
<div class="lyrico-lyrics-wrapper">theduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theduthae hoo oo oo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theduthae hoo oo oo "/>
</div>
<div class="lyrico-lyrics-wrapper">haa hae hae hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haa hae hae hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai jaadai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai jaadai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan jaadai nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan jaadai nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhal thandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhal thandhadhae"/>
</div>
</pre>
