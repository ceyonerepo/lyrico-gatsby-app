---
title: "pandrikku nandri solli song lyrics"
album: "Pandrikku Nandri Solli"
artist: "Suren Vikhash"
lyricist: "Bala Aran"
director: "Bala Aran"
path: "/albums/pandrikku-nandri-solli-lyrics"
song: "Pandrikku nandri Solli"
image: ../../images/albumart/pandrikku-nandri-solli.jpg
date: 2022-02-04
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Maria Kavita Thomas
  - Sofia Ashraf
  - Suren Vikhash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee varupiyaa meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varupiyaa meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottuka konjam beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottuka konjam beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaikeruchu naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaikeruchu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">adikanum more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikanum more"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum bashaiye ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum bashaiye ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">maari pochu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari pochu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">emathitu pona muttal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emathitu pona muttal"/>
</div>
<div class="lyrico-lyrics-wrapper">nu nenachaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu nenachaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pandriku nandri solli kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandriku nandri solli kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">kundrin mel aeri nindral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundrin mel aeri nindral"/>
</div>
<div class="lyrico-lyrics-wrapper">kondrilaiyal thulli vanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondrilaiyal thulli vanthidava"/>
</div>
<div class="lyrico-lyrics-wrapper">koluse kadanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koluse kadanai"/>
</div>
<div class="lyrico-lyrics-wrapper">pandriku nandri solli kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandriku nandri solli kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">kundrin mel aeri nindral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundrin mel aeri nindral"/>
</div>
<div class="lyrico-lyrics-wrapper">kondrilaiyal thulli vanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondrilaiyal thulli vanthidava"/>
</div>
<div class="lyrico-lyrics-wrapper">koluse kadanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koluse kadanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee varupiyaa meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varupiyaa meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottuka konjam beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottuka konjam beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaikeruchu naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaikeruchu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">adikanum more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikanum more"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum bashaiye ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum bashaiye ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">maari pochu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari pochu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">emathitu pona muttal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emathitu pona muttal"/>
</div>
<div class="lyrico-lyrics-wrapper">nu nenachaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu nenachaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasa thedi kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasa thedi kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam pochu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam pochu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudi narachachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudi narachachu"/>
</div>
<div class="lyrico-lyrics-wrapper">neethuku nermai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethuku nermai"/>
</div>
<div class="lyrico-lyrics-wrapper">kathula than pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula than pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vesam mari ipo kalanjanchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesam mari ipo kalanjanchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuruvikum aruvikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvikum aruvikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thuravikkum pallandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuravikkum pallandu"/>
</div>
<div class="lyrico-lyrics-wrapper">manam athu thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam athu thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">pallandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallandu"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan poranthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan poranthan"/>
</div>
<div class="lyrico-lyrics-wrapper">minusan aalanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minusan aalanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">panatha padachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panatha padachan"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu oor koodi par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu oor koodi par"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee varupiyaa meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varupiyaa meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottuka konjam beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottuka konjam beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaikeruchu naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaikeruchu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">adikanum more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikanum more"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum bashaiye ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum bashaiye ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">maari pochu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari pochu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">emathitu pona muttal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emathitu pona muttal"/>
</div>
<div class="lyrico-lyrics-wrapper">nu nenachaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu nenachaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasu kadanaali kadanaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu kadanaali kadanaali"/>
</div>
<div class="lyrico-lyrics-wrapper">aanan muthanaali aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanan muthanaali aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">meeritru kaasa aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeritru kaasa aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">meeritru kaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeritru kaasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee varupiyaa meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varupiyaa meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thottuka konjam beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottuka konjam beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaikeruchu naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaikeruchu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">adikanum more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikanum more"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum bashaiye ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum bashaiye ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">maari pochu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari pochu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">emathitu pona muttal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emathitu pona muttal"/>
</div>
<div class="lyrico-lyrics-wrapper">nu nenachaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu nenachaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaku onnum bayam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku onnum bayam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">payanthu oda villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanthu oda villa"/>
</div>
<div class="lyrico-lyrics-wrapper">kannum kathum nadungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannum kathum nadungala"/>
</div>
<div class="lyrico-lyrics-wrapper">yenna panam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna panam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasa thedi nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasa thedi nan"/>
</div>
<div class="lyrico-lyrics-wrapper">kadala than thanduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala than thanduven"/>
</div>
<div class="lyrico-lyrics-wrapper">yemane thaduthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemane thaduthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku bayam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku bayam illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pandriku nandri solli kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandriku nandri solli kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">kundrin mel aeri nindral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundrin mel aeri nindral"/>
</div>
<div class="lyrico-lyrics-wrapper">kondrilaiyal thulli vanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondrilaiyal thulli vanthidava"/>
</div>
<div class="lyrico-lyrics-wrapper">koluse kadanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koluse kadanai"/>
</div>
</pre>
