---
title: "moosetape intro song lyrics"
album: "Moosetape"
artist: "Wazir Patar"
lyricist: "Sidhu Moose Wala"
director: "Sidhu Moose Wala"
path: "/albums/moosetape-lyrics"
song: "Moosetape Intro"
image: ../../images/albumart/moosetape.jpg
date: 2021-05-15
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/0iscaY9_ecU"
type: "Mass"
singers:
  - Sidhu Moose Wala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lau ji, moose tape sunan aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lau ji, moose tape sunan aale"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare darshakan nu mere ni oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare darshakan nu mere ni oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar bhari sat sri akaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar bhari sat sri akaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na arz ae na vinti ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na arz ae na vinti ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Te na hi guzarish
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te na hi guzarish"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni tan sidhi sidhi warning ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni tan sidhi sidhi warning ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buddhijeevi aapdi buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhijeevi aapdi buddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapdi jeb ch rakhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapdi jeb ch rakhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alochkan di alochana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alochkan di alochana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi taisi mara gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi taisi mara gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Te sabhyacharak shrimaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te sabhyacharak shrimaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapda sabhyachar aapde keele naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapda sabhyachar aapde keele naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bann ke rakhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bann ke rakhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaki naa tan apne ch kise nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaki naa tan apne ch kise nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikhaun da madda ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikhaun da madda ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Te naa hi aapan koyi matt den di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te naa hi aapan koyi matt den di"/>
</div>
<div class="lyrico-lyrics-wrapper">Leeh khinchi hoyi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leeh khinchi hoyi ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh album khaas taur ton te saade vallon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh album khaas taur ton te saade vallon"/>
</div>
<div class="lyrico-lyrics-wrapper">Saade wargeya layin banayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saade wargeya layin banayi ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So thonu saareyan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So thonu saareyan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikko ik chetawani deni chauna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikko ik chetawani deni chauna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bai es album nu na suno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bai es album nu na suno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyon ki is album ch kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon ki is album ch kuch"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiho jeha content paya jaa sakda ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiho jeha content paya jaa sakda ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehda sunne ch thodde kanna nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehda sunne ch thodde kanna nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bilkul changa na lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bilkul changa na lage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So apni doori banayi rakho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So apni doori banayi rakho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyon ki bhaisahab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon ki bhaisahab"/>
</div>
<div class="lyrico-lyrics-wrapper">Sawari apne saman ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sawari apne saman ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aap zimmewar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aap zimmewar hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bye! Moosetape 2021
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye! Moosetape 2021"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan sidhu moose wala!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan sidhu moose wala!"/>
</div>
</pre>
