---
title: "agulu bagulu song lyrics"
album: "100"
artist: "Sam C.S."
lyricist: "Logan"
director: "Sam Anton"
path: "/albums/100-lyrics"
song: "Agulu Bagulu"
image: ../../images/albumart/100.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j-BJqSorKS8"
type: "mass"
singers:
  - Arjun Chandy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakku Pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakku Pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakku Pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakku Pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppila Singam Munadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppila Singam Munadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Gunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Gunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholula Staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholula Staru"/>
</div>
<div class="lyrico-lyrics-wrapper">Gypsyla Roundsu Pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gypsyla Roundsu Pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rowdy Inaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdy Inaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchikarannalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchikarannalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi Kitchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Kitchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja Odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja Odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maassa Kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassa Kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhu Makkalu Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhu Makkalu Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Irukku Evlo Poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Irukku Evlo Poruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olunga Odhungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olunga Odhungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Caseeh Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Caseeh Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Laadathoda Veppen Nalangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laadathoda Veppen Nalangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Neruppu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Neruppu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Theriyum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Theriyum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Therikkum Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Therikkum Thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Rangeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Rangeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Dangeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Dangeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Aavaan Bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Aavaan Bejaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamul Vangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamul Vangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalum Naan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalum Naan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayamunna Dharmamunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayamunna Dharmamunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Munne Nippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munne Nippen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thimura Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thimura Iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Odi Oliyum Aala Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Odi Oliyum Aala Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Adippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Adippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavara Thaduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavara Thaduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thiruntha Solli Thirunthalenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thiruntha Solli Thirunthalenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya Mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya Mudippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Modhuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Modhuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinna Perndhudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinna Perndhudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Remand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Remand"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Thookkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Thookkuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouser Kizhinjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouser Kizhinjidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Pesuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Pesuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Stylela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stylela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dressa Poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dressa Poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Thattu Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Thattu Thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police Ellarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Ellarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa Illeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa Illeenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Orderah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orderah Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Thallum Police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Thallum Police"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Illeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Illeenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuuundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuuundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La La Locku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La Locku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulu Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulu Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hookke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hookke"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Laadu Labakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laadu Labakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evana Irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana Irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">La Laa Lockuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La Laa Lockuu"/>
</div>
</pre>
