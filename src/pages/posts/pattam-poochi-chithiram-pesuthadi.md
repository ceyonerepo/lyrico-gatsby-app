---
title: "pattam poochi song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Kabilan Vairamuthu"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Pattam Poochi"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cM94Quyrk2w"
type: "love"
singers:
  - Timmy
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna aachuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna aachuda"/>
</div>
<div class="lyrico-lyrics-wrapper">eethu aachuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eethu aachuda"/>
</div>
<div class="lyrico-lyrics-wrapper">uthu paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkunu aachu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkunu aachu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattam poochi pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">chitu kuruviyin rekkai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitu kuruviyin rekkai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnil parapene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnil parapene"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thane kadhal thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thane kadhal thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ava vantha enaku thodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava vantha enaku thodha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannale kalavu senjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale kalavu senjale"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja siluvaiyil enna aranjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja siluvaiyil enna aranjale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">machan pechuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan pechuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ulta aachuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulta aachuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kadhal vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kadhal vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moochu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">otrai thingal mugamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai thingal mugamo"/>
</div>
<div class="lyrico-lyrics-wrapper">thumbai pookal nagamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thumbai pookal nagamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai katrai koonthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai katrai koonthal"/>
</div>
<div class="lyrico-lyrics-wrapper">katti potale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti potale"/>
</div>
<div class="lyrico-lyrics-wrapper">yayin neeyum yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yayin neeyum yaro"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai unthi yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai unthi yaro"/>
</div>
<div class="lyrico-lyrics-wrapper">sembula neerai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sembula neerai "/>
</div>
<div class="lyrico-lyrics-wrapper">ontrai kalanthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ontrai kalanthome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnoda semmoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda semmoli"/>
</div>
<div class="lyrico-lyrics-wrapper">enna onnum puriyalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna onnum puriyalada"/>
</div>
<div class="lyrico-lyrics-wrapper">nammoda local basaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammoda local basaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnal ok da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnal ok da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">salpeta parvai yale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salpeta parvai yale"/>
</div>
<div class="lyrico-lyrics-wrapper">kalpu ethi ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalpu ethi ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">meen padi vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meen padi vandiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">en jodi ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en jodi ponale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattam poochi pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">chitu kuruviyin rekkai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitu kuruviyin rekkai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnil parapene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnil parapene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuppu mancha mudiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu mancha mudiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">chine tee pol colour oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chine tee pol colour oh"/>
</div>
<div class="lyrico-lyrics-wrapper">ava konda pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava konda pota"/>
</div>
<div class="lyrico-lyrics-wrapper">sunda kanjiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunda kanjiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">makiskaran malaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makiskaran malaro"/>
</div>
1000 <div class="lyrico-lyrics-wrapper">light nilavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="light nilavo"/>
</div>
<div class="lyrico-lyrics-wrapper">ava vantha nagarin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava vantha nagarin"/>
</div>
<div class="lyrico-lyrics-wrapper">artham kowsow oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham kowsow oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiyayo intha loval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo intha loval"/>
</div>
<div class="lyrico-lyrics-wrapper">romba over da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba over da"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku antha sutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku antha sutha "/>
</div>
<div class="lyrico-lyrics-wrapper">thamile romba thevala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamile romba thevala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hikoo vin 3m vari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hikoo vin 3m vari"/>
</div>
<div class="lyrico-lyrics-wrapper">en kadhal aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kadhal aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">tholkapiyam noorpa vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholkapiyam noorpa vena"/>
</div>
<div class="lyrico-lyrics-wrapper">en kadhal vaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kadhal vaalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattam poochi pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">chitu kuruviyin rekkai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitu kuruviyin rekkai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnil parapene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnil parapene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thane kadhal thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thane kadhal thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ava vantha enaku thodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava vantha enaku thodha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannale kalavu senjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale kalavu senjale"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja siluvaiyil enna aranjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja siluvaiyil enna aranjale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattam poochi pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">chitu kuruviyin rekkai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitu kuruviyin rekkai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnil parapene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnil parapene"/>
</div>
</pre>
