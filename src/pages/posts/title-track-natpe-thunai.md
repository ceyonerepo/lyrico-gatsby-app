---
title: "title track song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Dr. Vadugam Sivakumar"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Title Track"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xUmy7luCVvA"
type: "title track"
singers:
  - Sanjith Hegde
  - Kaushik Krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oli tharum sooriyan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli tharum sooriyan "/>
</div>
<div class="lyrico-lyrics-wrapper">uranga sendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uranga sendral"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirinam valvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirinam valvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">valigalai thangidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigalai thangidum "/>
</div>
<div class="lyrico-lyrics-wrapper">muyarchi indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muyarchi indri"/>
</div>
<div class="lyrico-lyrics-wrapper">valkaiyil vetri illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valkaiyil vetri illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba unai"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaada machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada machan vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidiyum varai kathirunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyum varai kathirunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">porumaiyil thiramai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porumaiyil thiramai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyaluke velicham tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyaluke velicham tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">natpukoru pirivum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukoru pirivum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi matham parpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi matham parpathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai natpu thorpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai natpu thorpathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thunai varum tholan nirkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai varum tholan nirkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathil kavanam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathil kavanam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudintha varai matum muyarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudintha varai matum muyarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">seithal sathanai nadapathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seithal sathanai nadapathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">muditha varai muyandru vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muditha varai muyandru vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba sathika thadaiyum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba sathika thadaiyum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilunthal vithaiyai viluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthal vithaiyai viluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">eluvome maramai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluvome maramai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">malai tharum antha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai tharum antha "/>
</div>
<div class="lyrico-lyrics-wrapper">megam than un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam than un"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba unai"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaada machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada machan vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natpe thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpe thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba unai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba unai "/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaada machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada machan vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engo piranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo piranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai inainthom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai inainthom "/>
</div>
<div class="lyrico-lyrics-wrapper">inban thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inban thunbam"/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai inaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai inaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sila kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">chella kurumbugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella kurumbugal"/>
</div>
<div class="lyrico-lyrics-wrapper">nam uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu varavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu varavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sila kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">chella kurumbugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella kurumbugal"/>
</div>
<div class="lyrico-lyrics-wrapper">nam uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu varavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu varavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">mudintha varai muyandru vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudintha varai muyandru vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">asathiku thadaiyum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asathiku thadaiyum illai"/>
</div>
</pre>
