---
title: "un paarvai song lyrics"
album: "Arasakulam"
artist: "Velan Sakadevan"
lyricist: "Kavignar Nalangilli"
director: "Kumaramaran"
path: "/albums/arasakulam-lyrics"
song: "Un Paarvai"
image: ../../images/albumart/arasakulam.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zUKKZOYbISM"
type: "love"
singers:
  - Velan Sagadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">gara gara sa garasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara gara sa garasa"/>
</div>
<div class="lyrico-lyrics-wrapper">gara gara sa garasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara gara sa garasa"/>
</div>
<div class="lyrico-lyrics-wrapper">gara garasa sarigama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara garasa sarigama"/>
</div>
<div class="lyrico-lyrics-wrapper">gara gara sa garasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara gara sa garasa"/>
</div>
<div class="lyrico-lyrics-wrapper">gara gara sa garasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara gara sa garasa"/>
</div>
<div class="lyrico-lyrics-wrapper">gara garasa sarigama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara garasa sarigama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvai en mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai en mele"/>
</div>
<div class="lyrico-lyrics-wrapper">vilugira neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilugira neram"/>
</div>
<div class="lyrico-lyrics-wrapper">alagiya neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya neram"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuralil en peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuralil en peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">solgira kalam adai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solgira kalam adai"/>
</div>
<div class="lyrico-lyrics-wrapper">malai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai naane muluthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai naane muluthai"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaithen athu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaithen athu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nane ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nane ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasithen athu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasithen athu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">iru vizhiyal isaipavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru vizhiyal isaipavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ithal valiyai siripavalao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithal valiyai siripavalao"/>
</div>
<div class="lyrico-lyrics-wrapper">iruthaya araigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthaya araigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu mugam ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu mugam ini"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan alagu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan alagu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvai en mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai en mele"/>
</div>
<div class="lyrico-lyrics-wrapper">vilugira neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilugira neram"/>
</div>
<div class="lyrico-lyrics-wrapper">alagiya neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya neram"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuralil en peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuralil en peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">solgira kalam adai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solgira kalam adai"/>
</div>
<div class="lyrico-lyrics-wrapper">malai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadakura poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakura poova"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikira meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikira meena"/>
</div>
<div class="lyrico-lyrics-wrapper">enna neeye mayakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna neeye mayakura"/>
</div>
<div class="lyrico-lyrics-wrapper">ada enna neeye mayakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada enna neeye mayakura"/>
</div>
<div class="lyrico-lyrics-wrapper">pesura maana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesura maana"/>
</div>
<div class="lyrico-lyrics-wrapper">aadura kuyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadura kuyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vitha nee than katura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitha nee than katura"/>
</div>
<div class="lyrico-lyrics-wrapper">ada vitha nee than katura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vitha nee than katura"/>
</div>
<div class="lyrico-lyrics-wrapper">puthi ketu poyi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthi ketu poyi than"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum unna theduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum unna theduren"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir otikitu vaala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir otikitu vaala than"/>
</div>
<div class="lyrico-lyrics-wrapper">mandi ittu kekuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandi ittu kekuren"/>
</div>
<div class="lyrico-lyrics-wrapper">ava sirichutu ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava sirichutu ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">ava puthusa poranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava puthusa poranthene"/>
</div>
<div class="lyrico-lyrics-wrapper">enna morachutu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna morachutu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinnal varuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinnal varuven di"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinnal varuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinnal varuven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kulikira nathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulikira nathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthikira nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthikira nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">enna neeye miratura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna neeye miratura"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo enna neeye miratura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo enna neeye miratura"/>
</div>
<div class="lyrico-lyrics-wrapper">parakura muyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakura muyala"/>
</div>
<div class="lyrico-lyrics-wrapper">padura marama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padura marama"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja nee than kilikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja nee than kilikura"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenja nee than kilikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenja nee than kilikura"/>
</div>
<div class="lyrico-lyrics-wrapper">kanu muzhi kulla nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanu muzhi kulla nan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vachu pathukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vachu pathukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenju kuli kulla naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenju kuli kulla naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vachu sumakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vachu sumakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">adi usura parichaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi usura parichaye"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kiruka alanjen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kiruka alanjen di"/>
</div>
<div class="lyrico-lyrics-wrapper">nee velagi ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee velagi ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un urava varuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un urava varuven di"/>
</div>
<div class="lyrico-lyrics-wrapper">un urava varuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un urava varuven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvai en mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai en mele"/>
</div>
<div class="lyrico-lyrics-wrapper">vilugira neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilugira neram"/>
</div>
<div class="lyrico-lyrics-wrapper">alagiya neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya neram"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuralil en peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuralil en peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">solgira kalam adai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solgira kalam adai"/>
</div>
<div class="lyrico-lyrics-wrapper">malai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai kaalam"/>
</div>
</pre>
