---
title: "mama mama song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Mani Amudhavan"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Mama Mama"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-8VI8HgyVrQ"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dakkan dakkan koduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkan dakkan koduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Dain dakkan thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dain dakkan thariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunu dakkan variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunu dakkan variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey dain dakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dain dakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama mama mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama mama thangala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama thangala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava veedu enga thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava veedu enga thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Start achi love moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start achi love moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vaati thala neeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaati thala neeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai partha antha beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai partha antha beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha aanen kaana ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha aanen kaana ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti pidikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti pidikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam kudukanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam kudukanum mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandu pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti pidikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti pidikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam kudukanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam kudukanum mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnonna eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnonna eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan thinnuriyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan thinnuriyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelava soap-ah than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelava soap-ah than"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pottu kulichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pottu kulichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi minnuriyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi minnuriyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi enna aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enna aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naanthan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naanthan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thiruppi thanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thiruppi thanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan venam enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan venam enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kuda serthikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kuda serthikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pora pokkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pora pokkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu onnu podhum di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu onnu podhum di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver ethum kekalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver ethum kekalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya pudikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pudikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti pidikanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti pidikanum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam kudukanum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam kudukanum mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valathu kaala vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathu kaala vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo nee varaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo nee varaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma veetukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma veetukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo vittu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo vittu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu anji masam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu anji masam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla thethi illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla thethi illaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingayae ippothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingayae ippothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa kattikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kattikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarikkum pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarikkum pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa sollikelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa sollikelaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un odambu thanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un odambu thanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbu thollaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbu thollaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa pethu vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pethu vaazhalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ettu pullaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ettu pullaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama mama mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama mama thangala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama thangala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava veedu enga thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava veedu enga thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Start achi love moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start achi love moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vaati thala neeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaati thala neeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai partha antha beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai partha antha beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha aanen kaana ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha aanen kaana ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mapillai parichai pinni irukinghlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapillai parichai pinni irukinghlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedal eduthirikken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedal eduthirikken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduren da map-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduren da map-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thukkuren da maapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thukkuren da maapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman athula sharpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman athula sharpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku than da paapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku than da paapu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy happy mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy happy mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Very happy mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very happy mama"/>
</div>
</pre>
