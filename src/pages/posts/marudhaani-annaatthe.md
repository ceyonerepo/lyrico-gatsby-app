---
title: "marudhaani song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Mani Amuthavan"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Marudhaani"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8gm4jqB9KW8"
type: "happy"
singers:
  - Nakash Aziz
  - Anthony Daasan
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maana Madhuraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana Madhuraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman Kudhiraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Kudhiraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Kondu Vaaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Kondu Vaaraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenaa Minungaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaa Minungaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Minni Sinungaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minni Sinungaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Melangotta Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melangotta Poraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Malandhu Malandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Malandhu Malandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anichangolundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anichangolundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanakkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanakkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maappula Maappula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappula Maappula "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholoda Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholoda Aada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valandhu Valandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valandhu Valandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Somandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Somandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minukkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minukkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththala Veththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththala Veththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkoda Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkoda Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjana Anjaname Vizhi Poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjana Anjaname Vizhi Poosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjunnu Kojuthamma Valaiyosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjunnu Kojuthamma Valaiyosai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magaraani Sirippu Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraani Sirippu Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Medai Nenappu Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Medai Nenappu Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bommi Nadandhuvaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommi Nadandhuvaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummi Adinga Jorraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummi Adinga Jorraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali Soodapporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali Soodapporaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammi Mithikkapporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammi Mithikkapporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba Vithaikka Poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba Vithaikka Poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayi Veettu Seeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayi Veettu Seeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thavila Adikka Naayanam Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thavila Adikka Naayanam Olikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Nerungathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Nerungathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atchathai Atchathai Unmela Thoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchathai Atchathai Unmela Thoova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verala Pudika Veratham Mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verala Pudika Veratham Mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarandh Therunchukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarandh Therunchukkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Angana Ingana Nee Thulli Thaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angana Ingana Nee Thulli Thaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkara Akkaraiyaa Iruppaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkara Akkaraiyaa Iruppaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkara Sakkaraiya Inippaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkara Sakkaraiya Inippaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magaraani Sirippu Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraani Sirippu Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Medai Nenappu Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Medai Nenappu Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannuthaan Vedhai Onnudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannuthaan Vedhai Onnudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Nooraa Maathi Neettumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Nooraa Maathi Neettumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuthaan Enga Ponnudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuthaan Enga Ponnudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Veetta Yethi Kaattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Veetta Yethi Kaattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettaiyilla Kuttaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettaiyilla Kuttaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Karaiyum Neraagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Karaiyum Neraagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum Mazhai Kottaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Mazhai Kottaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangi Pudikkum Aaraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangi Pudikkum Aaraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Perusunnu Ennaatha Vittuthallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Perusunnu Ennaatha Vittuthallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Enanje Ninnaathaan Vaanavillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Enanje Ninnaathaan Vaanavillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanum Ponnum Onnukkonnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum Ponnum Onnukkonnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthu Vaazha Venungannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthu Vaazha Venungannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullatha Ullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullatha Ullatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallatha Nallatha Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallatha Nallatha Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magaraani Sirippu Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraani Sirippu Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Medai Nenappu Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Medai Nenappu Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magaraani Sirippu Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaraani Sirippu Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhaani Sevappu Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani Sevappu Sevappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Medai Nenappu Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Medai Nenappu Nenappu"/>
</div>
</pre>
