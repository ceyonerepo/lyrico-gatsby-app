---
title: "gundelo savvadi song lyrics"
album: "Krishna Rao Super Market"
artist: "Bhole Shavali"
lyricist: "Kandikonda"
director: "Sreenath Pulakuram"
path: "/albums/krishna-rao-super-market-lyrics"
song: "Gundelo Savvadi"
image: ../../images/albumart/krishna-rao-super-market.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0uxLnciyKfU"
type: "love"
singers:
  - Hemachandra
  - Amritha Nayak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">i am your love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your love"/>
</div>
<div class="lyrico-lyrics-wrapper">you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">your are my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your are my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">beat everytinghhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beat everytinghhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundelo savvadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo savvadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pedavipai mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedavipai mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">thenelaa nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenelaa nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukai raalene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukai raalene"/>
</div>
<div class="lyrico-lyrics-wrapper">aashalaa velluve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aashalaa velluve"/>
</div>
<div class="lyrico-lyrics-wrapper">shwasagaa maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shwasagaa maarene"/>
</div>
<div class="lyrico-lyrics-wrapper">neejethe cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neejethe cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">parugulu theesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugulu theesele"/>
</div>
<div class="lyrico-lyrics-wrapper">naaloni prema prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaloni prema prema"/>
</div>
<div class="lyrico-lyrics-wrapper">prema sarigamalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prema sarigamalai "/>
</div>
<div class="lyrico-lyrics-wrapper">nee paina vaali poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paina vaali poola"/>
</div>
<div class="lyrico-lyrics-wrapper">jallu kurisele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallu kurisele "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam vechi vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam vechi vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">vechi naa kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi naa kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne choosaaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne choosaaka "/>
</div>
<div class="lyrico-lyrics-wrapper">naalo edi theliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo edi theliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi modalaindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi modalaindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundelo savvadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo savvadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pedavipai mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedavipai mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">thenelaa nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenelaa nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukai raalene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukai raalene"/>
</div>
<div class="lyrico-lyrics-wrapper">aashalaa velluve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aashalaa velluve"/>
</div>
<div class="lyrico-lyrics-wrapper">shwasagaa maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shwasagaa maarene"/>
</div>
<div class="lyrico-lyrics-wrapper">neejethe cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neejethe cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">parugulu theesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugulu theesele"/>
</div>
<div class="lyrico-lyrics-wrapper">you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">i am your love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanandam nanu adigindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanandam nanu adigindi"/>
</div>
<div class="lyrico-lyrics-wrapper">naatho jatha untaanantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatho jatha untaanantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve thodaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve thodaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">aanandamtho pani emundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanandamtho pani emundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adbutham nanu adigindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adbutham nanu adigindi"/>
</div>
<div class="lyrico-lyrics-wrapper">andaalu choopisthanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andaalu choopisthanani"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve prapamcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve prapamcham"/>
</div>
<div class="lyrico-lyrics-wrapper">ayinaaka inkemundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayinaaka inkemundi"/>
</div>
<div class="lyrico-lyrics-wrapper">kale idi kaaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kale idi kaaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">ade nijamaindigaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ade nijamaindigaah"/>
</div>
<div class="lyrico-lyrics-wrapper">nammalenanthagaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammalenanthagaah"/>
</div>
<div class="lyrico-lyrics-wrapper">gilli choosaaka kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gilli choosaaka kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakaadidi nijamenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakaadidi nijamenani"/>
</div>
<div class="lyrico-lyrics-wrapper">thelindi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelindi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naaloni prema prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaloni prema prema"/>
</div>
<div class="lyrico-lyrics-wrapper">prema sarigamalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prema sarigamalai "/>
</div>
<div class="lyrico-lyrics-wrapper">nee paina vaali poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paina vaali poola"/>
</div>
<div class="lyrico-lyrics-wrapper">jallu kurisele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallu kurisele "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam vechi vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam vechi vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">vechi naa kanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi naa kanule"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne choosaaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne choosaaka "/>
</div>
<div class="lyrico-lyrics-wrapper">naalo edi theliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo edi theliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi modalaindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi modalaindi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundelo savvadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo savvadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pedavipai mogene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedavipai mogene"/>
</div>
<div class="lyrico-lyrics-wrapper">thenelaa nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenelaa nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukai raalene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukai raalene"/>
</div>
<div class="lyrico-lyrics-wrapper">aashalaa velluve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aashalaa velluve"/>
</div>
<div class="lyrico-lyrics-wrapper">shwasagaa maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shwasagaa maarene"/>
</div>
<div class="lyrico-lyrics-wrapper">neejethe cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neejethe cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">parugulu theesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugulu theesele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">i am your love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am your love"/>
</div>
<div class="lyrico-lyrics-wrapper">you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">your are my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="your are my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">beat everytinghhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beat everytinghhh"/>
</div>
</pre>
