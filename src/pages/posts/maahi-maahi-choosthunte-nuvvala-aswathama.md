---
title: "maahi maahi song lyrics"
album: "Aswathama"
artist: "Sricharan Pakala"
lyricist: "Kasarla Shyam"
director: "Ramana Teja"
path: "/albums/aswathama-lyrics"
song: "Maahi Maahi Choosthunte Nuvvula"
image: ../../images/albumart/aswathama.jpg
date: 2020-01-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yZhzipYB04c"
type: "happy"
singers:
  - Poojan Kohli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choosthunte Nuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunte Nuvvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhala Bommala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhala Bommala"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunte Nuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunte Nuvvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhala Bommala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhala Bommala"/>
</div>
<div class="lyrico-lyrics-wrapper">Velu Patti Nadichinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velu Patti Nadichinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee Annatho Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Annatho Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallallo Kaanthitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Kaanthitho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Aasatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Aasatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggupadutoo Butta Bommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggupadutoo Butta Bommai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edigaave Inthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edigaave Inthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Andhari Oopirai Perigave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Andhari Oopirai Perigave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Allari Ika Nene Cheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Allari Ika Nene Cheyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli Koduku Venaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli Koduku Venaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharani Nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharani Nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Attharinti Dhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attharinti Dhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Yedu Ganaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Yedu Ganaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Manasule Okatayye Velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Manasule Okatayye Velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapana Ee Jantane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapana Ee Jantane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Theliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Theliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Nizamauthundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Nizamauthundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelapana Ee Kshanamune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelapana Ee Kshanamune"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedani Needanu Nenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedani Needanu Nenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Musi Musi Nee Navvulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Musi Nee Navvulake"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduga Nestham thaanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduga Nestham thaanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Praanam Dhooramai Veluthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Praanam Dhooramai Veluthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Pranami Brathike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Pranami Brathike"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantha Dhorikene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantha Dhorikene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli Koduku Venaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli Koduku Venaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharani Nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharani Nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadindinthanaka Dintha Dinthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadindinthanaka Dintha Dinthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Attharinti Dhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attharinti Dhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Yedu Ganaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Yedu Ganaka"/>
</div>
</pre>
