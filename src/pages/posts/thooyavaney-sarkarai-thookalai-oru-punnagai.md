---
title: "thooyavaney song lyrics"
album: "Sarkarai Thookalai Oru Punnagai"
artist: "Rajesh Appukuttan"
lyricist: "Kattalai Jeya"
director: "Mahesh Padmanabhan "
path: "/albums/sarkarai-thookalai-oru-punnagai-lyrics"
song: "Thooyavaney"
image: ../../images/albumart/sarkarai-thookalai-oru-punnagai.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nQ4yt6o5zWE"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thooyavaney thooyavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavaney thooyavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enatharuge ulagai nee sirithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enatharuge ulagai nee sirithai"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavaney thooyavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavaney thooyavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu uyirai methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu uyirai methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee parithaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee parithaari"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ethaiyo paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ethaiyo paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unaiye paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unaiye paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ethaiyo paarkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ethaiyo paarkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">nee enai paarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enai paarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">poovadaiyai nee veesinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovadaiyai nee veesinai"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaadiye ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaadiye ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">nerodaiyai nee pesinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerodaiyai nee pesinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nan moolgiye ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan moolgiye ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nerungugirai thirumpugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerungugirai thirumpugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">inaithidum pothu virumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaithidum pothu virumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nuniyal nan urasugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuniyal nan urasugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">urasidum pothu mayangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasidum pothu mayangugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viliyinile vilunguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyinile vilunguren"/>
</div>
<div class="lyrico-lyrics-wrapper">vilungidum pothu naluvugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilungidum pothu naluvugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">virumbidava pirinthadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virumbidava pirinthadava"/>
</div>
<div class="lyrico-lyrics-wrapper">pirinthidum pothu virumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirinthidum pothu virumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavaley thooyavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavaley thooyavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enatharuge alagai nee sirithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enatharuge alagai nee sirithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inmel neeya ini nan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inmel neeya ini nan than"/>
</div>
<div class="lyrico-lyrics-wrapper">thina paarthal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thina paarthal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiye ninaikum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiye ninaikum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">paaram nee sirithal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaram nee sirithal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal nan thinam parithavithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal nan thinam parithavithen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale manam kothi kothithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale manam kothi kothithen"/>
</div>
<div class="lyrico-lyrics-wrapper">kannale nan unai valaithu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale nan unai valaithu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan pinnale nee alaiya vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan pinnale nee alaiya vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai virumbugirai vilagugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai virumbugirai vilagugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagidum pothu virumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagidum pothu virumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nuniyal urasugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuniyal urasugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">urasidum pothu mayangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasidum pothu mayangugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavaley thooyavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavaley thooyavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enatharuge alagai nee sirithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enatharuge alagai nee sirithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sarugai nee vilunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarugai nee vilunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaatrai aerpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaatrai aerpen"/>
</div>
<div class="lyrico-lyrics-wrapper">arugil neyum irunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugil neyum irunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">nan viralai korpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan viralai korpen"/>
</div>
<div class="lyrico-lyrics-wrapper">vidamal nan unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidamal nan unai"/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">padamal nan unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padamal nan unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">kedamal nan ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedamal nan ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">pidi vidamal nan unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidi vidamal nan unai"/>
</div>
<div class="lyrico-lyrics-wrapper">karam pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karam pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nerugugiren vilagugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nerugugiren vilagugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagidum pothu nerungugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagidum pothu nerungugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nuniyal urasugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuniyal urasugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">urasidum pothu mayangugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasidum pothu mayangugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">thooyavaney thooyavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooyavaney thooyavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enatharuge alagai nee sirithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enatharuge alagai nee sirithai"/>
</div>
<div class="lyrico-lyrics-wrapper">iniyavaley iniyavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniyavaley iniyavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu uyirai methuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu uyirai methuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee parithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee parithai"/>
</div>
</pre>
