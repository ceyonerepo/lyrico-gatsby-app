---
title: "eiffel mele yeri ninnu song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Eiffel Mele Yeri Ninnu"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j3hiobM8C7c"
type: "Love"
singers:
  - Ranjith
  - Suchithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eiffel Mele Yeri Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eiffel Mele Yeri Ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siluthu Kinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluthu Kinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Sirikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Sirikuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Enna Kootikinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Enna Kootikinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silent Uga Vanathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silent Uga Vanathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakhuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakhuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarimunai Theruvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarimunai Theruvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandha Paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandha Paya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parisula Kaadhal Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisula Kaadhal Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhakurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhakurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai Thamizh Kavignanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Thamizh Kavignanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedandha Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedandha Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French Ippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirichutha Meiyurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichutha Meiyurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate in Thithipula Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate in Thithipula Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Liquor Coffee Kasapula Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liquor Coffee Kasapula Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Kulla Kanavuga Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kulla Kanavuga Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudida Vandhaan Kaadhal Vaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudida Vandhaan Kaadhal Vaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Venna Poove Venna Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Venna Poove Venna Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Rottiyila Ottikina Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Rottiyila Ottikina Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olli Theeve Olli Theeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olli Theeve Olli Theeve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thanniyinnu Pannikinna Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thanniyinnu Pannikinna Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Un Kadhal Alainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Un Kadhal Alainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enmel Adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enmel Adikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjellam Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjellam Theeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Oththa Muththathukke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Oththa Muththathukke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piththam Thalaikkeruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piththam Thalaikkeruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Thala Venumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Thala Venumunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyama Thonudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Thonudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solati Vitta Bambaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solati Vitta Bambaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthuthadi Kalatti Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthuthadi Kalatti Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhiraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhiraiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikku Kettu Aliyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku Kettu Aliyuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavuga Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavuga Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudida Vandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudida Vandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Vaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vaadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eiffel Mela Yerri Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eiffel Mela Yerri Ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siluththu Kinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluththu Kinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Sirikuthaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Sirikuthaey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootti Vacha Manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti Vacha Manasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadi Nee Thorandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Nee Thorandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadi Yeri Mandai Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi Yeri Mandai Kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutha Mutha Chediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutha Mutha Chediyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadi Poo Paricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Poo Paricha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhirnthatha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirnthatha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanu Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanu Pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maman Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maman Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekka Chakka Vithai Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka Chakka Vithai Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Vara Onnu Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Vara Onnu Onna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Erakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ponnukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ponnukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eka Chakka Bodhai Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eka Chakka Bodhai Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum Ini Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Ini Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane Sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Sarakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Midukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Midukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maeni Minukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maeni Minukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Otha Muththathukke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Otha Muththathukke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ptham Thalaikkeruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ptham Thalaikkeruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Thala Venumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Thala Venumunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyama Thonudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Thonudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solati Vitta  Bambaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solati Vitta  Bambaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthuthadi Kalatti Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthuthadi Kalatti Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhiraiya Thikku Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhiraiya Thikku Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eiffel Mela Yeri Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eiffel Mela Yeri Ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siluthu Kittu En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluthu Kittu En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Rekka Maati Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Rekka Maati Kittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paanju Antha Vaanaththula Parakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Antha Vaanaththula Parakuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Louvre Monalisa Siripirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louvre Monalisa Siripirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lover Kannil Ivaloda Siripirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover Kannil Ivaloda Siripirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paris Idhu Kaadhalukku Thalainagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paris Idhu Kaadhalukku Thalainagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathu Sollu Jodi Namakaaru Nigaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Sollu Jodi Namakaaru Nigaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Louvre Monalisa Siripirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louvre Monalisa Siripirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lover Kannil Ivaloda Siripirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lover Kannil Ivaloda Siripirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paris Idhu Kaadhalukku Thalainagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paris Idhu Kaadhalukku Thalainagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathu Sollu Jodi Namakaaru Nigaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Sollu Jodi Namakaaru Nigaru"/>
</div>
</pre>
