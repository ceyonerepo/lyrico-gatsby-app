---
title: "koothadikudhu koothadikudhu song lyrics"
album: "Azhagumagan"
artist: "James Vasanthan"
lyricist: "yugabharathi"
director: "Azhagan Selva"
path: "/albums/azhagumagan-lyrics"
song: "Koothadikudhu Koothadikudhu"
image: ../../images/albumart/azhagumagan.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dKroMb2GoKA"
type: "happy"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">koothadikuthu koothadikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadikuthu koothadikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">galagalakura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galagalakura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">inga pethedukuthu pethedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga pethedukuthu pethedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiru kiru karagattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiru kiru karagattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu adikuthu kathu adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu adikuthu kathu adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kidu kidu puyalattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu puyalattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mazhai mazhai adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mazhai mazhai adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilu kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanikku theriyaatha settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanikku theriyaatha settai"/>
</div>
<div class="lyrico-lyrics-wrapper">aiayaiyo eralam than da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiayaiyo eralam than da"/>
</div>
<div class="lyrico-lyrics-wrapper">sol pecha ketkatha ragala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol pecha ketkatha ragala"/>
</div>
<div class="lyrico-lyrics-wrapper">ep epothum thagaraaru than da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ep epothum thagaraaru than da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothadikuthu koothadikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadikuthu koothadikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">galagalakura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galagalakura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">inga pethedukuthu pethedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga pethedukuthu pethedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiru kiru karagattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiru kiru karagattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu adikuthu kathu adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu adikuthu kathu adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kidu kidu puyalattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu puyalattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mazhai mazhai adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mazhai mazhai adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilu kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenavattu pechula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenavattu pechula than"/>
</div>
<div class="lyrico-lyrics-wrapper">alaparaiya katuvainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaparaiya katuvainga"/>
</div>
<div class="lyrico-lyrics-wrapper">panju mittai prachainaike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju mittai prachainaike"/>
</div>
<div class="lyrico-lyrics-wrapper">panjayatha kootuvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjayatha kootuvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">arasa mara thittu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasa mara thittu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu puli aaduvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu puli aaduvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">appanaiye thala mulugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appanaiye thala mulugi"/>
</div>
<div class="lyrico-lyrics-wrapper">alukku theera kulippainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alukku theera kulippainga"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanga vaartha ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanga vaartha ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">vachukittu veliyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachukittu veliyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivainga paarvai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivainga paarvai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pala ooru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala ooru "/>
</div>
<div class="lyrico-lyrics-wrapper">vithuputtu pasi aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithuputtu pasi aarum"/>
</div>
<div class="lyrico-lyrics-wrapper">thagaraaru pannatha naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaraaru pannatha naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivainga varalaaril irukanu kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivainga varalaaril irukanu kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayangal thalumbaagum munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayangal thalumbaagum munne"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu kaayangal uruvaagum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu kaayangal uruvaagum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothadikuthu koothadikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadikuthu koothadikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">galagalakura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galagalakura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">inga pethedukuthu pethedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga pethedukuthu pethedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiru kiru karagattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiru kiru karagattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu adikuthu kathu adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu adikuthu kathu adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kidu kidu puyalattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu puyalattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mazhai mazhai adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mazhai mazhai adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilu kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanikku theriyaatha settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanikku theriyaatha settai"/>
</div>
<div class="lyrico-lyrics-wrapper">aiayaiyo eralam than da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiayaiyo eralam than da"/>
</div>
<div class="lyrico-lyrics-wrapper">sol pecha ketkatha ragala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol pecha ketkatha ragala"/>
</div>
<div class="lyrico-lyrics-wrapper">ep epothum thagaraaru than da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ep epothum thagaraaru than da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothadikuthu koothadikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadikuthu koothadikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">galagalakura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galagalakura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">inga pethedukuthu pethedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga pethedukuthu pethedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiru kiru karagattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiru kiru karagattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu adikuthu kathu adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu adikuthu kathu adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kidu kidu puyalattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu puyalattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mazhai mazhai adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mazhai mazhai adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilu kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">asalooru vambai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asalooru vambai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">adaikaathu porippainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaikaathu porippainga"/>
</div>
<div class="lyrico-lyrics-wrapper">porambokka pesikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porambokka pesikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">membokka vaaluvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="membokka vaaluvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">sandaiyila mooku udanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandaiyila mooku udanju"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha thaanam pannuvainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha thaanam pannuvainga"/>
</div>
<div class="lyrico-lyrics-wrapper">thanda sotha thinnu puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanda sotha thinnu puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti nyaayam pesuvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti nyaayam pesuvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">ivainga nallavana kettavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivainga nallavana kettavana"/>
</div>
<div class="lyrico-lyrics-wrapper">aavainga mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavainga mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivainga alaparaiya alanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivainga alaparaiya alanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paaka alavukole kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaka alavukole kedaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">panam kaasa epothum ivainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam kaasa epothum ivainga"/>
</div>
<div class="lyrico-lyrics-wrapper">packetil semichathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="packetil semichathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthana epothum ivainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthana epothum ivainga"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaiku thadai potathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaiku thadai potathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothadikuthu koothadikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadikuthu koothadikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">galagalakura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galagalakura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">inga pethedukuthu pethedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga pethedukuthu pethedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiru kiru karagattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiru kiru karagattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kathu adikuthu kathu adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu adikuthu kathu adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kidu kidu puyalattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu puyalattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mazhai mazhai adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mazhai mazhai adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kilu kilu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilu kilu kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanikku theriyaatha settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanikku theriyaatha settai"/>
</div>
<div class="lyrico-lyrics-wrapper">aiayaiyo eralam than da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiayaiyo eralam than da"/>
</div>
<div class="lyrico-lyrics-wrapper">sol pecha ketkatha ragala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol pecha ketkatha ragala"/>
</div>
<div class="lyrico-lyrics-wrapper">ep epothum thagaraaru than da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ep epothum thagaraaru than da "/>
</div>
</pre>
