---
title: "maasama song lyrics"
album: "Engaeyum Eppothum"
artist: "C Sathya"
lyricist: "M Saravanan"
director: "M. Saravanan"
path: "/albums/engaeyum-eppothum-lyrics"
song: "Maasama"
image: ../../images/albumart/engaeyum-eppothum.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JcFWFYR20NE"
type: "love"
singers:
  - Sathya
  - Aalap Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maasamaaaru maasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasamaaaru maasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi thavichenae poongodikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi thavichenae poongodikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaramasila pala vaarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaramasila pala vaarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathukadanthenae poovizhikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathukadanthenae poovizhikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kann orangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann orangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevi madukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi madukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi edukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi edukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai sirikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai sirikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai kodukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai kodukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal nadakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal nadakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha veruppula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha veruppula"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maasama maasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maasama maasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi thavichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasamaaaru maasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasamaaaru maasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi thavichenae poongodikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi thavichenae poongodikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roattula paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roattula paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkula paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkula paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bus-ula paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus-ula paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto-la paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto-la paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Street-ula paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Street-ula paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathathu ellaam tholaivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathu ellaam tholaivula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattula nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattula nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mettula nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mettula nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaeyum nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaeyum nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingaeyum nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingaeyum nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaeyum nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaeyum nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkala nikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkala nikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnathu avaloda manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnathu avaloda manasula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnaalo paathaalo theruvila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnaalo paathaalo theruvila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paakaama ponenae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paakaama ponenae "/>
</div>
<div class="lyrico-lyrics-wrapper">mudhalulaNaa yengi thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhalulaNaa yengi thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha poongodikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha poongodikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasamaaaru maasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasamaaaru maasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathukadanthenae poovizhikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathukadanthenae poovizhikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Number vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Number vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Phonum pannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phonum pannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Address vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Address vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Letterum kodukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letterum kodukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow pannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow pannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothu anuppala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothu anuppala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi vanthaa nerila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi vanthaa nerila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kindalum pannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindalum pannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiyum podala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyum podala"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachu paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachu paakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichu pesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichu pesala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maraikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maraikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyya pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyya pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi vizhuntha kaadhalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi vizhuntha kaadhalula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava moochaagi ponaalae uyirila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava moochaagi ponaalae uyirila"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku matchaagi vittaalae life-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku matchaagi vittaalae life-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yengi thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yengi thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha poongodikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha poongodikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasama aaru maasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasama aaru maasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosama mosama kaadhalichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosama mosama kaadhalichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kaadhalichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kaadhalichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kann orangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann orangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevi madukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevi madukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi edukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi edukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai sirikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai sirikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho ho ho ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosama mosama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosama mosama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kaadhalichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kaadhalichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
</pre>
