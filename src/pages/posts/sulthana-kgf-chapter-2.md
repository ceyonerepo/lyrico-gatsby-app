---
title: "sulthana song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Madhurakavi"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Sulthana"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aUFalkzF99Q"
type: "mass"
singers:
  - Deepak Blue
  - Govind Prasad
  - Yogisekar
  - Mohan Krishna
  - Santhosh Venky
  - Sachin Basrur
  - Ravi Basrur
  - Puneeth Rudranag
  - Manish Dinakar
  - Vaish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi kottum Aagrosa soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi kottum Aagrosa soora"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara narambu theritherukum Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara narambu theritherukum Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodinaattum gambheera soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodinaattum gambheera soora"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai yettum muttum padai dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai yettum muttum padai dheera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karunjaambal kakki rana kang 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunjaambal kakki rana kang "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum sina veppa vengaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum sina veppa vengaye"/>
</div>
<div class="lyrico-lyrics-wrapper">karam veesi thaakki chira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karam veesi thaakki chira "/>
</div>
<div class="lyrico-lyrics-wrapper">vettayaadum mara yuttha aazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettayaadum mara yuttha aazhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharani eriyithu thaka thaka venave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani eriyithu thaka thaka venave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai yaai mugil veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai yaai mugil veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaigal nenjil amdi kidakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaigal nenjil amdi kidakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">achcham theetthaai adhikaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achcham theetthaai adhikaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sultana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sultana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padapadakum rana pokalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakum rana pokalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadala sudala vanaatane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadala sudala vanaatane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada sadakum lagappalayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada sadakum lagappalayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadiya kodiya sinataane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadiya kodiya sinataane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yutha kalathil yeri kaththi poriyeesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutha kalathil yeri kaththi poriyeesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathakkalariyile nitham laipaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathakkalariyile nitham laipaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha vanjagarai kothi kodaluruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha vanjagarai kothi kodaluruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Satha Pokkum oru suthamaakudane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satha Pokkum oru suthamaakudane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavvi vidum katti viriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvi vidum katti viriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaarame konda neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaarame konda neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagam adhita yellum routhirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagam adhita yellum routhirane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri vechi kudharividuvaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri vechi kudharividuvaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharani eriyithu thaka thaka venave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani eriyithu thaka thaka venave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai yaai mugil veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai yaai mugil veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaigal nenjil amdi kidakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaigal nenjil amdi kidakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">achcham theetthaai adhikaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achcham theetthaai adhikaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi kottum Aagrosa soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi kottum Aagrosa soora"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara narambu theritherukum Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara narambu theritherukum Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodinaattum gambheera soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodinaattum gambheera soora"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai yettum muttum padai dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai yettum muttum padai dheera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karunjaambal kakki rana kang 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunjaambal kakki rana kang "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum sina veppa vengaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum sina veppa vengaye"/>
</div>
<div class="lyrico-lyrics-wrapper">karam veesi thaakki chira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karam veesi thaakki chira "/>
</div>
<div class="lyrico-lyrics-wrapper">vettayaadum mara yuttha aazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettayaadum mara yuttha aazhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharani eriyithu thaka thaka venave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani eriyithu thaka thaka venave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai yaai mugil veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai yaai mugil veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaigal nenjil amdi kidakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaigal nenjil amdi kidakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">achcham theetthaai adhikaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achcham theetthaai adhikaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sultana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sultana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
</pre>
