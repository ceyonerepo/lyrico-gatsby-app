---
title: "mogam chinna song lyrics"
album: "Evanukku Engeyo Matcham Irukku"
artist: "Natarajan Sankaran"
lyricist: "Viveka"
director: "A R Mukesh"
path: "/albums/evanukku-engeyo-matcham-irukku-lyrics"
song: "Mogam Chinna"
image: ../../images/albumart/evanukku-engeyo-matcham-irukku.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0J29THfa5kM"
type: "love"
singers:
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kichu kichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram mootti mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram mootti mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikittu podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikittu podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thatti thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thatti thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodethi konjam mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodethi konjam mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallikittu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallikittu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu thaan kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thaan kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mogam chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogam chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil thogai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam ooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam ooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagam konda en nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam konda en nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam thanjam aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam thanjam aanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadam karka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnalae nirkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnalae nirkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli siruvanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli siruvanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal paadam nee karka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal paadam nee karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ennai thirudinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu ennai thirudinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan oru pen thaan enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru pen thaan enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee udan nindraal vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee udan nindraal vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan oru pen thaan enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru pen thaan enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee udan nindraal vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee udan nindraal vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae ohoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae ohoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadham muthal utchi varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham muthal utchi varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal ooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal ooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil ulla ovvoru anuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil ulla ovvoru anuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per kooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un per kooruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meedham vaithidaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedham vaithidaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvom intha vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvom intha vazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pothum endra sollae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pothum endra sollae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal oorilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oorilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai muttri en nenjam pattri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai muttri en nenjam pattri"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai eriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai eriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam indru oyaamal kenji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam indru oyaamal kenji"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ketkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan oru pen thaan enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru pen thaan enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee udan nindraal vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee udan nindraal vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru pen thaan enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru pen thaan enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee udan nindraal vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee udan nindraal vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthae poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthae poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram mootti mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram mootti mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikittu podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikittu podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thatti thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thatti thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodethi konjam mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodethi konjam mella"/>
</div>
</pre>