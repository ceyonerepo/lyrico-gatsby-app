---
title: "rangamma mangamma song lyrics"
album: "Rangasthalam"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/rangasthalam-lyrics"
song: "Rangamma Mangamma"
image: ../../images/albumart/rangasthalam.jpg
date: 2022-06-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GWWHI7SE0KQ"
type: "love"
singers:
  -	M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oy Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oy Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oy Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oy Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattinchunkodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattinchunkodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattinchunkodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattinchunkodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gollabhama Vachiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gollabhama Vachiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Goru Gilluthunte Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Goru Gilluthunte Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Gollabhama Vachiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gollabhama Vachiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Goru Gilluthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Goru Gilluthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Cheema Kutti Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Cheema Kutti Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Saluputhunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Saluputhunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Uffammaa Uffammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uffammaa Uffammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Oodhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Oodhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utthamaatakaina Nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utthamaatakaina Nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukobettadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukobettadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uffammaa Uffammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uffammaa Uffammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Oodhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Oodhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utthamaatakaina Nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utthamaatakaina Nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukobettadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukobettadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Picchi Picchi Oosulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi Picchi Oosulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Munigi Theluthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munigi Theluthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichipoyi Mirapakaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichipoyi Mirapakaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikinaanante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikinaanante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mantammaa Mantammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantammaa Mantammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante Soodadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante Soodadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Neellaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Neellaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethikiyyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethikiyyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mantammaa Mantammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantammaa Mantammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ante Soodadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ante Soodadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Neellaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Neellaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethikiyyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethikiyyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattinchunkodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattinchunkodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Raama Silakammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Raama Silakammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Regi Pandu Koduthunte Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regi Pandu Koduthunte Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Regipandu Gujju Vacchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regipandu Gujju Vacchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kothaga Suttukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothaga Suttukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Raika Meeda Paduthunte Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raika Meeda Paduthunte Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Raama Silakammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Raama Silakammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Regi Pandu Kodithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regi Pandu Kodithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Regipandu Gujju Naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regipandu Gujju Naa "/>
</div>
<div class="lyrico-lyrics-wrapper">Raika Meeda Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raika Meeda Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakammaa Marakammaa Ante Soodadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakammaa Marakammaa Ante Soodadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaru Raikaina Thecchi Iyyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaru Raikaina Thecchi Iyyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakammaa Marakammaa Antu Soodadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakammaa Marakammaa Antu Soodadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaru Raikaina Thecchi Iyyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaru Raikaina Thecchi Iyyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa Pattinchukodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa Pattinchukodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Andamantha Moota Gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andamantha Moota Gatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Kandi Senuke Yelithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Kandi Senuke Yelithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kandireegalocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kandireegalocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Eeda Gucchi Nannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Eeda Gucchi Nannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Muduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Muduthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Andamantha Moota Gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andamantha Moota Gatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandi Senuke Yelithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandi Senuke Yelithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandireegalocchi Nannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandireegalocchi Nannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Muduthunte Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Muduthunte Ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ushhammaa Ushhammaa Antu Tholadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushhammaa Ushhammaa Antu Tholadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulakadu Palakadu Banda Ramudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulakadu Palakadu Banda Ramudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ushhammaa Ushhammaa Antu Tholadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushhammaa Ushhammaa Antu Tholadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulakadu Palakadu Banda Ramudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulakadu Palakadu Banda Ramudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangammaa Mangammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangammaa Mangammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa Pattinchukodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa Pattinchukodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Rangammaa Mangammaa Em Pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rangammaa Mangammaa Em Pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane Untaadammaa Pattinchukodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane Untaadammaa Pattinchukodu"/>
</div>
</pre>
