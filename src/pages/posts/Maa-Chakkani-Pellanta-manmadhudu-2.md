---
title: "Maa Chakkani Pellanta song lyrics"
album: "Manmadhudu 2"
artist: "Chaitan Bharadwaj"
lyricist: "Kittu Vissapragada"
director: "Rahul Ravindran"
path: "/albums/manmadhudu-2-lyrics"
song: "Maa Chakkani Pellanta"
image: ../../images/albumart/manmadhudu-2.jpg
date: 2019-08-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vhxDuHvHs_I"
type: "happy"
singers:
  - Anurag Kulkarni
  - Chinmayi
  - Deepthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maa Chakkani Pellanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Chakkani Pellanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchataina Janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchataina Janta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulake Vaibhogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulake Vaibhogame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamaneeyamaayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamaneeyamaayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam Kalyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Kalyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaste Aape Veelunda Kalyanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaste Aape Veelunda Kalyanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Epudo Annarandi Lokam Motham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudo Annarandi Lokam Motham "/>
</div>
<div class="lyrico-lyrics-wrapper">Bomme Ayina Natakam Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomme Ayina Natakam Idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Munde Raasesuntada Swargam Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde Raasesuntada Swargam Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijame Nammalandi Artham Partham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijame Nammalandi Artham Partham"/>
</div>
<div class="lyrico-lyrics-wrapper">Lene Leni Jeevitham Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lene Leni Jeevitham Idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Peru Chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Peru Chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Aaraa Teesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Aaraa Teesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanyadanam Chese 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanyadanam Chese "/>
</div>
<div class="lyrico-lyrics-wrapper">Daare Choodala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daare Choodala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hadavide Ida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hadavide Ida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarijodu Kaduthunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarijodu Kaduthunnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarada Hey Modale Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarada Hey Modale Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli Laggam Kudire Vela Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli Laggam Kudire Vela Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Choodalantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Choodalantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paradaa Hey Jaripe Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paradaa Hey Jaripe Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Paduthunnaru Gola Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Paduthunnaru Gola Lo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karchuku Venakadoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karchuku Venakadoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Muchata Karuvavvoddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Muchata Karuvavvoddu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Prathi Chinna Panilona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Prathi Chinna Panilona "/>
</div>
<div class="lyrico-lyrics-wrapper">Dabulake Poye Golantaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabulake Poye Golantaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodalaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodalaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Ante Bandhuvukoche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Ante Bandhuvukoche"/>
</div>
<div class="lyrico-lyrics-wrapper">Teerani Anumaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teerani Anumaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetakaram Mamakaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetakaram Mamakaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Teluginti Pellilo Husharu Pongelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teluginti Pellilo Husharu Pongelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarada Hey Modale Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarada Hey Modale Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli Laggam Kudire Vela Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli Laggam Kudire Vela Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Choodalantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Choodalantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paradaa Hey Jaripe Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paradaa Hey Jaripe Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Paduthunnaru Gola Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Paduthunnaru Gola Lo"/>
</div>
</pre>
