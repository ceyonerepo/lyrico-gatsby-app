---
title: "osiliye sarakku song lyrics"
album: "Pillaiyar Theru Kadaisi Veedu"
artist: "Chakri"
lyricist: "Thirumalai Kishore"
director: "Thirumalai Kishore"
path: "/albums/pillaiyar-theru-kadaisi-veedu-lyrics"
song: "Osiliye Sarakku"
image: ../../images/albumart/pillaiyar-theru-kadaisi-veedu.jpg
date: 2011-06-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aDmbYYOzoNk"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oosile Sarakkadichi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosile Sarakkadichi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ohonnu Irundhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohonnu Irundhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Oruthanukkum Adangaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oruthanukkum Adangaama "/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthalaiyaa Thirindhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuthalaiyaa Thirindhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil Therinjum Kelvikkettaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil Therinjum Kelvikkettaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Virathamanasa Polanthu Poattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virathamanasa Polanthu Poattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathala Sollapoaren Paattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathala Sollapoaren Paattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhunga Ezhuthudaa Barotaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhunga Ezhuthudaa Barotaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Palikkanum Ava Kedaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Palikkanum Ava Kedaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraandu Naan Kalakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu Naan Kalakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoadi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoadi Neethaannu Kandupidichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoadi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoadi Neethaannu Kandupidichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosile Sarakkadichi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosile Sarakkadichi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ohonnu Irundhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohonnu Irundhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Oruthanukkum Adangaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oruthanukkum Adangaama "/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthalaiyaa Thirindhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuthalaiyaa Thirindhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Englisula Therinjathellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englisula Therinjathellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">My Name Is Ganesan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Name Is Ganesan Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthu Kooda Pallikkoodam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu Kooda Pallikkoodam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha Aalu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha Aalu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthalvaraiyaa Sollivittaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvaraiyaa Sollivittaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaikkaathu Velaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkaathu Velaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchippanni Thirunthidalaannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchippanni Thirunthidalaannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangaatha Nanbangathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangaatha Nanbangathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Kattalanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Kattalanna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasikku Pøyiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasikku Pøyiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasiyunthaan Maasuppadum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasiyunthaan Maasuppadum "/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaala Yøsikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaala Yøsikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
<div class="lyrico-lyrics-wrapper">enjøadi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enjøadi Neethaannu Kandupidichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
È<div class="lyrico-lyrics-wrapper">njøadi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="njøadi Neethaannu Kandupidichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenanthøarum Thirunaalaagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenanthøarum Thirunaalaagum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unkøøda Naa Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkøøda Naa Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Køundamani Thevaiyilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køundamani Thevaiyilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakalanu Nee Širikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalanu Nee Širikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyilae Thavikkavidaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyilae Thavikkavidaama "/>
</div>
<div class="lyrico-lyrics-wrapper">Veettøda Naan Iruppaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettøda Naan Iruppaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthumaiyilum Kønjiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumaiyilum Kønjiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mudivuvarai Thaangiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mudivuvarai Thaangiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla Ullathellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla Ullathellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oralavu Šøllivittaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oralavu Šøllivittaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraagathaan Nenaichirukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraagathaan Nenaichirukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Marukkaathadi Èn Kaathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukkaathadi Èn Kaathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
È<div class="lyrico-lyrics-wrapper">njøadi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="njøadi Neethaannu Kandupidichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyilum Kannumuzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyilum Kannumuzhichen"/>
</div>
È<div class="lyrico-lyrics-wrapper">n Paathi Neethaannu Kandupidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="n Paathi Neethaannu Kandupidichen"/>
</div>
</pre>
