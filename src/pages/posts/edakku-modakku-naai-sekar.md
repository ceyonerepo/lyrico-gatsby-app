---
title: "edakku modakku song lyrics"
album: "Naai Sekar"
artist: "Ajesh - Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Kishore Rajkumar"
path: "/albums/naai-sekar-lyrics"
song: "Edakku Modakku"
image: ../../images/albumart/naai-sekar.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j2jGPmPPFys"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edakku modakka life aye thirupivachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku modakka life aye thirupivachan"/>
</div>
<div class="lyrico-lyrics-wrapper">Left handil fateayae ezhudhivachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left handil fateayae ezhudhivachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edakku modakka life aye thirupivachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku modakka life aye thirupivachan"/>
</div>
<div class="lyrico-lyrics-wrapper">Left handil fateayae ezhudhivachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left handil fateayae ezhudhivachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullu kulla azhugura na jokeru machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullu kulla azhugura na jokeru machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila sirikira naai sekar uh machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila sirikira naai sekar uh machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nambi vandha care take ru machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nambi vandha care take ru machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum nanri maravaa naai sekaru macham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum nanri maravaa naai sekaru macham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola kola kolakirean naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola kola kolakirean naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavva kavva thudikirean naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavva kavva thudikirean naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambathula kaala thookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathula kaala thookum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shame uh shame uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shame uh shame uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Puppy shame uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puppy shame uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kanda therikirean naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kanda therikirean naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora kanda sirikirean naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora kanda sirikirean naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru fulla damage aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru fulla damage aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Name uh name uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Name uh name uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad uh name uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad uh name uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lovealey dog ah suthunaven mathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovealey dog ah suthunaven mathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dogaley love uh setta chu machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dogaley love uh setta chu machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Darkavey irukum paava patta life la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darkavey irukum paava patta life la"/>
</div>
<div class="lyrico-lyrics-wrapper">Sparkavey avalum vandhaley machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sparkavey avalum vandhaley machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu paaka pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu paaka pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaduchutean avaloda naina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduchutean avaloda naina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo adhanaal avalum enna vena sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo adhanaal avalum enna vena sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Odan jutten sukku noora naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odan jutten sukku noora naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullu kulla azhugura na jokeru machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullu kulla azhugura na jokeru machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila sirikira naai sekar uh machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila sirikira naai sekar uh machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna nambi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nambi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Care take ru machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Care take ru machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum nanri maravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum nanri maravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naai sekaru macham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naai sekaru macham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola kola kolakirean naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola kola kolakirean naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavva kavva thudikirean naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavva kavva thudikirean naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambathula kaala thookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathula kaala thookum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shame uh shame uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shame uh shame uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Puppy shame uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puppy shame uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kanda therikirean naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kanda therikirean naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora kanda sirikirean naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora kanda sirikirean naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru fulla damage aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru fulla damage aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Name uh name uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Name uh name uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad uh name uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad uh name uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edakku modakka life aye thirupivachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku modakka life aye thirupivachan"/>
</div>
<div class="lyrico-lyrics-wrapper">Left handil fateayae ezhudhivachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left handil fateayae ezhudhivachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edakku modakka life aye thirupivachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku modakka life aye thirupivachan"/>
</div>
<div class="lyrico-lyrics-wrapper">Left handil fateayae ezhudhivachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left handil fateayae ezhudhivachaan"/>
</div>
</pre>
