---
title: "vanavillum song lyrics"
album: "Nil Gavani Sellathey"
artist: "Selvaganesh"
lyricist: "J Francis Kriba"
director: "Anand Chakravarthy"
path: "/albums/nil-gavani-sellathey-lyrics"
song: "Vanavillum"
image: ../../images/albumart/nil-gavani-sellathey.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yOoVOgEdLYU"
type: "mass"
singers:
  - Karthik
  - Premji Amaren
  - Brodha V
  - Kalpana
  - Sri Madhumitha	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanavillum Naanalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillum Naanalaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Nooru Koodippoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Nooru Koodippoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Engum Vaasalaachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Engum Vaasalaachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkaloadu Peru Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkaloadu Peru Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Poanakaalam Poaye Poachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poanakaalam Poaye Poachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Kaalam Raasiyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Kaalam Raasiyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soagamellaam Gaaliyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soagamellaam Gaaliyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Loagam Thedu Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Loagam Thedu Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadaa Paattu Ondru Paadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa Paattu Ondru Paadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Aattam Poadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Aattam Poadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Kaatraikatti Poadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaatraikatti Poadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatraaga Naamum Maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraaga Naamum Maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiyum Maalaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyum Maalaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaelaiye Jaalithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaelaiye Jaalithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalibathil Thaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibathil Thaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalibathil Thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibathil Thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvirukku Thoazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvirukku Thoazha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvirukku Thoazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvirukku Thoazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumvarai Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumvarai Roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhiduvoam Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhiduvoam Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillum Naanalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillum Naanalaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Nooru Koodippoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Nooru Koodippoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Engum Vaasalaachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Engum Vaasalaachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkaloadu Peru Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkaloadu Peru Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Poanakaalam Poaye Poachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poanakaalam Poaye Poachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Kaalam Raasiyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Kaalam Raasiyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soagamellaam Gaaliyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soagamellaam Gaaliyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Loagam Thedu Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Loagam Thedu Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeppaadum Raagamenna Thaalamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeppaadum Raagamenna Thaalamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Poagum Thooramenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Poagum Thooramenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Pesum Vaarthai Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Pesum Vaarthai Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Osai Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Osai Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethodum Vaasal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethodum Vaasal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaang Tadaang Tadaang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaang Tadaang Tadaang"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Boomiyin Thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Boomiyin Thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrilum Koodu Kattum Thevadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilum Koodu Kattum Thevadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasaiyin Vaasalin Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasaiyin Vaasalin Aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sooriyanai Pattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sooriyanai Pattam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidum Thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidum Thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal Pesinaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Pesinaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigal Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigal Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalibathil Thaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibathil Thaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalibathil Thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibathil Thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvirukku Thoazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvirukku Thoazha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvirukku Thoazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvirukku Thoazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumvarai Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumvarai Roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhiduvoam Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhiduvoam Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Odivaa Vilaiyaadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Odivaa Vilaiyaadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaaney Ennoda Bablimaas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaaney Ennoda Bablimaas"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana Naannaa Naanannaa Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Naannaa Naanannaa Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesavaa Pesavaa Ennoada Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesavaa Pesavaa Ennoada Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchaikodi Kaattava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchaikodi Kaattava"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorgoalam Poagalaam Megamaagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorgoalam Poagalaam Megamaagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerkkoalam Poadalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerkkoalam Poadalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Koadi Minnal Angey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Koadi Minnal Angey"/>
</div>
<div class="lyrico-lyrics-wrapper">Orappaarvai Ingey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orappaarvai Ingey "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoalsaayum Kaalam Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoalsaayum Kaalam Engey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanavin Maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanavin Maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochilum Thooral Poadum Theeyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochilum Thooral Poadum Theeyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvaiyin Saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvaiyin Saalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattippoadum Vanna Vanna Peralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattippoadum Vanna Vanna Peralai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koadaiyai Aadaiyaai Ho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koadaiyai Aadaiyaai Ho "/>
</div>
<div class="lyrico-lyrics-wrapper">O Jaadaiyil Maatrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Jaadaiyil Maatrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillum Naanalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillum Naanalaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Nooru Koodippoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Nooru Koodippoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Engum Vaasalaachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Engum Vaasalaachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkaloadu Peru Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkaloadu Peru Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Poanakaalam Poaye Poachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poanakaalam Poaye Poachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Kaalam Raasiyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Kaalam Raasiyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soagamellaam Gaaliyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soagamellaam Gaaliyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Loagam Thedu Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Loagam Thedu Nanba"/>
</div>
</pre>
