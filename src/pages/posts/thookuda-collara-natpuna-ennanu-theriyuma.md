---
title: "thookuda collara song lyrics"
album: "Natpuna Ennanu Theriyuma"
artist: "Dharan"
lyricist: "Jeyachandra Hashmi"
director: "Shiva Arvind"
path: "/albums/natpuna-ennanu-theriyuma-lyrics"
song: "Thookuda Collara"
image: ../../images/albumart/natpuna-ennanu-theriyuma.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cg0u0fie1B8"
type: "happy"
singers:
  - Dr. Burn
  - Mark Anthony Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thookuda Collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookuda Collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjudhu Yezhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjudhu Yezhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyatum Sevippara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyatum Sevippara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappuda Alappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappuda Alappara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookuda Collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookuda Collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjudhu Yezhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjudhu Yezhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyatum Sevippara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyatum Sevippara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappuda Alappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappuda Alappara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevanume Madhikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevanume Madhikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachadhu Nadakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachadhu Nadakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiya Niruthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiya Niruthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethana Yethana Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethana Yethana Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ovvonna Nadakume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ovvonna Nadakume"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirthu Nikka Erangita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirthu Nikka Erangita"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir Kaatha Kaalgal Kadakume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir Kaatha Kaalgal Kadakume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Haa Hoo Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haa Hoo Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Daa Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Daa Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodra Thodra Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodra Thodra Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Avlo Dhan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Avlo Dhan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichaiyam Velvom Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichaiyam Velvom Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhaiyam Enkitta Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaiyam Enkitta Venda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnadha Naanga Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadha Naanga Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnavan Moonjila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnavan Moonjila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kari Poosuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari Poosuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarra Nagarra Nagarra Nagarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarra Nagarra Nagarra Nagarra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Varatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Varatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vida Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vida Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vida Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vida Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyardhavan Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyardhavan Iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Mel Enna Keezh Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Mel Enna Keezh Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalum Varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalum Varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkaye Thunaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaye Thunaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaram Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaram Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum Keezhum Suzhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Keezhum Suzhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Yaaruku Dhaan Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Yaaruku Dhaan Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu Pattaya Kelapuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu Pattaya Kelapuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Veikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Veikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Step-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Step-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooran Kaadhala Ooti Valakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooran Kaadhala Ooti Valakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukka Nenachu Kurukka Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka Nenachu Kurukka Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda Kulla Pori Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Kulla Pori Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asinga Paduthi Sirichavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asinga Paduthi Sirichavana"/>
</div>
<div class="lyrico-lyrics-wrapper">List-u Pottu Vechurukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="List-u Pottu Vechurukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aravanachu Ninnavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aravanachu Ninnavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Thachurukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Thachurukom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittum Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittum Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamba Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamba Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhaika Povan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhaika Povan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambi Neeyum Vaaipu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi Neeyum Vaaipu Kudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Kuduthu Uzhachu Povan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Kuduthu Uzhachu Povan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu Pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiya Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiya Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichu Povan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichu Povan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookuda Collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookuda Collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjudhu Yezhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjudhu Yezhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyatum Sevippara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyatum Sevippara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappuda Alappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappuda Alappara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevanume Madhikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevanume Madhikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachadhu Nadakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachadhu Nadakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiya Niruthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiya Niruthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiya Niruthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiya Niruthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhama Nindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhama Nindravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Padama Sendravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Padama Sendravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorakama Vendravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorakama Vendravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Nee Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Nee Vaa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Nee Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Nee Vaa Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Nee Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Nee Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodra Thodra Thodra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodra Thodra Thodra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Vaa Da Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarra Nagarra Nagarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarra Nagarra Nagarra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Vaa Da Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Paatha Evanum Tholanjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Paatha Evanum Tholanjan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi Paaka Thuninjadhevan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi Paaka Thuninjadhevan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Paaka Nenachadhevan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaka Nenachadhevan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaya Thorandhu Kaathurukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaya Thorandhu Kaathurukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Da Vandhu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da Vandhu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumtha Lakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumtha Lakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
</pre>
