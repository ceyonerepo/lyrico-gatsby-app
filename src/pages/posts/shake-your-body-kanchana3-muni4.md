---
title: "shake your body song lyrics"
album: "Kanchana3-Muni4"
artist: "S. Thaman"
lyricist: "	Jesse Samuel"
director: "Raghava Lawrence"
path: "/albums/kanchana3-muni4-lyrics"
song: "Shake Your Body"
image: ../../images/albumart/kanchana3-muni4.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wblQZmDYomA"
type: "Love"
singers:
  - Jesse Samuel
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vetka Padatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vetka Padatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikki Mukki Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikki Mukki Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sokka Vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sokka Vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vetka Padatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vetka Padatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikki Mukki Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikki Mukki Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sokka Vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sokka Vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Strawberry Penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strawberry Penney"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Kannai Paavthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kannai Paavthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pejaranenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pejaranenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Cadbery Chocolate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cadbery Chocolate"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Poneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Poneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paarvathathun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarvathathun"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-La Semma Beat Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-La Semma Beat Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Lubbu Tubbu Lubbu Tubbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Lubbu Tubbu Lubbu Tubbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthadicha enna thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthadicha enna thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Shake Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Shake Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vetka Padatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vetka Padatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikki Mukki Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikki Mukki Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sokka Vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sokka Vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kuyile En Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kuyile En Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutha Aatam Podendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutha Aatam Podendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Veliye Kathaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Veliye Kathaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham Senthu Adendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham Senthu Adendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teddy Bear Ah Honey Bunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teddy Bear Ah Honey Bunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Rendum Nanthana??
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Rendum Nanthana??"/>
</div>
<div class="lyrico-lyrics-wrapper">My Darling Ah Ur Sweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Darling Ah Ur Sweet"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Ethana Onnu Sollendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Ethana Onnu Sollendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Parthanalle Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Parthanalle Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Fixu Panniteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fixu Panniteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirulla Vara Unnodathandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirulla Vara Unnodathandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kalyanamum Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kalyanamum Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kathirikavum Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kathirikavum Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Love-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Love-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum Panna Podum Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum Panna Podum Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paarvathathun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarvathathun"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-La Semma Beat Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-La Semma Beat Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Lubbu Tubbu Lubbu Tubbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Lubbu Tubbu Lubbu Tubbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthadicha enna thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthadicha enna thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Shake Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Shake Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vetka Padatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vetka Padatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikki Mukki Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikki Mukki Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sokka Vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sokka Vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sarake En Muruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sarake En Muruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalaye Ithukiruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalaye Ithukiruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Um Perapey Athu Sirapey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Um Perapey Athu Sirapey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Allite Azhagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Allite Azhagey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cupcake Eh Milkshake Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cupcake Eh Milkshake Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Jillunu Iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Jillunu Iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sugar Eh Semma Power Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sugar Eh Semma Power Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oru Toppu Figure..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Toppu Figure.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Vantha Thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Vantha Thele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mutham Kuduthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mutham Kuduthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Um Muthamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Um Muthamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali Aneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Aneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mutta Vantha Maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mutta Vantha Maane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sotrunindra Tene Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sotrunindra Tene Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkudave Kuriporene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkudave Kuriporene"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paarvathathun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarvathathun"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-La Semma Beat Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-La Semma Beat Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Lubbu Tubbu Lubbu Tubbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Lubbu Tubbu Lubbu Tubbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthadicha enna thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthadicha enna thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Yo Shake Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Yo Shake Yo"/>
</div>
</pre>
