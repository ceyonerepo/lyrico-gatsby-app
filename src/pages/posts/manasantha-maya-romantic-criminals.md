---
title: "manasantha maya song lyrics"
album: "Romantic Criminals"
artist: "Sudhakar Mario"
lyricist: "Bala Vardhan"
director: "P Suneel Kumar Reddy"
path: "/albums/romantic-criminals-lyrics"
song: "Manasantha Maya"
image: ../../images/albumart/romantic-criminals.jpg
date: 2019-05-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YYzd2_3ZnqE"
type: "love"
singers:
  - Dhanunjay 
  - Swarajitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">katha edho gani kadhilindhe alalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha edho gani kadhilindhe alalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam neduthundhe nee vaipe oh hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam neduthundhe nee vaipe oh hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasane theda urake vesthundhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasane theda urake vesthundhi "/>
</div>
<div class="lyrico-lyrics-wrapper">maraam chesthundhi lolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraam chesthundhi lolona"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthaa maya adhi penche haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthaa maya adhi penche haya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tanavalle naalo edho modhalayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanavalle naalo edho modhalayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">samayam saripodhe saradhalu thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samayam saripodhe saradhalu thele"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosham thanai vasthe sari anene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosham thanai vasthe sari anene"/>
</div>
<div class="lyrico-lyrics-wrapper">vechi chusthunna ee vayasuku vekuvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi chusthunna ee vayasuku vekuvalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mosukosthundhe veluge neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosukosthundhe veluge neela"/>
</div>
<div class="lyrico-lyrics-wrapper">kastha kothhaina lavate maarela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastha kothhaina lavate maarela"/>
</div>
<div class="lyrico-lyrics-wrapper">chala bagundhe ne jatha valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chala bagundhe ne jatha valle"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthaa maya adhi penche haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthaa maya adhi penche haya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tanavalle naalo edho modhalayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanavalle naalo edho modhalayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo paruvam virisindhi neekosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo paruvam virisindhi neekosame"/>
</div>
<div class="lyrico-lyrics-wrapper">thapana theere tharunam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapana theere tharunam "/>
</div>
<div class="lyrico-lyrics-wrapper">thelisindhi naa ashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelisindhi naa ashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedi thagili karigindhile manchu naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedi thagili karigindhile manchu naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valapu vaana kurisi thadisanu nee jantalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapu vaana kurisi thadisanu nee jantalo"/>
</div>
<div class="lyrico-lyrics-wrapper">vere lokam lo viharisthunnane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vere lokam lo viharisthunnane "/>
</div>
<div class="lyrico-lyrics-wrapper">thelipoyane theliselope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelipoyane theliselope"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthaa maya adhi penche haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthaa maya adhi penche haya"/>
</div>
<div class="lyrico-lyrics-wrapper">tanavalle naalo edho modhalayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanavalle naalo edho modhalayyindhe"/>
</div>
</pre>
