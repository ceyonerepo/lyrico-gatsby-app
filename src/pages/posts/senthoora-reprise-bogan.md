---
title: "senthoora reprise song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Thamarai - Inno Genga"
director: "Lakshman"
path: "/albums/bogan-lyrics"
song: "Senthoora Reprise"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/taOaX0ec_gw"
type: "love"
singers:
  - Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nidha nidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidha nidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhanamaaga yositthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhanamaaga yositthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilla nilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilla nilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaamal odi yosithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamal odi yosithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhaan nee thedum maanbaalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhaan nee thedum maanbaalan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovaai unai yendhum boobaalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai unai yendhum boobaalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyin manavaalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyin manavaalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thondruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena thondruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengaandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhal poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai kaatril aigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai kaatril aigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengaandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhal poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai kaatril aigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai kaatril aigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna be with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna be with you"/>
</div>
<div class="lyrico-lyrics-wrapper">But baby you don’t know it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But baby you don’t know it"/>
</div>
<div class="lyrico-lyrics-wrapper">And when i look into your eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And when i look into your eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I wanna show it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna show it"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl i just wanna tell you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl i just wanna tell you"/>
</div>
<div class="lyrico-lyrics-wrapper">How i feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How i feel"/>
</div>
<div class="lyrico-lyrics-wrapper">And i know that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And i know that"/>
</div>
<div class="lyrico-lyrics-wrapper">We can be something real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We can be something real"/>
</div>
<div class="lyrico-lyrics-wrapper">So baby give us a chance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So baby give us a chance"/>
</div>
<div class="lyrico-lyrics-wrapper">Maybe you will see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maybe you will see"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m the king to your queen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m the king to your queen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah girl you’ve got 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah girl you’ve got "/>
</div>
<div class="lyrico-lyrics-wrapper">everything that i need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="everything that i need"/>
</div>
<div class="lyrico-lyrics-wrapper">You got me dreaming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You got me dreaming"/>
</div>
<div class="lyrico-lyrics-wrapper">About what we could be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="About what we could be"/>
</div>
<div class="lyrico-lyrics-wrapper">But you don’t no even know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But you don’t no even know"/>
</div>
<div class="lyrico-lyrics-wrapper">So take my hand and we can go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So take my hand and we can go"/>
</div>
<div class="lyrico-lyrics-wrapper">Where ever you wanna go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where ever you wanna go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girl you’re one of a kind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl you’re one of a kind"/>
</div>
<div class="lyrico-lyrics-wrapper">Why can’t i just get 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why can’t i just get "/>
</div>
<div class="lyrico-lyrics-wrapper">you out of my mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you out of my mind"/>
</div>
<div class="lyrico-lyrics-wrapper">Being that beautiful 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Being that beautiful "/>
</div>
<div class="lyrico-lyrics-wrapper">should be a crime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="should be a crime"/>
</div>
<div class="lyrico-lyrics-wrapper">You can be my jasmine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can be my jasmine"/>
</div>
<div class="lyrico-lyrics-wrapper">And i’mma be aladdin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And i’mma be aladdin"/>
</div>
<div class="lyrico-lyrics-wrapper">And we can see what happening
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And we can see what happening"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkaiyil anaithavaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkaiyil anaithavaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalai pinaithavaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalai pinaithavaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai ezhumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai ezhumpodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam varumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam varumpodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee piriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee piriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvae pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvae pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengaandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhal poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai kaatril aigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai kaatril aigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengaandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhal poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai kaatril aigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai kaatril aigindren"/>
</div>
</pre>
