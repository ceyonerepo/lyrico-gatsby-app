---
title: "naalo neeku song lyrics"
album: "Mr. Majnu"
artist: "S. Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/mr-majnu-lyrics"
song: "Naalo Neeku"
image: ../../images/albumart/mr-majnu.jpg
date: 2019-01-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iyNMThnyXCI"
type: "love"
singers:
  - Shreya Ghoshal
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalo neku nelo naaku Selavenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo neku nelo naaku Selavenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme kaani prema Odulukuntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme kaani prema Odulukuntunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Kaburinka Vintunantunna hrudhayanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Kaburinka Vintunantunna hrudhayanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve nindi vunnavandhi nijamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve nindi vunnavandhi nijamena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naake sadhyamaa ninne maravadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake sadhyamaa ninne maravadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhey neramaa ninne kalavadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhey neramaa ninne kalavadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Premanu kaadhaney bhadhuley ivvadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premanu kaadhaney bhadhuley ivvadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedo prashnalaa nene migaladam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedo prashnalaa nene migaladam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaayam chesi velutunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam chesi velutunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam laaga nenunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam laaga nenunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam intha chedai migilena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam intha chedai migilena"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyam cheruvai vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam cheruvai vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Teeram cheralekunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teeram cheralekunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuram yentha jaley chupinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuram yentha jaley chupinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalo neku nelo naaku Selavenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo neku nelo naaku Selavenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme kaani prema Odulukuntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme kaani prema Odulukuntunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Navve kallotho brathikestugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navve kallotho brathikestugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvula venuka neeray nuvvani chupaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvula venuka neeray nuvvani chupaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Theyyani vuhalaa kannipisthunugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theyyani vuhalaa kannipisthunugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuhala venuka bharam vundhani telupaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuhala venuka bharam vundhani telupaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvani yevarani teliyani guruthugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani yevarani teliyani guruthugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichayam jaraganey ledhantaanugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayam jaraganey ledhantaanugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natanai podha brathukanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natanai podha brathukanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalupai podha veluganthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalupai podha veluganthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupe leni aate chalikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupe leni aate chalikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mariche veelu lenanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mariche veelu lenanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchesave premanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchesave premanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thencheymante suluvem kaadugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thencheymante suluvem kaadugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasuley kalavadam varamaa saapamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuley kalavadam varamaa saapamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevariki viduvadam premaa nyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevariki viduvadam premaa nyayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo neku nelo naaku Selavenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo neku nelo naaku Selavenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme kaani prema Odulukuntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme kaani prema Odulukuntunnaa"/>
</div>
</pre>
