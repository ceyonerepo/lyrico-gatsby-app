---
title: "laaga le song lyrics"
album: "Luck"
artist: "Salim–Sulaiman - Amar Mohile"
lyricist: "Shabbir Ahmed"
director: "Soham Shah"
path: "/albums/luck-lyrics"
song: "Laaga Le - Ishq di Fitrat Hai"
image: ../../images/albumart/luck.jpg
date: 2009-07-24
lang: hindi
youtubeLink: "https://www.youtube.com/embed/Xrg89AJfbEM"
type: "happy"
singers:
  - Anushka Manchanda
  - Robert Bob Omulo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq di fitrat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq di fitrat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Badalta yeh kismat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badalta yeh kismat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehroom na hona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehroom na hona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq toh jannat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq toh jannat hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh daulat yeh shohrat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh daulat yeh shohrat"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh shaano yeh shauqat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh shaano yeh shauqat"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja mahi apna bana le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja mahi apna bana le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daav laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le daanv laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daanv laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daanv laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daanv laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let me see you make money in the party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me see you make money in the party"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Do it up you make money in the party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do it up you make money in the party"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Do your cash up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do your cash up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Put the money on the table if you want the cash up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put the money on the table if you want the cash up"/>
</div>
<div class="lyrico-lyrics-wrapper">Get money if you want to be a smart cash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get money if you want to be a smart cash"/>
</div>
<div class="lyrico-lyrics-wrapper">I paid my babes n i m rolling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I paid my babes n i m rolling"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn me who the mac coz i ll start calling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn me who the mac coz i ll start calling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naasha hai dhuwa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasha hai dhuwa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh pal pe zabaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh pal pe zabaan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dafan raat ke seene main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dafan raat ke seene main"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaz yeh kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaz yeh kya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaza hai maza hai nasheeli aada hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaza hai maza hai nasheeli aada hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Musalsal yun jeene ka aandaz kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musalsal yun jeene ka aandaz kya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do it up do it up do it up yehhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do it up do it up do it up yehhhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh daulat yeh shohrat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh daulat yeh shohrat"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh shaano yeh shauqat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh shaano yeh shauqat"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja mahi apna bana le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja mahi apna bana le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daav laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daav laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make this money take this money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make this money take this money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le laga le laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le laga le laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khili hai dhuli hai siyahi ghuli hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khili hai dhuli hai siyahi ghuli hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bisatoh pe shataranj ki zindagi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bisatoh pe shataranj ki zindagi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Here we go here we goooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Here we go here we goooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yakeen hai guma hai nawazish numa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakeen hai guma hai nawazish numa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo haat aaye tere toh phir kya kami hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo haat aaye tere toh phir kya kami hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh daulat yeh shohrat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh daulat yeh shohrat"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh shaano yeh shauqat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh shaano yeh shauqat"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja mahi apna bana le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja mahi apna bana le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laga le laga le daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Laga le laga le daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laga le laga le daav laga le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud pe yakeen hai toh daav laga le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud pe yakeen hai toh daav laga le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let me see you make money in the party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me see you make money in the party"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Do it up you make money in the party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do it up you make money in the party"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Do the cash up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do the cash up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Put the money on the table if you want the cash up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put the money on the table if you want the cash up"/>
</div>
<div class="lyrico-lyrics-wrapper">Get money if you want to be a smart cash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get money if you want to be a smart cash"/>
</div>
<div class="lyrico-lyrics-wrapper">I paid my babes n i m rolling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I paid my babes n i m rolling"/>
</div>
<div class="lyrico-lyrics-wrapper">Turn me who the mac coz i ll start calling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turn me who the mac coz i ll start calling"/>
</div>
</pre>
