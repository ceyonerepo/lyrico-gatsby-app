---
title: "thekkathi singamada song lyrics"
album: "Muthuramalingam"
artist: "Ilaiyaraaja"
lyricist: "Panchu Arunachalam"
director: "Rajadurai"
path: "/albums/muthuramalingam-lyrics"
song: "Thekkathi Singamada"
image: ../../images/albumart/muthuramalingam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pVzGhiJUi90"
type: "happy"
singers:
  - Kamal Haasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka pasum ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka pasum ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangamada en kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangamada en kula"/>
</div>
<div class="lyrico-lyrics-wrapper">tholukku sonthamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholukku sonthamada"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka pasum ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka pasum ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangamada en kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangamada en kula"/>
</div>
<div class="lyrico-lyrics-wrapper">tholukku sonthamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholukku sonthamada"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathellam thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathellam thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha pera matum sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha pera matum sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">veerathoda nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerathoda nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu ethirpathu yar sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu ethirpathu yar sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varinchu katti elunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varinchu katti elunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna vadaa unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna vadaa unna"/>
</div>
<div class="lyrico-lyrics-wrapper">vananga vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vananga vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vada unna vananga vaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vada unna vananga vaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thuninji elunthu murukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninji elunthu murukki"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna porumai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna porumai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">verachu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verachu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">porumai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porumai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">verachu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verachu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thannambikkai thalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannambikkai thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarthum thalarvilatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarthum thalarvilatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalarvillatha thanga gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalarvillatha thanga gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanjam endru varubavarku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjam endru varubavarku"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu uthavum thanga gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu uthavum thanga gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu uthavum thanga gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu uthavum thanga gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">vellakaranukkum vettu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellakaranukkum vettu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">beerangi vechalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beerangi vechalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pethu eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethu eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">vellakaranukkum vettu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellakaranukkum vettu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">beerangi vechalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beerangi vechalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pethu eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethu eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram unarthum santhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram unarthum santhana"/>
</div>
<div class="lyrico-lyrics-wrapper">thevar mannil vara suthuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevar mannil vara suthuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundu malli sendugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu malli sendugala"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruthu vaala thandugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruthu vaala thandugala"/>
</div>
<div class="lyrico-lyrics-wrapper">singam pola pendugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam pola pendugala"/>
</div>
<div class="lyrico-lyrics-wrapper">namma muthuramalingathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma muthuramalingathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti paattu sollungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti paattu sollungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peruketha veeram irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruketha veeram irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram irukku veeram irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram irukku veeram irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">verathoda rosam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verathoda rosam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">rosam iruku rosam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosam iruku rosam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnal ninnu sandi maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnal ninnu sandi maata"/>
</div>
<div class="lyrico-lyrics-wrapper">seendi paakum devarellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seendi paakum devarellam"/>
</div>
<div class="lyrico-lyrics-wrapper">munnal vanthu ethirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnal vanthu ethirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu mooka udaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu mooka udaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">muthu raamalingam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu raamalingam "/>
</div>
<div class="lyrico-lyrics-wrapper">muthu raamalingam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu raamalingam "/>
</div>
<div class="lyrico-lyrics-wrapper">muthu raamalingam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu raamalingam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattiya vittu paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattiya vittu paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poei pattamum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poei pattamum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">pattamum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattamum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">satamum padichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satamum padichu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhathi osaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhathi osaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyamaaga sathichuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyamaaga sathichuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyamaaga sathichuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyamaaga sathichuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha pola inga ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha pola inga ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda virinchu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda virinchu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam kooda virinchu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam kooda virinchu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha maranthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha maranthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">porantha manna vida mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porantha manna vida mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">porantha manna vida mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porantha manna vida mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai katti aandalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai katti aandalum"/>
</div>
<div class="lyrico-lyrics-wrapper">urava vittu thara mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava vittu thara mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai katti aandalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai katti aandalum"/>
</div>
<div class="lyrico-lyrics-wrapper">urava vittu thara mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava vittu thara mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">pakthiya vidamum mothathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakthiya vidamum mothathila"/>
</div>
<div class="lyrico-lyrics-wrapper">enga rathathula oduthu paruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga rathathula oduthu paruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka pasum ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka pasum ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangamada en kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangamada en kula"/>
</div>
<div class="lyrico-lyrics-wrapper">tholukku sonthamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholukku sonthamada"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka pasum ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka pasum ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangamada en kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangamada en kula"/>
</div>
<div class="lyrico-lyrics-wrapper">tholukku sonthamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholukku sonthamada"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathellam thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathellam thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha pera matum sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha pera matum sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">veerathoda nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerathoda nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu ethirpathu yar sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu ethirpathu yar sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
<div class="lyrico-lyrics-wrapper">thekkathi singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekkathi singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">muthuramalingamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthuramalingamada"/>
</div>
</pre>
