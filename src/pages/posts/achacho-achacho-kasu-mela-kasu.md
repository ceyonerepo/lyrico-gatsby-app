---
title: "achacho achacho song lyrics"
album: "Kasu Mela Kasu"
artist: "M.S. Pandian"
lyricist: "Karuppaiah"
director: "K.S. Pazhani"
path: "/albums/kasu-mela-kasu-lyrics"
song: "Achacho Achacho"
image: ../../images/albumart/kasu-mela-kasu.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9S5i6vHrvwo"
type: "love"
singers:
  - Jagadesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">achacho achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho achacho"/>
</div>
<div class="lyrico-lyrics-wrapper">daddy ku jackpot aduchuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daddy ku jackpot aduchuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku oru beauty kedachuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku oru beauty kedachuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">athirstam kathava thadiruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athirstam kathava thadiruche"/>
</div>
<div class="lyrico-lyrics-wrapper">nenacha vaalkai kedachuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenacha vaalkai kedachuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">en naina kaatina maina ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en naina kaatina maina ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">enake enaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake enaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho "/>
</div>
<div class="lyrico-lyrics-wrapper">achacho achacho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho "/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna thinaku thinana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna thinaku thinana"/>
</div>
<div class="lyrico-lyrics-wrapper">thin thin thin na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thin thin thin na"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna thinaku thinana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna thinaku thinana"/>
</div>
<div class="lyrico-lyrics-wrapper">thin thin thin na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thin thin thin na"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna thinaku thinana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna thinaku thinana"/>
</div>
<div class="lyrico-lyrics-wrapper">thin thin thin na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thin thin thin na"/>
</div>
<div class="lyrico-lyrics-wrapper">thinna thinna thinaku thinana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinna thinna thinaku thinana"/>
</div>
<div class="lyrico-lyrics-wrapper">thin thin thin na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thin thin thin na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tata birla varisaiyil enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tata birla varisaiyil enga"/>
</div>
<div class="lyrico-lyrics-wrapper">appanum sera poran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appanum sera poran da"/>
</div>
<div class="lyrico-lyrics-wrapper">late ah vanthalum latest 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="late ah vanthalum latest "/>
</div>
<div class="lyrico-lyrics-wrapper">nanu nu padida ipo thunuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanu nu padida ipo thunuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">enga appan peril kappal vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga appan peril kappal vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">business panna poren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="business panna poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">kandam vittu kandam thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandam vittu kandam thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal panna poren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal panna poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">ulage enna thirumbi pakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulage enna thirumbi pakum"/>
</div>
<div class="lyrico-lyrics-wrapper">oore enna tholil thookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore enna tholil thookum"/>
</div>
<div class="lyrico-lyrics-wrapper">ulage enna virumbi pakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulage enna virumbi pakum"/>
</div>
<div class="lyrico-lyrics-wrapper">oore enna tholil thookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore enna tholil thookum"/>
</div>
<div class="lyrico-lyrics-wrapper">caril paraka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="caril paraka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">kasil mithakka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasil mithakka poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho"/>
</div>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanum avalum onna sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum avalum onna sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">selfi eduthu kolveme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selfi eduthu kolveme"/>
</div>
<div class="lyrico-lyrics-wrapper">friends kellam sharing panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friends kellam sharing panni"/>
</div>
<div class="lyrico-lyrics-wrapper">tension aethi papome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tension aethi papome"/>
</div>
<div class="lyrico-lyrics-wrapper">atha ponnum maman ponnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha ponnum maman ponnum"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi sera koodalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi sera koodalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thinnu theeka vantha oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinnu theeka vantha oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">adada imsa thangalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada imsa thangalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">five star hote kooti povan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five star hote kooti povan"/>
</div>
<div class="lyrico-lyrics-wrapper">pizza udan burger keppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizza udan burger keppa"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam avan vaangi thinba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam avan vaangi thinba"/>
</div>
<div class="lyrico-lyrics-wrapper">bill ah en thalaiyil vaipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bill ah en thalaiyil vaipa"/>
</div>
<div class="lyrico-lyrics-wrapper">purse ah thodachuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purse ah thodachuduva"/>
</div>
<div class="lyrico-lyrics-wrapper">pulse ah ethiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulse ah ethiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho"/>
</div>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho"/>
</div>
<div class="lyrico-lyrics-wrapper">daddy ku jackpot aduchuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daddy ku jackpot aduchuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku oru beauty kedachuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku oru beauty kedachuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">athirstam kathava thadiruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athirstam kathava thadiruche"/>
</div>
<div class="lyrico-lyrics-wrapper">nenacha vaalkai kedachuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenacha vaalkai kedachuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">en naina kaatina maina ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en naina kaatina maina ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">enake enaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enake enaache"/>
</div>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho achacho"/>
</div>
</pre>
