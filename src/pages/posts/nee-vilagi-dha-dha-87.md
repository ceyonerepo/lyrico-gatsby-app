---
title: "Nee Vilagi song lyrics"
album: "Dha Dha 87"
artist: "Leander Lee Marty"
lyricist: "Vijay Sri G"
director: "Vijay Sri G"
path: "/albums/dha-dha-87-lyrics"
song: "Nee Vilagi"
image: ../../images/albumart/dha-dha-87.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Prd2NExpR1s"
type: "love"
singers:
  - Anand Aravindakshan
  - Sruthy Sasidharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee en idavalam inaivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en idavalam inaivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">min nagalena varuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="min nagalena varuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un vidayani tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vidayani tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vezhambal pennalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vezhambal pennalle"/>
</div>
<div class="lyrico-lyrics-wrapper">ven maari thenulill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ven maari thenulill"/>
</div>
<div class="lyrico-lyrics-wrapper">peyyanai kathille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyyanai kathille"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vilagi enai ean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vilagi enai ean"/>
</div>
<div class="lyrico-lyrics-wrapper">prindhaai adadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prindhaai adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyil paniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyil paniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee nijamena varuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nijamena varuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vin tholaivinil marivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vin tholaivinil marivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en manviral inaivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manviral inaivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ea kan munnil neeyille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ea kan munnil neeyille"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoorangal maayille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoorangal maayille"/>
</div>
<div class="lyrico-lyrics-wrapper">naan onnaai therillae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan onnaai therillae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee vilagi enai ean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vilagi enai ean"/>
</div>
<div class="lyrico-lyrics-wrapper">marandhaai adadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marandhaai adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanal nadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanal nadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">padagaai ean vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padagaai ean vandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marumurai oru murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marumurai oru murai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthida vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthida vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">ena dhinam amaidhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena dhinam amaidhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaiyaai vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaiyaai vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sirai karai marandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirai karai marandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvaai thondrinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvaai thondrinen"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai perum agavayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai perum agavayil"/>
</div>
<div class="lyrico-lyrics-wrapper">suyambaai maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suyambaai maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyosaiyile nee pesidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyosaiyile nee pesidave"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai kavidhaigal endrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai kavidhaigal endrene"/>
</div>
<div class="lyrico-lyrics-wrapper">un pun muruval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pun muruval"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanvarudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanvarudum"/>
</div>
<div class="lyrico-lyrics-wrapper">badi varum naal paarpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badi varum naal paarpene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madham konda yaanai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madham konda yaanai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">murchai yaana en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murchai yaana en"/>
</div>
<div class="lyrico-lyrics-wrapper">moochinai meetten naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochinai meetten naan"/>
</div>
<div class="lyrico-lyrics-wrapper">udhirum vayadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirum vayadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai unarndhen naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai unarndhen naan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vilagi enai ean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vilagi enai ean"/>
</div>
<div class="lyrico-lyrics-wrapper">prindhaai adadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prindhaai adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyil paniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyil paniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal uyire pirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal uyire pirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">adadaa kaanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adadaa kaanal"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhiyil padagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhiyil padagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ean vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ean vandhaai"/>
</div>
</pre>
