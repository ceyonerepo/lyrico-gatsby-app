---
title: "murali mogha song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Rohini"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Murali Mogha"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GVTyQZw_EeQ"
type: "happy"
singers:
  - Haricharan Seshadri
  - KS Chitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thakka thimitha thimitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thimitha thimitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi sariya sariya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi sariya sariya "/>
</div>
<div class="lyrico-lyrics-wrapper">sathiraatam vittu ponane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiraatam vittu ponane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru simitti simitti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru simitti simitti "/>
</div>
<div class="lyrico-lyrics-wrapper">oru ganathil anaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ganathil anaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru ganathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru ganathil "/>
</div>
<div class="lyrico-lyrics-wrapper">nanban aanavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban aanavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan neela vanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan neela vanna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaarmega kannan pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarmega kannan pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothathintha siru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothathintha siru "/>
</div>
<div class="lyrico-lyrics-wrapper">mayilin nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilin nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi angum engum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi angum engum "/>
</div>
<div class="lyrico-lyrics-wrapper">sacharavu nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sacharavu nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un uravu vendi utharavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uravu vendi utharavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuma aval idhazhgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuma aval idhazhgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nesa swasame isainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nesa swasame isainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En nesa swasame isainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nesa swasame isainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvai kadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvai kadhalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhalin mogathin varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhalin mogathin varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paranthom neeyum nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthom neeyum nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli vaanam aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli vaanam aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivom naam mei enil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivom naam mei enil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir neengum neengume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir neengum neengume"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paranthom neeyum naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthom neeyum naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">thuli vaanam aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli vaanam aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivom naam mei enil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivom naam mei enil "/>
</div>
<div class="lyrico-lyrics-wrapper">uyir neengum neengume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir neengum neengume"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammil unnai kandu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammil unnai kandu "/>
</div>
<div class="lyrico-lyrics-wrapper">unmatham unmatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmatham unmatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril neerukulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril neerukulla "/>
</div>
<div class="lyrico-lyrics-wrapper">thanmatham thanmatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanmatham thanmatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil unnai kandu unmatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil unnai kandu unmatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaigal meetha bandham endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaigal meetha bandham endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha aatharam aatharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha aatharam aatharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvai vetti pogathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvai vetti pogathe "/>
</div>
<div class="lyrico-lyrics-wrapper">paarvai parikkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvai parikkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam nanaiyum un vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam nanaiyum un vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir oviyam uyir oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir oviyam uyir oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai enge ponalum nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai enge ponalum nee vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un angam angam tharum antharangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un angam angam tharum antharangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi mayanguthe vizhi kalanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi mayanguthe vizhi kalanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nesa swasame isainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nesa swasame isainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En nesa swasame isainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nesa swasame isainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvai kadhalin sadhalin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvai kadhalin sadhalin "/>
</div>
<div class="lyrico-lyrics-wrapper">mogathin varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogathin varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murali mogha kuzhal isaiuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murali mogha kuzhal isaiuthe"/>
</div>
</pre>
