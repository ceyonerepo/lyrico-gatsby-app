---
title: "you're the one song lyrics"
album: "Gulaebaghavali"
artist: "Vivek – Mervin"
lyricist: "Ko. Sesha - Inno Genga"
director: "Kalyaan"
path: "/albums/gulaebaghavali-lyrics"
song: "You're The One"
image: ../../images/albumart/gulaebaghavali.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N0F6P2pqW7E"
type: "love"
singers:
  - Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">When we’re together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When we’re together"/>
</div>
<div class="lyrico-lyrics-wrapper">Feels like I’m flying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feels like I’m flying"/>
</div>
<div class="lyrico-lyrics-wrapper">Away with you forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Away with you forever"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I’m not lying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I’m not lying"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say that I need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say that I need you"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I believe you’re the one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I believe you’re the one"/>
</div>
<div class="lyrico-lyrics-wrapper">Only one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Only one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai pozhinthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pozhinthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kudayinil naamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudayinil naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappadhai edhir kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappadhai edhir kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal pizhayah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal pizhayah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam ondru kodu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam ondru kodu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarangalum theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarangalum theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani maram ena naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani maram ena naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppadu muraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppadu muraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thaaragai nee thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaaragai nee thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vizhiyal kolladhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vizhiyal kolladhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaadhadi kai viralal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaadhadi kai viralal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkaamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkaamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkaamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkaamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’ve been thinking about you lately
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ve been thinking about you lately"/>
</div>
<div class="lyrico-lyrics-wrapper">Got my feeling kinda wavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got my feeling kinda wavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl you know you got me crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl you know you got me crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">With you is where I wanna be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With you is where I wanna be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never had a doubt about us
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never had a doubt about us"/>
</div>
<div class="lyrico-lyrics-wrapper">Started friends but now we’re lovers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Started friends but now we’re lovers"/>
</div>
<div class="lyrico-lyrics-wrapper">We just need to be together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We just need to be together"/>
</div>
<div class="lyrico-lyrics-wrapper">For your love I got the key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For your love I got the key"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are the one for me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are the one for me"/>
</div>
<div class="lyrico-lyrics-wrapper">One day you’ll see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One day you’ll see"/>
</div>
<div class="lyrico-lyrics-wrapper">We’re perfect for each other baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We’re perfect for each other baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You and me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You and me"/>
</div>
<div class="lyrico-lyrics-wrapper">Always and forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always and forever"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll be there whenever wherever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll be there whenever wherever"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When we’re together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When we’re together"/>
</div>
<div class="lyrico-lyrics-wrapper">Feels like I’m flying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feels like I’m flying"/>
</div>
<div class="lyrico-lyrics-wrapper">Away with you forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Away with you forever"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I’m not lying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I’m not lying"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say that I need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say that I need you"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I believe you’re the one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I believe you’re the one"/>
</div>
<div class="lyrico-lyrics-wrapper">Only one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Only one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When we’re together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When we’re together"/>
</div>
<div class="lyrico-lyrics-wrapper">Feels like I’m flying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feels like I’m flying"/>
</div>
<div class="lyrico-lyrics-wrapper">Away with you forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Away with you forever"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I’m not lying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I’m not lying"/>
</div>
<div class="lyrico-lyrics-wrapper">When I say that I need you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I say that I need you"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl I believe you’re the one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl I believe you’re the one"/>
</div>
<div class="lyrico-lyrics-wrapper">Only one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Only one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkaamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkaamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
</pre>
