---
title: "kadha solla poren song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Kadha Solla Poren"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/C1QbUbBHrU0"
type: "happy"
singers:
  - Vikram
  - Shringa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadha Solla Poreyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Solla Poreyn"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Solla Poreyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Solla Poreyn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kadhappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadhappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaja Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Sollapporeyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Sollapporeyn"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakka Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka Kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Solla Poreyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Solla Poreyn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Kadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Kadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Kadhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Kadhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaiyaa Athellam Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiyaa Athellam Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaatha Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaatha Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Yaaravuthu Thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Yaaravuthu Thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamaa Eduthutaanganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamaa Eduthutaanganaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Kadhaiya Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Kadhaiya Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Kadhaiya Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Kadhaiya Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Kadha Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Kadha Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaadhi Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaadhi Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyaraja Kadhaya Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyaraja Kadhaya Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Oru Rajavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Rajavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Rajavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Rajavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorla Vera Yaarum Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorla Vera Yaarum Illaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmm Irukaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmm Irukaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajavoda Mandhiringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajavoda Mandhiringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Perum Koojavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Perum Koojavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Oru Rajavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Rajavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajavoda Mandhiringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajavoda Mandhiringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Perum Koojavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Perum Koojavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatula Por Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatula Por Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porila Sandai Nadakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porila Sandai Nadakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatula Por Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatula Por Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porila Sandai Nadakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porila Sandai Nadakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiyil Gundu Vedikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyil Gundu Vedikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dom Dom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dom Dom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaiyaa Ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyaa Ethuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanda Thaan Ethuku Nadanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Thaan Ethuku Nadanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda Thaan Ethuku Nadanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Thaan Ethuku Nadanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Vadaiya Thirudiruchula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Vadaiya Thirudiruchula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaan Athaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Athaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanda Thaan Ethuku Nadanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Thaan Ethuku Nadanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaathaan Vadaiya Thiruduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaathaan Vadaiya Thiruduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nariyumthaan Paada Solluchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nariyumthaan Paada Solluchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Paadichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Paadichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo Paadaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Paadaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Kaa Kaa Kaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Kaa Kaa Kaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiiyo Vada Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiiyo Vada Poche"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Paavamla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Paavamla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Viduvaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Viduvaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haahaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haahaahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajaa Pattu Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaa Pattu Paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nari Vadaiya Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nari Vadaiya Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Rajakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Rajakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakka Thanks Sollichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakka Thanks Sollichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OOreyllam Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OOreyllam Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaatanthaan Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaatanthaan Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nariyoda Thittam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nariyoda Thittam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindatam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindatam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadellam Vazhthuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadellam Vazhthuthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Vaazhgha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vaazhgha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaja Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Vaazhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Vaazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Raja Kadhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Raja Kadhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkaa Kadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkaa Kadhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Raajaa Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Raajaa Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hehe He He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehe He He"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Raajaa Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Raajaa Vaazhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Raajaa Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Raajaa Vaazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kadha Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kadha Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichu Sirichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichu Sirichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Adhai Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adhai Ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Kadha Irukku Enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Kadha Irukku Enkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kettu Nee Paaraatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kettu Nee Paaraatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Oru Rajavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Rajavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajavoda Mandhiringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajavoda Mandhiringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Perum Koojavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Perum Koojavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyoo Thirumbavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo Thirumbavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhe Raajaa Kadhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhe Raajaa Kadhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Antha Raajaa Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Antha Raajaa Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Raajaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Raajaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Raajaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Superman Superman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superman Superman"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Sollatha Atha Sollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Sollatha Atha Sollatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anakonda Kadhaiya Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anakonda Kadhaiya Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anakonda Kadhaiya Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anakonda Kadhaiya Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Sir Yaar Sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Sir Yaar Sir"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahh Dinosaur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahh Dinosaur"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatula Romba Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatula Romba Naalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinosaur Thollanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinosaur Thollanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal Raajaakitta Ponaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal Raajaakitta Ponaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinosaura Vetaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinosaura Vetaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Superman Pola Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superman Pola Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajaava Poga Solli Ketaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajaava Poga Solli Ketaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Ellam Anakonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Ellam Anakonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Kaatuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Kaatuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thaandi Ponaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thaandi Ponaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Thaakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Thaakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Ellam Anakonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Ellam Anakonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Kaatuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Kaatuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Thaandi Ponaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Thaandi Ponaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Thaakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Thaakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Thaandi Ponaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Thaandi Ponaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Urumuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Urumuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajavin Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajavin Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Pathunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Pathunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dinosaur La Saaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinosaur La Saaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Yen Sethuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Yen Sethuchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadellam Vaazhthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadellam Vaazhthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajaa Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajaa Vaazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Raaja Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Raaja Vaazhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkaa Raaja Vaazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Raaja Vaazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Pothumpaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Pothumpaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittudupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittudupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhichi Muzhichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhichi Muzhichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kadha Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kadha Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Madakki Madakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madakki Madakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Adha Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adha Ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Kadha Irukku Enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Kadha Irukku Enkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kettu Nee Paaraatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kettu Nee Paaraatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Oru Rajavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Rajavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajaavoda Kadhaiya Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajaavoda Kadhaiya Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonguthu Nilaavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonguthu Nilaavaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumba Thirumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba Thirumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kadha Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kadha Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongi Thoongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongi Thoongi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Adha Kettka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adha Kettka"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Kadha Irukku Enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Kadha Irukku Enkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kettu Nee Paaraatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kettu Nee Paaraatta"/>
</div>
</pre>
