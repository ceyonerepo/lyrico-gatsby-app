---
title: "kadhal cricket song lyrics"
album: "Thani Oruvan"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "M. Raja"
path: "/albums/thani-oruvan-lyrics"
song: "Kadhal Cricket"
image: ../../images/albumart/thani-oruvan.jpg
date: 2015-08-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xzxr6fxdI_E"
type: "Love"
singers:
  - Kharesma Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Param Parampam Pararampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Param Parampam Pararampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Param Parampam Pararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Param Parampam Pararam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Param Parampam Pararampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Param Parampam Pararampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Param Parampam Pararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Param Parampam Pararam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romance Romance Idhuthaan En Chance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance Romance Idhuthaan En Chance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaazhkai Unkaiyil Irukkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkai Unkaiyil Irukkuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pinnaal Naaiaattam Suththurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pinnaal Naaiaattam Suththurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paarthu Oore Sirikkuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paarthu Oore Sirikkuthuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Senja Oththukkuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Senja Oththukkuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nee Eppa Yethukkuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Eppa Yethukkuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennenna Venum Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Venum Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Enna Maathikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Enna Maathikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Thoondil Pottu Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Thoondil Pottu Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu Valaiyil Maattalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Valaiyil Maattalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elumbu Thundu Pottu Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu Thundu Pottu Paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayum Vaale Aattalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayum Vaale Aattalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaikku Mela Kovam Varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikku Mela Kovam Varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Veli Kaattalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Veli Kaattalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Enai Maathikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Enai Maathikitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Nee Enna Madhikkalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Nee Enna Madhikkalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhaalum Unnai Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum Unnai Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Seivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seivene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan En Boomi Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan En Boomi Unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththi Varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Varuvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaga Irukkura Ponnungal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Irukkura Ponnungal Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ariva irukka Maattaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariva irukka Maattaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivaa Irukkura Ponnunga Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaa Irukkura Ponnunga Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halwa Kuduthuttu Povaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa Kuduthuttu Povaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alagum Arivum Kalandhu Ennai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagum Arivum Kalandhu Ennai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagi Ulagi Yarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Ulagi Yarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pinnaal Naa Suththuradhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pinnaal Naa Suththuradhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Arumai Unakku Puriyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Arumai Unakku Puriyavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhalum Unnai Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhalum Unnai Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Seivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seivene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan En Boomi Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan En Boomi Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththi Varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Varuvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraparampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraparampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraparampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraparampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraparampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraparampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraparampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraparampam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Cricketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Cricketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wicketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wicketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Crickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Crickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthuruchu Wicketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthuruchu Wicketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Paarthathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Paarthathaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanene Duck Outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Duck Outu"/>
</div>
</pre>
