---
title: "enge enathu kavithai song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Enge Enathu Kavithai"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qfAJxAbUGNI"
type: "sad"
singers:
  - Srinivas
  - K. S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pirai Vandhavudan Nila Vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Vandhavudan Nila Vandhavudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Vandhadhendru Ullam Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhadhendru Ullam Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kandavudam Nee Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kandavudam Nee Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nenjam Nenjam Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nenjam Nenjam Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Vandhavudan Nila Vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Vandhavudan Nila Vandhavudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Vanthathendru Ullam Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vanthathendru Ullam Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kandavudam Nee Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kandavudam Nee Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nenjam Nenjam Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nenjam Nenjam Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enge Enathu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enathu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhudhi Maditha Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhudhi Maditha Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Enadhu Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Enadhu Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhudhi Maditha Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhudhi Maditha Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil Karaindhu Vittadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil Karaindhu Vittadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammammaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyal Azhithu Vittadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal Azhithu Vittadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavidhai Thedi Thaarungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai Thedi Thaarungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai En Kanavai Meetu Thaarungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai En Kanavai Meetu Thaarungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae Enadhu Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Enadhu Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhudhi Maditha Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhudhi Maditha Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Enadhu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Enadhu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavilae Eluthi Madiththa Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilae Eluthi Madiththa Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Andhigalil Manadhin Sandhugalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Andhigalil Manadhin Sandhugalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaindha Mugaththai Manam Thedudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindha Mugaththai Manam Thedudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyil Thaarozhugum Nagara Veedhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil Thaarozhugum Nagara Veedhigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maiyal Kondu Malar Vaadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyal Kondu Malar Vaadudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam Sindhum Iru Thuliyin Idaiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Sindhum Iru Thuliyin Idaiveliyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuruvi Thuruvi Unai Thedudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuruvi Thuruvi Unai Thedudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaiyum Nuraigalilum Tholaindha Kaadhalanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyum Nuraigalilum Tholaindha Kaadhalanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urugi Urugi Manam Thedudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Urugi Manam Thedudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Vandhavudan Nila Vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Vandhavudan Nila Vandhavudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Vandhadhendru Ullam Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhadhendru Ullam Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kandavudam Nee Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kandavudam Nee Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nenjam Nenjam Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nenjam Nenjam Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirai Vandhavudan Nila Vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Vandhavudan Nila Vandhavudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila Vanthathendru Ullam Thullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vanthathendru Ullam Thullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kandavudam Nee Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kandavudam Nee Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nenjam Nenjam Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nenjam Nenjam Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enge Enathu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enathu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhudhi Maditha Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhudhi Maditha Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Enadhu Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Enadhu Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhudhi Maditha Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhudhi Maditha Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Paarvai Ada Orey Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Paarvai Ada Orey Vaarthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Orey Thodudhal Manam Yenguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Orey Thodudhal Manam Yenguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthampodum Andha Moochin Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthampodum Andha Moochin Veppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Nitham Vendum Endru Yengudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Nitham Vendum Endru Yengudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Pooththa Undhan Sattai Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Pooththa Undhan Sattai Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Ottum Endru Manam Yengudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Ottum Endru Manam Yengudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam Poothirukum Mudiyil Ondrirendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Poothirukum Mudiyil Ondrirendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutthum Inbam Kannam Ketkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutthum Inbam Kannam Ketkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraiyil Seithathu En Manam Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraiyil Seithathu En Manam Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhikku Solli Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhikku Solli Irundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraiyin Idukkil Vaer Vitta Kodiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraiyin Idukkil Vaer Vitta Kodiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenjil Mulaithuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenjil Mulaithuvittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enge Enathu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enathu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhuthi Madiththa Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhuthi Madiththa Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Enadhu Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Enadhu Kavidhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaviley Ezhuthi Madiththa Kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaviley Ezhuthi Madiththa Kavidhai"/>
</div>
</pre>
