---
title: "vaanam namadhe song lyrics"
album: "Pathinaaru"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Sabapathy - Dekshinamurthy"
path: "/albums/pathinaaru-lyrics"
song: "Vaanam Namadhe"
image: ../../images/albumart/pathinaaru.jpg
date: 2011-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DRuJBUaLHDM"
type: "happy"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam Namathey Bhoomi Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Namathey Bhoomi Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Namathey Kadalum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Namathey Kadalum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Namathey Veliyum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Namathey Veliyum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Thisaiyum Ovvoru Kizhakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thisaiyum Ovvoru Kizhakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vizhiyum Ovvoru Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vizhiyum Ovvoru Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Seyalum Ovvoru Ilakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Seyalum Ovvoru Ilakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poar Kalamaai Vazhkaithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poar Kalamaai Vazhkaithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Munnae Nikuthey Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Munnae Nikuthey Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poar Idavae Ninaithaal Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poar Idavae Ninaithaal Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Pookal Pookuthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pookal Pookuthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Nammakkana Kaalamthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Nammakkana Kaalamthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Izhakkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Izhakkathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhanthaalae Meendumthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhanthaalae Meendumthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikaathey Vaazhvoamae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikaathey Vaazhvoamae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Namathey Bhoomi Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Namathey Bhoomi Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Namathey Kadalum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Namathey Kadalum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Namathey Veliyum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Namathey Veliyum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Thisaiyum Ovvoru Kizhakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thisaiyum Ovvoru Kizhakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vizhiyum Ovvoru Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vizhiyum Ovvoru Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Seyalum Ovvoru Ilakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Seyalum Ovvoru Ilakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Yaarukkum Kai Katti Nirkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Yaarukkum Kai Katti Nirkaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaththai Puram Thalli Naam Oadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaththai Puram Thalli Naam Oadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oa.. Vaazhkai Eppothum Ellaiyai Kaatathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oa.. Vaazhkai Eppothum Ellaiyai Kaatathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaahvodu Ellaiyai Naam Thaedalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaahvodu Ellaiyai Naam Thaedalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvil Eppothum Othigal Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Eppothum Othigal Kidaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae Othigal Poalthaanae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae Othigal Poalthaanae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum Manmaelae Sutramal Nirkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Manmaelae Sutramal Nirkaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzharchi Ondraethaan Vaazhvae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzharchi Ondraethaan Vaazhvae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Oru Jenmam Manmeethu Irukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Oru Jenmam Manmeethu Irukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Intha Jenmathai Muzhuthaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Intha Jenmathai Muzhuthaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoamae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoamae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Namathey Bhoomi Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Namathey Bhoomi Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Namathey Kadalum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Namathey Kadalum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Namathey Veliyum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Namathey Veliyum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Thisaiyum Ovvoru Kizhakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thisaiyum Ovvoru Kizhakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vizhiyum Ovvoru Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vizhiyum Ovvoru Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Seyalum Ovvoru Ilakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Seyalum Ovvoru Ilakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaahkai Pazhanathil Thidum Endru Thiruppangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaahkai Pazhanathil Thidum Endru Thiruppangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaatai Thalaikaatum Athai Thaandalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaatai Thalaikaatum Athai Thaandalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Nadaipoata Thisaiyil Naam Pogamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Nadaipoata Thisaiyil Naam Pogamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Pogum Thisai Naalai Vazhi Aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Pogum Thisai Naalai Vazhi Aagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhum Nimidangal Ovvundrum Uyarvaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Nimidangal Ovvundrum Uyarvaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethaiyum Ezhakkamal Vaazhvomae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyum Ezhakkamal Vaazhvomae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitham Yuthangal Nizhal Pola Thodarnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitham Yuthangal Nizhal Pola Thodarnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Satham Podaamal Jeyipomae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satham Podaamal Jeyipomae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Theriyaamal Tholaintheythaan Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Theriyaamal Tholaintheythaan Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Naam Ingae Izhakkamal Vaazhvomae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Naam Ingae Izhakkamal Vaazhvomae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Namathey Bhoomi Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Namathey Bhoomi Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Namathey Kadalum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Namathey Kadalum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Namathey Veliyum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Namathey Veliyum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Thisaiyum Ovvoru Kizhakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thisaiyum Ovvoru Kizhakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vizhiyum Ovvoru Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vizhiyum Ovvoru Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Seyalum Ovvoru Ilakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Seyalum Ovvoru Ilakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poar Kalamaai Vazhkaithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poar Kalamaai Vazhkaithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Munnae Nikuthey Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Munnae Nikuthey Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poar Idavae Ninaithaal Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poar Idavae Ninaithaal Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Pookal Pookuthu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pookal Pookuthu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Nammakkana Kaalamthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Nammakkana Kaalamthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Izhakkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Izhakkathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhanthaalae Meendumthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhanthaalae Meendumthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikaathey Vaazhvoamae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikaathey Vaazhvoamae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Namathey Bhoomi Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Namathey Bhoomi Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Namathey Kadalum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Namathey Kadalum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Namathey Veliyum Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Namathey Veliyum Namathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Thisaiyum Ovvoru Kizhakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thisaiyum Ovvoru Kizhakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Vizhiyum Ovvoru Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Vizhiyum Ovvoru Vilakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Seyalum Ovvoru Ilakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Seyalum Ovvoru Ilakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvoam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvoam Vaa Vaa"/>
</div>
</pre>
