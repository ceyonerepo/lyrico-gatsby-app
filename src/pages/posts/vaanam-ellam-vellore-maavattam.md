---
title: "vaanam ellam song lyrics"
album: "Vellore Maavattam"
artist: "Sundar C Babu"
lyricist: "Kavivarman"
director: "R.N.R. Manohar"
path: "/albums/vellore-maavattam-lyrics"
song: "Vaanam Ellam"
image: ../../images/albumart/vellore-maavattam.jpg
date: 2011-10-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IsShzyNJKB8"
type: "happy"
singers:
  - Hariharan
  - Shruthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam ellaam ini namadhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam ellaam ini namadhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">makkal thogai angu iranduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkal thogai angu iranduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrennum theril unnai kondupoaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrennum theril unnai kondupoaven"/>
</div>
<div class="lyrico-lyrics-wrapper">megathiley mella iranguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathiley mella iranguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettenendraal vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettenendraal vali"/>
</div>
<div class="lyrico-lyrics-wrapper">thottuvittaal thuyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottuvittaal thuyil"/>
</div>
<div class="lyrico-lyrics-wrapper">thathalithen thaniye haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathalithen thaniye haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">nammidam idaivili illaadha nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammidam idaivili illaadha nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkul kadhagaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkul kadhagaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravaa adhisayamaana pudhu vidha vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaa adhisayamaana pudhu vidha vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">aruginil vandhaal veesum veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruginil vandhaal veesum veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">nuni viral pattaal sila nodi neram nirkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuni viral pattaal sila nodi neram nirkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nirkkum en swaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirkkum en swaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamaigalin Osai idigalai poaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaigalin Osai idigalai poaley"/>
</div>
<div class="lyrico-lyrics-wrapper">sevigalai inbam yeno yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevigalai inbam yeno yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">iragaiyai kondu vizhigalil vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iragaiyai kondu vizhigalil vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thulla kandeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulla kandeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirum neruppum sudugira paniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirum neruppum sudugira paniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu vizhigaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu vizhigaliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moaga kangal rasithadhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moaga kangal rasithadhai "/>
</div>
<div class="lyrico-lyrics-wrapper">nagakangal rasithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagakangal rasithida"/>
</div>
<div class="lyrico-lyrics-wrapper">valigalum surangalum kalandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigalum surangalum kalandhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu vidha anubavamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu vidha anubavamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagaliravendru irundhidavendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaliravendru irundhidavendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">iravugal rendum vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravugal rendum vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">nindridum boomi sutridum vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nindridum boomi sutridum vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">maarum boo koalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarum boo koalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru murai thandhaal marumurai ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai thandhaal marumurai ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhan per enna m m m
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhan per enna m m m"/>
</div>
<div class="lyrico-lyrics-wrapper">idhuvarai andha anubavamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuvarai andha anubavamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">indru m m m
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru m m m"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avizhandha paarvaiyil aayiram kavidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avizhandha paarvaiyil aayiram kavidhai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoguthu veliyidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoguthu veliyidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil sirippadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil sirippadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiyanai kizhivadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiyanai kizhivadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhu manam manadhinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhu manam manadhinai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudiya iruvarin anubavamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudiya iruvarin anubavamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam ellaam ini namadhu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam ellaam ini namadhu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">makkal thogai angu iranduthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkal thogai angu iranduthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrennum theril unnai kondupoaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrennum theril unnai kondupoaven"/>
</div>
<div class="lyrico-lyrics-wrapper">megathiley mella iranguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathiley mella iranguven"/>
</div>
</pre>
