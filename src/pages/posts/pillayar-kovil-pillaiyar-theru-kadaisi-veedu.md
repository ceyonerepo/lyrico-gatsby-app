---
title: "pillayar kovil song lyrics"
album: "Pillaiyar Theru Kadaisi Veedu"
artist: "Chakri"
lyricist: "Snehan"
director: "Thirumalai Kishore"
path: "/albums/pillaiyar-theru-kadaisi-veedu-lyrics"
song: "Pillayar Kovil"
image: ../../images/albumart/pillaiyar-theru-kadaisi-veedu.jpg
date: 2011-06-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mFyao088qG4"
type: "happy"
singers:
  - Chakri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pillaiyaar Koayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaar Koayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaaru Koayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaaru Koayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vettai Eellaam Ithuvaraikkum Thoathathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vettai Eellaam Ithuvaraikkum Thoathathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koattaiyila Kodiyumilla Kolgaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koattaiyila Kodiyumilla Kolgaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu Naalaiyellaam Engalukku Therivathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu Naalaiyellaam Engalukku Therivathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothu Aarambichaa Oorathoonga Viduvathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothu Aarambichaa Oorathoonga Viduvathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthisthaan Muthisthaan Muthisthaan Muthula Villo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthisthaan Muthisthaan Muthisthaan Muthula Villo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichaesko Ichaesko Ichaesko Ichaesko Muthula Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichaesko Ichaesko Ichaesko Ichaesko Muthula Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaar Koayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaar Koayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaarum Innaattu Kudimakkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum Innaattu Kudimakkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrethaan Sonnaaley Poathaathudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrethaan Sonnaaley Poathaathudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattukku Eppoathum Unmaiyaaga Irukkununna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattukku Eppoathum Unmaiyaaga Irukkununna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Kudikkoanunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Kudikkoanunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinamum Kondaattanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinamum Kondaattanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Nee Aadu Ènnaaatanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Nee Aadu Ènnaaatanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kavalaiyilla Kaattaarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kavalaiyilla Kaattaarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduthida Mudiyaathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduthida Mudiyaathuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthisthaan Muthisthaan Muthisthaan Muthula Villø
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthisthaan Muthisthaan Muthisthaan Muthula Villø"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaar Køayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaar Køayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Køyilukku Nenthuvitta Kaalaigalapøla Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køyilukku Nenthuvitta Kaalaigalapøla Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oørasuthi Varuvøamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oørasuthi Varuvøamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappakkøøda Šariyaaga Èppadi Nee Šeivathunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappakkøøda Šariyaaga Èppadi Nee Šeivathunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kathutharuvømada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kathutharuvømada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Vaazhathaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Vaazhathaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Nee Bayanthaal Køazhaithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Nee Bayanthaal Køazhaithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Èllaarukkum Intha Bøømi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Èllaarukkum Intha Bøømi"/>
</div>
Šø<div class="lyrico-lyrics-wrapper">nthanthaan Marakkaathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nthanthaan Marakkaathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthisthaan Muthisthaan Muthisthaan Muthula Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthisthaan Muthisthaan Muthisthaan Muthula Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Villa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaar Køayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaar Køayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
È<div class="lyrico-lyrics-wrapper">nga Vettai Èellaam Ithuvaraikkum Thøathathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nga Vettai Èellaam Ithuvaraikkum Thøathathilla"/>
</div>
È<div class="lyrico-lyrics-wrapper">nga Køattaiyila Kødiyumilla Kølgaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nga Køattaiyila Kødiyumilla Kølgaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu Naalaiyellaam Èngalukku Therivathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu Naalaiyellaam Èngalukku Therivathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Køøthu Aarambichaa Oørathøønga Viduvathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køøthu Aarambichaa Oørathøønga Viduvathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthisthaan Muthisthaan Muthisthaan Muthula Villø
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthisthaan Muthisthaan Muthisthaan Muthula Villø"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichaeskø Ichaeskø Ichaeskø Ichaeskø Muthula Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaar Køayil Theru Kadaisi Veettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaar Køayil Theru Kadaisi Veettula"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Pillaiyaaru Pasangalathaan Ketka Aalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pillaiyaaru Pasangalathaan Ketka Aalilla"/>
</div>
</pre>
