---
title: "yavarukkum song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Vaali"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Yavarukkum"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/soaKqQERgMM"
type: "happy"
singers:
  - Chinmayi
  - Senthil Das
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaavarukkum Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavarukkum Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Ulla Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Ulla Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vanangum Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vanangum Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Oruvan Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Oruvan Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelakkadalthaanavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelakkadalthaanavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenthugindra Meenaavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenthugindra Meenaavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaveliyil Kathiraai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaveliyil Kathiraai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaai Thirivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaai Thirivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai Kodiyin Madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai Kodiyin Madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaraai Virivaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaraai Virivaanaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaithoda Enaithoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaithoda Enaithoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Thanithani Veyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Thanithani Veyil Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena Enakkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Enakkena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Mazhai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Mazhai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayangal Sadangugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayangal Sadangugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Samathuva Vilangugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Samathuva Vilangugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkavai Etharkkena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkavai Etharkkena "/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Udaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Udaithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undiyaargal Veralla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiyaargal Veralla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhai Vayiruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai Vayiruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Nirappu Unai Vaazhthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Nirappu Unai Vaazhthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Uyirum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Uyirum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavarukkum Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavarukkum Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Ulla Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Ulla Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Vanangum Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vanangum Thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Oruvan Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Oruvan Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirarkena Ilagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirarkena Ilagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Iraivanin Iruppidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Iraivanin Iruppidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthavidum Karangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthavidum Karangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Uraigiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Uraigiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanum Punithanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanum Punithanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Manangalum Gunangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Manangalum Gunangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavidum Idamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavidum Idamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Niraigiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Niraigiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Theendum Poongaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Theendum Poongaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Theenduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Theenduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaangum Boomithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaangum Boomithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thaanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thaanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavarukkum Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavarukkum Thalaivan"/>
</div>
</pre>
