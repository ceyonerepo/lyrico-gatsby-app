---
title: "uchithanai mukarnthal song lyrics"
album: "Uchithanai Muharnthaal"
artist: "D. Imman"
lyricist: "Kasi Anandan"
director: "Pugazhendhi Thangaraj"
path: "/albums/uchithanai-muharnthaal-lyrics"
song: "Uchithanai Mukarnthal"
image: ../../images/albumart/uchithanai-muharnthaal.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/V32PIQkjhUU"
type: "melody"
singers:
  - Mathangi Jagdish
  - Priyanka
  - Balaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uchithanai muharnthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchithanai muharnthal"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangal varai silirkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangal varai silirkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi unai anaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi unai anaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam kodi thulikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam kodi thulikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilavenil kaalam vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilavenil kaalam vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">poovai poovai poothu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovai poovai poothu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu vanthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu vanthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavu vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavu vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulage velichathil kaithu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulage velichathil kaithu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchithanai muharnthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchithanai muharnthal"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangal varai silirkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangal varai silirkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi unai anaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi unai anaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam kodi thulikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam kodi thulikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unai maranthu ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai maranthu ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">thiranthu kanne sirikinraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiranthu kanne sirikinraye"/>
</div>
<div class="lyrico-lyrics-wrapper">chinnan chiru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinnan chiru "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam kidaithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam kidaithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee siragai virikindraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee siragai virikindraye"/>
</div>
<div class="lyrico-lyrics-wrapper">malarai nilavai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarai nilavai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">valarai magale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarai magale "/>
</div>
<div class="lyrico-lyrics-wrapper">alagai alagaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai alagaku"/>
</div>
<div class="lyrico-lyrics-wrapper">alagai sirikum un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai sirikum un"/>
</div>
<div class="lyrico-lyrics-wrapper">milagai siripal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milagai siripal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvai poo kolamaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvai poo kolamaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">uchithanai muharnthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchithanai muharnthal"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangal varai silirkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangal varai silirkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">matinagar paatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matinagar paatum"/>
</div>
<div class="lyrico-lyrics-wrapper">matalan kootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matalan kootum "/>
</div>
<div class="lyrico-lyrics-wrapper">manasu marakalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu marakalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ooril nan valartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooril nan valartha"/>
</div>
<div class="lyrico-lyrics-wrapper">kili pillai en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kili pillai en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu parakalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu parakalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchithanai muharnthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchithanai muharnthal"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangal varai silirkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangal varai silirkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi unai anaithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi unai anaithal"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam kodi thulikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam kodi thulikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilavenil kaalam vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilavenil kaalam vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">poovai poovai poothu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovai poovai poothu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu vanthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu vanthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavu vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavu vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulage velichathil kaithu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulage velichathil kaithu vidu"/>
</div>
</pre>
