---
title: "praanamichinavu song lyrics"
album: "Pogaru"
artist: "Chandan Shetty"
lyricist: "Vengi"
director: "Nandha kishor"
path: "/albums/pogaru-lyrics"
song: "Praanamichinavu Gnanamichinavu"
image: ../../images/albumart/pogaru.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vF2cf82gCl0"
type: "affection"
singers:
  - Aniruddha Sastry
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pranamichinavu gnanamichinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamichinavu gnanamichinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha manchi ammave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha manchi ammave"/>
</div>
<div class="lyrico-lyrics-wrapper">Laala posinavu laali paadinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laala posinavu laali paadinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kanna janmave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kanna janmave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanubadu dhaivame kannita munigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanubadu dhaivame kannita munigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukiki arthamundunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukiki arthamundunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuvu posina amme aligithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuvu posina amme aligithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwasaku shwasa undunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwasaku shwasa undunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamichinavu gnanamichinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamichinavu gnanamichinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha manchi ammave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha manchi ammave"/>
</div>
<div class="lyrico-lyrics-wrapper">Laala posinavu laali paadinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laala posinavu laali paadinavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kanna janmave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kanna janmave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanubadu dhaivame kannita munigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanubadu dhaivame kannita munigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukiki arthamundunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukiki arthamundunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuvu posina amme aligithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuvu posina amme aligithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwasaku shwasa undunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwasaku shwasa undunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam thirige pranam amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam thirige pranam amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thyagam needhe thaguve kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thyagam needhe thaguve kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni anuraaganiki prathi rupame amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni anuraaganiki prathi rupame amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha padina aasha theerani amruthame amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha padina aasha theerani amruthame amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabadu adugune nadipinchavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabadu adugune nadipinchavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharagani varam neevule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagani varam neevule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OOravadi prematho nanu kannavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OOravadi prematho nanu kannavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheragani swaram neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani swaram neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaika murthi needhe keeerthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaika murthi needhe keeerthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Buvva petti nannu bujjaginchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buvva petti nannu bujjaginchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujji devathe amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji devathe amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastapettina nannu kottani devathave amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastapettina nannu kottani devathave amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Swantham annadhe enthaku teliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swantham annadhe enthaku teliyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche jeevame amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche jeevame amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bashalu maarina artham maarani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bashalu maarina artham maarani"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyani bavame amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyani bavame amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam thirige pranam amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam thirige pranam amma"/>
</div>
</pre>
