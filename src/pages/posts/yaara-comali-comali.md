---
title: 'yaara comali song lyrics'
album: 'Comali'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Pradeep Ranganathan'
path: '/albums/comali-song-lyrics'
song: 'Yaara Comali'
image: ../../images/albumart/comali.jpg
date: 2019-08-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8lN55rxVypI"
type: 'sad'
singers: 
- Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey haan…hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey haan…hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey haan…hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey haan…hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veri yeruthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Veri yeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maarudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi maarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veral serudhu comaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Veral serudhu comaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manashankitta pesa sonna
<input type="checkbox" class="lyrico-select-lyric-line" value="Manashankitta pesa sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Comali-aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Comali-aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Machine-u kitta pesitrukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Machine-u kitta pesitrukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaali-aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemaali-aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unmaiyaana ulagam unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmaiyaana ulagam unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Comali-aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Comali-aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimbathula maatikitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Bimbathula maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaali-aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemaali-aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey pazhasula naa kingudaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey pazhasula naa kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennikkum youngu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennikkum youngu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">90’<div class="lyrico-lyrics-wrapper">s kid-u daa</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="s kid-u daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Always in trendu maa
<input type="checkbox" class="lyrico-select-lyric-line" value="Always in trendu maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaara comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaara comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga pora yemaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaga pora yemaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Comali …comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Comali …comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Comali …yemaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Comali …yemaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Comala irundha comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Comala irundha comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaana godhaala erangunaaka nae gaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaana godhaala erangunaaka nae gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan comala irundha comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan comala irundha comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaana godhaala erangunaaka nae gaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaana godhaala erangunaaka nae gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poli mugavari pin
<input type="checkbox" class="lyrico-select-lyric-line" value="Poli mugavari pin"/>
</div>
<div class="lyrico-lyrics-wrapper">Olindhu kollum mugangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Olindhu kollum mugangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poli adaiyalam edharkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Poli adaiyalam edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann thirangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kann thirangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nija mugathudan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nija mugathudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla mudiyaatha vakkirangalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla mudiyaatha vakkirangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollubavarai udanae puram thallungal
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollubavarai udanae puram thallungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Virtual reality and reality
<input type="checkbox" class="lyrico-select-lyric-line" value="Virtual reality and reality"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t nature bad vibes no negativity
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t nature bad vibes no negativity"/>
</div>
<div class="lyrico-lyrics-wrapper">I goota hit you with the truth
<input type="checkbox" class="lyrico-select-lyric-line" value="I goota hit you with the truth"/>
</div>
<div class="lyrico-lyrics-wrapper">Set you free
<input type="checkbox" class="lyrico-select-lyric-line" value="Set you free"/>
</div>
<div class="lyrico-lyrics-wrapper">Who’s the slave here?
<input type="checkbox" class="lyrico-select-lyric-line" value="Who’s the slave here?"/>
</div>
<div class="lyrico-lyrics-wrapper">You or the technology huh?
<input type="checkbox" class="lyrico-select-lyric-line" value="You or the technology huh?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaara comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaara comali"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaara"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naana comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana comali"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naana
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee thaan comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan comali"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaga pora yemaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaga pora yemaali"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thats right</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Thats right"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Prabalam aaganumna
<input type="checkbox" class="lyrico-select-lyric-line" value="Prabalam aaganumna"/>
</div>
<div class="lyrico-lyrics-wrapper">Na enna vena pannuvaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Na enna vena pannuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Prabalam aavadharkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Prabalam aavadharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinam kuda thinnuvaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinam kuda thinnuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirarin uzhaippa
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirarin uzhaippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurai solli prabalam aaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurai solli prabalam aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta panta pottukittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Satta panta pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallavanaa suthuvaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nallavanaa suthuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu oru mana nooi
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu oru mana nooi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinjidum seekiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Thelinjidum seekiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Dislike ah share panni
<input type="checkbox" class="lyrico-select-lyric-line" value="Dislike ah share panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Like ah pichcha ketkurom
<input type="checkbox" class="lyrico-select-lyric-line" value="Like ah pichcha ketkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurai sollum kalai ena
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurai sollum kalai ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhappa thaan marakkurom
<input type="checkbox" class="lyrico-select-lyric-line" value="Pozhappa thaan marakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththavan backa-yen
<input type="checkbox" class="lyrico-select-lyric-line" value="Maththavan backa-yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorinjuda ninaikirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Sorinjuda ninaikirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey…hey… hey… hey…. hey.. hey… hey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…hey… hey… hey…. hey.. hey… hey…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">I put 7 hey’s in a row
<input type="checkbox" class="lyrico-select-lyric-line" value="I put 7 hey’s in a row"/>
</div>
<div class="lyrico-lyrics-wrapper">Still getting paid
<input type="checkbox" class="lyrico-select-lyric-line" value="Still getting paid"/>
</div>
<div class="lyrico-lyrics-wrapper">Why do you burn
<input type="checkbox" class="lyrico-select-lyric-line" value="Why do you burn"/>
</div>
<div class="lyrico-lyrics-wrapper">When i win all my race
<input type="checkbox" class="lyrico-select-lyric-line" value="When i win all my race"/>
</div>
<div class="lyrico-lyrics-wrapper">Why do you wanna show me
<input type="checkbox" class="lyrico-select-lyric-line" value="Why do you wanna show me"/>
</div>
<div class="lyrico-lyrics-wrapper">All of your hate
<input type="checkbox" class="lyrico-select-lyric-line" value="All of your hate"/>
</div>
<div class="lyrico-lyrics-wrapper">Last week i bought an estate
<input type="checkbox" class="lyrico-select-lyric-line" value="Last week i bought an estate"/>
</div>
<div class="lyrico-lyrics-wrapper">But i felt sorry looking at your face
<input type="checkbox" class="lyrico-select-lyric-line" value="But i felt sorry looking at your face"/>
</div>
<div class="lyrico-lyrics-wrapper">Bcoz all you know is to just hate
<input type="checkbox" class="lyrico-select-lyric-line" value="Bcoz all you know is to just hate"/>
</div>
<div class="lyrico-lyrics-wrapper">And all i know is to celebrate
<input type="checkbox" class="lyrico-select-lyric-line" value="And all i know is to celebrate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaara comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaara comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan comali
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan comali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga pora yemaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaga pora yemaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Comali …comali …comali …
<input type="checkbox" class="lyrico-select-lyric-line" value="Comali …comali …comali …"/>
</div>
</pre>