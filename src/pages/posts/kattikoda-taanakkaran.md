---
title: "kattikoda song lyrics"
album: "Taanakkaran"
artist: "Ghibran"
lyricist: "Chandru"
director: "Tamizh"
path: "/albums/taanakkaran-lyrics"
song: "Kattikoda"
image: ../../images/albumart/taanakkaran.jpg
date: 2022-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Uo-yy43AWMo"
type: "love"
singers:
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kattikoda enna kattikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikoda enna kattikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta paiyil pothi vachikkoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta paiyil pothi vachikkoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha vaartha motha vaazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha vaartha motha vaazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga naan vazhvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga naan vazhvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikoda enna kattikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikoda enna kattikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta paiyil pothi vachikkoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta paiyil pothi vachikkoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha vaartha motha vazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha vaartha motha vazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga naan vazhvenae da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga naan vazhvenae da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallikooda paadam polathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikooda paadam polathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenappathan padikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenappathan padikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoodi kaadhal kadatheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoodi kaadhal kadatheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaipudichi naanum nadakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaipudichi naanum nadakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppumootta enna thookki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppumootta enna thookki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suthi kaata venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suthi kaata venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappukotti un nenappa thinnenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappukotti un nenappa thinnenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah idaikanda udai needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah idaikanda udai needhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee vaada nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee vaada nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikoda enna kattikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikoda enna kattikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta paiyil pothi vachikkoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta paiyil pothi vachikkoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha vaartha motha vazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha vaartha motha vazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga naan vazhvenae da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga naan vazhvenae da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakura paarvai mattumdhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakura paarvai mattumdhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttimodhi enna kolludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttimodhi enna kolludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukura indha nodikooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukura indha nodikooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru unakaaga thudikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru unakaaga thudikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna enni ullukulla ullnaatu kalavaramdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna enni ullukulla ullnaatu kalavaramdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavendru naanum solla needhanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavendru naanum solla needhanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaikanda udai needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaikanda udai needhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee vaada nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee vaada nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikoda enna kattikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikoda enna kattikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta paiyil pothi vachikkoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta paiyil pothi vachikkoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha vaartha motha vazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha vaartha motha vazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga naan vazhvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga naan vazhvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikoda enna kattikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikoda enna kattikoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satta paiyil pothi vachikkoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta paiyil pothi vachikkoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha vaartha motha vazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha vaartha motha vazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga naan vazhvenae da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga naan vazhvenae da"/>
</div>
</pre>
