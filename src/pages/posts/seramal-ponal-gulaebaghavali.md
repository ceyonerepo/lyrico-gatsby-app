---
title: "seramal ponal song lyrics"
album: "Gulaebaghavali"
artist: "Vivek – Mervin"
lyricist: "Ko. Sesha"
director: "Kalyaan"
path: "/albums/gulaebaghavali-lyrics"
song: "Seramal Ponal"
image: ../../images/albumart/gulaebaghavali.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/URgqLY3Z7PQ"
type: "love"
singers:
  - Mervin Solomon
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai pozhinthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pozhinthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kudayinil naamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudayinil naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappadhai edhir kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappadhai edhir kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal pizhayah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal pizhayah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam ondru kodu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam ondru kodu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarangalum theeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarangalum theeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani maram ena naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani maram ena naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppadu muraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppadu muraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thaaragai nee thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaaragai nee thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vizhiyal kolladhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vizhiyal kolladhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaadhadi kai viralal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaadhadi kai viralal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu veyilil kadal karayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu veyilil kadal karayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padagadiyil inaindhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padagadiyil inaindhidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu iravil adai mazhayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu iravil adai mazhayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai vazhiyil inaindhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai vazhiyil inaindhidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janal vazhiyil minnal pugundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janal vazhiyil minnal pugundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigalilum inaindhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigalilum inaindhidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil arayil kaalai varayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil arayil kaalai varayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvai sirayil inaindhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvai sirayil inaindhidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee indri naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan indri neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan indri neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum vaazhkai yennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum vaazhkai yennada"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae neeyum sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae neeyum sollada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neer indri vaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer indri vaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan indri neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan indri neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundaal ulagam yedhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundaal ulagam yedhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae purindhu kolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae purindhu kolladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seramal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seramal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarkamal ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkamal ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhagi povenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhagi povenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovae"/>
</div>
</pre>
