---
title: "yethikka yethikka song lyrics"
album: "Nota"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "Anand Shankar"
path: "/albums/nota-lyrics"
song: "Yethikka Yethikka"
image: ../../images/albumart/nota.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zco_Boa9Rjk"
type: "happy"
singers:
  - Benny Dayal
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu thudhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu thudhdhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu thudhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu thudhdhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu thudhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu thudhdhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo hooo ooo ooo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo hooo ooo ooo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiguthigu thudhdhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiguthigu thudhdhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththam seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththam seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vazhi thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhi thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Champa champa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champa champa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala vazhi ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vazhi ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Champa champa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champa champa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadivaazham udaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadivaazham udaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaa champa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaa champa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu bhoomi padaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu bhoomi padaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa champa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa champa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil yeri kudikka polam champa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil yeri kudikka polam champa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum innum utcham thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum innum utcham thoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 1
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 1"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 2
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 2"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 3
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 3"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 4
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 4"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkala nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkala nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalil idhala orasiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalil idhala orasiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sediyil kodiyil parachiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sediyil kodiyil parachiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Siriya kettu therinjuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siriya kettu therinjuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Javukku pinnala innaichiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Javukku pinnala innaichiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathil otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathil otti"/>
</div>
<div class="lyrico-lyrics-wrapper">Skydiving variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Skydiving variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoda nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoda nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Bungyjump variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bungyjump variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Scorpion molaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scorpion molaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadikka ready-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikka ready-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Night ellaam moon shine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night ellaam moon shine"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudikka ready-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikka ready-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 1
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 1"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 2
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 2"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 3
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 3"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 4
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 4"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hero yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hero yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagazhil mattum nallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagazhil mattum nallavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Villain yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villain yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttum pozhuthum nallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttum pozhuthum nallavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Beauty yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beauty yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu thiranthu sirippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu thiranthu sirippavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sexy yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sexy yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Marachu manasa keduppavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachu manasa keduppavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vibrator mode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vibrator mode"/>
</div>
<div class="lyrico-lyrics-wrapper">Phone-ku ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone-ku ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lubricant ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lubricant ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engine-ku ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engine-ku ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Twitter-il profile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitter-il profile"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakku ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakku ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala athukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala athukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 1
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 1"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 2
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 2"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 3
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 3"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikka yethikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikka yethikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Shotnumber shotnumber 4
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shotnumber shotnumber 4"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalla nikkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalla nikkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyyala oyyala oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyyala oyyala oh"/>
</div>
</pre>
