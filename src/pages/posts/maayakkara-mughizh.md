---
title: "maayakkara song lyrics"
album: "Mughizh"
artist: "Revaa"
lyricist: "Balaji Tharaneetharan"
director: "Karthik Swaminathan"
path: "/albums/mughizh-lyrics"
song: "Maayakkara"
image: ../../images/albumart/mughizh.jpg
date: 2021-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j-07ANaPues"
type: "happy"
singers:
  - Govind Vasantha
  - Malvi Sundaresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedho Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Ondrai Ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Ondrai Ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Unarvadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Unarvadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Mella Thaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Mella Thaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Anaippadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Anaippadhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Ulle Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Ulle Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Thullum Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Thullum Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vaasam Therindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vaasam Therindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Paasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Paasakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Kootil Serndha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Kootil Serndha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nesakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nesakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Vilaiyaadi Magizhaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Vilaiyaadi Magizhaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalum Ini Kidaiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalum Ini Kidaiyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enai Thedi Vaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enai Thedi Vaaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Ondrum Ini Varadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Ondrum Ini Varadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillai Pole Odi Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai Pole Odi Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Pole Thadi Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Pole Thadi Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgal Yaavum Ondru Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgal Yaavum Ondru Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattru Thandhaai Anbai Thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattru Thandhaai Anbai Thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo Oh Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo Oh Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillai Pole Odi Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai Pole Odi Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Pole Thadi Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Pole Thadi Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgal Yaavum Ondru Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgal Yaavum Ondru Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattru Thandhaai Anbai Thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattru Thandhaai Anbai Thandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Pudhidhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Pudhidhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Uravaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Uravaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Endrum Ini Nilaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Endrum Ini Nilaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Irundha Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Irundha Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevaai Thirindha Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevaai Thirindha Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maariyadhe Maayamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maariyadhe Maayamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Sol Petchai Ketkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sol Petchai Ketkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Seiyaaga Nee Vandhaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Seiyaaga Nee Vandhaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Vilai Thandhum Varadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vilai Thandhum Varadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Pasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Pasakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Nesakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Nesakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ange Inge Odikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ange Inge Odikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaigala Pottukittu Kobakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaigala Pottukittu Kobakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Istam Pola Suthikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam Pola Suthikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanaiyai Kootikittu Sandakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanaiyai Kootikittu Sandakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridamum Sikkidaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridamum Sikkidaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichikkittu Odivitta Vithaikkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichikkittu Odivitta Vithaikkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Vandhu Serndhu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Vandhu Serndhu Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Engum Pogidadhe Settaikkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Engum Pogidadhe Settaikkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandrathaiyum Panniputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandrathaiyum Panniputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonjiyai Mattum Thookkikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonjiyai Mattum Thookkikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veshakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluratha Ketkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluratha Ketkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paatukku Paduthukittu Roshakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paatukku Paduthukittu Roshakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Mayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Mayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Mayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Mayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Pasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Pasakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Mayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Mayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Vaada Nesakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Vaada Nesakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo Mayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo Mayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo Mayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo Mayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Maayakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maayakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkaara"/>
</div>
</pre>
