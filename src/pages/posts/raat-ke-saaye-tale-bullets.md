---
title: "raat ke saaye tale song lyrics"
album: "Bullets"
artist: "Raghav Sachar"
lyricist: "Rohit Sharma"
director: "Devang Dholakia"
path: "/albums/bullets-lyrics"
song: "Raat Ke Saaye Tale"
image: ../../images/albumart/bullets.jpg
date: 2021-01-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/0xMCiJXQUF0"
type: "love"
singers:
  - Aakanksha Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raat ke saaye tale,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ke saaye tale,"/>
</div>
<div class="lyrico-lyrics-wrapper">Saans saans hai jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saans saans hai jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaam lo labon ki narmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam lo labon ki narmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir yeh pal mile na mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir yeh pal mile na mile"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat ke saaye tale,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ke saaye tale,"/>
</div>
<div class="lyrico-lyrics-wrapper">Saans saans hai jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saans saans hai jale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odhke jismon ki chandni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhke jismon ki chandni"/>
</div>
<div class="lyrico-lyrics-wrapper">Baahon mein khoye rahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baahon mein khoye rahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon ko aankhon se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon ko aankhon se"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeke soye rahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeke soye rahe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo bhi darmiyan kar de bayaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi darmiyan kar de bayaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi ka kya bharosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ka kya bharosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kab ho yeh dhuan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kab ho yeh dhuan"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat ke saaye tale,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ke saaye tale,"/>
</div>
<div class="lyrico-lyrics-wrapper">Saans saans hai jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saans saans hai jale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rokte lamho ke qafile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rokte lamho ke qafile"/>
</div>
<div class="lyrico-lyrics-wrapper">Subah ko hone na de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subah ko hone na de"/>
</div>
<div class="lyrico-lyrics-wrapper">Behke behke pal hai jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Behke behke pal hai jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mile sone na de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mile sone na de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaja jane jaan tham jaye sama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja jane jaan tham jaye sama"/>
</div>
<div class="lyrico-lyrics-wrapper">Baat ka kya bharosa kab badle sama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baat ka kya bharosa kab badle sama"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat ke saaye tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ke saaye tale"/>
</div>
<div class="lyrico-lyrics-wrapper">Saans saans hai jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saans saans hai jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaam lo labon ki narmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam lo labon ki narmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir yeh pal mile na mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir yeh pal mile na mile"/>
</div>
</pre>
