---
title: "manasa song lyrics"
album: "Touch Chesi Chudu"
artist: "JAM 8 - Mani Sharma"
lyricist: "Rehman"
director: "Vikram Sirikonda"
path: "/albums/touch-chesi-chudu-lyrics"
song: "Manasa"
image: ../../images/albumart/touch-chesi-chudu.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Tthuqudoiik"
type: "love"
singers:
  - Benny Dayal
  - Neeti Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasa Manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaa Nee Padhanisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaa Nee Padhanisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvey Naa Tholi Nishaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvey Naa Tholi Nishaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedavi Daati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedavi Daati "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yedanu Meeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yedanu Meeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannallukundhi Navveley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannallukundhi Navveley "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalalu Koti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalalu Koti "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kanulananti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kanulananti "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Lokamantha Nuvveley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Lokamantha Nuvveley"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvveyle Nuvveyle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvveyle Nuvveyle "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenukooda Nuvveyle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenukooda Nuvveyle"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka Chaka Chakamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka Chaka Chakamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Chethulu Chaachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethulu Chaachi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Hathhuko Saawariya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Hathhuko Saawariya "/>
</div>
<div class="lyrico-lyrics-wrapper">Padha Padha Padha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha Padha Padha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Dhikkulu Dhaati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Dhikkulu Dhaati "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Ethhukuni Po Povaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Ethhukuni Po Povaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Mama Mama Mama Manushulleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Mama Mama Mama Manushulleni "/>
</div>
<div class="lyrico-lyrics-wrapper">Maro Lokamey Chupavaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro Lokamey Chupavaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Papa Papa Papa Pedhavini Patti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Papa Papa Papa Pedhavini Patti "/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhey Pettuko Thassadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhey Pettuko Thassadhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Rangullo Pongullo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Rangullo Pongullo "/>
</div>
<div class="lyrico-lyrics-wrapper">Tharangam Aadinadi Naa Oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharangam Aadinadi Naa Oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevaipu Vasthooney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevaipu Vasthooney "/>
</div>
<div class="lyrico-lyrics-wrapper">Sarangi Mogindi Lolo Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarangi Mogindi Lolo Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo Nee Ompulloni Enno Merupulni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Nee Ompulloni Enno Merupulni "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Chusthunnaney Kalle Muyyaleka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Chusthunnaney Kalle Muyyaleka "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulloni Enno Maatalni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulloni Enno Maatalni "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vintunnaney Ballo Patamlaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vintunnaney Ballo Patamlaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Emi Cheyaalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Emi Cheyaalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpichavey Sarigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpichavey Sarigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka Chaka Chakamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka Chaka Chakamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Chethulu Chaachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethulu Chaachi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Hathhuko Saawariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Hathhuko Saawariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha Padha Padha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha Padha Padha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Dhikkulu Dhaati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Dhikkulu Dhaati "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Ethhukuni Po Povaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Ethhukuni Po Povaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Mama Mama Mama Manushulleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Mama Mama Mama Manushulleni "/>
</div>
<div class="lyrico-lyrics-wrapper">Maro Lokamey Chupavaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro Lokamey Chupavaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Papa Papa Papa Pedhavini Patti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Papa Papa Papa Pedhavini Patti "/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhey Pettuko Thassadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhey Pettuko Thassadhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Chinni Pranamlo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Chinni Pranamlo "/>
</div>
<div class="lyrico-lyrics-wrapper">Nippedho Regindhi Aarpedhela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippedho Regindhi Aarpedhela "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kontey Konamlo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kontey Konamlo "/>
</div>
<div class="lyrico-lyrics-wrapper">Muppendho Dhaagundhi Aapendhela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppendho Dhaagundhi Aapendhela "/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo Aakasamantha Andhamneekundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo Aakasamantha Andhamneekundhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhookampamai Nannu Oopesthoo Unnadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhookampamai Nannu Oopesthoo Unnadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooranni Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooranni Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Thosesthoo Undhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thosesthoo Undhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Soodhantulaa Nannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhantulaa Nannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Laagesthoo Unnadhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagesthoo Unnadhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudemcheyyamantaavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudemcheyyamantaavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppeyavey Twaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppeyavey Twaraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka Chaka Chakamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka Chaka Chakamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Chethulu Chaachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethulu Chaachi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Hathhuko Saawariya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Hathhuko Saawariya "/>
</div>
<div class="lyrico-lyrics-wrapper">Padha Padha Padha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha Padha Padha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Dhikkulu Dhaati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Dhikkulu Dhaati "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanney Ethhukuni Po Povaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanney Ethhukuni Po Povaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Mama Mama Mama Manushulleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Mama Mama Mama Manushulleni "/>
</div>
<div class="lyrico-lyrics-wrapper">Maro Lokamey Chupavaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro Lokamey Chupavaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Papa Papa Papa Pedhavini Patti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Papa Papa Papa Pedhavini Patti "/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhey Pettuko Thassadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhey Pettuko Thassadhiya"/>
</div>
</pre>
