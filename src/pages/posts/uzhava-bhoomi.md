---
title: 'uzhavaa lyrics'
album: 'Bhoomi'
artist: 'D Imman'
lyricist: 'Madhan Karky'
director: 'Lakshman'
path: '/albums/bhoomi-song-lyrics'
song: 'Uzhavaa'
image: ../../images/albumart/bhoomi.jpg
date: 2020-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-ginNmxt6h8"
type: 'mass'
singers: 
- Sid Sriram
- Yogi B
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai ulagai padaicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai ulagai padaicka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhidu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhidu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai ulagai padaicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai ulagai padaicka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhidu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhidu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattanai poottanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattanai poottanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unackulle poottidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unackulle poottidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannenum thaayival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannenum thaayival"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinai Meettidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinai Meettidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marabanu yavilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marabanu yavilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhavanai theeditu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhavanai theeditu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam ini yaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ini yaarena"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiricku kaattidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiricku kaattidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai nellai ne uru maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai nellai ne uru maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjathey vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjathey vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uram ingey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uram ingey "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjuram dhaney kaikorthu vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjuram dhaney kaikorthu vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhirathai neerena paaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirathai neerena paaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu vaa vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu vaa vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">come with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come with me"/>
</div>
<div class="lyrico-lyrics-wrapper">viralellam yerena maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralellam yerena maara"/>
</div>
<div class="lyrico-lyrics-wrapper">man maatra vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man maatra vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">piniyena paravidum varatchiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piniyena paravidum varatchiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arivinil azhicka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arivinil azhicka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudhithoru vayalveli puratchiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhithoru vayalveli puratchiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nodiyinil thodanga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nodiyinil thodanga vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai ulagai padaicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai ulagai padaicka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhidu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhidu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaignan ninaithal mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaignan ninaithal mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivedu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivedu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">edu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aadiyum paadiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadiyum paadiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhaithitta nam inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhaithitta nam inam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniniyin thiraiyinil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniniyin thiraiyinil "/>
</div>
<div class="lyrico-lyrics-wrapper">siraipattu kidappadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siraipattu kidappadha"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhavanin kattudal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhavanin kattudal"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhaithida kidaickayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhaithida kidaickayil"/>
</div>
<div class="lyrico-lyrics-wrapper">visaipori tharaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visaipori tharaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">manidhargal nadappadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manidhargal nadappadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagin mudhal perarivalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagin mudhal perarivalan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaney vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaney vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come on"/>
</div>
<div class="lyrico-lyrics-wrapper">tharaiyinil kaal vaithidum thevadhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaiyinil kaal vaithidum thevadhai "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaney vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaney vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">lets go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lets go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aimbootham udan varum thozhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimbootham udan varum thozhan "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaney vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaney vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pasiyatri magizhgira thaayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasiyatri magizhgira thaayum "/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaney vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaney vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">narambinil therigira pasumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambinil therigira pasumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">indha vayalgalil iracka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha vayalgalil iracka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nelindhidum puluckalin sagathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelindhidum puluckalin sagathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ilam sarithiram sedhuckava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilam sarithiram sedhuckava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaignan ninaithal mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaignan ninaithal mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivedu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivedu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">edu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uzhavenbadhu oru poludhu pocku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhavenbadhu oru poludhu pocku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vilayum payirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilayum payirum"/>
</div>
<div class="lyrico-lyrics-wrapper">ulavan uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulavan uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">unacku aattamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unacku aattamey"/>
</div>
<div class="lyrico-lyrics-wrapper">unadharasiyal emacku thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadharasiyal emacku thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaignar uzhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaignar uzhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">inayum pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inayum pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">perugum eettamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perugum eettamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ini oru uyir veenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini oru uyir veenai"/>
</div>
<div class="lyrico-lyrics-wrapper">azhindhida vida mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhindhida vida mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">maruthuvam sattam poriyiyal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthuvam sattam poriyiyal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaviyal thozhil nutpamum maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaviyal thozhil nutpamum maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udazhalickum baanam vircka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udazhalickum baanam vircka "/>
</div>
<div class="lyrico-lyrics-wrapper">engal neerai vitradhaarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal neerai vitradhaarada"/>
</div>
<div class="lyrico-lyrics-wrapper">pugai umizhum aalaickaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugai umizhum aalaickaga"/>
</div>
<div class="lyrico-lyrics-wrapper">engal mannai virapadhaarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal mannai virapadhaarada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilavasa minsaram vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavasa minsaram vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyinil minsaram kaanbom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyinil minsaram kaanbom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadangalai ninaicka vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadangalai ninaicka vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">uzhaipinil ellamey theerpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uzhaipinil ellamey theerpom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa mannai aala uzhavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa mannai aala uzhavane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhan unmai pachai tamilaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhan unmai pachai tamilaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nilam kizhithidum undhan tholil yerodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam kizhithidum undhan tholil yerodu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhirigalin mugam kizhidida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhirigalin mugam kizhidida "/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai koodi poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai koodi poradu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai ulagai padaicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai ulagai padaicka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhidu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhidu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaignan ninaithal mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaignan ninaithal mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivedu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivedu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">edu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhava Uzhava Uzhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhava Uzhava Uzhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Uzha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Uzha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai ulagai padaicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai ulagai padaicka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhidu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhidu thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu thalaiva"/>
</div>
</pre>