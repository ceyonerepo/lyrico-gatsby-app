---
title: "thaakura song lyrics"
album: "Thittam Poattu Thirudura Kootam"
artist: "Ashwath"
lyricist: "Muralidaran K N"
director: "Sudhar"
path: "/albums/thittam-poattu-thirudura-kootam-lyrics"
song: "Thaakura"
image: ../../images/albumart/thittam-poattu-thirudura-kootam.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AVZLO-dygjs"
type: "happy"
singers:
  - Naresh Iyer
  - Divya Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vittathula padukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittathula padukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta kotta muzhikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta kotta muzhikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu thiruduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu thiruduren"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">vatta vatta mogathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatta vatta mogathula"/>
</div>
<div class="lyrico-lyrics-wrapper">katta kattaa tholaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta kattaa tholaikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaakura vedikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaakura vedikura"/>
</div>
<div class="lyrico-lyrics-wrapper">pataasaa nee dhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pataasaa nee dhaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kolutha than thudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolutha than thudikira"/>
</div>
<div class="lyrico-lyrics-wrapper">mittasu naan dhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mittasu naan dhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyura neliyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyura neliyura"/>
</div>
<div class="lyrico-lyrics-wrapper">thannala nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannala nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagula tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagula tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un aalu naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aalu naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala kathi kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala kathi kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadathatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadathatha "/>
</div>
<div class="lyrico-lyrics-wrapper">odambu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu than"/>
</div>
<div class="lyrico-lyrics-wrapper">thangathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangathey"/>
</div>
<div class="lyrico-lyrics-wrapper">pechala patha vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pechala patha vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum "/>
</div>
<div class="lyrico-lyrics-wrapper">theekuchi nee thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theekuchi nee thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kathukita motha vitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukita motha vitha"/>
</div>
<div class="lyrico-lyrics-wrapper">eraki vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eraki vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">un jaathi naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jaathi naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">othukitu oru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othukitu oru dharam"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponjathi nee thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponjathi nee thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kotta kotta muzhikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta kotta muzhikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu thiruduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu thiruduren"/>
</div>
<div class="lyrico-lyrics-wrapper">vatta vatta mogathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatta vatta mogathula"/>
</div>
<div class="lyrico-lyrics-wrapper">katta kattaa tholaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta kattaa tholaikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sathatha podamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathatha podamaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku muthatha koduthaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku muthatha koduthaley"/>
</div>
<div class="lyrico-lyrics-wrapper">michatha kaatamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="michatha kaatamaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku uchatha koduthaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku uchatha koduthaley"/>
</div>
<div class="lyrico-lyrics-wrapper">pitch ah than paakamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pitch ah than paakamaley"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa sixeru adichaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa sixeru adichaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch ah than podamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch ah than podamaley"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasa vachu than senjaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasa vachu than senjaley"/>
</div>
<div class="lyrico-lyrics-wrapper">open panna beer ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="open panna beer ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">summave pongadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summave pongadha"/>
</div>
<div class="lyrico-lyrics-wrapper">moodi vacha scotch ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodi vacha scotch ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thangidaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangidaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">raathiri paakura paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raathiri paakura paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhukuthe enna than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhukuthe enna than"/>
</div>
<div class="lyrico-lyrics-wrapper">pokiri aanen unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokiri aanen unna"/>
</div>
<div class="lyrico-lyrics-wrapper">pothika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothika than"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">sendhu kalakura naalachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendhu kalakura naalachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">aala mayakudhu un pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala mayakudhu un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachu paadhi vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachu paadhi vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalla thanama erangi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalla thanama erangi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandha varaikum laabam sethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha varaikum laabam sethu"/>
</div>
<div class="lyrico-lyrics-wrapper">sorga vaasal thorandhu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorga vaasal thorandhu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathu verthu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathu verthu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanathula kaanama ponenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanathula kaanama ponenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vittathula padukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittathula padukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta kotta muzhikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta kotta muzhikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu thiruduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu thiruduren"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">vatta vatta mogathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatta vatta mogathula"/>
</div>
<div class="lyrico-lyrics-wrapper">katta kattaa tholaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta kattaa tholaikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaakura vedikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaakura vedikura"/>
</div>
<div class="lyrico-lyrics-wrapper">pataasaa nee dhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pataasaa nee dhaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kolutha than thudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolutha than thudikira"/>
</div>
<div class="lyrico-lyrics-wrapper">mittasu naan dhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mittasu naan dhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyura neliyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyura neliyura"/>
</div>
<div class="lyrico-lyrics-wrapper">thannala nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannala nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagula tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagula tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un aalu naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aalu naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaala kathi kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaala kathi kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadathatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadathatha "/>
</div>
<div class="lyrico-lyrics-wrapper">odambu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu than"/>
</div>
<div class="lyrico-lyrics-wrapper">thangathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangathey"/>
</div>
<div class="lyrico-lyrics-wrapper">pechala patha vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pechala patha vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum "/>
</div>
<div class="lyrico-lyrics-wrapper">theekuchi nee thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theekuchi nee thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kathukita motha vitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukita motha vitha"/>
</div>
<div class="lyrico-lyrics-wrapper">eraki vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eraki vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">un jaathi naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jaathi naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">othukitu oru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othukitu oru dharam"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponjathi nee thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponjathi nee thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vittathula padukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittathula padukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta kotta muzhikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta kotta muzhikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu thiruduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu thiruduren"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">vatta vatta mogathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatta vatta mogathula"/>
</div>
<div class="lyrico-lyrics-wrapper">katta kattaa tholaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta kattaa tholaikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kola kola"/>
</div>
</pre>
