---
title: "vasantham poothathadi song lyrics"
album: "Methagu"
artist: "Praveen Kumar"
lyricist: "Thirukumaran"
director: "T. Kittu"
path: "/albums/methagu-lyrics"
song: "Vasantham Poothathadi"
image: ../../images/albumart/methagu.jpg
date: 2021-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i0c9pcKx5S4"
type: "happy"
singers:
  - Udhay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vasantham poothathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantham poothathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal eeri vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal eeri vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithai paadu chinna kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithai paadu chinna kuyile"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vayalil oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vayalil oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alagil selai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagil selai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanamadu vanna mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanamadu vanna mayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ini marangal kolusaninthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini marangal kolusaninthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulunga mani kilungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulunga mani kilungum"/>
</div>
<div class="lyrico-lyrics-wrapper">thulasi poo pookum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulasi poo pookum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu mugilgal keelirangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu mugilgal keelirangi"/>
</div>
<div class="lyrico-lyrics-wrapper">alagile kirangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagile kirangi"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiyil poochoodi pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiyil poochoodi pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vasantham poothathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantham poothathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal eeri vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal eeri vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithai paadu chinna kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithai paadu chinna kuyile"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vayalil oodi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vayalil oodi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alagil selai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagil selai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanamadu vanna mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanamadu vanna mayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maalai ilaveyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai ilaveyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai malai thannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai malai thannil"/>
</div>
<div class="lyrico-lyrics-wrapper">manjal poosum adi poosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal poosum adi poosum"/>
</div>
<div class="lyrico-lyrics-wrapper">man magalin poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man magalin poovai"/>
</div>
<div class="lyrico-lyrics-wrapper">alli veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">yaal pala magal thannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaal pala magal thannai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaana varumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaana varumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">veenai mudi kondu koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenai mudi kondu koovum"/>
</div>
<div class="lyrico-lyrics-wrapper">aval meni puthu vaasam veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval meni puthu vaasam veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theva magal mattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva magal mattu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaavi thanai thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaavi thanai thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">paadum isai nammai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadum isai nammai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">vayal yavum alagagi aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayal yavum alagagi aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vanni vaasal adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vanni vaasal adi"/>
</div>
<div class="lyrico-lyrics-wrapper">eri poga aval enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri poga aval enai"/>
</div>
<div class="lyrico-lyrics-wrapper">anaikindra neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaikindra neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil oori varum paasa eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil oori varum paasa eeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalai ilankaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai ilankaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum annarin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum annarin"/>
</div>
<div class="lyrico-lyrics-wrapper">alagemmai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagemmai kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">aval selai kadal thanai killum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval selai kadal thanai killum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada paalai nilam engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paalai nilam engal"/>
</div>
<div class="lyrico-lyrics-wrapper">thayin madi endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayin madi endru"/>
</div>
<div class="lyrico-lyrics-wrapper">aalai pidinthingu vaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai pidinthingu vaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">aval alagai paar endru koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval alagai paar endru koorum"/>
</div>
</pre>
