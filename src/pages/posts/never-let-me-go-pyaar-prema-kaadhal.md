---
title: "i will never let me go song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Oviya Oomapathy"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "I Will Never Let Me Go"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O8iVwjC69VY"
type: "sad"
singers:
  - Shweta Pandit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannae en kangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae en kangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneeraai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneeraai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangal maayamai pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangal maayamai pogatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum un nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum un nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan munnae thondruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan munnae thondruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ullam en kaadhalaal maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullam en kaadhalaal maaratho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaikkorthu naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkorthu naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandha paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandha paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endru kurai kooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endru kurai kooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuligal mannai sergaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuligal mannai sergaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu vaasam ennul ninaivoottuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu vaasam ennul ninaivoottuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatrilae mithakkum odamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrilae mithakkum odamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaiyai thedinen un paathathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaiyai thedinen un paathathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae en kangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae en kangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneeraai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneeraai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangal maayamaai pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangal maayamaai pogatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaa dhey aanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaa dhey aanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilai neeril moozhguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai neeril moozhguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila kaadhal kooruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila kaadhal kooruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli kooda thunaiyindri vaaduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli kooda thunaiyindri vaaduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaara mutkal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaara mutkal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai sutri varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai sutri varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazh kaalam nizhalindri vaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazh kaalam nizhalindri vaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamalae manangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamalae manangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthadhandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthadhandru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai solliyae murindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai solliyae murindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae kalaiyum megamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae kalaiyum megamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogiren thooramai ohoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogiren thooramai ohoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae en kangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae en kangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneerai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangal maayamaai pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangal maayamaai pogatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum un nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum un nyaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan munnae thondruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan munnae thondruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ullam en kaadhalal maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullam en kaadhalal maaratho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaikkorthu naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkorthu naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandha paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandha paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illai endru kurai kooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illai endru kurai kooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuligal mannai sergaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuligal mannai sergaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu vaasam ennul ninaivoottuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu vaasam ennul ninaivoottuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatrilae mithakkum odamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrilae mithakkum odamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaiyai thedinen un paathathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaiyai thedinen un paathathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae en kangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae en kangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneerai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangal maayamaai pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangal maayamaai pogatho"/>
</div>
</pre>
