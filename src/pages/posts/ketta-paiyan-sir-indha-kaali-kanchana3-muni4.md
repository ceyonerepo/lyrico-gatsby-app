---
title: "ketta paiyan sir indha kaali song lyrics"
album: "Kanchana3-Muni4"
artist: "S. Thaman"
lyricist: "Madhan Karky"
director: "Raghava Lawrence"
path: "/albums/kanchana3-muni4-lyrics"
song: "Ketta Paiyan Sir Indha Kaali"
image: ../../images/albumart/kanchana3-muni4.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QoXRpEvV3l8"
type: "Mass Intro"
singers:
  - Dhivagaran Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Enga Pakkam Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Enga Pakkam Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavumey Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumey Jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Thala Sir Indha kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Thala Sir Indha kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Vandhu Mothuruvan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandhu Mothuruvan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Thala Sir Indha Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Thala Sir Indha Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Vandhu Mothuruvan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandhu Mothuruvan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Paiyan Sir Indha Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Paiyan Sir Indha Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaetti Katti Nikkum Enga Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaetti Katti Nikkum Enga Veli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella Mudi Singamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Mudi Singamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Kooda Thangamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Kooda Thangamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkum Munney Alli Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkum Munney Alli Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karnan Paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karnan Paarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaya Pola Pullaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaya Pola Pullaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallam Edhum Illayadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallam Edhum Illayadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovamaana Paasakkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovamaana Paasakkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verayaarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verayaarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyaattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyaattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Gaali Gaali Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Gaali Gaali Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhuravan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhuravan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Enga Pakkam Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Enga Pakkam Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavumey Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumey Jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Piyan Sir Indha Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Piyan Sir Indha Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti Katti Nikkum Enga Vaeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Katti Nikkum Enga Vaeli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Kaal Illaati Oonam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kaal Illaati Oonam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Illatti Muttal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Illatti Muttal Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodutthaa Ennaikkum Nashtam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodutthaa Ennaikkum Nashtam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mattum Jeichaa Vetri Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Jeichaa Vetri Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenachadhellaam Adanjapinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachadhellaam Adanjapinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisiyila Edhu Nilaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyila Edhu Nilaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannukulla Pona Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukulla Pona Pinnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmam Unnai Vaazhaveikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam Unnai Vaazhaveikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiyil Vena Konjamum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Vena Konjamum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodukkathaan Manasirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukkathaan Manasirundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Raaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyaattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyaattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Gaali Gaali Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Gaali Gaali Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhuravan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhuravan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Enga Pakkam Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Enga Pakkam Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavumey Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumey Jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Paakaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Paakaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodukkanum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukkanum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorey Paakathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkanum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkanum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezha Mel Yaarum Kaiya Vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezha Mel Yaarum Kaiya Vechaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Aagunnu Kaatanundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aagunnu Kaatanundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhaviellam Udhavidathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaviellam Udhavidathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayamuruttha Adhu Edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamuruttha Adhu Edhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panabalamey Uyir Tharathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panabalamey Uyir Tharathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhiparikka Adhu Edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhiparikka Adhu Edhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatchiyum Venaa Kottaiyum Venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchiyum Venaa Kottaiyum Venaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalladhu Senjaaley Neeye Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhu Senjaaley Neeye Raaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyaattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyaattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Gaali Gaali Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Gaali Gaali Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhuravan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhuravan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Kaali Kaali Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Kaali Kaali Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyattam Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyattam Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Enga Pakkam Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Enga Pakkam Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavumey Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumey Jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Thala Sir Indha Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Thala Sir Indha Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Vandhu Mothuruvan Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandhu Mothuruvan Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Paiyan Sir Indha Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Paiyan Sir Indha Kaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaetti Katti Nikkum Enga Vaeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaetti Katti Nikkum Enga Vaeli"/>
</div>
</pre>
