---
title: "hey bro song lyrics"
album: "Lift"
artist: "Britto Michael"
lyricist: "Nishanth"
director: "Vineeth Varaprasad"
path: "/albums/lift-lyrics"
song: "Hey Bro"
image: ../../images/albumart/lift.jpg
date: 2021-10-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HGZwK-gzaRA"
type: "happy"
singers:
  - Krithika Nelson
  - Ben Kingsley
  - Adheef Muhamed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">It’s hard to see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s hard to see"/>
</div>
<div class="lyrico-lyrics-wrapper">What I’m trying to be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What I’m trying to be"/>
</div>
<div class="lyrico-lyrics-wrapper">Quit dissing me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quit dissing me"/>
</div>
<div class="lyrico-lyrics-wrapper">You ain’t better than me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You ain’t better than me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It’s hard to see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s hard to see"/>
</div>
<div class="lyrico-lyrics-wrapper">What I’m trying to be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What I’m trying to be"/>
</div>
<div class="lyrico-lyrics-wrapper">Quit dissing me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quit dissing me"/>
</div>
<div class="lyrico-lyrics-wrapper">You ain’t better than me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You ain’t better than me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind loud sleeves now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind loud sleeves now"/>
</div>
<div class="lyrico-lyrics-wrapper">Bars how Thoughts now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bars how Thoughts now"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind loud sleeves now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind loud sleeves now"/>
</div>
<div class="lyrico-lyrics-wrapper">Bars how Thoughts now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bars how Thoughts now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">punnaagai ennu paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnaagai ennu paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaal puvi agum nam vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaal puvi agum nam vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai ennum paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai ennum paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaal puvi agum nam vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaal puvi agum nam vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">Paleechidum muhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paleechidum muhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarthidum viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarthidum viralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarndhidum nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarndhidum nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhidu ulagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhidu ulagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virithidu chiragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virithidu chiragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirandharam endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandharam endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru punnagai pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru punnagai pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I thought we were strangers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I thought we were strangers"/>
</div>
<div class="lyrico-lyrics-wrapper">in the first place
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in the first place"/>
</div>
<div class="lyrico-lyrics-wrapper">I knew that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I knew that"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby you said
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby you said"/>
</div>
<div class="lyrico-lyrics-wrapper">Make it quick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make it quick"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah I heard that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah I heard that"/>
</div>
<div class="lyrico-lyrics-wrapper">Maybe you found
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maybe you found"/>
</div>
<div class="lyrico-lyrics-wrapper">Something in me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Something in me"/>
</div>
<div class="lyrico-lyrics-wrapper">I kinda know that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I kinda know that"/>
</div>
<div class="lyrico-lyrics-wrapper">My heart was missing a piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My heart was missing a piece"/>
</div>
<div class="lyrico-lyrics-wrapper">You really knew that?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You really knew that?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paarkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enai theydineyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enai theydineyn"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ponn muruvalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ponn muruvalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiye tholakireyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiye tholakireyn"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithai oru punnaga veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithai oru punnaga veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival ena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melidhaai our edaiveli yene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melidhaai our edaiveli yene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketkireyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketkireyn"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithaai oru punnagai veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithaai oru punnagai veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulur nilavo ival vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulur nilavo ival vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Melidhaai our edaiveli yene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melidhaai our edaiveli yene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketkireyn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketkireyn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paleechidum muhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paleechidum muhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarthidum viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarthidum viralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarndhidum nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarndhidum nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhidu ulagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhidu ulagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virithidu chiragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virithidu chiragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirandharam endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandharam endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">punnaagai ennu paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnaagai ennu paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaal puvi agum nam vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaal puvi agum nam vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai ennum paravasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai ennum paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaal puvi agum nam vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaal puvi agum nam vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bro!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bro!"/>
</div>
</pre>
