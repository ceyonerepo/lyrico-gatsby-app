---
title: "rich rich song lyrics"
album: "Mahaan"
artist: "Santhosh Narayanan"
lyricist: "Durai"
director: "Karthik Subbaraj"
path: "/albums/mahaan-lyrics"
song: "Rich Rich"
image: ../../images/albumart/mahaan.jpg
date: 2022-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/X431TeHxEdQ"
type: "happy"
singers:
  - OfRo
  - Durai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naadu poora basic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu poora basic"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir u ondi classic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir u ondi classic"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu poora basic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu poora basic"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir u ondi classic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir u ondi classic"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaas vandhathum taste maaripochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas vandhathum taste maaripochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Car maadi veedu pakkathula beach u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car maadi veedu pakkathula beach u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna salayile annanoda statue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna salayile annanoda statue"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina sabhayila kekkuraanga speech u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina sabhayila kekkuraanga speech u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini yen banee ini yen banee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yen banee ini yen banee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkachakkamaana company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakkamaana company"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnamaari illa time maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnamaari illa time maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaporen ini vere maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaporen ini vere maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhishta lakshmi she touch me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhishta lakshmi she touch me"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraiya pichikkittu mothama kotri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraiya pichikkittu mothama kotri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala pathithaan ooru full ah pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala pathithaan ooru full ah pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily suthi podanum kannu padra stage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily suthi podanum kannu padra stage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laksha lakhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laksha lakhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu vandhu koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu vandhu koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mood irundhaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mood irundhaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyanguven perumbaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyanguven perumbaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shortcut la vandhabana irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shortcut la vandhabana irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Leabe vittadhile enakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leabe vittadhile enakku "/>
</div>
<div class="lyrico-lyrics-wrapper">naam oru naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam oru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha naatile too much jenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha naatile too much jenam"/>
</div>
1% <div class="lyrico-lyrics-wrapper">kitta yekecheka panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta yekecheka panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootikazhchu paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootikazhchu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum andha yenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum andha yenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaalum pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaas vandhathum taste maaripochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas vandhathum taste maaripochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Car maadi veedu pakkathula beach u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car maadi veedu pakkathula beach u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna salayile annanoda statue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna salayile annanoda statue"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina sabhayila kekkuraanga speech u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina sabhayila kekkuraanga speech u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaas vandhathum taste maaripochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas vandhathum taste maaripochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Car maadi veedu pakkathula beach u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car maadi veedu pakkathula beach u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna salayile annanoda statue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna salayile annanoda statue"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina sabhayila kekkuraanga speech u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina sabhayila kekkuraanga speech u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
30 <div class="lyrico-lyrics-wrapper">day la panakkaran aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="day la panakkaran aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppen naa oru online course
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppen naa oru online course"/>
</div>
<div class="lyrico-lyrics-wrapper">Warner bros oda padathula nolan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Warner bros oda padathula nolan"/>
</div>
<div class="lyrico-lyrics-wrapper">Calling me for mukkidamana roles
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calling me for mukkidamana roles"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Big bank la loan vaangi oda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Big bank la loan vaangi oda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku namma sattathula holes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku namma sattathula holes"/>
</div>
<div class="lyrico-lyrics-wrapper">Messy ah easy ah kaasu koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Messy ah easy ah kaasu koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluven podra goals
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluven podra goals"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just 20 billion loss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just 20 billion loss"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillara kaas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillara kaas"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacation nukku poven vegas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacation nukku poven vegas"/>
</div>
<div class="lyrico-lyrics-wrapper">Villa suthi safety ku alsation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villa suthi safety ku alsation"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna suthi suthi paaru naan sensation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna suthi suthi paaru naan sensation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen banee ini yen banee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen banee ini yen banee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkacakkamaana company
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkacakkamaana company"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnamaari illa time maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnamaari illa time maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Panna poren ini vera maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna poren ini vera maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa already rich rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa already rich rich"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaas panam nammakkitta vandhu rich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaas panam nammakkitta vandhu rich"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadu poora basic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu poora basic"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir u ondi classic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir u ondi classic"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu poora basic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu poora basic"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir u ondi classic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir u ondi classic"/>
</div>
</pre>
