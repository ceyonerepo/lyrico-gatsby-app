---
title: "gurkha theme song lyrics"
album: "Gurkha"
artist: "Raj Aryan"
lyricist: "Raj Aryan"
director: "Sam Anton"
path: "/albums/gurkha-lyrics"
song: "Gurkha Theme"
image: ../../images/albumart/gurkha.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NFTVwvz6akU"
type: "theme"
singers:
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbilla Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbilla Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirilla Pinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilla Pinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathaanin Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathaanin Gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyadum Ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyadum Ratham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin Vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Pali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Pali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithiyin Sathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyin Sathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemai Nanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai Nanmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam Dharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Dharmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhchi Soolchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhchi Soolchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellam Yennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellam Yennul"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanguvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanguvathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalbhavan Naane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalbhavan Naane Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarithiratha Nalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiratha Nalla "/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi Paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Namuku Solli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Namuku Solli "/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthathu Onnae Onnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthathu Onnae Onnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vazhanumna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vazhanumna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Venalum Eppo Venalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Venalum Eppo Venalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Vittu Munadi Venaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Vittu Munadi Venaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Security Ah Nikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Security Ah Nikkalaam"/>
</div>
</pre>
