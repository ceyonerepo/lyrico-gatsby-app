---
title: "sarkaru vaari paata title song song lyrics"
album: "Sarkaru Vaari Paata"
artist: "S Thaman"
lyricist: "Ananta Sriram"
director: "Parasuram"
path: "/albums/sarkaru-vaari-paata-lyrics"
song: "Sarkaru Vaari Paata - Title Song"
image: ../../images/albumart/sarkaru-vaari-paata.jpg
date: 2022-05-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EqyKcZibh-Y"
type: "title track"
singers:
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sara Sara Sara Sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Sara Sara Sara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Shuru Shuru Annadu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shuru Shuru Annadu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Alluri Vaari Beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluri Vaari Beta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara Sara Sara Sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Sara Sara Sara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ira Gira Gistadu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ira Gira Gistadu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvalsinodi Quota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvalsinodi Quota"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soft’u Gunnadanantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soft’u Gunnadanantha "/>
</div>
<div class="lyrico-lyrics-wrapper">Sambaralu Poka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambaralu Poka"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapu Cheyyalsi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapu Cheyyalsi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaste Aagipoddi Keka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaste Aagipoddi Keka"/>
</div>
<div class="lyrico-lyrics-wrapper">Eela Kottentala Yelamesthadata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eela Kottentala Yelamesthadata"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadu Addhochina Maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadu Addhochina Maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagili Pagili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagili Pagili "/>
</div>
<div class="lyrico-lyrics-wrapper">Pagili Padunata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagili Padunata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Weapons Leni Veta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weapons Leni Veta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Reverse Leni Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reverse Leni Baata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Weapons Leni Veta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weapons Leni Veta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarkaru Vaari Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkaru Vaari Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Reverse Leni Baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reverse Leni Baata"/>
</div>
</pre>
