---
title: "na bujjiKonda song lyrics"
album: "Runam"
artist: "S.V. Mallik Teja"
lyricist: "S.V. Mallik Teja"
director: "S. Gundreddy"
path: "/albums/runam-lyrics"
song: "Na BujjiKonda"
image: ../../images/albumart/runam.jpg
date: 2019-04-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Bz5S_jc4erI"
type: "love"
singers:
  - Hemachandra
  - Swapna Niranjan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naa bujjikonda naa chikkbonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bujjikonda naa chikkbonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenundalene ninnu soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenundalene ninnu soodakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa thenekonda bangarukonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa thenekonda bangarukonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaagalene maatadakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaagalene maatadakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">maatadakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatadakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emiseyyulene neeku seppakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emiseyyulene neeku seppakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">elukuntaane innu thappakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elukuntaane innu thappakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey nuvve na pooladanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey nuvve na pooladanda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na prema jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na prema jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na golukonda pillagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na golukonda pillagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">are nuvve na kallaninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="are nuvve na kallaninda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na gunde ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na gunde ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">unde yetellakunda chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unde yetellakunda chinnadaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bujjikonda naa chikkbonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bujjikonda naa chikkbonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenundalene ninnu soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenundalene ninnu soodakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee vypu choodakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vypu choodakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">naa choopulo chimma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa choopulo chimma "/>
</div>
<div class="lyrico-lyrics-wrapper">cheekatlu cherukuntaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekatlu cherukuntaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa venta neevunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa venta neevunte"/>
</div>
<div class="lyrico-lyrics-wrapper">na chitti yedalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na chitti yedalo "/>
</div>
<div class="lyrico-lyrics-wrapper">raketlu dusukeltaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raketlu dusukeltaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oooo ammathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooo ammathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu veedaleney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu veedaleney"/>
</div>
<div class="lyrico-lyrics-wrapper">maa amma lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa amma lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">soosukuntaney  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soosukuntaney  "/>
</div>
<div class="lyrico-lyrics-wrapper">aa brahmakyna chancu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa brahmakyna chancu"/>
</div>
<div class="lyrico-lyrics-wrapper">luuvalene nee ratha marchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="luuvalene nee ratha marchi"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu rasukuntaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu rasukuntaney "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bujjikonda naa chikkbonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bujjikonda naa chikkbonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenundalene ninnu soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenundalene ninnu soodakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee okka chinna navvukoraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee okka chinna navvukoraku"/>
</div>
<div class="lyrico-lyrics-wrapper">lekkaleni rojulyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lekkaleni rojulyna"/>
</div>
<div class="lyrico-lyrics-wrapper">hayigu eduru chusthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hayigu eduru chusthaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee chekkaranti matalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chekkaranti matalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">okktante okkatyna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okktante okkatyna "/>
</div>
<div class="lyrico-lyrics-wrapper">prananne theesi isthaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prananne theesi isthaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo nee kantipapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo nee kantipapa"/>
</div>
<div class="lyrico-lyrics-wrapper">reppanavuthaney ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reppanavuthaney ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">santipalekka soosukuntaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santipalekka soosukuntaney"/>
</div>
<div class="lyrico-lyrics-wrapper">naa intidaanni sesuukuntaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa intidaanni sesuukuntaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">aa kante baga ninnu soosukuntaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa kante baga ninnu soosukuntaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bujjikonda naa chikkbonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bujjikonda naa chikkbonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenundalene ninnu soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenundalene ninnu soodakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa thenekonda bangarukonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa thenekonda bangarukonda"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaagalene maatadakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaagalene maatadakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">maatadakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatadakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emiseyyulene neeku seppakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emiseyyulene neeku seppakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">elukuntaane innu thappakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elukuntaane innu thappakunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey nuvve na pooladanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey nuvve na pooladanda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na prema jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na prema jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na golukonda pillagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na golukonda pillagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">are nuvve na kallaninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="are nuvve na kallaninda"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve na gunde ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve na gunde ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">unde yetellakunda chinnadaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unde yetellakunda chinnadaana"/>
</div>
</pre>
