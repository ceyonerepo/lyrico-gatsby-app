---
title: "nalla nanban vendum endru song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Na Muthukumar"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "Nalla Nanban Vendum Endru"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GN4V3hAEclI"
type: "Sad"
singers:
  - Ramakrishnan Murthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nalla Nanban Vendum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nanban Vendum Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Maranamum Ninaikkindratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Maranamum Ninaikkindratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siranthavan Neethaan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siranthavan Neethaan Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kootti Sella Thudikkindratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kootti Sella Thudikkindratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivane Iraivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivane Iraivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Uyir Venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Uyir Venduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Uyir Eduthukkol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Uyir Eduthukkol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkathu Pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkathu Pothuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Engal Roja Chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Engal Roja Chedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Maranam Thinbatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Maranam Thinbatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Siriththu Pesum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Siriththu Pesum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Vendinom Meendumtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Vendinom Meendumtha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ninaivin Thaazhvaaraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivin Thaazhvaaraththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Kural Konjam Ketkavillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Kural Konjam Ketkavillaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamennum Melmadaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamennum Melmadaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Niyabagangal Pookkavillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Niyabagangal Pookkavillaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivane Iraivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivane Iraivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkillai Irakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkillai Irakkamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Ival Azhu Kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Ival Azhu Kural"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Pinnum Urakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Pinnum Urakkamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Nanba Vaa Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Nanba Vaa Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhgalil Saaya Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhgalil Saaya Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhnthidum Naal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthidum Naal Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnai Thaangavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Thaangavaa"/>
</div>
</pre>
