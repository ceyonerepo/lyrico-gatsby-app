---
title: "karakaan murakaan song lyrics"
album: "Margazhi 16"
artist: "E.K. Bobby"
lyricist: "Priyan"
director: "K. Stephen"
path: "/albums/margazhi-16-lyrics"
song: "Karakaan Murakaan"
image: ../../images/albumart/margazhi-16.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tkNAsvPefqo"
type: "happy"
singers:
  - Shankar Mahadevan
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kambarakkattu vaangiththarattaa karakkammurukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambarakkattu vaangiththarattaa karakkammurukkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kambarakkattu vaangiththarattaa karakkammurukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambarakkattu vaangiththarattaa karakkammurukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakkaakkadi kadichithaadaa karakkammurukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakkaakkadi kadichithaadaa karakkammurukkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Skechchi pencil vaangiththaaren karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Skechchi pencil vaangiththaaren karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee meke up pottu azhagum kaattu karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee meke up pottu azhagum kaattu karakkaammurukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edakku modakku pannaadhedaa karakkaamurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku modakku pannaadhedaa karakkaamurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">un sandiththanam sellaadhedaa karakkaamurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sandiththanam sellaadhedaa karakkaamurukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">panjummittaai vaangitharattaa karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjummittaai vaangitharattaa karakkaammurukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhippichchi thinnuttu thaadaa karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhippichchi thinnuttu thaadaa karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhippichchi thinnuttu thaadaa karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhippichchi thinnuttu thaadaa karakkaammurukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkachekka aasavachi unnathedi naanum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachekka aasavachi unnathedi naanum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">romba biguppannaadhedi menaa minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba biguppannaadhedi menaa minukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei ekkuththappaa pesikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei ekkuththappaa pesikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">summaa setta pannikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summaa setta pannikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">engitta thaan kaattaadhadaa neeyum minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engitta thaan kaattaadhadaa neeyum minukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppu mootta onnaththookka enna tharattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu mootta onnaththookka enna tharattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badavaa nee kelambu kelambu kaaththu varattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badavaa nee kelambu kelambu kaaththu varattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkuppudikka vachchipputtu nadikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkuppudikka vachchipputtu nadikkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei surukkuppoattu enna nee thaan izhukkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei surukkuppoattu enna nee thaan izhukkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kambarakkattu vaangiththarattaa karakkammurukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambarakkattu vaangiththarattaa karakkammurukkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan ettumaadi thalathai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ettumaadi thalathai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kuththagaikku eduththu vachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuththagaikku eduththu vachchi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai imsappannuriye kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai imsappannuriye kalavaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei viththaiyellaam kaththuvachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei viththaiyellaam kaththuvachchi"/>
</div>
<div class="lyrico-lyrics-wrapper">mallukkatta naanum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallukkatta naanum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">rombathaandi salichikkira gundhaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rombathaandi salichikkira gundhaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratta suzhikkaaran neethaan killaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratta suzhikkaaran neethaan killaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei rattai jadaikkaari neethaan vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei rattai jadaikkaari neethaan vaayendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O summaa vandhu vambizhuththu moraikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O summaa vandhu vambizhuththu moraikkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O poiyaakkooda enna vittu vilagaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O poiyaakkooda enna vittu vilagaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karakkaammurukkaan karakkammurukkan karakkaammurukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karakkaammurukkaan karakkammurukkan karakkaammurukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kambarakkattu vaangiththarattaa karakkammurukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambarakkattu vaangiththarattaa karakkammurukkan"/>
</div>
</pre>
