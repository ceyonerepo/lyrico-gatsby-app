---
title: "raaye raaye song lyrics"
album: "Touch Chesi Chudu"
artist: "JAM 8 - Mani Sharma"
lyricist: "Kasarla Shyam"
director: "Vikram Sirikonda"
path: "/albums/touch-chesi-chudu-lyrics"
song: "Raaye Raaye"
image: ../../images/albumart/touch-chesi-chudu.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XAvuaml0tRg"
type: "love"
singers:
  - Nakash Aziz
  - Madhu Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Sukka Bottu Sotta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sukka Bottu Sotta "/>
</div>
<div class="lyrico-lyrics-wrapper">Buggaku Pettavey Naanchaaree 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buggaku Pettavey Naanchaaree "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Kattu Batta Thotey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Kattu Batta Thotey "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukapothaa Ballaaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukapothaa Ballaaree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sukka Bottu Sotta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sukka Bottu Sotta "/>
</div>
<div class="lyrico-lyrics-wrapper">Buggaku Pettavey Naanchaaree 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buggaku Pettavey Naanchaaree "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Kattu Batta Thotey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Kattu Batta Thotey "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukapothaa Ballaaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukapothaa Ballaaree"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Panche Kattu Kattara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Panche Kattu Kattara "/>
</div>
<div class="lyrico-lyrics-wrapper">Pilagaa Vasthaa Nee Dhaaree 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilagaa Vasthaa Nee Dhaaree "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kanchelanney Thenchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kanchelanney Thenchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paruguna Cheddham Savvaaree 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruguna Cheddham Savvaaree "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ole Ole Ole Oyyari Hampee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ole Ole Ole Oyyari Hampee "/>
</div>
<div class="lyrico-lyrics-wrapper">Okariki Okarayyamey Selfie 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okariki Okarayyamey Selfie "/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala Sayyata Nerpee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala Sayyata Nerpee "/>
</div>
<div class="lyrico-lyrics-wrapper">Allinaavey Soofi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allinaavey Soofi "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Bhale Bhale Bhale Majakulaapi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bhale Bhale Bhale Majakulaapi "/>
</div>
<div class="lyrico-lyrics-wrapper">Melaalaku Meghaalu Dhimpi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melaalaku Meghaalu Dhimpi "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaaluku Thaaralni Thempi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaaluku Thaaralni Thempi "/>
</div>
<div class="lyrico-lyrics-wrapper">Sallukuntey Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallukuntey Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poddhugala Sallagali Olai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhugala Sallagali Olai "/>
</div>
<div class="lyrico-lyrics-wrapper">Navi Jivvuna Gundey Gunjinavey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navi Jivvuna Gundey Gunjinavey "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Manjilvi Nuvvey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Manjilvi Nuvvey "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Praanam Needheney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Praanam Needheney "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Koppuna Mallelalu Theeruna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Koppuna Mallelalu Theeruna "/>
</div>
<div class="lyrico-lyrics-wrapper">Guppuna Maikam Thestiviro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppuna Maikam Thestiviro "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gurthullo Neney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gurthullo Neney "/>
</div>
<div class="lyrico-lyrics-wrapper">Attarulagaa Maarinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attarulagaa Maarinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Kila Kila Kila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kila Kila Kila "/>
</div>
<div class="lyrico-lyrics-wrapper">Singaari Silaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaari Silaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala Bangari Gilaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala Bangari Gilaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Selakalabadi Nee Yenta Uraka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selakalabadi Nee Yenta Uraka "/>
</div>
<div class="lyrico-lyrics-wrapper">Samburaala Yenaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samburaala Yenaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Chura Chura Chura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chura Chura Chura "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Choopu Churaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Choopu Churaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadimindhira Nadumula Melika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadimindhira Nadumula Melika "/>
</div>
<div class="lyrico-lyrics-wrapper">Adugatu Pade Inkaaga Leka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugatu Pade Inkaaga Leka "/>
</div>
<div class="lyrico-lyrics-wrapper">Attha Inti Daakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attha Inti Daakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Love Maggam Neskoye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Maggam Neskoye "/>
</div>
<div class="lyrico-lyrics-wrapper">O Raaye Itu Raaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Raaye Itu Raaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Laggam Cheskoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Laggam Cheskoye"/>
</div>
</pre>
