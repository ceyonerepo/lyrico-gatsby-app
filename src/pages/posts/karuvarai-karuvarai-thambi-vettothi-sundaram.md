---
title: "karuvarai karuvarai song lyrics"
album: "Thambi Vettothi Sundaram"
artist: "Vidyasagar"
lyricist: "Vairamuthu"
director: "V.C. Vadivudaiyan"
path: "/albums/thambi-vettothi-sundaram-lyrics"
song: "Karuvarai Karuvarai"
image: ../../images/albumart/thambi-vettothi-sundaram.jpg
date: 2011-11-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vPAWskhcDH0"
type: "mass"
singers:
  - Palakaddu Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aah aah haa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah aah haa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah aah haa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah aah haa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai karuvarai thodangumidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai karuvarai thodangumidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallarai kallarai adangumidam oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallarai kallarai adangumidam oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal paasangal yenbadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal paasangal yenbadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanavan vasadhikku thangumidam oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanavan vasadhikku thangumidam oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhil kottai poattavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil kottai poattavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottai aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattai pottavan paraman aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattai pottavan paraman aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukkiyum oru naal buththan aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukkiyum oru naal buththan aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maaralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharmam naalaikku thappu aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam naalaikku thappu aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu enbadhae dharmam aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu enbadhae dharmam aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu maatramae ozhukkam aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu maatramae ozhukkam aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maaralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam unnodu irukkira podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam unnodu irukkira podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti sellum gyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti sellum gyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam yellaam vittu poana pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam yellaam vittu poana pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta kitta varum gyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta kitta varum gyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai karuvarai thodangumidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai karuvarai thodangumidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallarai kallarai adangumidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallarai kallarai adangumidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann puzhuvo mann 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann puzhuvo mann "/>
</div>
<div class="lyrico-lyrics-wrapper">puzhuvo mannai thingidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puzhuvo mannai thingidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha mannai thinnum mann 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha mannai thinnum mann "/>
</div>
<div class="lyrico-lyrics-wrapper">puzhuva thavalai thingidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puzhuva thavalai thingidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuva thinnum thavalaiyathaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuva thinnum thavalaiyathaan "/>
</div>
<div class="lyrico-lyrics-wrapper">paambu thingudhuMaela parandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paambu thingudhuMaela parandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">pogum kazhugu andha paamba thingidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum kazhugu andha paamba thingidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paamba thinnum kazhugath thaanae nariyum thingidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paamba thinnum kazhugath thaanae nariyum thingidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha nariya kooda vettaiyaadi manushan thinguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha nariya kooda vettaiyaadi manushan thinguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha manushanathaan kadaisiyilae mannu thingudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha manushanathaan kadaisiyilae mannu thingudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha manna purinja manushanukku gyanam pongudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manna purinja manushanukku gyanam pongudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna purinja manushanukku gyanam pongudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna purinja manushanukku gyanam pongudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam unnodu irukkira podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam unnodu irukkira podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti sellum gyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti sellum gyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam yellaam vittu poana pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam yellaam vittu poana pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta kitta varum gyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta kitta varum gyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai karuvarai thodangumidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai karuvarai thodangumidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallarai kallarai adangumidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallarai kallarai adangumidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam thaedi inbam thaedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam thaedi inbam thaedi "/>
</div>
<div class="lyrico-lyrics-wrapper">manasu alaiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu alaiyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Inbam thaedum paadhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Inbam thaedum paadhai "/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam thunbam aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam thunbam aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Garvam saerndhu manushan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garvam saerndhu manushan "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvil karai pidikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvil karai pidikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thoalvi endra kallil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thoalvi endra kallil "/>
</div>
<div class="lyrico-lyrics-wrapper">adichi thovaichi kudukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichi thovaichi kudukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal paasam kudumbam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal paasam kudumbam "/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam kaala suththudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam kaala suththudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuga parichchai ezhudha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuga parichchai ezhudha "/>
</div>
<div class="lyrico-lyrics-wrapper">vacha piragae paadam solludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha piragae paadam solludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti pora vaazhvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti pora vaazhvil "/>
</div>
<div class="lyrico-lyrics-wrapper">enna irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna irukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa mutti mutti paarkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa mutti mutti paarkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvu muzhumai aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvu muzhumai aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa mutti mutti paarkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa mutti mutti paarkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvu muzhumai aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvu muzhumai aagudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam unnodu irukkira podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam unnodu irukkira podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti sellum gyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti sellum gyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam yellaam vittu poana pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam yellaam vittu poana pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta kitta varum gyanam oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta kitta varum gyanam oo"/>
</div>
</pre>
