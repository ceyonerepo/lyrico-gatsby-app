---
title: "adada daa song lyrics"
album: "Kaathadi"
artist: "R. Pavan - Deepan B"
lyricist: "Mohan Rajan"
director: "Kalyaan"
path: "/albums/kaathadi-song-lyrics"
song: "Adada daa"
image: ../../images/albumart/kaathadi.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZLr9Y50UD6o"
type: "happy"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adada daa nilavai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada daa nilavai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">atharku innum alagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharku innum alagu "/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu uyirile thudaithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu uyirile thudaithu "/>
</div>
<div class="lyrico-lyrics-wrapper">unaku tharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku tharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alaigalin nuraiyai virithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigalin nuraiyai virithu"/>
</div>
<div class="lyrico-lyrics-wrapper">athil oru padukai amaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil oru padukai amaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">chellam nee thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellam nee thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">alagai rasikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai rasikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee antha vaanathu megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee antha vaanathu megam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrellam unnidam konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrellam unnidam konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">noor aandu nee vaazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noor aandu nee vaazha "/>
</div>
<div class="lyrico-lyrics-wrapper">pookal ellam kadavulai kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal ellam kadavulai kenjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee antha vaanathu megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee antha vaanathu megam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrellam unnidam konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrellam unnidam konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">noor aandu nee vaazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noor aandu nee vaazha "/>
</div>
<div class="lyrico-lyrics-wrapper">pookal ellam kadavulai kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal ellam kadavulai kenjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada daa nilavai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada daa nilavai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">atharku innum alagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharku innum alagu "/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu uyirile thudaithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu uyirile thudaithu "/>
</div>
<div class="lyrico-lyrics-wrapper">unaku tharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku tharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alaigalin nuraiyai virithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigalin nuraiyai virithu"/>
</div>
<div class="lyrico-lyrics-wrapper">athil oru padukai amithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil oru padukai amithu"/>
</div>
<div class="lyrico-lyrics-wrapper">chellam nee thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellam nee thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">alagai rasikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai rasikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unathu uyiril en yuir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu uyiril en yuir"/>
</div>
<div class="lyrico-lyrics-wrapper">iruku nee thuli amutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruku nee thuli amutham"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum enaku ulagam athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum enaku ulagam athu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu virinju irukum en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu virinju irukum en"/>
</div>
<div class="lyrico-lyrics-wrapper">devathaiye sogam etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathaiye sogam etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">en chella oviyam nee allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en chella oviyam nee allava"/>
</div>
<div class="lyrico-lyrics-wrapper">en mouna kaaviyam nee allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mouna kaaviyam nee allava"/>
</div>
<div class="lyrico-lyrics-wrapper">en ulagam enbathu nee allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ulagam enbathu nee allava"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaal thadam nerungira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaal thadam nerungira"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalanuku idam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalanuku idam illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada daa nilavai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada daa nilavai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">atharku innum alagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharku innum alagu "/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu uyirile thudaithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu uyirile thudaithu "/>
</div>
<div class="lyrico-lyrics-wrapper">unaku tharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku tharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en jenmam muluthanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en jenmam muluthanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unai partha pinbu thana di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai partha pinbu thana di"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanne en rathiname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanne en rathiname "/>
</div>
<div class="lyrico-lyrics-wrapper">nan meesai vaitha thai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan meesai vaitha thai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna chinnathai adi eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinnathai adi eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne boomi alanthathal kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne boomi alanthathal kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi unai vitu pirinthidum mani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unai vitu pirinthidum mani"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli than en jeevan nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli than en jeevan nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum nimisam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum nimisam adi"/>
</div>
</pre>
