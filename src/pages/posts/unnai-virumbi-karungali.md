---
title: "unnai virumbi song lyrics"
album: "Karungali"
artist: "Srikanth Deva"
lyricist: "Erode Erivan"
director: "Kalanjiyam"
path: "/albums/karungali-lyrics"
song: "Unnai Virumbi"
image: ../../images/albumart/karungali.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RWCTRWjyZcY"
type: "love"
singers:
  - Naresh Iyer
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai nerungi virumbi varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nerungi virumbi varuveney"/>
</div>
<div class="lyrico-lyrics-wrapper">un nizhalaai nizhalaai thodarveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nizhalaai nizhalaai thodarveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ninaivil alaiveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivil alaiveney"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhagil tholaiveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhagil tholaiveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sinna chinna asaivugal rasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sinna chinna asaivugal rasithen"/>
</div>
<div class="lyrico-lyrics-wrapper">un anbaikkandu anbey naan viyandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un anbaikkandu anbey naan viyandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ullam konjam soarndhaal soarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ullam konjam soarndhaal soarven"/>
</div>
<div class="lyrico-lyrics-wrapper">un idhazhadhu malarndhaal malarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhazhadhu malarndhaal malarven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini neeyindri naan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neeyindri naan illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini naan indri nee illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naan indri nee illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai virumbi unnai virumbi varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai virumbi unnai virumbi varuveney"/>
</div>
<div class="lyrico-lyrics-wrapper">un nizhalaai nizhalaai thodarveny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nizhalaai nizhalaai thodarveny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ninaivil ninaivil alaiveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivil ninaivil alaiveney"/>
</div>
<div class="lyrico-lyrics-wrapper">un zhagil tholaiveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un zhagil tholaiveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaikandu kandu kaadhal kondu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikandu kandu kaadhal kondu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">konji konji ennai allicheilgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji konji ennai allicheilgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochukaatril kaatril nenjikkulley ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukaatril kaatril nenjikkulley ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyirai kaayam neeyum seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyirai kaayam neeyum seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marubadiyum marubadiyum unakkaaga pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadiyum marubadiyum unakkaaga pirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo Ohoo en manadhil en manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo Ohoo en manadhil en manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai sumappeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai sumappeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un chinna chinna asaivugal asaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un chinna chinna asaivugal asaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">un anbai kandu anbey naan viyandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un anbai kandu anbey naan viyandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OhO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">un moagam konjam soagamthaan sumapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moagam konjam soagamthaan sumapen"/>
</div>
<div class="lyrico-lyrics-wrapper">un idhazh adhil malarndhaal malarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idhazh adhil malarndhaal malarven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini neeyindri naan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neeyindri naan illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO ini naanindri neeyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO ini naanindri neeyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO hO hO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OhO hO thadaiyum indry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO thadaiyum indry"/>
</div>
<div class="lyrico-lyrics-wrapper">engey thirudichendraai chendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey thirudichendraai chendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam adhan ellai ellai ellaikkey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam adhan ellai ellai ellaikkey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravum indry indry urasi chendraai chendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravum indry indry urasi chendraai chendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal adhai nenjil nenjil valarthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal adhai nenjil nenjil valarthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO oru nodiyum oru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO oru nodiyum oru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">piriya manamillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriya manamillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OhO hO hO sila nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO hO sila nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhaal poadhum anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhaal poadhum anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai nerungi virumbi varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nerungi virumbi varuveney"/>
</div>
<div class="lyrico-lyrics-wrapper">un nizhalaai nizhalaai thodarveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nizhalaai nizhalaai thodarveney"/>
</div>
</pre>
