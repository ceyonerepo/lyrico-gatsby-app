---
title: 'vinnil vinmeen song lyrics'
album: 'Kaappaan'
artist: 'Harris Jayaraj'
lyricist: 'Vairamuthu'
director: 'K V Anand'
path: '/albums/kaappaan-song-lyrics'
song: 'Vinnil Vinmeen'
image: ../../images/albumart/kaappaan.jpg
date: 2019-09-20
lang: tamil
singers: 
- Nikitha Harris
youtubeLink: "https://www.youtube.com/embed/EB_CqM6ehI0"
type: 'philosophy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Vinnil vinmeen aaiyiraam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinnil vinmeen aaiyiraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil pookkal aaiyiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannil pookkal aaiyiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ondru (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi ondru"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Udal niram maaralaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Udal niram maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir niram ondru dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir niram ondru dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbaal ondraai serum
<input type="checkbox" class="lyrico-select-lyric-line" value="Peranbaal ondraai serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam nandru dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Desam nandru dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thozhum murai maaralaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thozhum murai maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irai endrum ondru dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Irai endrum ondru dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam eshwar allah devan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nam eshwar allah devan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ondru dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam ondru dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vinnil vinmeen aaiyiraam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinnil vinmeen aaiyiraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanam ondru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa…aaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil pookkal aaiyiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannil pookkal aaiyiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi ondru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa….aaaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa….aaaa…aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vinnil vinmeen aaiyiraam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinnil vinmeen aaiyiraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil pookkal aaiyiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannil pookkal aaiyiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ottrai desam endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottrai desam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai vaazhkaiyendrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottrai vaazhkaiyendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ninaithaalum thinithaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar ninaithaalum thinithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraiverumaa?….
<input type="checkbox" class="lyrico-select-lyric-line" value="Niraiverumaa?…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pala vannangalaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Pala vannangalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seidha oviyam pol
<input type="checkbox" class="lyrico-select-lyric-line" value="Seidha oviyam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru niram konda padam endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru niram konda padam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaagumaa?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaagumaa?…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetrumaiyil azhagiyal undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetrumaiyil azhagiyal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrumaiyil ottrumai nandru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetrumaiyil ottrumai nandru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagippennum panpaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sagippennum panpaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaitha nilam idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Padaitha nilam idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hmm hmmm hmmmhm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmmm hmmmhm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmhmmhmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmmmhmmhmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmmm hmmmhm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmmm hmmmhm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmhmmhmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmmmhmmhmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah vinnil vinmeen aaiyiraam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaah vinnil vinmeen aaiyiraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil pookkal aaiyiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannil pookkal aaiyiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hmm hmmm hmmmhm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmmm hmmmhm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmhmmhmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmmmhmmhmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmmm hmmmhm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm hmmm hmmmhm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmhmmhmm (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmmmhmmhmm"/></div>
</div>
</pre>