---
title: "kangalai suttrum kanavugalai song lyrics"
album: "Kattappava Kanom"
artist: "Santhosh Dhayanidhi"
lyricist: "Muthamil"
director: "Mani Seiyon"
path: "/albums/kattappava-kanom-lyrics"
song: "Kangalai Suttrum Kanavugalai"
image: ../../images/albumart/kattappava-kanom.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7vE4zItpTVE"
type: "happy"
singers:
  -	Sathya Prakash
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kangalai Sutrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Sutrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugalai Kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalai Kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidithida Vazhi Irukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidithida Vazhi Irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Madiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Madiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhu Vizhum Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhu Vizhum Aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhudhida Mozhi Irukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhudhida Mozhi Irukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirago Mulaikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirago Mulaikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinile Pudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinile Pudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Nirangalai Thelikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Nirangalai Thelikiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoo Rale Thoo Rudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoo Rale Thoo Rudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar Unnale Manasukul Vedithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar Unnale Manasukul Vedithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Yedhum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Yedhum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Thannale Ilagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Thannale Ilagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Ingu Yedhum Vesham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Ingu Yedhum Vesham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaiye Illadha Thelindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaiye Illadha Thelindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithidum Neram Thondriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithidum Neram Thondriyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave Unnodu Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Unnodu Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhiyai Pesi Pookiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhiyai Pesi Pookiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Ho Thaen Poove 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho Thaen Poove "/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Manasukul Vedithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Manasukul Vedithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Sogam Yedhum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Sogam Yedhum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Naaldhorum Indru Ilagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Naaldhorum Indru Ilagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Ingu Yedhum Vesham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Ingu Yedhum Vesham Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralodu Viralum Inaindhidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu Viralum Inaindhidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Ulavum Oru Sugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Ulavum Oru Sugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kadhalo Kuliro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kadhalo Kuliro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Meerudhe Manadho Manadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Meerudhe Manadho Manadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Parka Thedi Thudikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Parka Thedi Thudikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai Idhazho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Idhazho "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Theenda Yengudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Theenda Yengudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannale Mutham Kondene Nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannale Mutham Kondene Nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullasam Modhudhe Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullasam Modhudhe Ho Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbale Konjam Yepodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbale Konjam Yepodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minjum Unnodu Vaazhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjum Unnodu Vaazhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalai Sutrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Sutrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugalai Kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalai Kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidithida Vazhi Irukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidithida Vazhi Irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Madiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Madiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Pirandhu Vizhum Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirandhu Vizhum Aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhudhida Mozhi Irukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhudhida Mozhi Irukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Sirago Mulaikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Sirago Mulaikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinile Pudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinile Pudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Nirangalai Thelikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Nirangalai Thelikiradhe"/>
</div>
</pre>
