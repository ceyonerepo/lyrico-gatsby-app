---
title: "bahubalikku oru kattappa song lyrics"
album: "Sivakumarin Sabadham"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "Hiphop Tamizha"
path: "/albums/sivakumarin-sabadham-lyrics"
song: "Bahubalikku Oru Kattappa"
image: ../../images/albumart/sivakumarin-sabadham.jpg
date: 2021-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0WO-PH0U8kw"
type: "happy"
singers:
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhkaina Vetriyum Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaina Vetriyum Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviyum Irrukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviyum Irrukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Othukka Othukka Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othukka Othukka Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Tharum Kastathil Endha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Tharum Kastathil Endha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nastamum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nastamum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadatha Kathukka Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadatha Kathukka Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennam Pola Vaazhka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Pola Vaazhka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhe Nenaingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhe Nenaingada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraiyum Emathaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraiyum Emathaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhunga Ozhainga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhunga Ozhainga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaadhi Madham Inamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi Madham Inamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala Adaikkum Siraingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala Adaikkum Siraingada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Romba Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Romba Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka Virichi Paraingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Virichi Paraingada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahubalikku Oru Kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahubalikku Oru Kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Pola Thaan Enga Chithappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Pola Thaan Enga Chithappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potiye Pala Getup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potiye Pala Getup Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadasila Ellaame Setup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadasila Ellaame Setup Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahubalikku Oru Kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahubalikku Oru Kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Pola Thaan Enga Chithappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Pola Thaan Enga Chithappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potiye Pala Getup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potiye Pala Getup Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadasila Ellaame Setup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadasila Ellaame Setup Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Vula Vizhundhu Life Aiye Marakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Vula Vizhundhu Life Aiye Marakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodathuda Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodathuda Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathoda Pethavanga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathoda Pethavanga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Stove Vula Vizhuntha Kariya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stove Vula Vizhuntha Kariya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegatha Da Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegatha Da Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vendha Pinnaalum Nondha Pinnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vendha Pinnaalum Nondha Pinnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Friends Irukkan Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friends Irukkan Thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesham Pottu Vesham Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Pottu Vesham Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Pazhagitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Pazhagitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Kaathin Vaasam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaathin Vaasam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veshamaagidum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshamaagidum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Kaati Nesam Kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Kaati Nesam Kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Pazhagitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Pazhagitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosamana Vaazhkai Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosamana Vaazhkai Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasamaagidum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasamaagidum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Fu Adhu Once Su Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Fu Adhu Once Su Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkum Pala Bunsu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukkum Pala Bunsu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku Nalla Mansirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku Nalla Mansirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Friends Irundha Funsu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friends Irundha Funsu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Fu Adhu Once Su Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Fu Adhu Once Su Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkum Pala Bunsu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukkum Pala Bunsu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku Nalla Mansirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku Nalla Mansirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Friends Irundha Funsu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friends Irundha Funsu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahubalikku Oru Kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahubalikku Oru Kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Pola Thaan Enga Chithappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Pola Thaan Enga Chithappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potiye Pala Getup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potiye Pala Getup Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadasila Ellaame Setup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadasila Ellaame Setup Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ad by Valueimpression
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ad by Valueimpression"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahubalikku Oru Kattappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahubalikku Oru Kattappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Pola Thaan Enga Chithappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Pola Thaan Enga Chithappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potiye Pala Getup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potiye Pala Getup Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadasila Ellaame Setup Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadasila Ellaame Setup Ah"/>
</div>
</pre>
