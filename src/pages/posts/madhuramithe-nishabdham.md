---
title: "madhuramithe song lyrics"
album: "Nishabdham"
artist: "Gopi Sundar"
lyricist: "Sreejo"
director: "Hemant Madhukar"
path: "/albums/nishabdham-lyrics"
song: "Madhuramithe"
image: ../../images/albumart/nishabdham.jpg
date: 2020-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JAkr0avx4II"
type: "love"
singers:
  - Najim Arshad
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Madhuramithe madhuramithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramithe madhuramithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna ee paravasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna ee paravasame"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholisari yedhani thadamuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholisari yedhani thadamuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Melukolupe swarame vinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukolupe swarame vinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi nee yadha nundi payanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi nee yadha nundi payanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu cherey mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu cherey mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramithe madhuramithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramithe madhuramithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna ee paravasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna ee paravasame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ara chethilo gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara chethilo gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupindhi ee chelime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupindhi ee chelime"/>
</div>
<div class="lyrico-lyrics-wrapper">Varnala vinyasam thelipey kalaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varnala vinyasam thelipey kalaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti saavasam nee nadaga nee varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti saavasam nee nadaga nee varame"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthandhama sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthandhama sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho kottha lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho kottha lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uhinachanidhey maru janami yedhuraindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uhinachanidhey maru janami yedhuraindhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalo mounamantha kavyamai karigindhi vinamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalo mounamantha kavyamai karigindhi vinamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramithe madhuramithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramithe madhuramithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna ee paravasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna ee paravasame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalale nee hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalale nee hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghana na nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghana na nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravahinche nee vaipe chinuke pranayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravahinche nee vaipe chinuke pranayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kathalake gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kathalake gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oopirai gamagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oopirai gamagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Spandhinchu nee pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spandhinchu nee pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvanteney nenu ante nijamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvanteney nenu ante nijamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuketai athi manamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuketai athi manamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Premai kaalamantha undipo undanu jathapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premai kaalamantha undipo undanu jathapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramithe madhuramithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramithe madhuramithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna ee paravasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna ee paravasame"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholisari yedhani thadamuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholisari yedhani thadamuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Melukolupe swarame vinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukolupe swarame vinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi nee yadha nundi payanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi nee yadha nundi payanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu cherey mahima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu cherey mahima"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuramithe madhuramithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuramithe madhuramithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuna ee paravasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna ee paravasame"/>
</div>
</pre>
