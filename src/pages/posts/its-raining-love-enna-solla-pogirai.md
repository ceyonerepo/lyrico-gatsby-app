---
title: "its raining love song lyrics"
album: "Enna Solla Pogirai"
artist: "Vivek – Mervin"
lyricist: "Ku. Karthik"
director: "A. Hariharan"
path: "/albums/enna-solla-pogirai-lyrics"
song: "It's Raining Love"
image: ../../images/albumart/enna-solla-pogirai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o-6T9-RfS6k"
type: "love"
singers:
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vanthai orunodiyil vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthai orunodiyil vizhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai ooru puyale uyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai ooru puyale uyirile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum mazhai thuliyin naduvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum mazhai thuliyin naduvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum unadhazhagil uruginen aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum unadhazhagil uruginen aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaram thanthai penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaram thanthai penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirvarai thindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirvarai thindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ooru velayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ooru velayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharayinil thathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharayinil thathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavum oruvaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavum oruvaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagavale illai anaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagavale illai anaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge puyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge puyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollathe kollathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollathe kollathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn kanga kathi pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn kanga kathi pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullathey thullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullathey thullathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi paavam inge aangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paavam inge aangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaley thannaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaley thannaley"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil etho koochal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil etho koochal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaley unnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaley unnaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyamaley unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyamaley unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkirain nijamaaidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkirain nijamaaidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ketkirian
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ketkirian"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthai orunodiyil vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthai orunodiyil vizhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai ooru puyale uyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai ooru puyale uyirile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum mazhai thuliyin naduviley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum mazhai thuliyin naduviley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum unadhazhagil uruginen oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum unadhazhagil uruginen oh ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthai orunodiyil vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthai orunodiyil vizhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai ooru puyale uyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai ooru puyale uyirile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum mazhai thuliyin naduviley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum mazhai thuliyin naduviley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum unadhazhagil uruginen oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum unadhazhagil uruginen oh ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyamalae unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyamalae unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkirain nijamaaidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkirain nijamaaidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ketkirian
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ketkirian"/>
</div>
</pre>
