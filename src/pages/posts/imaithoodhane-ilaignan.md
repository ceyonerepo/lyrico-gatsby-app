---
title: "imaithoodhane song lyrics"
album: "Ilaignan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Suresh Krishna"
path: "/albums/ilaignan-lyrics"
song: "Imaithoodhane"
image: ../../images/albumart/ilaignan.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/d5TlCU1aOFQ"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Imai Thoothane Imai Thoothane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Thoothane Imai Thoothane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarthaal Nenjil Panikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthaal Nenjil Panikkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithazh Thoazhane Ithazh Thoazhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithazh Thoazhane Ithazh Thoazhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirppaesumbozhuthu Isaikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirppaesumbozhuthu Isaikkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enathu Nilaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enathu Nilaakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vizhiyil Kanaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vizhiyil Kanaakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enathu Nilaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enathu Nilaakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kalanthaal Vizhaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kalanthaal Vizhaakkaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Thoothane Imai Thoothane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Thoothane Imai Thoothane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarthaal Nenjil Panikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthaal Nenjil Panikkaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounakkudaiyudan Nadanthaen Nee Mazhakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounakkudaiyudan Nadanthaen Nee Mazhakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaervai Udaiyil Vizhunthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaervai Udaiyil Vizhunthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veyil Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veyil Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Theendal Nizhal Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Theendal Nizhal Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetpu Koonthal Ethirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetpu Koonthal Ethirkaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maelaa Inbam Ival Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelaa Inbam Ival Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai Thoothane Imai Thoothane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Thoothane Imai Thoothane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarthaal Nenjil Panikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthaal Nenjil Panikkaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Kambali Anindaen Nee Kulirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Kambali Anindaen Nee Kulirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Kadalil Kavizhndaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Kadalil Kavizhndaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Puyal Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Puyal Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee En Nenjil Kaarkkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Nenjil Kaarkkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenda Mutham Poarkkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Mutham Poarkkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Ennul Pookkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Ennul Pookkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Thoothane Imai Thoothane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Thoothane Imai Thoothane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarthaal Nenjil Panikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthaal Nenjil Panikkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithazh Thøazhane Ithazh Thøazhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithazh Thøazhane Ithazh Thøazhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirppaesumbøzhuthu Isaikkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirppaesumbøzhuthu Isaikkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ènathu Nilaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ènathu Nilaakkaalam"/>
</div>
È<div class="lyrico-lyrics-wrapper">n Vizhiyil Kanaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="n Vizhiyil Kanaakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ènathu Nilaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ènathu Nilaakkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kalanthaal Vizhaakkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kalanthaal Vizhaakkaalam"/>
</div>
</pre>
