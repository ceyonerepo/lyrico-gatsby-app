---
title: "lady kunaro song lyrics"
album: "Swecha"
artist: "Bhole Shawali"
lyricist: "Kasarla Shyam"
director: "KPN Chawhan"
path: "/albums/swecha-lyrics"
song: "Lady Kunaro"
image: ../../images/albumart/swecha.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Wu0OKGsqXEE"
type: "happy"
singers:
  - Bole Shavali
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ledikoonaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledikoonaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Audi caruroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Audi caruroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ollu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ollu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chnnai Shopping Malluroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chnnai Shopping Malluroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladi queenroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladi queenroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Railu Gaadhiroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Railu Gaadhiroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooragallu Queuelu Kadatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooragallu Queuelu Kadatharo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey.. Gun Lanti Ponnuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey.. Gun Lanti Ponnuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Iphone Tenvee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iphone Tenvee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ton Pulu thisukocchi Challukona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ton Pulu thisukocchi Challukona"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Thotakunna Gatuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Thotakunna Gatuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Paita Chatu Flightuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paita Chatu Flightuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Flatu Theesukuntanu Raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flatu Theesukuntanu Raaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingilaala Jingilaala Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Jingilaala Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingilaala Jingilaala Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Jingilaala Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ledikoonaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledikoonaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Audi caruroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Audi caruroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ollu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ollu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chnnai Shopping Malluroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chnnai Shopping Malluroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajo Aajo Aajo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajo Aajo Aajo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenta padi Leyzo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenta padi Leyzo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek dinka Raju nuvve choraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek dinka Raju nuvve choraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sukkea muttakunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sukkea muttakunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Kikkeyekkuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Kikkeyekkuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenetta Thattukone Poreee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenetta Thattukone Poreee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyalu Pettocchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyalu Pettocchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadabida Cheyocchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadabida Cheyocchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Churachura Navvuthunte Chori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Churachura Navvuthunte Chori"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongutunna Shampane Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongutunna Shampane Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Glasulona Ompeyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glasulona Ompeyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkumantu Dhimpuna Kori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkumantu Dhimpuna Kori"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Esko Esko Jaldhi Smalo largeoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esko Esko Jaldhi Smalo largeoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Najjuggaa Penchai Sizeoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Najjuggaa Penchai Sizeoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusko Chusko mundu kattai feesoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusko Chusko mundu kattai feesoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopigaa Ekkey burujoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopigaa Ekkey burujoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yehe Mojukunna Pageve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe Mojukunna Pageve"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasukunte Rojuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasukunte Rojuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajukunte Righte raaye Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajukunte Righte raaye Yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingilaala Jingilaala Jingilaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Jingilaala Jingilaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingilaala Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyalu Jarocchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyalu Jarocchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gantalu Kaavocchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gantalu Kaavocchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salasala Kagutunte Chori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salasala Kagutunte Chori"/>
</div>
<div class="lyrico-lyrics-wrapper">Goda cheayi Jaarinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goda cheayi Jaarinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Malu chusthe foreignaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malu chusthe foreignaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dholubaza Suru chesthaa Pyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dholubaza Suru chesthaa Pyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thesko Thesko Masthu Rammo Jinoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thesko Thesko Masthu Rammo Jinoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasthalo Turning Lenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasthalo Turning Lenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusko Chusko Gyemmu Lona runno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusko Chusko Gyemmu Lona runno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesthunte Angles Yenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesthunte Angles Yenno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Swich Vesthe Lightve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Swich Vesthe Lightve"/>
</div>
<div class="lyrico-lyrics-wrapper">Scotchlona Spriteve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scotchlona Spriteve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjukunta Ee Nightye Raaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjukunta Ee Nightye Raaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehe Yehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehe Yehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingilaala Jingilaala Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Jingilaala Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingilaala Jingilaala Ompu sompule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilaala Jingilaala Ompu sompule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha Medha Banghi Jumpule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha Medha Banghi Jumpule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ledikoonaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledikoonaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Audi caruroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Audi caruroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ollu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ollu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chnnai Shopping Malluroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chnnai Shopping Malluroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladi queenroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladi queenroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Railu Gaadhiroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Railu Gaadhiroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooragallu Queuelu Kadatharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooragallu Queuelu Kadatharo"/>
</div>
</pre>
