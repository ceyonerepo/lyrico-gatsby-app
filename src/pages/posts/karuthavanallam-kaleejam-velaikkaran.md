---
title: "karuthavanallam kaleejam song lyrics"
album: "Velaikkaran"
artist: "Anirudh Ravichander"
lyricist: "Viveka"
director: "Mohan Raja"
path: "/albums/velaikkaran-lyrics"
song: "Karuthavanallam Kaleejam"
image: ../../images/albumart/velaikkaran.jpg
date: 2017-12-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/es-5OLRrUf8"
type: "Mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappi Vuttaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi Vuttaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Karuththa Maaththu Goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Karuththa Maaththu Goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odhingi Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhingi Nikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Va Therikkavidu Goiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Va Therikkavidu Goiyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakkaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Indha Nagaram Ippothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Indha Nagaram Ippothaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanagaram Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanagaram Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Maara Muzhu Kaaranamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Maara Muzhu Kaaranamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Annachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Annachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thagara Kottaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thagara Kottaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangi Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi Irundhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennaiyoda Annai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaiyoda Annai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kuppam Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kuppam Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappi Vuttaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi Vuttaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Karuththa Maaththu Goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Karuththa Maaththu Goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odhingi Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhingi Nikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Va Therikkavidu Goiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Va Therikkavidu Goiyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakkaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Area Kaasi Kupppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area Kaasi Kupppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellorum Onna Nippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum Onna Nippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarunu Paakka Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunu Paakka Mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasatha Pangu Vaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasatha Pangu Vaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tajmahala Kattinadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahala Kattinadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shahjahaan Kitta Sonnaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shahjahaan Kitta Sonnaa Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththupaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tajmahala Kattinadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahala Kattinadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shahjahaan Kitta Sonnaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shahjahaan Kitta Sonnaa Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththupaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhavinnu Kettaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhavinnu Kettaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apartmentu Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apartmentu Aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nice Ah Appeat Aavaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nice Ah Appeat Aavaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phone Ah Switchoff Seivaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone Ah Switchoff Seivaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanummey Azhaikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanummey Azhaikkaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppam Gopaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppam Gopaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Kooda Nippaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Kooda Nippaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Vela Paappaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Vela Paappaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukku Sondhakkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku Sondhakkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukku Veliya Ninnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku Veliya Ninnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perukku Chennaikkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukku Chennaikkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Sattam Sonnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Sattam Sonnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Karuthavanlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Karuthavanlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ulaichathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ulaichathellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappi Vuttaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi Vuttaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakkaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Indha Nagaram Ippothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Indha Nagaram Ippothaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanagaram Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanagaram Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Maara Muzhu Kaaranamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Maara Muzhu Kaaranamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Annachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Annachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thagara Kottaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thagara Kottaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangi Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi Irundhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennaiyoda Annai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaiyoda Annai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Kuppam Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kuppam Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuthavanlaam Galeejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthavanlaam Galeejaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelappi Vuttaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi Vuttaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Karuththa Maaththu Goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Karuththa Maaththu Goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ulaichathellam Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ulaichathellam Nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odhingi Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhingi Nikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Va Therikkavidu Goiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Va Therikkavidu Goiyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakkaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettukuththa Nikkuthuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettukuththa Nikkuthuppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shopping Maalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shopping Maalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Nikka Vecha Komban Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Nikka Vecha Komban Enga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppam Aaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppam Aaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tajmahal Kattinadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal Kattinadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shahjahaan Kitta Sonnaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shahjahaan Kitta Sonnaa Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththupaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhukulla Bondhukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhukulla Bondhukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Salaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Salaiyellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmuzhuchi Pottadhu Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmuzhuchi Pottadhu Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tajmahal Kattinadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal Kattinadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shahjahaan Kitta Sonnaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shahjahaan Kitta Sonnaa Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththupaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththupaaru"/>
</div>
</pre>
