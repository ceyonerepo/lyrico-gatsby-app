---
title: "enn uyire song lyrics"
album: "Asuravadham"
artist: "Govind Menon"
lyricist: "Karthik Netha"
director: "Maruthupandian"
path: "/albums/asuravadham-lyrics"
song: "Enn Uyire"
image: ../../images/albumart/asuravadham.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YtxU5ebMOGo"
type: "sad"
singers:
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen sidhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen sidhaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae koo raamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae koo raamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae theerndhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae theerndhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thedal yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thedal yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar anbae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar anbae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho naan thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho naan thunai thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen sidhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen sidhaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae kooraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae kooraamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoora parakkum en vazhvin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoora parakkum en vazhvin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vergal anaithum nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vergal anaithum nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatil tholainthae naan poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatil tholainthae naan poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaanae koottin nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanae koottin nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam thorum kaaval kaapaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam thorum kaaval kaapaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kanneerai maraithiruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kanneerai maraithiruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorndhu pogum ennai thetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorndhu pogum ennai thetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram yaavum sumappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram yaavum sumappai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En uyire yen sidhaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyire yen sidhaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae koo raamalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae koo raamalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae theerndhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae theerndhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thedal yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thedal yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar anbaeaeae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar anbaeaeae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho naan thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho naan thunai thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettil nulaindhaal un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettil nulaindhaal un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam anaithum pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam anaithum pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyai valarkkum un paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyai valarkkum un paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaayai katti pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaayai katti pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram yaavum thaangum thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram yaavum thaangum thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaiyae thaanum marandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaiyae thaanum marandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral pola pesum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral pola pesum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai kooda marandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai kooda marandhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vidhiyae yen sidhaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vidhiyae yen sidhaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo en saamiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo en saamiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae theerndhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae theerndhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thedal yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thedal yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar anbae enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar anbae enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho naan thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho naan thunai thunai"/>
</div>
</pre>
