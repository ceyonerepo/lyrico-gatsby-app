---
title: "villathi villangal song lyrics"
album: "Rajapattai"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/rajapattai-lyrics"
song: "Villathi Villangal"
image: ../../images/albumart/rajapattai.jpg
date: 2011-12-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dIVY5ieGpZ8"
type: "happy"
singers:
  - Mano
  - Malathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Villaathi villangal ellorumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villaathi villangal ellorumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vilaipesa vanthaargalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vilaipesa vanthaargalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu villangam illaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu villangam illaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna vilai kooda thanthaargalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna vilai kooda thanthaargalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endraalum naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraalum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai tharavillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai tharavillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattodu masthaana udal illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattodu masthaana udal illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam enbathae perithillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam enbathae perithillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa altaapu raani nee vittu pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa altaapu raani nee vittu pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu fullstop-pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu fullstop-pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkaama kattippudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaama kattippudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa thotaalae thoolaagum othukadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa thotaalae thoolaagum othukadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meththa mele nee paadatha kathukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththa mele nee paadatha kathukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu silu silu seraathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu silu silu seraathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru kuru kuru ethum illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru kuru kuru ethum illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">Indruthaanae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indruthaanae inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saru saru saru kelaathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saru saru saru kelaathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettaal inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaal inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pari pari pari paaraathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pari pari pari paaraathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthaal inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaal inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane athai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikkum pothae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkum pothae inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli tharuven inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli tharuven inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotu thodangaamal irunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotu thodangaamal irunthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumaa inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumaa inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattukadangaamal alaipaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattukadangaamal alaipaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalae inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti koduthaalum kuraiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti koduthaalum kuraiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porulae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porulae inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili kili eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili kili eppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidu vidu vidu veeram illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu vidu vidu veeram illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thidu thidu thidu kaalam vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thidu thidu thidu kaalam vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam vilakivitta kaadhal summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam vilakivitta kaadhal summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sidu sidu sidu koavam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidu sidu sidu koavam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayil summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayil summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadu kadu kadu leelai illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadu kadu kadu leelai illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagam summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagam summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bothai thelintha pinbu raavae summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai thelintha pinbu raavae summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodavaa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodavaa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollai tharavaa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai tharavaa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti pidipenae unnai naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti pidipenae unnai naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudaa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudaa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti paripenae thayangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti paripenae thayangaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodudaa summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodudaa summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti murukera tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti murukera tharuvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Villaathi villangal ellorumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villaathi villangal ellorumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vilaipesa vanthaargalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vilaipesa vanthaargalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu vilangam illaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu vilangam illaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna vilai kooda thanthaargalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna vilai kooda thanthaargalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endraalum nee unnai tharavillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraalum nee unnai tharavillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enpola aal yaarum ithil illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enpola aal yaarum ithil illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam enbathae enakillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam enbathae enakillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Altaapu raani naa oththukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Altaapu raani naa oththukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu fullstop-pe vekkaama kattikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu fullstop-pe vekkaama kattikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">shaba raba reppo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shaba raba reppo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">shaba raba reppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shaba raba reppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">shaba raba reppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shaba raba reppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba raba pa reppapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba raba pa reppapa"/>
</div>
</pre>
