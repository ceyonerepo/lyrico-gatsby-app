---
title: "ranga rangaringa song lyrics"
album: "EPCO 302"
artist: "Alex Paul"
lyricist: "Muthu Vijayan"
director: "Salangai Durai"
path: "/albums/epco-302-lyrics"
song: "Ranga Rangaringa"
image: ../../images/albumart/epco-302.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JlHmqRmH-Mw"
type: "happy"
singers:
  - K.Velu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ooi ranga ooi ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooi ranga ooi ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ooi ranga rangu rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooi ranga rangu rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">rangu rangu rangu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangu rangu rangu "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangu "/>
</div>
<div class="lyrico-lyrics-wrapper">ooi ranga ooi ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooi ranga ooi ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ooi ranga ooi ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooi ranga ooi ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ooi ranga ranga ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooi ranga ranga ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangu rangu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangu rangu "/>
</div>
<div class="lyrico-lyrics-wrapper">rangu ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangu ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">rangu rangu rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangu rangu rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">rangu rangu rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangu rangu rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">ranga ranga ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga ranga ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ye ranga ranga ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye ranga ranga ranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kumuku kumukunu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuku kumukunu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">siluku ponnu yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluku ponnu yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">achuku achuku nu thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achuku achuku nu thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">inipu mutham nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inipu mutham nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada ada ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ada ada"/>
</div>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum kick ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum kick ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathava thati kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathava thati kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla varuvathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla varuvathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulaiyum kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulaiyum kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vachathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vachathu illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathava thati kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathava thati kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla varuvathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla varuvathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulaiyum kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulaiyum kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu vachathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu vachathu illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irava pagalaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irava pagalaki "/>
</div>
<div class="lyrico-lyrics-wrapper">irumba karumbaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumba karumbaki"/>
</div>
<div class="lyrico-lyrics-wrapper">ponava address theriuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponava address theriuma"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya suvar thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya suvar thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai kugai thondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai kugai thondi"/>
</div>
<div class="lyrico-lyrics-wrapper">nulanja ponna viduvoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nulanja ponna viduvoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathadicha vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathadicha vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">varum megamappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum megamappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalicha naadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalicha naadi "/>
</div>
<div class="lyrico-lyrics-wrapper">narambil vegamappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambil vegamappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeppa kathadicha vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeppa kathadicha vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">varum megamappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum megamappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalicha naadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalicha naadi "/>
</div>
<div class="lyrico-lyrics-wrapper">narambil vegamappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambil vegamappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vantha puyal aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha puyal aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">ulage maranthu pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulage maranthu pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kulfi avaloda selfi eduthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulfi avaloda selfi eduthale"/>
</div>
<div class="lyrico-lyrics-wrapper">like eh rendu kodi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like eh rendu kodi da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kumuku kumukunu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumuku kumukunu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">siluku ponnu yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluku ponnu yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">achuku achuku nu thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achuku achuku nu thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">inipu mutham nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inipu mutham nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada ada ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ada ada"/>
</div>
<div class="lyrico-lyrics-wrapper">suda suda than malai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda than malai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu nu veyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu nu veyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala nu love pathikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala nu love pathikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">ranga rangaringa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranga rangaringa "/>
</div>
<div class="lyrico-lyrics-wrapper">rangaringa ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangaringa ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka love kidacha life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka love kidacha life "/>
</div>
<div class="lyrico-lyrics-wrapper">irukum young ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukum young ah"/>
</div>
</pre>
