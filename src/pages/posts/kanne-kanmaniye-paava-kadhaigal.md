---
title: "kanne kanmaniye song lyrics"
album: "Paava Kadhaigal"
artist: "R. Sivatmikha"
lyricist: "Yugabharathi"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Kanne Kanmaniye"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AR334zO2tT0"
type: "sad"
singers:
  - Ananthu
  - R. Sivatmikha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanne kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum minnoliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum minnoliye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paada"/>
</div>
<div class="lyrico-lyrics-wrapper">sol tharumo nam uravugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol tharumo nam uravugale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponne ponmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponne ponmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en arunchunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en arunchunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">perinba vaalvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinba vaalvum"/>
</div>
<div class="lyrico-lyrics-wrapper">verathuvo ippiraviyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verathuvo ippiraviyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en chellam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en chellam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin olive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin olive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naane enai paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naane enai paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">thondrum oru puthaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondrum oru puthaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">manathinil unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathinil unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">padukida sugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padukida sugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanave sila poluthugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanave sila poluthugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaipathai yar unaruvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaipathai yar unaruvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam seitha theengai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam seitha theengai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar vanthu thiruthuvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar vanthu thiruthuvatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanne kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum minnoliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum minnoliye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponne ponmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponne ponmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee en arunchunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee en arunchunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">perinba vaalvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinba vaalvum"/>
</div>
<div class="lyrico-lyrics-wrapper">verathuvo ippiraviyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verathuvo ippiraviyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sollal sudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal sudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koyil irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyil irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">guru poojai nadanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guru poojai nadanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulam kaaka vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulam kaaka vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">servathilai theivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="servathilai theivam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanne kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evarum oru uyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarum oru uyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">unarnthal pagai valarnthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarnthal pagai valarnthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal edhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal edhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">madhi edhuvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhi edhuvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">ini thodarnthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini thodarnthiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanne kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum minnoliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum minnoliye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paada"/>
</div>
<div class="lyrico-lyrics-wrapper">sol tharumo nam uravugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol tharumo nam uravugale"/>
</div>
</pre>
