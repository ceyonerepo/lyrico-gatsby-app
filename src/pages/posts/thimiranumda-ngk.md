---
title: "thimiranumda song lyrics"
album: "NGK"
artist: "Yuvan Shankar Raja"
lyricist: "Vignesh Shivan"
director: "Selvaraghavan"
path: "/albums/ngk-lyrics"
song: "Thimiranumda"
image: ../../images/albumart/ngk.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SyZOAgXiPMw"
type: "mass"
singers:
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nadakkura Valiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura Valiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigala Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigala Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkura Pozhuthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkura Pozhuthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katharala Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katharala Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkara Edathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambunga Puguntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambunga Puguntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaa Thimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaa Thimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odukkara Yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odukkara Yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyavan Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyavan Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkara Mugathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkara Mugathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralai Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alugara Saththamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alugara Saththamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedhi Vellathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhi Vellathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mithakkira Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithakkira Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadi Veettil Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi Veettil Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaththa Paththaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththa Paththaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angirunthae Namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angirunthae Namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththuna Ketkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththuna Ketkaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eranganum Daa Uthavanum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eranganum Daa Uthavanum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattam Pottu Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam Pottu Inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki Vechaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Vechaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam Pottu Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Pottu Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudakki Vechaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudakki Vechaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiya Solla Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya Solla Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Madakki Vechaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madakki Vechaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkura Valiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura Valiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigala Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigala Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkura Pozhuthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkura Pozhuthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katharala Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katharala Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkara Edathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambunga Puguntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambunga Puguntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaa Thimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaa Thimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odukkara Yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odukkara Yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyavan Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyavan Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkara Mugathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkara Mugathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralai Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alugara Saththamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alugara Saththamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooraththa Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooraththa Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakkavae Alakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakkavae Alakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhura Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhura Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakkaththa Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakkaththa Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhakkavae Izhakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhakkavae Izhakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukku Poranthonnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Poranthonnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Puriyum Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Puriyum Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku Appuram Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku Appuram Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Paaru Themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Paaru Themba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorkkum Nerathil Odaiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorkkum Nerathil Odaiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeikkum Nerathil Olaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikkum Nerathil Olaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorkkum Nerathil Odaiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorkkum Nerathil Odaiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeikkum Nerathil Olaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikkum Nerathil Olaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naal Maarum Ellaam Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Maarum Ellaam Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nodi Varum Daa Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nodi Varum Daa Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkura Valiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura Valiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigala Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigala Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkura Pozhuthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkura Pozhuthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katharala Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katharala Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukkara Edathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambunga Puguntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambunga Puguntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaa Thimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaa Thimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odukkara Yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odukkara Yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyavan Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyavan Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkara Mugathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkara Mugathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralai Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alugara Saththamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alugara Saththamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiranumdaathimiranumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiranumdaathimiranumdaa"/>
</div>
</pre>
