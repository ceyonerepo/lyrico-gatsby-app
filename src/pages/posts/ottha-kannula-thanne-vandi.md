---
title: "ottha kannula song lyrics"
album: "Thanne Vandi"
artist: "Moses"
lyricist: "Kadhir Mozhi"
director: "Manika Vidya"
path: "/albums/thanne-vandi-lyrics"
song: "Ottha Kannula"
image: ../../images/albumart/thanne-vandi.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x4naNDPAGds"
type: "happy"
singers:
  - Rakshitha Suresh
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oththa kannula thoosiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa kannula thoosiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu kannalaiyum kuthuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu kannalaiyum kuthuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kallu yerinja kolathu thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu yerinja kolathu thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kalanga nee vaikuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kalanga nee vaikuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi munaiyile nathaiya nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi munaiyile nathaiya nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">nagarnthu nagarnthu poguthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagarnthu nagarnthu poguthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu kuliyila viluntha thannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu kuliyila viluntha thannee"/>
</div>
<div class="lyrico-lyrics-wrapper">samuthirama marutunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samuthirama marutunga"/>
</div>
<div class="lyrico-lyrics-wrapper">yuthama rathama nithama vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yuthama rathama nithama vena"/>
</div>
<div class="lyrico-lyrics-wrapper">mothama naan tharen kan muzhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothama naan tharen kan muzhida"/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi thotigul meenaga vaazhntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi thotigul meenaga vaazhntha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadala kaaturen enthiruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala kaaturen enthiruda"/>
</div>
<div class="lyrico-lyrics-wrapper">vedha manala iruntha manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedha manala iruntha manam"/>
</div>
<div class="lyrico-lyrics-wrapper">putha manala elukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putha manala elukuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">vedha manala iruntha manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedha manala iruntha manam"/>
</div>
<div class="lyrico-lyrics-wrapper">putha manala elukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putha manala elukuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanatha urasum patathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanatha urasum patathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thraiyila oru pudi unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thraiyila oru pudi unda"/>
</div>
<div class="lyrico-lyrics-wrapper">kalla thanam seiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalla thanam seiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">unnil naanum kallapoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil naanum kallapoma"/>
</div>
<div class="lyrico-lyrics-wrapper">nodi kandukitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodi kandukitten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthaigalellam thandi ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthaigalellam thandi ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mounathil oru bashai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounathil oru bashai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallipoo kalladi thangikitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallipoo kalladi thangikitu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayathil sirikum katru kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayathil sirikum katru kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">silaneram yenakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaneram yenakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">palaneram unakaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaneram unakaga"/>
</div>
<div class="lyrico-lyrics-wrapper">enimelum namakaga vazhthidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enimelum namakaga vazhthidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilodum malaiyodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilodum malaiyodum"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyodum kulirodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyodum kulirodum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalaga ungooda vanthidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalaga ungooda vanthidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">valipariyaa valipariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valipariyaa valipariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyala onnum puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyala onnum puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavula than kanduthumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavula than kanduthumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kandukonden naan kathalukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandukonden naan kathalukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalukkule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalukkule "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaayatha kothum kaakaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayatha kothum kaakaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalangal yennai thorathuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalangal yennai thorathuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">dheevaga yennai nee seelntha pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheevaga yennai nee seelntha pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">aaratha kaayangal aariduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaratha kaayangal aariduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yethana dhumpangal vanthathundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethana dhumpangal vanthathundu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaiymadi dheda dhonalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaiymadi dheda dhonalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">entha nodi neeyum thayumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha nodi neeyum thayumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu viral yenthan thopul kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu viral yenthan thopul kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangatha sumanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangatha sumanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ooli veesa marukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooli veesa marukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaaga en vaalvil vanthayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaaga en vaalvil vanthayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeravaana yelipola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeravaana yelipola"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulla neyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulla neyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en thookam ipothu kalaithayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thookam ipothu kalaithayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni vandiya irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni vandiya irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vida nee nenaikalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vida nee nenaikalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tharikettu than thirinjavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharikettu than thirinjavana"/>
</div>
<div class="lyrico-lyrics-wrapper">unakulla than karaikiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakulla than karaikiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oththa kannula thoosiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa kannula thoosiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu kannalaiyum kuthuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu kannalaiyum kuthuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kallu yerinja kolathu thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu yerinja kolathu thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kalanga nee vaikuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kalanga nee vaikuraye"/>
</div>
</pre>
