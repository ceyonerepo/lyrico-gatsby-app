---
title: "otha sollala song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Ekadesi"
director: "Vetrimaran"
path: "/albums/aadukalam-lyrics"
song: "Otha Sollala"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vXLAIaSkFIQ"
type: "love"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Otha Sollaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Otha Sollaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usureduthu Vachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usureduthu Vachikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Kannaala Enna Thinnaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Kannaala Enna Thinnaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thannipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thannipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sombukulla Oothivachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sombukulla Oothivachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niththam Kudichu Enna Konnaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Kudichu Enna Konnaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Potta Kaattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Potta Kaattula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalangatti Mazhapenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalangatti Mazhapenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaronnu Oduradhaa Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaronnu Oduradhaa Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Pattaampoochi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Pattaampoochi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Sattaiyila Ottikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattaiyila Ottikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattasu Pola Naan Vedichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu Pola Naan Vedichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutta Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta Kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mucha Eduthu Ponavadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mucha Eduthu Ponavadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Pinnaala Yedho Aanendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Pinnaala Yedho Aanendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Powder Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Powder Dappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerndhu Ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerndhu Ponadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kannadiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kannadiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaduppu Aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduppu Aanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kuppurathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kuppurathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paduththu Kedandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduththu Kedandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kudhura Mela Yethi Vittaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kudhura Mela Yethi Vittaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnum Sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Sollaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Thottaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Thottaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Inikkavecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Inikkavecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheeni Mittaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeni Mittaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Otha Sollaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Otha Sollaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usureduthu Vachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usureduthu Vachikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Kannaala Enna Thinnaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Kannaala Enna Thinnaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thannipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thannipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sombukulla Oothivachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sombukulla Oothivachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niththam Kudichu Enna Konnaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Kudichu Enna Konnaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kattavandi Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kattavandi Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kanazhaga Paathu Pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanazhaga Paathu Pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kattu Choru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kattu Choru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattivandhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattivandhudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kazhuthalaga Paathupongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kazhuthalaga Paathupongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththaazha Pazha Sevappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththaazha Pazha Sevappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththaatha Elam Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththaatha Elam Sirippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaththaatha Ava Iduppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaththaatha Ava Iduppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kirukkaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kirukkaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Otha Sollaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Otha Sollaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usureduthu Vachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usureduthu Vachikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Kannaala Enna Thinnaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Kannaala Enna Thinnaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thannipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thannipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sombukulla Oothivachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sombukulla Oothivachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niththam Kudichu Enna Konnaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Kudichu Enna Konnaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ration Cardil Pera Yethuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ration Cardil Pera Yethuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Kuruchu Thattu Maathuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Kuruchu Thattu Maathuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oorukellaam Sethi Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oorukellaam Sethi Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kaadhil Mattum Meedhi Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kaadhil Mattum Meedhi Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu Karupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Karupatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Theepetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Theepetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mennu Thinnaley Enna Oruvaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mennu Thinnaley Enna Oruvaati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oththa Sollaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oththa Sollaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usureduthu Vachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usureduthu Vachikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Kannaala Enna Thinnaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Kannaala Enna Thinnaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thannipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thannipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sombukulla Oothivachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sombukulla Oothivachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niththam Kudichu Enna Konnaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Kudichu Enna Konnaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Potta Kaattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Potta Kaattula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalangatti Mazhapenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalangatti Mazhapenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaronnu Oduradhaa Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaronnu Oduradhaa Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Pattaampoochi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Pattaampoochi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Sattaiyila Ottikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sattaiyila Ottikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattasu Pola Naan Vedichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu Pola Naan Vedichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutta Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta Kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mucha Eduthu Ponavadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mucha Eduthu Ponavadhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Pinnaala Yedho Aanendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Pinnaala Yedho Aanendaa"/>
</div>
</pre>
