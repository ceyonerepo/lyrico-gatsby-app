---
title: "kaappaathu karuppa song lyrics"
album: "Madura Veeran"
artist: "Santhosh Dhayanidhi"
lyricist: "Yugabharathi"
director: "P.G. Muthiah"
path: "/albums/madura-veeran-lyrics"
song: "Kaappaathu Karuppa"
image: ../../images/albumart/madura-veeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6yzZPzwLZwc"
type: "devotional"
singers:
  - Mathichiyam Bala
  - Kottai Samy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadhi Garuda Vania Neela Meghavanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Garuda Vania Neela Meghavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Meghavanna Neela Meghavanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Meghavanna Neela Meghavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Andharathil Dhaan Varavazhaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Andharathil Dhaan Varavazhaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavazhaithu Varavazhaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavazhaithu Varavazhaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavazhaithu Varavazhaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavazhaithu Varavazhaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Enga Ooru Karavamaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Enga Ooru Karavamaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kota Karuppu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kota Karuppu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kota Karuppu Vaada Kota Karuppu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kota Karuppu Vaada Kota Karuppu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Makkal Ellam Kaathuruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Makkal Ellam Kaathuruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Maarinaatu Karupu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Maarinaatu Karupu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinaatu Karupu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinaatu Karupu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaateri Nerungaama Kaapaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaateri Nerungaama Kaapaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootaaga Kuriketom Kai Thooki Vidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaaga Kuriketom Kai Thooki Vidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettukathi Veecharuvaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettukathi Veecharuvaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathuku Kaathidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathuku Kaathidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu Engala Pallakil Yethida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Engala Pallakil Yethida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaateri Nerungaama Kaapaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaateri Nerungaama Kaapaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootaaga Kuriketom Kai Thooki Vidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaaga Kuriketom Kai Thooki Vidappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallarukum Kettarukum Inge Ethana Poti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallarukum Kettarukum Inge Ethana Poti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi Irukum Engaluku Nanma Senjidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi Irukum Engaluku Nanma Senjidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillaraikum Kallaraikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillaraikum Kallaraikum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Sindhura Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Sindhura Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikum Munne Sella Thembu Thandhidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikum Munne Sella Thembu Thandhidappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattala Itidappa Gaandhampola Ottidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattala Itidappa Gaandhampola Ottidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Yaarum Senjaalume Kattam Kattidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Yaarum Senjaalume Kattam Kattidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Iyyane Konjam Kann Paarappa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Iyyane Konjam Kann Paarappa "/>
</div>
<div class="lyrico-lyrics-wrapper">Allala Kollu Munnerappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allala Kollu Munnerappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam Thittam Illaiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Thittam Illaiyappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaateri Nerungaama Kaapaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaateri Nerungaama Kaapaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootaaga Kuriketom Kai Thooki Vidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaaga Kuriketom Kai Thooki Vidappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pithalainga Vengalatha Nakkal Senjidalaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithalainga Vengalatha Nakkal Senjidalaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthamulla Nenja Vandhu Sutham Pannidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamulla Nenja Vandhu Sutham Pannidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattil Mallukatta Munne Nikira Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattil Mallukatta Munne Nikira Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann Eduthu Potuvechi Pattam Kattanumppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Eduthu Potuvechi Pattam Kattanumppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallaiyum Thattanumppa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaiyum Thattanumppa "/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Kaala Vekanumppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Kaala Vekanumppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathame Sotta Sotta Yudham Seiyanumppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathame Sotta Sotta Yudham Seiyanumppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettupattiyum Solla Panpaadappa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettupattiyum Solla Panpaadappa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrivel Endru Thundaadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Endru Thundaadappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai Illa Kondaadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Illa Kondaadappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaateri Nerungaama Kaapaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaateri Nerungaama Kaapaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootaaga Kuriketom Kai Thooki Vidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaaga Kuriketom Kai Thooki Vidappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettukathi Veecharuvaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettukathi Veecharuvaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennathuku Kaathidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathuku Kaathidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu Engala Pallakil Yethida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Engala Pallakil Yethida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaateri Nerungaama Kaapaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaateri Nerungaama Kaapaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootaaga Kuriketom Kai Thooki Vidappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaaga Kuriketom Kai Thooki Vidappa"/>
</div>
</pre>
