---
title: "kodakaa koteswar rao song lyrics"
album: "Agnyaathavaasi"
artist: "Anirudh Ravichander"
lyricist: "Bhaskarabhatla"
director: "Trivikram Srinivas"
path: "/albums/agnyaathavaasi-lyrics"
song: "Kodakaa Koteswar Rao"
image: ../../images/albumart/agnyaathavaasi.jpg
date: 2018-01-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0vV36TV0oms"
type: "mass"
singers:
  - Pawan Kalyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Sharma Hu Hu Hu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sharma Hu Hu Hu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Hu Ivvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Hu Ivvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Va Va Va Sharma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Va Va Sharma "/>
</div>
<div class="lyrico-lyrics-wrapper">Inkaa Neeku Touch Poledoyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkaa Neeku Touch Poledoyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Continue Continue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Continue Continue"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah Ah Ah Huhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Ah Ah Huhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baabu Thukaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baabu Thukaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Glaasu Miriyaala Paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Glaasu Miriyaala Paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Krachanukokapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krachanukokapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Yaallukaayalu Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Yaallukaayalu Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Tagilinchavoyy Kummeddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tagilinchavoyy Kummeddham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar Rao Vaa Hm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar Rao Vaa Hm"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharma Gaaru Nenem Paaduthunnanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharma Gaaru Nenem Paaduthunnanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Meerem Vinipisthunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meerem Vinipisthunnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippude Baagundannav Baabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippude Baagundannav Baabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appudu Baagundannanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appudu Baagundannanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudu Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudu Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry Kanth Nen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry Kanth Nen "/>
</div>
<div class="lyrico-lyrics-wrapper">Quality Vishayamlo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quality Vishayamlo "/>
</div>
<div class="lyrico-lyrics-wrapper">Compramise Avalenu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Compramise Avalenu "/>
</div>
<div class="lyrico-lyrics-wrapper">Avalenante Avalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalenante Avalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Naa Oppandukune 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Naa Oppandukune "/>
</div>
<div class="lyrico-lyrics-wrapper">Young And Talented 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Young And Talented "/>
</div>
<div class="lyrico-lyrics-wrapper">Musicians Ye Leraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musicians Ye Leraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wow Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodakaa I Will Give 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa I Will Give "/>
</div>
<div class="lyrico-lyrics-wrapper">A Signal 1 2 3
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Signal 1 2 3"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodakaa Kodakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Kodakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulasalaaga Egiri Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulasalaaga Egiri Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Pulasalaaga Egiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pulasalaaga Egiri "/>
</div>
<div class="lyrico-lyrics-wrapper">Padithe Pulusaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padithe Pulusaipothavvro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi Padithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Padithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Maadipoyina Arisaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadipoyina Arisaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirigina Chirigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirigina Chirigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Chirigina Pursaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Chirigina Pursaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Irigina Irrusaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irigina Irrusaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagaani Thikkalesthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagaani Thikkalesthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Bommaleni Borusaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommaleni Borusaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegina Golusaipothavvro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegina Golusaipothavvro "/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Manishepothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Manishepothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda Kodakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda Kodakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orey Neechanikrusta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Neechanikrusta "/>
</div>
<div class="lyrico-lyrics-wrapper">Koteswara Rao Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koteswara Rao Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dharidram Cheppadaaniki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dharidram Cheppadaaniki "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Daggarunna Dialoguelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Daggarunna Dialoguelu "/>
</div>
<div class="lyrico-lyrics-wrapper">Saripovadam Ledhu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripovadam Ledhu Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattaipothavvro Cheepuru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattaipothavvro Cheepuru "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Kindapadina Itthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Kindapadina Itthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bindeki Sottaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bindeki Sottaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Rotaipothavvro Chettha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rotaipothavvro Chettha "/>
</div>
<div class="lyrico-lyrics-wrapper">Buttaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buttaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Sagam Kaali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sagam Kaali "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaripoyina Chuttaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaripoyina Chuttaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Ori Nee Chetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Ori Nee Chetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Midatha Laaga Midisi Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midatha Laaga Midisi Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Midata Laaga Midisi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midata Laaga Midisi "/>
</div>
<div class="lyrico-lyrics-wrapper">Padithe Massai Pothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padithe Massai Pothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhe Kada Kaavalsindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe Kada Kaavalsindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ko Ko Ko Kodakaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko Ko Ko Kodakaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Koteswar Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koteswar Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaachi Kodithe Penam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaachi Kodithe Penam "/>
</div>
<div class="lyrico-lyrics-wrapper">Meeda Dosai Pothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeda Dosai Pothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagilina Glassaipothavvro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagilina Glassaipothavvro "/>
</div>
<div class="lyrico-lyrics-wrapper">Karigina Icaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigina Icaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolli Chesthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolli Chesthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikemo Alusaipothavro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikemo Alusaipothavro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampara Pulasaipothavvro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampara Pulasaipothavvro "/>
</div>
<div class="lyrico-lyrics-wrapper">Popula Dinusaipothavro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Popula Dinusaipothavro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ko Ko Ko Kodakaa Koteswar Rao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko Ko Ko Kodakaa Koteswar Rao"/>
</div>
<div class="lyrico-lyrics-wrapper">Padmaja Gaaru Meeru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padmaja Gaaru Meeru "/>
</div>
<div class="lyrico-lyrics-wrapper">Madyaanam Bonchesinattu Leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madyaanam Bonchesinattu Leru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anduke Gattigaa Paadatledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anduke Gattigaa Paadatledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodarimanulantha Marokkasaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodarimanulantha Marokkasaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Muktha Kantamtho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muktha Kantamtho "/>
</div>
<div class="lyrico-lyrics-wrapper">Kodakaa Koteswar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodakaa Koteswar "/>
</div>
<div class="lyrico-lyrics-wrapper">Rao Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rao Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaga Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaga Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkaa Krachaipothavvro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkaa Krachaipothavvro"/>
</div>
<div class="lyrico-lyrics-wrapper">Krachaipoyinattunnadugaa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krachaipoyinattunnadugaa Haa"/>
</div>
</pre>
