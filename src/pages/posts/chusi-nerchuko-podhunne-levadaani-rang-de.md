---
title: "chusi nerchuko song lyrics"
album: "Rang De"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/rang-de-lyrics"
song: "Chusi Nerchuko - Podhunne Levadaani"
image: ../../images/albumart/rang-de.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/aVzIlleIDQw"
type: "happy"
singers:
  - David Simon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poddhunne levadanni konni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhunne levadanni konni "/>
</div>
<div class="lyrico-lyrics-wrapper">chusi nerchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi nerchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Antaru konni kose peddhalendhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antaru konni kose peddhalendhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Shubranga undadanni aanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shubranga undadanni aanni "/>
</div>
<div class="lyrico-lyrics-wrapper">chusi nerchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi nerchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Comapre cheyadanni aaparendhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comapre cheyadanni aaparendhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">First rank kottadanni eenni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First rank kottadanni eenni"/>
</div>
 <div class="lyrico-lyrics-wrapper">chusi nerchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi nerchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">First class nunchi ee torture endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First class nunchi ee torture endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Time kochhi poye pillaganni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time kochhi poye pillaganni "/>
</div>
<div class="lyrico-lyrics-wrapper">chusi nerchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi nerchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Punctuality leni eepunchulendhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punctuality leni eepunchulendhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanni chusi eenni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanni chusi eenni "/>
</div>
<div class="lyrico-lyrics-wrapper">chusi Nerchukuntu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi Nerchukuntu "/>
</div>
<div class="lyrico-lyrics-wrapper">pothe unte Xerox laaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothe unte Xerox laaga "/>
</div>
<div class="lyrico-lyrics-wrapper">jeevithalu maarava so
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevithalu maarava so"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi nerchukoku evanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi nerchukoku evanni "/>
</div>
<div class="lyrico-lyrics-wrapper">chusi nerchukoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusi nerchukoku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka cheyamaku ivanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka cheyamaku ivanni "/>
</div>
<div class="lyrico-lyrics-wrapper">lekka cheyamaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lekka cheyamaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sun light ni chusi nerchukoni unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun light ni chusi nerchukoni unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Full moon coolga undevaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full moon coolga undevaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Class mate nu chusi nerchukoni unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class mate nu chusi nerchukoni unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Einstein scientist ayyevaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Einstein scientist ayyevaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Monkey la nunche manishi puttukantaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkey la nunche manishi puttukantaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari monkey la unte thattukoru endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari monkey la unte thattukoru endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Day and night hard work chesthundhi mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Day and night hard work chesthundhi mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Donkey ni chusi nerchukora endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donkey ni chusi nerchukora endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi laaga eedi laaga undadanni copy chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi laaga eedi laaga undadanni copy chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Life waste avvadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life waste avvadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusinerchukoku… koku Chusi nerchukoku… koku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusinerchukoku… koku Chusi nerchukoku… koku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka cheyamaku… maku ivanni lekka cheyamaku… maku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka cheyamaku… maku ivanni lekka cheyamaku… maku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinka laaga memu cute gaa undali antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinka laaga memu cute gaa undali antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Simham joole trim chesthundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simham joole trim chesthundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Fish lage nenu eetha kottali antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fish lage nenu eetha kottali antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Elephant swim suit vesesthundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elephant swim suit vesesthundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Own style marchukovu animals eppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Own style marchukovu animals eppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaati clarity manaku ledu endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaati clarity manaku ledu endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Conclusion enti ante confusion vaddhuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conclusion enti ante confusion vaddhuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku laaga nuvvu unte digulu deniko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku laaga nuvvu unte digulu deniko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadilaaga eedi laaga signature nerchukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadilaaga eedi laaga signature nerchukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Future forgery avvadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Future forgery avvadha"/>
</div>
<div class="lyrico-lyrics-wrapper">So chusi nerchukoku evanni chusi nerchukoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So chusi nerchukoku evanni chusi nerchukoku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka cheyamaku uvanni lekka cheyamaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka cheyamaku uvanni lekka cheyamaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi nerchukoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi nerchukoku"/>
</div>
</pre>
