---
title: "thailaanguyil song lyrics"
album: "Yutha Satham"
artist: "	D. Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/yutha-satham-lyrics"
song: "Thailaanguyil"
image: ../../images/albumart/yutha-satham.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5QU_ubjZvUs"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thailaanguyil thailaanguyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thailaanguyil thailaanguyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyattudhea paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyattudhea paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkakavum unakkakavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkakavum unakkakavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaimeetuthea kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaimeetuthea kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil paarthidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil paarthidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil malarkkattai neetudhea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil malarkkattai neetudhea"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu pagal neeyum nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu pagal neeyum nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesida kadal osai neekkuthea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesida kadal osai neekkuthea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavathu kooda vaaimoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavathu kooda vaaimoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namadhu kathai kettea thugaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namadhu kathai kettea thugaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthanai sugam vazhvil endrethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthanai sugam vazhvil endrethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam namai parthea yeangadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam namai parthea yeangadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thailaanguyil thailaanguyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thailaanguyil thailaanguyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyattudhea paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyattudhea paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkakavum unakkakavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkakavum unakkakavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaimeetuthea kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaimeetuthea kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye ellamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye ellamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal kolam theetadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal kolam theetadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam ayyo endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam ayyo endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paasam jaadai kaatadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paasam jaadai kaatadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manalveligal un ninaivugalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalveligal un ninaivugalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon thugalgal enragum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon thugalgal enragum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavukalo en kavalaikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukalo en kavalaikalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam uravai thundatitadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam uravai thundatitadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oli veesum suriyan neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli veesum suriyan neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathai pesum kathalin thayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathai pesum kathalin thayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathai mazhaiyaai pozhivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathai mazhaiyaai pozhivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thailaanguyil thailaanguyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thailaanguyil thailaanguyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyattudhea paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyattudhea paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkakavum unakkakavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkakavum unakkakavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaimeetuthea kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaimeetuthea kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil paarthidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil paarthidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil malarkkattai neetudhea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil malarkkattai neetudhea"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu pagal neeyum nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu pagal neeyum nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesida kadal osai neekkuthea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesida kadal osai neekkuthea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavathu kooda vaaimoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavathu kooda vaaimoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namadhu kathai kettea thugaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namadhu kathai kettea thugaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthanai sugam vazhvil endrethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthanai sugam vazhvil endrethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam namai parthea yeangadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam namai parthea yeangadho"/>
</div>
</pre>
