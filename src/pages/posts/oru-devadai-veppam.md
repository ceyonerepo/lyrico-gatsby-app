---
title: "oru devadai song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Oru Devadai"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4XqCswapZyw"
type: "love"
singers:
  - Clinton Cerejo
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Veesidum Paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Veesidum Paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvathu Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathu Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Dhoorathil Varuvathai Parkkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Dhoorathil Varuvathai Parkkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivathum Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivathum Oru Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Pudhu Maattram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Pudhu Maattram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum Uru Maattram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum Uru Maattram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Oru Yettram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Oru Yettram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Enakku Endru Intha Mannil Vanthu Piranthavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Enakku Endru Intha Mannil Vanthu Piranthavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Thoongum Pothum Kaadhal Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thoongum Pothum Kaadhal Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kadavul Thantha Parisaaga Kaiyil Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kadavul Thantha Parisaaga Kaiyil Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Oh Hoho Ho Ooo Ho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Oh Hoho Ho Ooo Ho Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Veesidum Paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Veesidum Paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvathu Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathu Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Dhoorathil Varuvathai Parkkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Dhoorathil Varuvathai Parkkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivathum Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivathum Oru Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaanil Megangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanil Megangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal Thooruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal Thooruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham Thoovudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosham Thoovudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thantha Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thantha Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanainthaalae Paavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanainthaalae Paavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Endhan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Endhan Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Veesum Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Veesum Kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul Poosum Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Poosum Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Mugam Endhan Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Mugam Endhan Kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minsaaram Illaa Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaaram Illaa Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalaai Vandhu Oli Tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalaai Vandhu Oli Tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Velicha Mazhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Velicha Mazhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nanainthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nanainthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral Thottu Vidum Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Thottu Vidum Thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Sutterikkum Bhaarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Sutterikkum Bhaarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadha Bodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaadha Bodhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Purindha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Purindha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Pakkam Vara Pakkam Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Pakkam Vara Pakkam Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Padapadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wow Ho Wowho Ooooh Ho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Ho Wowho Ooooh Ho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Ho Wowho Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Ho Wowho Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Ho Wowho Ooooh Ho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Ho Wowho Ooooh Ho Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Ho Wowho Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Ho Wowho Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Maalaiyil Malarndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Maalaiyil Malarndhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanai En Sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanai En Sondham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Anaivarum Rasiththidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Anaivarum Rasiththidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Mattum En Sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Mattum En Sondham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Oh Hooh Ho Oh Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Oh Hooh Ho Oh Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Oh Ho Oh Ho Oh Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Oh Ho Oh Ho Oh Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Aval Paarthathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Aval Paarthathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Endru Naan Kettathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Endru Naan Kettathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Azhagai Azhakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Azhagai Azhakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Karuvi Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Karuvi Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kattalaiyai Kettu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kattalaiyai Kettu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kattupattu Vaazhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kattupattu Vaazhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaadha Paadhai Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaadha Paadhai Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arindha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arindha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Pakkam Vara Pakkam Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Pakkam Vara Pakkam Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Padapadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Ho Oo Hoo Oo Hoo Oo Hoo Ooo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho Oo Hoo Oo Hoo Oo Hoo Ooo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Veesidum Paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Veesidum Paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvathu Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathu Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Dhoorathil Varuvathai Parkkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Dhoorathil Varuvathai Parkkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivathum Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivathum Oru Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Pudhu Maattram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Pudhu Maattram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum Uru Maattram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum Uru Maattram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Oru Yettram Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Oru Yettram Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Enakku Endru Intha Mannil Vanthu Piranthavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Enakku Endru Intha Mannil Vanthu Piranthavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Thoongum Pothum Kaadhal Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thoongum Pothum Kaadhal Thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Kadavul Thantha Parisaaga Kaiyil Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kadavul Thantha Parisaaga Kaiyil Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Oh Hoho Ho Ooo Ho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Oh Hoho Ho Ooo Ho Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Oh Hoho Ho Ooo Ho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Oh Hoho Ho Ooo Ho Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhai Veesidum Paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhai Veesidum Paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvathu Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathu Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Dhoorathil Varuvathai Parkkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Dhoorathil Varuvathai Parkkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivathum Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivathum Oru Sugam"/>
</div>
</pre>
