---
title: "vennilavin perai matrava song lyrics"
album: "Ramanaa"
artist: "Ilaiyaraja"
lyricist: "Pazhani Barathi - M Metha"
director: "A.R. Murugadoss"
path: "/albums/ramanaa-lyrics"
song: "Vennilavin Perai Matrava"
image: ../../images/albumart/ramanaa.jpg
date: 2002-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fPydKTsQie0"
type: "happy"
singers:
  - Hariharan
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thodum Thendral Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thodum Thendral Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru Kobam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru Kobam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu Valiyil Udalai Thaluvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Valiyil Udalai Thaluvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthiduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jennal Nilavu Ennai Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jennal Nilavu Ennai Paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Endru Unnai Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Endru Unnai Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendidum Iravile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendidum Iravile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolusu Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanngal Unnai Thedi Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanngal Unnai Thedi Paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendidum Kanavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendidum Kanavile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolusugal Sollum Isaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Sollum Isaiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagiya Kaadhal Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagiya Kaadhal Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgalai Vaazha Vaippathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgalai Vaazha Vaippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai Mazhai Chaaral Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Mazhai Chaaral Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thuli Manasukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thuli Manasukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalena Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalena Aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullamennum Kadalukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullamennum Kadalukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manam Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Guthikkum Kulanthaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guthikkum Kulanthaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Nenjum Nindru Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Nenjum Nindru Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Kaiyil Bommai Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kaiyil Bommai Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisayam Alagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisayam Alagile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Paarvai Thendral Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Paarvai Thendral Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennmai Indru Pookkal Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennmai Indru Pookkal Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Uravile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Uravile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyarkkaiyin Alagu Vendrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkkaiyin Alagu Vendrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Alagu Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Alagu Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagukku Alagu Serppathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagukku Alagu Serppathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Kaadhal Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Kaadhal Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandiranum Suriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandiranum Suriyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalukku Rendu Viizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalukku Rendu Viizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu Vennilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Vennilavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttrudhu Sutrudhu Unnaye Ulagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttrudhu Sutrudhu Unnaye Ulagile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thodum Thendral Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thodum Thendral Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru Kobam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru Kobam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu Valiyil Udalai Thaluvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Valiyil Udalai Thaluvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthiduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavin Perai Maattravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavin Perai Maattravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Perai Soottaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Perai Soottaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utharavu Podu En Mainaa Mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utharavu Podu En Mainaa Mainaa"/>
</div>
</pre>
