---
title: 'thani vazhi song lyrics'
album: 'Darbar'
artist: 'Anirudh Ravichander'
lyricist: 'Yogi B'
director: 'A R Murugadoss'
path: '/albums/darbar-song-lyrics'
song: 'Thani Vazhi'
image: ../../images/albumart/darbar.jpg
date: 2020-01-09
lang: tamil
singers:
- Yogi B
- Anirudh Ravichander Ravichander
- Shakthisree Gopalan
youtubeLink: "https://www.youtube.com/embed/oimut0lUVag"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Oh we taking over this show
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh we taking over this show"/>
</div>
<div class="lyrico-lyrics-wrapper">Step it up back it up lock and load
<input type="checkbox" class="lyrico-select-lyric-line" value="Step it up back it up lock and load"/>
</div>
<div class="lyrico-lyrics-wrapper">Damn you never seen this before
<input type="checkbox" class="lyrico-select-lyric-line" value="Damn you never seen this before"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow the leader now
<input type="checkbox" class="lyrico-select-lyric-line" value="Follow the leader now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tholpattai natchathiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholpattai natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki sattai neethi kavasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaki sattai neethi kavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Per sonnaal kottam adangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Per sonnaal kottam adangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhitya arunaachalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadhitya arunaachalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Rap  Commissioner
<input type="checkbox" class="lyrico-select-lyric-line" value="Rap  Commissioner"/>
</div>
<div class="lyrico-lyrics-wrapper">Come stand up salute
<input type="checkbox" class="lyrico-select-lyric-line" value="Come stand up salute"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivar in charge
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivar in charge"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum thani route
<input type="checkbox" class="lyrico-select-lyric-line" value="Endrum thani route"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi olindhaal vyugam vaguppaar
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi olindhaal vyugam vaguppaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar venaa vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar venaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Get ready for war…war
<input type="checkbox" class="lyrico-select-lyric-line" value="Get ready for war…war"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thani vazhi endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani vazhi endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani vazhi.. hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani vazhi.. hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada evan inga ethurula
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada evan inga ethurula"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiradi rombha adhiradi.. hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhiradi rombha adhiradi.. hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee seendatha da dey (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee seendatha da dey"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="Round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
<div class="lyrico-lyrics-wrapper">We gonna round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="We gonna round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Annan da ellorukkum avar
<input type="checkbox" class="lyrico-select-lyric-line" value="Annan da ellorukkum avar"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan da…annan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Annan da…annan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan da aanavam vettaiyaadum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannan da aanavam vettaiyaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Start praying now
<input type="checkbox" class="lyrico-select-lyric-line" value="Start praying now"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi vizhum veri
<input type="checkbox" class="lyrico-select-lyric-line" value="Idi vizhum veri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi saththam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar kanna paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Paar kanna paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae ellarukkum orae sattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae ellarukkum orae sattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Por kadandhaar uyarndhar
<input type="checkbox" class="lyrico-select-lyric-line" value="Por kadandhaar uyarndhar"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal avar pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Makkal avar pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar edhirppaar
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar edhirppaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi nenjangal sollidum per
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi nenjangal sollidum per"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thotta morattu kaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Thotta morattu kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttum edhirkum aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttum edhirkum aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa seiyaadha vela
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappa seiyaadha vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm unakku maala
<input type="checkbox" class="lyrico-select-lyric-line" value="Confirm unakku maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta padai irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetta padai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil thunivirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil thunivirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu muzhudhum vetri thodarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naadu muzhudhum vetri thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku jae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Annanukku jae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Arunaachalam…
<input type="checkbox" class="lyrico-select-lyric-line" value="Arunaachalam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhithya arunaachalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhithya arunaachalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vari pesunaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vari pesunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">News thaan purinjuko
<input type="checkbox" class="lyrico-select-lyric-line" value="News thaan purinjuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaipula uyarnthathaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Uzhaipula uyarnthathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu thaan therinjuko
<input type="checkbox" class="lyrico-select-lyric-line" value="Maasu thaan therinjuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisayam arpudham
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhisayam arpudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iranguna kalathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Iranguna kalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakena irukiraar
<input type="checkbox" class="lyrico-select-lyric-line" value="Namakena irukiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam enna nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Bayam enna nenjukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="Round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
<div class="lyrico-lyrics-wrapper">Andrum endrum orae superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Andrum endrum orae superstar"/>
</div>
<div class="lyrico-lyrics-wrapper">We gonna round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="We gonna round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
<div class="lyrico-lyrics-wrapper">Andrum endrum orae superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Andrum endrum orae superstar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Catch dem liars
<input type="checkbox" class="lyrico-select-lyric-line" value="Catch dem liars"/>
</div>
<div class="lyrico-lyrics-wrapper">Set dem on fire
<input type="checkbox" class="lyrico-select-lyric-line" value="Set dem on fire"/>
</div>
<div class="lyrico-lyrics-wrapper">Movin up higer
<input type="checkbox" class="lyrico-select-lyric-line" value="Movin up higer"/>
</div>
<div class="lyrico-lyrics-wrapper">Never retire (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Never retire"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thani vazhi endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani vazhi endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani vazhi.. hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani vazhi.. hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada evan inga ethurula
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada evan inga ethurula"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiradi rombha adhiradi.. hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhiradi rombha adhiradi.. hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee seendatha da dey (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee seendatha da dey"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keep movin..keep movin
<input type="checkbox" class="lyrico-select-lyric-line" value="Keep movin..keep movin"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t stop
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t stop"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep movin..keep movin
<input type="checkbox" class="lyrico-select-lyric-line" value="Keep movin..keep movin"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t stop
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t stop"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">We gonna round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="We gonna round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
<div class="lyrico-lyrics-wrapper">We gonna round-em up
<input type="checkbox" class="lyrico-select-lyric-line" value="We gonna round-em up"/>
</div>
<div class="lyrico-lyrics-wrapper">And take-em down
<input type="checkbox" class="lyrico-select-lyric-line" value="And take-em down"/>
</div>
</pre>