---
title: "tharaga thanga thai song lyrics"
album: "Virudhu"
artist: "Dhina "
lyricist: "Athavan"
director: "Athavan"
path: "/albums/virudhu-lyrics"
song: "Tharaga Thanga Thai"
image: ../../images/albumart/virudhu.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JarG7xuNMv8"
type: "love"
singers:
  - Antonydass
  - Shiranya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tharagu thanga thai un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu thanga thai un"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu vila enanu sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu vila enanu sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sollu sollu thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu sollu thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">vayakatu veera samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayakatu veera samy"/>
</div>
<div class="lyrico-lyrics-wrapper">en aadu vila unaku ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aadu vila unaku ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thallu thallu thallu samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thallu thallu thallu samy"/>
</div>
<div class="lyrico-lyrics-wrapper">coimbatore pullu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coimbatore pullu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee iruka un othala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee iruka un othala"/>
</div>
<div class="lyrico-lyrics-wrapper">ennanu sollu thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennanu sollu thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu theriyatha aal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu theriyatha aal "/>
</div>
<div class="lyrico-lyrics-wrapper">mari nee vaala en kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari nee vaala en kita"/>
</div>
<div class="lyrico-lyrics-wrapper">aatathu veera samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatathu veera samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharagu thanga thai un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu thanga thai un"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu vila enanu sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu vila enanu sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sollu sollu thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu sollu thayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koginur vairam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koginur vairam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">un meni yen nee kasata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meni yen nee kasata "/>
</div>
<div class="lyrico-lyrics-wrapper">padaum sollu thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaum sollu thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">meni pathu vanthudatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meni pathu vanthudatha"/>
</div>
<div class="lyrico-lyrics-wrapper">aemali intha idatha vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aemali intha idatha vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaali panna pothum samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaali panna pothum samy"/>
</div>
<div class="lyrico-lyrics-wrapper">rasiyavilum kanadavilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasiyavilum kanadavilum"/>
</div>
<div class="lyrico-lyrics-wrapper">kidaikum platinam pol un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidaikum platinam pol un"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu irupathala enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu irupathala enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">isatam thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isatam thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">edaku madaka pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edaku madaka pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">mokka peesum aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mokka peesum aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda poorveegam enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda poorveegam enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyum veera samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyum veera samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharagu thanga thai un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu thanga thai un"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu vila enanu sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu vila enanu sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sollu sollu thayee thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollu sollu thayee thayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">santhaiyila nee nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhaiyila nee nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha summa thaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha summa thaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">maaru kalakalakum thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaru kalakalakum thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosama thaali katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosama thaali katta"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna unna nooru varusam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna unna nooru varusam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala vaipen thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala vaipen thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">ellathaiyum vevarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellathaiyum vevarama"/>
</div>
<div class="lyrico-lyrics-wrapper">therunju iruku samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therunju iruku samy"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaiyum neey vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiyum neey vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha ethukuven samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha ethukuven samy"/>
</div>
<div class="lyrico-lyrics-wrapper">mayandi veeran nu petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayandi veeran nu petha"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla un manasukula pooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla un manasukula pooti"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vaiye pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vaiye pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pattikatu ponnusamy petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattikatu ponnusamy petha"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla unna pathirama pooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla unna pathirama pooti"/>
</div>
<div class="lyrico-lyrics-wrapper">vaipen manasu kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipen manasu kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharagu yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu yei"/>
</div>
<div class="lyrico-lyrics-wrapper">tharagu thanga thai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu thanga thai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai illa namma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai illa namma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valuvome thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valuvome thayee"/>
</div>
<div class="lyrico-lyrics-wrapper">vayakatu veera samy en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayakatu veera samy en"/>
</div>
<div class="lyrico-lyrics-wrapper">aaisukum unnoda nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaisukum unnoda nalla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala venum samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala venum samy"/>
</div>
<div class="lyrico-lyrics-wrapper">tharagu ei pulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu ei pulla "/>
</div>
<div class="lyrico-lyrics-wrapper">tharagu thanga thai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharagu thanga thai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai illa namma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai illa namma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valuvome thayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valuvome thayee"/>
</div>
</pre>
