---
title: "therikkudha song lyrics"
album: "Jackpot"
artist: "Vishal Chandrasekhar"
lyricist: "Arunraja Kamaraj - Lady Kash"
director: "Kalyaan"
path: "/albums/jackpot-lyrics"
song: "Therikkudha"
image: ../../images/albumart/jackpot.jpg
date: 2019-08-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jOnll1cVGS8"
type: "mass"
singers:
  - Arunraja Kamaraj
  - Lady Kash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Therikkudha Vedikkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkudha Vedikkudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaiyil Namma Kodi Parakudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaiyil Namma Kodi Parakudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyudhaa Puriyudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyudhaa Puriyudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhudhu Ninja Thara Polakkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhudhu Ninja Thara Polakkudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkum Massu Pozhuthi Face-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkum Massu Pozhuthi Face-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Polakkum Class-u Nadandhu Varadhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polakkum Class-u Nadandhu Varadhula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimundhu Pesu Manusha Race-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimundhu Pesu Manusha Race-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinju Pogum Dash-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinju Pogum Dash-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theekkanalai Thorkadikkum Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theekkanalai Thorkadikkum Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Kanavai Theerthuvaikkum Vervai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Kanavai Theerthuvaikkum Vervai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Paarkkadalil Kandedutha Theervai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Paarkkadalil Kandedutha Theervai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Karangal Yendhuvadhu Jackpot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karangal Yendhuvadhu Jackpot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theervai Pola Namma Sutriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervai Pola Namma Sutriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhmai Soozha Vaazhum Koottamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhmai Soozha Vaazhum Koottamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai Ulaavum Maarum Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai Ulaavum Maarum Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Vetrumai Ozhikkura Kootathin Jackpot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Vetrumai Ozhikkura Kootathin Jackpot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkum Massu Pozhuthi Face-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkum Massu Pozhuthi Face-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Polakkum Class-u Nadandhu Varadhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polakkum Class-u Nadandhu Varadhula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimundhu Pesu Manusha Race-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimundhu Pesu Manusha Race-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinju Pogum Dash-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinju Pogum Dash-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkudha Vedikkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkudha Vedikkudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaiyil Namma Kodi Parakudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaiyil Namma Kodi Parakudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyudhaa Puriyudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyudhaa Puriyudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhudhu Ninja Thara Polakkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhudhu Ninja Thara Polakkudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Listen To Me Sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Listen To Me Sir"/>
</div>
<div class="lyrico-lyrics-wrapper">she Got It Going So Good
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="she Got It Going So Good"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Ivalathu Era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Ivalathu Era"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Ival Oru Terror
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Ival Oru Terror"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All Round Pen Thanthavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Round Pen Thanthavai"/>
</div>
<div class="lyrico-lyrics-wrapper">So Be Making No Error
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Be Making No Error"/>
</div>
<div class="lyrico-lyrics-wrapper">She A Real Like Wire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She A Real Like Wire"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetta Oru Kathaiyum Kooda Nijamalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetta Oru Kathaiyum Kooda Nijamalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei Yenum Vizhiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Yenum Vizhiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin Kannil Vanthu Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin Kannil Vanthu Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naraga Theeyil Thayam Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraga Theeyil Thayam Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Jackpot Aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Jackpot Aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Y Think You Know The Game Huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Y Think You Know The Game Huh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Pogum Vegathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pogum Vegathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathathu Onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathathu Onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Close Ya Eyes And Roll The Dice
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Close Ya Eyes And Roll The Dice"/>
</div>
<div class="lyrico-lyrics-wrapper">We Taking The Jackpot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Taking The Jackpot"/>
</div>
<div class="lyrico-lyrics-wrapper">So Name Ya Price
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Name Ya Price"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkum Massu Pozhuthi Face-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkum Massu Pozhuthi Face-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Polakkum Class-u Nadandhu Varadhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polakkum Class-u Nadandhu Varadhula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimundhu Pesu Manusha Race-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimundhu Pesu Manusha Race-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinju Pogum Dash-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinju Pogum Dash-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkkunangal Ootivitta Thaaimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkkunangal Ootivitta Thaaimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathirangal Madtrividum Nermai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathirangal Madtrividum Nermai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovulagai Thaangugira Thooimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovulagai Thaangugira Thooimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manadhai Thoondividum Jackpot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manadhai Thoondividum Jackpot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbam Endra Ondru Engumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Endra Ondru Engumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Sutri Vanthu Thangumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Sutri Vanthu Thangumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Endra Ondraith Thedi Adhai Vendrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Endra Ondraith Thedi Adhai Vendrida"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippadhae Endrumae Jackpot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippadhae Endrumae Jackpot"/>
</div>
</pre>
