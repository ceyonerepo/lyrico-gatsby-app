---
title: "sandiyare song lyrics"
album: "Thavam"
artist: "Srikanth Deva"
lyricist: "Mayil"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Sandiyare"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IOzTmBUnXrk"
type: "love"
singers:
  - Chitti
  - Bhagyashree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aaah aah aah aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaah aah aah aah"/>
</div>
<div class="lyrico-lyrics-wrapper">aaah aah aah aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaah aah aah aah"/>
</div>
<div class="lyrico-lyrics-wrapper">sandiyare sandiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandiyare sandiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasil vanthavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasil vanthavare"/>
</div>
<div class="lyrico-lyrics-wrapper">sandiyare sandiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandiyare sandiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasil vanthavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasil vanthavare"/>
</div>
<div class="lyrico-lyrics-wrapper">antha ramarum tharmarum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha ramarum tharmarum "/>
</div>
<div class="lyrico-lyrics-wrapper">serthathu pola oru uthamare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthathu pola oru uthamare"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla veeramum theeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla veeramum theeramum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula thangura nallavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula thangura nallavare"/>
</div>
<div class="lyrico-lyrics-wrapper">vellatu kutti pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellatu kutti pola"/>
</div>
<div class="lyrico-lyrics-wrapper">vellanthi pen maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi pen maane"/>
</div>
<div class="lyrico-lyrics-wrapper">vellanga katukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanga katukula"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilaga nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilaga nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">koora pattu aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koora pattu aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">aalagi poothu iruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalagi poothu iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">maasamottu nesa patu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasamottu nesa patu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga kathu irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga kathu irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">nan sumapen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan sumapen "/>
</div>
<div class="lyrico-lyrics-wrapper">un usura en usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un usura en usura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">velakethu sayangalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velakethu sayangalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vili oram unna pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vili oram unna pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">seema thora samiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seema thora samiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyura theriyura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyura theriyura "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukula neraiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukula neraiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyatha kala neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyatha kala neram"/>
</div>
<div class="lyrico-lyrics-wrapper">vanatha nanum pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanatha nanum pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thane vennilavaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thane vennilavaga "/>
</div>
<div class="lyrico-lyrics-wrapper">sirikira sirikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikira sirikira"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukula parakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukula parakura"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru pulla pethukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru pulla pethukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru vaya katiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru vaya katiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">ambalaya ne nadathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambalaya ne nadathu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa patu othukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa patu othukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">aala maram ne rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala maram ne rasa"/>
</div>
<div class="lyrico-lyrics-wrapper">viluthaga nan thaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viluthaga nan thaya"/>
</div>
<div class="lyrico-lyrics-wrapper">aathu thani ne thaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu thani ne thaya"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyaga ne vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyaga ne vaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna senjen nan theepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna senjen nan theepen"/>
</div>
<div class="lyrico-lyrics-wrapper">un kadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kadana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maruthani vachu viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthani vachu viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">thalagani thachu tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalagani thachu tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">maga rani thoongum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maga rani thoongum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">madiyila madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madiyila madiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">masapatu thanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masapatu thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">tharisaga iruntha mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharisaga iruntha mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavanikul intha ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanikul intha ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">vagai nathi vasalil vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagai nathi vasalil vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nindrathu nindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nindrathu nindrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">valkaiya thanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valkaiya thanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">aaraga nan iruntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaraga nan iruntha "/>
</div>
<div class="lyrico-lyrics-wrapper">aathu vellam ne thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu vellam ne thane"/>
</div>
<div class="lyrico-lyrics-wrapper">theraga na nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theraga na nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">sakarame ne thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakarame ne thane"/>
</div>
<div class="lyrico-lyrics-wrapper">rasathi unakaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasathi unakaga "/>
</div>
<div class="lyrico-lyrics-wrapper">maru jenmam pirapen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru jenmam pirapen di"/>
</div>
<div class="lyrico-lyrics-wrapper">un kooda vala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kooda vala than"/>
</div>
<div class="lyrico-lyrics-wrapper">varam vangi varuven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam vangi varuven di"/>
</div>
<div class="lyrico-lyrics-wrapper">en rasa unakaga kathirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en rasa unakaga kathirupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sandiyare sandiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandiyare sandiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasil vanthavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasil vanthavare"/>
</div>
<div class="lyrico-lyrics-wrapper">antha ramarum tharmarum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha ramarum tharmarum "/>
</div>
<div class="lyrico-lyrics-wrapper">serthathu pola oru uthamare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthathu pola oru uthamare"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla veeramum theeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla veeramum theeramum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula thangura nallavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula thangura nallavare"/>
</div>
<div class="lyrico-lyrics-wrapper">vellatu kutti pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellatu kutti pola"/>
</div>
<div class="lyrico-lyrics-wrapper">vellanthi pen maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi pen maane"/>
</div>
<div class="lyrico-lyrics-wrapper">vellanga katukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanga katukula"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilaga nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilaga nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">koora pattu aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koora pattu aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">aalagi poothu iruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalagi poothu iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">maasamottu nesa patu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasamottu nesa patu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga kathu irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga kathu irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">nan sumapen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan sumapen "/>
</div>
<div class="lyrico-lyrics-wrapper">un usura en usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un usura en usura"/>
</div>
</pre>
