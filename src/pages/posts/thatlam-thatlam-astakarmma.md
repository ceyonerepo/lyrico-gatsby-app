---
title: "thatlam thatlam song lyrics"
album: "Astakarmma"
artist: "LV Muthu Ganesh"
lyricist: "T Rajendar"
director: "Vijay Tamilselvan"
path: "/albums/astakarmma-lyrics"
song: "Thatlam Thatlam"
image: ../../images/albumart/astakarmma.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h2fC2vSYMKw"
type: "happy"
singers:
  - T Rajendar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machaan thattu paattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaan thattu paattu "/>
</div>
<div class="lyrico-lyrics-wrapper">aavattum hit-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavattum hit-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thabela thatlaam thalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thabela thatlaam thalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">poda thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Melam kooda thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melam kooda thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">miruthangam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miruthangam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamma kooda thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamma kooda thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">dammu oda thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammu oda thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam machaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam machaan "/>
</div>
<div class="lyrico-lyrics-wrapper">machaan thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machaan thatlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laddu pola chittu kidacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu pola chittu kidacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Corect-ah thatlam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corect-ah thatlam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch-u pottu oththu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch-u pottu oththu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu katlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu katlaam machaan vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattuketha beat irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattuketha beat irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalam thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalam thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle oda party vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle oda party vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakka thatlaam machan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakka thatlaam machan vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Side dish irundha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side dish irundha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakka ethlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakka ethlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil dhillu irundha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil dhillu irundha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Problem face panlaam nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problem face panlaam nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugatha maraikanumnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugatha maraikanumnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Face-il mask podanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face-il mask podanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnera venumnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnera venumnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-il task venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-il task venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam Thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam Thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laddu pola chittu kidacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laddu pola chittu kidacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Corect-ah thatlam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corect-ah thatlam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch-u pottu oththu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch-u pottu oththu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu katlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu katlaam machaan vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattuketha beat irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattuketha beat irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalam thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalam thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle oda party vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle oda party vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakka thatlaam machan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakka thatlaam machan vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Side dish irundha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side dish irundha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakka ethlaam machaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakka ethlaam machaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil dhillu irundha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil dhillu irundha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Problem face panlaam nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problem face panlaam nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugatha maraikanumnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugatha maraikanumnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Face-il mask podanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face-il mask podanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnera venumnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnera venumnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-il task venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-il task venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machaan vaa thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaan vaa thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatlaam thatlaam thatlaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam thatlaam thatlaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thatlaam thatlaam thatlaam thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatlaam thatlaam thatlaam thatlaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatlaam"/>
</div>
</pre>
