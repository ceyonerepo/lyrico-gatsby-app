---
title: "vaalai suruttu song lyrics"
album: "Rudra Thandavam"
artist: "Jubin"
lyricist: "Jubin - Mohan G"
director: "Mohan G Kshatriyan"
path: "/albums/rudra-thandavam-lyrics"
song: "Vaalai Suruttu"
image: ../../images/albumart/rudra-thandavam.jpg
date: 2021-10-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hag-hJHen2M"
type: "mass"
singers:
  - Ranjith Govind
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mudinthidum ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinthidum ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">oru parvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru parvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">adangidum elaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangidum elaam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">viraindhidum ivan kaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraindhidum ivan kaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">maanai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">verithidum ivan ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verithidum ivan ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">vengai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vengai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee un vaalai suruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee un vaalai suruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee un vaalai suruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee un vaalai suruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">padhungidum nariyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhungidum nariyin"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir olamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir olamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanadhu pidiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanadhu pidiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu paavamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu paavamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thavithudum adhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavithudum adhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">eri naragamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri naragamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">azhinthidum ivan kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhinthidum ivan kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">koramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaamal ponnalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaamal ponnalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thee yaai naaney varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee yaai naaney varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaamal ponnalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaamal ponnalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thee yaai naaney varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee yaai naaney varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dum dum dumda dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dumda dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dumda dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dumda dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dumda dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dumda dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dumda dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dumda dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dumda dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dumda dumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu viyugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu viyugam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai arinthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai arinthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumarum nilaimarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumarum nilaimarum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai virati virati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai virati virati"/>
</div>
<div class="lyrico-lyrics-wrapper">thuvachi eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuvachi eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">yerum veri yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerum veri yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan kovam ayyo paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan kovam ayyo paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai theliya theliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai theliya theliya"/>
</div>
<div class="lyrico-lyrics-wrapper">adichi norukki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichi norukki "/>
</div>
<div class="lyrico-lyrics-wrapper">theruvil eluthu poduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvil eluthu poduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudinthidum ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinthidum ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">oru parvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru parvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">adangidum elaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangidum elaam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">viraindhidum ivan kaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraindhidum ivan kaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">maanai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">verithidum ivan ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verithidum ivan ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">vengai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vengai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee un vaalai suruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee un vaalai suruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">yaara irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaara irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee un vaalai suruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee un vaalai suruttu"/>
</div>
</pre>
