---
title: "acha telugandhame song lyrics"
album: "Hero"
artist: "Ghibran"
lyricist: "Ramajogayya Sastry"
director: "Sriram Aditya"
path: "/albums/hero-lyrics"
song: "Acha Telugandhame"
image: ../../images/albumart/hero-telugu.jpg
date: 2022-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tOq0zjTcswg"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ningilo taaraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningilo taaraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelapai vaalene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelapai vaalene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula pandagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula pandagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame aagene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame aagene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Premane baaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premane baaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaa thaakene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaa thaakene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenane praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenane praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvuga maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvuga maarene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bujji gunde vendi thera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji gunde vendi thera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choosi mecchukundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choosi mecchukundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukunna heroinnu nuvvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukunna heroinnu nuvvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dream landu theateru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dream landu theateru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu bomma geesukundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu bomma geesukundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppa mooyakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppa mooyakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju choosukovalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju choosukovalani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Accha telugandhamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Accha telugandhamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela kalisey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela kalisey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaraala nandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaraala nandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilo pathangamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo pathangamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa repu maapu payaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa repu maapu payaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho jathakalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho jathakalise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippativaraku ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippativaraku ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu thana chappudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu thana chappudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu vinaledhu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu vinaledhu kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati varaku kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati varaku kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu rangulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu rangulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana kanaledhu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana kanaledhu kadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gurthuku raadhasale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthuku raadhasale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye roju ye vaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye roju ye vaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruguta maaninadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruguta maaninadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gadhi gadiyaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gadhi gadiyaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inninaalla okka nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inninaalla okka nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddaralle maarinaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddaralle maarinaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattilepinaave naalo premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattilepinaave naalo premani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka pakka nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka pakka nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandugalle undhi seenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandugalle undhi seenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adbuthanga maarchinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adbuthanga maarchinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi okka frameu ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi okka frameu ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Accha telugandhamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Accha telugandhamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela kalisey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela kalisey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaraala nandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaraala nandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo manase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaalilo pathangamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo pathangamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa repu maapu payaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa repu maapu payaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho jathakalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho jathakalise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nemmadhi nemmadhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nemmadhi nemmadhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhariki nanu pilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhariki nanu pilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanuvuku padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanuvuku padipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggara daggaragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggara daggaragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarigi nee kaugili lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigi nee kaugili lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatha padipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatha padipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppudu cheriginadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu cheriginadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggula sarihaddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggula sarihaddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chappuna dorikinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chappuna dorikinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkera tholimuddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkera tholimuddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi unna gundeloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi unna gundeloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu nenu pampinaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu nenu pampinaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumbanaala sambarala daariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumbanaala sambarala daariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaku nuvvu neeku nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku nuvvu neeku nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhakaalu chesinaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhakaalu chesinaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi nela neeru nippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi nela neeru nippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali vaana sakshigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali vaana sakshigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Accha telugandhamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Accha telugandhamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela kalisey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela kalisey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaraala nandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaraala nandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilo pathangamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo pathangamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa repu maapu payaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa repu maapu payaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho jathakalise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho jathakalise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my baby"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my love"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
</pre>
