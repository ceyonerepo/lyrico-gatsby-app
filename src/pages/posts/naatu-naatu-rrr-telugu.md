---
title: "naatu naatu song lyrics"
album: "RRR Telugu"
artist: "Maragathamani"
lyricist: "Chandrabose"
director: "S.S. Rajamouli "
path: "/albums/rrr-telugu-lyrics"
song: "Naatu Naatu"
image: ../../images/albumart/rrr-telugu.jpg
date: 2022-03-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Gt9WzC4WDEA"
type: "happy"
singers:
  - Rahul Sipligunj
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Polamgattu Dhummulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamgattu Dhummulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Potlagitta Dhookinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potlagitta Dhookinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poleramma Jataraalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poleramma Jataraalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Potharaju Ooginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potharaju Ooginattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirruseppulu Esukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirruseppulu Esukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Karrasamu Sesianattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karrasamu Sesianattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marrisettu Needalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marrisettu Needalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurragumpu Koodinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurragumpu Koodinattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerrajonna Rottelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerrajonna Rottelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirapathokku Kalipinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirapathokku Kalipinattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Veera Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Veera Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Oora Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Oora Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachi Mirapalaga Picha Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachi Mirapalaga Picha Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vichhu Katthi Laaga Verri Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vichhu Katthi Laaga Verri Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundeladhiri Poyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeladhiri Poyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakara Moginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakara Moginattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu Sillu Padelaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu Sillu Padelaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Keesu Pitta Koosinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keesu Pitta Koosinattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelu Sitikalesela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelu Sitikalesela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavvaaram Saaginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavvaaram Saaginattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Sindhu Thokkela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Sindhu Thokkela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhummaram Reginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhummaram Reginattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vollu Sematapattela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollu Sematapattela"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerangam Sesinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerangam Sesinattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Paata Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paata Soodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Veera Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Veera Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Oora Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Oora Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadda Paralaga Chedda Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadda Paralaga Chedda Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkapotha Laaga Thikha Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkapotha Laaga Thikha Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dugu Dugu Dugu Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dugu Dugu Dugu Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dugu Dugu Dugu Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dugu Dugu Dugu Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dugu Dugu Dugu Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dugu Dugu Dugu Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomi Dhaddharillela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Dhaddharillela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vonttiloni Ragathamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vonttiloni Ragathamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rankelesi Yegirela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rankelesi Yegirela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeseyro Yakayeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeseyro Yakayeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yes’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes’aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Dhummu Dhummu Dhulipe Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Dhummu Dhummu Dhulipe Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lopalunna Paanamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lopalunna Paanamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumuku Dumuk Laadela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumuku Dumuk Laadela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhookeyro Sarasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhookeyro Sarasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatu Dhinka Chika Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Dhinka Chika Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu Naatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Naatu Naatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Naatu Naatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Adhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Adhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakara Kanakara Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakara Kanakara Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakara Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakara Nakara Nakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakara Nakara Nakara Nakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakara Nakara Nakara Nakara"/>
</div>
</pre>
