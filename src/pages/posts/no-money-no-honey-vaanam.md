---
title: "no money no honey song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "No Money No Honey"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v0KPb7sBYOw"
type: "happy"
singers:
  - Silambarasan
  - Andrea Jeremiah
  - Srikanth Deva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkum Venum Enakkum Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Venum Enakkum Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Katta Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Katta Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dappu Maalu Thuttukku Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappu Maalu Thuttukku Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daily Namma Thiriyuromae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Namma Thiriyuromae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattillukkum Thottillukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattillukkum Thottillukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Collegekum Marriagekum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collegekum Marriagekum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaiyila Pogum Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaiyila Pogum Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaraiyil Thoongum Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaraiyil Thoongum Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figure ah Thaethidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure ah Thaethidavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Vachidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Vachidavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pursu Muluvathumaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pursu Muluvathumaey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">BP Yaeridicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BP Yaeridicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tension Kudidicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Kudidicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunthu Vangidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunthu Vangidavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Thaaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoolagam Sutri Vara Petrollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoolagam Sutri Vara Petrollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaasu Thaaney Engaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaasu Thaaney Engaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Eppavum Remote Controlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Eppavum Remote Controlu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Kaiyula Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kaiyula Than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavum Sikkathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Sikkathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Sikkinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Sikkinalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavum Nikkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Nikkathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaasu Illa Aalu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaasu Illa Aalu Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Dhoosu Than Ma Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Dhoosu Than Ma Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money Money Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money Money Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey Honey Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey Honey Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money Money Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money Money Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey Honey Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey Honey Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Panam Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Panam Panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Illa Yenna Ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Illa Yenna Ponam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Panam Panam Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Panam Panam Panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Illa Yaena Ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Illa Yaena Ponam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Panam Panam Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Panam Panam Panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Illa Yena Ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Illa Yena Ponam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Panam Panam Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Panam Panam Panam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Illa Yena Ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Illa Yena Ponam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Padachathumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Padachathumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manishan Padacha Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishan Padacha Oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti Saaththan Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Saaththan Intha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Than Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Than Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Elunthathumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Elunthathumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvil Adichukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvil Adichukittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti Porala Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Porala Vaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Than Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Than Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baathalam Varaikkum Paaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baathalam Varaikkum Paaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaalam Thorum Kaasalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaalam Thorum Kaasalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Aagayam Kuda Saaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Aagayam Kuda Saaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ambaani Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ambaani Aanaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ara Ticket Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ara Ticket Aanaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Enna Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Enna Life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaa Kuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaa Kuthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaasu Knife
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaasu Knife"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money  No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money  No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah No MoneyNo Honeyda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah No MoneyNo Honeyda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Money Money Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money Money Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey Honey Honey Honey Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey Honey Honey Honey Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money Money Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money Money Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey Honey Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey Honey Honey da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Money No Money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Money No Money"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No Honey No Honey da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Honey No Honey da"/>
</div>
</pre>
