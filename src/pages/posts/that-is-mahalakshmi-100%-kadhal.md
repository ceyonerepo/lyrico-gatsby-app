---
title: "that is mahalakshmi song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "That Is Mahalakshmi"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_jpoGWqwqLY"
type: "happy"
singers:
  - Clinton Cerejo
  - Megha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumma Dumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumma Dumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emmammaa Jeyichitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmammaa Jeyichitaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">King of King Aa King Konga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King of King Aa King Konga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Jump Il Over Take Pannitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Jump Il Over Take Pannitaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Over Night Il Ohhho Nu Aayitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Over Night Il Ohhho Nu Aayitaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minus Minus Plus Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minus Minus Plus Unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plusum Plusum Minusse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plusum Plusum Minusse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa Kanakku Sonnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Kanakku Sonnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First U Vanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First U Vanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thaan King Unnu Sonnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaan King Unnu Sonnaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thaan Pokkunnu Ninnaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thaan Pokkunnu Ninnaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Facebook Twitter Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Twitter Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Hero Aannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Hero Aannaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Balu Fan Su Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Balu Fan Su Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippa Maara Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Maara Vachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekka Chekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka Chekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Firstu Vandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firstu Vandhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamaavu Pulli Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamaavu Pulli Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Star A Maartaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star A Maartaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katta Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Vandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speedaa Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedaa Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Flight A Aitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flight A Aitaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paper Tv News Il Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper Tv News Il Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Famous Aitaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Famous Aitaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Formula Baluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Formula Baluvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dragulavaa Paathome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dragulavaa Paathome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Casula Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Casula Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Top La Vanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Top La Vanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zero Ippo Thaan Nooraachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero Ippo Thaan Nooraachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lastu Ippo Thaan First Aachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lastu Ippo Thaan First Aachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Muyalu Aamai Kadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muyalu Aamai Kadhaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aamai Jeychiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aamai Jeychiche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sitterumbu Singatha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sitterumbu Singatha Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saachiduchey Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saachiduchey Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inky Pinky Aattam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inky Pinky Aattam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Puttaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Puttaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Ye Kanna Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Ye Kanna Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu Puttaale Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Puttaale Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ego Va Thaan Go Go Nnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ego Va Thaan Go Go Nnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solli Puttaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Puttaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam Potta Baluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Potta Baluvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhi Vanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhi Vanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaraana Bouque Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaraana Bouque Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toppu Marku Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toppu Marku Ku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Red U Carpet Pottu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Red U Carpet Pottu Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Gate u KKu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Gate u KKu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is Mahalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That Is That Is Mahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is That Is Mahalakshmi"/>
</div>
</pre>
