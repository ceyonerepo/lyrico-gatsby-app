---
title: "naa yaarunu theriyuma song lyrics"
album: "Raja Ranguski"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Raj"
director: "Dharani Dharan"
path: "/albums/raja-ranguski-lyrics"
song: "Naa Yaarunu Theriyuma"
image: ../../images/albumart/raja-ranguski.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qt0ftxnXOsw"
type: "intro"
singers:
  - STR (Simbu)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa yaarunu theriyuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yaarunu theriyuma "/>
</div>
<div class="lyrico-lyrics-wrapper">haahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jeichida mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jeichida mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">haa haa haahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haa haa haahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kazhugu paarvakaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kazhugu paarvakaran"/>
</div>
<div class="lyrico-lyrics-wrapper">hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thorathi thooka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thorathi thooka "/>
</div>
<div class="lyrico-lyrics-wrapper">porenhey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porenhey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa yaarunu theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yaarunu theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jeichida mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jeichida mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kazhugu paarvakaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kazhugu paarvakaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thorathi thooka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thorathi thooka poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa periya vettaikaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa periya vettaikaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri vetchi thakka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri vetchi thakka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thaduka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thaduka mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nirutha mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nirutha mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarka mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pathunga mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pathunga mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa yaarunu theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yaarunu theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jeichida mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jeichida mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi odu odu odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un thola urikka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thola urikka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula maala seiya poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula maala seiya poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mela anupa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mela anupa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha naala kurikka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha naala kurikka poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee theda mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee theda mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikka valaikka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikka valaikka mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai azhikka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai azhikka mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa yaarunu theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yaarunu theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jeichida mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jeichida mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja odu odu odu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja odu odu odu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi odu odu oduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi odu odu oduuu"/>
</div>
</pre>
