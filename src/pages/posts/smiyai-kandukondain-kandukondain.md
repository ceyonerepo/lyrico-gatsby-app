---
title: "smiyai song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Smiyai"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/frSw_l_17SM"
type: "happy"
singers:
  - Devan Ekambaram
  - Clinton Cerejo
  - Dominique Cerejo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Smiyai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smiyai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai Thirudivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Thirudivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magnet Vizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magnet Vizhiyaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathai Thirudivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathai Thirudivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magnet Vizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magnet Vizhiyaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai Thirudivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Thirudivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Centimeter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Centimeter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootha Punnagayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootha Punnagayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeevan Alandhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan Alandhuvittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhinaalil Poo Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhinaalil Poo Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhinelil Thaen Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhinelil Thaen Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vaakumoolam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vaakumoolam Edharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pull Veliyin Thaagam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull Veliyin Thaagam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poonj Saaral Megam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonj Saaral Megam Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nanaithu Poganum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nanaithu Poganum Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Veril Uyir Konjam Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Veril Uyir Konjam Minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thooral Podu Illai Saaral Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thooral Podu Illai Saaral Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Naanam Nanaiyattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Naanam Nanaiyattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirandha Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandha Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirandha Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandha Bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirantha Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirantha Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Va Vaazhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Va Vaazhavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olitha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olitha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirpadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirpadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirpadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirpadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Va Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Va Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannilae Yei Yei Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae Yei Yei Yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorpanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorpanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyudhey Yei Yei Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyudhey Yei Yei Yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thaamadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thaamadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavil Paal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavil Paal Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seidha Tholu Kandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidha Tholu Kandru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neringi Vaa Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neringi Vaa Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yai Yai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yai Yai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupai Thindral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupai Thindral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inika Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inika Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adharku Per Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharku Per Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka Ka Kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka Ka Kaadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irakka Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakka Sonnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikka Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adharku Per Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharku Per Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka Ka Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka Ka Kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondhalin Hae Hae Hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhalin Hae Hae Hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodithanam Ho Ho Ho Kollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodithanam Ho Ho Ho Kollavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Vaangivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Vaangivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookam Kalainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam Kalainthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavu Kalaiyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Kalaiyadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Vaalgndren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Vaalgndren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yai Yai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yai Yai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magnet Vizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magnet Vizhiyaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai Thirudivittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai Thirudivittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Centimeter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Centimeter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pooththa Punnagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooththa Punnagaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeevan Alandhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan Alandhuvittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhinaalil Poo Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhinaalil Poo Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhinezhil Thaen Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhinezhil Thaen Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vaakumoolam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vaakumoolam Edharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pull Veliyin Thaagam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull Veliyin Thaagam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poonj Saaral Megam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonj Saaral Megam Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nanaithu Poganum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nanaithu Poganum Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Veril Uyir Konjam Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Veril Uyir Konjam Minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thooral Podu Illai Saaral Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thooral Podu Illai Saaral Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Naanam Nanaiyattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Naanam Nanaiyattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smaiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smaiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smayiyai Yai Yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smayiyai Yai Yai"/>
</div>
</pre>
