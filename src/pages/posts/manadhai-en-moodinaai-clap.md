---
title: "manadhai en moodinaai song lyrics"
album: "Clap"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Prithivi Adithya"
path: "/albums/clap-lyrics"
song: "Manadhai En Moodinaai"
image: ../../images/albumart/clap.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dAjMQs37SSo"
type: "happy"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam urudhi kondaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam urudhi kondaale"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam un kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam un kaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhvu unakagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhvu unakagave"/>
</div>
<div class="lyrico-lyrics-wrapper">pokum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokum kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sorvu vendamadaa ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorvu vendamadaa ho"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna paruvam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna paruvam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirumeen neendhi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirumeen neendhi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">solli thandhargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli thandhargala"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilgal parandhu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilgal parandhu sella"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam sonnargalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam sonnargalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadhaigal pottu dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhaigal pottu dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">megangal vaanil pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megangal vaanil pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhigalai than vagutha pinbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhigalai than vagutha pinbu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga thenral veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga thenral veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">malarum malar vanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarum malar vanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sila sedigal poothalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sila sedigal poothalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothidum malargale pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothidum malargale pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">pootha malar pola nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pootha malar pola nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">malarnthu manam veesa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarnthu manam veesa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aruvi pola kuruvi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruvi pola kuruvi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">manathu maratume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathu maratume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam urudhi kondaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam urudhi kondaale"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam un kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam un kaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoongathe thoongum vizhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongathe thoongum vizhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkathe puthiya vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkathe puthiya vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennam pol vaalvu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennam pol vaalvu irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">enni paar sakthi pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enni paar sakthi pirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thayin madi polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayin madi polave"/>
</div>
<div class="lyrico-lyrics-wrapper">unakoru thanthai tholundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakoru thanthai tholundu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri varum ullam thalaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri varum ullam thalaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">virudhum poo maalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virudhum poo maalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thozhil thannaal vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhil thannaal vizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri tholvi ellai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri tholvi ellai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">odi konde iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi konde iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam urudhi kondaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam urudhi kondaale"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam un kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam un kaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhvu unakagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhvu unakagave"/>
</div>
<div class="lyrico-lyrics-wrapper">pokum kaalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pokum kaalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sorvu vendamadaa ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorvu vendamadaa ho"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna paruvam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna paruvam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirumeen neendhi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirumeen neendhi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">solli thandhargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli thandhargala"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilgal parandhu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilgal parandhu sella"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam sonnargalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam sonnargalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhai en moodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhai en moodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thirandhu vei selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thirandhu vei selvame"/>
</div>
</pre>
