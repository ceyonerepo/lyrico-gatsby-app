---
title: "yendi raasathi song lyrics"
album: "Ispade Rajavum Idhaya Raniyum"
artist: "Sam C. S."
lyricist: "Sam C. S."
director: "Ranjit Jeyakodi"
path: "/albums/idpade-rajavum-idhaya-raniyum-lyrics"
song: "Yendi Raasathi"
image: ../../images/albumart/ispade-rajavum-idhaya-raniyum.jpg
date: 2019-03-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gaVPZeIS1Dc"
type: "love"
singers:
  - D. Sathyaprakash
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yendi Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mela Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konji Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhka Poora Pesa Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka Poora Pesa Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Pothumunu Osa Osa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Pothumunu Osa Osa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya Osa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Osa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Bhasai Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Bhasai Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesa Pesa Pesa Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Pesa Pesa Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkadi Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil Vanthu Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil Vanthu Sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Pirinjitta Thudikkaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pirinjitta Thudikkaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mani Kanakkula Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Kanakkula Unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattum Thaane Nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum Thaane Nenaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Enna Marakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Enna Marakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmoodiththanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodiththanama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Pannuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ulla Naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ulla Naanthaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkuren Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuren Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Paithiyam Aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Paithiyam Aaguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththaadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmoodiththanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodiththanama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Pannuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pannuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thedi Neeyum Vaa Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thedi Neeyum Vaa Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unna Niththam Paakkum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Niththam Paakkum Pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Rosa Pookkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Rosa Pookkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seththu Vechcha Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu Vechcha Aasai Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Koodi Pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Koodi Pesuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Nee Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Nee Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukku Noora Aakki Podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukku Noora Aakki Podura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetho Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Thoththu Poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Thoththu Poguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modha Modha Puthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modha Modha Puthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali Thanthu Enna Urukkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Thanthu Enna Urukkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula Vanthu Enna Thookki Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula Vanthu Enna Thookki Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Naanum Enna Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Naanum Enna Kadal Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Azhaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Azhaikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennil Kalakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennil Kalakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakkaatha Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkaatha Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraatha Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraatha Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikkiren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkiren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraatha Mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatha Mogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pikkithey Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pikkithey Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unniley Ennaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unniley Ennaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakkaatha Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkaatha Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraatha Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraatha Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikkiren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkiren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kooda Vaazha Kodi Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kooda Vaazha Kodi Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney"/>
</div>
</pre>
