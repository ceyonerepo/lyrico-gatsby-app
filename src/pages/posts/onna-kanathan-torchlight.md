---
title: "onna kanathan song lyrics"
album: "Torchlight"
artist: "JV"
lyricist: "Vairamuthu"
director: "Abdul Majith"
path: "/albums/torchlight-lyrics"
song: "Onna Kanathan"
image: ../../images/albumart/torchlight.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sgAZBv6ahyU"
type: "love"
singers:
  - Naresh iyer 
  - Monisa 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unna kanathan nan poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanathan nan poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi than ne porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi than ne porantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kanathan nan poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanathan nan poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi than ne porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi than ne porantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kanathan nan poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanathan nan poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi than ne porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi than ne porantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire ne thane enakoru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire ne thane enakoru "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">alaithal varuvene udal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaithal varuvene udal "/>
</div>
<div class="lyrico-lyrics-wrapper">porul aviyum tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porul aviyum tharuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">aavi thevaiya un adimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavi thevaiya un adimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodi thaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodi thaa "/>
</div>
<div class="lyrico-lyrics-wrapper">theva theviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva theviye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru siruthuli punnagai thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru siruthuli punnagai thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothum alage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothum alage"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatru veli theernthu vidil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru veli theernthu vidil"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu uyir moochinil tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu uyir moochinil tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kanathan nan poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanathan nan poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi than ne porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi than ne porantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seru seitha thavangal enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seru seitha thavangal enna "/>
</div>
<div class="lyrico-lyrics-wrapper">nooru kamalangal poothu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru kamalangal poothu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">yelai seitha thavangal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelai seitha thavangal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">theivam en veetil vaalthirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivam en veetil vaalthirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai theendu ellai thaandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai theendu ellai thaandu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir thoondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir thoondu "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai paal paal aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai paal paal aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thool thoolaaku ennai aandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thool thoolaaku ennai aandu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir nee thaane enakoru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir nee thaane enakoru "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kanathan nan poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanathan nan poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi than ne porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi than ne porantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karpai pola unnai kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karpai pola unnai kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kala veliyengum atharipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala veliyengum atharipen"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku pinnal thondrum pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku pinnal thondrum pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai pola vaazha vaalnthirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai pola vaazha vaalnthirupen"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu penne sem bonne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu penne sem bonne"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanne ini nee aanagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanne ini nee aanagi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pennagi vaalvom anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pennagi vaalvom anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire ne thane enakoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire ne thane enakoru"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">alaithaal varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaithaal varuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">udal porul aaviyul tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal porul aaviyul tharuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">aavi thevaiya un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavi thevaiya un "/>
</div>
<div class="lyrico-lyrics-wrapper">adimadi oru nodi thaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimadi oru nodi thaa "/>
</div>
<div class="lyrico-lyrics-wrapper">theva theviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theva theviye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru siruthuli punnagai thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru siruthuli punnagai thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothum alage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothum alage"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatru veli theernthu vidil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru veli theernthu vidil"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu uyir moochinil tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu uyir moochinil tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire ne thane enakoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire ne thane enakoru"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire ne thane enakoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire ne thane enakoru"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam nee thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sitang kuruviye serntha thadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitang kuruviye serntha thadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kattan tharaiyila koodu thadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattan tharaiyila koodu thadi"/>
</div>
</pre>
