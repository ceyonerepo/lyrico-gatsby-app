---
title: "chinnanchiru kiliye song lyrics"
album: "Velvet Nagaram"
artist: "Achu Rajamani"
lyricist: "Kaber Vasuki"
director: "Manojkumar Natarajan"
path: "/albums/velvet-nagaram-lyrics"
song: "Chinnanchiru Kiliye"
image: ../../images/albumart/velvet-nagaram.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i1tFhJgK9GI"
type: "melody"
singers:
  - Achu Rajamani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kangalil ennai kaangayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kangalil ennai kaangayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Putham puthithai naan nindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham puthithai naan nindrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan vaasathil naan vasikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan vaasathil naan vasikkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki sorgam naan sendrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki sorgam naan sendrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuthaagavae sariyaanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthaagavae sariyaanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan ullamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan ullamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjamae nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjamae nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram sellatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram sellatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaagavae kaagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaagavae kaagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan piranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan piranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thandhavalae thandhavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham thandhavalae thandhavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kaadhalae…ae……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kaadhalae…ae……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram nindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram nindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi pennae devathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pennae devathaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanchiru kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanchiru kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjal azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjal azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaal sinungiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaal sinungiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi endhan kann azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi endhan kann azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugamaanathae pudhidhaanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaanathae pudhidhaanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naam vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naam vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjamae nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjamae nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram sellatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram sellatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaagavae kaagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaagavae kaagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan piranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan piranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thandhavalae thandhavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham thandhavalae thandhavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowu wow hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowu wow hoo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
</pre>
