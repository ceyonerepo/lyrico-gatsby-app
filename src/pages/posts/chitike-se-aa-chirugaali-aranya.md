---
title: "chitike se song lyrics"
album: "Aranya"
artist: "Shantanu Moitra"
lyricist: "Vanamali"
director: "Prabhu Solomon"
path: "/albums/aranya-lyrics"
song: "Chitike Se aa Chirugaali"
image: ../../images/albumart/aranya.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RpxvT-l5xJI"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chitike’se’aa chirugaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitike’se’aa chirugaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesi aade nemali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesi aade nemali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kila kila mane kokila valee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kila kila mane kokila valee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadenule haayiga laali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadenule haayiga laali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivantha okatai ahwaname palakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivantha okatai ahwaname palakani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadani paadani chindu ladaiyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadani paadani chindu ladaiyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadani paadani chindu ladaiyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadani paadani chindu ladaiyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chitikesi aa chirugaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitikesi aa chirugaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesi aade nemali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesi aade nemali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivantha okatai ahwaname palakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivantha okatai ahwaname palakani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadani paadani chindu ladaiyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadani paadani chindu ladaiyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadani paadani chindu ladaiyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadani paadani chindu ladaiyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chukk ledi koonalla raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukk ledi koonalla raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivamma paapalla raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivamma paapalla raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andamaina lokam idee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamaina lokam idee"/>
</div>
<div class="lyrico-lyrics-wrapper">Andukoo Mani antunna dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andukoo Mani antunna dee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kommalloo Pooche poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommalloo Pooche poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuripinchenu akshinthallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuripinchenu akshinthallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allari chese themmeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allari chese themmeralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosenule sumagandhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosenule sumagandhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saageni darulloo harivillanee dimsenii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saageni darulloo harivillanee dimsenii"/>
</div>
</pre>
