---
title: "oru nooru murai song lyrics"
album: "Dev"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Rajath Ravishankar"
path: "/albums/dev-lyrics"
song: "Oru Nooru Murai"
image: ../../images/albumart/dev.jpg
date: 2019-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NYVI5M2u9iM"
type: "love"
singers:
  - D. Sathyaprakash
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Nooru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nooru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Pona Pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Pona Pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Indru Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Indru Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Indha Bodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Indha Bodhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Endru Sol Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Endru Sol Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vandhen Un Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vandhen Un Pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neduthooram Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduthooram Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendu Konde Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendu Konde Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Varthaiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Varthaiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathai Nee Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathai Nee Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethenum Sol Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethenum Sol Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollum Sol Theane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollum Sol Theane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nee Eduthai Sirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Eduthai Sirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamarai Poo Varumo Tharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamarai Poo Varumo Tharai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrilum Nee Sethukkum Kaanal Silai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilum Nee Sethukkum Kaanal Silai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkul Nee Ninaikkum Adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul Nee Ninaikkum Adhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Solla Vendumendral Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Solla Vendumendral Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verondrum Thondravillai Naan Mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondrum Thondravillai Naan Mazhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nooru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nooru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Pona Pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Pona Pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Indru Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Indru Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Indha Bodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Indha Bodhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Endru Sol Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Endru Sol Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vandhen Un Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vandhen Un Pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Mazhaiynil Nanainthathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mazhaiynil Nanainthathu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Naduvinil Kulithathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Naduvinil Kulithathu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Maragatha Malaigalai Paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Maragatha Malaigalai Paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavilum Vaithathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavilum Vaithathu Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilavil Siragugal Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavil Siragugal Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaava Unnudan Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaava Unnudan Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthen Vizhunthen Karainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthen Vizhunthen Karainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nooru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nooru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Pona Pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Pona Pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Indru Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Indru Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Indha Bodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Indha Bodhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Endru Sol Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Endru Sol Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vandhen Un Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vandhen Un Pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paranthidum Uyarathil Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paranthidum Uyarathil Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Paravayin Paarvayil Paarththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Paravayin Paarvayil Paarththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Siru Siru Uruvangal Virainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Siru Siru Uruvangal Virainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nagarvathai Erumbena Ninaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nagarvathai Erumbena Ninaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellame Nadakkuthu Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Nadakkuthu Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Pidikithu Nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Pidikithu Nandru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthen Enai Naan Ilanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthen Enai Naan Ilanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pola Entha Naalum Endrum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola Entha Naalum Endrum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Melum Varum Endru Nambavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Melum Varum Endru Nambavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Engum Oh Kaarmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Engum Oh Kaarmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Endral Hey Neer Vaarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Endral Hey Neer Vaarkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thogai Mayil Thotri Konda Tholil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thogai Mayil Thotri Konda Tholil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Eeram Vandhu Saaral Veesum Naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Eeram Vandhu Saaral Veesum Naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeadhenum Sol Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeadhenum Sol Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollum Sol Theane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollum Sol Theane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yen Ennai Nee Eduthai Sirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yen Ennai Nee Eduthai Sirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamarai Poo Varumo Tharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamarai Poo Varumo Tharai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrilum Nee Sethukkum Kaanal Silai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilum Nee Sethukkum Kaanal Silai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkule Ninaikkum Adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkule Ninaikkum Adhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Solla Villai Endral Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Solla Villai Endral Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogattum Nambivitten Nee Mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogattum Nambivitten Nee Mazhalai"/>
</div>
</pre>
