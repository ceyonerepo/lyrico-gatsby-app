---
title: "jalabulajangu song lyrics"
album: "Don"
artist: "Anirudh Ravichander"
lyricist: "Rokesh"
director: "Cibi Chakaravarthi"
path: "/albums/don-lyrics"
song: "Jalabulajangu"
image: ../../images/albumart/don.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2VOL-VWXMnQ"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amukku Dumukku Amaal Dumaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku Dumukku Amaal Dumaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jalabula Jangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jalabula Jangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dama Duma Dangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dama Duma Dangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thug Life La Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thug Life La Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Settingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Settingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jinukunaa Jingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jinukunaa Jingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Our Age Yo Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our Age Yo Youngu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Parakanum Pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Parakanum Pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Fittingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Fittingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavo Bhai Dosta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavo Bhai Dosta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayegaa Aayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayegaa Aayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly Mela Maja Aayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Mela Maja Aayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Vibe Jaldi Jaayegaa Jaayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Vibe Jaldi Jaayegaa Jaayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peace Dhaan Bro Tension Jaayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peace Dhaan Bro Tension Jaayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Ah Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Ah Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Ah Bunk Adichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Ah Bunk Adichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Deanuku Dhaan Paadam Ethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deanuku Dhaan Paadam Ethaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan Paa Don Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan Paa Don Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilla Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilla Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalukku Jalukku Jalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalukku Jalukku Jalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Arrear Adhigam Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrear Adhigam Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">But Life La Jeichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Life La Jeichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhan Paa Don Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhan Paa Don Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Ah Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Ah Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaikitta Suthudhu Funnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikitta Suthudhu Funnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipuchu Confirmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipuchu Confirmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Ya Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Ya Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilla Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilla Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalukku Jalukku Jalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalukku Jalukku Jalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Puchikka Podhu Sulukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puchikka Podhu Sulukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Staffsu Ellam Dandanaka Dannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Staffsu Ellam Dandanaka Dannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amukku Dumukku Amaal Dumaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku Dumukku Amaal Dumaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jalabula Jangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jalabula Jangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dama Duma Dangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dama Duma Dangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thug Life La Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thug Life La Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Settingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Settingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jinukunaa Jingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jinukunaa Jingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Our Age Yo Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our Age Yo Youngu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Parakanum Pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Parakanum Pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Fittingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Fittingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhungethaadha Kadupethaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhungethaadha Kadupethaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriousa Pesi Nee Sirippethaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriousa Pesi Nee Sirippethaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Ootadha Padam Kaatadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Ootadha Padam Kaatadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanambha Last Bencha Verruppethaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanambha Last Bencha Verruppethaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dama Dama Dama Dama Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama Dama Dama Dama Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukku Dampa Dammapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damukku Dampa Dammapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Galanurukkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Galanurukkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Ennappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Ennappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para Paranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para Paranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhuduvom Onnaappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhuduvom Onnaappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachanaa Therikum Yappappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachanaa Therikum Yappappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Freeya Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freeya Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Ah Bunk Adichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Ah Bunk Adichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Deanuku Dhaan Paadam Ethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deanuku Dhaan Paadam Ethaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan Paa Don Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan Paa Don Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilla Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilla Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalukku Jalukku Jalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalukku Jalukku Jalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Arrear Adhigam Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrear Adhigam Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">But Life La Jeichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Life La Jeichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhan Paa Don Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhan Paa Don Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Free Ah Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Ah Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaikitta Suthudhu Funnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikitta Suthudhu Funnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipuchu Confirmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipuchu Confirmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Ya Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Ya Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilla Vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilla Vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalukku Jalukku Jalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalukku Jalukku Jalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Puchikka Podhu Sulukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puchikka Podhu Sulukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Staffsu Ellam Dandanaka Dannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Staffsu Ellam Dandanaka Dannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amukku Dumukku Amaal Dumaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku Dumukku Amaal Dumaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jalabula Jangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jalabula Jangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Dama Duma Dangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Dama Duma Dangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thug Life La Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thug Life La Kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Settingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Settingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jinukunaa Jingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jinukunaa Jingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Our Age Yo Youngu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our Age Yo Youngu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Parakanum Pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Parakanum Pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Fittingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Fittingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavo Bhai Dosta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavo Bhai Dosta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayegaa Aayegaa Inga Aayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayegaa Aayegaa Inga Aayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna Thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna Thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad Vibe Jaldi Jaayegaa Jaayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad Vibe Jaldi Jaayegaa Jaayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaayegaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amukku Dumukku Amaal Dumaalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku Dumukku Amaal Dumaalthaan"/>
</div>
</pre>
