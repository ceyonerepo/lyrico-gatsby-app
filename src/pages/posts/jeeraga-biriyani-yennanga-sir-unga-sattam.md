---
title: "jeeraga biriyani song lyrics"
album: "Yennanga Sir Unga Sattam"
artist: "Guna Balasubramanian"
lyricist: "Jegan Kaviraj"
director: "Prabhu Jeyaram"
path: "/albums/yennanga-sir-unga-sattam-lyrics"
song: "Jeeraga Biriyani"
image: ../../images/albumart/yennanga-sir-unga-sattam.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nrzfei_qZQg"
type: "love"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un azhagaana kannukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagaana kannukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhathil thallivitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhathil thallivitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaanu paarvaiyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaanu paarvaiyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">solli vidaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli vidaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tune aah oru raagam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune aah oru raagam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Moon aah un face uh therithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moon aah un face uh therithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana en manasum ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana en manasum ada "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanathil parakkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanathil parakkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allah ena unna thozhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allah ena unna thozhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa naan mannil vizhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa naan mannil vizhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliye en kaalum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliye en kaalum un "/>
</div>
<div class="lyrico-lyrics-wrapper">paathaiya rasikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaiya rasikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En imaiye perunjumaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En imaiye perunjumaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena kavani oru kanamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena kavani oru kanamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thirumbaama ponaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thirumbaama ponaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbavum varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbavum varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyilaaga unnil vizhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilaaga unnil vizhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayilaaga unnai thodarven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayilaaga unnai thodarven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodinera asaivum adi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodinera asaivum adi "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjila pathiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjila pathiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeraa nee enna nenaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraa nee enna nenaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Joraa enai sundi izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joraa enai sundi izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirkaala madi nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirkaala madi nee "/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vazhum vithiye nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vazhum vithiye nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuliye uyir thuliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliye uyir thuliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pogum unthan vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pogum unthan vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye en iraiye enthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye en iraiye enthan "/>
</div>
<div class="lyrico-lyrics-wrapper">randaam piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="randaam piraiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un azhagaana kannukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagaana kannukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhaththil thallivitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhaththil thallivitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaannu paarvaiyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaannu paarvaiyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">sollividaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollividaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jeeraga biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeeraga biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevaney neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevaney neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatham vaippen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatham vaippen di"/>
</div>
</pre>
