---
title: "kandukondain song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Kandukondain"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CstXxovQubc"
type: "love"
singers:
  - Hariharan
  - Mahalakshmi Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kandukondain Kandukondain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukondain Kandukondain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukonden Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukonden Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralthodum Dhoorathiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralthodum Dhoorathiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavu Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilaa Velicham Kinnathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaa Velicham Kinnathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundhu Niraindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhu Niraindhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhindhaal Magizhchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhindhaal Magizhchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilaa Veliccham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaa Veliccham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KinnaththaiUdaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KinnaththaiUdaithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Udaipaal Oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Udaipaal Oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kan Paarthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kan Paarthadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kai Serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Serumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Seraamaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Seraamaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaneer Serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneer Serumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukonden Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukonden Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar Manjam Vizhi Kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Manjam Vizhi Kenjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Anjum Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Anjum Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Minjum Ival Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Minjum Ival Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thanjam Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thanjam Allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thanimai Kadhavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thanimai Kadhavin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaazh Neekkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazh Neekkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukonden Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukonden Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam Thirandhaal Adharkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Thirandhaal Adharkul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkal Thiranthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Thiranthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atharkul Un Kural Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkul Un Kural Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalai Thirandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Thirandhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Valarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalin Viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin Viralgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaiyum Thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaiyum Thirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thediyae Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thediyae Ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Payanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Payanamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Saalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Saalaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Veettil Mudiyumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veettil Mudiyumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kanavu Mangaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kanavu Mangaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Manadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Manadhil Inaiyumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Manadhil Inaiyumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukonden Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukonden Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralthodum Dhooraththiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralthodum Dhooraththiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavu Kandukondaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu Kandukondaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyin Thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyin Thedal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Kadal Kaanbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Kadal Kaanbadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Thedal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Unai Kaanbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Unai Kaanbadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalkonda Nadhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalkonda Nadhiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam Thanai Ilakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Thanai Ilakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnil Kalandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnil Kalandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhumugam Kidaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumugam Kidaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natchathirangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathirangalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naaril Kattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naaril Kattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Neramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Neramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kadhavu Thattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhavu Thattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal Dhevaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Dhevaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Imaiyil Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Imaiyil Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigal Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukondaen Kandukondaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukondaen Kandukondaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukondaen Kandukondaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukondaen Kandukondaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Mugam Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralthodum Dhoorathiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralthodum Dhoorathiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralthodum Dhoorathiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralthodum Dhoorathiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavu Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukonden Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukonden Kandukonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathal Mugam Kandukonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathal Mugam Kandukonden"/>
</div>
</pre>
