---
title: "mana kadha song lyrics"
album: "World Famous Lover"
artist: "Gopi Sundar"
lyricist: "Ramajogayya Sastry"
director: "Kranthi Madhav"
path: "/albums/world-famous-lover-lyrics"
song: "Mana Kadha"
image: ../../images/albumart/world-famous-lover.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bJY_gzCIx_A"
type: "happy"
singers:
  - L.V. Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mana Kadha Preme Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Kadha Preme Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamadhi Guruthe Radha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamadhi Guruthe Radha"/>
</div>
<div class="lyrico-lyrics-wrapper">Upiri Kalchey Badha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upiri Kalchey Badha"/>
</div>
<div class="lyrico-lyrics-wrapper">Challarchaga Cheliya Rava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challarchaga Cheliya Rava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reppa Muyaninka Anchulalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppa Muyaninka Anchulalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi Chemmanayi Vechi Chusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Chemmanayi Vechi Chusthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Lothu Gayalathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Lothu Gayalathona"/>
</div>
<div class="lyrico-lyrics-wrapper">Manduthondhi Mouna Vedhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manduthondhi Mouna Vedhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Where Are You Yamini? Yamini?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where Are You Yamini? Yamini?"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting For You Yamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting For You Yamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Where Are You Yamini? Yamini?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where Are You Yamini? Yamini?"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting For You Yamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting For You Yamini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilavila Hrudhayam Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavila Hrudhayam Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellarina Udhayam Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellarina Udhayam Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Melakuva Chuse Lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Melakuva Chuse Lope"/>
</div>
<div class="lyrico-lyrics-wrapper">Chejarina Kalavayinavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chejarina Kalavayinavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Nallu Ni Cheruva Kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Nallu Ni Cheruva Kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusthakamla Padi Unnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthakamla Padi Unnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Leni Ye Dharilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Leni Ye Dharilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetuga Konasaganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetuga Konasaganu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Where Are You Yamini? Yamini?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where Are You Yamini? Yamini?"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting For You Yamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting For You Yamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Where Are You Yamini? Yamini?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where Are You Yamini? Yamini?"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting For You Yamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting For You Yamini"/>
</div>
</pre>
