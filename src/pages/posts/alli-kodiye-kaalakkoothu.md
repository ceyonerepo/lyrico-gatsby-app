---
title: "alli kodiye song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "Justin Prabhakaran"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Alli Kodiye"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IjQNy2j4dIk"
type: "happy"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee en molacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee en molacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannanthaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala vethacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala vethacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaa pagalum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaa pagalum mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Irutena irukae aaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutena irukae aaee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illa iravummmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa iravummmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavingae suduthae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavingae suduthae aee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta veliyaa vaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta veliyaa vaaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum vandha vazhiyae yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vandha vazhiyae yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm mmmmhmmmm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmmmhmmmm "/>
</div>
<div class="lyrico-lyrics-wrapper">mmmhmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmhmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmmmhmmmm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmmmhmmmm "/>
</div>
<div class="lyrico-lyrics-wrapper">mmmhmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mmmhmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee en molacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee en molacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannanthaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala vethacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala vethacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya ninaivodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya ninaivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru odai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru odai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum athil neendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum athil neendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaalum moozhgum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum moozhgum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraithaalum maraivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraithaalum maraivathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam polae vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam polae vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pirinthaalum viduvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pirinthaalum viduvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal polae thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal polae thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirudanae saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirudanae saagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali irundhum sirikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali irundhum sirikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyabagangal thoduruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagangal thoduruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval mugam marakka marakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval mugam marakka marakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu thaanae nelavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thaanae nelavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasukkulla kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukkulla kalavaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana nana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana nana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanthaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana nana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana nana naa naa"/>
</div>
</pre>
