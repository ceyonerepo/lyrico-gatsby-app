---
title: "yemito ee sambaram song lyrics"
album: "Runam"
artist: "S.V. Mallik Teja"
lyricist: "S.V. Mallik Teja"
director: "S. Gundreddy"
path: "/albums/runam-lyrics"
song: "Yemito Ee Sambaram"
image: ../../images/albumart/runam.jpg
date: 2019-04-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wzUZdUPnHQc"
type: "love"
singers:
  - Haricharan
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yemito ee sambharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemito ee sambharam"/>
</div>
<div class="lyrico-lyrics-wrapper">andhuthondhi ambharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhuthondhi ambharam"/>
</div>
<div class="lyrico-lyrics-wrapper">ee runam a janma punyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee runam a janma punyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">endhuko ee poovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhuko ee poovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">venta paduthondhi ee kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venta paduthondhi ee kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranam nuvvallina bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranam nuvvallina bandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo ale ela mila mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo ale ela mila mila"/>
</div>
<div class="lyrico-lyrics-wrapper">ee merupula saadhyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee merupula saadhyamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kila kila gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kila kila gala gala"/>
</div>
<div class="lyrico-lyrics-wrapper">palukule aaradhyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palukule aaradhyamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee alajaadi ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee alajaadi ento"/>
</div>
<div class="lyrico-lyrics-wrapper">kalabadi nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalabadi nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">godave chesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godave chesthundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee panduga ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee panduga ento"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo motham"/>
</div>
<div class="lyrico-lyrics-wrapper">rangulu poosthandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangulu poosthandhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundelona inni naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelona inni naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">migilina dhigule ipude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="migilina dhigule ipude"/>
</div>
<div class="lyrico-lyrics-wrapper">paari poindhaa jevithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paari poindhaa jevithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">kothavelugu korakunda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothavelugu korakunda nee"/>
</div>
<div class="lyrico-lyrics-wrapper">varami cheripoindha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varami cheripoindha "/>
</div>
<div class="lyrico-lyrics-wrapper">aagithe baagunde kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagithe baagunde kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">ee samayam illage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee samayam illage"/>
</div>
<div class="lyrico-lyrics-wrapper">aagithe baagunde kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagithe baagunde kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">ee samayam illage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee samayam illage"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kaadhe kadha kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kaadhe kadha kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi nijame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi nijame"/>
</div>
<div class="lyrico-lyrics-wrapper">cheli mide cheraganithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheli mide cheraganithe"/>
</div>
<div class="lyrico-lyrics-wrapper">ee preme ee alajadi ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee preme ee alajadi ento"/>
</div>
<div class="lyrico-lyrics-wrapper">kalabadi nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalabadi nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">godave chesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godave chesthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee panduga ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee panduga ento"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo motham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo motham "/>
</div>
<div class="lyrico-lyrics-wrapper">rangulu poosthandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangulu poosthandhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inthavaraku intha parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthavaraku intha parugu"/>
</div>
<div class="lyrico-lyrics-wrapper">ledhu kadha emayyindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ledhu kadha emayyindho"/>
</div>
<div class="lyrico-lyrics-wrapper">naa adugulaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa adugulaku "/>
</div>
<div class="lyrico-lyrics-wrapper">nee vallane naa manasila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vallane naa manasila"/>
</div>
<div class="lyrico-lyrics-wrapper">rekkalanu thodigesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekkalanu thodigesthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa oohalaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa oohalaku "/>
</div>
<div class="lyrico-lyrics-wrapper">undani ullaasamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undani ullaasamika"/>
</div>
<div class="lyrico-lyrics-wrapper">enaadu etu vellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaadu etu vellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">undani ullaasamika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undani ullaasamika"/>
</div>
<div class="lyrico-lyrics-wrapper">enaadu etu vellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaadu etu vellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">paravasame paavaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravasame paavaname"/>
</div>
<div class="lyrico-lyrics-wrapper">premante naaloni praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante naaloni praname"/>
</div>
<div class="lyrico-lyrics-wrapper">neevante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neevante "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee alajaadi ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee alajaadi ento"/>
</div>
<div class="lyrico-lyrics-wrapper">kalabadi nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalabadi nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">godave chesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godave chesthundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee panduga ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee panduga ento"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo motham"/>
</div>
<div class="lyrico-lyrics-wrapper">rangulu poosthandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangulu poosthandhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yemito ee sambharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemito ee sambharam"/>
</div>
<div class="lyrico-lyrics-wrapper">andhuthondhi ambharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhuthondhi ambharam"/>
</div>
<div class="lyrico-lyrics-wrapper">ee runam a janma punyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee runam a janma punyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">endhuko ee poovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhuko ee poovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">venta paduthondhi ee kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venta paduthondhi ee kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranam nuvvallina bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranam nuvvallina bandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo ale ela mila mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo ale ela mila mila"/>
</div>
<div class="lyrico-lyrics-wrapper">ee merupula saadhyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee merupula saadhyamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kila kila gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kila kila gala gala"/>
</div>
<div class="lyrico-lyrics-wrapper">palukule aaradhyamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palukule aaradhyamo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee alajaadi ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee alajaadi ento"/>
</div>
<div class="lyrico-lyrics-wrapper">kalabadi nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalabadi nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">godave chesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godave chesthundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee panduga ento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee panduga ento"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo motham"/>
</div>
<div class="lyrico-lyrics-wrapper">rangulu poosthandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangulu poosthandhi"/>
</div>
</pre>
