---
title: "obulamma song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "M. M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Obulamma"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JiqIa5uzYGk"
type: "love"
singers:
  - Satya Yamini
  - PVNS Rohit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ginja Ginja Ginja Meeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ginja Ginja Ginja Meeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Busaka Busaka Busaka Teesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Busaka Busaka Busaka Teesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyanga Bathemayi Pooye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyanga Bathemayi Pooye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Botte Katti Chetha Battina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Botte Katti Chetha Battina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethi Loki Cheraleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethi Loki Cheraleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundujalla Aarata Padipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundujalla Aarata Padipoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Obulamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Obulamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Karra Meni Chaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Karra Meni Chaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhugumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhugumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kapara Kapara Vekuva Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapara Kapara Vekuva Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamantha Lekkalu Gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamantha Lekkalu Gatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona Nee Peru Japamaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona Nee Peru Japamaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yidivarakepudu Teliyani Yeragani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yidivarakepudu Teliyani Yeragani"/>
</div>
<div class="lyrico-lyrics-wrapper">Turupe Maimaripisthu Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turupe Maimaripisthu Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantikemo Kunuke Doorapu Chuttamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantikemo Kunuke Doorapu Chuttamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulu Kannulu Vintunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulu Kannulu Vintunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupulu Chupulu Chebuthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupulu Chupulu Chebuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalu Maatalu Chusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalu Maatalu Chusthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Magathalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magathalalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevvarikevvaru Saawaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvarikevvaru Saawaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkadikakkada Prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkadikakkada Prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppatikappati Yedurayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppatikappati Yedurayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupulalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chadivesademo Naa Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadivesademo Naa Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Untaade Needai Reypavalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaade Needai Reypavalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tishtesinaade Goontharalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tishtesinaade Goontharalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pommante Pode Eedigalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pommante Pode Eedigalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Obulamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Obulamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttachendu Aatallona Pulakomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttachendu Aatallona Pulakomma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kapara Kapara Rethiri Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapara Kapara Rethiri Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamantha Lekkalu Tappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamantha Lekkalu Tappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona Nee Peru Japamaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona Nee Peru Japamaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yidivarakepudu Teliyani Yeragani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yidivarakepudu Teliyani Yeragani"/>
</div>
<div class="lyrico-lyrics-wrapper">Talape Maimaripisthu Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talape Maimaripisthu Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantikemo Kunuke Doorapu Chuttamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantikemo Kunuke Doorapu Chuttamaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Obulamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Obulamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Karra Meni Chaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Karra Meni Chaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhugumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhugumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Obulamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Obulamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttachendu Aatallona Pulakomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttachendu Aatallona Pulakomma"/>
</div>
</pre>
