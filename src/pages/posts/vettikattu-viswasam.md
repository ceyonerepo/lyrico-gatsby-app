---
title: 'vettikattu song lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Yugabharathi'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Vettikattu'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
lang: tamil
singers:
- Shankar Mahadevan
youtubeLink: "https://www.youtube.com/embed/jUD3OuR1ado"
type: 'mass'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thookku dhora na adaavadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookku dhora na adaavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookku dhora na alappara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookku dhora na alappara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookku dhora na thadaaladi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookku dhora na thadaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookku dhora na kattu kadangatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookku dhora na kattu kadangatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Grammathu kaattu adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Grammathu kaattu adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adaavadi thookku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Adaavadi thookku dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Alapparayaana dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Alapparayaana dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaaladi sokku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadaaladi sokku dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunathula yethu kora
<input type="checkbox" class="lyrico-select-lyric-line" value="Gunathula yethu kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sandaikkum pandhikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandaikkum pandhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mundhuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikku mattum thaan anjuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Saamikku mattum thaan anjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukku onnuna thulluvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannukku onnuna thulluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaiyum thittithaan solluvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbaiyum thittithaan solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhora ezhunthu vandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhora ezhunthu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraiyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Paraiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri vandha adithadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yegiri vandha adithadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithadi adithadi adithadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adithadi adithadi adithadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu …heyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu …heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy adaavadi thookku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy adaavadi thookku dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Alapparayaana dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Alapparayaana dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaaladi sokku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadaaladi sokku dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunathula yethu kora
<input type="checkbox" class="lyrico-select-lyric-line" value="Gunathula yethu kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hohoo ohooo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hohoo ohooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hohoo ohoo oo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hohoo ohoo oo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aduthavan munnaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Aduthavan munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukkumae kaikatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukkumae kaikatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangura koottam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adangura koottam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padakkunnu munnera
<input type="checkbox" class="lyrico-select-lyric-line" value="Padakkunnu munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikkira aalaattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenaikkira aalaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungiyum paathathilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Pathungiyum paathathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Koduvaala naanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Koduvaala naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookki vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai illainu solli nippom
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagai illainu solli nippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda saanji poga ennaamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Koda saanji poga ennaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadham senjaethaan kokkarippom
<input type="checkbox" class="lyrico-select-lyric-line" value="Vadham senjaethaan kokkarippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Varum rosaththa kaattama
<input type="checkbox" class="lyrico-select-lyric-line" value="Varum rosaththa kaattama"/>
</div>
  <div class="lyrico-lyrics-wrapper">Marachikittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Marachikittu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Veli vesanthaan podaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Veli vesanthaan podaama"/>
</div>
  <div class="lyrico-lyrics-wrapper">Veluthukattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Veluthukattu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Balam enna enna enna kaattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Balam enna enna enna kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Podraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Podraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adaavadi thookku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Adaavadi thookku dhora"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Alapparayaana dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Alapparayaana dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaaladi sokku dhora
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadaaladi sokku dhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunathula yethu kora
<input type="checkbox" class="lyrico-select-lyric-line" value="Gunathula yethu kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sandaikkum pandhikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandaikkum pandhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mundhuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikku mattum thaan anjuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Saamikku mattum thaan anjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukku onnuna thulluvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannukku onnuna thulluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaiyum thittithaan solluvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbaiyum thittithaan solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhora ezhunthu vandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhora ezhunthu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraiyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Paraiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri vandha adithadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yegiri vandha adithadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithadi adithadi adithadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adithadi adithadi adithadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti vetti vettikattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti vetti vettikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera chola pandikellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Chera chola pandikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serththu kattu … (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Serththu kattu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
</pre>