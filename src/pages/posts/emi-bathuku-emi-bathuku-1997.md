---
title: "emi bathuku emi bathuku song lyrics"
album: "1997"
artist: "Koti"
lyricist: "Dr.Mohan - Adesh Ravi"
director: "Dr. Mohan"
path: "/albums/1997-lyrics"
song: "Emi Bathuku Emi Bathuku"
image: ../../images/albumart/1997.jpg
date: 2021-11-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fnRP8vsqNic"
type: "sad"
singers:
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oori Bayata Gudiselallaa Undetollam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oori Bayata Gudiselallaa Undetollam"/>
</div>
<div class="lyrico-lyrics-wrapper">Devunne Kaadhu Gudi Metlanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devunne Kaadhu Gudi Metlanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Koodaa Thaakanollam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodaa Thaakanollam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindileni Chaduvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindileni Chaduvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Leni Pedavaallam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni Pedavaallam"/>
</div>
<div class="lyrico-lyrics-wrapper">Deshamemo Pavithramaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deshamemo Pavithramaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Bathukulemo Apavithramaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Bathukulemo Apavithramaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru Cheppe Vedamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru Cheppe Vedamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Bathukunanthaa Paadu Chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Bathukunanthaa Paadu Chese"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emi Bathuku Emi Bathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Bathuku Emi Bathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedda Bathuku Chedda Bathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedda Bathuku Chedda Bathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedda Bathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedda Bathuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okka Poota Methuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Poota Methuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kosam Memu Saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosam Memu Saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Raathri Panulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Raathri Panulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Chesi Alasipothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesi Alasipothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniki Addu Avuthaarani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniki Addu Avuthaarani "/>
</div>
<div class="lyrico-lyrics-wrapper">Pasipillagaalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasipillagaalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Gintha Kallu Posi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gintha Kallu Posi "/>
</div>
<div class="lyrico-lyrics-wrapper">Matthulone Pandabedtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthulone Pandabedtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Endlakendlu Gadisipoyinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endlakendlu Gadisipoyinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maarani Maa Thalaraathalaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarani Maa Thalaraathalaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emi Jettham Emi Jettham Emi Jettham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jettham Emi Jettham Emi Jettham"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Jettham Emi Jettham Emi Jettham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jettham Emi Jettham Emi Jettham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intidaanni Eedochhina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intidaanni Eedochhina "/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Aadapillanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Aadapillanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddhalaaga Ethkapoyi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddhalaaga Ethkapoyi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamamanthaa Theerchukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamamanthaa Theerchukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennupoosa Iragagotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennupoosa Iragagotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaluka Kosi Petro Posi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaluka Kosi Petro Posi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalabedithe Thagalabedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalabedithe Thagalabedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalabedithe Thagalabedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalabedithe Thagalabedithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirigiraani Aadabiddanu Thalasukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigiraani Aadabiddanu Thalasukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki Vekki Vekki Vekki Memu Edusthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki Vekki Vekki Vekki Memu Edusthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvarikemi Cheppukomu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvarikemi Cheppukomu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaru Mammula Aadhukoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaru Mammula Aadhukoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Jettham Emi Jettham Emi Jettham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jettham Emi Jettham Emi Jettham"/>
</div>
<div class="lyrico-lyrics-wrapper">Emi Jettham Emi Jettham Emi Jettham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emi Jettham Emi Jettham Emi Jettham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaddhalaaga Maa Aadapillanu Ethkapoyinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddhalaaga Maa Aadapillanu Ethkapoyinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Petrol Posi Sajeeva Dahanam Cheyakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petrol Posi Sajeeva Dahanam Cheyakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Champakundaa Idsipedithe Ayya Saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champakundaa Idsipedithe Ayya Saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Aadabidda Maa Kalla Mundhe Unde Saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Aadabidda Maa Kalla Mundhe Unde Saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Bathukuledho Maaku Meme Bathiketollam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Bathukuledho Maaku Meme Bathiketollam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Bathukuledho Maaku Meme Bathiketollam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Bathukuledho Maaku Meme Bathiketollam"/>
</div>
</pre>
