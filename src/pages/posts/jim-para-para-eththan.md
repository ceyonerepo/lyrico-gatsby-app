---
title: "jim para para song lyrics"
album: "Eththan"
artist: "Taj Noor"
lyricist: "S.Kalaimani"
director: "L Suresh"
path: "/albums/eththan-lyrics"
song: "Jim Para Para"
image: ../../images/albumart/eththan.jpg
date: 2011-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-kL_zqGfIxE"
type: "happy"
singers:
  - Taj Noor
  - Manthangi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaachi sandhaiyila naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaachi sandhaiyila naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa viyaabaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa viyaabaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">sivagaasi aalaiyiley naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivagaasi aalaiyiley naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa vivagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa vivagaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">poadupoadu poadupoadu poadupoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poadupoadu poadupoadu poadupoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaachi sandhaiyila naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaachi sandhaiyila naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa viyaabaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa viyaabaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">sivagaasi aalaiyiley naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivagaasi aalaiyiley naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa vivagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa vivagaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teen age-i Boy-sukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teen age-i Boy-sukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Teey age-i Girls-sukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teey age-i Girls-sukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dring Girl naanadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dring Girl naanadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">naanadaa naanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanadaa naanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taapten listtiniley mudhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taapten listtiniley mudhal "/>
</div>
<div class="lyrico-lyrics-wrapper">idandhaan enakkuthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idandhaan enakkuthaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkuthaaney enakkuthaaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkuthaaney enakkuthaaney "/>
</div>
<div class="lyrico-lyrics-wrapper">enakkuthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkuthaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shair market shairellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shair market shairellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaaley dowu-naagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaaley dowu-naagum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaachi ennaaley ennaaley ennaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaachi ennaaley ennaaley ennaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">raathiri sooper menan senjupputtaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raathiri sooper menan senjupputtaan"/>
</div>
<div class="lyrico-lyrics-wrapper">sinjak manam neethaaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinjak manam neethaaney "/>
</div>
<div class="lyrico-lyrics-wrapper">neethaaney neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethaaney neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lakkalakka lakka lai lai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakkalakka lakka lai lai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakkalakka lakka lai lai lai lai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakkalakka lakka lai lai lai lai"/>
</div>
<div class="lyrico-lyrics-wrapper">uiyaam uiyaam uiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uiyaam uiyaam uiyaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaviyum ada kaaviyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaviyum ada kaaviyum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaamathil ondradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamathil ondradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ondradaa ondradaa ondradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondradaa ondradaa ondradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei aariyam ada thraavidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei aariyam ada thraavidam "/>
</div>
<div class="lyrico-lyrics-wrapper">kaamathil ondradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamathil ondradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kaamathil meloarum keezhoarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kaamathil meloarum keezhoarum"/>
</div>
<div class="lyrico-lyrics-wrapper">elloarum ondradaa ondradaa ondradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elloarum ondradaa ondradaa ondradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara Jimbarabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara Jimbarabara "/>
</div>
<div class="lyrico-lyrics-wrapper">Jimbarabara jimbarabee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimbarabara jimbarabee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaachi sandhaiyila naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaachi sandhaiyila naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa viyaabaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa viyaabaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">sivagaasi aalaiyiley naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivagaasi aalaiyiley naan "/>
</div>
<div class="lyrico-lyrics-wrapper">sirichaa vivagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichaa vivagaaram"/>
</div>
</pre>
