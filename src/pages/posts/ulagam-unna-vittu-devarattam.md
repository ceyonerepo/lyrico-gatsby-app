---
title: "ulagam unna vittu song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Magizh Thirumeni"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Ulagam Unna Vittu"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F3Qh5HxXWWY"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Velukkadha Adi Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velukkadha Adi Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karukkadha Venmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukkadha Venmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porakkadha Ethirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porakkadha Ethirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaaga Kai Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaaga Kai Serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Chellame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Chellame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Anbu Vittu Suththaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Anbu Vittu Suththaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravum Unnai Vittu Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravum Unnai Vittu Pogaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Anbu Vittu Suththaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Anbu Vittu Suththaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravum Unnai Vittu Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravum Unnai Vittu Pogaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Manna Vittu Vaththaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Manna Vittu Vaththaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Pol Sondham Inga Makkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pol Sondham Inga Makkaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham Illaama Nizhalum Neelaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham Illaama Nizhalum Neelaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravum Illaama Ethuvum Vaazhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravum Illaama Ethuvum Vaazhathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhalum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivum Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etta Erukkuthu Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta Erukkuthu Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Irukkuthu Aathaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Irukkuthu Aathaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Muzhuvathum Thee Kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Muzhuvathum Thee Kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraatho…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatho…ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Kanavula Poo Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanavula Poo Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Ethirula Porkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Ethirula Porkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda Valigalum Yeraalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda Valigalum Yeraalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Aval Aarambikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Aval Aarambikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai Avan Aatharikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai Avan Aatharikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththugira Boomiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththugira Boomiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamum Undaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamum Undaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbam Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam Endraal Sernthanaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Endraal Sernthanaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Athu Thedi Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Athu Thedi Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivathu Anbaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivathu Anbaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathu Pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathu Pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanathu Aagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathu Aagattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal Maaridum Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Maaridum Thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Sondhagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Sondhagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Bandhagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Bandhagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum Thaanguthu Manmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum Thaanguthu Manmela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugama Sumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugama Sumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya Varama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya Varama"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaa Puthira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaa Puthira"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuve Urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuve Urava"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Chellame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thangame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Chellame"/>
</div>
</pre>
