---
title: "na manasuki song lyrics"
album: "Appudu Ippudu"
artist: "Padmanav Bharadwaj"
lyricist: "Chirravuri Vijaykumar"
director: "Chalapathi Puvvala"
path: "/albums/appudu-ippudu-lyrics"
song: "Na Manasuki"
image: ../../images/albumart/appudu-ippudu.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wbAb27jBsKQ"
type: "love"
singers:
  - Usha
  - Sai Charan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">na manasuku thalanoppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na manasuku thalanoppi"/>
</div>
<div class="lyrico-lyrics-wrapper">na manasuku thalanoppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na manasuku thalanoppi"/>
</div>
<div class="lyrico-lyrics-wrapper">thaggalante chude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaggalante chude"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu naa vaipu thala thippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu naa vaipu thala thippi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paitaku mathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paitaku mathi thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paitaku mathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paitaku mathi thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">chusthe chalu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusthe chalu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">thega jaruddi gathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thega jaruddi gathi thappi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnala chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnala chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kudaradhe kudhurey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudaradhe kudhurey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvila dosthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvila dosthi"/>
</div>
<div class="lyrico-lyrics-wrapper">chilipigaa chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chilipigaa chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mana e chetime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana e chetime"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukai nadulai madine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukai nadulai madine"/>
</div>
<div class="lyrico-lyrics-wrapper">kudipi kudipi kudipi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudipi kudipi kudipi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee bommale nindale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bommale nindale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula album lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula album lo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee navvule gundela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee navvule gundela "/>
</div>
<div class="lyrico-lyrics-wrapper">allaram lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allaram lo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee andhele vinapade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee andhele vinapade"/>
</div>
<div class="lyrico-lyrics-wrapper">hear phonullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hear phonullo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee anthame ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee anthame ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">instagram lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="instagram lo"/>
</div>
<div class="lyrico-lyrics-wrapper">akkada kelithe akkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkada kelithe akkada"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve takkuna kanipisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve takkuna kanipisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">niddhura poyi nimusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niddhura poyi nimusham"/>
</div>
<div class="lyrico-lyrics-wrapper">avadhu kalavi kavvisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avadhu kalavi kavvisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadu erugani kinnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadu erugani kinnera"/>
</div>
<div class="lyrico-lyrics-wrapper">vannela tannula anandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannela tannula anandham"/>
</div>
<div class="lyrico-lyrics-wrapper">suddenga kalige feelinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suddenga kalige feelinge"/>
</div>
<div class="lyrico-lyrics-wrapper">kshanam lo modalai scrollinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kshanam lo modalai scrollinge"/>
</div>
<div class="lyrico-lyrics-wrapper">ne marichina maravadhu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne marichina maravadhu manase"/>
</div>
<div class="lyrico-lyrics-wrapper">ne sogasuki marigina vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sogasuki marigina vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">gantakokka mentalekki BP repi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gantakokka mentalekki BP repi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">am kikkule nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="am kikkule nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa venuke kurchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa venuke kurchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">ne byike pai eddaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne byike pai eddaram"/>
</div>
<div class="lyrico-lyrics-wrapper">welthu unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="welthu unte"/>
</div>
<div class="lyrico-lyrics-wrapper">time teliyadhe jantaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time teliyadhe jantaga"/>
</div>
<div class="lyrico-lyrics-wrapper">movie chusthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="movie chusthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">am thochade ontarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="am thochade ontarai"/>
</div>
<div class="lyrico-lyrics-wrapper">move avuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="move avuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaka nidure samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaka nidure samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">okatai message esthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okatai message esthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">poddhuti varaku saradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poddhuti varaku saradha"/>
</div>
<div class="lyrico-lyrics-wrapper">saradha discuss chesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradha discuss chesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">fresh ga alaa break fast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fresh ga alaa break fast"/>
</div>
<div class="lyrico-lyrics-wrapper">rammani vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rammani vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">garam ga ufff ani udesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garam ga ufff ani udesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aaraam gaa tiffin chesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaraam gaa tiffin chesthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne marichina maravadhu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne marichina maravadhu manase"/>
</div>
<div class="lyrico-lyrics-wrapper">ne sogasuki marigina vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sogasuki marigina vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">gantakokka mentalekki BP repi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gantakokka mentalekki BP repi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">na manasuku thalanoppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na manasuku thalanoppi"/>
</div>
<div class="lyrico-lyrics-wrapper">na manasuku thalanoppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na manasuku thalanoppi"/>
</div>
<div class="lyrico-lyrics-wrapper">thaggalante chude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaggalante chude"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu naa vaipu thala thippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu naa vaipu thala thippi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa paitaku mathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paitaku mathi thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa paitaku mathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paitaku mathi thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">chusthe chalu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusthe chalu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">thega jaruddi gathi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thega jaruddi gathi thappi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninnala chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnala chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kudaradhe kudhurey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudaradhe kudhurey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvila dosthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvila dosthi"/>
</div>
<div class="lyrico-lyrics-wrapper">chilipigaa chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chilipigaa chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mana e chetime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana e chetime"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukai nadulai madine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukai nadulai madine"/>
</div>
<div class="lyrico-lyrics-wrapper">kudipi kudipi kudipi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudipi kudipi kudipi"/>
</div>
</pre>
