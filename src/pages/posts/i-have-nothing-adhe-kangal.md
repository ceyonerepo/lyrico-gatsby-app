---
title: "i have nothing song lyrics"
album: "Adhe Kangal"
artist: "Ghibran"
lyricist: "Addie Nicole"
director: "Rohin Venkatesan"
path: "/albums/adhe-kangal-lyrics"
song: "I Have Nothing"
image: ../../images/albumart/adhe-kangal.jpg
date: 2017-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RIO2EQZlHl8"
type: "love"
singers:
  - Addie Nicole
  - Jah Mil
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Look at what you have done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look at what you have done"/>
</div>
<div class="lyrico-lyrics-wrapper">Clouded the morning sun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clouded the morning sun"/>
</div>
<div class="lyrico-lyrics-wrapper">I would have shined for you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I would have shined for you"/>
</div>
<div class="lyrico-lyrics-wrapper">But now that i have learned the truth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But now that i have learned the truth"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wont let you break me down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wont let you break me down"/>
</div>
<div class="lyrico-lyrics-wrapper">I will take what i still have and get out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I will take what i still have and get out"/>
</div>
<div class="lyrico-lyrics-wrapper">When you’re falling deep asleep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When you’re falling deep asleep"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll be there haunting in your dreams
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll be there haunting in your dreams"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You should know when you left me alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You should know when you left me alone"/>
</div>
<div class="lyrico-lyrics-wrapper">The silence of sound
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The silence of sound"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">I have nothing left to say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I have nothing left to say"/>
</div>
<div class="lyrico-lyrics-wrapper">You have taken it all away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You have taken it all away"/>
</div>
<div class="lyrico-lyrics-wrapper">For all the scars and all the pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For all the scars and all the pain"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m never coming back now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m never coming back now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">We have nothing left to do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We have nothing left to do"/>
</div>
<div class="lyrico-lyrics-wrapper">I gave all of my heart to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I gave all of my heart to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Perhaps i fell in love too soon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perhaps i fell in love too soon"/>
</div>
<div class="lyrico-lyrics-wrapper">Or maybe you brought us down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or maybe you brought us down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tears have fallen like the rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tears have fallen like the rain"/>
</div>
<div class="lyrico-lyrics-wrapper">And lighting struck my soul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And lighting struck my soul"/>
</div>
<div class="lyrico-lyrics-wrapper">But i have found my strength again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But i have found my strength again"/>
</div>
<div class="lyrico-lyrics-wrapper">And i wont take it anymore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And i wont take it anymore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyday i am reminded of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyday i am reminded of"/>
</div>
<div class="lyrico-lyrics-wrapper">What we could have been
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What we could have been"/>
</div>
<div class="lyrico-lyrics-wrapper">Every moment comes back to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every moment comes back to me"/>
</div>
<div class="lyrico-lyrics-wrapper">Each time i let you in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Each time i let you in"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">There is a fire growing inside me and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There is a fire growing inside me and"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s headed your way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s headed your way"/>
</div>
<div class="lyrico-lyrics-wrapper">All of this madness could have been
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All of this madness could have been"/>
</div>
<div class="lyrico-lyrics-wrapper">Ended if you had just stayed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ended if you had just stayed"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">I have nothing left to say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I have nothing left to say"/>
</div>
<div class="lyrico-lyrics-wrapper">You have taken it all away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You have taken it all away"/>
</div>
<div class="lyrico-lyrics-wrapper">For all the scars and all the pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For all the scars and all the pain"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m never coming back now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m never coming back now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">We have nothing left to do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We have nothing left to do"/>
</div>
<div class="lyrico-lyrics-wrapper">I gave all of my heart to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I gave all of my heart to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Perhaps i fell in love too soon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perhaps i fell in love too soon"/>
</div>
<div class="lyrico-lyrics-wrapper">Or maybe you brought us down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or maybe you brought us down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">I have nothing left to say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I have nothing left to say"/>
</div>
<div class="lyrico-lyrics-wrapper">You have taken it all away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You have taken it all away"/>
</div>
<div class="lyrico-lyrics-wrapper">For all the scars and all the pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For all the scars and all the pain"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m never coming back now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m never coming back now"/>
</div>
</pre>
