---
title: "ichu ichu song lyrics"
album: "Vedi"
artist: "Vijay Antony"
lyricist: "Vaali"
director: "Prabhu Deva"
path: "/albums/vedi-lyrics"
song: "Ichu Ichu"
image: ../../images/albumart/vedi.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bE0fhtiE65g"
type: "love"
singers:
  - Vijay Antony
  - Sangeetha Rajeshwaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A    Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A    Right-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achu Achu Achu Achu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achu Achu Achu Achu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachu Thachu Thachu Thachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachu Thachu Thachu Thachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichu Pichu Pichu Pichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichu Pichu Pichu Pichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A   Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A   Right-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottaagi Poovaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottaagi Poovaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaagi Kaayaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaagi Kaayaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayaagi Kaniyaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayaagi Kaniyaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaagi Thaniyaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaagi Thaniyaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnenae Naanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnenae Naanthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae Muthaagira Pennuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Muthaagira Pennuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnukum Pinnukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnukum Pinnukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannangal Punnaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangal Punnaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal Undaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Undaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhenae Vandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhenae Vandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A    Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A    Right-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadaga Vaadaga Ennadi Vaadaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaga Vaadaga Ennadi Vaadaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mana Veetukul Okaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mana Veetukul Okaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae Ketaalum Indhaanu Thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae Ketaalum Indhaanu Thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ottipen Atta Pol Un Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ottipen Atta Pol Un Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menaga Menaga Vaanathu Menaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menaga Menaga Vaanathu Menaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetathaa Vituthaan Vandhaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetathaa Vituthaan Vandhaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhiran Sandhiran Rendumae Needhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhiran Sandhiran Rendumae Needhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannathaan Un Kaiyil Thanthaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannathaan Un Kaiyil Thanthaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Polae Illa Unna Enni Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Polae Illa Unna Enni Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachirupen Romba Bathirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachirupen Romba Bathirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Siripula Ennai Valaichuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Siripula Ennai Valaichuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitenna Vaasalil Chithirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitenna Vaasalil Chithirama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A    Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A    Right-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasana Vaasana Malliga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasana Vaasana Malliga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasana Gapunu Gupunu Mabbetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasana Gapunu Gupunu Mabbetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athikum Ithikum Ethikum Pathikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athikum Ithikum Ethikum Pathikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothikum Thee Onnu Soodetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothikum Thee Onnu Soodetha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poosana Poosana Manmadha Poosana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosana Poosana Manmadha Poosana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Paadhi Mei Paadhi Seiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Paadhi Mei Paadhi Seiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mai Vaitha Kannukul Poi Vaitha Pennukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Vaitha Kannukul Poi Vaitha Pennukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyaaga Thaen Mazhai Peiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaaga Thaen Mazhai Peiyaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokku Thalaiyila Venna Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokku Thalaiyila Venna Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Bakunnu Kai Patha Paakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Bakunnu Kai Patha Paakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athi Karukaiyil Adha Idha Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athi Karukaiyil Adha Idha Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhi Virichida Kekuriyea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhi Virichida Kekuriyea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A    Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A    Right-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthaagira Pennuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaagira Pennuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnukum Pinnukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnukum Pinnukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannangal Punnaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangal Punnaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal Undaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Undaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhenae Vandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhenae Vandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottaagi Poovaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottaagi Poovaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaagi Kaayaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaagi Kaayaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayaagi Kaniyaagira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayaagi Kaniyaagira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyaagi Thaniyaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaniyaagi Thaniyaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnenae Naanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnenae Naanthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichu Ichu Ichu Ichu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichu Ichu Ichu Ichu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu Nachu Nachu Nachu Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu Nachu Nachu Nachu Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-A    Right-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-A    Right-U"/>
</div>
</pre>
