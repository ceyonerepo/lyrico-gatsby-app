---
title: "ilayathalapathy song lyrics"
album: "Moondru Rasikarkal"
artist: "Ronnie Raphael"
lyricist: "Charu Hariharan"
director: "Shebi"
path: "/albums/moondru-rasikarkal-lyrics"
song: "Ilayathalapathy"
image: ../../images/albumart/moondru-rasikarkal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9UyEfqv8PwU"
type: "happy"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sa pa sa mama ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa sa mama ga"/>
</div>
<div class="lyrico-lyrics-wrapper">pama ga ri magasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pama ga ri magasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa sa mama ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa sa mama ga"/>
</div>
<div class="lyrico-lyrics-wrapper">pama ga ri magasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pama ga ri magasa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey gilli gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey gilli gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">nee puli puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee puli puli"/>
</div>
<div class="lyrico-lyrics-wrapper">kushi kushi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kushi kushi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagiya tamil magan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagiya tamil magan"/>
</div>
<div class="lyrico-lyrics-wrapper">tirumalai nee bhagavathy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tirumalai nee bhagavathy"/>
</div>
<div class="lyrico-lyrics-wrapper">nee saravedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee saravedi "/>
</div>
<div class="lyrico-lyrics-wrapper">engal azhakiya pokiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal azhakiya pokiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engal thalaiva thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal thalaiva thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum neye nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum neye nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">engal thullum manathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal thullum manathin"/>
</div>
<div class="lyrico-lyrics-wrapper">uthayam nee yada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthayam nee yada"/>
</div>
<div class="lyrico-lyrics-wrapper">adhi thamila thamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhi thamila thamila"/>
</div>
<div class="lyrico-lyrics-wrapper">manathai aalum selva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathai aalum selva"/>
</div>
<div class="lyrico-lyrics-wrapper">priyamudan unai adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="priyamudan unai adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">rasigan nanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasigan nanada"/>
</div>
<div class="lyrico-lyrics-wrapper">villenthum raamane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villenthum raamane"/>
</div>
<div class="lyrico-lyrics-wrapper">madhurai eithan devane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurai eithan devane"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan vazhi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan vazhi than"/>
</div>
<div class="lyrico-lyrics-wrapper">engal puthiya geethaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal puthiya geethaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thirupaachi veerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirupaachi veerane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sivakashi soorane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sivakashi soorane"/>
</div>
<div class="lyrico-lyrics-wrapper">manathai kollai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathai kollai kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai kaarane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai kaarane"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiva thalaiva thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiva thalaiva thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye engal ilaya thalapathy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye engal ilaya thalapathy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unthan nadipe nadipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan nadipe nadipe"/>
</div>
<div class="lyrico-lyrics-wrapper">uythin ithaya thudipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uythin ithaya thudipe"/>
</div>
<div class="lyrico-lyrics-wrapper">nerukku nerai unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerukku nerai unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">once more pakkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="once more pakkava"/>
</div>
<div class="lyrico-lyrics-wrapper">seerum puliyum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seerum puliyum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">parakkum kuruvi neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakkum kuruvi neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">tamizhin pugalai enthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamizhin pugalai enthum"/>
</div>
<div class="lyrico-lyrics-wrapper">sachin neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachin neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">minsaara kannane nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minsaara kannane nee"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthale pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthale pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">ullukul malargal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullukul malargal "/>
</div>
<div class="lyrico-lyrics-wrapper">pookum mayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookum mayame"/>
</div>
<div class="lyrico-lyrics-wrapper">endrendum kaathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrendum kaathala"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukul nilavu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukul nilavu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">marangalku endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marangalku endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalan neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalan neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">thozha thozha thozah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozha thozha thozah"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye engal ilayathalapathy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye engal ilayathalapathy"/>
</div>
</pre>
