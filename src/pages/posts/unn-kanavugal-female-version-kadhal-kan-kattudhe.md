---
title: "unn kanavugal Female version song lyrics"
album: "Kadhal Kan Kattudhe"
artist: "Pavan"
lyricist: "Mohanraja"
director: "Shivaraj"
path: "/albums/kadhal-kan-kattudhe-lyrics"
song: "Unn Kanavugal Female Version"
image: ../../images/albumart/kadhal-kan-kattudhe.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S7edAa0QrsQ"
type: "love"
singers:
  - Madhumitha
  - Ajitha 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un ninaivugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanavugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un thedalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thedalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedi kondae nadanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedi kondae nadanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraavadhu un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraavadhu un"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil serum muzhuthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil serum muzhuthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee odi kondae irundhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee odi kondae irundhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraavadhu adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraavadhu adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri perum perithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri perum perithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ninaivugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanavugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aangal endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangal endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila kelvi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila kelvi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kelvi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kelvi varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum bodhai tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum bodhai tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru geethai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru geethai aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellum varai oyaamal odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellum varai oyaamal odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhikondu munneru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhikondu munneru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettum varai thoongaamal thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettum varai thoongaamal thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum vendum unnodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum vendum unnodu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatin kangal pengal enbaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatin kangal pengal enbaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal endru thoonga seivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal endru thoonga seivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal veetin thangam enbaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal veetin thangam enbaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam endru pootti vaipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam endru pootti vaipaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellum varai oyaamal odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellum varai oyaamal odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhikondu munneru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhikondu munneru nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettum varai thoongaamal thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettum varai thoongaamal thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum vendum unnodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum vendum unnodu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ninaivugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavugal yedhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanavugal yedhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan un idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan un idhayam"/>
</div>
</pre>
