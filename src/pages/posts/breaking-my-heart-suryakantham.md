---
title: "breaking my heart song lyrics"
album: "Suryakantham"
artist: "Mark K Robin"
lyricist: "Krishna Kanth"
director: "Pranith Bramandapally"
path: "/albums/suryakantham-lyrics"
song: "Breaking My Heart"
image: ../../images/albumart/suryakantham.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_ZnwAcvxMng"
type: "sad"
singers:
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hrudhayamlo O Sagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayamlo O Sagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhilesi Poyanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhilesi Poyanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Migilunna Ee Sagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migilunna Ee Sagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Asale Vinane Vinadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asale Vinane Vinadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo Ee Nijame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo Ee Nijame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudaina Nammakane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudaina Nammakane"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichena Puvvunidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichena Puvvunidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanane Vidane Vidadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanane Vidane Vidadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yedabatu Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yedabatu Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavatuga Chesedhela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavatuga Chesedhela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Yedha Vedhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Yedha Vedhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Dhaatuthu Poyedhela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Dhaatuthu Poyedhela"/>
</div>
<div class="lyrico-lyrics-wrapper">Natho Nene Lene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natho Nene Lene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanathonayina Lene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathonayina Lene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Aage Telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Aage Telise"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayalevi Leve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayalevi Leve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalemo Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalemo Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhe Lolo Yegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhe Lolo Yegase"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontare Ika Neevane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontare Ika Neevane"/>
</div>
<div class="lyrico-lyrics-wrapper">Needane Telipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needane Telipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantilo Irukaindhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantilo Irukaindhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerila Kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerila Kurise"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Puttanaa Nenemaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Puttanaa Nenemaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Leka Nene Undanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Leka Nene Undanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kali Chese Baruve Brathakanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali Chese Baruve Brathakanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Theesi Nalonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Theesi Nalonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s Breaking My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s Breaking My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Heart Sweet Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Heart Sweet Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">My Heart My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Heart My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Stole You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Stole You"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Heart Sweet Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Heart Sweet Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">My Heart My Heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Heart My Heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Stole You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Stole You"/>
</div>
</pre>
