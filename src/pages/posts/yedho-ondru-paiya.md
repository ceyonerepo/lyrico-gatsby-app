---
title: 'yedho ondru song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'Yedho Ondru'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
lang: tamil
singers: 
- Yuvan Shankar Raja
youtubeLink: "https://www.youtube.com/embed/UeYP7909Ovg"
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Yedho ondru ennai thaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedho ondru ennai thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro pola unnai paarkka
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro pola unnai paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutri engum naadagam nadakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutri engum naadagam nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae naanum eppadi nadikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae naanum eppadi nadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam muzhuthum vaazhum kanavai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam muzhuthum vaazhum kanavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vaithu thoonginen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil vaithu thoonginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai vidinthu pogum nilavai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalai vidinthu pogum nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil pidikka yenginen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil pidikka yenginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pennae undhan nyaabagathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae undhan nyaabagathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil serthu vaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil serthu vaithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirinthu pogaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pirinthu pogaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai ingu tholaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjai ingu tholaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennai unnidam vittu chelgiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai unnidam vittu chelgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illaiyae ennidathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illaiyae ennidathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae povathu yaarai ketpathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Engae povathu yaarai ketpathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa paadhaiyum unnidathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaa paadhaiyum unnidathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen endhan vaazhvil vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen endhan vaazhvil vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En iravaiyum pagalaiyum maatri ponaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En iravaiyum pagalaiyum maatri ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indha pirivai thanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen indha pirivai thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayathil thanimaiyai ootri ponaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En idhayathil thanimaiyai ootri ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ullae un kural ketkuthadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullae un kural ketkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ennnuyir thaakkuthadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai ennnuyir thaakkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae irukkiren engae nadakkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Engae irukkiren engae nadakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthen naan…ooooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranthen naan…ooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pennae undhan nyaabagathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae undhan nyaabagathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil serthu vaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil serthu vaithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirinthu pogaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pirinthu pogaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai ingu tholaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjai ingu tholaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yedho ondru ennai thaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedho ondru ennai thaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro pola unnai paarkka
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro pola unnai paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutri engum naadagam nadakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutri engum naadagam nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae naanum eppadi nadikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae naanum eppadi nadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam muzhuthum vaazhum kanavai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam muzhuthum vaazhum kanavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vaithu thoonginen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil vaithu thoonginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai vidinthu pogum nilavai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalai vidinthu pogum nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil pidikka yenginen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil pidikka yenginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pennae undhan nyaabagathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae undhan nyaabagathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil serthu vaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil serthu vaithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirinthu pogaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pirinthu pogaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai ingu tholaithenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjai ingu tholaithenae"/>
</div>
</pre>