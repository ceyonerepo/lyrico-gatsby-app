---
title: "yaarumillaa song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Vivek"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Yaarumillaa"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TkRXBEq58zs"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarumillaa Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillaa Vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Irukka Yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irukka Yenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Varai Kaadhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Varai Kaadhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Madiyil Thoonginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madiyil Thoonginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pirinthu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pirinthu Pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyiril Odaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyiril Odaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enji Vitta Dhoosilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enji Vitta Dhoosilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ennai Korkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ennai Korkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Paarvaigal Sumakkamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Paarvaigal Sumakkamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Naalaiyum Varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Naalaiyum Varuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi Nerathil Uyar Vanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Nerathil Uyar Vanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Saavenum Varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Saavenum Varamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Tholaitha Aazhathil Naan Oligiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tholaitha Aazhathil Naan Oligiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaathai Kaattilae Naan Karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaathai Kaattilae Naan Karaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneerai Kappaattri Unakaaga Serkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai Kappaattri Unakaaga Serkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaagamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Illaadha Meenum Thanneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Illaadha Meenum Thanneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Nyayathai Yerkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Nyayathai Yerkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Selladha Theevin Maiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Selladha Theevin Maiyathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli Poovaaga Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Poovaaga Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eesal Rekkai Mel Eeyin Padhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesal Rekkai Mel Eeyin Padhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram Eppadi Thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Eppadi Thaanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Illaadha Keeral Kolladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Illaadha Keeral Kolladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Engae Naan Vaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Engae Naan Vaanguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaneerai Kappaattri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneerai Kappaattri"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga Serkkiren Thadaagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga Serkkiren Thadaagamae"/>
</div>
</pre>
