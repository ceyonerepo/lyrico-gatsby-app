---
title: "oru naal vidu murai song lyrics"
album: "Doo"
artist: "Abhishek — Lawrence"
lyricist: "G. Kumar"
director: "Sriram Padmanabhan"
path: "/albums/doo-lyrics"
song: "Oru Naal Vidu Murai"
image: ../../images/albumart/doo.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gNz7j14RV44"
type: "love"
singers:
  - Haricharan
  - Prashanthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru naal vidumurai neeyeduthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal vidumurai neeyeduthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal mattum juramadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal mattum juramadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">marunaal unai paarppadharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunaal unai paarppadharku"/>
</div>
<div class="lyrico-lyrics-wrapper">un theruvil kaalga adampidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un theruvil kaalga adampidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">varugaippadhivu illaiyendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varugaippadhivu illaiyendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhu kidandhu pada padakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhu kidandhu pada padakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un perai azhakkayiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un perai azhakkayiley"/>
</div>
<div class="lyrico-lyrics-wrapper">yei uyirum vandhu angu kural kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei uyirum vandhu angu kural kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul vandhu eththanai naalaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul vandhu eththanai naalaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">nenappula thaaney paadudhu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenappula thaaney paadudhu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna suththi kaadhal geraganam vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna suththi kaadhal geraganam vandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul vandhu eththanai naalaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul vandhu eththanai naalaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">nenappula thaaney vaazhudhu en moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenappula thaaney vaazhudhu en moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna suththi kaadhal geraganam vandhaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna suththi kaadhal geraganam vandhaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaguppukkul mun irukkaiyiley amarndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaguppukkul mun irukkaiyiley amarndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanathil parakkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanathil parakkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">karumbalagaiyiley ezhuthukkalellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumbalagaiyiley ezhuthukkalellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">un peyarthaan ninaikkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un peyarthaan ninaikkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">oru murai yetrum kodiyiniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai yetrum kodiyiniley"/>
</div>
<div class="lyrico-lyrics-wrapper">naan dhinam dhinam un mugam paarkkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan dhinam dhinam un mugam paarkkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">maalaiyil adikkum palli mani Osaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalaiyil adikkum palli mani Osaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum verukkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum verukkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">enadhu puththagathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enadhu puththagathil "/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal pakkam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal pakkam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudan iruppadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudan iruppadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">maruppakkam naanaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruppakkam naanaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">veettukku varuvadharku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettukku varuvadharku "/>
</div>
<div class="lyrico-lyrics-wrapper">en theruvum varam kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en theruvum varam kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">thaazhppaalum poadaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaazhppaalum poadaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">veettukadhavum thirandhirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettukadhavum thirandhirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vali enna puriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vali enna puriyalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">enadhu kaadhal oru pizhayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enadhu kaadhal oru pizhayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">purindhukol en manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purindhukol en manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai innum sila dhinamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai innum sila dhinamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu mugavariyil en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu mugavariyil en "/>
</div>
<div class="lyrico-lyrics-wrapper">peyarai serththuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyarai serththuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum idamaara naal ondrai paarthuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum idamaara naal ondrai paarthuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai marappadharkku ennaalum mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai marappadharkku ennaalum mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">viral pidithu nadandhuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral pidithu nadandhuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">ini pirivum inikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini pirivum inikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkul vandhu eththanai naalaacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkul vandhu eththanai naalaacho"/>
</div>
<div class="lyrico-lyrics-wrapper">nenappiley thaaney vaazhuren en koochey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenappiley thaaney vaazhuren en koochey"/>
</div>
<div class="lyrico-lyrics-wrapper">nammai sutrum kaadhal keragam vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai sutrum kaadhal keragam vandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkul vandhu eththanai naalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkul vandhu eththanai naalaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenappula thaaney vaazhudhey en moochey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenappula thaaney vaazhudhey en moochey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammai suththi kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai suththi kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">geraganam vandhaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="geraganam vandhaachey"/>
</div>
<div class="lyrico-lyrics-wrapper">hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei hei"/>
</div>
</pre>
