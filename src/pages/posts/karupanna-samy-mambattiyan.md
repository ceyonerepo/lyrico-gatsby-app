---
title: "karupanna samy song lyrics"
album: "Mambattiyan"
artist: "S Thaman"
lyricist: "Annamalai"
director: "Thiagarajan"
path: "/albums/mambattiyan-lyrics"
song: "Karupanna Samy"
image: ../../images/albumart/mambattiyan.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n8RqmY_vsWs"
type: "devotional"
singers:
  - Ranjith
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maavilakku Yethivachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavilakku Yethivachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Pacharisi Pongavachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pacharisi Pongavachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaiyala Thaan Pottu Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyala Thaan Pottu Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ellarukkum Pangu Veiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ellarukkum Pangu Veiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhisanam Othumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhisanam Othumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikkithaan Nendhukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikkithaan Nendhukkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyiradhu Athunaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyiradhu Athunaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettriyaaga Mudinjirumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettriyaaga Mudinjirumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Vettriyaaga Mudinjirumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vettriyaaga Mudinjirumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Karuppanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karuppanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppanna Saamithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppanna Saamithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Adhirudhu Adhirudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Adhirudhu Adhirudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Nenaichadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Nenaichadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkura Thedhidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura Thedhidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Ellaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Ellaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nerandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nerandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nallaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nallaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kaalamdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kaalamdhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muradaa Ilangudhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muradaa Ilangudhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigiradhae Adakkidavaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigiradhae Adakkidavaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Kadivaalam Thookkidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kadivaalam Thookkidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangida Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangida Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharigidathoam Tharigidathoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigidathoam Tharigidathoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigidathoam Tharigidathoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigidathoam Tharigidathoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Adidhadi Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Adidhadi Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Padapada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Padapada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppannan Karuppannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppannan Karuppannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Varaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhellaam Nammalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhellaam Nammalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyavaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyavaraandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumalai Ezhu Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumalai Ezhu Kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Varaandaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Varaandaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaippaazhai Makkalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaippaazhai Makkalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkavaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkavaraandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthunnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthunnuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Bhramaanum Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Bhramaanum Azhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Vachchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Vachchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottigoburam Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottigoburam Adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottukkaaran Varavaa Aaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottukkaaran Varavaa Aaththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaantharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaantharai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Nenappaai Kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nenappaai Kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Edukkanum Sodakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Edukkanum Sodakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanalaa Mogam Thiruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanalaa Mogam Thiruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhandhidum Padakkena Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhandhidum Padakkena Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badavaa Adakkirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badavaa Adakkirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichirukkae Enai Thoda Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichirukkae Enai Thoda Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathula Nee Irukkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula Nee Irukkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppukkundhaan Kuliredukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppukkundhaan Kuliredukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kodi Idai Odaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kodi Idai Odaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaiveeran Nenappudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiveeran Nenappudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppannan Karuppannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppannan Karuppannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Varaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhellaam Nammalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhellaam Nammalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyavaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyavaraandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumalai Ezhu Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumalai Ezhu Kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Varaandaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Varaandaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaippaazhai Makkalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaippaazhai Makkalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkavaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkavaraandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo Ooo Ooo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo Ooo Ooo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo Ooo Ooo Oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo Ooo Ooo Oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Kadivaalam Thookkidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kadivaalam Thookkidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangida Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangida Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharigidathoam Tharigidathoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigidathoam Tharigidathoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigidathoam Tharigidathoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigidathoam Tharigidathoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Adidhadi Nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Adidhadi Nadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Padapada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Padapada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppannan Karuppannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppannan Karuppannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Varaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhellaam Nammalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhellaam Nammalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyavaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyavaraandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumalai Ezhu Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumalai Ezhu Kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Varaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Varaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaippaazhai Makkalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaippaazhai Makkalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkavaraandaa Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkavaraandaa Aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ye Ye Ye Ye Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ye Ye Ye Ye Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaaa"/>
</div>
</pre>
