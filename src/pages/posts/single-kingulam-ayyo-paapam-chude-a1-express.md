---
title: "single kingulam song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Samrat"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "Single Kingulam - Ayyo Paapam Chude"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rwo4cUg8nPY"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ayyo paapam chude papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo paapam chude papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne sommem podde tuna chepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne sommem podde tuna chepa"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbayilante plastic cup aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbayilante plastic cup aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa hero kanna nuvvem goppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa hero kanna nuvvem goppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thella thellagunna Taj Mahal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thella thellagunna Taj Mahal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulesi rachha lepe gabbar singhulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulesi rachha lepe gabbar singhulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meme single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meme single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru mingle aithe swingulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru mingle aithe swingulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ringu petti gunde doche A1 dongalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringu petti gunde doche A1 dongalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo papam choode papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo papam choode papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne sommem podde tuna chepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne sommem podde tuna chepa"/>
</div>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbayilante plastic cup aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbayilante plastic cup aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa hero kanna nuvvem goppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa hero kanna nuvvem goppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana colour kastha ekkuvemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana colour kastha ekkuvemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Parledhu bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parledhu bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">Skinnu kandhakunda chusukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Skinnu kandhakunda chusukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekenti lossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekenti lossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala pogaru kuda masthugundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala pogaru kuda masthugundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhe ga massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhe ga massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa chevulalona pettaku bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa chevulalona pettaku bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Cauliflowersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cauliflowersu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanu pakkana unte enda kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu pakkana unte enda kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuthadhi manchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuthadhi manchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aallayya soodu ettunado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aallayya soodu ettunado"/>
</div>
<div class="lyrico-lyrics-wrapper">Soundu thagginchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu thagginchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana peru meeda rasestha RK beachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana peru meeda rasestha RK beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizagollu thantharemo aapeyi speechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizagollu thantharemo aapeyi speechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sotta buggala lavanya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sotta buggala lavanya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu love chesthane laavu ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu love chesthane laavu ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthane edhemaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthane edhemaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharlo traffic jam aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharlo traffic jam aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee puvvunautha jallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee puvvunautha jallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarath undi chhalo na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarath undi chhalo na"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde neeku pillow na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde neeku pillow na"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv kallokosthe thillana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv kallokosthe thillana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoy single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thella thellagunna Taj Mahal ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thella thellagunna Taj Mahal ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulesi rachha lepe gabbar singhulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulesi rachha lepe gabbar singhulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meme single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meme single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru mingle aithe swingulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru mingle aithe swingulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ringu petti gunde doche A1 dongalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringu petti gunde doche A1 dongalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo papam choode papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo papam choode papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Single kingulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single kingulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne sommem podde tuna chepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne sommem podde tuna chepa"/>
</div>
</pre>
