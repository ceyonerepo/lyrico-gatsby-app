---
title: "bombay ponnu song lyrics"
album: "Vedi"
artist: "Vijay Antony"
lyricist: "Viveka"
director: "Prabhu Deva"
path: "/albums/vedi-lyrics"
song: "Bombay Ponnu"
image: ../../images/albumart/vedi.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cAq1b7Ssssc"
type: "happy"
singers:
  - Mamta Sharma
  - M.L.R. Karthikeyan
  - Senthil Dass
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aek Dho Theen Chaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aek Dho Theen Chaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Bombay Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Bombay Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lucknow Lucknow Lucknow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucknow Lucknow Lucknow"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Lucknow Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Lucknow Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jiginaa Jinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiginaa Jinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pinnaala Yen Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pinnaala Yen Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kai Mela Yen Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kai Mela Yen Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarakkudi Kaanaamoona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarakkudi Kaanaamoona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavundhadhu Yen Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavundhadhu Yen Kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi Aanaaronnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi Aanaaronnaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhuputtaan Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhuputtaan Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haann"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Bombay Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Bombay Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lucknow Lucknow Lucknow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucknow Lucknow Lucknow"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Lucknow Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Lucknow Kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jiginaa Jinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiginaa Jinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithom Thom Thom Tha Thikithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithom Thom Thom Tha Thikithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithom Thom Thom Tha Thikithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithom Thom Thom Tha Thikithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Thithom Thom Thom Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Thithom Thom Thom Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Kithom Thom Thom Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Kithom Thom Thom Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Thom Thom Thom Thathiki Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Thom Thom Thom Thathiki Thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alcohollu Yen Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alcohollu Yen Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattu Paalu Yen Thozhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattu Paalu Yen Thozhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sexy Yaana Sendheluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sexy Yaana Sendheluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sixty Litre Petrollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sixty Litre Petrollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Matthalam Thaan Pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthalam Thaan Pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada Pura Munnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada Pura Munnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Mulacha Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Mulacha Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmayaa Nee Ponnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmayaa Nee Ponnaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajaa Aajaa Aajaa Aajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajaa Aajaa Aajaa Aajaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarakkudi Kaanaamoona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarakkudi Kaanaamoona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavundhadhu Yen Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavundhadhu Yen Kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae Kabutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Kabutharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoothukudi Aanaaronnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi Aanaaronnaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhuputtaan Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhuputtaan Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae Kabutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Kabutharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Bombay Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Bombay Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lucknow Lucknow Lucknow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucknow Lucknow Lucknow"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Lucknow Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Lucknow Kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jiginaa Jinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiginaa Jinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aajaa Aarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aajaa Aarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri Jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri Jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehu Aa Jaa Rae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehu Aa Jaa Rae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Vaangum Moonaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Vaangum Moonaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi Vacha Paalaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi Vacha Paalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nootthuku Naan Iynooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootthuku Naan Iynooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeppidicha Thanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppidicha Thanneeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saamburaani Un Meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamburaani Un Meni"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippu Symphoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippu Symphoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallu Vella Pattaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallu Vella Pattaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesukristhu Thottaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesukristhu Thottaa Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mae Kabhutharu Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Kabhutharu Jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajaa Aajaa Aajaa Aajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajaa Aajaa Aajaa Aajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarakkudi Kaanaamoona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarakkudi Kaanaamoona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavundhadhu Un Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavundhadhu Un Kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae Thu Jaa Rae Jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Thu Jaa Rae Jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerae Thoothukudi Aanaaronnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerae Thoothukudi Aanaaronnaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhuputtaan Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhuputtaan Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaerae Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaerae Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Mela Melaela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Mela Melaela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Bombay Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Bombay Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lucknow Lucknow Lucknow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucknow Lucknow Lucknow"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Lucknow Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Lucknow Kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jiginaa Jinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiginaa Jinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithom Thom Thom Tha Thikithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithom Thom Thom Tha Thikithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithom Thom Thom Tha Thikithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithom Thom Thom Tha Thikithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Thithom Thom Thom Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Thithom Thom Thom Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Kithom Thom Thom Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Kithom Thom Thom Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaki Thom Thom Thom Thathiki Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaki Thom Thom Thom Thathiki Thom"/>
</div>
</pre>
