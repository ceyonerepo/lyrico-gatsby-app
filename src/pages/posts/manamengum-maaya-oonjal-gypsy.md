---
title: "manamengum maaya oonjal song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Yugabharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Manamengum Maaya Oonjal"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NuwkB1UuZQo"
type: "melody"
singers:
  - Dhee
  - Ananthu
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manamengum maaya oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum maaya oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamengum maaya oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum maaya oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathanbil aada aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathanbil aada aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathanbil aada aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathanbil aada aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pongum thooya megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pongum thooya megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pongum thooya megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pongum thooya megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ullae saaral poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ullae saaral poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ullae saaral poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ullae saaral poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodaiyum vaadaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyum vaadaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathidaa thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathidaa thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam nee thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam nee thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan pookuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan pookuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan un tholil kan saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un tholil kan saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Venmeengal pon thoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmeengal pon thoova"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa..aaa…aaa..aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa..aaa…aaa..aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae siragai naam virithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae siragai naam virithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli aagathoo bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli aagathoo bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam kaadhalaal niraithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam kaadhalaal niraithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkeedeathu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkeedeathu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum maaya oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum maaya oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathanbil aada aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathanbil aada aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pongum thooya megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pongum thooya megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ullae saaral poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ullae saaral poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeneer kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeneer kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti kollum eeera kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti kollum eeera kolangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaal thadam naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaal thadam naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaai nilam kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaai nilam kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaaagam dhegam vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaaagam dhegam vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum meengal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum meengal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mann serntha neer pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mann serntha neer pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Un saayal kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un saayal kondenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa…aaa…aaa…aaah…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa…aaa…aaa…aaah…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae siragai naam virithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae siragai naam virithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli aagathoo bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli aagathoo bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam kaadhalaal niraithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam kaadhalaal niraithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkeedeathu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkeedeathu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum maaya oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum maaya oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathanbil aada aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathanbil aada aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pongum thooya megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pongum thooya megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ullae saaral poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ullae saaral poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram oru isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram oru isai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottarumboru isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottarumboru isai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal oru isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal oru isai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namm udal oru isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm udal oru isai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavoli veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavoli veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyin thaaimai yaazhisai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaiyin thaaimai yaazhisai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ven siragai veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven siragai veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochchu kaatrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochchu kaatrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann uruga paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann uruga paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi moongilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi moongilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mozhigalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mozhigalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean valigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean valigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En maarbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maarbilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesum vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesum vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee allavaa…aaaa…aaa….aaah…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee allavaa…aaaa…aaa….aaah…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae siragai naam virithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae siragai naam virithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli aagathoo bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli aagathoo bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam kaadhalaal niraithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam kaadhalaal niraithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharkeedeathu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkeedeathu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa…..aaa….aa….aa…..aa.aaah…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa…..aaa….aa….aa…..aa.aaah…"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum maaya oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum maaya oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathanbil aada aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathanbil aada aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai pongum thooya megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pongum thooya megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ullae saaral poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ullae saaral poda"/>
</div>
</pre>
