---
title: "suno aisha song lyrics"
album: "Aisha"
artist: "Amit Trivedi"
lyricist: "Javed Akhtar"
director: "Rajshree Ojha"
path: "/albums/aisha-lyrics"
song: "Suno Aisha - Thum ho kamaal"
image: ../../images/albumart/aisha.jpg
date: 2010-08-06
lang: hindi
youtubeLink: "https://www.youtube.com/embed/fAzO3s7Dgnw"
type: "happy"
singers:
  - Ash King
  - Nakash Aziz
  - Amit Trivedi
  - Addit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tum ho kamaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum ho kamaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum bemisaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum bemisaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum lajawab ho aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum lajawab ho aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi haseen ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi haseen ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis ko choo lo usko haseen kar do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis ko choo lo usko haseen kar do"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum sochti ho duniya mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum sochti ho duniya mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi bhi kyun kharaab ho aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi bhi kyun kharaab ho aisha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tum chahati ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum chahati ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum koi rang har zindagi mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum koi rang har zindagi mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhar do bhar do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhar do bhar do"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikli ho likhne kismat kisiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikli ho likhne kismat kisiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni hi dhun mein tum aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni hi dhun mein tum aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh shauk kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh shauk kya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh zid hai kaisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh zid hai kaisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna bata do humko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna bata do humko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suljaane mein tum aur bhi uljane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suljaane mein tum aur bhi uljane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoti ho jo ghum aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoti ho jo ghum aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni bhi koi uljan ko door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni bhi koi uljan ko door"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar ke dikha do humko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar ke dikha do humko"/>
</div>
<div class="lyrico-lyrics-wrapper">Suno aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna to tum bhi samjho aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna to tum bhi samjho aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum chahe jitna chaho aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum chahe jitna chaho aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum jitni koshish karo lo aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum jitni koshish karo lo aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum sa na hoga koi suno aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum sa na hoga koi suno aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna to tumbhi samjho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna to tumbhi samjho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum chahe jitna chaho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum chahe jitna chaho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum jitni koshish karlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum jitni koshish karlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum sa na hoga koi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum sa na hoga koi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baton mein ho aa jaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baton mein ho aa jaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jaati ho jazbaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jaati ho jazbaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehli samjho to ke mohabbat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehli samjho to ke mohabbat"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki hai rah kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki hai rah kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal mein ho ke diwaaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal mein ho ke diwaaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Karti ho tum manmaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karti ho tum manmaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Tumko hai duniya vuniya ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tumko hai duniya vuniya ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi parwaah kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi parwaah kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisha jise kehte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisha jise kehte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek hain woh lakhon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek hain woh lakhon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mein jaake rukti hain ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein jaake rukti hain ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aati hai jo ankhhon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aati hai jo ankhhon mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno aisha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno aisha"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna to tumbhi samjho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna to tumbhi samjho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum chahe jitna chaho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum chahe jitna chaho"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum jitni koshish karlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum jitni koshish karlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum sa na hoga koi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum sa na hoga koi"/>
</div>
</pre>
