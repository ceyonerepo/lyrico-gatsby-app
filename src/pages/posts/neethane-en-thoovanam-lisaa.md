---
title: "neethane en thoovanam song lyrics"
album: "Lisaa"
artist: "Santhosh Dhayanidhi"
lyricist: "Mani Amuthavan"
director: "Raju Viswanath"
path: "/albums/lisaa-lyrics"
song: "Neethane En Thoovanam"
image: ../../images/albumart/lisaa.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r6YQUqIcDrA"
type: "love"
singers:
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neethanae En Thoovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae En Thoovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Nindra Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindra Pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Vaanam Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Vaanam Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalyenum Oru Naalyenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalyenum Oru Naalyenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saithu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saithu Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaal Enna Poothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaal Enna Poothaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaan Poothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaan Poothaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothaan Poothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothaan Poothaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae En Thoovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae En Thoovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Nindra Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindra Pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Vaanam Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Vaanam Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalyenum Oru Naalyenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalyenum Oru Naalyenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saithu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saithu Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaal Enna Poothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaal Enna Poothaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruviyilae Nanaigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruviyilae Nanaigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyaattum Kuruvigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyaattum Kuruvigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagazhagai Isaikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagazhagai Isaikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyar Theriya Paravaigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyar Theriya Paravaigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruviyilae Nanaigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruviyilae Nanaigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyaattum Kuruvigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyaattum Kuruvigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagazhagai Isaikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagazhagai Isaikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyar Theriya Paravaigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyar Theriya Paravaigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae En Thoovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae En Thoovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Nindra Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindra Pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Vaanam Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Vaanam Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerilae Vannam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerilae Vannam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verilae Vannam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verilae Vannam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkalae Vannam Engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalae Vannam Engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi Vanthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Vanthaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovilae Pattampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovilae Pattampoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothiyae Vannamachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothiyae Vannamachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Unnai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Unnai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Seivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Seivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moongil Thengiya Paniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Thengiya Paniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Thonuthae Thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Thonuthae Thaniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Ennaiyae Veyilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Ennaiyae Veyilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthi Chelvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthi Chelvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaravanooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaravanooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaravano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaravano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae En Thoovanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae En Thoovanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Nindra Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindra Pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Vaanam Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Vaanam Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalyenum Oru Naalyenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalyenum Oru Naalyenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saithu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saithu Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaal Enna Poothaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaal Enna Poothaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaahmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaahmmmmmmmm"/>
</div>
</pre>
