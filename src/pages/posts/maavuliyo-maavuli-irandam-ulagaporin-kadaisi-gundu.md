---
title: "maavuliyo maavuli song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Thanikodi"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Maavuliyo Maavuli"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O1KK17pK9h4"
type: "love"
singers:
  - Tenma
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai Virithidum Sudargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Virithidum Sudargala"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkini Marangalin Malargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkini Marangalin Malargala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Iruttai Sutri Viratti Selludhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Iruttai Sutri Viratti Selludhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Panam Pol Thugalgala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Panam Pol Thugalgala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Chinnanchiru Pagalgala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Chinnanchiru Pagalgala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kizhakkum Merkum Pinni Kolludhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kizhakkum Merkum Pinni Kolludhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulin Nadhiyil Vinmeengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulin Nadhiyil Vinmeengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Podum Ethirneechal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Podum Ethirneechal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikkum Theeyin Panam Pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikkum Theeyin Panam Pookkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukullae Pudhu Kaaichal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukullae Pudhu Kaaichal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril Poothu Neeril Kaayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Poothu Neeril Kaayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambal Poo Thaan Penmaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambal Poo Thaan Penmaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeril Nindru Poril Vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Nindru Poril Vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagai Soodum Illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagai Soodum Illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulavu Kaattu Kodaiyaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulavu Kaattu Kodaiyaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulachi Thiranda Balasaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulachi Thiranda Balasaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyil Ozhi Vidum Neelamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil Ozhi Vidum Neelamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu Thisaigalin Vaanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Thisaigalin Vaanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Theendum Inbam Vendi Ketkudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Theendum Inbam Vendi Ketkudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetchi Pookkalin Kaalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetchi Pookkalin Kaalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyum Meeridum Vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyum Meeridum Vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kaatril Vandhu Kaigal Korkumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kaatril Vandhu Kaigal Korkumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paniyin Oliyil Vilaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin Oliyil Vilaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagum Manadhin Uraiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagum Manadhin Uraiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Paadhi Swaasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Paadhi Swaasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimai Thaanae Naam Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimai Thaanae Naam Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenda Kaala Irulai Anaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Kaala Irulai Anaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirum Neruppu Pattrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirum Neruppu Pattrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Serum Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Serum Tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idathaai Bhoomi Suttrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idathaai Bhoomi Suttrumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai Viriththidum Sudargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Viriththidum Sudargala"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkini Marangalin Malargala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkini Marangalin Malargala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Iruttai Sutri Viratti Selluthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Iruttai Sutri Viratti Selluthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Panam Pol Thugalgala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Panam Pol Thugalgala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Chinnanchiru Pagalgala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Chinnanchiru Pagalgala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kizhakkum Merkum Pinni Kolludhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kizhakkum Merkum Pinni Kolludhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Saattai Kodi Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Saattai Kodi Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicha Kangu Vidhai Thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicha Kangu Vidhai Thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenda Kaala Sinameri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Kaala Sinameri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirndhu Vaaran Komberi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu Vaaran Komberi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavuliyo Maavuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavuliyo Maavuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavulikaaran Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavulikaaran Poraali"/>
</div>
</pre>
