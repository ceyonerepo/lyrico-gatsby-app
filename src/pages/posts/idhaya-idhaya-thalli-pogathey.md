---
title: "idhaya idhaya song lyrics"
album: "Thalli Pogathey"
artist: "Gopi Sunder"
lyricist: "Kabilan Vairamuthu"
director: "R. Kannan"
path: "/albums/thalli-pogathey-song-lyrics"
song: "Idhaya Idhaya"
image: ../../images/albumart/thalli-pogathey.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hQjYk1JZAbY"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya idhaya enai aalgira idhaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya idhaya enai aalgira idhaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan vizhagum nizhai yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan vizhagum nizhai yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai unara pirivenbadhu vilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai unara pirivenbadhu vilaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai namai inaikkira alai thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai namai inaikkira alai thaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En karmegam angae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En karmegam angae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi pallam ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi pallam ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru niraiyadha vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru niraiyadha vaazhvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhuthae neezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhuthae neezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulan aindhum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulan aindhum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulam peyarntha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulam peyarntha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin ozhiyo unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin ozhiyo unadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un peyar ondrae mandhiramaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un peyar ondrae mandhiramaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai sonnaal en irulae kuyilaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai sonnaal en irulae kuyilaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">En maarbilae un muga regaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maarbilae un muga regaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhiyamalae en dhegam kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyamalae en dhegam kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavo ninavaai nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavo ninavaai nadikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya idhaya enai aalgira idhaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya idhaya enai aalgira idhaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan vizhagum nizhai yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan vizhagum nizhai yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai unara pirivenbadhu vilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai unara pirivenbadhu vilaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai namai inaikkira alai thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai namai inaikkira alai thaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En karmegam angae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En karmegam angae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi pallam ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi pallam ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru niraiyadha vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru niraiyadha vaazhvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhuthae neezhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhuthae neezhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulan aindhum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulan aindhum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulam peyarntha podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulam peyarntha podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin ozhiyo unadhaeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin ozhiyo unadhaeae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm mm mm"/>
</div>
</pre>
