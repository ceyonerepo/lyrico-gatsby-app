---
title: "nethu ora kannil song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Dhanush"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Nethu Ora Kannil"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2020-07-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0xDn8x-7Vew"
type: "love"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nethu ora kannil nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu ora kannil nan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">eh nethu jada senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh nethu jada senju"/>
</div>
<div class="lyrico-lyrics-wrapper">ne enna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne enna patha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ekuthappa matikiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekuthappa matikiten"/>
</div>
<div class="lyrico-lyrics-wrapper">akkaporil sikikiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkaporil sikikiten"/>
</div>
<div class="lyrico-lyrics-wrapper">vanji kodi en konjum kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanji kodi en konjum kili"/>
</div>
<div class="lyrico-lyrics-wrapper">pakathula ne nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathula ne nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">vekathula nan sirika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekathula nan sirika"/>
</div>
<div class="lyrico-lyrics-wrapper">kachithama nenjil otikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kachithama nenjil otikita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethu ora kannil nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu ora kannil nan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">eh nethu jada senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh nethu jada senju"/>
</div>
<div class="lyrico-lyrics-wrapper">ne enna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne enna patha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thu thu thu thu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu thu thu thu"/>
</div>
<div class="lyrico-lyrics-wrapper">thu thu thu thu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thu thu thu thu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">vai nama namankuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vai nama namankuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethavathu sapdalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethavathu sapdalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">baby neyum sweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baby neyum sweet"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum konjam cute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum konjam cute"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal la nan solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal la nan solla"/>
</div>
<div class="lyrico-lyrics-wrapper">ini enna vali pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini enna vali pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">jillavuke nan thane treat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jillavuke nan thane treat"/>
</div>
<div class="lyrico-lyrics-wrapper">adi nee pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi nee pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">en kai korkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kai korkura"/>
</div>
<div class="lyrico-lyrics-wrapper">gear aagi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gear aagi ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">en heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethu ora kannil nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu ora kannil nan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">eh nethu jada senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh nethu jada senju"/>
</div>
<div class="lyrico-lyrics-wrapper">ne enna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne enna patha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">yenga public ah vendame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga public ah vendame"/>
</div>
<div class="lyrico-lyrics-wrapper">apo room podrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apo room podrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ehtavathu idea iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ehtavathu idea iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">aahan apdilam illanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahan apdilam illanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya unga kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya unga kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">apdiye manthopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apdiye manthopu"/>
</div>
<div class="lyrico-lyrics-wrapper">poonthoppu pump set
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poonthoppu pump set"/>
</div>
<div class="lyrico-lyrics-wrapper">nu polama jolly ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nu polama jolly ah"/>
</div>
<div class="lyrico-lyrics-wrapper">enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna"/>
</div>
<div class="lyrico-lyrics-wrapper">serious ah solranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serious ah solranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthi illa yarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi illa yarum"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham thara venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham thara venum"/>
</div>
<div class="lyrico-lyrics-wrapper">epavum nan thillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavum nan thillu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo vuturuche allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo vuturuche allu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum konjam pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum konjam pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinusa thaakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinusa thaakura"/>
</div>
<div class="lyrico-lyrics-wrapper">athil nan verkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil nan verkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kooda sogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kooda sogama"/>
</div>
<div class="lyrico-lyrics-wrapper">than iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">aathi aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">ne ipo en kootula sethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne ipo en kootula sethi"/>
</div>
<div class="lyrico-lyrics-wrapper">eh aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">aathi aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathuruvom pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathuruvom pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">peran pethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peran pethi"/>
</div>
</pre>
