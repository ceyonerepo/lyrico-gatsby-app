---
title: "kaanal neeraai song lyrics"
album: "Writer"
artist: "Govind Vasantha"
lyricist: "Muthuvel"
director: "Franklin Jacob"
path: "/albums/writer-song-lyrics"
song: "Kaanal Neeraai"
image: ../../images/albumart/writer.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h7grrT0-fzU"
type: "melody"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaanal Neerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Neerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkum Vazhiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkum Vazhiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Sunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Sunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Theendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Than Vazhvai Meettum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Vazhvai Meettum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Vizhuthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vizhuthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaadhum Neeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Neeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Suzhalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Suzhalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhalum Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalum Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Veril Neerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veril Neerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum Uyirin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Uyirin "/>
</div>
<div class="lyrico-lyrics-wrapper">Uravin Nal Varave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravin Nal Varave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Eriyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Eriyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kilaiyin Thulirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilaiyin Thulirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu Adhu Pugayagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Adhu Pugayagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayatha Mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayatha Mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Varavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Varavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai Ethanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai Ethanai "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ezhumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ezhumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Than Kantu Muttum Valiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kantu Muttum Valiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangatha Pasu Thaan Undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangatha Pasu Thaan Undo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Nee Aanathu Kande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Nee Aanathu Kande"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Un Udal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Un Udal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thulir Nazhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulir Nazhuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Vanthe Soozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Vanthe Soozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Vanthe Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Vanthe Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yaaro Thedum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yaaro Thedum "/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttai Pol Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttai Pol Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pul Nilam Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pul Nilam Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Mazhai Thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Mazhai Thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Naatrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Naatrum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirai Vilaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirai Vilaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelum Paadhai Yenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Paadhai Yenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum Siragaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum Siragaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanum Thaayaai Negizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanum Thaayaai Negizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil Mezhugaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Mezhugaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammai Soozhum Vazhvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Soozhum Vazhvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhire Niraiyum Ethiril Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhire Niraiyum Ethiril Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkum Naalil Karayil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkum Naalil Karayil "/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho Kaatchi Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho Kaatchi Unde"/>
</div>
</pre>
