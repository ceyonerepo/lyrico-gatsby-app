---
title: "kandaangi selai song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Yugabharathi"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Kandaangi Selai"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Ananthu
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kandaangi sela kaiyoadu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi sela kaiyoadu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaandhu kaasa nee kududaa paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaandhu kaasa nee kududaa paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">munthaanaimela munnooru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthaanaimela munnooru aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">undaagumboadhu nee kududaa paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undaagumboadhu nee kududaa paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu thavaru illa thavaru illa thaththalikka vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu thavaru illa thavaru illa thaththalikka vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru olivu illa maraivu illa oththukkolladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru olivu illa maraivu illa oththukkolladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vellaavi kannula vetkatha nee thinnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellaavi kannula vetkatha nee thinnudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">muttaayi kaiyila muththamittukkolludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttaayi kaiyila muththamittukkolludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi sela kaiyoadu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi sela kaiyoadu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaandhu kaasa nee kududaa paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaandhu kaasa nee kududaa paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">munthaanaimela munnooru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthaanaimela munnooru aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoam tharigida thoa theem tharigida theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoam tharigida thoa theem tharigida theem"/>
</div>
<div class="lyrico-lyrics-wrapper">thoam theem tharigida thagida thagida thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoam theem tharigida thagida thagida thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoam tharigida thoa theem tharigida theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoam tharigida thoa theem tharigida theem"/>
</div>
<div class="lyrico-lyrics-wrapper">thoam theem tharigida thagida thagida thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoam theem tharigida thagida thagida thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodi poattu maraikkira muzhusungaatta marukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi poattu maraikkira muzhusungaatta marukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vandha engala nee thethipoala kizhikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vandha engala nee thethipoala kizhikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodhakkaathu veesala ooru innum thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhakkaathu veesala ooru innum thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">boadhai yeri poanadhum polamburadhu thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boadhai yeri poanadhum polamburadhu thaangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegathoada vandhoam naanga vervai theeyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegathoada vandhoam naanga vervai theeyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi neeyum poavadhenna theervu agala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi neeyum poavadhenna theervu agala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porumaiyillaa manushanukku yedhu nimmadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porumaiyillaa manushanukku yedhu nimmadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">poruthirundhaa poara mazhai irukku sammadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poruthirundhaa poara mazhai irukku sammadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnoada vettikkadhai ippa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoada vettikkadhai ippa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhaarai vaazhavaikka neram odhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhaarai vaazhavaikka neram odhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi sela kaiyoadu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi sela kaiyoadu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaandhu kaasa nee kududaa paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaandhu kaasa nee kududaa paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">munthaanaimela munnooru aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthaanaimela munnooru aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koalam poada vaasalu koovathaaney sevalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koalam poada vaasalu koovathaaney sevalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaavi neenga thazhumboadhu naanumkooda yenjalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaavi neenga thazhumboadhu naanumkooda yenjalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamambonna pudikka maadiveedum pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamambonna pudikka maadiveedum pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">saamakkoazhi koovumboadhu saadhamkooda pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamakkoazhi koovumboadhu saadhamkooda pudikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadagatha medaiyetha venum oththigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagatha medaiyetha venum oththigai"/>
</div>
<div class="lyrico-lyrics-wrapper">ragasiyathai purinjikkittaa thenamum pandigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragasiyathai purinjikkittaa thenamum pandigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhagatha paarththu yeruppoottala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhagatha paarththu yeruppoottala"/>
</div>
<div class="lyrico-lyrics-wrapper">saayangaala aruvadaiya innum paarkkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayangaala aruvadaiya innum paarkkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha ungitta viththaiyirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha ungitta viththaiyirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">theendaama theeraadhu nakkal kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendaama theeraadhu nakkal kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oiyaaramaaga ullaasamaaga santhoasamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiyaaramaaga ullaasamaaga santhoasamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee velaiyaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee velaiyaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">embaattu Gaanaa nippaattu venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="embaattu Gaanaa nippaattu venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoadu onnaa nee uravaadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoadu onnaa nee uravaadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakala kalakala kannivedi neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakala kalakala kannivedi neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">minu minukkura minu minukkura seppuselaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minu minukkura minu minukkura seppuselaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkaama sokkura sokkaththangam nikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkaama sokkura sokkaththangam nikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">vikkama vikkura vittadhellaam kaetkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vikkama vikkura vittadhellaam kaetkura"/>
</div>
<div class="lyrico-lyrics-wrapper">oiyaaramaaga ullaasamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oiyaaramaaga ullaasamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">sonthoasamaaga nee vilaiyaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthoasamaaga nee vilaiyaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">embaattu Gaanaa nippaattu venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="embaattu Gaanaa nippaattu venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoadu onnaa nee uravaadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoadu onnaa nee uravaadavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoam tharigida thoa theem tharigida theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoam tharigida thoa theem tharigida theem"/>
</div>
<div class="lyrico-lyrics-wrapper">thoam theem tharigida thagida thagida thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoam theem tharigida thagida thagida thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoam tharigida thoa theem tharigida theem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoam tharigida thoa theem tharigida theem"/>
</div>
<div class="lyrico-lyrics-wrapper">thoam theem tharigida thagida thagida thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoam theem tharigida thagida thagida thaa"/>
</div>
</pre>
