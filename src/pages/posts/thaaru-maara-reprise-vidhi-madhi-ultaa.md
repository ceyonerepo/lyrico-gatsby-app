---
title: "thaaru maara reprise song lyrics"
album: "Vidhi Madhi Ultaa"
artist: "Ashwin Vinayagamoorthy"
lyricist: "Kabilan"
director: "Vijai Balaji"
path: "/albums/vidhi-madhi-ultaa-lyrics"
song: "Thaaru Maara Reprise"
image: ../../images/albumart/vidhi-madhi-ultaa.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MakwG1aEKfE"
type: "love"
singers:
  - Anthony Kevin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tholaivil nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaivil nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaindhen nan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaindhen nan than"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalai thodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalai thodava"/>
</div>
<div class="lyrico-lyrics-wrapper">nilave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilave "/>
</div>
<div class="lyrico-lyrics-wrapper">marunaal badhilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunaal badhilai"/>
</div>
<div class="lyrico-lyrics-wrapper">arindhal indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arindhal indre"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyai kudhipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyai kudhipen"/>
</div>
<div class="lyrico-lyrics-wrapper">azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya naa thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya naa thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikiren nenava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikiren nenava"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenava kedakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenava kedakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">rayila nee rayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rayila nee rayila"/>
</div>
<div class="lyrico-lyrics-wrapper">kadakura udan vaa di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadakura udan vaa di"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal vettukathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal vettukathi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil thookadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil thookadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye naano attakathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye naano attakathi"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan romba paavam di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan romba paavam di"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhala nenjukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhala nenjukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">moota kattadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moota kattadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kannakuli kulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakuli kulle"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kappal otaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kappal otaadhe"/>
</div>
</pre>
