---
title: 'pullinangal song lyrics'
album: '2.0'
artist: 'A R Rahman'
lyricist: 'Na. Muthu Kumar'
director: 'Shankar'
path: '/albums/2.0-song-lyrics'
song: 'Pullinangal'
image: ../../images/albumart/2-0.jpg
date: 2017-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sNm55ptmDcs"
type: 'Motivational'
singers: 
- Bamba Bakya
- A.R. Ameen
- Suzanne D'Mello
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Pullinangal Oh Pullinangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullinangal Oh Pullinangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pecharavam Kettu Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pecharavam Kettu Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullinangaal Oh Pullinangaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullinangaal Oh Pullinangaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Keecholigal Vendugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Keecholigal Vendugindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhi Illai Madham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Illai Madham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhum Oorae Engiraaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Oorae Engiraaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi Illai Madham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Illai Madham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhum Oorae Engiraaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Oorae Engiraaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pul Poondu Athu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pul Poondu Athu Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Endru Solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Endru Solgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu Vilaiyaada Oonjal ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Vilaiyaada Oonjal ingey"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadan Vaangi Sirikkindra Maanudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadan Vaangi Sirikkindra Maanudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Koigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Koigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Entham Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Entham Chellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Ullam Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Ullam Vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Alinthe Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Alinthe Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kaakka Thondrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaakka Thondrume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sel Sel Sel Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sel Sel Sel Sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Sel Sel Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sel Sel Sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaigal Ellai Sel Sel Sel Sel Sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaigal Ellai Sel Sel Sel Sel Sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Yendhi Sell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Yendhi Sell"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkaalathu Kadhir Oliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkaalathu Kadhir Oliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragasaithu Varaverpaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragasaithu Varaverpaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Maanin Tholgalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Maanin Tholgalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottanaithu Thoonga Veippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottanaithu Thoonga Veippaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Kaalin Men Nadaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kaalin Men Nadaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Kolam Pottu Veippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Kolam Pottu Veippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Poley Parapatharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Poley Parapatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Indru Yenga Veippai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Indru Yenga Veippai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullinangal Pullinangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullinangal Pullinangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Petcharavam Kettu Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Petcharavam Kettu Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullinangaal Oh Pullinangaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullinangaal Oh Pullinangaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pecharavam Kettu Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pecharavam Kettu Nindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullinangaal Oh Pullinangaaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullinangaal Oh Pullinangaaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Keecholigal Vendugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Keecholigal Vendugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Keecholigal Vendugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Keecholigal Vendugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Keecholigal Vendugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Keecholigal Vendugindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendugindren Vendugindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendugindren Vendugindren"/>
</div>
</pre>