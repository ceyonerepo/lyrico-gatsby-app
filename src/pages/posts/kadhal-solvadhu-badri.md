---
title: "kadhal solvadhu song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Kadhal Solvadhu"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kcZDewQidNY"
type: "love"
singers:
  - Srinivas
  - Sunitha Upadrashta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaathal Solvathu Oothadugal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Solvathu Oothadugal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalthaan Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalthaan Thalaivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Solvathum Vaarthaigal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Solvathum Vaarthaigal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaigal Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaigal Thalaivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithai Enbathu Putthagam Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Enbathu Putthagam Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal Thaan Sakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal Thaan Sakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal Yaavarum Kavithaigal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal Yaavarum Kavithaigal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Sakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Sakiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadadadaa Innum En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadadadaa Innum En "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Puriyallayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Puriyallayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Madaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Madaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Ennadi Ithayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Ennadi Ithayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyaeri Alagindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyaeri Alagindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ithuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ithuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Solvaen Puriyum Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Solvaen Puriyum Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalai Vidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalai Vidudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannichikkadi Kaathal Seyvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannichikkadi Kaathal Seyvaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalai Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalai Padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Heyy..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Heyy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Solvathu Oothadugal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Solvathu Oothadugal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalthaan Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalthaan Thalaivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Solvathum Vaarthaigal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Solvathum Vaarthaigal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaigal Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaigal Thalaivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padapadakkum Yenathu Vili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkum Yenathu Vili "/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Nadanthukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Nadanthukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvathu Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathu Sariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavaru Seythaal Mooththam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaru Seythaal Mooththam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthu Ennai Thiruthikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthu Ennai Thiruthikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandanai Sariyaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandanai Sariyaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppoluthellaam Thavaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppoluthellaam Thavaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Seyvai Sollividudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyvai Sollividudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollugiraen Ippothoru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollugiraen Ippothoru "/>
</div>
<div class="lyrico-lyrics-wrapper">Mooththam Koduudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooththam Koduudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Solvathu Oothadugal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Solvathu Oothadugal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalthaan Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalthaan Thalaivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Solvathum Vaarthaigal Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Solvathum Vaarthaigal Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaigal Thalaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaigal Thalaivaa"/>
</div>
</pre>
