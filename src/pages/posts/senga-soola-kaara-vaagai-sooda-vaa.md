---
title: "senga soola kaara song lyrics"
album: "Vaagai Sooda Vaa"
artist: "Ghibran"
lyricist: "Vairamuthu"
director: "A. Sarkunam"
path: "/albums/vaagai-sooda-vaa-lyrics"
song: "Senga Soola Kaara"
image: ../../images/albumart/vaagai-sooda-vaa.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7hTrRarI5hY"
type: "happy"
singers:
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmmm mmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmm mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanja kallu venthu poochu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanja kallu venthu poochu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanja kallu venthu poochu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanja kallu venthu poochu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam koodi iruti poochu vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam koodi iruti poochu vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta sutta mannu kallachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta sutta mannu kallachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta natta kallu veedachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta natta kallu veedachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu nachu patta namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachu nachu patta namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozappu thaan pacha manna poochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozappu thaan pacha manna poochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vitha vitha kallu ennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitha vitha kallu ennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinna vinna thottu ninnaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinna vinna thottu ninnaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu kuzhi poola namma parampara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu kuzhi poola namma parampara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam aagi poochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam aagi poochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyanaaru saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyanaaru saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Azuthu theerthu paarthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azuthu theerthu paarthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooranaketta saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranaketta saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Sootha thaana kettom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sootha thaana kettom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalavaasal kangu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalavaasal kangu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kali mulla vetti vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kali mulla vetti vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Senga soola kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanja kallu venthu poochu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanja kallu venthu poochu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam koodi iruti poochu vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam koodi iruti poochu vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannu mannu mattum sooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu mannu mattum sooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka makka vazunthu varaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka makka vazunthu varaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai vara pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vara pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai vara pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vara pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazha mazha vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazha mazha vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu karaigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu karaigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka enga poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka enga poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ittha kali mannu vegaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittha kali mannu vegaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga thala mura maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga thala mura maarathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna kindi vazhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna kindi vazhum "/>
</div>
<div class="lyrico-lyrics-wrapper">mannu puzuvukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannu puzuvukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu vaasal yethu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu vaasal yethu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyanaaru saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyanaaru saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna thiranthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna thiranthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga sanam vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga sanam vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vita yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vita yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirkaalam unakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalam unakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu ettu vachu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu ettu vachu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaanae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaanae nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthananae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthananae nanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verva thanni veetukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verva thanni veetukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakku yethum vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakku yethum vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Verva thanni veetukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verva thanni veetukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakku yethum vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakku yethum vaada"/>
</div>
</pre>
