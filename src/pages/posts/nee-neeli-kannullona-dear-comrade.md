---
title: "nee neeli kannullona song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Nee Neeli Kannullona"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_tBZM1sNT9M"
type: "love"
singers:
  - Gowtham Bharathwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Neeli Kannulloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neeli Kannulloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellari Allesindhi Nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellari Allesindhi Nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaali Andelloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaali Andelloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethame Soki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethame Soki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaipe Lagesthundhi Nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaipe Lagesthundhi Nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poola Navvulloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poola Navvulloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanamdhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanamdhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenello Munchesindhi Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenello Munchesindhi Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosame Naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosame Naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle Vakille Theesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle Vakille Theesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Mungille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Mungille"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Ilaa Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Ilaa Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Veche Vunnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veche Vunnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vooge Pranam Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vooge Pranam Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru Chudani Ee Alajadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru Chudani Ee Alajadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhuru Marachina Na Yedha Sadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhuru Marachina Na Yedha Sadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuru Chusthu Prathi Vekuvalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuru Chusthu Prathi Vekuvalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidura Marachina Raathiri Vodilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidura Marachina Raathiri Vodilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Neeli Kannulloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neeli Kannulloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaali Andelloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaali Andelloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethame Soki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethame Soki"/>
</div>
</pre>
