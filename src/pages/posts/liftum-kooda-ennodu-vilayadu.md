---
title: "liftum kooda song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Sarathy"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Liftum Kooda"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/99l7fNRnduw"
type: "love"
singers:
  - Deepak
  - B Mac
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">i can feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i can feel"/>
</div>
<div class="lyrico-lyrics-wrapper">you next to me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you next to me"/>
</div>
<div class="lyrico-lyrics-wrapper">so close girl yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so close girl yo"/>
</div>
<div class="lyrico-lyrics-wrapper">gotta into my world
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gotta into my world"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">love is rough
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love is rough"/>
</div>
<div class="lyrico-lyrics-wrapper">i sent an angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i sent an angel"/>
</div>
<div class="lyrico-lyrics-wrapper">she came with a love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="she came with a love"/>
</div>
<div class="lyrico-lyrics-wrapper">in her heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in her heart"/>
</div>
<div class="lyrico-lyrics-wrapper">colors fallen angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colors fallen angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">liftum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="liftum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">gift aai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gift aai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">penne unnaal kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne unnaal kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">left ah right ah kicker
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left ah right ah kicker"/>
</div>
<div class="lyrico-lyrics-wrapper">gear ellam unnal maranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gear ellam unnal maranthene"/>
</div>
<div class="lyrico-lyrics-wrapper">o o o dinamum unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o o o dinamum unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyil thunaikku ivanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyil thunaikku ivanai"/>
</div>
<div class="lyrico-lyrics-wrapper">ini alaipaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini alaipaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya valiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya valiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiya payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiya payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum tharuvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum tharuvaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai enakulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai enakulle"/>
</div>
<div class="lyrico-lyrics-wrapper">adaikkathu vaipeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaikkathu vaipeno"/>
</div>
<div class="lyrico-lyrics-wrapper">kudai saainthu poveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudai saainthu poveno"/>
</div>
<div class="lyrico-lyrics-wrapper">o o o o o o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o o o o o o"/>
</div>
</pre>
