---
title: "yennuyirey female version song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Thamarai"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Yennuyirey Female Version"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/K1mfRG8TSN4"
type: "sad"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yennuyirae yennuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyirae yennuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thandhaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravendru sonnal nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravendru sonnal nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhirathil odum poonthaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirathil odum poonthaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamum thavamum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamum thavamum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyum marunthum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyum marunthum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil kalandha en thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil kalandha en thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam undhan thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam undhan thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam sokkathangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam sokkathangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam undhan thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam undhan thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam sokkathangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam sokkathangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyirae yennuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyirae yennuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thandhaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pona podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pona podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaaval neezhum angaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaaval neezhum angaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaalin paadham theendatha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalin paadham theendatha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endredhum illai engaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endredhum illai engaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyil mazhaiyil vendum nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil mazhaiyil vendum nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan varuvaai neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan varuvaai neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal kulaikkum bhoomi inilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal kulaikkum bhoomi inilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai iruppaai thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai iruppaai thoongaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam kalaindhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam kalaindhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum thodargiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum thodargiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kai viral pattri padargiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kai viral pattri padargiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangai manam gangai yena perugudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangai manam gangai yena perugudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyirae yennuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyirae yennuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thandhaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravendru sonnal nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravendru sonnal nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhirathil odum poonthaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirathil odum poonthaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamum thavamum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamum thavamum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyum marunthum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyum marunthum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil kalandha en thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil kalandha en thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam undhan thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam undhan thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam sokkathangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam sokkathangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam undhan thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam undhan thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam thangam chella thangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam thangam chella thangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella thangam thangam sokkathangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella thangam thangam sokkathangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyirae"/>
</div>
</pre>
