---
title: "kovakkara ponnorithi song lyrics"
album: "Adanga Pasanga"
artist: "AK Alldern"
lyricist: "A Sivakumar"
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Kovakkara Ponnorithi"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8IQHwuVyJ4I"
type: "happy"
singers:
  - Sulpi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vantha varavil vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha varavil vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ponal selavil vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponal selavil vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya kathal na uyira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya kathal na uyira "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda panaya vaipome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda panaya vaipome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">college vasalila kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college vasalila kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">valikka naanga nipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valikka naanga nipom"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu kannu saladaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu kannu saladaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">figure pottu salippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="figure pottu salippom"/>
</div>
<div class="lyrico-lyrics-wrapper">therina ponnungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therina ponnungala"/>
</div>
<div class="lyrico-lyrics-wrapper">theduvom theduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduvom theduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu paduvom aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu paduvom aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">oththa thoondil pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa thoondil pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">anju meena pidipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anju meena pidipom"/>
</div>
<div class="lyrico-lyrics-wrapper">meenoda history matum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meenoda history matum"/>
</div>
<div class="lyrico-lyrics-wrapper">pocket la irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pocket la irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">alagaana ponnungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagaana ponnungala"/>
</div>
<div class="lyrico-lyrics-wrapper">pathuputta naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathuputta naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">siruchu siruchu pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siruchu siruchu pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi potuduvom goalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi potuduvom goalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">routula sanda itta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="routula sanda itta"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga anga naataama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga anga naataama"/>
</div>
<div class="lyrico-lyrics-wrapper">prachanaya theethu vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prachanaya theethu vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">panjayatha kootaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjayatha kootaama"/>
</div>
<div class="lyrico-lyrics-wrapper">ajaal kujaal paasa ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ajaal kujaal paasa ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku aththu padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku aththu padi"/>
</div>
<div class="lyrico-lyrics-wrapper">adangatha figure ngala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangatha figure ngala"/>
</div>
<div class="lyrico-lyrics-wrapper">athiradiya vituduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athiradiya vituduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaatha vilai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaatha vilai "/>
</div>
<div class="lyrico-lyrics-wrapper">romba irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">adangatha ponnuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangatha ponnuku "/>
</div>
<div class="lyrico-lyrics-wrapper">mouse romba irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mouse romba irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi mela adi aducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi mela adi aducha"/>
</div>
<div class="lyrico-lyrics-wrapper">ammiyum nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammiyum nagarum"/>
</div>
<div class="lyrico-lyrics-wrapper">ava manasum maaridume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava manasum maaridume"/>
</div>
<div class="lyrico-lyrics-wrapper">rummy um egirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rummy um egirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kovakaara ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovakaara ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kochukittu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kochukittu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">amsamaana ponnoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amsamaana ponnoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu puttu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu puttu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vantha varavil vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha varavil vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ponal selavil vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponal selavil vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya kathal na uyira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya kathal na uyira "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda panaya vaipome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda panaya vaipome"/>
</div>
</pre>
