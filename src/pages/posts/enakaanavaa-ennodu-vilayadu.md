---
title: "enakaanavaa song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Kadhirmozhi"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Enakaanavaa"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NOB8PKYnNhk"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaalai theaneerai kaiyil yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai theaneerai kaiyil yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhaigal peasu poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhaigal peasu poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai theerndhaal kangal rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai theerndhaal kangal rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">peachu thunaiyaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peachu thunaiyaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kan pandhaadum nearam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kan pandhaadum nearam"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo ennaagum naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo ennaagum naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">ulley pala nooru vaanam kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulley pala nooru vaanam kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">anbaai nee peasumboadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbaai nee peasumboadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">vambaai nee paarkkum poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambaai nee paarkkum poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">yedho ondraaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedho ondraaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupillai poal unai paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupillai poal unai paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangam yedhum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangam yedhum illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal pillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal pillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">avasthaigal azhagena thoanudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avasthaigal azhagena thoanudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkaaga vaazhavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaaga vaazhavey"/>
</div>
<div class="lyrico-lyrics-wrapper">saaigiraai enai saaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaigiraai enai saaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhi marandhidum anubavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhi marandhidum anubavam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai nanaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai nanaigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaikkaanavaa enaikkaanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaikkaanavaa enaikkaanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril poagudhey kaalgaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril poagudhey kaalgaley"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkaanavaa enaikkaanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkaanavaa enaikkaanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkaagavey O varamenakidaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkaagavey O varamenakidaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho ennil maatram vandhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho ennil maatram vandhey"/>
</div>
<div class="lyrico-lyrics-wrapper">koochal konden ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koochal konden ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvai yedhum theendumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvai yedhum theendumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">poothukolven ulley ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothukolven ulley ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennil yegaandha meaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil yegaandha meaga"/>
</div>
<div class="lyrico-lyrics-wrapper">unai poal ennai paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai poal ennai paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadham thadumaari yengum endrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadham thadumaari yengum endrey"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam pudhithaaga thoandrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam pudhithaaga thoandrum"/>
</div>
<div class="lyrico-lyrics-wrapper">penmai unai etti paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penmai unai etti paarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam thalaikeezhaai maarum endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam thalaikeezhaai maarum endren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu poadhumey idhu poadhumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu poadhumey idhu poadhumey"/>
</div>
<div class="lyrico-lyrics-wrapper">enathaagumey edhirkaalamey O O Ovv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathaagumey edhirkaalamey O O Ovv"/>
</div>
<div class="lyrico-lyrics-wrapper">anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbey"/>
</div>
</pre>
