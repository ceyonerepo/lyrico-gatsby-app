---
title: "idhayam thudikirathe song lyrics"
album: "Good Luck"
artist: "Manoj Bhatnaghar"
lyricist: "Vairamuthu"
director: "Manoj Bhatnaghar"
path: "/albums/good-luck-song-lyrics"
song: "Idhayam Thudikirathe"
image: ../../images/albumart/good-luck.jpg
date: 2000-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LwOolIM_K1M"
type: "melody"
singers:
  - K. Prabhakaran
  - Chithra
  - Baby Deepika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal kadalil naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal kadalil naanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai theevaai aanaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai theevaai aanaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naallum unnai mattum enni kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naallum unnai mattum enni kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhkirean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavodu inge vilaiyaada vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavodu inge vilaiyaada vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirai pothum endrea uravaadukindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai pothum endrea uravaadukindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkaana maegam inge thisai maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaana maegam inge thisai maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai meerum kanneer ennai vilai pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai meerum kanneer ennai vilai pesuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anai pottum nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anai pottum nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai veesak kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai veesak kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai meedhum meenai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai meedhum meenai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaarinaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaarinaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna kuyilai thaalaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna kuyilai thaalaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil mouna poraattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil mouna poraattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano ullangaiyil thanneer kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano ullangaiyil thanneer kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo…..ooo….ooh…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo…..ooo….ooh….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo…..ooo….ooh…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo…..ooo….ooh….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorengum dheivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorengum dheivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiyaaga kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaiyaaga kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaana dheivam nadamaada kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaana dheivam nadamaada kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuyil paattu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyil paattu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu kulippaattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu kulippaattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal kettu vantha sontham uyir meetkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal kettu vantha sontham uyir meetkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirodu ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai soodi konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai soodi konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayaththil deepam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththil deepam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai yaetrinaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai yaetrinaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil thottil koduththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil thottil koduththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil poththi valarththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil poththi valarththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naali neeyum naanum nilaa pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naali neeyum naanum nilaa pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa kaangiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thudikkirathae thaalam kidaikkurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudikkirathae thaalam kidaikkurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal pirakkirathae paasam malarkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal pirakkirathae paasam malarkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal kadalil naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal kadalil naanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai theevaai aanaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai theevaai aanaene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naallum unnai mattum enni kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naallum unnai mattum enni kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhkirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhkirean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam malarkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam malarkirathae"/>
</div>
</pre>
