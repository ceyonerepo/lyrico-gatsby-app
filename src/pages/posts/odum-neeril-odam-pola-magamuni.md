---
title: "odum neeril odam pola song lyrics"
album: "Magamuni"
artist: "S. Thaman"
lyricist: "Kavingnar Muthulingam"
director: "Santhakumar"
path: "/albums/magamuni-lyrics"
song: "Odum Neeril Odam Pola"
image: ../../images/albumart/magamuni.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YsmJGgcZcKo"
type: "happy"
singers:
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Odum neeril odum pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum neeril odum pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhakai odutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhakai odutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulle ulla shakthi ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle ulla shakthi ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai aatutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai aatutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvamellam silarukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvamellam silarukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge ondrai irukkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge ondrai irukkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya thiraiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya thiraiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam kannai maraikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kannai maraikkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomiyile podum vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyile podum vinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam mulaikkutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam mulaikkutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennugira ennam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennugira ennam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge nadakkutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge nadakkutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna vendum enbathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vendum enbathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai minji ethuvum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai minji ethuvum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovum paniyum pullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovum paniyum pullum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu punitham aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu punitham aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu pozhiyum nenjin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu pozhiyum nenjin"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu kadavul aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu kadavul aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan arintha viththaiglai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan arintha viththaiglai"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla thudikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla thudikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaana nizhai thedi manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaana nizhai thedi manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai virukkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai virukkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethai ethaiyo ninaitha padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai ethaiyo ninaitha padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarathu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarathu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan potta koottinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan potta koottinile"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhaluthu ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhaluthu ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirappukellam nookkam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirappukellam nookkam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai peruvathellam kaalangalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai peruvathellam kaalangalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendiyathai tharuvabarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyathai tharuvabarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thaan andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan andro"/>
</div>
<div class="lyrico-lyrics-wrapper">Venda vaithu paarpathuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venda vaithu paarpathuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thaan andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thaan andro"/>
</div>
</pre>
