---
title: "ramar nadu song lyrics"
album: "Ayudha Porattam"
artist: "Nandhan Raj - S.S. Athreya"
lyricist: "Thamizh Amuthan"
director: "Jai Akash"
path: "/albums/ayudha-porattam-lyrics"
song: "Ramar Nadu"
image: ../../images/albumart/ayudha-porattam.jpg
date: 2011-09-09
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamar naadu vandhadhu hightec pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamar naadu vandhadhu hightec pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">koocha naacham yedhuvumey adhukku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocha naacham yedhuvumey adhukku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kokkarakko gummaango pepsi thoazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokkarakko gummaango pepsi thoazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pouder poosi vandhuttaa aalaa jeelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pouder poosi vandhuttaa aalaa jeelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai yedhum illamma tharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai yedhum illamma tharaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">inbamaa ladaai yedhum illammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbamaa ladaai yedhum illammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam sirimmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam sirimmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un nadaiyila elloaraa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nadaiyila elloaraa sirppam"/>
</div>
<div class="lyrico-lyrics-wrapper">en sirippila Ragumaanu Albam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sirippila Ragumaanu Albam"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasula pudhu Roja pushpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasula pudhu Roja pushpam"/>
</div>
<div class="lyrico-lyrics-wrapper">un saayalo Ravivarmaa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un saayalo Ravivarmaa sirppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi konjam paarthendi paththirakaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi konjam paarthendi paththirakaali"/>
</div>
<div class="lyrico-lyrics-wrapper">serndhukkalaam vaayendi ikkadavaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serndhukkalaam vaayendi ikkadavaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam saanjikkavaa illa moandhukkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam saanjikkavaa illa moandhukkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">illa koadu poattu roadu poattu thaandikkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa koadu poattu roadu poattu thaandikkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aththai veettu pulla onnu paattu paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aththai veettu pulla onnu paattu paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">aaththu saisil ulladhamma vettu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaththu saisil ulladhamma vettu vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam theendikkiren illa saami vesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam theendikkiren illa saami vesham"/>
</div>
<div class="lyrico-lyrics-wrapper">poattu nillu vendikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poattu nillu vendikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">andha jeensu poatta paartti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha jeensu poatta paartti"/>
</div>
<div class="lyrico-lyrics-wrapper">ava manasu vellakkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava manasu vellakkatti"/>
</div>
<div class="lyrico-lyrics-wrapper">andha mayakkum ponnu sutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha mayakkum ponnu sutti"/>
</div>
<div class="lyrico-lyrics-wrapper">arey arey arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey arey arey"/>
</div>
<div class="lyrico-lyrics-wrapper">un nadaiyila elloaraa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nadaiyila elloaraa sirppam"/>
</div>
<div class="lyrico-lyrics-wrapper">en sirippila Ragumaanu Albam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sirippila Ragumaanu Albam"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasula pudhu Roja pushpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasula pudhu Roja pushpam"/>
</div>
<div class="lyrico-lyrics-wrapper">un saayalo Ravivarmaa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un saayalo Ravivarmaa sirppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Orappaarvai paarthutta moanaalisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Orappaarvai paarthutta moanaalisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en thoalu mela saanjikko paisa paisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thoalu mela saanjikko paisa paisa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kattikkanum illa vachikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kattikkanum illa vachikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada rendum ingu illaiyinnaa muttikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada rendum ingu illaiyinnaa muttikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">hei Muttinaalo meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei Muttinaalo meena"/>
</div>
<div class="lyrico-lyrics-wrapper">hei Meenaponnu Lena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei Meenaponnu Lena"/>
</div>
<div class="lyrico-lyrics-wrapper">Lena ingu thaanaa varaa varaa varaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lena ingu thaanaa varaa varaa varaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un nadaiyila elloaraa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nadaiyila elloaraa sirppam"/>
</div>
<div class="lyrico-lyrics-wrapper">en sirippila Ragumaanu Albam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sirippila Ragumaanu Albam"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasula pudhu Roja pushpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasula pudhu Roja pushpam"/>
</div>
<div class="lyrico-lyrics-wrapper">un saayalo Ravivarmaa sirppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un saayalo Ravivarmaa sirppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi jigidi jimbaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi jigidi jimbaa "/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigidi jimbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigidi jimbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hei jigidi jigididaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei jigidi jigididaa"/>
</div>
</pre>
