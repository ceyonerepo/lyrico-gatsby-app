---
title: "ek pappi song lyrics"
album: "Sayonee"
artist: "Anamta - Amaan"
lyricist: "Anamta - Amaan - Francis Eshwar Yash"
director: "Nitin Kumar Gupta - Abhay Singhal"
path: "/albums/sayonee-lyrics"
song: "Ek Pappi"
image: ../../images/albumart/sayonee.jpg
date: 2020-12-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/VFNBWp_vqWU"
type: "celebration"
singers:
  - Mika Singh
  - Anamta Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oye chup kar!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye chup kar!"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye kudiye!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye kudiye!"/>
</div>
<div class="lyrico-lyrics-wrapper">Kithhe chali!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithhe chali!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhari jawani manjha geela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhari jawani manjha geela"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight hoja laga tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight hoja laga tequila"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhari jawani manjha geela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhari jawani manjha geela"/>
</div>
<div class="lyrico-lyrics-wrapper">Tight hoja laga tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tight hoja laga tequila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhse na hoga tujhse na hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse na hoga tujhse na hoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse na hoga baby tujhse na hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse na hoga baby tujhse na hoga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main hun fire, tu baraf ki silli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hun fire, tu baraf ki silli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek pappi mein jaake girega dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek pappi mein jaake girega dilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hun sherni tu hai bheegi billi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hun sherni tu hai bheegi billi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek pappi mein jaake girega dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek pappi mein jaake girega dilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoye sun meri gall ni kudi patola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye sun meri gall ni kudi patola"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle chadd bhar balti pee laan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle chadd bhar balti pee laan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haye sunn meri gall ni kudi patola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye sunn meri gall ni kudi patola"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle chadd bhar balti pee laan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle chadd bhar balti pee laan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise na hoga main hun casanova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise na hoga main hun casanova"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise na hoga baby kaise na hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise na hoga baby kaise na hoga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ankh mili te sekega main chhalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ankh mili te sekega main chhalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naam te jatt de rukk jaandi ae dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naam te jatt de rukk jaandi ae dilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Main haan duffer tu hai jhalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main haan duffer tu hai jhalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ek pappi vich patt jaegi dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ek pappi vich patt jaegi dilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kudiye!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kudiye!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaye fighter jet mangvavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye fighter jet mangvavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Cockpit me baithawanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cockpit me baithawanga"/>
</div>
<div class="lyrico-lyrics-wrapper">London sondon paris varis
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="London sondon paris varis"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya la habibi dikhawanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya la habibi dikhawanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal hatt!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal hatt!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoor tainu kohinoor puwadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoor tainu kohinoor puwadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere mere roke te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere mere roke te"/>
</div>
<div class="lyrico-lyrics-wrapper">Je tu ik baar russ gayi taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Je tu ik baar russ gayi taan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mar jawanga mauke te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mar jawanga mauke te"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lambi lambi fekta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lambi lambi fekta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghoor ghoor ke dekhta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghoor ghoor ke dekhta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hun sexy sohni kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hun sexy sohni kudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu aankhein apni sekta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu aankhein apni sekta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhse na hoga tujhse na hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse na hoga tujhse na hoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaise na hoga baby kaise na hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise na hoga baby kaise na hoga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun tu banta hai sheikh chilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun tu banta hai sheikh chilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek pappi mein jaake girega dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek pappi mein jaake girega dilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiddan rahvegi ni tu kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiddan rahvegi ni tu kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aake pappi le pappi mallo malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake pappi le pappi mallo malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hunn taan tu dede mainu pappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taan tu dede mainu pappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaldi main taan turr jawanga dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaldi main taan turr jawanga dilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh datt ja oye kudiye kithe chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh datt ja oye kudiye kithe chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek pappi vich pair pahuchava dilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek pappi vich pair pahuchava dilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na ja oye!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na ja oye!"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakde!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakde!"/>
</div>
</pre>
