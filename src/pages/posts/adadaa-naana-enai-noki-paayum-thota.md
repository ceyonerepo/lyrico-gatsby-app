---
title: "adadaa naana song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Thamizhanangu"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Adadaa Naana"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xshaQwZjFbA"
type: "love"
singers:
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adadaa Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyil Nooragi Udainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyil Nooragi Udainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Nee En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee En"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velicha Poongatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicha Poongatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagi Pogathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagi Pogathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Siridhai Ingu Kumaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Siridhai Ingu Kumaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anindha Udaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anindha Udaiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarvai Mazhaiyaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarvai Mazhaiyaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Nanainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Nanainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalaa Mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalaa Mudivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalae Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalae Nee Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaa Nizhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaa Nizhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaai Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaai Nee Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhazhil Konjam Oramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazhil Konjam Oramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeneer Thandhaai Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeneer Thandhaai Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyalbaai Unnai Paarpadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyalbaai Unnai Paarpadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkam Illaamal Thaakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkam Illaamal Thaakudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrum Indrum Enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrum Indrum Enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenoo Ponadhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenoo Ponadhuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Kaalai Unmadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Kaalai Unmadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum Yena Thonuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum Yena Thonuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padarum Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padarum Vali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalaa Mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalaa Mudivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalae Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalae Nee Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaa Nizhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaa Nizhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaai Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaai Nee Sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyil Nooragi Udainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyil Nooragi Udainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Nee En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee En"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velicha Poongatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicha Poongatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagi Pogathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagi Pogathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Siridhai Ingu Kumaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Siridhai Ingu Kumaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Anindha Udaiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anindha Udaiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarvai Mazhaiyaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarvai Mazhaiyaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Nanainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Nanainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalaa Mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalaa Mudivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalae Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalae Nee Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaa Nizhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaa Nizhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaai Nee Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaai Nee Sol"/>
</div>
</pre>
