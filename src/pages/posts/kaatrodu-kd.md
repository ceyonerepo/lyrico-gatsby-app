---
title: "kaatrodu song lyrics"
album: "K D"
artist: "Karthikeya Murthy"
lyricist: "Karthikeya Murthy"
director: "Madhumitha"
path: "/albums/kd-lyrics"
song: "Kaatrodu"
image: ../../images/albumart/kd.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3ZVzGK1QPn0"
type: "sad"
singers:
  - Balram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Katrodu isai sernthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu isai sernthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirindha Naan vaazhndida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pirindha Naan vaazhndida vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjum kenjum un ninaival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjum kenjum un ninaival"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam thanjam serndidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam thanjam serndidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katrodu isai sernthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu isai sernthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirindha Naan vaazhndida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pirindha Naan vaazhndida vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam naragam agiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam naragam agiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai paasam vaatiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai paasam vaatiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrodu isai sernthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu isai sernthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirindha Naan vaazhndida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pirindha Naan vaazhndida vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai ennai marandodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai ennai marandodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malayum Inge maduvagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayum Inge maduvagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Endhan pakkam irukum naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Endhan pakkam irukum naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimai ennai ini aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimai ennai ini aalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai ennai kanthodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai ennai kanthodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum undhan pakkam irukum naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum undhan pakkam irukum naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan moondram piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan moondram piraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal muzhu nilavaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal muzhu nilavaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam pirive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam pirive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam naragam agiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam naragam agiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai paasam vattiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai paasam vattiduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katrodu isai sernthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu isai sernthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirindha Naan vaazhndida vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pirindha Naan vaazhndida vendum"/>
</div>
</pre>
