---
title: "yaen ennai pirindhaai song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Radhan"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Yaen Ennai Pirindhaai"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9pWrJM5nkl4"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannilae Kanneerilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae Kanneerilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthae Naan Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthae Naan Pogindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnilae Ven Megamaai Kalainthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnilae Ven Megamaai Kalainthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mella Mella Karainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mella Mella Karainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhugai Ennum Aruviyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhugai Ennum Aruviyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Naanum Vilunthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Naanum Vilunthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavae Un Nizhalinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae Un Nizhalinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarnthida Naanum Vizhainthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarnthida Naanum Vizhainthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Ennai Pirinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ennai Pirinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Eriththaai En Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Eriththaai En Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Ennai Pirinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ennai Pirinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Uraindhaai Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Uraindhaai Kanavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Ennai Pirinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ennai Pirinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Eriththaai En Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Eriththaai En Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Ennai Pirinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ennai Pirinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Uraindhaai Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Uraindhaai Kanavae"/>
</div>
</pre>
