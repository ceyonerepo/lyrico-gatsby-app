---
title: "top tucker song lyrics"
album: "Sarkar"
artist: "AR Rahman"
lyricist: "Vivek"
director: "AR Murugadoss"
path: "/albums/sarkar-lyrics"
song: "Top Tucker"
image: ../../images/albumart/sarkar.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4qx5qxh0t9A"
type: "mass"
singers:
  - Mohit Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai rai rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai rai rai rai rai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai rai rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai rai rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai rai rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai rai rai rai rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai rai rai rai rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai rai rai rai rai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top tuckeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top tuckeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharpu lookeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharpu lookeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Stuntu nakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stuntu nakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Spark flickerru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spark flickerru"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu shutteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu shutteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda utturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda utturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Risk namma radaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Risk namma radaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaraaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendae yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendae yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoihoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoihoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendae yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendae yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoihoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoihoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top tuckeru saathu udanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top tuckeru saathu udanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara thodura paarthu varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara thodura paarthu varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaththi kelambu kaaththu varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaththi kelambu kaaththu varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit aana bit aava vaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit aana bit aava vaa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morachi tholachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachi tholachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaadha energy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaadha energy"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirthaa adhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirthaa adhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda valarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda valarchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top tuckeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top tuckeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Spark flickerru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spark flickerru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendae yendae yendae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendae yendae yendae"/>
</div>
<div class="lyrico-lyrics-wrapper">Heey he heey he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heey he heey he"/>
</div>
<div class="lyrico-lyrics-wrapper">Heey he heey he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heey he heey he"/>
</div>
<div class="lyrico-lyrics-wrapper">Knife-ah thottavan flat-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Knife-ah thottavan flat-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkiren neat-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkiren neat-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Loaded gunnula thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loaded gunnula thotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top tuckeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top tuckeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharpu lookeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharpu lookeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Stuntu nakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stuntu nakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Spark flickerru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spark flickerru"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu shutteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu shutteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda utturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda utturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Risk namma radaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Risk namma radaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaraaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai mudinja vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai mudinja vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu mummyta solliru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu mummyta solliru"/>
</div>
<div class="lyrico-lyrics-wrapper">Clinic-u numberu irunthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clinic-u numberu irunthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee step eduthu vaiiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee step eduthu vaiiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vena thattika enteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena thattika enteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkum thunderu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkum thunderu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasaadhammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasaadhammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polaam raiiiiiohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polaam raiiiiiohoooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rai polaam rai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rai polaam rai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top tuckeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top tuckeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharpu lookeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharpu lookeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Stuntu nakkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stuntu nakkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Spark flickerru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spark flickerru"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu shutteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu shutteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda utturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda utturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Risk namma radaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Risk namma radaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaraaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadanganuyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madanganuyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madanganuyaa"/>
</div>
</pre>
