---
title: "thala keezha song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "Thala Keezha"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fVFnx-9e1vY"
type: "sad"
singers:
  - Manikka Vinayagam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Porakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Porakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Nadakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Nadakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayiru Endra Pallaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayiru Endra Pallaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayaththaiye Podhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththaiye Podhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Porakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Porakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Nadakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Nadakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayiru Endra Pallaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayiru Endra Pallaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayaththaiye Podhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththaiye Podhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishan Oru Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishan Oru Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka Pora Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka Pora Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegham Oru Satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegham Oru Satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yara Vaazha Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yara Vaazha Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Thuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Thuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Thuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Thuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Latcham Oru Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Latcham Oru Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru Irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru Irukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athana Usuraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athana Usuraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi Thorathittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Thorathittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Uyir Oththa Uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Uyir Oththa Uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvil Valarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvil Valarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla Mannu Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla Mannu Pattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kalangurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kalangurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Moththathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Moththathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannula Podhaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannula Podhaikkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla Mannu Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla Mannu Pattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kalangurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kalangurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyil Moththathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyil Moththathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannula Podhaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannula Podhaikkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishan Oru Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishan Oru Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka Pora Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka Pora Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegham Oru Satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegham Oru Satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yara Vaazha Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yara Vaazha Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Porakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Porakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Nadakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Nadakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayiru Endra Pallaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayiru Endra Pallaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayaththaiye Podhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththaiye Podhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppo Porakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppo Porakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppo Porakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppo Porakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethu Podum Aaththaalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethu Podum Aaththaalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyala Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyala Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppo Porakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppo Porakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppo Porakkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppo Porakkurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethu Podum Aaththaalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethu Podum Aaththaalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyala Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyala Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkaiya Muzhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkaiya Muzhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhavan Yaarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhavan Yaarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusan Oru Otta Paana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusan Oru Otta Paana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Neraiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Neraiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkaiya Muzhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkaiya Muzhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhavan Yaarammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhavan Yaarammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusan Oru Otta Paana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusan Oru Otta Paana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Neraiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Neraiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishan Oru Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishan Oru Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka Pora Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka Pora Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegham Oru Satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegham Oru Satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yara Vaazha Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yara Vaazha Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Porakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Porakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakeezhaa Nadakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakeezhaa Nadakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayiru Endra Pallaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayiru Endra Pallaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayaththaiye Podhaikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayaththaiye Podhaikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana Thanghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana Thanghame"/>
</div>
</pre>
