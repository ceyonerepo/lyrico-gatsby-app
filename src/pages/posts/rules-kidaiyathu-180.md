---
title: "rules kidaiyathu song lyrics"
album: "180"
artist: "Sharreth"
lyricist: "Madhan Karky"
director: "Jayendra Panchapakesan"
path: "/albums/180-lyrics"
song: "Rules Kidaiyathu"
image: ../../images/albumart/180.jpg
date: 2011-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4cJPv7ShKQE"
type: "happy"
singers:
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vuvasella Oothu Rules Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuvasella Oothu Rules Kidaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyattum Kaadhu Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyattum Kaadhu Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poharappo Beetu Kelaputhu Heatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poharappo Beetu Kelaputhu Heatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiruthu Streetu Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthu Streetu Yo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Thoonga Ima Padachaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Thoonga Ima Padachaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulla Paaththa Kathavadachaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulla Paaththa Kathavadachaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamiyum Vizhikum Nee Oothu Voovey Saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamiyum Vizhikum Nee Oothu Voovey Saala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyum Sezhikum Nee Oothu Voovey Saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyum Sezhikum Nee Oothu Voovey Saala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vuvasella Oothu Rules Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuvasella Oothu Rules Kidaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyattum Kaadhu Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyattum Kaadhu Yo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigazh Nigazh Kaalaam Idho Idho Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazh Nigazh Kaalaam Idho Idho Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh Panam Moochu Dhinam Selavaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazh Panam Moochu Dhinam Selavaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maegamaai Parakka Nee Oothu Voovae Saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maegamaai Parakka Nee Oothu Voovae Saala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathai Thirakka Nee Oodhu Voovae Saala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathai Thirakka Nee Oodhu Voovae Saala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vuvasella Oothu Rules Kidaiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuvasella Oothu Rules Kidaiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhiyattum Kaadhu Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhiyattum Kaadhu Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poharappo Beetu Kelaputhu Heatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poharappo Beetu Kelaputhu Heatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiruthu Streetu Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthu Streetu Yo"/>
</div>
</pre>
