---
title: "oru sudidhar poo song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Oru Sudidhar Poo"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/D6_o-z6yEpY"
type: "love"
singers:
  - Haricharan
  - Dr. Lavanya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru sudithaar vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sudithaar vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">enai thottuppoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thottuppoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannam rendil highkoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannam rendil highkoo "/>
</div>
<div class="lyrico-lyrics-wrapper">kavidhai sollippoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavidhai sollippoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sudithaar vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sudithaar vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">enai thottuppoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thottuppoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannam rendil highkoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannam rendil highkoo "/>
</div>
<div class="lyrico-lyrics-wrapper">kavidhai sollippoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavidhai sollippoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sudithaar vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sudithaar vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">enai thottuppoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thottuppoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannam rendil highkoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannam rendil highkoo "/>
</div>
<div class="lyrico-lyrics-wrapper">kavidhai sollippoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavidhai sollippoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa arugey vaazhvin poruley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa arugey vaazhvin poruley"/>
</div>
<div class="lyrico-lyrics-wrapper">aindhadi uyara azhagu nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aindhadi uyara azhagu nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradi ennai parugidum vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi ennai parugidum vizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un sella punnagai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sella punnagai "/>
</div>
<div class="lyrico-lyrics-wrapper">enai kondru poanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai kondru poanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi unaikkandu boomi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unaikkandu boomi "/>
</div>
<div class="lyrico-lyrics-wrapper">pandhu nindruppoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandhu nindruppoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sudithaar vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sudithaar vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">enai thottuppoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thottuppoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannam rendil highkoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannam rendil highkoo "/>
</div>
<div class="lyrico-lyrics-wrapper">kavidhai sollippoanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavidhai sollippoanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppadhu naalum pounami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai thirudi"/>
</div>
<div class="lyrico-lyrics-wrapper">anbaai varudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbaai varudi"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhippesumboadhu noayaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhippesumboadhu noayaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">viral theendumboadhu marundhaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral theendumboadhu marundhaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi unnai seravey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unnai seravey "/>
</div>
<div class="lyrico-lyrics-wrapper">en aavi thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aavi thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada aasai nenjil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada aasai nenjil "/>
</div>
<div class="lyrico-lyrics-wrapper">Osaiyindri aavi adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osaiyindri aavi adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Muppadhu naalum pounami"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Muppadhu naalum pounami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Muppadhu naalum pounami"/>
</div>
</pre>
