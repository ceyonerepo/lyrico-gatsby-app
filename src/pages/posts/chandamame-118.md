---
title: "chandamame song lyrics"
album: "118"
artist: "Shekar Chandra"
lyricist: "Ramanjaneyulu"
director: "K.V. Guhan"
path: "/albums/118-lyrics"
song: "Chandamame"
image: ../../images/albumart/118.jpg
date: 2019-03-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/W2DMBYJnGLg"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chandamame chethikande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamame chethikande"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelemo mabbulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelemo mabbulone"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolachette kallamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolachette kallamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulemo kommapaine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulemo kommapaine"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthoone enthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthoone enthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithene enti thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithene enti thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathikella bramhachari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathikella bramhachari"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadha choodavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadha choodavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli date eppudantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli date eppudantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkalesi choosukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkalesi choosukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance cheyyaniyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance cheyyaniyyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh my God em chesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God em chesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Check icchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Check icchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhakaanni aapesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhakaanni aapesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my God munchesaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God munchesaav"/>
</div>
<div class="lyrico-lyrics-wrapper">iPhone icchi screen lock esaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iPhone icchi screen lock esaav"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chethilona cheyyivesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethilona cheyyivesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata neeku isthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata neeku isthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadiana ninnu veedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadiana ninnu veedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhamaina poneenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhamaina poneenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kallalo hu hu hu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kallalo hu hu hu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpukunna nee roopaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpukunna nee roopaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppa moosina naalolo nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppa moosina naalolo nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema ante iddaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema ante iddaraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkaralle puttukele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkaralle puttukele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandamame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamame"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamame chenthanundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamame chenthanundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelemo mabbulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelemo mabbulone"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolachette kallamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolachette kallamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulemo kommapaine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulemo kommapaine"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthoone enthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthoone enthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithene enti thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithene enti thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathikella bramhachari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathikella bramhachari"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadha choodavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadha choodavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli date eppudantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli date eppudantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkalesi choosukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkalesi choosukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance cheyyaniyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance cheyyaniyyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh my God em chesaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God em chesaav"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha bike icchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha bike icchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalamemo dhaachesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalamemo dhaachesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my God munchesaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God munchesaav"/>
</div>
<div class="lyrico-lyrics-wrapper">ATM icchi no cash board ettaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ATM icchi no cash board ettaav"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu unna chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu unna chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu kooda eeroje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu kooda eeroje"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu vellu baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu vellu baata"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola thota ayyele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola thota ayyele"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalanduko hu hu hu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalanduko hu hu hu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilona thelaalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilona thelaalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi andhuko aa megam paike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi andhuko aa megam paike"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaramalle maaripoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaramalle maaripoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nenu cherchuthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nenu cherchuthaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandamame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamame"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamame chethikande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamame chethikande"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelemo mabbulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelemo mabbulone"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolachette kallamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolachette kallamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulemo kommapaine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulemo kommapaine"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthoone enthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthoone enthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakithene enti thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakithene enti thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathikella bramhachari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathikella bramhachari"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadha choodavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadha choodavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli date eppudantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli date eppudantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkalesi choosukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkalesi choosukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance cheyyaniyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance cheyyaniyyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh my God em chesaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God em chesaav"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha bike icchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha bike icchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalamemo dhaachesav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalamemo dhaachesav"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my God munchesaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my God munchesaav"/>
</div>
<div class="lyrico-lyrics-wrapper">ATM icchi no cash board ettaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ATM icchi no cash board ettaav"/>
</div>
</pre>
