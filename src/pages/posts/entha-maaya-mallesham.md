---
title: "entha maaya song lyrics"
album: "Mallesham"
artist: "Mark K Robin"
lyricist: "Ashok Peddinti"
director: "Raj R"
path: "/albums/mallesham-lyrics"
song: "Entha Maaya"
image: ../../images/albumart/mallesham.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YeMX5VlndsI"
type: "melody"
singers:
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">entha maaya galladee batta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha maaya galladee batta"/>
</div>
<div class="lyrico-lyrics-wrapper">nava naadulanugoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nava naadulanugoni "/>
</div>
<div class="lyrico-lyrics-wrapper">padugu pekalu varasagaa jutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padugu pekalu varasagaa jutta"/>
</div>
<div class="lyrico-lyrics-wrapper">panchaboothamu lothi vidhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panchaboothamu lothi vidhi "/>
</div>
<div class="lyrico-lyrics-wrapper">thanenchagaanolunda jesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanenchagaanolunda jesi"/>
</div>
<div class="lyrico-lyrics-wrapper">panchaboothamu lothi vidhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panchaboothamu lothi vidhi "/>
</div>
<div class="lyrico-lyrics-wrapper">thanenchagaanolunda jesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanenchagaanolunda jesi"/>
</div>
<div class="lyrico-lyrics-wrapper">garbhmanu moggamu meeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garbhmanu moggamu meeda"/>
</div>
<div class="lyrico-lyrics-wrapper">brahma nesenu navamaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brahma nesenu navamaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">entha maaya galladee batta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha maaya galladee batta"/>
</div>
<div class="lyrico-lyrics-wrapper">nava naadulanugoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nava naadulanugoni "/>
</div>
<div class="lyrico-lyrics-wrapper">padugu pekalu varasagaa jutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padugu pekalu varasagaa jutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">araaru mooralu edu geeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araaru mooralu edu geeralu"/>
</div>
<div class="lyrico-lyrics-wrapper">moora mooraku maaru rangulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moora mooraku maaru rangulu"/>
</div>
<div class="lyrico-lyrics-wrapper">araaru mooralu edu geeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araaru mooralu edu geeralu"/>
</div>
<div class="lyrico-lyrics-wrapper">moora mooraku maaru rangulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moora mooraku maaru rangulu"/>
</div>
<div class="lyrico-lyrics-wrapper">erupu nalupu neeli telupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erupu nalupu neeli telupu"/>
</div>
<div class="lyrico-lyrics-wrapper">pasidi kunkuma ranguladduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasidi kunkuma ranguladduka"/>
</div>
<div class="lyrico-lyrics-wrapper">ledi kari mandooka makaraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ledi kari mandooka makaraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">pannendu dalamula addakamutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannendu dalamula addakamutho"/>
</div>
<div class="lyrico-lyrics-wrapper">kamala poovulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamala poovulu"/>
</div>
<div class="lyrico-lyrics-wrapper">batta choopuku mandhamundunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="batta choopuku mandhamundunu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogu poguna randhramundunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogu poguna randhramundunu"/>
</div>
<div class="lyrico-lyrics-wrapper">batta choopuku mandhamundunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="batta choopuku mandhamundunu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogu poguna randhramundunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogu poguna randhramundunu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhyanamuna naanesi utikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhyanamuna naanesi utikina"/>
</div>
<div class="lyrico-lyrics-wrapper">battalo brahmaandamundunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="battalo brahmaandamundunu"/>
</div>
<div class="lyrico-lyrics-wrapper">surya chandrulu anchulandhundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surya chandrulu anchulandhundu"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevudu devuduga batalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevudu devuduga batalone"/>
</div>
<div class="lyrico-lyrics-wrapper">maaru thaanundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaru thaanundu "/>
</div>
<div class="lyrico-lyrics-wrapper">maayi maayaa galladee batta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayi maayaa galladee batta"/>
</div>
<div class="lyrico-lyrics-wrapper">maayalanu telisi mayakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayalanu telisi mayakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">madutha gaa betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madutha gaa betta"/>
</div>
<div class="lyrico-lyrics-wrapper">batta marmam telisi bathikite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="batta marmam telisi bathikite"/>
</div>
<div class="lyrico-lyrics-wrapper">brahma yogam kalla mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brahma yogam kalla mundu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadu ledani bhramala bathikithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadu ledani bhramala bathikithe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaali pidikedu boodidhagunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaali pidikedu boodidhagunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maayi maayaa galladee batta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayi maayaa galladee batta"/>
</div>
<div class="lyrico-lyrics-wrapper">maayalanu telisi mayakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayalanu telisi mayakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">madutha gaa betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madutha gaa betta"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakunda madutha gaa betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakunda madutha gaa betta"/>
</div>
</pre>
