---
title: "poraatame song lyrics"
album: "HIT - The First Case"
artist: "Vivek Sagar"
lyricist: "Krishna Kanth"
director: "Sailesh Kolanu"
path: "/albums/hit-the-first-case-lyrics"
song: "Poraatame"
image: ../../images/albumart/hit-the-first-case.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4EY-qVKycTY"
type: "mass"
singers:
  - David Simon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Uppeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Uppeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Dookenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dookenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Meedake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Meedake"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Galinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Galinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Malinchana Inko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malinchana Inko"/>
</div>
<div class="lyrico-lyrics-wrapper">vaipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Theesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Theesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thosesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thosesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamande Daatesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamande Daatesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paikochene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paikochene"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Rekkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Rekkale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthati Satruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthati Satruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Thagganika Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Thagganika Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kshana Kshana Savale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana Kshana Savale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesiddame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesiddame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara Naramulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara Naramulona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Chere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Chere"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyandile Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyandile Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ventade Mruthyuve Asale Longadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventade Mruthyuve Asale Longadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Parugu Theesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Parugu Theesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekesi Korale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekesi Korale"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayana Oh Kotha Rathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayana Oh Kotha Rathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakasi CHeekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakasi CHeekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadule Apadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadule Apadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Bediripothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Bediripothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chacheti Korale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacheti Korale"/>
</div>
<div class="lyrico-lyrics-wrapper">Longava nE Poddunaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Longava nE Poddunaithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalopale Dagundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalopale Dagundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Lokame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Lokame"/>
</div>
<div class="lyrico-lyrics-wrapper">Korindhile Povalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korindhile Povalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Daare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Daare"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigeti Gadiyarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigeti Gadiyarana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagiponi Mullayyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagiponi Mullayyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipedhi Evaro Telusthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipedhi Evaro Telusthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalannape Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalannape Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthati Satruvu Ayina Thagganika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthati Satruvu Ayina Thagganika"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshana Kshana Savale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana Kshana Savale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesiddame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesiddame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara Naramulona Kopam CHere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara Naramulona Kopam CHere"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyandhile Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyandhile Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ventade Mruthyuve Asale Longadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventade Mruthyuve Asale Longadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Parugu Theesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Parugu Theesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekesi KOrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekesi KOrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayana Oh Kotha Rathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayana Oh Kotha Rathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakasi CHeekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakasi CHeekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Daadule Aapadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daadule Aapadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Bediripothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Bediripothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chacheti Korale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacheti Korale"/>
</div>
<div class="lyrico-lyrics-wrapper">Longava Ne Poddunaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Longava Ne Poddunaithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In D City Of HUrricanes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In D City Of HUrricanes"/>
</div>
<div class="lyrico-lyrics-wrapper">I wil I wil Abstain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wil I wil Abstain"/>
</div>
<div class="lyrico-lyrics-wrapper">Frm MY Wrath For ur Spite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frm MY Wrath For ur Spite"/>
</div>
<div class="lyrico-lyrics-wrapper">C D C D Bloodstains
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="C D C D Bloodstains"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraatame"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthati Satruvu Ayina Thagganika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthati Satruvu Ayina Thagganika"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kShana Kshana Savale NEsiddame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kShana Kshana Savale NEsiddame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara Naramulona Kopam Chere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara Naramulona Kopam Chere"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyandhile Poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyandhile Poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ventade Mruthyuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventade Mruthyuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Asale Longadhu Ila Parugu THeesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asale Longadhu Ila Parugu THeesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekesi Korale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekesi Korale"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayana Oh Kotha Rathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayana Oh Kotha Rathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakasi Cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakasi Cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadule Aapadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadule Aapadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Bediripothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Bediripothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chacheti KOrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacheti KOrale"/>
</div>
<div class="lyrico-lyrics-wrapper">Longava Ne Poddunaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Longava Ne Poddunaithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In D CIty Of Hurricanes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In D CIty Of Hurricanes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will I will Abstain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will I will Abstain"/>
</div>
<div class="lyrico-lyrics-wrapper">Frm My Wrath For Ur SPite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frm My Wrath For Ur SPite"/>
</div>
<div class="lyrico-lyrics-wrapper">C D C D Bloodstains
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="C D C D Bloodstains"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In D CIty OF Hurricanes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In D CIty OF Hurricanes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will I WIll Abstain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will I WIll Abstain"/>
</div>
<div class="lyrico-lyrics-wrapper">Frm My Wrath Fr Ur Spite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frm My Wrath Fr Ur Spite"/>
</div>
<div class="lyrico-lyrics-wrapper">C d C D Bloodstains
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="C d C D Bloodstains"/>
</div>
</pre>
