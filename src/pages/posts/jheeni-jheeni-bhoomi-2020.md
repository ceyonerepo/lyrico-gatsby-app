---
title: "jheeni jheeni song lyrics"
album: "Bhoomi 2020"
artist: "Salim Sulaiman"
lyricist: "Dhiren Garg - Sant Kabir Das"
director: "Shakti Hasija"
path: "/albums/bhoomi-2020-lyrics"
song: "Jheeni Jheeni"
image: ../../images/albumart/bhoomi-2020.jpg
date: 2020-12-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/iU1HkDZDzbU"
type: "happy"
singers:
  - Jonita Gandhi
  - Swaroop Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Leaning on the edge and I wonder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaning on the edge and I wonder"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The things I left unsaid, I still wonder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The things I left unsaid, I still wonder"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Calling out your name
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calling out your name"/>
</div>
<div class="lyrico-lyrics-wrapper">But I don't believe that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I don't believe that"/>
</div>
<div class="lyrico-lyrics-wrapper">Things would be the same again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Things would be the same again"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feelings in the way oh I can't explain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feelings in the way oh I can't explain"/>
</div>
<div class="lyrico-lyrics-wrapper">It's better not to say aloud
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's better not to say aloud"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jheeni jheeni beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jheeni jheeni beeni chadariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jheeni jheeni beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jheeni jheeni beeni chadariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadaa ram ras beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadaa ram ras beeni chadariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadaa ram ras beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadaa ram ras beeni chadariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jheeni jheeni beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jheeni jheeni beeni chadariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jheeni jheeni beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jheeni jheeni beeni chadariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I'm tearing apart and I need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm tearing apart and I need"/>
</div>
<div class="lyrico-lyrics-wrapper">Someone to hear me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Someone to hear me now"/>
</div>
<div class="lyrico-lyrics-wrapper">Stitch the pieces of my heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stitch the pieces of my heart"/>
</div>
<div class="lyrico-lyrics-wrapper">With your love if you can hear me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With your love if you can hear me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Searching for a sign
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Searching for a sign"/>
</div>
<div class="lyrico-lyrics-wrapper">That I'm gonna make it,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That I'm gonna make it,"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby I've been reaching out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby I've been reaching out"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trying to carry on,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trying to carry on,"/>
</div>
<div class="lyrico-lyrics-wrapper">But I'm scared of breaking,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I'm scared of breaking,"/>
</div>
<div class="lyrico-lyrics-wrapper">Need someone to show me how
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Need someone to show me how"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jheeni jheeni jheeni jheeni..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jheeni jheeni jheeni jheeni.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hear me now.. Ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hear me now.. Ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Sadaa ram ras beeni chadariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadaa ram ras beeni chadariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I'm calling out to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm calling out to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm calling out to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm calling out to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you hear me now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you hear me now?"/>
</div>
</pre>
