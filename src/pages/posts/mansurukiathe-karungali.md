---
title: "mansurukiathe song lyrics"
album: "Karungali"
artist: "Srikanth Deva"
lyricist: "Erodu Iraivan"
director: "Kalanjiyam"
path: "/albums/karungali-lyrics"
song: "Mansurukiathe"
image: ../../images/albumart/karungali.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2D3oV8eglA8"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mansurukiathe udal melikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansurukiathe udal melikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppathaan oorariya seruvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppathaan oorariya seruvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam thodarkirathe, aasa vazhkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam thodarkirathe, aasa vazhkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppathaan thazhi katti vazhuvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppathaan thazhi katti vazhuvatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi pesi unna pathi kettu pochu en puthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi pesi unna pathi kettu pochu en puthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla pacha kuthi nikirene unna suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla pacha kuthi nikirene unna suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamellam irundhidanum unkoodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamellam irundhidanum unkoodathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagi vandha vazhkai ellam poiya poguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagi vandha vazhkai ellam poiya poguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa thotta nenapu ellam maraka mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa thotta nenapu ellam maraka mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pola nee enna kalandhiruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pola nee enna kalandhiruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madisaaya kadaisi vara kathirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madisaaya kadaisi vara kathirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha saami irundha enna unna suthuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha saami irundha enna unna suthuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha varamum vendam ennaku unn ketkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha varamum vendam ennaku unn ketkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">EN usuraga nee vandhu irundhidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="EN usuraga nee vandhu irundhidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanna naa kanna moodidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanna naa kanna moodidanum"/>
</div>
</pre>
