---
title: "sirika vechu song lyrics"
album: "Shivalinga"
artist: "S S Thaman"
lyricist: "Viveka"
director: "P Vasu"
path: "/albums/shivalinga-lyrics"
song: "Sirika Vechu"
image: ../../images/albumart/shivalinga.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fzsGpyBMySM"
type: "love"
singers:
  -	Vijay Yesudas
  - Ramya Behra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sirikka Vechu Sirikka Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Vechu Sirikka Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragadichu Parakka Vekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragadichu Parakka Vekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Rasa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Rasa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthiruken Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthiruken Munnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenachivecha Kavalaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachivecha Kavalaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishathula Odi Poogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishathula Odi Poogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu Rasa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Rasa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ethira Vandhu Ninnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethira Vandhu Ninnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Butterfly Nee Parandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Butterfly Nee Parandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattamaram Pookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattamaram Pookume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambalainga Nagakkannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambalainga Nagakkannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annandhu Pakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annandhu Pakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhiran Chandhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhiran Chandhiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikkatti Unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkatti Unnidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevagam Puriya Vaikavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevagam Puriya Vaikavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Z Il Thodangi A Vil Mudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Z Il Thodangi A Vil Mudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Englisha Maththava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Englisha Maththava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikka Vechu Sirikka Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Vechu Sirikka Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragadichu Parakka Vekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragadichu Parakka Vekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Rasa Vanthiruken Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Rasa Vanthiruken Munnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagathoda Nirathai Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagathoda Nirathai Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghathoda Nirathai Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghathoda Nirathai Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhu Yeduthu Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhu Yeduthu Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalukku Mai Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalukku Mai Tharavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathai Nimithi Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathai Nimithi Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigai Thanni Nirappi Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai Thanni Nirappi Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kulichu Magizha Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kulichu Magizha Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neechal Kolam Amechidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neechal Kolam Amechidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannathula Kaiya Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula Kaiya Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkarave Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkarave Koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannathu Poochi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathu Poochi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattamadi Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattamadi Ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathadi Naanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadi Naanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Athadi Aanaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Athadi Aanaiyidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiye Senchiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiye Senchiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikka Vechu Sirikka Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Vechu Sirikka Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragadichu Parakka Vekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragadichu Parakka Vekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Rasa Vanthiruken Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Rasa Vanthiruken Munnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naradhar Isaiamaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naradhar Isaiamaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkeeran Pattezhudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkeeran Pattezhudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagi Unai Pugazhandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagi Unai Pugazhandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnani Padattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnani Padattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalankatti Yeduthu Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalankatti Yeduthu Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranmanai Vadivamachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai Vadivamachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nadakkum Paathaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadakkum Paathaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathiram Thelikkattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathiram Thelikkattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naan Katti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Katti Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thavam Seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thavam Seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu Mel Inbam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu Mel Inbam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sendha Neethaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sendha Neethaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaga Naan Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaga Naan Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakaalu Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakaalu Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthanathom Poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthanathom Poduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikka Vechu Sirikka Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Vechu Sirikka Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragadichu Parakka Vekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragadichu Parakka Vekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Rasa Vanthiruken Munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Rasa Vanthiruken Munnale"/>
</div>
</pre>
