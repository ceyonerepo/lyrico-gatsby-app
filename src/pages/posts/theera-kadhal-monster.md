---
title: "theera kadhal song lyrics"
album: "Monster"
artist: "Justin Prabhakaran"
lyricist: "Karthik Netha"
director: "Nelson Venkatesan"
path: "/albums/monster-lyrics"
song: "Theera Kadhal"
image: ../../images/albumart/monster.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HqoXSJ4Hpz0"
type: "love"
singers:
  - Sathya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theera Kaadhal Kaana Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Kaadhal Kaana Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thedal Yaavum Theera Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thedal Yaavum Theera Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Neram Maara Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Neram Maara Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalam Ondril Naanum Nindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalam Ondril Naanum Nindrene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave Nanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Nanavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maname Iragaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maname Iragaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suru Suru Suru Anilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suru Suru Suru Anilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Veettinil Anumadhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Veettinil Anumadhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siru Siru Unavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Siru Unavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu Piriyangal Samaithu Vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Piriyangal Samaithu Vaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adupparaiyinil Paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupparaiyinil Paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Velichathai Parisalippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Velichathai Parisalippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Arupperunchudar Ozhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arupperunchudar Ozhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Thanimaiyai Rasithiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Thanimaiyai Rasithiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Vaanam Poonai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Vaanam Poonai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi Ozhigindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi Ozhigindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Easy Chairil Saindhu Konde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Easy Chairil Saindhu Konde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Sirikkindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Sirikkindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedum Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedum Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhekkam Sengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhekkam Sengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottram Kondathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottram Kondathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhi Kaalaiyai Azhaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kaalaiyai Azhaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Rasithida Adam Pidippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Rasithida Adam Pidippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvarkozhiyin Ozhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvarkozhiyin Ozhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Isaiyena Layiththiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Isaiyena Layiththiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharai Vizhugira Ozhi Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai Vizhugira Ozhi Mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Nizhal Ena Paduthiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Nizhal Ena Paduthiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Narai Vizhugira Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narai Vizhugira Varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Araigalai Rasithiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Araigalai Rasithiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooral Yaavum Theerndha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral Yaavum Theerndha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerapadham Ullathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerapadham Ullathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Yaavum Kaana Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Yaavum Kaana Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchiyaagindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchiyaagindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathiruppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiruppil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevan Ullathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan Ullathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Jeevan Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jeevan Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal Nindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Nindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera Kaadhal Kaana Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Kaadhal Kaana Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thedal Yaavum Theera Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thedal Yaavum Theera Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Neram Maara Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Neram Maara Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalam Ondril Naanum Nindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalam Ondril Naanum Nindrene"/>
</div>
</pre>
