---
title: "madham madham song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Madham Madham"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/db-MRBRWbVM"
type: "motivational"
singers:
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">madham madham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madham madham"/>
</div>
<div class="lyrico-lyrics-wrapper">vena madham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vena madham"/>
</div>
<div class="lyrico-lyrics-wrapper">pidithalin pidu neri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidithalin pidu neri"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalidam irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalidam irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kulanthaigal bathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kulanthaigal bathiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraiva enai kapatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraiva enai kapatru"/>
</div>
<div class="lyrico-lyrics-wrapper">ena nan kekka matanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena nan kekka matanae"/>
</div>
<div class="lyrico-lyrics-wrapper">unn nilai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unn nilai than"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalai kidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalai kidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraiva unnai kapatri kol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraiva unnai kapatri kol"/>
</div>
<div class="lyrico-lyrics-wrapper">ena naan ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena naan ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilamai aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilamai aiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulae paathu bathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulae paathu bathiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">buthar vantha boomiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buthar vantha boomiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">ratham ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratham ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">matham enra peyarilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matham enra peyarilae"/>
</div>
<div class="lyrico-lyrics-wrapper">yutham yutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yutham yutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pagutharivu chemmal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagutharivu chemmal"/>
</div>
<div class="lyrico-lyrics-wrapper">aiya periyarin boomiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiya periyarin boomiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">paguthariyavillai enral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paguthariyavillai enral"/>
</div>
<div class="lyrico-lyrics-wrapper">paithiyam tan avom pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paithiyam tan avom pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbae sivam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbae sivam "/>
</div>
<div class="lyrico-lyrics-wrapper">athulae sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athulae sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">enru anbodu valntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enru anbodu valntha"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil inam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manithathai ethirthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithathai ethirthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">matham enil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matham enil"/>
</div>
<div class="lyrico-lyrics-wrapper">kolvomae sinthaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolvomae sinthaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">naam sinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam sinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irai aguthu pirayamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irai aguthu pirayamai"/>
</div>
<div class="lyrico-lyrics-wrapper">pennai adaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennai adaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ninapathila aanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninapathila aanmai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanthal seivan enru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthal seivan enru"/>
</div>
<div class="lyrico-lyrics-wrapper">solbavanai vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solbavanai vida"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai seithu vanthom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai seithu vanthom "/>
</div>
<div class="lyrico-lyrics-wrapper">enru solubavanai nambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enru solubavanai nambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vote tomorrow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vote tomorrow"/>
</div>
<div class="lyrico-lyrics-wrapper">but most importantly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="but most importantly"/>
</div>
<div class="lyrico-lyrics-wrapper">vote to the right person
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vote to the right person"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraivanai parvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivanai parvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">anaivarum samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaivarum samam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanmurai valarthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanmurai valarthal"/>
</div>
<div class="lyrico-lyrics-wrapper">pin ethuku intha matham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin ethuku intha matham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theeviravathi enra matha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeviravathi enra matha "/>
</div>
<div class="lyrico-lyrics-wrapper">thiraiku muthirai kuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiraiku muthirai kuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">arasiyal vathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasiyal vathi"/>
</div>
<div class="lyrico-lyrics-wrapper">nammai piditha vyathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai piditha vyathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athil nee entha jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil nee entha jaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">enru melum keelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enru melum keelum"/>
</div>
<div class="lyrico-lyrics-wrapper">manithanai prikum neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithanai prikum neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">epadi neethi agum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epadi neethi agum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu aneethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu aneethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">makkal aatchi nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkal aatchi nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">naatil rajiyangal ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatil rajiyangal ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">sarva rajiyangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarva rajiyangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thagarthu makkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagarthu makkal"/>
</div>
<div class="lyrico-lyrics-wrapper">atchi amaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atchi amaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">india en naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="india en naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">pala ina moligalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala ina moligalin"/>
</div>
<div class="lyrico-lyrics-wrapper">thogupu athil onrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thogupu athil onrai"/>
</div>
<div class="lyrico-lyrics-wrapper">alithu matraonrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alithu matraonrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pugatharivipathu etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugatharivipathu etharku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valarchikum vyabarathukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarchikum vyabarathukum"/>
</div>
<div class="lyrico-lyrics-wrapper">vithyasam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithyasam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">valarchi enbathu makkalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarchi enbathu makkalin"/>
</div>
<div class="lyrico-lyrics-wrapper">adipadiyai alika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipadiyai alika"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaipathu anru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaipathu anru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naangal arinthi kolvom inru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal arinthi kolvom inru"/>
</div>
<div class="lyrico-lyrics-wrapper">neengal purinthu kondal nanru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengal purinthu kondal nanru"/>
</div>
<div class="lyrico-lyrics-wrapper">maatram enru sonathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatram enru sonathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ematrathai enru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ematrathai enru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbulla athigariku neeyzum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbulla athigariku neeyzum"/>
</div>
<div class="lyrico-lyrics-wrapper">thamilar thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamilar thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai un pillaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai un pillaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">unna unavu thevai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna unavu thevai thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maasilatha kaatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maasilatha kaatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasilatha neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasilatha neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai un pillaigaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai un pillaigaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">illamal poga nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illamal poga nerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unarvu mattum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarvu mattum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">urimaiyum tan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urimaiyum tan"/>
</div>
<div class="lyrico-lyrics-wrapper">kan vizhlithu paar nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan vizhlithu paar nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">kan vizhilithu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan vizhilithu paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aakum kadavul engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakum kadavul engae"/>
</div>
<div class="lyrico-lyrics-wrapper">alikum kadavul engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alikum kadavul engae"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakum kadavul unthanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakum kadavul unthanai"/>
</div>
<div class="lyrico-lyrics-wrapper">thati kekkum kadavul engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thati kekkum kadavul engae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuvaraiyil kathai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvaraiyil kathai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhalai kannir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhalai kannir"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvilae iranthirpaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvilae iranthirpaen"/>
</div>
<div class="lyrico-lyrics-wrapper">yen veli vara choneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen veli vara choneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul undu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul undu "/>
</div>
<div class="lyrico-lyrics-wrapper">mathangal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathangal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">enru nambikai kol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enru nambikai kol"/>
</div>
<div class="lyrico-lyrics-wrapper">anaal un arivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaal un arivu"/>
</div>
<div class="lyrico-lyrics-wrapper">knnai maraithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="knnai maraithal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru manithanai sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru manithanai sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraivanin parvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivanin parvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">anaivarum samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaivarum samam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanmurai valarthal pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanmurai valarthal pin"/>
</div>
<div class="lyrico-lyrics-wrapper">etharku intha matham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharku intha matham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaathum oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaathum oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavarum kelir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavarum kelir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">humanity is all we need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="humanity is all we need"/>
</div>
<div class="lyrico-lyrics-wrapper">god is love and 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="god is love and "/>
</div>
<div class="lyrico-lyrics-wrapper">god is pease
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="god is pease"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">then why are you killing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then why are you killing"/>
</div>
<div class="lyrico-lyrics-wrapper">juvenile over religion huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="juvenile over religion huh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">then why are you killing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then why are you killing"/>
</div>
<div class="lyrico-lyrics-wrapper">juvenile over religion huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="juvenile over religion huh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">humanity is all we need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="humanity is all we need"/>
</div>
<div class="lyrico-lyrics-wrapper">god is love and 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="god is love and "/>
</div>
<div class="lyrico-lyrics-wrapper">god is pease
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="god is pease"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaathum oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaathum oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavarum kelir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavarum kelir"/>
</div>
</pre>
