---
title: "hey manasendukila song lyrics"
album: "Ichata Vahanamulu Niluparadu"
artist: "Praveen Lakkaraju"
lyricist: "Sreejo"
director: "S Darshan"
path: "/albums/ichata-vahanamulu-niluparadu-lyrics"
song: "Hey Manasendukila"
image: ../../images/albumart/ichata-vahanamulu-niluparadu.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YWXPrPmJQMs"
type: "love"
singers:
  - Armaan Malik
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey manasendukila nilichina chotika niluvadugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey manasendukila nilichina chotika niluvadugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanulaki bahushaa emaindho telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanulaki bahushaa emaindho telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa pedavulu chese maayaki maatalu chaalavika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa pedavulu chese maayaki maatalu chaalavika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa nadakalu nanne cheraka maanavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa nadakalu nanne cheraka maanavugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arakshanamu undadhu thinnagaa praanamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakshanamu undadhu thinnagaa praanamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadi padi ninu vidadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadi padi ninu vidadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi vini gundelanaapina dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi vini gundelanaapina dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Melamellagaa kariginadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melamellagaa kariginadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daggaraina koddhi dhorakka jaaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggaraina koddhi dhorakka jaaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeli kalla thoti korakka maanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli kalla thoti korakka maanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasha theerakunte ekantham endhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha theerakunte ekantham endhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamu kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraadakunte ee kougilendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraadakunte ee kougilendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha korikante oo gunde chaaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha korikante oo gunde chaaladu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema ponguthoone pedhaalu dhaachaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema ponguthoone pedhaalu dhaachaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapadavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthagaano nannu nenu aapukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthagaano nannu nenu aapukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chentha cheramantu saiga chesthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chentha cheramantu saiga chesthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatalaaduthoone okkatai kalise manasulive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatalaaduthoone okkatai kalise manasulive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa chempa gilluthunte nee choopu challagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa chempa gilluthunte nee choopu challagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde andhukundhe kerintha kotthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde andhukundhe kerintha kotthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukomanante aagedhi kaadhugaa madhi saradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukomanante aagedhi kaadhugaa madhi saradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppaleka neetho manassu dhaachagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppaleka neetho manassu dhaachagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalochhinattu vayassu golagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalochhinattu vayassu golagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkamaatathone theesindhi sootigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkamaatathone theesindhi sootigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi paradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi paradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhukani undaneevu nannu oorike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukani undaneevu nannu oorike"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalaadi maayaloki thosthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalaadi maayaloki thosthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha muddhugundhi dhaggaravuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha muddhugundhi dhaggaravuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jagame"/>
</div>
</pre>
