---
title: "paniye pani poove song lyrics"
album: "Rajapattai"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/rajapattai-lyrics"
song: "Paniye Pani Poove"
image: ../../images/albumart/rajapattai.jpg
date: 2011-12-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vWgH-0J6Hg0"
type: "love"
singers:
  - Javed Ali
  - Renu Kannan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh paniyae pani poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh paniyae pani poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manameno parakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manameno parakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaal puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaal puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthu saami aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthu saami aadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire uyir theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire uyir theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal polae kothikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal polae kothikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae theriyaamal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae theriyaamal unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu moochu vaangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu moochu vaangudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethaalam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethaalam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velai seiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velai seiyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengo thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengo thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae yeradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae yeradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu adikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu adikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju vedikudhae ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju vedikudhae ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothikudhae pei pol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothikudhae pei pol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi tholikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi tholikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam sevakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam sevakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondi izhukudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondi izhukudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noipol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noipol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paniyae pani poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyae pani poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manameno parakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manameno parakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaal puriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaal puriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthu saami aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthu saami aadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Aa kannae nee kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kannae nee kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaal karam kallaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaal karam kallaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unpaarvai ennai theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpaarvai ennai theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai kallaaga aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai kallaaga aanenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh anbae nee pesum munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh anbae nee pesum munnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sama makaana aal naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sama makaana aal naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechai keta pinnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechai keta pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu book aagiponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu book aagiponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthenae munbu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthenae munbu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam thelivaaga oru yogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam thelivaaga oru yogi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru naan unnai ninaithaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru naan unnai ninaithaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell engum vinmeenthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell engum vinmeenthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu adikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu adikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju vedikudhae ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju vedikudhae ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothikudhae pei pol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothikudhae pei pol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi tholikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi tholikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam sevakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam sevakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondi izhukudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondi izhukudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noipol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noipol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh hoo mulvelikullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hoo mulvelikullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadum pullaanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum pullaanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un anbil naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un anbil naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani naadaagi povenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani naadaagi povenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomiku eerkum sakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiku eerkum sakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yaararo aaraainthaargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yaararo aaraainthaargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un eerpai sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un eerpai sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Palar appovae saainthaargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palar appovae saainthaargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal etharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal etharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivomae kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivomae kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal etharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal etharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivomae kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivomae kaaranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam kalanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam kalanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharku illai kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharku illai kaaranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu adikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu adikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju vedikudhae ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju vedikudhae ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothikudhae pei pol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothikudhae pei pol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathi tholikudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi tholikudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam sevakudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam sevakudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondi izhukudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondi izhukudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noipol kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noipol kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho ho"/>
</div>
</pre>
