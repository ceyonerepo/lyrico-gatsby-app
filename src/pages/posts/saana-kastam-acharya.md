---
title: "saana kastam song lyrics"
album: "Acharya"
artist: "Manisharma"
lyricist: "Bhaskarabhatla"
director: "Koratala Siva"
path: "/albums/acharya-lyrics"
song: "Saana Kastam"
image: ../../images/albumart/acharya.jpg
date: 2022-04-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Kn42r5UUhqE"
type: "happy"
singers:
  - Revanth
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kallolam Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Vaada Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Vaada Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenosthe Alla Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenosthe Alla Kallolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallolam Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinda Meedha Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinda Meedha Kallolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Navvu Alla Kallolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Navvu Alla Kallolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Jadaganthalu Ooge Koddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jadaganthalu Ooge Koddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">O Aragantalo Perige Raddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Aragantalo Perige Raddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaga Dhagala Vayyaaraanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga Dhagala Vayyaaraanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachipettedi Yettaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachipettedi Yettaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saana Kastam Saana Kashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kastam Saana Kashtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kastam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kastam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosevaalla Kallu Kaakuletthuku Poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosevaalla Kallu Kaakuletthuku Poni"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nadum Madathalona Janam Nalige Poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadum Madathalona Janam Nalige Poni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kolathe Chudaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kolathe Chudaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathodu Tailor’la Ayipothade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathodu Tailor’la Ayipothade"/>
</div>
<div class="lyrico-lyrics-wrapper">O Nijanga Bhale Baagunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Nijanga Bhale Baagunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Moolanga Oka Pani Dorikindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Moolanga Oka Pani Dorikindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaadedo Nimarocchanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadedo Nimarocchanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurralle RMPLU Avuthunnare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurralle RMPLU Avuthunnare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Idhedho Koncham Thedagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Idhedho Koncham Thedagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Abaddham Kuda Andhangundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Abaddham Kuda Andhangundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illu Dhaatithe Ibbandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Dhaatithe Ibbandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vompu Sompultho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vompu Sompultho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Papam Saana Kashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Papam Saana Kashtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kastam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kastam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Antinchake Andala Agarotthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antinchake Andala Agarotthini"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanammatho Theeyinchei Nara Dhishtini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanammatho Theeyinchei Nara Dhishtini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yeah Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Young Young Yammayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Young Young Yammayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yeah Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Young Young Yammayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Young Young Yammayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Payita Pinneesuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Payita Pinneesuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhento Villain’la Choosthuntaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhento Villain’la Choosthuntaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Level Lo Pose Eduthunnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Level Lo Pose Eduthunnaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Chevullo Pooleduthunnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Chevullo Pooleduthunnaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daabale Ekkestaare Peratlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daabale Ekkestaare Peratlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamme Nalugeduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamme Nalugeduthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kahaani Maaku Endhuku Cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kahaani Maaku Endhuku Cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mem’u Vintunam Ani Kottake Dappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mem’u Vintunam Ani Kottake Dappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gampa Gutthugaa Sokultho Yetta Vegaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gampa Gutthugaa Sokultho Yetta Vegaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Ara Rey Saana Kashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Ara Rey Saana Kashtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchayithi Lettodhe Vaddoddani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchayithi Lettodhe Vaddoddani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana Kashtam Vacchindhe Mandakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana Kashtam Vacchindhe Mandakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Acchoo Bomma Aatadinchu Yaavatthuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acchoo Bomma Aatadinchu Yaavatthuni"/>
</div>
</pre>
