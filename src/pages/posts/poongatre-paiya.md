---
title: 'poongatre poongatre song lyrics'
album: 'Paiya'
artist: 'Yuvan Shankar Raja'
lyricist: 'Na Muthukumar'
director: 'N.Lingusamy'
path: '/albums/paiya-song-lyrics'
song: 'Poongatre Poongatre'
image: ../../images/albumart/paiya.jpg
date: 2010-04-02
lang: tamil
singers: 
- Benny Dayal
youtubeLink: "https://www.youtube.com/embed/pbGTuJiC7bI"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Poongaatrae poongaatrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongaatrae poongaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola vandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Poopola vandhaal ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindra vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindra vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham thandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham thandhaal ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En nenjodu veesum
<input type="checkbox" class="lyrico-select-lyric-line" value="En nenjodu veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ponnoda paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha ponnoda paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival kannodu pookkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival kannodu pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala vinmeengal pesum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pala vinmeengal pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kaadhal solla
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaarthai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannukkullae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kannukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini kanavae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini kanavae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poongaatrae poongaatrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongaatrae poongaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola vandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Poopola vandhaal ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindra vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindra vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham thandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham thandhaal ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manjal vaanam konjam megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Manjal vaanam konjam megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjipesum kaatru thottu selludhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjipesum kaatru thottu selludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthaamal sirikkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Niruthaamal sirikkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nimidangal punnagaiyai
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha nimidangal punnagaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottikkondadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poottikkondadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannaadi sari seidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaadi sari seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaadi un kannai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinnaadi un kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkindren paarkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarkindren paarkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae naan unn munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae naan unn munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaarthai pesaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaarthai pesaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorkkindren thorkkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Thorkkindren thorkkindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vazhippokkan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhippokkan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil kaaladi thadam irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyil kaaladi thadam irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyilae indha nodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkaiyilae indha nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanaiyodu ninaivirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaasanaiyodu ninaivirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poongaatrae poongaatrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongaatrae poongaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola vandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Poopola vandhaal ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindra vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindra vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham thandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham thandhaal ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Azhagaana nadhi paarthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaana nadhi paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan peyarinai ketka manam thudikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhan peyarinai ketka manam thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival yaaro enna pero
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival yaaro enna pero"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai arindhidum varayil oru mayakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhai arindhidum varayil oru mayakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhedho oor thaandi eraalam perthaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhedho oor thaandi eraalam perthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindren pogindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindren pogindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillendru solgindra nedunjaalai vilakkaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillendru solgindra nedunjaalai vilakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaigindren erigindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Anaigindren erigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mozhi theriyaa paadalilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mozhi theriyaa paadalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arthangal indru purigiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Arthangal indru purigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhithunaiyaai nee vandhaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhithunaiyaai nee vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum thooram kuraigiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum thooram kuraigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En nenjodu veesum
<input type="checkbox" class="lyrico-select-lyric-line" value="En nenjodu veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ponnoda paasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha ponnoda paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival kannodu pookkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival kannodu pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala vinmeengal pesum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pala vinmeengal pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kaadhal solla
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaarthai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannukkullae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kannukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini kanavae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini kanavae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poongaatrae poongaatrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poongaatrae poongaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola vandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Poopola vandhaal ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindra vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogindra vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham thandhaal ival
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham thandhaal ival"/>
</div>
</pre>