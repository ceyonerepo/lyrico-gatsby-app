---
title: "ilam poove song lyrics"
album: "Anveshanam"
artist: "Jakes Bejoy"
lyricist: "Joe Paul"
director: "Prasobh Vijayan"
path: "/albums/anveshanam-lyrics"
song: "Ilam poove"
image: ../../images/albumart/gauthamante-radham.jpg
date: 2020-01-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/BNYwvdQhULk"
type: "happy"
singers:
  - Sooraj Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ilam poove ennum neeyanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilam poove ennum neeyanen"/>
</div>
<div class="lyrico-lyrics-wrapper">lokham vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokham vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhathullil neeyanaavolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhathullil neeyanaavolam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru naalum ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru naalum ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">venal nullaale achan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venal nullaale achan "/>
</div>
<div class="lyrico-lyrics-wrapper">thanalaai nillillee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanalaai nillillee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathiye neeyo valaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathiye neeyo valaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil nee valaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil nee valaru"/>
</div>
<div class="lyrico-lyrics-wrapper">tholileraan melle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholileraan melle"/>
</div>
<div class="lyrico-lyrics-wrapper">madiyil ninnuyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madiyil ninnuyaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhooreyetho vazhikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooreyetho vazhikal"/>
</div>
<div class="lyrico-lyrics-wrapper">theliyaathaavumbol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theliyaathaavumbol"/>
</div>
<div class="lyrico-lyrics-wrapper">achan mizhi aayi vannille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achan mizhi aayi vannille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veyilennilla mazhayennillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilennilla mazhayennillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraai neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraai neelum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralaai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralaai ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaal mullil kondalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal mullil kondalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjullam nonthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjullam nonthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">achan karayaanundallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achan karayaanundallee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanmanee nee vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmanee nee vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">lokham kaanenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokham kaanenam"/>
</div>
<div class="lyrico-lyrics-wrapper">kunju kaaryam polum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunju kaaryam polum"/>
</div>
<div class="lyrico-lyrics-wrapper">arivaai maarenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arivaai maarenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholviyoronnekum theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholviyoronnekum theera"/>
</div>
<div class="lyrico-lyrics-wrapper">paadangal achan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadangal achan"/>
</div>
<div class="lyrico-lyrics-wrapper">parayaam kelkkillee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parayaam kelkkillee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evideyoo swapnangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evideyoo swapnangal"/>
</div>
<div class="lyrico-lyrics-wrapper">thedippoyaalum paithalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedippoyaalum paithalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ennil thirike porenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ennil thirike porenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanuvolam nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanuvolam nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyaneppozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyaneppozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">achan parayathorkkillee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achan parayathorkkillee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veyilennillaa mazhayennillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilennillaa mazhayennillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraai neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraai neelum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralaai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralaai ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaal mullil kondalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal mullil kondalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjullam nonthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjullam nonthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">achan karayaanundallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achan karayaanundallee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanoram vaazhaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanoram vaazhaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhathoronaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhathoronaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paarippovenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarippovenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaazheyaa nennorama venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaazheyaa nennorama venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhum kaanum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhum kaanum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">etho dhooratholam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho dhooratholam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam pinnil veezhumbozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam pinnil veezhumbozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaarum kolum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarum kolum"/>
</div>
<div class="lyrico-lyrics-wrapper">moodumbozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodumbozhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naamonnaai ninnaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamonnaai ninnaalee"/>
</div>
<div class="lyrico-lyrics-wrapper">novethum vannalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="novethum vannalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ore karuthode neridamennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore karuthode neridamennum"/>
</div>
</pre>
