---
title: 'quit pannuda song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Vignesh Sivan'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Quit Pannuda'
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/J4dvqRNQymU'
type: 'advice'
singers: 
- Anirudh Ravichander
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">En jeevanae en bodhaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En jeevanae en bodhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podhumendru thondrum neram dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee podhumendru thondrum neram dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae naan thalladiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaalae naan thalladiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kaalam aarum neram indru dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kaalam aarum neram indru dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adichathu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichathu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Out pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Out pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alunjathu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Alunjathu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Off pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Off pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichadhu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudichadhu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Quit pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Quit pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odachithaan podu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Odachithaan podu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki podu daa (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooki podu daa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Valiyaana nerathila
<input type="checkbox" class="lyrico-select-lyric-line" value="Valiyaana nerathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukka yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathukka yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandha pakkathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vandha pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda vaasathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda vaasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda paasathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda paasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulunthaen ulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulunthaen ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobam illa… un mela kobam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kobam illa… un mela kobam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne onnum paavam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ne onnum paavam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaana ne thevai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaana ne thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennatha naanum solla…. vaarthai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennatha naanum solla…. vaarthai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jorathila nadungina nerathila
<input type="checkbox" class="lyrico-select-lyric-line" value="Jorathila nadungina nerathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunthena nee dhaan irundha
<input type="checkbox" class="lyrico-select-lyric-line" value="Marunthena nee dhaan irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhichu naan attam potta
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudhichu naan attam potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamellaam enakenna nee dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalamellaam enakenna nee dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhoshamum sogathukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhoshamum sogathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathukkum kettathukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nallathukkum kettathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaan irundha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee dhaan irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga mudinjuthu un vela…ippo viduma aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga mudinjuthu un vela…ippo viduma aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adichathu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichathu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Out pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Out pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alunjathu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Alunjathu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Off pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Off pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichadhu podhum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudichadhu podhum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Quit pannu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Quit pannu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odachithaan podu daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Odachithaan podu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki podu daa (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooki podu daa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Inimae kudipiyaa…   Noo
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimae kudipiyaa…   Noo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaazhkaiyaa keduppiyaa   Maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkaiyaa keduppiyaa   Maaten"/>
</div>
  <div class="lyrico-lyrics-wrapper">Namakku idhu thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Namakku idhu thevaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sonna kedppiyaa   Yes Master (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa sonna kedppiyaa   Yes Master"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Inimae kudipiyaa…   Noo
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimae kudipiyaa…   Noo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaazhkaiyaa keduppiyaa   Maaten
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkaiyaa keduppiyaa   Maaten"/>
</div>
  <div class="lyrico-lyrics-wrapper">Namakku idhu thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Namakku idhu thevaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sonna kedppiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa sonna kedppiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sonna kedppiyaa kedppiyaa   Yes Master (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa sonna kedppiyaa kedppiyaa   Yes Master"/></div>
</div>
</pre>