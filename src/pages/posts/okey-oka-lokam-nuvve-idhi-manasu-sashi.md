---
title: "okey oka lokam song lyrics"
album: "Sashi"
artist: "Arun Chiluveru"
lyricist: "Chabdrabose"
director: "Srinivas Naidu Nadikatla"
path: "/albums/sashi-lyrics"
song: "Okey Oka Lokam Nuvve"
image: ../../images/albumart/sashi.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_PtH3QraTz0"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oke Oka Lokam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke Oka Lokam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Lona Andham Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Lona Andham Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaanike Hrudhayam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaanike Hrudhayam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Andhaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Andhaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekaa Eki Kopam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaa Eki Kopam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Lona Dheepam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Lona Dheepam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Leni Veluthuru Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Leni Veluthuru Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranaannila Veliginchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranaannila Veliginchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu Ninnuga Preminchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Ninnuga Preminchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nannugaa Andhinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nannugaa Andhinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Velalaa Thodundana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Velalaa Thodundana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Janma Janmalaa Jantavvanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Janma Janmalaa Jantavvanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oke Oka Lokam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke Oka Lokam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Lona Andham Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Lona Andham Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaanike Hrudhayam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaanike Hrudhayam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Andhaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Andhaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekaa Eki Kopam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaa Eki Kopam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Lona Dheepam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Lona Dheepam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Leni Veluthuru Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Leni Veluthuru Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranaannila Veliginchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranaannila Veliginchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu Ninnuga Preminchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Ninnuga Preminchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nannugaa Andhinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nannugaa Andhinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Velalaa Thodundana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Velalaa Thodundana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Janma Janmalaa Jantavvanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Janma Janmalaa Jantavvanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooh Kallathoti Nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Kallathoti Nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Kougilinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Kougilinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamantha Neeke Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamantha Neeke Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavalundanaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalundanaa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooh Kallathoti Nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Kallathoti Nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Kougilinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Kougilinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamantha Neeke Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamantha Neeke Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavalundanaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalundanaa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna Monna… Gurtheraani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Monna… Gurtheraani"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshaanne Pancheina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshaanne Pancheina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaallaina Gurthundeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaallaina Gurthundeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhamlo Muncheina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhamlo Muncheina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvule Sirimuvvaga Kattana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvule Sirimuvvaga Kattana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kshana Maina Kanabadakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana Maina Kanabadakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanamaagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamaagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaina Dhooram Velithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaina Dhooram Velithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraadadhe Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraadadhe Eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ende Neeku Thaakindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ende Neeku Thaakindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemata Naaku Pattene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemata Naaku Pattene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chale Ninnu Cherindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chale Ninnu Cherindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanuku Naaku Puttene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanuku Naaku Puttene"/>
</div>
<div class="lyrico-lyrics-wrapper">Deham Needhi Nee Praaname Nenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deham Needhi Nee Praaname Nenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oke Oka Lokam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke Oka Lokam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Lona Andham Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Lona Andham Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaanike Hrudhayam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaanike Hrudhayam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Andhaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Andhaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekaa Eki Kopam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaa Eki Kopam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Lona Dheepam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Lona Dheepam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Leni Veluthuru Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Leni Veluthuru Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranaannila Veliginchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranaannila Veliginchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu Ninnuga Preminchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Ninnuga Preminchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nannugaa Andhinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nannugaa Andhinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Velalaa Thodundana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Velalaa Thodundana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Janma Janmalaa Jantavvanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Janma Janmalaa Jantavvanaa"/>
</div>
</pre>
