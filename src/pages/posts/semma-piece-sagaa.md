---
title: "semma piece song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Semma Piece"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SIKx5uHWK2U"
type: "love"
singers:
  - Andrea Jeremiah
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">7299 022 344 Ennoda Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Number"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Nee Paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Nee Paru"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorana Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorana Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhalae Pongum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhalae Pongum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanga Kadal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga Kadal Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatta Kadichi Maatta Kadipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatta Kadichi Maatta Kadipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatta Kadichi Aala Kadipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatta Kadichi Aala Kadipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Kadichi Coola Irupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Kadichi Coola Irupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambellam Veri Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambellam Veri Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayatta Adipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayatta Adipan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pecha Korachi Unna Morappan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pecha Korachi Unna Morappan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochi Thenara Katti Pudipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi Thenara Katti Pudipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kuduthu Unna Edupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kuduthu Unna Edupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambellam Veri Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambellam Veri Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayatta Kadipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayatta Kadipan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece Semma Piece"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Nee Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Nee Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mela Na Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Na Loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Nee Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Nee Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mela Na Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Na Loosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuva Thodava Muthamidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva Thodava Muthamidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithayellam Naan Kathu Tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithayellam Naan Kathu Tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtapada Pora Moochi Mutti Pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtapada Pora Moochi Mutti Pova"/>
</div>
<div class="lyrico-lyrics-wrapper">Michadhellam Senjiputta Pithukuli Aava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michadhellam Senjiputta Pithukuli Aava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyala Enna Kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyala Enna Kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Mooti Nenja Mellura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Mooti Nenja Mellura"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Payala Pichi Thinnura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Payala Pichi Thinnura"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthiyellam Un Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthiyellam Un Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthudhadi Kirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthudhadi Kirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mooku Pudika Unna Rusipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku Pudika Unna Rusipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku Rusika Mutham Kudupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Rusika Mutham Kudupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikka Inikka Erumba Kadipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikka Inikka Erumba Kadipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallellaam Karanjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallellaam Karanjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meedhi Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meedhi Irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Sema Sema Sema Sema Sema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Sema Sema Sema Sema"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Piece Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Piece Semma Piece"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Nee Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Nee Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mela Na Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Na Loosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Nee Semma Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Nee Semma Piece"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mela Na Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Na Loosu"/>
</div>
</pre>
