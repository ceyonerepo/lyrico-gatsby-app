---
title: "kani nee song lyrics"
album: "Pothanur Thabal Nilayam"
artist: "Tenma"
lyricist: "Kaber Vasuki"
director: "Praveen"
path: "/albums/pothanur-thabal-nilayam-lyrics"
song: "Kani Nee"
image: ../../images/albumart/pothanur-thabal-nilayam.jpg
date: 2022-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jPJ0lJD2VNo"
type: "love"
singers:
  - Rajesh Giriprasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">swasicha mudhal nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swasicha mudhal nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nuniyil vidhyin vidhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuniyil vidhyin vidhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">thavizhndha ovvoru adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavizhndha ovvoru adi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kavithu vanden seyalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kavithu vanden seyalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">swasicha mudhal nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swasicha mudhal nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nuniyil vidhyin vidhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuniyil vidhyin vidhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">thavizhndha ovvoru adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavizhndha ovvoru adi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kavithu vanden seyalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kavithu vanden seyalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidhaitha vilaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhaitha vilaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">kilaigalaga viriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilaigalaga viriya"/>
</div>
<div class="lyrico-lyrics-wrapper">kilaigalin siraiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilaigalin siraiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kaithiyaga kuraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kaithiyaga kuraya"/>
</div>
<div class="lyrico-lyrics-wrapper">nokkum ettu thisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nokkum ettu thisaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">verum mullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum mullum"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaiyum kandavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaiyum kandavan"/>
</div>
<div class="lyrico-lyrics-wrapper">kai ettum dhoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai ettum dhoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhutha kani kandavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhutha kani kandavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">nakku oorudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku oorudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">pidippu paravudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidippu paravudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniya pathu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniya pathu than"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu pidhungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu pidhungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniya thinnathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniya thinnathan"/>
</div>
<div class="lyrico-lyrics-wrapper">manam thudikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam thudikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nodikku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodikku nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">thittatha theetudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittatha theetudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nakku oorudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku oorudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">pidippu paravudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidippu paravudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniya pathu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniya pathu than"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu pidhungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu pidhungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaniya thinnathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaniya thinnathan"/>
</div>
<div class="lyrico-lyrics-wrapper">manam thudikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam thudikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nodikku nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodikku nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">thittatha theetudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittatha theetudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silaiyil pathungiya kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyil pathungiya kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">boothamaga maarinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boothamaga maarinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">siraiyil irunthu viduvikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siraiyil irunthu viduvikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">endru vanthu koorinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru vanthu koorinaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silaiyil pathungiya kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyil pathungiya kadavule"/>
</div>
<div class="lyrico-lyrics-wrapper">boothamaga maarinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boothamaga maarinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">siraiyil irunthu viduvikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siraiyil irunthu viduvikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">endru vanthu koorinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru vanthu koorinaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalathin otathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathin otathai "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyaalum karvathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyaalum karvathai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathin otathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathin otathai "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyaalum karvathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyaalum karvathai"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu koduththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu koduththu"/>
</div>
<div class="lyrico-lyrics-wrapper">varam vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">vanangi kumbida mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanangi kumbida mudiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu "/>
</div>
<div class="lyrico-lyrics-wrapper">aatralai adainthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatralai adainthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">adakka siraigal kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adakka siraigal kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nokathai nugarnthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nokathai nugarnthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thadukka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thadukka mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatralai adainthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatralai adainthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">adakka siraigal kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adakka siraigal kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nokathai nugarnthu viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nokathai nugarnthu viten"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thadukka mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thadukka mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kani nee enakku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enakku than"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kani ellame enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kani ellame enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">kani nee enaku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kani nee enaku than"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku than kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku than kani"/>
</div>
</pre>
