---
title: "dindukallu poota potu song lyrics"
album: "Pammal K Sambandam"
artist: "Deva"
lyricist: "Pa. Vijay"
director: "Moulee"
path: "/albums/pammal-k-sambandam-lyrics"
song: "Dindukallu Poota Potu"
image: ../../images/albumart/pammal-k-sambandam.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q30tdFa9EZQ"
type: "love"
singers:
  - Shankar Mahadevan
  - Mahalakshmi Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dindukallu Poota Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dindukallu Poota Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaiya Pootikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaiya Pootikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Coimbatore Panja Vachee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coimbatore Panja Vachee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadha Pothikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadha Pothikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kaarakudi Ungapan Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaarakudi Ungapan Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Poiyikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Poiyikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kancheepuram Madathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kancheepuram Madathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Sernthukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Sernthukoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavur Bommaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Bommaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Thalaikanam Ethukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Thalaikanam Ethukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaankeyan Kaalaiyinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaankeyan Kaalaiyinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Enna Midhappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Enna Midhappo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandukkum Poovukkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandukkum Poovukkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhedhukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhedhukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Murukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Murukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Oththukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Oththukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kozhi Seval Kokkkokkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kozhi Seval Kokkkokkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dindukallu Poota Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dindukallu Poota Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaiya Pootikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaiya Pootikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Coimbatore Panja Vachee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coimbatore Panja Vachee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadha Pothikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadha Pothikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenapola Maari Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenapola Maari Poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaneethaan Enna Seiva Nu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaneethaan Enna Seiva Nu Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Kadalum Valaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Kadalum Valaiya Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pidithu Kattil Varuththu Thinben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pidithu Kattil Varuththu Thinben"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinbenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinbenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjakaatu Maramaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjakaatu Maramaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaripoovene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaripoovene"/>
</div>
<div class="lyrico-lyrics-wrapper">Marangothi Pol Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangothi Pol Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththi Paarpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththi Paarpene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Keerai Thottama Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Keerai Thottama Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vadhangi Vaata Thaan Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vadhangi Vaata Thaan Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Aatu Kuttiya Naanum Maarithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aatu Kuttiya Naanum Maarithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Meeyathaan Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Meeyathaan Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Morada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Morada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Porudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Porudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Maanam Paarpadhundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Maanam Paarpadhundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Thoone Seeni Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Thoone Seeni Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorvaikulle Onna Kedapoma Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorvaikulle Onna Kedapoma Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soora Kaathe Eera Kaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kaathe Eera Kaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Thalli Thaniyaai Irupoma Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Thalli Thaniyaai Irupoma Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Un Panjaloga Angaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Un Panjaloga Angaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhukkava Summaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhukkava Summaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Annakarandi Kondaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Annakarandi Kondaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkavaa Summaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkavaa Summaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kulikkapogaiyil Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kulikkapogaiyil Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Mudhugu Theikkavaa Summaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Mudhugu Theikkavaa Summaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Ithukku Poiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ithukku Poiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ketkkura Summaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ketkkura Summaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Rasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Rasame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Rasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Rasame"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Valiyil Kalandha Sugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Valiyil Kalandha Sugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dindukallu Poota Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dindukallu Poota Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal Pootikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Pootikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Coimbatore Panja Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Coimbatore Panja Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meththa Vangikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththa Vangikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kaarakudi Malligapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaarakudi Malligapoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vechikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vechikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kancheepuram Pattuselai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kancheepuram Pattuselai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanghi Vanthukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanghi Vanthukoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavur Bommaipola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Bommaipola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Aati Oththukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Aati Oththukoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaankeyan Kaalaipola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaankeyan Kaalaipola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vanthu Muttikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vanthu Muttikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandukkum Poovukkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandukkum Poovukkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Kirukkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Kirukkoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kattikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kattikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Vechukkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Vechukkoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kozhi Seval Kokkkokkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kozhi Seval Kokkkokkoo"/>
</div>
</pre>
