---
title: "sagaa song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Sagaa"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4OwIqSH_15s"
type: "sad"
singers:
  - Alphones
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaar senja paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar senja paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura enakul kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura enakul kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar senja paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar senja paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura enakul kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura enakul kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethu en pakkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethu en pakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">sirucha mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirucha mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">innaiku kaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innaiku kaanala"/>
</div>
<div class="lyrico-lyrics-wrapper">enge un thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge un thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">sodaku potathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sodaku potathum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthudum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthudum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">satham pol nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham pol nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ninuda yutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninuda yutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sagaa sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa sagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sagaa sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa sagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanal neer kadalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanal neer kadalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">sera thane kaalam selgirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sera thane kaalam selgirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam kolgirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam kolgirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thai madi pirinthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai madi pirinthome"/>
</div>
<div class="lyrico-lyrics-wrapper">tholamai enum thaayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholamai enum thaayai"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vananga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vananga"/>
</div>
<div class="lyrico-lyrics-wrapper">peigirane uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peigirane uranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aayiram uravunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram uravunga"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthalum ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthalum ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">natpa pol vanthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpa pol vanthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">kollathu aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollathu aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">usura tholachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura tholachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaga elakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaga elakuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sagaa sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa sagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sagaa sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa sagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sagaa sagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagaa sagaa"/>
</div>
</pre>
