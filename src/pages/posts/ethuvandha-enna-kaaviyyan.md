---
title: "ethuvandha enna song lyrics"
album: "Kaaviyyan"
artist: "Syam Mohan MM"
lyricist: "Mohan Rajan"
director: "Sarathi"
path: "/albums/kaaviyyan-lyrics"
song: "Ethuvandha Enna"
image: ../../images/albumart/kaaviyyan.jpg
date: 2019-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UxTj9jCB2OA"
type: "happy"
singers:
  - Ananthu
  - Dhivakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">edhu vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhu vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">evan vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiku aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiku aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">ada konja kaalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada konja kaalam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pazhi vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhi vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nanmaiku aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanmaiku aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">ada romba kaalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada romba kaalam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i don't care
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i don't care"/>
</div>
<div class="lyrico-lyrics-wrapper">don't deal with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="don't deal with me"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyayam than vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyayam than vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">en pola oru villan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pola oru villan than"/>
</div>
<div class="lyrico-lyrics-wrapper">indha worlduke raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha worlduke raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhu vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhu vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">evan vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiku aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiku aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">ada konja kaalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada konja kaalam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pazhi vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhi vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai vandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai vandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nanmaiku aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanmaiku aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">ada romba kaalam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada romba kaalam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonna nee kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna nee kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kaaval kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kaaval kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">onna verodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna verodu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan saaika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan saaika poren"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ye paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ye paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vettai karan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vettai karan"/>
</div>
<div class="lyrico-lyrics-wrapper">naade adhu sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naade adhu sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan moola kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan moola kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">neruku neraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruku neraga"/>
</div>
<div class="lyrico-lyrics-wrapper">modhi jeyipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhi jeyipen da"/>
</div>
<div class="lyrico-lyrics-wrapper">enga nee irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga nee irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu naan pidipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu naan pidipen da"/>
</div>
<div class="lyrico-lyrics-wrapper">en elaikul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en elaikul "/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nuzhaiyade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nuzhaiyade"/>
</div>
<div class="lyrico-lyrics-wrapper">edhu nuzhanjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhu nuzhanjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu thirumbathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu thirumbathe"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pulli vappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pulli vappen"/>
</div>
<div class="lyrico-lyrics-wrapper">pistol vachi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pistol vachi than"/>
</div>
</pre>
