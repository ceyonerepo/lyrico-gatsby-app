---
title: "middle class song lyrics"
album: "Sivakumarin Sabadham"
artist: "Hiphop Tamizha"
lyricist: "Rokesh"
director: "Hiphop Tamizha"
path: "/albums/sivakumarin-sabadham-lyrics"
song: "Middle Class"
image: ../../images/albumart/sivakumarin-sabadham.jpg
date: 2021-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q-SQGBXdBKI"
type: "happy"
singers:
  - Bamba Bakya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey hey Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey top um illa bottom um illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey top um illa bottom um illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga centre piece u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga centre piece u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhukumae asaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhukumae asaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga middle class u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga middle class u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangathu ennaikumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangathu ennaikumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaloda maasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaloda maasuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangathu kaila dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangathu kaila dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam kaasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam kaasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ellam middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ellam middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum kola massu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum kola massu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae kolar pieceu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae kolar pieceu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ellam middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ellam middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum kola massu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum kola massu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae kolar pieceu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae kolar pieceu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jova kava slowva poivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jova kava slowva poivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulta ulagil yedhuda nirandharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulta ulagil yedhuda nirandharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Day to night u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Day to night u"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily kalavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula narkunna irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula narkunna irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga nadutharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga nadutharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
3 <div class="lyrico-lyrics-wrapper">shiftu workla than tholaiyurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shiftu workla than tholaiyurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Office mudinja happyathaan alaiyurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Office mudinja happyathaan alaiyurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam macha yengaluku vilambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam macha yengaluku vilambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandipadha jaipom lifela oru tharam…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandipadha jaipom lifela oru tharam…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Hey maasa kadsi budget thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maasa kadsi budget thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Middle class middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middle class middle class"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasa kadsi budget thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa kadsi budget thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Middle class middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middle class middle class"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbo engalukku kaal dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbo engalukku kaal dhoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha naanga ellam middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha naanga ellam middle class"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbo engalukku kaal dhoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbo engalukku kaal dhoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha naanga ellam middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha naanga ellam middle class"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga middle classu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga middle classu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ellam middle classu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ellam middle classu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovumae kola massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumae kola massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae kolar pieceu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae kolar pieceu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toppa kaitu thadaigala dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toppa kaitu thadaigala dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappaluna thaanduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappaluna thaanduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Offeru la thuniya vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Offeru la thuniya vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Branda scene ah poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Branda scene ah poduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oc wifi la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oc wifi la"/>
</div>
<div class="lyrico-lyrics-wrapper">Iphone ah nonduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iphone ah nonduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarum nalla irruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarum nalla irruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavula thaan venduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavula thaan venduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah ah enne pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah enne pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Solidu shoikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solidu shoikkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinji pona serupula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinji pona serupula than"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin kuthikuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin kuthikuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja thukki nimirdhukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja thukki nimirdhukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga suthikuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga suthikuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey maasa kadsi budget thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey maasa kadsi budget thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Middle class middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middle class middle class"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasa kadsi budget thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa kadsi budget thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Middle class middle class
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middle class middle class"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ellam middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ellam middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovumae kola massu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumae kola massu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae kolar pieceu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae kolar pieceu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ellam middle classu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ellam middle classu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovumae kola massu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovumae kola massu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae kolar pieceu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae kolar pieceu da"/>
</div>
</pre>
