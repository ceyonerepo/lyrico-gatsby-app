---
title: "ippadi mazhai song lyrics"
album: "Vedi"
artist: "Vijay Antony"
lyricist: "Na. Muthukumar"
director: "Prabhu Deva"
path: "/albums/vedi-lyrics"
song: "Ippadi Mazhai"
image: ../../images/albumart/vedi.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/k0HfXii45JM"
type: "love"
singers:
  - Karthik
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ippadi Mazhai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Mazhai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kudai Pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kudai Pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Alai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Alai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kaal Nanaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kaal Nanaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Kan Imaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Kan Imaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Unnai Rasipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Unnai Rasipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Nee Sirithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Nee Sirithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Uyir Pizhaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Uyir Pizhaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo Hoo Hoo Oho Oho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo Hoo Hoo Oho Oho Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Mazhai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Mazhai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kudai Pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kudai Pidipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Alai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Alai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kaal Nanaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kaal Nanaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Vazhi Marithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Vazhi Marithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Nadandhiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Nadandhiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Mugam Sivandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Mugam Sivandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Mutham Iduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Mutham Iduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Poo Koidhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Poo Koidhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Malarndhiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Malarndhiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Thadai Vidhithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Thadai Vidhithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Nerungiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Nerungiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo Hoo Hoo Oho Oho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo Hoo Hoo Oho Oho Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Mazhai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Mazhai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kudai Pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kudai Pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Alai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Alai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kaal Nanaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kaal Nanaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Porthi Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Porthi Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Thirandhiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Thirandhiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadi Nee Adam Pidithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadi Nee Adam Pidithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Vilagiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Vilagiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadiyae Kirangadithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadiyae Kirangadithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Urangiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Urangiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Ippadi Nee Kaadhalithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Ippadi Nee Kaadhalithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Eppadi Naan Maruthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Eppadi Naan Maruthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo Hoo Hoo Oho Oho Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo Hoo Hoo Oho Oho Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Mazhai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Mazhai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kudai Pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kudai Pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Alai Adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Alai Adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Eppadi Kaal Nanaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Eppadi Kaal Nanaipen"/>
</div>
</pre>
