---
title: "murada murada song lyrics"
album: "Dhanusu Raasi Neyargale"
artist: "Ghibran"
lyricist: "Viveka"
director: "Sanjay Bharathi"
path: "/albums/dhanusu-raasi-neyargale-lyrics"
song: "Murada Murada"
image: ../../images/albumart/dhanusu-raasi-neyargale.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tbnNVsrXAa4"
type: "love"
singers:
  - Bombay Jayashri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Yedho Vibaritham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Yedho Vibaritham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhu Illai Orupodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadhu Illai Orupodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai Naan Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai Naan Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Yedho Vibaritham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Yedho Vibaritham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhu Illai Orupodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadhu Illai Orupodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Vazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Vazhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithen Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithen Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Ninaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Ninaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithen Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithen Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi Varaiyil Needhaan Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi Varaiyil Needhaan Thunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai En Thalaiyanaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai En Thalaiyanaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murada Murada Murada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murada Murada Murada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuthum Tharuven Poruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthum Tharuven Poruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Murada Murada Murada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murada Murada Murada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuthum Tharuven Poruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthum Tharuven Poruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Yedho Vibaritham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Yedho Vibaritham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhu Illai Orupodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadhu Illai Orupodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Paarthidum Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Paarthidum Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Nee Nindre Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Nee Nindre Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Dhooramum Perum Baaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Dhooramum Perum Baaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vathaikindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathaikindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadiyaai Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadiyaai Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyaadha Bimbam Pol Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyaadha Bimbam Pol Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalume Ondraagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalume Ondraagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Ketkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kooda Nidhanam Illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kooda Nidhanam Illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaava Ulaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaava Ulaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhal Kooda Thanneerai Polaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhal Kooda Thanneerai Polaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulaava Kulaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaava Kulaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimirodu Kanaithu Anaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirodu Kanaithu Anaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugarnthaai Mugarnthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugarnthaai Mugarnthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakaadha Kadhavum Thiranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakaadha Kadhavum Thiranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhainthaai Nuzhainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhainthaai Nuzhainthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muradaa Murada Murada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muradaa Murada Murada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuthum Tharuven Poruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthum Tharuven Poruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Yedho Vibaritham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Yedho Vibaritham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhu Illai Orupodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadhu Illai Orupodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Vazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Vazhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithen Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithen Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Ninaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Ninaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithen Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithen Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi Varaiyil Needhaan Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi Varaiyil Needhaan Thunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai En Thalaiyanaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai En Thalaiyanaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murada Murada Murada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murada Murada Murada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuthum Tharuven Poruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthum Tharuven Poruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Murada Murada Murada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murada Murada Murada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuthum Tharuven Poruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuthum Tharuven Poruda"/>
</div>
</pre>
