---
title: "hey song lyrics"
album: "Mohini"
artist: "	Vivek-Mervin"
lyricist: "Mohan Rajan - Balan Kashmir"
director: "Ramana Madhesh"
path: "/albums/mohini-lyrics"
song: "Hey"
image: ../../images/albumart/mohini.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dgNykeTT500"
type: "love"
singers:
  - Mervin Solomon
  - Balan Kashmir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey un kangal pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un kangal pesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey oru minnal veesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oru minnal veesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey en nenjam pookuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en nenjam pookuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhu ennai thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhu ennai thaakuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey un kangal pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un kangal pesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey oru minnal veesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oru minnal veesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey en nenjam pookuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en nenjam pookuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhu ennai thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhu ennai thaakuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae adi nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae adi nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal unnai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal unnai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t-u worry now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t-u worry now"/>
</div>
<div class="lyrico-lyrics-wrapper">Time to let it go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time to let it go"/>
</div>
<div class="lyrico-lyrics-wrapper">Try-na hold my hands
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try-na hold my hands"/>
</div>
<div class="lyrico-lyrics-wrapper">Time to fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time to fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You know you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know you know"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae adi nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae adi nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal unnai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal unnai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t-u worry now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t-u worry now"/>
</div>
<div class="lyrico-lyrics-wrapper">Time to let it go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time to let it go"/>
</div>
<div class="lyrico-lyrics-wrapper">Try-na hold my hands
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try-na hold my hands"/>
</div>
<div class="lyrico-lyrics-wrapper">Time to fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time to fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You know you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know you know"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kankothi paravaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankothi paravaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee vangi pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee vangi pogirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun andhi maalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun andhi maalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum en tholil saaigirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum en tholil saaigirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ennai neenginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai neenginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan ninaivodu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan ninaivodu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En veetil thoongayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetil thoongayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kanavodu vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kanavodu vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnadi nirkumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi nirkumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi parkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi parkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi saayumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi saayumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladi pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thikku thikku kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thikku thikku kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">I swear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I swear"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sikkikitten unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sikkikitten unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">I care
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I care"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sutha vitta thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sutha vitta thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">I swear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I swear"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu adhuvo idhuvo edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu adhuvo idhuvo edhuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you dance with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you dance with me all night"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby would you hold me really tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby would you hold me really tight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Good lord good lord
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good lord good lord"/>
</div>
<div class="lyrico-lyrics-wrapper">This girl got me sprung
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This girl got me sprung"/>
</div>
<div class="lyrico-lyrics-wrapper">And her smile got me done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And her smile got me done"/>
</div>
<div class="lyrico-lyrics-wrapper">Can she be the only one?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can she be the only one?"/>
</div>
<div class="lyrico-lyrics-wrapper">But the only question is
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But the only question is"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby, would you wanna come and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby, would you wanna come and"/>
</div>
<div class="lyrico-lyrics-wrapper">Ride with me ride with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ride with me ride with me"/>
</div>
<div class="lyrico-lyrics-wrapper">ride with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ride with me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let’s take a long drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s take a long drive"/>
</div>
<div class="lyrico-lyrics-wrapper">And sip on the coffee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And sip on the coffee"/>
</div>
<div class="lyrico-lyrics-wrapper">You can try to be naughty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can try to be naughty"/>
</div>
<div class="lyrico-lyrics-wrapper">Or try to be a lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or try to be a lady"/>
</div>
<div class="lyrico-lyrics-wrapper">You can be my lil shorty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can be my lil shorty"/>
</div>
<div class="lyrico-lyrics-wrapper">With coke bottle body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With coke bottle body"/>
</div>
<div class="lyrico-lyrics-wrapper">I like it when you moody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I like it when you moody"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s why you-re so cutie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s why you-re so cutie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uhh whut chu wanna go a ride with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uhh whut chu wanna go a ride with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Uhh whut chu wanna go a ride with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uhh whut chu wanna go a ride with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Uhh whut chu wanna go a ride with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uhh whut chu wanna go a ride with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby girl come and ride with me all night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby girl come and ride with me all night"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey un kangal pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un kangal pesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey oru minnal veesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oru minnal veesuthae"/>
</div>
</pre>
