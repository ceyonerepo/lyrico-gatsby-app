---
title: "enna elavu kathal song lyrics"
album: "Appathava Aattaya Pottutanga"
artist: "Asvamitra"
lyricist: "S.Selvakumaar"
director: "N. Stephen Rangaraj"
path: "/albums/appathava-aattaya-pottutanga-lyrics"
song: "Enna Elavu Kathal"
image: ../../images/albumart/appathava-aattaya-pottutanga.jpg
date: 2021-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hr6XmeeS-0Y"
type: "sad"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna elavu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna elavu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paadu paduthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paadu paduthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vaaltha kudumbathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vaaltha kudumbathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadutheruvula niruthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadutheruvula niruthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna elavu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna elavu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paadu paduthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paadu paduthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vaaltha kudumbathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vaaltha kudumbathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadutheruvula niruthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadutheruvula niruthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethu vacha konja nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu vacha konja nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">perai ellam kedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perai ellam kedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu vaala police station
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu vaala police station"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaliyoda ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaliyoda ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal paduthum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal paduthum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadavuluke velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadavuluke velicham"/>
</div>
<div class="lyrico-lyrics-wrapper">karumam pudicha kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumam pudicha kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">antha karpathaiyum kalaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha karpathaiyum kalaikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaya katti vayitha katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaya katti vayitha katti"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam padika vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam padika vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">notu booka ooram katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="notu booka ooram katti"/>
</div>
<div class="lyrico-lyrics-wrapper">face book la layicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face book la layicha"/>
</div>
<div class="lyrico-lyrics-wrapper">anga enga alatti katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga enga alatti katti"/>
</div>
<div class="lyrico-lyrics-wrapper">selfi onna potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selfi onna potta"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vantha rasikana than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vantha rasikana than"/>
</div>
<div class="lyrico-lyrics-wrapper">ragasiyama anacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragasiyama anacha"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathuku love u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathuku love u"/>
</div>
<div class="lyrico-lyrics-wrapper">isthukinu ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isthukinu ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mandai kulla pulpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandai kulla pulpu"/>
</div>
<div class="lyrico-lyrics-wrapper">color color ah eriuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color color ah eriuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna elavu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna elavu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paadu paduthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paadu paduthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vaaltha kudumbathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vaaltha kudumbathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadutheruvula niruthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadutheruvula niruthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onnu illa rendu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu illa rendu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu figara pudichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu figara pudichan"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu peru manasukullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu peru manasukullum"/>
</div>
<div class="lyrico-lyrics-wrapper">duet paadi naduchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duet paadi naduchan"/>
</div>
<div class="lyrico-lyrics-wrapper">anga inga room ah potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga inga room ah potu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam aadi muduchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam aadi muduchan"/>
</div>
<div class="lyrico-lyrics-wrapper">appan aatha aasa patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appan aatha aasa patta"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumanathula kudichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumanathula kudichan"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathuku love u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathuku love u"/>
</div>
<div class="lyrico-lyrics-wrapper">isthukinu ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isthukinu ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mandai kulla bulpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandai kulla bulpu"/>
</div>
<div class="lyrico-lyrics-wrapper">color color ah eriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color color ah eriyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna elavu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna elavu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paadu paduthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paadu paduthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vaaltha kudumbathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vaaltha kudumbathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nadutheruvula niruthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadutheruvula niruthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethu vacha konja nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu vacha konja nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">perai ellam kedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perai ellam kedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu vaala police station
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu vaala police station"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaliyoda ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaliyoda ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal paduthum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal paduthum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadavuluke velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadavuluke velicham"/>
</div>
<div class="lyrico-lyrics-wrapper">karumam pudicha kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumam pudicha kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">antha karpathaiyum kalaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha karpathaiyum kalaikum"/>
</div>
</pre>
