---
title: "yenakku mattum yaen song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "Yenakku Mattum Yaen"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6_pnZhrk2pg"
type: "mass"
singers:
  - Chinna
  - Badava Gopi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaarukaaga eluthungal en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukaaga eluthungal en"/>
</div>
<div class="lyrico-lyrics-wrapper">kallaraiyil aval konjam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallaraiyil aval konjam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">irakkam illathaval endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakkam illathaval endru"/>
</div>
<div class="lyrico-lyrics-wrapper">paadungal en kallaraiyil ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadungal en kallaraiyil ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vadikatuna muttal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vadikatuna muttal endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">avala than paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avala than paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kasanthiduchu vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasanthiduchu vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu mudiyum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu mudiyum naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">ivala katti kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivala katti kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyatha sooravali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyatha sooravali"/>
</div>
<div class="lyrico-lyrics-wrapper">coloural ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coloural ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">adipathu thala vithiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipathu thala vithiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sinam enna mithika vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinam enna mithika vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">sirutha enna kadika ninnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirutha enna kadika ninnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku ellam payapadama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku ellam payapadama"/>
</div>
<div class="lyrico-lyrics-wrapper">pogira aal aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogira aal aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">aana avala nenaikum pothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana avala nenaikum pothe"/>
</div>
<div class="lyrico-lyrics-wrapper">peethiyum aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peethiyum aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">natpum kooda karpe endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpum kooda karpe endru"/>
</div>
<div class="lyrico-lyrics-wrapper">paadi ponan vaaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadi ponan vaaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">karpum ingum koodathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karpum ingum koodathappa"/>
</div>
<div class="lyrico-lyrics-wrapper">laali poche vaaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laali poche vaaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">pochu pochu aadi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu pochu aadi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">aani verum aadi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aani verum aadi pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ranji yendi ranji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranji yendi ranji"/>
</div>
<div class="lyrico-lyrics-wrapper">ade pavi payale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ade pavi payale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannu vachu avala pathene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu vachu avala pathene"/>
</div>
<div class="lyrico-lyrics-wrapper">kandathaiyum vaangi potene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandathaiyum vaangi potene"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthum kooda seruthu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthum kooda seruthu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu thaan ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu thaan ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha arukkum avanukaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha arukkum avanukaga"/>
</div>
<div class="lyrico-lyrics-wrapper">colour fulla naan aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colour fulla naan aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">nrin mela kolam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nrin mela kolam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">aagi pone naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagi pone naane"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali katti kooti vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali katti kooti vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman aanen veene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman aanen veene"/>
</div>
<div class="lyrico-lyrics-wrapper">pochu pochu mosam pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu pochu mosam pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda senthu maanam pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda senthu maanam pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">yenaku mattum yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenaku mattum yen yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ipadi ellam nadakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipadi ellam nadakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">avala than paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avala than paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kasanthiduchu vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasanthiduchu vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu mudiyum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu mudiyum naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">ivala katti kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivala katti kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyatha sooravali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyatha sooravali"/>
</div>
<div class="lyrico-lyrics-wrapper">coloural ingu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coloural ingu "/>
</div>
<div class="lyrico-lyrics-wrapper">adipathu thala vithiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipathu thala vithiya"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku mattum"/>
</div>
</pre>
