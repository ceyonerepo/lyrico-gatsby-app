---
title: "matlabi yariyan song lyrics"
album: "The Girl On The Train"
artist: "Vipin Patwa"
lyricist: "Kumaar"
director: "Ribhu Dasgupta"
path: "/albums/the-girl-on-the-train-lyrics"
song: "Matlabi Yariyan"
image: ../../images/albumart/the-girl-on-the-train.jpg
date: 2021-02-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/x0VYXVGCfZY"
type: "sad"
singers:
  - Neha Kakkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kyun beech mein yeh deewarein hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun beech mein yeh deewarein hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhne lagi yeh dararein hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhne lagi yeh dararein hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai wajah, kuch wajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai wajah, kuch wajah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In dooriyon ke ishare hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In dooriyon ke ishare hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Samundar mein doobe kinare hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samundar mein doobe kinare hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Har jagah, har jagah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har jagah, har jagah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soyi nazar mein sau dard jaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soyi nazar mein sau dard jaage"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikharti gayi main jod jod dhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikharti gayi main jod jod dhaage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq mein thi kitni khaamiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mein thi kitni khaamiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas mili hain badnaamiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas mili hain badnaamiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq mein thi kitni khaamiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mein thi kitni khaamiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas mili hain badnaamiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas mili hain badnaamiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ke musafir ne dekhi hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ke musafir ne dekhi hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Har pal nakaamiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har pal nakaamiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare khwaab toote aankhon ke aage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare khwaab toote aankhon ke aage"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikharti gayi main jod jod dhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikharti gayi main jod jod dhaage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhoothi sajna ki yaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi sajna ki yaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoothi thi woh daavedari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi thi woh daavedari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoothi nikli dil'on ki saude baaziyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi nikli dil'on ki saude baaziyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhoothi sajna ki yaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi sajna ki yaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoothi thi woh daavedari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi thi woh daavedari"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoothi nikli dil'on ki saude baaziyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoothi nikli dil'on ki saude baaziyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Haariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara matlabi thi teri yariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara matlabi thi teri yariyan"/>
</div>
</pre>
