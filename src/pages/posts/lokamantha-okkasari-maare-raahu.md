---
title: "lokamantha okkasari maare song lyrics"
album: "Raahu"
artist: "Praveen Lakkaraju"
lyricist: "Srinivasa Mouli"
director: "Subbu Vedula"
path: "/albums/raahu-lyrics"
song: "Lokamantha Okkasari Maare"
image: ../../images/albumart/raahu.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lcgANYy0o4Y"
type: "love"
singers:
  - Manisha Eerabathini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lokamantha okkasari maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha okkasari maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargamantha ochi gunde meeda vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargamantha ochi gunde meeda vaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulanni dhati manasu thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulanni dhati manasu thele"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukante anthu thelade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukante anthu thelade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maataleni paata putti naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataleni paata putti naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayadari manta pette lolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayadari manta pette lolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchugaali thaakinattu may lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchugaali thaakinattu may lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirantha uthsavaalu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirantha uthsavaalu le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itentha dooram illa yedhi adagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itentha dooram illa yedhi adagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam neetho adugey annade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam neetho adugey annade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokamantha okkasari maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha okkasari maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargamantha ochi gunde meeda vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargamantha ochi gunde meeda vaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulanni dhati manasu thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulanni dhati manasu thele"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukante anthu thelade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukante anthu thelade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagipothey kaalam antha neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipothey kaalam antha neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppukunte oosulenno neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppukunte oosulenno neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu unte navvuthunte naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu unte navvuthunte naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalu inka lokam enduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalu inka lokam enduku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janta gaane saaguthunte neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta gaane saaguthunte neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepi chere jeevithaana entho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepi chere jeevithaana entho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethi geetha navvuthondi naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethi geetha navvuthondi naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelusu neeku enduku endukenduko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelusu neeku enduku endukenduko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee venta maataa mounam anni andhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venta maataa mounam anni andhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Andham Addham Ardham ika Nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andham Addham Ardham ika Nuvvele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokamantha okkasari maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha okkasari maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargamantha ochi gunde meeda vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargamantha ochi gunde meeda vaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulanni dhati manasu thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulanni dhati manasu thele"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukante anthu thelade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukante anthu thelade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey pranama antha ninnu kori undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey pranama antha ninnu kori undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu ento pattanandi gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu ento pattanandi gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Needa thone cheekatina andhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needa thone cheekatina andhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukunna kotha snehame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukunna kotha snehame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu evvarante emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu evvarante emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Snehamanna maata chalademo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehamanna maata chalademo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapaatu uppenaindi premo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapaatu uppenaindi premo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnaleni kotha bhavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnaleni kotha bhavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalochi naalo cheri nanne marchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalochi naalo cheri nanne marchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne tholiga sariga chusane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne tholiga sariga chusane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokamantha okkasari maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha okkasari maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargamantha ochi gunde meeda vaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargamantha ochi gunde meeda vaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulanni dhati manasu thele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulanni dhati manasu thele"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukante anthu thelade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukante anthu thelade"/>
</div>
</pre>
