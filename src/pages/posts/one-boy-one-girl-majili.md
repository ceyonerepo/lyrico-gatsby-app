---
title: "one boy one girl song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Bhaskarabhatla"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "One Boy One Girl"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jf8xegqYNko"
type: "happy"
singers:
  - Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">One boy.. one girl.. looking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One boy.. one girl.. looking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Height-u.. weight-u.. checking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Height-u.. weight-u.. checking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parents.. fixing.. matching-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parents.. fixing.. matching-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowry.. giving-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dowry.. giving-u"/>
</div>
<div class="lyrico-lyrics-wrapper">One boy.. one girl.. looking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One boy.. one girl.. looking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Height-u.. weight-u.. checking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Height-u.. weight-u.. checking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parents.. fixing.. matching-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parents.. fixing.. matching-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowry.. giving-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dowry.. giving-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marriage bells.. ringing-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage bells.. ringing-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Single bed-u.. sharing-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single bed-u.. sharing-u"/>
</div>
<div class="lyrico-lyrics-wrapper">In-side.. first love.. killing-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In-side.. first love.. killing-u "/>
</div>
<div class="lyrico-lyrics-wrapper">You are dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are dying-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the? 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the? "/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the loving-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the loving-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the feeling-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the feeling-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">You are feeling-u.. crying-u.. dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are feeling-u.. crying-u.. dying-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the loving-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the loving-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the feeling-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the feeling-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">You are feeling-u.. crying-u.. dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are feeling-u.. crying-u.. dying-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One boy.. one girl.. looking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One boy.. one girl.. looking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Height-u.. weight-u.. checking-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Height-u.. weight-u.. checking-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Parents.. fixing.. matching-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parents.. fixing.. matching-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowry.. giving-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dowry.. giving-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naannake maaticchaavanee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naannake maaticchaavanee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aa.. naannake maaticchaavanee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa.. naannake maaticchaavanee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Amma aligindanee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma aligindanee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Manusuke uri taadesee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusuke uri taadesee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi vestaavaa taalinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi vestaavaa taalinee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantakaanee pillato..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantakaanee pillato.."/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu adugulu veyyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu adugulu veyyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru adugula goyyinee..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru adugula goyyinee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku nuvve tavvadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku nuvve tavvadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku todu.. marri chettu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku todu.. marri chettu.."/>
</div>
<div class="lyrico-lyrics-wrapper">School ground-u.. gaandhi bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School ground-u.. gaandhi bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelonaa.. cherigipone..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelonaa.. cherigipone.."/>
</div>
<div class="lyrico-lyrics-wrapper">Nee prema bommaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee prema bommaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Where is the? where is the?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the? 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the? "/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the loving-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the loving-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the feeling-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the feeling-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">You are feeling-u.. crying-u.. dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are feeling-u.. crying-u.. dying-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla mundunde roopam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla mundunde roopam "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa.. kalla mundunde roopam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa.. kalla mundunde roopam "/>
</div>
<div class="lyrico-lyrics-wrapper">Manusune taakade..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusune taakade.."/>
</div>
<div class="lyrico-lyrics-wrapper">Manusulo nindina praanam..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusulo nindina praanam.."/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurugaa vacchi vaalade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurugaa vacchi vaalade"/>
</div>
<div class="lyrico-lyrics-wrapper">Rail-u jarnee kaaduraa.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail-u jarnee kaaduraa.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Life jarnee pelliraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life jarnee pelliraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yevvaraite yentanee.. 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevvaraite yentanee.. "/>
</div>
<div class="lyrico-lyrics-wrapper">Sardukolem sodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sardukolem sodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaloki choodalekaa ..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaloki choodalekaa .."/>
</div>
<div class="lyrico-lyrics-wrapper">Maatakooda kalapalekaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatakooda kalapalekaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevitaantam.. janta madhya..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevitaantam.. janta madhya.."/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomadhya rekhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomadhya rekhaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Where is the? where is the?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the? 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the? "/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the loving-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the loving-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the feeling-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the feeling-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">You are feeling-u.. crying-u.. dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are feeling-u.. crying-u.. dying-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the loving-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the loving-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Where is the? where is the feeling-u?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where is the? where is the feeling-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">You are feeling-u.. crying-u.. dying-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are feeling-u.. crying-u.. dying-u"/>
</div>
</pre>
