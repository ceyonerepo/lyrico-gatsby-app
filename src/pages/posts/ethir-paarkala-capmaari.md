---
title: "ethir paarkala song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "Mohan Rajan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Ethir Paarkala"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Sanjith Hegde 
  - Pallavi Vinothkumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiyae Intha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiyae Intha Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla Nikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Nikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyum Un Manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyum Un Manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Vandhathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Vandhathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalum En Nizhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalum En Nizhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Kondu Poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Kondu Poguthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Ondru Kekkum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Ondru Kekkum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Udhadu Thittudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udhadu Thittudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbiyaa Thisai Yellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbiyaa Thisai Yellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Theriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Theriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbiya Padiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbiya Padiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manam Piriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Piriyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiyae Intha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiyae Intha Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla Nikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Nikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyum Un Manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyum Un Manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Vandhathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Vandhathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyoo Ival Thittaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyoo Ival Thittaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Kidaiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Kidaiyaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Nee Mannipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Nee Mannipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakae Theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakae Theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Vathaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Vathaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Sollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Sollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagai Poi Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagai Poi Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Naanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Naanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therindhum Adi Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhum Adi Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Yen Virumbinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yen Virumbinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiyae Intha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiyae Intha Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla Nikkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Nikkuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyum En Manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyum En Manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidathil Vandhathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidathil Vandhathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethaiyum Kalavadum Padavaa Unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyum Kalavadum Padavaa Unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Thadumaari Vizhuthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Thadumaari Vizhuthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittamillaamal Thirudi Vanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamillaamal Thirudi Vanthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraiyil Vaithaayae Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraiyil Vaithaayae Vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadayam Illamaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam Illamaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagaval Sollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagaval Sollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Thiruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Thiruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naan Thirudinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Thirudinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiyae Intha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiyae Intha Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla Nikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Nikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyum Un Manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyum Un Manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Vandhathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Vandhathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahoo Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahoo Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalum En Nizhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalum En Nizhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Kondu Poguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Kondu Poguthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethir Paarkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Ondru Vaikka Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Ondru Vaikka Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Udhadu Thittudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udhadu Thittudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittudhada Thittudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittudhada Thittudhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittudhada Thittudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittudhada Thittudhada"/>
</div>
</pre>
