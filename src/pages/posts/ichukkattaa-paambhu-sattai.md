---
title: "ichukkattaa song lyrics"
album: "Paambhu Sattai"
artist: "Ajeesh"
lyricist: "Mani Amuthavan"
director: "Adam Dasan"
path: "/albums/paambhu-sattai-lyrics"
song: "Ichukkattaa"
image: ../../images/albumart/paambhu-sattai.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qJmZkpjoo04"
type: "happy"
singers:
  - Deepak
  - Ranjini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey ichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ichukattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mechikattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mechikattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna konjam vechukkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna konjam vechukkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bit-u pada tent-u kottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bit-u pada tent-u kottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla konjam nozhanjukkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla konjam nozhanjukkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachikattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochikkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochikkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna konjam pichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna konjam pichukattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumilla unna vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumilla unna vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanoondu kadichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanoondu kadichukattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaa pinnaa rusi nee yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa pinnaa rusi nee yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey enna enna pasi aathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey enna enna pasi aathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kanna nee kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kanna nee kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eppo ichu velayaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eppo ichu velayaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayangaala neram saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayangaala neram saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola meya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola meya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosi thatti podu paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosi thatti podu paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu vittu koodu paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu koodu paaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichukattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mechikattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mechikattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna konjam vechukkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna konjam vechukkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bit-u pada tent-u kottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bit-u pada tent-u kottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla konjam nozhanjukkattaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla konjam nozhanjukkattaa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachikattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochikkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochikkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna konjam pichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna konjam pichukattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumilla unna vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumilla unna vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithanoondu kadichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanoondu kadichukattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kanna ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kanna ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu vizhi ambaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu vizhi ambaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi enna kadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi enna kadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha manam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha manam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallavana enga nadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavana enga nadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhiyila vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhiyila vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaya thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaya thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae ingae idam pidipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae ingae idam pidipa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthiyila pasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthiyila pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangi pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga engeyo parapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga engeyo parapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edutherinju pesa venaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutherinju pesa venaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anusarichu poyendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anusarichu poyendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavikka venum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavikka venum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae thaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae thaayendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru thiru-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru thiru-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhikkum onaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhikkum onaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku indha poochaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku indha poochaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku vera aala paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku vera aala paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaa maayaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaa maayaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvara manavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvara manavara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappilla maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappilla maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhamura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana ara thorakkuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana ara thorakkuranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minungura ilampira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minungura ilampira"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogathula dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogathula dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenam dhenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam dhenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhikkuranae hoii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhikkuranae hoii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vara vara ivan thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara vara ivan thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumtholla dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumtholla dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukkala hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukkala hara hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva sivanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva sivanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu enna modha mura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu enna modha mura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhamburen naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhamburen naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thara nenakkuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thara nenakkuranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saara kannu oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara kannu oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora pallu keera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora pallu keera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala mennu poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala mennu poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meera ella meera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meera ella meera"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam mella thoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam mella thoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram solli podiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram solli podiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paara kallu paara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara kallu paara"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan salli vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan salli vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti ulla vaariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti ulla vaariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera enna koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera enna koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa nenju maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa nenju maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen veetukaariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen veetukaariyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ichukattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ichukattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Mechikattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mechikattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna konjam vechukkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna konjam vechukkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bit-u pada tent-u kottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bit-u pada tent-u kottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla konjam nozhanjukkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla konjam nozhanjukkattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaa pinnaa rusi nee yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa pinnaa rusi nee yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey enna enna pasi aathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey enna enna pasi aathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kanna nee kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kanna nee kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eppo ichu velayaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eppo ichu velayaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayangaala neram saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayangaala neram saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola meya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola meya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosi thatti podu paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosi thatti podu paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu vittu koodu paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu koodu paaya"/>
</div>
</pre>
