---
title: "thanjavuru melathukku song lyrics"
album: "Seemathurai"
artist: "Jose Franklin"
lyricist: "Santhosh Thiyagarajan"
director: "Santhosh Thiyagarajan"
path: "/albums/seemathurai-lyrics"
song: "Thanjavuru Melathukku"
image: ../../images/albumart/seemathurai.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3VUz69tETkY"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thanjavuru melathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavuru melathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi serum nathaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi serum nathaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nattu panpattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nattu panpattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru asthivaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru asthivaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalil kolusadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalil kolusadum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil maruthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil maruthani"/>
</div>
<div class="lyrico-lyrics-wrapper">nammooruthan maralada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammooruthan maralada"/>
</div>
<div class="lyrico-lyrics-wrapper">thavani poo potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavani poo potta"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnayuthan kan pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnayuthan kan pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha ottam kooduthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha ottam kooduthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">pengala rasithida than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengala rasithida than"/>
</div>
<div class="lyrico-lyrics-wrapper">selvavonnum illayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selvavonnum illayada"/>
</div>
<div class="lyrico-lyrics-wrapper">ilavasa inbam ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilavasa inbam ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu rasingada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu rasingada"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagila ponnungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagila ponnungala"/>
</div>
<div class="lyrico-lyrics-wrapper">aandavan padaikalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandavan padaikalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru ponnum inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru ponnum inge"/>
</div>
<div class="lyrico-lyrics-wrapper">then sema azhaguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then sema azhaguda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanjavuru melathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavuru melathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi serum nathaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi serum nathaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nattu panpattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nattu panpattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru asthivaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru asthivaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanja punja sezhipana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanja punja sezhipana"/>
</div>
<div class="lyrico-lyrics-wrapper">karikalan nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karikalan nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathu makkalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathu makkalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">soruttum veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soruttum veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaviriyil thanni vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaviriyil thanni vara"/>
</div>
<div class="lyrico-lyrics-wrapper">poradi vazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poradi vazhu"/>
</div>
<div class="lyrico-lyrics-wrapper">natta kappatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natta kappatha"/>
</div>
<div class="lyrico-lyrics-wrapper">naam thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula veerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula veerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">peru pona nammooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peru pona nammooru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma pola aal yaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma pola aal yaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvengum koyilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvengum koyilgal"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvizha santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvizha santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vera engum kedaiyathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera engum kedaiyathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">chozha nattu veera singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chozha nattu veera singam"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhunthu naama ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhunthu naama ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">vanam thala muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanam thala muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanjavuru melathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavuru melathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi serum nathaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi serum nathaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nattu panpattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nattu panpattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru asthivaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru asthivaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thannan thani aalu inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannan thani aalu inge"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illa ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illa ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi ulla aalungallam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi ulla aalungallam"/>
</div>
<div class="lyrico-lyrics-wrapper">sonthakaran paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthakaran paru"/>
</div>
<div class="lyrico-lyrics-wrapper">panthi vachu parimarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panthi vachu parimarum"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathuku eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathuku eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi nee paru kedaikathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi nee paru kedaikathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu sanda silambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu sanda silambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kavadi karakattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavadi karakattam"/>
</div>
<div class="lyrico-lyrics-wrapper">namma oorin adayalanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma oorin adayalanda"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi matham paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi matham paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">samiya than kumbiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samiya than kumbiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">onna kondadum thirunalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna kondadum thirunalada"/>
</div>
<div class="lyrico-lyrics-wrapper">chozha nattu veera singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chozha nattu veera singam"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhunthu naama ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhunthu naama ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">vanam thala muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanam thala muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanjavuru melathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavuru melathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi serum nathaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi serum nathaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nattu panpattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nattu panpattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru asthivaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru asthivaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanjavuru melathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavuru melathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi serum nathaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi serum nathaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nattu panpattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nattu panpattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru asthivaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru asthivaram"/>
</div>
</pre>
