---
title: "kalla manna song lyrics"
album: "Kanavu Variyam"
artist: "Shyam Benjamin"
lyricist: "Arun Chidambaram"
director: "Arun Chidambaram"
path: "/albums/kanavu-variyam-lyrics"
song: "Kalla Manna"
image: ../../images/albumart/kanavu-variyam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OfVsOcLQqks"
type: "happy"
singers:
  - Mithuna
  - Sai Rishi
  - Sai Lakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalla manna kalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla manna kalla "/>
</div>
<div class="lyrico-lyrics-wrapper">manna aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna aadalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu pudikka naadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu pudikka naadu "/>
</div>
<div class="lyrico-lyrics-wrapper">pudikka odalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudikka odalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tyre vandi otiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyre vandi otiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu poiyi seralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu poiyi seralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kichu thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu thaambaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya kiya thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya kiya thaambaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai kudhirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai kudhirai "/>
</div>
<div class="lyrico-lyrics-wrapper">thaandum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaandum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunibavan dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunibavan dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru mempaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru mempaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla manna kalla manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla manna kalla manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu pudikka naadu pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu pudikka naadu pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaatukku panjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaatukku panjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gramaathula suththmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gramaathula suththmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai vaasi kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai vaasi kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engallukku ennaikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engallukku ennaikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannu dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Elai dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elai dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Keerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keerai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poriyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada eppadi enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada eppadi enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaiyal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga manala kumichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga manala kumichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Touseraa pudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touseraa pudichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayilu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayilu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Burrrunnu ottuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burrrunnu ottuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike-u dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike-u dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarrrunnu adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarrrunnu adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Break dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Horn adichu vara maatiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horn adichu vara maatiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maiyai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai theetalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai theetalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppaakiyaai aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppaakiyaai aagalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaippadhil inbam kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaippadhil inbam kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada engakitta yosanai ketkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada engakitta yosanai ketkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma kuththu aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kuththu aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga soththu sandhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga soththu sandhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga thevai romba sirisu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga thevai romba sirisu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa karpanaiyo romba perusu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa karpanaiyo romba perusu dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athili buththili maha suha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athili buththili maha suha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal parangi raatmaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal parangi raatmaa "/>
</div>
<div class="lyrico-lyrics-wrapper">pootmaaChee sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pootmaaChee sal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam ochiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ochiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam paadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam paadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nila poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nila poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa aadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaithakaadhai yai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaithakaadhai yai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nondiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nondiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa uppu vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa uppu vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamum pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamum pogalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookku killi killalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookku killi killalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallan yaaru thedalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallan yaaru thedalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannaam podhi odalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaam podhi odalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakka kunju aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka kunju aagalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu viral serndhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu viral serndhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeppee dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeppee dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali lottaa engallukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali lottaa engallukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappi dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappi dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokku dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokku dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam aadinaa illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam aadinaa illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekku dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekku dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girra girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girra girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Domma domma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Domma domma"/>
</div>
<div class="lyrico-lyrics-wrapper">Avva avva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avva avva"/>
</div>
<div class="lyrico-lyrics-wrapper">Fu fu fuuffuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fu fu fuuffuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappu saadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappu saadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil kadaiyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil kadaiyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandu oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandu oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichchu kichchu moottalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichchu kichchu moottalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla piraandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla piraandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhu adikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhu adikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirchchu sirchchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirchchu sirchchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum aadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum aadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kichu thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu thaambaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya kiya thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya kiya thaambaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai kudhirai thaandum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai kudhirai thaandum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunibavan dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunibavan dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru mempaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru mempaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kichu thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu thaambaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya kiya thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya kiya thaambaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchai kudhirai thaandum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai kudhirai thaandum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunibavan dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunibavan dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru mempaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru mempaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uffuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uffuuu"/>
</div>
</pre>
