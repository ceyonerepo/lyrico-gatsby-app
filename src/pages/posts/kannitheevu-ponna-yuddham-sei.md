---
title: "kannitheevu ponna song lyrics"
album: "Yuddham Sei"
artist: "Krishnakumar"
lyricist: "	Kabilan"
director: "Mysskin"
path: "/albums/yuddham-sei-lyrics"
song: "Kannitheevu Ponna"
image: ../../images/albumart/yuddham-sei.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h0hdFPTvTyo"
type: "happy"
singers:
  - Raqueeb Alam
  - M.L.R. Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">katterumbu kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katterumbu kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumara thuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumara thuduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iduppa aatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iduppa aatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kallu paanaa udhatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kallu paanaa udhatta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kaduppu yaethuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kaduppu yaethuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">katterumbu kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katterumbu kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumara thuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumara thuduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iduppa aatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iduppa aatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kallu paanaa udhatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kallu paanaa udhatta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kaduppu yaethuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kaduppu yaethuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mathaappu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathaappu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sirichitu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichitu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kithaappu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kithaappu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">midhichitu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="midhichitu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">indha vappaatiya paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha vappaatiya paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pondaatiya maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pondaatiya maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan mundhaanaya modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan mundhaanaya modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan moppam pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan moppam pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">katterumbu kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katterumbu kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumara thuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumara thuduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iduppa aatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iduppa aatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kallu paanaa udhatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kallu paanaa udhatta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kaduppu yaethuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kaduppu yaethuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja seelaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja seelaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru maasi karuvaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru maasi karuvaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">vattam poatu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vattam poatu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu vaanavillu roadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu vaanavillu roadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharpoosu pazhathukke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharpoosu pazhathukke"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thenneer kaattaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thenneer kaattaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadichaa kasakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadichaa kasakkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sweetu beedaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sweetu beedaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kudichaa yaeppam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudichaa yaeppam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">goli sodaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goli sodaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idichaa usuru pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idichaa usuru pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni lorry nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni lorry nee"/>
</div>
<div class="lyrico-lyrics-wrapper">adichaa podha varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichaa podha varum"/>
</div>
<div class="lyrico-lyrics-wrapper">pondichery nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pondichery nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un sammaththa sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sammaththa sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en sambalatha tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sambalatha tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kai naluvi ponaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kai naluvi ponaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kannagiya azhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kannagiya azhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">katterumbu kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katterumbu kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumara thuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumara thuduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iduppa aatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iduppa aatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kallu paanaa udhatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kallu paanaa udhatta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kaduppu yaethuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kaduppu yaethuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vennilaa cake-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilaa cake-u"/>
</div>
<div class="lyrico-lyrics-wrapper">ena vittu thara naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena vittu thara naakku"/>
</div>
<div class="lyrico-lyrics-wrapper">kollikatta naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollikatta naakku"/>
</div>
<div class="lyrico-lyrics-wrapper">ena koppalamaa aakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena koppalamaa aakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tanjavooru thatta yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanjavooru thatta yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">pichcha kaetkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichcha kaetkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urutti vilaiyaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urutti vilaiyaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaya katta nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaya katta nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhuki vila vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhuki vila vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhamatta nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhamatta nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manakkum malaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manakkum malaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhaa puttu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhaa puttu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbi paakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbi paakkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thenavettu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenavettu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iva kannakuzhiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kannakuzhiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu pallanguzhi aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu pallanguzhi aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna muthamittu moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna muthamittu moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam saththunavu poadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam saththunavu poadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">katterumbu kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katterumbu kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kattumara thuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattumara thuduppa"/>
</div>
<div class="lyrico-lyrics-wrapper">pola iduppa aatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola iduppa aatturaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kallu paanaa udhatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kallu paanaa udhatta"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kaduppu yaethuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kaduppu yaethuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mathaappu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathaappu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sirichitu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirichitu ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kithaappu ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kithaappu ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">midhichitu ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="midhichitu ponaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">indha vappaatiya paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha vappaatiya paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pondaatiya maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pondaatiya maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan mundhaanaya modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan mundhaanaya modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan moppam pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan moppam pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni theevu ponnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni theevu ponnaa"/>
</div>
</pre>
