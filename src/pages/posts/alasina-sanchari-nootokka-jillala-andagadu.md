---
title: "alasina sanchari song lyrics"
album: "Nootokka Jillala Andagadu"
artist: "Shakthikanth Karthick"
lyricist: "Sree Vishwa"
director: "Rachakonda Vidyasagar"
path: "/albums/nootokka-jillala-andagadu-lyrics"
song: "Alasina Sanchari"
image: ../../images/albumart/nootokka-jillala-andagadu.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8Q9zxLEuulE"
type: "melody"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O alasaina sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O alasaina sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu ye daari nilabadu o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu ye daari nilabadu o saari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee bathukanu baaraasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bathukanu baaraasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadi raajesi adugide neekesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadi raajesi adugide neekesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kalalanu kaajesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalalanu kaajesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelimine kolimilo tosesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelimine kolimilo tosesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee talapuna maatesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee talapuna maatesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolo veluthuru thodesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolo veluthuru thodesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musirene thimarapu raakaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musirene thimarapu raakaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avahelana chesi kalathala saavaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avahelana chesi kalathala saavaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musirene thimarapu raakaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musirene thimarapu raakaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avahelana chesi kalathala saavaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avahelana chesi kalathala saavaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey, thana unike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey, thana unike"/>
</div>
<div class="lyrico-lyrics-wrapper">vadhulukoni kalagaapulagam dishagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadhulukoni kalagaapulagam dishagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natanalatho gadipenilaa bidiyam valachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natanalatho gadipenilaa bidiyam valachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugokati thodugukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugokati thodugukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">kuhanaagathine nadavagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuhanaagathine nadavagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sahacharine edamaya kaajesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sahacharine edamaya kaajesina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye parihaaram koranundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye parihaaram koranundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">baatasaari valayam needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baatasaari valayam needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee lopaale addhamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee lopaale addhamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">etthi choope ee nadiche dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etthi choope ee nadiche dhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erugave alasina sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erugave alasina sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu ye daari nilabadu o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu ye daari nilabadu o saari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erugave alasina sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erugave alasina sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu ye daari nilabadu o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu ye daari nilabadu o saari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayame ee gamanamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayame ee gamanamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu gelupu jathapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu gelupu jathapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Padagoduthu nilabedithe adhi okay le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padagoduthu nilabedithe adhi okay le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalachanidhe jariginadho verave vadhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalachanidhe jariginadho verave vadhili"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabadi nilakadagaa adugiduthu nuvu nadavaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabadi nilakadagaa adugiduthu nuvu nadavaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee madhiloki thongi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee madhiloki thongi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavachhe nee sisalu lotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavachhe nee sisalu lotu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee payanaana aatupotu daatukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee payanaana aatupotu daatukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee theeram meetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee theeram meetu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadalako alasina sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadalako alasina sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu ye dhaari nilabadu o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu ye dhaari nilabadu o saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadalako alasina sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadalako alasina sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu ye dhaari nilabadu o saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu ye dhaari nilabadu o saari"/>
</div>
</pre>
