---
title: "yeno yeno song lyrics"
album: "Uchithanai Muharnthaal"
artist: "D. Imman"
lyricist: "Kathirmozhi"
director: "Pugazhendhi Thangaraj"
path: "/albums/uchithanai-muharnthaal-lyrics"
song: "Yeno Yeno"
image: ../../images/albumart/uchithanai-muharnthaal.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fTSg0BQqOTE"
type: "sad"
singers:
  - Sirkazhi G. Sivachidambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yeno yeno yeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno yeno "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">varipuli inathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varipuli inathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nari nagam theerntho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari nagam theerntho"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeno yeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno yeno "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">marankothi paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marankothi paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">viruchathai saikumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viruchathai saikumo"/>
</div>
<div class="lyrico-lyrics-wrapper">adukumo adukumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adukumo adukumo"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivanai ketpatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivanai ketpatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapathai thakave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapathai thakave"/>
</div>
<div class="lyrico-lyrics-wrapper">sagunangal parpatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagunangal parpatha"/>
</div>
<div class="lyrico-lyrics-wrapper">erigiratha erikiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erigiratha erikiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">therikiratha alukuralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikiratha alukuralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavugal alum kuralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavugal alum kuralgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeno yeno yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalin kanavinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalin kanavinai"/>
</div>
<div class="lyrico-lyrics-wrapper">idi vanthu kulaikutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi vanthu kulaikutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeno yeno yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">pathungudium kuligale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathungudium kuligale"/>
</div>
<div class="lyrico-lyrics-wrapper">manitharai puthaikutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitharai puthaikutho"/>
</div>
<div class="lyrico-lyrics-wrapper">malargal magarantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malargal magarantham"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrinil saivatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrinil saivatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thee patta kayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee patta kayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vel vanthu paivatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vel vanthu paivatha"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaigiratha tholaikiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaigiratha tholaikiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">thodargiratha yugam yugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodargiratha yugam yugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavinil theipiraigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavinil theipiraigal"/>
</div>
</pre>
