---
title: "parvathi thanayudavo song lyrics"
album: "Needi Naadi Oke Katha"
artist: "Suresh Bobbili"
lyricist: "Kandikonda"
director: "Venu Udugula"
path: "/albums/needi-naadi-oke-katha-lyrics"
song: "Parvathi Thanayudavo"
image: ../../images/albumart/needi-naadi-oke-katha.jpg
date: 2018-03-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IuK9OpGri4Y"
type: "mass"
singers:
  -	Suresh Bobbili
  - Shankar Babu
  - Sathyavathi (Mangli)
  - Ranjani Sivakumar Siddareddy
  - Naresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Parvathi Thanayudavo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parvathi Thanayudavo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parvathi Thanayudavo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parvathi Thanayudavo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeshvara Puthrunivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeshvara Puthrunivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeshvara Puthrunivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeshvara Puthrunivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sdhuvula Chendhrudivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sdhuvula Chendhrudivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sdhuvula Chendhrudivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sdhuvula Chendhrudivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rankula Rajavo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rankula Rajavo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rankula Rajavo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rankula Rajavo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Degree Paasayithivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Degree Paasayithivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilliki Repeithivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilliki Repeithivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Degree Paasayithivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Degree Paasayithivo Gajananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilliki Repeithivo Gajananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilliki Repeithivo Gajananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allo Neradallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allo Neradallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Podisindhenti Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Podisindhenti Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Allallo Neraadi Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allallo Neraadi Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neradi Idu Sinchindhenti Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neradi Idu Sinchindhenti Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">IAS Aa Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IAS Aa Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari IPS Aa Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari IPS Aa Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Allallo Neradi Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allallo Neradi Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neradi Idu Peekindhenti Allo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neradi Idu Peekindhenti Allo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chel Adhbutha Praveenyamu Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chel Adhbutha Praveenyamu Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeperu Charithaarthamu Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeperu Charithaarthamu Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Keerthi Shaashvathamu Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Keerthi Shaashvathamu Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini Eruganidhi Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini Eruganidhi Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Munumundhu Jaraganidhi Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munumundhu Jaraganidhi Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sadhuvu Sakknidhi Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sadhuvu Sakknidhi Jangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaana Sithramidhi Jangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaana Sithramidhi Jangama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hari Hari Brahmandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Brahmandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmandam Veerathvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmandam Veerathvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Brahmandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Brahmandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmandam Veerathvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmandam Veerathvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Brahmandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Brahmandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Brahmandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Brahmandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hari Hari Brahmandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hari Hari Brahmandam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chadhuvula Bidda Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhuvula Bidda Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kithaabu Kingu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithaabu Kingu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Aksharayodhudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Aksharayodhudave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennuku Friendu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku Friendu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusthaka Ghanudavu Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthaka Ghanudavu Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Priya Puthrudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Priya Puthrudave"/>
</div>
<div class="lyrico-lyrics-wrapper">Akhilamu Nikhilamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akhilamu Nikhilamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaramu Nee Gelupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaramu Nee Gelupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamu Nithyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamu Nithyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choochule Neevaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choochule Neevaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadhuvula Bidda Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhuvula Bidda Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kithaabu Kingu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithaabu Kingu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Aksharayodhudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Aksharayodhudave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennuku Friendu Neevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku Friendu Neevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusthaka Ghanudavu Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthaka Ghanudavu Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Priya Puthrudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Priya Puthrudave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Emaindhira Bangaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Emaindhira Bangaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoo Saraswathi Maatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoo Saraswathi Maatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Saraswathi Maatha Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saraswathi Maatha Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichi Dheevinchindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichi Dheevinchindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunaa Saraswathi Maatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunaa Saraswathi Maatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Pilichi Dheevinchindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Pilichi Dheevinchindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy Raa Dharuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy Raa Dharuvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vey Raa Veyyi Theenmareyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vey Raa Veyyi Theenmareyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenmaar Khaan Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenmaar Khaan Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoomdhaam Cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoomdhaam Cheyyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheyyi Raa Cheyyi Racche Cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi Raa Cheyyi Racche Cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangule Challi Lolley Cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule Challi Lolley Cheyyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangule Vaalvaay Challaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangule Vaalvaay Challaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalvaay Lolle Vaalvaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalvaay Lolle Vaalvaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyara Vaalvaay Nene Thopu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyara Vaalvaay Nene Thopu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">King Raa Vaalvaay Naake Sati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Raa Vaalvaay Naake Sati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhura Vaalvaay Nuvve Thopuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhura Vaalvaay Nuvve Thopuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">King Raa Vaalvaay Neeke Sati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Raa Vaalvaay Neeke Sati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhura Vaalvaay Nuvve Thopuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhura Vaalvaay Nuvve Thopuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">King Raa Vaalvaay Thopu Ra Valvaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Raa Vaalvaay Thopu Ra Valvaay"/>
</div>
<div class="lyrico-lyrics-wrapper">King Raa Vaalvaay Naake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="King Raa Vaalvaay Naake"/>
</div>
<div class="lyrico-lyrics-wrapper">Sati Ledhuraa Vaalvaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sati Ledhuraa Vaalvaay"/>
</div>
</pre>
