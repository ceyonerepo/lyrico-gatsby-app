---
title: "yen maraikkirai song lyrics"
album: "Kalathil Santhippom"
artist: "Yuvan Shankar Raja"
lyricist: "Pa. Vijay"
director: "N. Rajasekar"
path: "/albums/kutty-kalathil-santhippom-lyrics"
song: "Yen Maraikkirai"
image: ../../images/albumart/kalathil-santhippom.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zMiwUDto5Xs"
type: "Sad"
singers:
  - Aslam Abdul Majeed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen maraikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen maraikkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadhalil irukkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadhalil irukkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee marukkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee marukkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanal ennai ninaikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanal ennai ninaikkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho oru vazhi manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho oru vazhi manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesa vandhu vandhu urasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa vandhu vandhu urasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thalli thalli nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thalli thalli nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu mattum ennal aagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu mattum ennal aagala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo enna solli mudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo enna solli mudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiye naalum vidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiye naalum vidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu thunda ulla udaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu thunda ulla udaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna purinjukka thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna purinjukka thonala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai asaikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai asaikkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum pol sirikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum pol sirikkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh orr nodiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh orr nodiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul naan pudhaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul naan pudhaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha paravai ennai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha paravai ennai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum podhum undhan saayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum podhum undhan saayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamal enna seiyya naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamal enna seiyya naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum sonna varthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum sonna varthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru kooda tholaiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru kooda tholaiyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin veettil semithene naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin veettil semithene naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En palaiya naatkal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En palaiya naatkal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai konjam konjamai kolgirathedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai konjam konjamai kolgirathedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un varugai ullangaiyil mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un varugai ullangaiyil mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnan siru iragai sergirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnan siru iragai sergirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanammo nadathinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanammo nadathinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar marapathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar marapathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli sendrum nerunginai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli sendrum nerunginai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol innum ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol innum ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna seiya ninaikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna seiya ninaikkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh nee sollavittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh nee sollavittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadhalai tholaikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadhalai tholaikkirai"/>
</div>
</pre>
