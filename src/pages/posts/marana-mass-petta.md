---
title: 'marana mass song lyrics'
album: 'Petta'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'Karthik Subbaraj'
path: '/albums/petta-song-lyrics'
song: 'Marana Mass'
image: ../../images/albumart/petta.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/88iypMO9H7g"
type: 'mass'
singers: 
- S P Balasubrahmanyam
- Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thatlaatam dhaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thatlaatam dhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharlaanga song ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharlaanga song ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara vandhaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara vandhaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha veanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Pollaadha veanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thatlaatam dhaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thatlaatam dhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharlaanga song ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharlaanga song ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara vandhaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara vandhaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha veanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Pollaadha veanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thimuraama vaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimuraama vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulpaaidu veenga
<input type="checkbox" class="lyrico-select-lyric-line" value="Bulpaaidu veenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappoda nippaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Morappoda nippaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaama ponga
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttaama ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gethaa nadandhu varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Gethaa nadandhu varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate ah ellaam kadandhu varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Gate ah ellaam kadandhu varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha vediya onnu podu thillaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Tha vediya onnu podu thillaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sleeve ah surutti varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Sleeve ah surutti varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Collor ah thaan peratti varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Collor ah thaan peratti varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiya siluppi uttaa yerum ullaara…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiya siluppi uttaa yerum ullaara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranam massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranam massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku avan thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukku avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Porandhu varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppu moraiyaa vuzhanum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Steppu moraiyaa vuzhanum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thatlaatam dhaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thatlaatam dhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharlaanga song ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharlaanga song ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara vandhaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara vandhaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha veanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Pollaadha veanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thimuraama vaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimuraama vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulpaaidu veenga
<input type="checkbox" class="lyrico-select-lyric-line" value="Bulpaaidu veenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappoda nippaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Morappoda nippaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaama ponga
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttaama ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hei hei hei …(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hei hei hei"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Evandaa mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Evandaa mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Evandaa keezha
<input type="checkbox" class="lyrico-select-lyric-line" value="Evandaa keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa uyiraiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaa uyiraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaavae paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnaavae paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mudinja varaikum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudinja varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba seru
<input type="checkbox" class="lyrico-select-lyric-line" value="Anba seru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalayil yethi vachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalayil yethi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadum ooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kondaadum ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nyaayam irundhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nyaayam irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuththu variyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuththu variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna madhippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna madhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu en pazhakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu en pazhakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaala izhuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaala izhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyara nenachaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyara nenachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paya sir
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketta paya sir"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiyaa idikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idiyaa idikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Gethaa nadandhu varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Gethaa nadandhu varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate ah ellaam kadandhu varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Gate ah ellaam kadandhu varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha vediya onnu podu thillaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Tha vediya onnu podu thillaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sleeve ah surutti varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Sleeve ah surutti varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Collor ah thaan peratti varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Collor ah thaan peratti varaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiya siluppi uttaa yerum ullaara…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiya siluppi uttaa yerum ullaara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranam massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranam massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku avan thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukku avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Porandhu varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppu moraiyaa vuzhanum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Steppu moraiyaa vuzhanum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thatlaatam dhaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thatlaatam dhaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharlaanga song ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharlaanga song ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara vandhaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara vandhaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha veanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Pollaadha veanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thimuraama vaanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Thimuraama vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulpaaidu veenga
<input type="checkbox" class="lyrico-select-lyric-line" value="Bulpaaidu veenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappoda nippaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Morappoda nippaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaama ponga
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttaama ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ittal pathiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittal pathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittal pathiri sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittal pathiri sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Salphilo
<input type="checkbox" class="lyrico-select-lyric-line" value="Salphilo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hei hei hei …(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hei hei hei …(2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ittalu pathiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittalu pathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittalu pathiri sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittalu pathiri sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Salphilo
<input type="checkbox" class="lyrico-select-lyric-line" value="Salphilo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hei hei hei …(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hei hei hei …(2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ittal pathiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittal pathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittal pathiri sollu puthiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittal pathiri sollu puthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Salphilo
<input type="checkbox" class="lyrico-select-lyric-line" value="Salphilo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hei hei hei …(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hei hei hei"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranam massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranam massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukku avan thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukku avan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Porandhu varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Massu maranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Massu maranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Toughu tharanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Toughu tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppu moraiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Steppu moraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu vuzhanum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu vuzhanum…"/>
</div>
</pre>