---
title: "kalakalakuthu kalakalakuthu song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Kalakalakuthu Kalakalakuthu"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0CdVfNxNz5s"
type: "happy"
singers:
  - Mano
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalakalakkuthu Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalakkuthu Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusu Satham Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu Satham Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Veettukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Veettukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai Vanthuvittaal Paathukkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai Vanthuvittaal Paathukkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alagaana Mallipoo Ponna Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagaana Mallipoo Ponna Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkathil Rosavaa Maaruthupaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkathil Rosavaa Maaruthupaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Konjam Santhanam Poosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Konjam Santhanam Poosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoda Kaadhal Sangathi Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoda Kaadhal Sangathi Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi Unn Kurumbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Unn Kurumbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Miga Rasippaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Miga Rasippaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarugal Seithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarugal Seithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyai Pola Ival Kandippaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyai Pola Ival Kandippaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi Iravil Thaamatham Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Iravil Thaamatham Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukku Vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukku Vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Motti Poda Solli Thandippaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motti Poda Solli Thandippaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirthu Ennai Jeypatharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthu Ennai Jeypatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Illai Munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Illai Munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Oru Sol Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Oru Sol Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangiduven Anbaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangiduven Anbaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanukku Nandri Solvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanukku Nandri Solvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Namakku Kidaithathu Oru Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Namakku Kidaithathu Oru Varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakalakkuthu Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalakkuthu Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusu Satham Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu Satham Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Veettukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Veettukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai Vanthuvittaal Paathukkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai Vanthuvittaal Paathukkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Annan Thozh Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Annan Thozh Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poomaalai Aaga Aanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomaalai Aaga Aanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaalae Nam Veettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae Nam Veettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalum Raani Aanaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalum Raani Aanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyil Supirabhatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyil Supirabhatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkum Inimel Nam Veettil Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum Inimel Nam Veettil Eppodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakalakkuthu Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalakkuthu Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusu Satham Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu Satham Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Veettukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Veettukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai Vanthuvittaal Paathukkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai Vanthuvittaal Paathukkoo"/>
</div>
</pre>
