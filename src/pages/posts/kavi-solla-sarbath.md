---
title: "kavi solla song lyrics"
album: "Sarbath"
artist: "Ajesh"
lyricist: "Ku. Karthick"
director: "Prabhakaran"
path: "/albums/sarbath-song-lyrics"
song: "Kavi Solla"
image: ../../images/albumart/sarbath.jpg
date: 2021-04-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cOxXpK4D2I4"
type: "Sad"
singers:
  - Ajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Podhum undhan nayanam endhan oonjalaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum undhan nayanam endhan oonjalaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengi pogum thuyarangal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengi pogum thuyarangal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum indha piravi payanam theerndhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum indha piravi payanam theerndhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum konjam pun muruval thandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum konjam pun muruval thandhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivodu pesa manam pazhagiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivodu pesa manam pazhagiyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Analodu neengi uyir nanaigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analodu neengi uyir nanaigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaandham ennai dhinam sudugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaandham ennai dhinam sudugiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor vaarthai pesividu alaigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor vaarthai pesividu alaigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavi solla sonnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavi solla sonnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi ondril solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi ondril solven"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal solla kaalam podhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal solla kaalam podhadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal kondu ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal kondu ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai naanum serven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai naanum serven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadhal vellam neendha theeradhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadhal vellam neendha theeradhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiveli thaan neelamal theeradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli thaan neelamal theeradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru manangal sehramal pogadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru manangal sehramal pogadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavi solla sonnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavi solla sonnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi ondril solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi ondril solven"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal solla kaalam podhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal solla kaalam podhadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal kondu ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal kondu ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai naanum serven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai naanum serven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadhal vellam neendha theeradhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadhal vellam neendha theeradhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai theendi ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai theendi ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilam pesum penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam pesum penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai seivadho unnudan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai seivadho unnudan pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhal thaandi ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhal thaandi ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai aagum kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai aagum kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai thaandi nee poga naan yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai thaandi nee poga naan yenginen"/>
</div>
</pre>
