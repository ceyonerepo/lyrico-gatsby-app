---
title: "thaya ella seya song lyrics"
album: "Appathava Aattaya Pottutanga"
artist: "Asvamitra"
lyricist: "S.Selvakumaar"
director: "N. Stephen Rangaraj"
path: "/albums/appathava-aattaya-pottutanga-lyrics"
song: "Thaya Ella Seya"
image: ../../images/albumart/appathava-aattaya-pottutanga.jpg
date: 2021-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tPvOsVTDBu0"
type: "sad"
singers:
  - Ramani Ammal
  - Dhivya Sathiyamurthi 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">anbala aasai kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbala aasai kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">ariva mooti valarthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariva mooti valarthene"/>
</div>
<div class="lyrico-lyrics-wrapper">panbala pasam katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbala pasam katti"/>
</div>
<div class="lyrico-lyrics-wrapper">nesam kotti thanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesam kotti thanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">eathivitta eani engu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eathivitta eani engu"/>
</div>
<div class="lyrico-lyrics-wrapper">nathiyindru thavikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathiyindru thavikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiketta pullai engu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiketta pullai engu"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkikittu mulikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkikittu mulikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru ingu puthi solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru ingu puthi solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru vanthu kooti sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru vanthu kooti sella"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru ingu puthi solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru ingu puthi solla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru vanthu kooti sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru vanthu kooti sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayaa ella seya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayaa ella seya"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yarai yar kappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yarai yar kappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thayum ingu seythan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayum ingu seythan"/>
</div>
<div class="lyrico-lyrics-wrapper">intru muthumaithan thotrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intru muthumaithan thotrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaiyaga kaththale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaiyaga kaththale"/>
</div>
<div class="lyrico-lyrics-wrapper">sumaiyaga paththane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumaiyaga paththane"/>
</div>
<div class="lyrico-lyrics-wrapper">antru viral piditha kaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antru viral piditha kaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">kural kodukka maruththathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kural kodukka maruththathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu vaalum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu vaalum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">athil pogum thoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil pogum thoram"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nanum serava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nanum serava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayaa ella seya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayaa ella seya"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yarai yar kappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yarai yar kappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thayum ingu seythan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayum ingu seythan"/>
</div>
<div class="lyrico-lyrics-wrapper">intru muthumaithan thotrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intru muthumaithan thotrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallam illatha ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallam illatha ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnu thanichu ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnu thanichu ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennam ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennam ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla mela tholaicha thenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla mela tholaicha thenna"/>
</div>
<div class="lyrico-lyrics-wrapper">thanna thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanna thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">pasi maranthu mulichi ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi maranthu mulichi ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla vanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla vanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">oga mudiyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oga mudiyame"/>
</div>
<div class="lyrico-lyrics-wrapper">thavicha thenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavicha thenna"/>
</div>
<div class="lyrico-lyrics-wrapper">en ekkam theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ekkam theera"/>
</div>
<div class="lyrico-lyrics-wrapper">nalai unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalai unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">magana porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magana porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">peththa pasathoda aalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peththa pasathoda aalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum konjam kattanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum konjam kattanum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkave valuren enguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkave valuren enguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayaa ella seya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayaa ella seya"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yarai yar kappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yarai yar kappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thayum ingu seythan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayum ingu seythan"/>
</div>
<div class="lyrico-lyrics-wrapper">intru muthumaithan thotrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intru muthumaithan thotrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaiyaga kaththale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaiyaga kaththale"/>
</div>
<div class="lyrico-lyrics-wrapper">sumaiyaga paththane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumaiyaga paththane"/>
</div>
<div class="lyrico-lyrics-wrapper">antru viral piditha kaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antru viral piditha kaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">kural kodukka maruththathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kural kodukka maruththathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu vaalum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu vaalum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">athil pogum thoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil pogum thoram"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nanum serava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nanum serava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayaa ella seya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayaa ella seya"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu yarai yar kappathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu yarai yar kappathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thayum ingu seythan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayum ingu seythan"/>
</div>
<div class="lyrico-lyrics-wrapper">intru muthumaithan thotrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intru muthumaithan thotrathu"/>
</div>
</pre>
