---
title: "valliamma song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Pa. Vijay"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Valliamma"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jVK6Zv7AQTE"
type: "happy"
singers:
  - Shankar Mahadevan
  - Chinnaponnu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maambazhathukku kobappattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maambazhathukku kobappattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu ninna idam indha pazhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu ninna idam indha pazhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam thamizhnaattu annan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam thamizhnaattu annan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku valli dheivaanaiyudan bavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku valli dheivaanaiyudan bavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanaga thirukkalyaana kadhai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanaga thirukkalyaana kadhai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaa chinnaponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaa chinnaponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idha ketka vandha ungalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha ketka vandha ungalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthil Murugan thandhiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthil Murugan thandhiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nal arula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nal arula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo valliyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo valliyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo valliyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo valliyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma dheivaanai thaayae ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma dheivaanai thaayae ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arogaraa saththam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arogaraa saththam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae aruluvandhu aadumammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae aruluvandhu aadumammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sooranaiya soodaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sooranaiya soodaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulukkedukka thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulukkedukka thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paramasivan paarvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paramasivan paarvaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhaman aaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhaman aaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhuthaandaa murugan kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhuthaandaa murugan kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinjidhaa sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjidhaa sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo valliyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo valliyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma dheivaanai thaayae ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma dheivaanai thaayae ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arogaraa saththam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arogaraa saththam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae aruluvandhu aadumammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae aruluvandhu aadumammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey jinukku nakka naka naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jinukku nakka naka naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukku nakka naka naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukku nakka naka naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukku nakka naka naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukku nakka naka naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukku nakka naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukku nakka naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachcha malaiyoaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha malaiyoaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Valli vandhu paattu padichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valli vandhu paattu padichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha kandukkitta murugan vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha kandukkitta murugan vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal padichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal padichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku vel vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku vel vel"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku vel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venthaadi kizhavanappol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venthaadi kizhavanappol"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham kattinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham kattinaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma valliyammaa kaippudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma valliyammaa kaippudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa thattinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa thattinaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Escape-u aanadhuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape-u aanadhuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahun hahun hahun hahun hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahun hahun hahun hahun hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Escape-u aanadhuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape-u aanadhuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valliyun dhaanpaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valliyun dhaanpaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Elephanttaa vandhaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elephanttaa vandhaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganabadhi babaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganabadhi babaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valliyamma anjinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valliyamma anjinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murugan kitta odunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugan kitta odunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumurugan manaiviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumurugan manaiviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruthaniyila kooduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruthaniyila kooduna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaandaa valli kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaandaa valli kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purinjidhaa Sollungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjidhaa Sollungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo valliyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo valliyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma dheivaanai thaayae ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma dheivaanai thaayae ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arogaraa saththam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arogaraa saththam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae aruluvandhu aadumammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae aruluvandhu aadumammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila soorabathman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila soorabathman"/>
</div>
<div class="lyrico-lyrics-wrapper">Teroraaga aattippadaichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teroraaga aattippadaichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan indhiranai podaavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan indhiranai podaavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu adichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu adichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku vel vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku vel vel"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku vel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanoda thaayum vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanoda thaayum vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velu koduththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velu koduththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta sooranaithaan pottuthallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta sooranaithaan pottuthallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyakkoduthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyakkoduthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enimiyaaga ninnaanae ho hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enimiyaaga ninnaanae ho hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enimiyaaga ninnaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enimiyaaga ninnaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooranum maramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranum maramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Encounter paannaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Encounter paannaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Murugan varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugan varamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooranaiyum konnu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranaiyum konnu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval mayil aakkunaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval mayil aakkunaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhiranain magalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhiranain magalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisaaga vaanginaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisaaga vaanginaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaan dheivaanai kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan dheivaanai kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinjidhaa Sollungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjidhaa Sollungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo valliyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo valliyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma dheivaanai thaayae ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma dheivaanai thaayae ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arogaraa saththam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arogaraa saththam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae aruluvandhu aadumammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae aruluvandhu aadumammaa"/>
</div>
</pre>
