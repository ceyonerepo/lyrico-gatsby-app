---
title: "vaazhum valluvarae song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Mohan Rajan"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Vaazhum Valluvarae"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/70eoOi6MaRM"
type: "happy"
singers:
  - V. Priyadarshini
  - R. Apoorva
  - N. Aparajitha
  - Jyothsna Akilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaazhum valluvarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum valluvarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralilae vaazhum valluvarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralilae vaazhum valluvarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai bharadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai bharadhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin aasai maakaviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin aasai maakaviyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhin amudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhin amudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emai mayakkum paadalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emai mayakkum paadalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum avvaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum avvaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh vanangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh vanangidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhum valluvarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum valluvarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralilae vaazhum valluvarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralilae vaazhum valluvarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai bharadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai bharadhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin aasai maakaviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin aasai maakaviyae"/>
</div>
</pre>
