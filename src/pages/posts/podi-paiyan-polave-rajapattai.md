---
title: "podi paiyan polave song lyrics"
album: "Rajapattai"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/rajapattai-lyrics"
song: "Podi Paiyan Polave"
image: ../../images/albumart/rajapattai.jpg
date: 2011-12-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZWjEYu0AyoU"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Podi paiyan polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi paiyan polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam indru thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam indru thulludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu unnai thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu unnai thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi vanthadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi vanthadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavilla kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavilla kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharacholli kenjudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharacholli kenjudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam unnai kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam unnai kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solludhae settaigal seiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solludhae settaigal seiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogamalae kaadhal pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamalae kaadhal pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugindradhae sol pechai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugindradhae sol pechai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamal eppothumae thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamal eppothumae thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannalae tharugindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalae tharugindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podi paiyan polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi paiyan polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam indru thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam indru thulludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu unnai thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu unnai thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi vanthadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi vanthadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavilla kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavilla kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharacholli kenjudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharacholli kenjudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam unnai kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam unnai kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solludhae settaigal seiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solludhae settaigal seiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadai vandi pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai vandi pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum oru thaaiyaai kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum oru thaaiyaai kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakullae odakanden silanaalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakullae odakanden silanaalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai uppu mootai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai uppu mootai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookum mudhal aalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookum mudhal aalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal sumanthengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal sumanthengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga kanden pagal raavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga kanden pagal raavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivillai en moolaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivillai en moolaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyae adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyae adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyae angae eppothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyae angae eppothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbae neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbae neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogamalae kaadhal pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamalae kaadhal pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugindradhae sol pechai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugindradhae sol pechai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamal eppothumae thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamal eppothumae thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannalae tharugindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalae tharugindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangaamal paadal keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangaamal paadal keten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumbodhae theneer keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumbodhae theneer keten"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimelae ketpen unnaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelae ketpen unnaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhae nee pogumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhae nee pogumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaipaathai pookal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaipaathai pookal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthu vaikum kannayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthu vaikum kannayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkaamal oduven bayanthoduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkaamal oduven bayanthoduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae nee enbathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae nee enbathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nanaiyakoodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nanaiyakoodadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogamalae kaadhal pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamalae kaadhal pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugindradhae oh hoo sol pechai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugindradhae oh hoo sol pechai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamal eppothumae thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamal eppothumae thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannalae tharugindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalae tharugindradhae"/>
</div>
</pre>
