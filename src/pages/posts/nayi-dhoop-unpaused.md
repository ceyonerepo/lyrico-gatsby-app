---
title: "nayi dhoop song lyrics"
album: "Unpaused"
artist: "Tanishk Bagchi"
lyricist: "Rashmi Virag"
director: "Tannishtha Chatterjee - Raj & DK - Nitya Mehra - Nikkhil Advani - Avinash Arun"
path: "/albums/unpaused-lyrics"
song: "Nayi Dhoop"
image: ../../images/albumart/unpaused.jpg
date: 2020-12-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/SrIe5Ap1MzA"
type: "love"
singers:
  - Zara Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maana ke aajkal rootha waqt hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana ke aajkal rootha waqt hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi ka safar thoda sakht hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ka safar thoda sakht hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ruka hua hai iss jahaan ka karwan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruka hua hai iss jahaan ka karwan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh raat jaayegi guzar tu dekhna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh raat jaayegi guzar tu dekhna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayi dhoop hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi dhoop hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khidkiyon pe isse aane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khidkiyon pe isse aane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhere hain kis kaam ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhere hain kis kaam ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu inhein jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu inhein jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayi dhoop hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi dhoop hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khidkiyon pe isse aane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khidkiyon pe isse aane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhere hain kis kaam ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhere hain kis kaam ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu inhein jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu inhein jaane de"/>
</div>
</pre>
