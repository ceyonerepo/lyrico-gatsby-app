---
title: "suduraala teeraala paata song lyrics"
album: "Ye Mantram Vesave"
artist: "Abdus Samad"
lyricist: "Arun Vemuri"
director: "Shridhar Marri"
path: "/albums/ye-mantram-vesave-lyrics"
song: "Suduraala Teeraala Paata"
image: ../../images/albumart/ye-mantram-vesave.jpg
date: 2018-03-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/W47n-P_QR48"
type: "melody"
singers:
  -	Pranavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Suduraala Teeraala Paata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Suduraala Teeraala Paata "/>
</div>
<div class="lyrico-lyrics-wrapper">Sudai Pedavi Chera Veedenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudai Pedavi Chera Veedenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Mugagaa Nilichi Mainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Mugagaa Nilichi Mainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Manase Thelipenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Manase Thelipenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vilaapaala Punthallo Thaaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vilaapaala Punthallo Thaaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Navve Ruvvi Ilaa Raalenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navve Ruvvi Ilaa Raalenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Alaa Maaya Lokaanni Vidichi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa Maaya Lokaanni Vidichi "/>
</div>
<div class="lyrico-lyrics-wrapper">Melukavalo Oka Kalalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukavalo Oka Kalalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thapisthunna O Praanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapisthunna O Praanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shramisthunna Naa Nesthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shramisthunna Naa Nesthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalisthundha Anveshanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalisthundha Anveshanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Suduraala Teeraala Paata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Suduraala Teeraala Paata "/>
</div>
<div class="lyrico-lyrics-wrapper">Sudai Pedavi Chera Veedenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudai Pedavi Chera Veedenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Mugagaa Nilichi Mainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Mugagaa Nilichi Mainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Manase Thelipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Manase Thelipenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shilaga Migili Ragile Manasu Mellagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shilaga Migili Ragile Manasu Mellagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Karuguthonda Sadde Cheyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuguthonda Sadde Cheyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula Jaaru Nayagaaraala Sakshigaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Jaaru Nayagaaraala Sakshigaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Eduru Chupulanni Pogada Pulugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduru Chupulanni Pogada Pulugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eri Daachi Haaraalallanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eri Daachi Haaraalallanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Alaa Sande Chikatlu Karigi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Alaa Sande Chikatlu Karigi "/>
</div>
<div class="lyrico-lyrics-wrapper">Priyaa Vekuve Viriyagaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyaa Vekuve Viriyagaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Usha Raagamai Gunde Thaduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usha Raagamai Gunde Thaduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vaakita Nilichevaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vaakita Nilichevaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Suduraala Teeraala Paata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Suduraala Teeraala Paata "/>
</div>
<div class="lyrico-lyrics-wrapper">Sudai Pedavi Chera Veedenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudai Pedavi Chera Veedenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Mugagaa Nilichi Mainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Mugagaa Nilichi Mainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Manase Thelipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Manase Thelipenaa"/>
</div>
</pre>
