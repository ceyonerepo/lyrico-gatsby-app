---
title: "verasa pogayile song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Parvathy"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Verasa Pogayile"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1SxFSDdHv1c"
type: "Love"
singers:
  - D. Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Verasa Pogayile Puthusa Poravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa Pogayile Puthusa Poravale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Varaiyila Kulir Edukala Pennaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Varaiyila Kulir Edukala Pennaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Sirichathum Thala Oraiyuthu Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirichathum Thala Oraiyuthu Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pechu Moochu Enga Kaanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechu Moochu Enga Kaanaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verasa Pogayile Puthusa Poravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa Pogayile Puthusa Poravale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Varaiyila Kulir Edukala Pennaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Varaiyila Kulir Edukala Pennaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Sirichathum Thala Oraiyuthu Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirichathum Thala Oraiyuthu Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pechu Moochu Enga Kaanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechu Moochu Enga Kaanaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Naana Enna Palasu Ellam Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Naana Enna Palasu Ellam Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Sandhegangal Undaaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Sandhegangal Undaaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thindaatama Illa Thullaatama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thindaatama Illa Thullaatama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Vittu Rendu Kaal Thaavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Vittu Rendu Kaal Thaavuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Naan Pogum Paadhai Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Naan Pogum Paadhai Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippothu Nirkaamal Yen Neeluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippothu Nirkaamal Yen Neeluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullam Lesaaga Kai Neeluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullam Lesaaga Kai Neeluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verasa Pogaiyile Puthusa Poravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa Pogaiyile Puthusa Poravale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Kachithama Enna Pichu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Kachithama Enna Pichu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachu Sekurathu Un Velaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachu Sekurathu Un Velaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu Vattarathil Thantha Pattam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu Vattarathil Thantha Pattam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Nool Aruntha Kaathaadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Nool Aruntha Kaathaadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethoda Nee Vera Naa Veraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethoda Nee Vera Naa Veraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo Nee En Nenjin Mer Kooraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Nee En Nenjin Mer Kooraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennulle Nee Paadhi Naan Meedhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle Nee Paadhi Naan Meedhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verasa Pogayile Puthusa Poravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa Pogayile Puthusa Poravale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Varaiyila Kulir Edukala Pennaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Varaiyila Kulir Edukala Pennaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Sirichathum Thala Oraiyuthu Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirichathum Thala Oraiyuthu Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pechu Moochu Enga Kaanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechu Moochu Enga Kaanaala"/>
</div>
</pre>
