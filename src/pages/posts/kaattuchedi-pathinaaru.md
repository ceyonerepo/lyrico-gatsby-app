---
title: "kaattuchedi song lyrics"
album: "Pathinaaru"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Sabapathy - Dekshinamurthy"
path: "/albums/pathinaaru-lyrics"
song: "Kaattuchedi"
image: ../../images/albumart/pathinaaru.jpg
date: 2011-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lhBMwgN-w9I"
type: "happy"
singers:
  - Karthik Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaattu Chedikku Kaaval Kidachache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Chedikku Kaaval Kidachache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Kullae Paadam Padichachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Kullae Paadam Padichachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Palaya Ulagam Ippa Puthusa Theriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Palaya Ulagam Ippa Puthusa Theriyuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Intha Veppathirku Paerenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Intha Veppathirku Paerenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Ithu Anbu Thaane Verenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Ithu Anbu Thaane Verenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Chedikku Kaaval Kidachache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Chedikku Kaaval Kidachache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Kullae Paadam Padichachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Kullae Paadam Padichachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruthi Panju Pola Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruthi Panju Pola Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha Thanni Pola Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thanni Pola Vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Thavira Aethum Theriyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Thavira Aethum Theriyathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poigal Illa Veru Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poigal Illa Veru Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poati Indri Nenjam Palagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poati Indri Nenjam Palagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanmurai'ku Velai Kidayathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmurai'ku Velai Kidayathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Moota Aerikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Moota Aerikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Suthi Paarkum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Suthi Paarkum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Thanda Aethum Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thanda Aethum Indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Thodaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Thodaruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aathi Manam Thuli Thuli Kudikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Manam Thuli Thuli Kudikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi Aethi Dhinam Kathu Pola Parakuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Aethi Dhinam Kathu Pola Parakuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallikooda Padam Namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikooda Padam Namaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidatha Vazhkai Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidatha Vazhkai Iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Paadam Ingae Nadakirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Paadam Ingae Nadakirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu Onrae Saami Kanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Onrae Saami Kanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuku Eedu Enna Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuku Eedu Enna Iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodutham Vaangum Manitham Jeyikirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodutham Vaangum Manitham Jeyikirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Paruka Aanapothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paruka Aanapothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paguthu Unnum Nenjam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paguthu Unnum Nenjam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Indri Kaathalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Indri Kaathalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhum Manasuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhum Manasuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Aathi Rendum Vetkapattu Sirikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aathi Rendum Vetkapattu Sirikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam Aethi Manam Ellai Thaandi Parakuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Aethi Manam Ellai Thaandi Parakuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattu Chedikku Kaaval Kidachache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Chedikku Kaaval Kidachache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Kullae Paadam Padichachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Kullae Paadam Padichachae"/>
</div>
</pre>
