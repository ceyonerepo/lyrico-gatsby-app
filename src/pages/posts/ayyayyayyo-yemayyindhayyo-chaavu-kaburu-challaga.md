---
title: "ayyayyayyo song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Karunakar Adigarla"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "Ayyayyayyo Yemayyindhayyo"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YiZoNQe0Rtg"
type: "love"
singers:
 - Aditya Tadepalli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hayyayyayyo yemayyindhayyo praananike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyayyayyo yemayyindhayyo praananike"/>
</div>
<div class="lyrico-lyrics-wrapper">Silipi sethabadaiayyindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silipi sethabadaiayyindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayyo hayyayyo yento yemmayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyo hayyayyo yento yemmayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee chethilo bujji bommaipoindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethilo bujji bommaipoindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitti siraaku purugu seethakokainattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitti siraaku purugu seethakokainattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangula rekkala bathuke modhalaindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangula rekkala bathuke modhalaindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattilo puttina siguru marri chettainattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattilo puttina siguru marri chettainattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu aa mabbula dhaka egirellindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu aa mabbula dhaka egirellindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramlo sindhe sepa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramlo sindhe sepa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegisi ningini chusinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegisi ningini chusinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundekemo lokamanthaa kotthagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundekemo lokamanthaa kotthagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedu nela vaana jalley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedu nela vaana jalley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaki pacchani polamainattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaki pacchani polamainattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhatisari saavu kabure challagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhatisari saavu kabure challagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayyayyayyo yemayyindhayyo praananike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyayyayyo yemayyindhayyo praananike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silipi sethabadaiayyindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silipi sethabadaiayyindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayyo hayyayyo yento yemmayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyo hayyayyo yento yemmayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chethilo bujji bommaipoindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethilo bujji bommaipoindho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buradha seruvalle untadhe na bathuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buradha seruvalle untadhe na bathuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru kamalaalu poosaayi aa neetike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru kamalaalu poosaayi aa neetike"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuku minugurulaa saage ee na kathake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuku minugurulaa saage ee na kathake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punnamiloni vennellu addhaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnamiloni vennellu addhaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelapai ralina sinuku nadhula nuragainattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelapai ralina sinuku nadhula nuragainattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhame selayeralle theese parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhame selayeralle theese parugu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesaviki endina modu poola kommainattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesaviki endina modu poola kommainattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasithanamtho madhalayye janme ipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasithanamtho madhalayye janme ipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatu potu alallona theeram theliyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatu potu alallona theeram theliyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padavaivunna anthalone prema dheevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaivunna anthalone prema dheevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhurayaave ee moratodi gundekaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhurayaave ee moratodi gundekaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalike lenidhe anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalike lenidhe anukunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippudento uppenalle ponguthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudento uppenalle ponguthondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayyayyayyo yemayyindhayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyayyayyo yemayyindhayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakaasham anthandham edhurayyindayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaasham anthandham edhurayyindayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayyo hayyayyo yento yemmayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayyo hayyayyo yento yemmayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaratha thiragesindhayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaratha thiragesindhayyo"/>
</div>
</pre>
