---
title: "vaan mazhaiyin thuligal song lyrics"
album: "Narthagi"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "G. Vijayapadma"
path: "/albums/narthagi-lyrics"
song: "Vaan Mazhaiyin Thuligal"
image: ../../images/albumart/narthagi.jpg
date: 2011-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3jvGbkLhTYY"
type: "happy"
singers:
  - P. Unnikrishnan 
  - Sudha Raghunathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaan mazhaiyin thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mazhaiyin thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin thuliyaa pennin thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin thuliyaa pennin thuliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar kadalin alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar kadalin alaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin alaiyaa pennin alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin alaiyaa pennin alaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril aan pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril aan pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Baedhamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baedhamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyil veiyil kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil veiyil kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru nerkkodu inaiyum idathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru nerkkodu inaiyum idathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaam idhu vallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaam idhu vallavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanai nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanai nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivan uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivan uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Umaiyai nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umaiyai nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Umai uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umai uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann paarvai kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann paarvai kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalin dheiveega mozhiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalin dheiveega mozhiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vadivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vadivam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan mazhaiyin thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mazhaiyin thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin thuliyaa pennin thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin thuliyaa pennin thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar kadalin alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar kadalin alaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin alaiyaa pennin alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin alaiyaa pennin alaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalin aazhaththai yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin aazhaththai yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu sollalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalin aazhaththai yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalin aazhaththai yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana mudiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhandhai kaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhai kaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru udal oru udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru udal oru udal"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarndha kaalathil ver udalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarndha kaalathil ver udalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidhaiyil thee vaiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidhaiyil thee vaiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriththidum malaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriththidum malaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalin urumaatram thodar kadhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalin urumaatram thodar kadhaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aangal enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangal enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya thoatrangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya thoatrangalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiru nangai ival iru karai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru nangai ival iru karai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil odum nadhiyaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil odum nadhiyaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan mazhaiyin thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mazhaiyin thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin thuliyaa pennin thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin thuliyaa pennin thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar kadalin alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar kadalin alaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin alaiyaa pennin alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin alaiyaa pennin alaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril aan pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril aan pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Baedhamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baedhamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyil veiyil kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil veiyil kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru nerkkodu inaiyum idathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru nerkkodu inaiyum idathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaam idhu vallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaam idhu vallavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanai nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanai nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivan uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivan uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Umaiyai nee paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umaiyai nee paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Umai uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umai uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann paarvai kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann paarvai kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalin dheiveega mozhiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalin dheiveega mozhiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vadivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vadivam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan mazhaiyin thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mazhaiyin thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin thuliyaa pennin thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin thuliyaa pennin thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar kadalin alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar kadalin alaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanin alaiyaa pennin alaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanin alaiyaa pennin alaiyaa"/>
</div>
</pre>
