---
title: "nee enthan uyirin song lyrics"
album: "Eghantham"
artist: "Ganesh Raghavendra"
lyricist: "Yuga Bharathi"
director: "Arsal Arumugam"
path: "/albums/eghantham-lyrics"
song: "Nee Enthan Uyirin"
image: ../../images/albumart/eghantham.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I2IMP0xtnl8"
type: "love"
singers:
  - Saindhavi
  - Ganesh Raghavendra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan unthan udalin paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unthan udalin paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enthan uyirin meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan uyirin meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae ondraai sernthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarumae ondraai sernthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unthan udalin paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unthan udalin paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enthan uyirin meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan uyirin meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae ondraai sernthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarumae ondraai sernthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannukulla unna naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukulla unna naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini enna vittu poga vida maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna vittu poga vida maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unthan udalin paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unthan udalin paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enthan uyirin meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan uyirin meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae ondraai sernthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarumae ondraai sernthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serthu neeyum vecha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu neeyum vecha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju kuli nirambiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju kuli nirambiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna naanum sollaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna naanum sollaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan aasai kanda pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan aasai kanda pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan kaadhal chinnadaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan kaadhal chinnadaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri solla vaarthai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri solla vaarthai illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna muzhusaa neeyae aazhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna muzhusaa neeyae aazhura"/>
</div>
<div class="lyrico-lyrics-wrapper">En ennam muzhusaa chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ennam muzhusaa chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae vaazhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vaazhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanam indri naanum ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanam indri naanum ketpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal mutham thaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal mutham thaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enthan uyirin meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan uyirin meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unthan udalin paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unthan udalin paathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna pola paasam veikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pola paasam veikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor ulagil yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ulagil yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodi naanum solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodi naanum solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnarugil naan irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnarugil naan irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai ondrae pothum aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai ondrae pothum aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera onnum thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera onnum thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna surutti ullangaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna surutti ullangaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada mella thiranthu ora kannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada mella thiranthu ora kannaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala neram koodi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala neram koodi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam nooru seivenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam nooru seivenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enthan uyirin meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan uyirin meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unthan udalin paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unthan udalin paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae ondraai sernthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarumae ondraai sernthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannukulla unna naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukulla unna naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini enna vittu poga vida maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna vittu poga vida maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaaaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaaaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaaaa"/>
</div>
</pre>
