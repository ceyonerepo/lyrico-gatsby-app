---
title: "paaren paaren song lyrics"
album: "Dagaalty"
artist: "Vijay Narain"
lyricist: "Subu"
director: "Vijay Anand"
path: "/albums/dagaalty-song-lyrics"
song: "Paaren Paaren"
image: ../../images/albumart/dagaalty.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uXWW7CjALFc"
type: "Love"
singers:
  - Vijaynarain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paaaren Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaaren Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Parava Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Parava Poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Parakkudhu Koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Parakkudhu Koodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanaiya Rasikka Marakkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanaiya Rasikka Marakkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Payaname Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payaname Paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhumaingala Pogura Pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumaingala Pogura Pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakki Viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakki Viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaaren Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaaren Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Parava Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Parava Poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Parakkudhu Koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Parakkudhu Koodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanaiya Rasikka Marakkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanaiya Rasikka Marakkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Payaname Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payaname Paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhumaingala Pogura Pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumaingala Pogura Pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakki Viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakki Viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakka Thaan Aasai Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka Thaan Aasai Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanan Dhaan Siragu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanan Dhaan Siragu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka Thaan Sillarai Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Thaan Sillarai Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Pookkanaagayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Pookkanaagayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Olagam Surungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olagam Surungudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaaren Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaaren Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Parava Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Parava Poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Parakkudhu Koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Parakkudhu Koodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhai Kadhaiya Kettaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Kadhaiya Kettaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kooda Paathaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kooda Paathaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathu Inbam Sonna Puriyathadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathu Inbam Sonna Puriyathadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Nadaiya Sila Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Nadaiya Sila Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oordhiyila Pala Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oordhiyila Pala Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunjaala Pesum Baashai Pudhiraagudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunjaala Pesum Baashai Pudhiraagudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum Vara Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Vara Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum Vara Gnyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum Vara Gnyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyum Vara Neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyum Vara Neelum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodaiyila Mayakkam Thaan Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodaiyila Mayakkam Thaan Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhaiya Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhaiya Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrangala Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrangala Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi Tholaiyum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi Tholaiyum Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaaren Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaaren Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Parava Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Parava Poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Parakkudhu Koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Parakkudhu Koodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyellaam Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyellaam Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasanaiya Rasikka Marakkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanaiya Rasikka Marakkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Payaname Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payaname Paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhumaingala Pogura Pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumaingala Pogura Pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakki Viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakki Viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakka Thaan Aasai Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka Thaan Aasai Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanan Dhaan Siragu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanan Dhaan Siragu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka Thaan Sillarai Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka Thaan Sillarai Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Pookkanaagayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Pookkanaagayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Olagam Surungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olagam Surungudhu"/>
</div>
</pre>
