---
title: "varranda muni vanthutaan muni song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Varranda Muni Vanthutaan Muni"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kZSv4HKiNyI"
type: "Mass"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varraandaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraandaa muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuttaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuttaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni podu machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni podu machchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppathukku raajaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathukku raajaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma saathi sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma saathi sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathukkum sonthandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum sonthandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan nikka sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan nikka sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru moththam kekkundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru moththam kekkundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thappu senjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thappu senjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattiketkkum veerandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattiketkkum veerandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanukku uyir kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanukku uyir kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kaasu panam illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kaasu panam illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiveduppaan thadiyeduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiveduppaan thadiyeduppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adippaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikoduppaan kannasaippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikoduppaan kannasaippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathai mudippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathai mudippaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu thisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu thisaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarnthu nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarnthu nikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varraandaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraandaa muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuttaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuttaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni podu machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni podu machchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodikkanakkil panankoduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodikkanakkil panankoduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarammaattaan muni thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarammaattaan muni thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettaiyilae irukkum ezhaikkannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettaiyilae irukkum ezhaikkannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer vanthaa vedippaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer vanthaa vedippaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaakku thantha pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakku thantha pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil thappi chellamaattaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil thappi chellamaattaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natppu endru sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natppu endru sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha etti thalla maattaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha etti thalla maattaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni muni muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga saami thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga saami thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni muni muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varraandaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraandaa muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuttaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuttaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni podu machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni podu machchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni muni varaandaa…………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni varaandaa………….."/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni vaaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni vaaraandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni varaandaa…………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni varaandaa………….."/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni vaaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni vaaraandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muni muni varaandaa…………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni varaandaa………….."/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni vaaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni vaaraandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni varaandaa…………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni varaandaa………….."/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni vaaraandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni vaaraandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppathula gopuram paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathula gopuram paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppathula saamiyappaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathula saamiyappaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Varraan varraan………..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraan varraan……….."/>
</div>
<div class="lyrico-lyrics-wrapper">Varraan varraan………varraandoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraan varraan………varraandoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduththavan kaasil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduththavan kaasil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhathumila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhathumila"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal nenchil bayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaal nenchil bayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakkura saathi irukkura pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakkura saathi irukkura pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangura saathi muni illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangura saathi muni illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna sollu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna sollu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil kaththu nikkum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil kaththu nikkum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha onnu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha onnu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kaththi serum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kaththi serum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam saathithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam saathithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge paathithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge paathithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varraandaa muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varraandaa muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnuttaan muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnuttaan muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muni muni podu machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muni muni podu machchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppathukku raajaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppathukku raajaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma saathi sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma saathi sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathukkum sonthandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum sonthandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan nikka sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan nikka sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru moththam kekkundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru moththam kekkundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thappu senjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thappu senjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattiketkkum veerandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattiketkkum veerandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanukku uyir kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanukku uyir kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kaasu panam illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kaasu panam illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiveduppaan thadiyeduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiveduppaan thadiyeduppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adippaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikoduppaan kannasaippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikoduppaan kannasaippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathai mudippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathai mudippaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu thisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu thisaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarnthu nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarnthu nikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu thisaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu thisaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarnthu nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarnthu nikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa muni muni muni muni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa muni muni muni muni"/>
</div>
</pre>
