---
title: "uyir unnodu serumo song lyrics"
album: "Triples"
artist: "Jai"
lyricist: "Archana Sabeshh"
director: "Charukesh Sekar"
path: "/albums/triples-lyrics"
song: "Uyir Unnodu Serumo"
image: ../../images/albumart/triples.jpg
date: 2020-12-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XMsOiUTptNY"
type: "sad"
singers:
  - Bobo Shashi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyir unnodu serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnodu serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illamal pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illamal pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mounathin pesum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mounathin pesum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum kadhal pesuida varumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum kadhal pesuida varumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale unnale yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale unnale yen"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavu indru niram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavu indru niram maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaagum saayangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaagum saayangal "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi koodum kayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi koodum kayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennulle sirai vazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle sirai vazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaivelai illamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaivelai illamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ninaivu indru nijam theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivu indru nijam theduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaratha nerangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaratha nerangal "/>
</div>
<div class="lyrico-lyrics-wrapper">thee moottum thorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee moottum thorangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnukkul theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnukkul theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan theigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nizhal neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nizhal neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En nijam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nijam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuzhanthai yena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuzhanthai yena naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un en uyiril sumappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un en uyiril sumappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhum varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhum varaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir unnodu serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnodu serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illamal pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illamal pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kobangal pesum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kobangal pesum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum kadhal pesida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum kadhal pesida"/>
</div>
</pre>
