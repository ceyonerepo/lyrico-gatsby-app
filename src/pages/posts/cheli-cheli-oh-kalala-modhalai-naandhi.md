---
title: "cheli cheli song lyrics"
album: "Naandhi"
artist: "Sricharan Pakala"
lyricist: "Sri Mani"
director: "Vijay Kanakamedala"
path: "/albums/naandhi-lyrics"
song: "Cheli Cheli - Oh Kalala Modhalai"
image: ../../images/albumart/naandhi.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wm3zn5Gn8aY"
type: "love"
singers:
  - NC Karunya
  - Haripriya Maranganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kalala modhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalala modhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kathal kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kathal kadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohinchanidhe naa oopirikedhurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchanidhe naa oopirikedhurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Santoshamila varadhai munchinadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santoshamila varadhai munchinadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kalalaa modhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalalaa modhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kathala kadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kathala kadhiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaboye vaade premisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaboye vaade premisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham aagenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham aagenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa manuvaade vaade manasisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa manuvaade vaade manasisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa bandham thonikena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa bandham thonikena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeve panchu ee premaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve panchu ee premaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam chaalunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam chaalunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhae yellu ee gundeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhae yellu ee gundeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vele koranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vele koranaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Nuvvunte naavente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nuvvunte naavente"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalantunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalantunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye janmaina nee jantai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye janmaina nee jantai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduge vese bhagyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduge vese bhagyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake kavalantunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake kavalantunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kalala modhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalala modhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kathal kadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kathal kadhiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli cheli"/>
</div>
</pre>
