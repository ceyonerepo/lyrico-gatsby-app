---
title: 'pookalin porvai song lyrics'
album: 'Pon Magal Vandhal'
artist: 'Govind Vasantha'
lyricist: 'Vivek'
director: 'J J Fredrick'
path: '/albums/pon-magal-vandhal-song-lyrics'
song: 'Pookalin Porvai'
image: ../../images/albumart/ponmagal-vandhal.jpg
date: 2020-05-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1RUr70zv7wM"
type: 'happy'
singers: 
- Sean Roldan
- Keerthana Vaidyanathan
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Appan kudutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Appan kudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi pandhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi pandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee odhachu aattam poda
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee odhachu aattam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnapola uyirgal undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnapola uyirgal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vidu athaiyum kooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazha vidu athaiyum kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pookkaludan serndhu vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pookkaludan serndhu vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru varum kavidhai uraiyaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatru varum kavidhai uraiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam varum endrae nadaipodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam varum endrae nadaipodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Both  Bhoomi tharum mazhaiyil aadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Bhoomi tharum mazhaiyil aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maanidar kaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maanidar kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanidar kaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maanidar kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamaai oodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vegamaai oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum paarkaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhaiyum paarkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Oorae oodum
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Oorae oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvi ketkaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kelvi ketkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhaiyaagi urundodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mandhaiyaagi urundodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pookalin porva
<input type="checkbox" class="lyrico-select-lyric-line" value="Pookalin porva"/>
</div>
<div class="lyrico-lyrics-wrapper">Porva… porva porva porva
<input type="checkbox" class="lyrico-select-lyric-line" value="Porva… porva porva porva"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyil aruvi verva
<input type="checkbox" class="lyrico-select-lyric-line" value="Malaiyil aruvi verva"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgala somakkum….
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirgala somakkum…."/>
</div>
<div class="lyrico-lyrics-wrapper">Mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line" value="Mm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyum penno…
<input type="checkbox" class="lyrico-select-lyric-line" value="Malaiyum penno…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Pennmaiyo bayamae ariyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Pennmaiyo bayamae ariyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thaazhndhu valaivadhu kedaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai thaazhndhu valaivadhu kedaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana podhum uravukkaaga dhenamumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana podhum uravukkaaga dhenamumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi yengi vaazhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Yengi yengi vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimanam suyamae ariyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaimanam suyamae ariyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku alavae kedaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbukku alavae kedaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pidichi urugumbothum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thee pidichi urugumbothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaiyae dheebama neetum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannaiyae dheebama neetum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee vazhavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vazhavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  …………………………….
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thanna vidavum unna nenaikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanna vidavum unna nenaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil oru jeevan pookkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannil oru jeevan pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi irundhaa naalai olagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambi irundhaa naalai olagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna vechu kaaval kaakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna vechu kaaval kaakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Vaazhkka varum mazhalai paechaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Vaazhkka varum mazhalai paechaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettirukka neram onaikkilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettirukka neram onaikkilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi uyir kanneer kannoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadhi uyir kanneer kannoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkumae kavalai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarukkumae kavalai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maarumae vaazhkka maarumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Maarumae vaazhkka maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum vaasana veesa varumae orunaalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum vaasana veesa varumae orunaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa vedhaiyum mela pogum inga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaa vedhaiyum mela pogum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera paadha kedaiyaadhu…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera paadha kedaiyaadhu….."/>
</div>
</pre>