---
title: "youtube la melam song lyrics"
album: "Mohini"
artist: "	Vivek-Mervin"
lyricist: "Vivek"
director: "Ramana Madhesh"
path: "/albums/mohini-lyrics"
song: "Youtube La Melam"
image: ../../images/albumart/mohini.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ddevXhCRT24"
type: "happy"
singers:
  - Sanjana Kalmanje
  - Vivek Siva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neram poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram poranthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oorae therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oorae therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaasal therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaasal therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nichayam than aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nichayam than aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram poranthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oorae therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oorae therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaasal therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaasal therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nichayam than aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nichayam than aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaeeeaaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeeeaaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Youtube-la melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youtube-la melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo facetime-la thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo facetime-la thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha neram romba thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha neram romba thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa maaripochu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa maaripochu kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontham bantham nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham bantham nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vaalthu solla venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vaalthu solla venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu sontham illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu sontham illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Smiley vanthu whatsapp-kul koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smiley vanthu whatsapp-kul koovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha dowry money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha dowry money"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo matrimony
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo matrimony"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam slip aachu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam slip aachu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Head-u mela thuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Head-u mela thuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga technology
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga technology"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodu kilichaachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodu kilichaachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha thaandi sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha thaandi sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu heart-um ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu heart-um ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeeeaaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeeeaaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Youtube-la melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youtube-la melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo facetime-la thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo facetime-la thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha neram romba thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha neram romba thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa maaripochu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa maaripochu kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontham bantham nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham bantham nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vaalthu solla venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vaalthu solla venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu sontham illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu sontham illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Smiley vanthu whatsapp-kul koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smiley vanthu whatsapp-kul koovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontham varum pozhivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham varum pozhivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathai pandhiyaa vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathai pandhiyaa vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Contract-nu kouthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Contract-nu kouthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu pochu paayasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pochu paayasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hungry cow-vu pasiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hungry cow-vu pasiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya waiting-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya waiting-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Plastic ilai kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plastic ilai kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Straight-ah kailasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straight-ah kailasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mappila kedacha athuenna perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappila kedacha athuenna perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam santhosam 50-50 than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam santhosam 50-50 than"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandabam kedacha aattatha podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandabam kedacha aattatha podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck-u illana online-la thaali kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck-u illana online-la thaali kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Youtube-la melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youtube-la melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo facetime-la thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo facetime-la thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha neram romba thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha neram romba thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa maaripochu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa maaripochu kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram poranthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oorae therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oorae therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaasal therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaasal therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nichayam than aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nichayam than aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam achu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini enna naan solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna naan solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan achu nee achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan achu nee achu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Youtube-la melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youtube-la melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo facetime-la thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo facetime-la thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha neram romba thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha neram romba thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa maaripochu kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa maaripochu kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram poranthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oorae therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oorae therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaasal therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaasal therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nichayam than aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nichayam than aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram poranthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oorae therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oorae therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaasal therandhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaasal therandhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nichayam than aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nichayam than aachu"/>
</div>
</pre>
