---
title: "vetri bodha song lyrics"
album: "Thanne Vandi"
artist: "Moses"
lyricist: "Manika Vidya - Kavignar Ve. Madhan Kumar"
director: "Manika Vidya"
path: "/albums/thanne-vandi-lyrics"
song: "Vetri Bodha"
image: ../../images/albumart/thanne-vandi.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5Sl0IdwFsOc"
type: "happy"
singers:
  - Monisha Soundararajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dan daangu dumaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dan daangu dumaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri botha thalaiku aera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri botha thalaiku aera"/>
</div>
<div class="lyrico-lyrics-wrapper">dun dangu dumakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dun dangu dumakka"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu noriki pataya kilapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu noriki pataya kilapu"/>
</div>
<div class="lyrico-lyrics-wrapper">kikkana thanne vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kikkana thanne vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathukaga thanee vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathukaga thanee vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulachi kalachu nilala pudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulachi kalachu nilala pudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthu pokka rasikum thaniyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthu pokka rasikum thaniyada"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla vali paathaiyakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla vali paathaiyakki"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu inna bothada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu inna bothada"/>
</div>
<div class="lyrico-lyrics-wrapper">anubava ulagathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubava ulagathula "/>
</div>
<div class="lyrico-lyrics-wrapper">aasa periya botha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa periya botha da"/>
</div>
<div class="lyrico-lyrics-wrapper">pennulayum botha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennulayum botha da"/>
</div>
<div class="lyrico-lyrics-wrapper">aanulayum botha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanulayum botha da"/>
</div>
<div class="lyrico-lyrics-wrapper">mannulayum botha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannulayum botha da"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnulayaum botha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnulayaum botha da"/>
</div>
<div class="lyrico-lyrics-wrapper">boomikulla mothamum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikulla mothamum than"/>
</div>
<div class="lyrico-lyrics-wrapper">bothayila suthuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothayila suthuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei dingiri dangari dangiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei dingiri dangari dangiri"/>
</div>
<div class="lyrico-lyrics-wrapper">dingiri dingiri dangiri da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dingiri dingiri dangiri da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei vandi vandi vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vandi vandi vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">naama thanni vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama thanni vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei dunguru danguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei dunguru danguru"/>
</div>
<div class="lyrico-lyrics-wrapper">danguru dunguru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danguru dunguru"/>
</div>
<div class="lyrico-lyrics-wrapper">dunguru danguruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dunguru danguruda"/>
</div>
<div class="lyrico-lyrics-wrapper">yei vandi vandi vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vandi vandi vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">naama thanni vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama thanni vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha naatu jallikaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha naatu jallikaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">mappu ethum botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mappu ethum botha"/>
</div>
<div class="lyrico-lyrics-wrapper">malli poovu vaasan thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli poovu vaasan thane"/>
</div>
<div class="lyrico-lyrics-wrapper">kikku aethum botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kikku aethum botha"/>
</div>
<div class="lyrico-lyrics-wrapper">eh pathavikana arasiyalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh pathavikana arasiyalil"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaku mela botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaku mela botha"/>
</div>
<div class="lyrico-lyrics-wrapper">ootu podum janangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ootu podum janangalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu mela botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu mela botha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aeru pootum ulavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeru pootum ulavanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vilachalu than botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilachalu than botha"/>
</div>
<div class="lyrico-lyrics-wrapper">padaiparivu ullavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaiparivu ullavanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">puthagam than botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthagam than botha"/>
</div>
<div class="lyrico-lyrics-wrapper">kalainganuku rasiganoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalainganuku rasiganoda"/>
</div>
<div class="lyrico-lyrics-wrapper">kai thatale botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai thatale botha"/>
</div>
<div class="lyrico-lyrics-wrapper">samsaariya aagiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samsaariya aagiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbam mela botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbam mela botha"/>
</div>
<div class="lyrico-lyrics-wrapper">star hotel vaalkaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="star hotel vaalkaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya sore botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya sore botha"/>
</div>
<div class="lyrico-lyrics-wrapper">pattalathu veeranukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattalathu veeranukku "/>
</div>
<div class="lyrico-lyrics-wrapper">naatru mela botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatru mela botha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharuthalanga puthiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharuthalanga puthiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi pere botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi pere botha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharumam seiya manasiruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumam seiya manasiruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">puniyam than botha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puniyam than botha"/>
</div>
<div class="lyrico-lyrics-wrapper">boomikkula mothamum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikkula mothamum than"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaiyila suthuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaiyila suthuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaiku than samamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaiku than samamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">thani vandi irukumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani vandi irukumada"/>
</div>
<div class="lyrico-lyrics-wrapper">yei dingiri dangari dangiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei dingiri dangari dangiri"/>
</div>
<div class="lyrico-lyrics-wrapper">dingiri dingiri dangiri da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dingiri dingiri dangiri da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei vandi vandi vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vandi vandi vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">naama thanni vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama thanni vandi da"/>
</div>
</pre>
