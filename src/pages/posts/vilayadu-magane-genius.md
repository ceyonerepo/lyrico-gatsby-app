---
title: "vilayadu magane song lyrics"
album: "Genius"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Suseenthiran"
path: "/albums/genius-lyrics"
song: "Vilayadu Magane"
image: ../../images/albumart/genius.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ofYeLZ9AYEs"
type: "happy"
singers:
  - Senthildass Velayutham
  - Surmukhi Raman
  - Sam
  - Pawan
  - Priya Himesh
  - Sri Varthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yehhhheeehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehhhheeehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahhaaa yehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahhaaa yehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadum vayathil vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadum vayathil vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu aadi vinnodu yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu aadi vinnodu yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae vilaiyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalennum koottil uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalennum koottil uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">odum varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir kaala vazhvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir kaala vazhvai "/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kolla neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kolla neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae vilaiyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadum vayathil vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadum vayathil vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu aadi vinnodu yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu aadi vinnodu yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae vilaiyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadum vayathil vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadum vayathil vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu aadi vinnodu yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu aadi vinnodu yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu maganae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu maganae vilaiyadu"/>
</div>
</pre>
