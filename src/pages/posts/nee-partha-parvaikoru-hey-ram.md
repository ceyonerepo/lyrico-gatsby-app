---
title: "nee partha song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Kamal Haasan"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Nee Partha Parvaikoru"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JVTMWYcHyfA"
type: "melody"
singers:
  - Asha Bhosle
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Partha Parvaikkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Partha Parvaikkoru Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai Sertha Iravukkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai Sertha Iravukkoru Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayaraatha Ilamai Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaraatha Ilamai Sollum Nandri Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Akalaatha Ninaivu Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akalaatha Ninaivu Sollum Nandri Nandri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Endra Sol Ini Vendamm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Endra Sol Ini Vendamm"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enbathe Ini Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enbathe Ini Naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimelum Varam Ketka Thevaillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum Varam Ketka Thevaillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithupol Verengum Sorgamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithupol Verengum Sorgamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadagam Mudintha Pinnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagam Mudintha Pinnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadippinnum Thodarvathu Enaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadippinnum Thodarvathu Enaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oranga Vedam Ini Podhum Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oranga Vedam Ini Podhum Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Pogum Mattum Unn Ninaive Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogum Mattum Unn Ninaive Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Partha Parvaikkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Partha Parvaikkoru Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Partha Parvaikkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Partha Parvaikkoru Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai Sertha Iravukkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai Sertha Iravukkoru Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai Sertha Iravukkoru Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai Sertha Iravukkoru Nandri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayaraatha Ilamai Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaraatha Ilamai Sollum Nandri Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayaraatha Ilamai Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaraatha Ilamai Sollum Nandri Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Akalaatha Ninaivu Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akalaatha Ninaivu Sollum Nandri Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Akalaatha Ninaivu Sollum Nandri Nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akalaatha Ninaivu Sollum Nandri Nandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Vaa"/>
</div>
</pre>
