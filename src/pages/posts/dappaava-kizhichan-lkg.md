---
title: "dappaava kizhichaan song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Pa. Vijay"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Dappaava Kizhichaan"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wxTORPxMM6U"
type: "happy"
singers:
  - Shruti Hassan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annatha Mass Illa Boss Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatha Mass Illa Boss Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeychi Putta Lays Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeychi Putta Lays Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… O… O… O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… O… O… O…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Showkkana Face Illa Piece Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showkkana Face Illa Piece Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Verai Choice Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Verai Choice Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… O… O… O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… O… O… O…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moonja Enga Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moonja Enga Veppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Body God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Body God"/>
</div>
<div class="lyrico-lyrics-wrapper">Munikku Ponga Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munikku Ponga Veppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh… Oh… Oh… Oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh… Oh… Oh… Oh…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odachaaney Mookka Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odachaaney Mookka Ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eppadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththula Naakka Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththula Naakka Veppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dappaava Kizhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dappaava Kizhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka Uttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka Uttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Podu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Podu Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dappaava Kizhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dappaava Kizhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka Uttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka Uttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappa Un Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappa Un Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paprappaa Paaruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paprappaa Paaruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annatha Face-la Fuse Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatha Face-la Fuse Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Paththi News Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Paththi News Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… O… O… O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… O… O… O…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennatha Sollurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennatha Sollurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Ippo Vellurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ippo Vellurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Race Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Race Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… O… O… O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… O… O… O…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Manda Kaanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manda Kaanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Black-Ishah Theenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black-Ishah Theenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorama Saanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorama Saanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai…O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai…O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethithen Range-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethithen Range-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduthuthen Change-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduthuthen Change-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduren Paar Unja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduren Paar Unja"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey... Hey... Hey... Hey…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey... Hey... Hey... Hey…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey En Ganguly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey En Ganguly"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatathil Neeyum Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatathil Neeyum Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Old Thakkaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Old Thakkaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram Po Dummangoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram Po Dummangoli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa En Pangali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa En Pangali"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthuko Verai Joli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthuko Verai Joli"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali Jaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali Jaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamai Da Kohli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamai Da Kohli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dappaava Kizhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dappaava Kizhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka Uttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka Uttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Podu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Podu Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Jippava Pirichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jippava Pirichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirichu Thechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichu Thechchan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappa Un Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappa Un Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paprappaa Paaruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paprappaa Paaruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annatha Mass Illa Boss Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatha Mass Illa Boss Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeychi Putta Lays Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeychi Putta Lays Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O… O… O… O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O… O… O… O…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Showkkana Face Illa Piece Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showkkana Face Illa Piece Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Verai Choice Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Verai Choice Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">O O O O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O O O O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moonja Enga Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moonja Enga Veppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Body God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Body God"/>
</div>
<div class="lyrico-lyrics-wrapper">Munikku Ponga Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munikku Ponga Veppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh…Oh…Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh…Oh…Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odachaaney Mookka Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odachaaney Mookka Ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eppadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththula Naakka Veppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththula Naakka Veppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dappaava Kizhichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dappaava Kizhichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka Uttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka Uttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance Podu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Podu Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Jippava Pirichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jippava Pirichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyala Goyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goyyala Goyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirichu Thechchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichu Thechchan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappa Un Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappa Un Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paprappaa Paaruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paprappaa Paaruppa"/>
</div>
</pre>
