---
title: "kashtam vasthe nake song lyrics"
album: "Dear Megha"
artist: "Gowra Hari"
lyricist: "Krishna Kanth"
director: "A. Sushanth Reddy"
path: "/albums/dear-megha-lyrics"
song: "Kashtam Vasthe Nake"
image: ../../images/albumart/dear-megha.jpg
date: 2021-09-03
lang: telugu
youtubeLink: 
type: "love"
singers:
  - Gowra Hari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kastam vasthe nake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastam vasthe nake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannullo neerey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannullo neerey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela nanne veedellave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela nanne veedellave"/>
</div>
<div class="lyrico-lyrics-wrapper">Edusthunte nene chusthuntava neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edusthunte nene chusthuntava neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve leka ela undale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve leka ela undale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashale aavirai okkasare kuppakule kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashale aavirai okkasare kuppakule kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayame maanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayame maanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone gundekinko gayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone gundekinko gayama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhoshale maayamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshale maayamaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Shunyamthone daari maarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shunyamthone daari maarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhalanni dhuramaayenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhalanni dhuramaayenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thufaanemo aagaledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thufaanemo aagaledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkovaipe ningi kule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkovaipe ningi kule"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkemundhe jeevithamlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkemundhe jeevithamlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashale aavirai okkasare kuppakule kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashale aavirai okkasare kuppakule kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayame maanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayame maanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone gundekinko gayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone gundekinko gayama"/>
</div>
</pre>
