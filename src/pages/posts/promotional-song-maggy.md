---
title: "promotional song song lyrics"
album: "Maggy"
artist: "UM Steven Sathish"
lyricist: "Hayas John"
director: "Kartikeyen"
path: "/albums/maggy-lyrics"
song: "promotional song"
image: ../../images/albumart/maggy.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xSte68HoGow"
type: "promotional song"
singers:
  - Hayas John
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mamboo camboo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamboo camboo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru treata combo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru treata combo"/>
</div>
<div class="lyrico-lyrics-wrapper">high tempo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="high tempo "/>
</div>
<div class="lyrico-lyrics-wrapper">so sharpoo orampo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so sharpoo orampo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mamboo camboo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamboo camboo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru treata combo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru treata combo"/>
</div>
<div class="lyrico-lyrics-wrapper">heart pumpo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart pumpo"/>
</div>
<div class="lyrico-lyrics-wrapper">blood highpo aalampo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="blood highpo aalampo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hala la party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hala la party"/>
</div>
<div class="lyrico-lyrics-wrapper">munnala beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku mela enada duty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku mela enada duty"/>
</div>
<div class="lyrico-lyrics-wrapper">valkaila treaty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valkaila treaty"/>
</div>
<div class="lyrico-lyrics-wrapper">un getha katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un getha katti"/>
</div>
<div class="lyrico-lyrics-wrapper">bejaruka bejara katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bejaruka bejara katti"/>
</div>
<div class="lyrico-lyrics-wrapper">phoneix mathiree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phoneix mathiree"/>
</div>
<div class="lyrico-lyrics-wrapper">meendu nee enthiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendu nee enthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">un life u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un life u "/>
</div>
<div class="lyrico-lyrics-wrapper">enaikum thookathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaikum thookathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">girl friend mathri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="girl friend mathri"/>
</div>
<div class="lyrico-lyrics-wrapper">una nee kadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una nee kadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">un jolly epavum korayathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jolly epavum korayathuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chock lock straight walku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chock lock straight walku"/>
</div>
<div class="lyrico-lyrics-wrapper">slopu rock podamatan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="slopu rock podamatan "/>
</div>
<div class="lyrico-lyrics-wrapper">adichalum pudichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichalum pudichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">high bp vachalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="high bp vachalum"/>
</div>
<div class="lyrico-lyrics-wrapper">right beatu un heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right beatu un heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee follow nee follow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee follow nee follow"/>
</div>
<div class="lyrico-lyrics-wrapper">liquour flicker 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="liquour flicker "/>
</div>
<div class="lyrico-lyrics-wrapper">netthiela sticker
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netthiela sticker"/>
</div>
<div class="lyrico-lyrics-wrapper">kilichuru nee paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilichuru nee paru"/>
</div>
<div class="lyrico-lyrics-wrapper">takkaru tharjolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkaru tharjolu"/>
</div>
<div class="lyrico-lyrics-wrapper">background sutalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="background sutalum"/>
</div>
<div class="lyrico-lyrics-wrapper">high reacha thotalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="high reacha thotalum"/>
</div>
<div class="lyrico-lyrics-wrapper">follow maggy maragathavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="follow maggy maragathavalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kigulu bigulu padidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kigulu bigulu padidu"/>
</div>
<div class="lyrico-lyrics-wrapper">aah kigulu bigulu padidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aah kigulu bigulu padidu"/>
</div>
<div class="lyrico-lyrics-wrapper">kigulu bigulu padidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kigulu bigulu padidu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey padidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey padidu "/>
</div>
<div class="lyrico-lyrics-wrapper">hey padidu padidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey padidu padidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kigulu bigulu lead edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kigulu bigulu lead edu"/>
</div>
<div class="lyrico-lyrics-wrapper">aah kigulu bigulu lead edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aah kigulu bigulu lead edu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey kigulu bigulu lead edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey kigulu bigulu lead edu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey lead eduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey lead eduu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ice ah macho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice ah macho "/>
</div>
<div class="lyrico-lyrics-wrapper">size ah catchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="size ah catchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">freeze ah beachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="freeze ah beachoo"/>
</div>
<div class="lyrico-lyrics-wrapper">statue statue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="statue statue"/>
</div>
<div class="lyrico-lyrics-wrapper">sweet keecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sweet keecho"/>
</div>
<div class="lyrico-lyrics-wrapper">hot pecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hot pecho"/>
</div>
<div class="lyrico-lyrics-wrapper">neat ah sketcho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neat ah sketcho"/>
</div>
<div class="lyrico-lyrics-wrapper">cho cho cho cho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cho cho cho cho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ice ah macho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice ah macho "/>
</div>
<div class="lyrico-lyrics-wrapper">size ah catchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="size ah catchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">freeze ah beachoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="freeze ah beachoo"/>
</div>
<div class="lyrico-lyrics-wrapper">statue statue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="statue statue"/>
</div>
<div class="lyrico-lyrics-wrapper">sweet keecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sweet keecho"/>
</div>
<div class="lyrico-lyrics-wrapper">hot pecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hot pecho"/>
</div>
<div class="lyrico-lyrics-wrapper">neat ah straight ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neat ah straight ah"/>
</div>
<div class="lyrico-lyrics-wrapper">solliyacho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solliyacho "/>
</div>
</pre>
