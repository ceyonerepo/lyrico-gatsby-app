---
title: "kadaram kondan song lyrics"
album: "Kadaram Kondan"
artist: "Ghibran"
lyricist: "Priyan - Shabir"
director: "Rajesh M. Selva"
path: "/albums/kadaram-kondan-lyrics"
song: "Kadaram Kondan"
image: ../../images/albumart/kadaram-kondan.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sRR3UcJLOvY"
type: "mass"
singers:
  - Shruti Haasan
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhoorathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakumbothae Verkkutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakumbothae Verkkutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thondakuzhi Varandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thondakuzhi Varandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni Kekkutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni Kekkutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula Vandhu Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Padharutha Kaal Udharutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharutha Kaal Udharutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theva Illai 8 Rounds Bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illai 8 Rounds Bro"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Take You Down In
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Take You Down In"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Let’s Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Let’s Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookkipottu Midhippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkipottu Midhippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naaru Naara Kizhippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naaru Naara Kizhippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhipaaru Sirippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhipaaru Sirippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nodiyil Kadhaya Mudippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodiyil Kadhaya Mudippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookkuvaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkuvaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu Chaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Chaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Thaakkuna Athirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Thaakkuna Athirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Singam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Singam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Perae Mirala Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perae Mirala Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Paaru Kettu Paaru Kettu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Paaru Kettu Paaru Kettu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theva Illai 8 Rounds Bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illai 8 Rounds Bro"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Take You Down In
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Take You Down In"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Let’s Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Let’s Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkuna Scene-u Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuna Scene-u Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama Odu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama Odu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkuna Scene-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuna Scene-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama Odu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama Odu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethurila Vandhu Nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethurila Vandhu Nippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeman Bayanthu Nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeman Bayanthu Nippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Puriyudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Puriyudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungu Odhungudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungu Odhungudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyam Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyam Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaram Kondaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaram Kondaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theva Illai 8 Rounds Bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illai 8 Rounds Bro"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Take You Down In
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Take You Down In"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Let’s Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Let’s Go"/>
</div>
</pre>
