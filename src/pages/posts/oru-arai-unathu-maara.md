---
title: "oru arai unathu lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Oru Arai Unathu"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jeIOk2z-FtI"
type: "love"
singers:
  - Yazin Nizar
  - Sanah Moidutty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Oru arai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru arai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kathavu thiranthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kathavu thiranthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru alai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru alai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kadalum karainthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kadalum karainthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru munai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru munai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru munai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru munai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarin dhuruvam inainthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruvarin dhuruvam inainthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru mugil unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugil unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mugil enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugil enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil nilavu kadanthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil nilavu kadanthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru kadhai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kadhai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kadhai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kadhai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukathai mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidukathai mudiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru arai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru arai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kathavu thiranthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kathavu thiranthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru alai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru alai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kadalum karainthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kadalum karainthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ae…..aehae……ae…..aehae……
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae…..aehae……ae…..aehae……"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae…..aehae……ae…..aehae……
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae…..aehae……ae…..aehae……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vannam nooru vaasal nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vannam nooru vaasal nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Both  Kan munnae kaangindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Kan munnae kaangindren"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaanampaadi polae maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanampaadi polae maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Both  Engaeyum pogindraen
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Engaeyum pogindraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanathukkum megathukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanathukkum megathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oode ulla veedondril
<input type="checkbox" class="lyrico-select-lyric-line" value="Oode ulla veedondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum vanthu aadi pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarum vanthu aadi pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal vaitha en mundril
<input type="checkbox" class="lyrico-select-lyric-line" value="Oonjal vaitha en mundril"/>
</div>
<div class="lyrico-lyrics-wrapper">Both  Pogum pokkil porvai portham poonthendral
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Pogum pokkil porvai portham poonthendral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru pagal unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru pagal unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pagal enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru pagal enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil iravu urangidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil iravu urangidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru imai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru imai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru imai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru imai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kanavu nigazhnthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kanavu nigazhnthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru malar unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru malar unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru malar enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru malar enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandilum idhazhgal oru niramaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Irandilum idhazhgal oru niramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru mugam unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugam unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mugam enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugam enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum nilavin irupuramaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruvarum nilavin irupuramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru badhil unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru badhil unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru badhil enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru badhil enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthirgalum udayumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthirgalum udayumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru arai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru arai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru arai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kathavu thiranthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kathavu thiranthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru alai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru alai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru alai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil kadalum karainthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil kadalum karainthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru munai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru munai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru munai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru munai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarin dhuruvam inainthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruvarin dhuruvam inainthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru mugil unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugil unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mugil enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mugil enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil nilavu kadanthidumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyinil nilavu kadanthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru kadhai unathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kadhai unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kadhai enathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kadhai enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukathai mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidukathai mudiyumaa"/>
</div>
</pre>
