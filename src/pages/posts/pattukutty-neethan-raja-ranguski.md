---
title: "pattukutty neethan song lyrics"
album: "Raja Ranguski"
artist: "Yuvan Shankar Raja"
lyricist: "Kabilan"
director: "Dharani Dharan"
path: "/albums/raja-ranguski-lyrics"
song: "Pattukutty Neethan"
image: ../../images/albumart/raja-ranguski.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QVCLZR02wfk"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pattukutty needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukutty needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En pattu kutty nee dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pattu kutty nee dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvadhu vayathanavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvadhu vayathanavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathai thottu ponavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai thottu ponavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival mugam oru vaanavilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival mugam oru vaanavilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itharkku mel solla thonavillae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itharkku mel solla thonavillae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinasari unnai parthithadavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinasari unnai parthithadavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam kodu en devathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam kodu en devathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena oru oorumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena oru oorumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittaal oru perumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittaal oru perumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukutty needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukutty needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyal enna thaakuradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyal enna thaakuradi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pattukutty needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pattukutty needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungiyae nenjil paayuradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungiyae nenjil paayuradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalal ennai nee azhaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalal ennai nee azhaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbiyae vanthu poo kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbiyae vanthu poo kuduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbiyae konjam nee sirithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbiyae konjam nee sirithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruvizha thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvizha thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduvathum siripathum adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvathum siripathum adikadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaruma thodaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaruma thodaruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalaal izhukiraai ivanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalaal izhukiraai ivanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadiyaai paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyaai paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therivathum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therivathum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodiyae paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodiyae paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavilum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivilae un mugam paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivilae un mugam paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondilai podum kaadhalan naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondilai podum kaadhalan naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirilae kadavul vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirilae kadavul vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum thaan naan kaanbenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum thaan naan kaanbenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukutty needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukutty needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nadayilae uyir thudikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nadayilae uyir thudikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pattukutty needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pattukutty needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir pizhaikavae oru vazhi solladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir pizhaikavae oru vazhi solladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai vida ver yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vida ver yaarumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu poga thonavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu poga thonavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin sumai baramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin sumai baramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathil sollu naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathil sollu naalai"/>
</div>
</pre>
