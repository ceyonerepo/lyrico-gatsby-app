---
title: "thelavaaruthunte song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Kittu Vissapragada"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "Thelavaaruthunte Kiranalu Ninne"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2_aRGr66SGE"
type: "happy"
singers:
  - Suswaram Anirudh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thelavaaruthunte kiranalu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelavaaruthunte kiranalu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Painunchi neepai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Painunchi neepai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooki sallabaddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooki sallabaddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegaalu penche dhaarullo pothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegaalu penche dhaarullo pothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neepai oollo unna kalla paddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neepai oollo unna kalla paddave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doosukupothe baanam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukupothe baanam la"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde sairen la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde sairen la"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogindhe thellare alarm la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogindhe thellare alarm la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doosukupothe baanam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukupothe baanam la"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde sairen la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde sairen la"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogindhe thellare alarm la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogindhe thellare alarm la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ningi nunchi kaalu jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi nunchi kaalu jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamama neela maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamama neela maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela paina bike-u ekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela paina bike-u ekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugutunnadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugutunnadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook lona like kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook lona like kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Twitter lona follow kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitter lona follow kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Instagram lo filter petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instagram lo filter petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Tingu kottanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tingu kottanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu kanulaku kanapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kanulaku kanapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku mudipadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku mudipadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuruga nilapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuruga nilapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatha padagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatha padagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamudaina kannu kottipoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamudaina kannu kottipoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choosinaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choosinaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna oopirantha podha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna oopirantha podha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanu padha padha padhamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu padha padha padhamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharimina manasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharimina manasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu thadabadi kadhalavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu thadabadi kadhalavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavulaina kotha polikedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavulaina kotha polikedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti rayaleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti rayaleka"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi patti poraa poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi patti poraa poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merupe neelaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupe neelaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu merisinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu merisinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thega merisinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega merisinadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakashamlo nakshthralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamlo nakshthralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Queue le kattaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Queue le kattaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthunna ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthunna ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalipoyaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalipoyaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli malli navve ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli malli navve ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle kutti mallepoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle kutti mallepoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palle koriki komme dhooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle koriki komme dhooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindhe padi vaadipoyaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindhe padi vaadipoyaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doosukupothe baanam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukupothe baanam la"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde sairen la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde sairen la"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogindhe thellare alarm la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogindhe thellare alarm la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelavaaruthunte kiranalu ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelavaaruthunte kiranalu ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Pai nunchi neepai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pai nunchi neepai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooki sallabaddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooki sallabaddave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doosukupothe baanam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doosukupothe baanam la"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde sairen la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde sairen la"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogindhe thellare alarm la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogindhe thellare alarm la"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo dhaare thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo dhaare thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Addanga book aipoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addanga book aipoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaindho emo antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindho emo antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaarukunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaarukunaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamudaina kannu kottipoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamudaina kannu kottipoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choosinaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choosinaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna oopirantha podha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna oopirantha podha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavulaina kotha polikedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavulaina kotha polikedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti rayaleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti rayaleka"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi patti poraa poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi patti poraa poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakashamlo nakshthralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamlo nakshthralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Queue le kattaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Queue le kattaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthunna ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthunna ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalipoyaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalipoyaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli malli navve ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli malli navve ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle kutti mallepoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle kutti mallepoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palle koriki komme dhooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle koriki komme dhooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha padi vaadipoyaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha padi vaadipoyaaye"/>
</div>
</pre>
