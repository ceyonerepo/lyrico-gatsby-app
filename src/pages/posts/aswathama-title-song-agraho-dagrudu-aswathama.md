---
title: "aswathama title song song lyrics"
album: "Aswathama"
artist: "Sricharan Pakala"
lyricist: "Ramajogayya Sastry"
director: "Ramana Teja"
path: "/albums/aswathama-lyrics"
song: "Aswathama title song - Agraho Dagrudu"
image: ../../images/albumart/aswathama.jpg
date: 2020-01-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Xa7KAZoHwBk"
type: "title song"
singers:
  - Divya Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agraho Dagrudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agraho Dagrudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agni Varna Nethrudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agni Varna Nethrudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Roudra Marthandudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roudra Marthandudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Prachandudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Prachandudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rudrudu Pralaya Veera Bhadrudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rudrudu Pralaya Veera Bhadrudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Durmadanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durmadanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuaithyajanaluanu Upeshinchadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuaithyajanaluanu Upeshinchadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuhyamaina Yukthikithadu Kendtastanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuhyamaina Yukthikithadu Kendtastanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajeyamaina Vidhyu Shakthi Veedi Pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajeyamaina Vidhyu Shakthi Veedi Pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Achanchalam Manobalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achanchalam Manobalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha Dhanurbanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha Dhanurbanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sankalapame Prakampanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankalapame Prakampanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prabhanjanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prabhanjanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada Manane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Manane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasamarakshanaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasamarakshanaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathya Sangtamame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathya Sangtamame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathya Sangramame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathya Sangramame"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi Janma Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi Janma Kaaranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chetta Kodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetta Kodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotu Kodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotu Kodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaavi Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaavi Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varasa Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasa Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Asale Gurthu Raadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Asale Gurthu Raadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadaite Chaale Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadaite Chaale Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aada Dante Evaru Raa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Dante Evaru Raa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadishakathi Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadishakathi Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Thalli Kaata Padina Chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Thalli Kaata Padina Chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni Gauravamtho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni Gauravamtho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Vanchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Vanchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aswathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aswathama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Barinchinaadu Gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barinchinaadu Gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Lothu Padhunu Gayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lothu Padhunu Gayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Darinchinaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darinchinaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganthulo Halahalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganthulo Halahalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaga Bhaga Jwalinchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhaga Jwalinchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhavanalam Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavanalam Veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sthereejaathike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sthereejaathike"/>
</div>
<div class="lyrico-lyrics-wrapper">Labhinchina Mahabalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labhinchina Mahabalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakanti Viluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakanti Viluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyanatti Durtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyanatti Durtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dussasalanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dussasalanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kachitanga Raastadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachitanga Raastadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Shasanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Shasanam"/>
</div>
</pre>
