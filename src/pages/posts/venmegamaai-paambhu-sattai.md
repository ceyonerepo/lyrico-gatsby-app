---
title: "venmegamaai song lyrics"
album: "Paambhu Sattai"
artist: "Ajeesh"
lyricist: "Vairamuthu"
director: "Adam Dasan"
path: "/albums/paambhu-sattai-lyrics"
song: "Venmegamaai"
image: ../../images/albumart/paambhu-sattai.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p6iZsbJc2X4"
type: "melody"
singers:
  - Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Venmegamaai Nee Pogiraai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmegamaai Nee Pogiraai "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaadhe Thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaadhe Thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeengalum Un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeengalum Un "/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhanthaan Vizhaadhe Thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhanthaan Vizhaadhe Thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaazhkkaiyo Siridhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaazhkkaiyo Siridhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaettkkaiyo Peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaettkkaiyo Peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Thedale Vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Thedale Vaazhvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thedinaal Adaivaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thedinaal Adaivaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi Valam Serkkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi Valam Serkkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu Munnerivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu Munnerivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Vaazhve Mul Kaadudhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Vaazhve Mul Kaadudhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyai Kondaadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyai Kondaadavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kanngalukkul Kaathirukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanngalukkul Kaathirukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Koadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Koadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Vaithaalo Vaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Vaithaalo Vaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkum Kavidhai Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkum Kavidhai Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaimai Edhuvo Valimai Adhuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaimai Edhuvo Valimai Adhuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thooimaiyae Kedumaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thooimaiyae Kedumaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosidhaan Vizhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosidhaan Vizhumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Latchiyathai Munniruthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Latchiyathai Munniruthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarndhu Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarndhu Podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaervai Kotti Mei 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaervai Kotti Mei "/>
</div>
<div class="lyrico-lyrics-wrapper">Varuthi Jeyithu Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthi Jeyithu Vaadaa"/>
</div>
</pre>
