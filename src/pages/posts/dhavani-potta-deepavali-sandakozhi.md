---
title: "dhavani potta deepavali song lyrics"
album: "Sandakozhi"
artist: "Yuvan Sankar Raja"
lyricist: "Yugabharathi"
director: "N. Linguswamy"
path: "/albums/sandakozhi-lyrics"
song: "Dhavani Potta Deepavali"
image: ../../images/albumart/sandakozhi.jpg
date: 2005-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FDK0FbOoWHM"
type: "Love"
singers:
  - 	Vijay Yesudas
  - 	Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhavanipotta Deepavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavanipotta Deepavali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu En Veetukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu En Veetukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Molachi Kaal Molachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Molachi Kaal Molachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduthu En paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthu En paattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Kanna Moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kanna Moochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enn Kanna Pinna Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn Kanna Pinna Pechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Pattam Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Pattam Poochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pakkam Vanthu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pakkam Vanthu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum Varuthu Pagalum Varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Varuthu Pagalum Varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Azaghu Sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Azaghu Sariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Eriya Kanakku Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Eriya Kanakku Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttuthu Muttuthu Moochu Muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttuthu Muttuthu Moochu Muttuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Kandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Kandale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottuthu Kottuthu Aruvi Kottuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthu Kottuthu Aruvi Kottuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arukil Ninnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arukil Ninnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittidu Vittidu Aala Vittidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittidu Vittidu Aala Vittidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polachu Poran Aampala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polachu Poran Aampala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum Varuthu Pagalum Varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Varuthu Pagalum Varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Azaghu Sarilya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Azaghu Sarilya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Eriya Kanakku Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Eriya Kanakku Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Vizhi Rendu Vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Vizhi Rendu Vizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaiyidum Kozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyidum Kozhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Viral Pathu Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Viral Pathu Viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panchu Metha Kozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchu Metha Kozhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pamparatha Pola Naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pamparatha Pola Naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Aadurene Margama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadurene Margama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Thanni Nee Kodukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Thanni Nee Kodukka "/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipogum Theerthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipogum Theerthama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magha Magha Kulame En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magha Magha Kulame En "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Ketha Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Ketha Mugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nava Pazha Nirame Enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nava Pazha Nirame Enna "/>
</div>
<div class="lyrico-lyrics-wrapper">Narukki Potta Nagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narukki Potta Nagame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithukku Mela Ithukku Mela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithukku Mela Ithukku Mela "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Ethum Thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Ethum Thonala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilakku Mela Vilakku Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilakku Mela Vilakku Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka Vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka Vanthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Adukku Paana Murukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Adukku Paana Murukku "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Udachu Thinnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Udachu Thinnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattazhagu Kattazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattazhagu Kattazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Pada Koodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Pada Koodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettiyiru Ettiyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettiyiru Ettiyiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Vegu Thoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vegu Thoorame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavada Katti Nirkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavada Katti Nirkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Paavalaru Paattu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavalaru Paattu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathathi Kesam Vara Paasathoda Kaattu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathathi Kesam Vara Paasathoda Kaattu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thekku Mara Jennal Nee Theva Loga Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekku Mara Jennal Nee Theva Loga Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ichamara Thottil Nee Elantha Pazha Kattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichamara Thottil Nee Elantha Pazha Kattil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruntha Vaalu Kurumbu Thezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruntha Vaalu Kurumbu Thezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanalum Neen Angelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Neen Angelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irakola Kulunga Kulunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakola Kulunga Kulunga "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichi Ninnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichi Ninnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Ora Vizhi Nadunga Nadunga Neruppu Vachane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Ora Vizhi Nadunga Nadunga Neruppu Vachane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhavanipotta Deepavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavanipotta Deepavali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathathu En Veetukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathathu En Veetukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Molachi Kaal Molachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Molachi Kaal Molachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduthu En Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthu En Paattukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Kannna Moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kannna Moochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna Pinna Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna Pinna Pechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Pattam Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Pattam Poochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pakkam vanthu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pakkam vanthu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttuthu Muttuthu Moochu Muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttuthu Muttuthu Moochu Muttuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Kandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Kandale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottuthu Kottuthu Aruvi Kottuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthu Kottuthu Aruvi Kottuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arukil Ninnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arukil Ninnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittidu Vittidu Aala Vittidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittidu Vittidu Aala Vittidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polachu Poran Aampala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polachu Poran Aampala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum Varuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Varuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalum Varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalum Varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Azaghu Sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Azaghu Sariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Eriya Kanakku Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Eriya Kanakku Puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum Varuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Varuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalum Varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalum Varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Azaghu Sariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Azaghu Sariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Eriya Kanakku Puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Eriya Kanakku Puriyala"/>
</div>
</pre>
