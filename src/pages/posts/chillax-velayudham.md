---
title: "chillax song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Chillax"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/17JGaTnoSAY"
type: "love"
singers:
  - Karthik
  - Charulatha Mani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manja Nethi Marathu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Nethi Marathu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya Vechi Mayaki Puta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Vechi Mayaki Puta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Katta Townu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Katta Townu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Kalandha Sema Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Kalandha Sema Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayi Rendum Urutu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayi Rendum Urutu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Vetta Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Vetta Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Ratham Sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Ratham Sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuku Varra Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuku Varra Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyanae Thevai Illa Vithudalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanae Thevai Illa Vithudalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Laamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiriyil Matum Inga Vachukalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiriyil Matum Inga Vachukalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupaachi Meesaiyilae Sikikalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupaachi Meesaiyilae Sikikalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Laamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyachu Naanaachu Paathukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyachu Naanaachu Paathukalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Chillax Chillax Chillak Chillak Chillax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillax Chillax Chillak Chillak Chillax"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja Nethi Marathu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Nethi Marathu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya Vechi Mayaki Puta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Vechi Mayaki Puta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Katta Townu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Katta Townu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Kalandha Sema Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Kalandha Sema Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayi Rendum Urutu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayi Rendum Urutu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Vetta Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Vetta Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Ratham Sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Ratham Sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuku Varra Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuku Varra Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheem Dheem Thanananam Dheem Dheem Thanananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Dheem Thanananam Dheem Dheem Thanananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah Aah Aah Aah Aah Aah Aah Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Aah Aah Aah Aah Aah Aah Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Udhatu Saayathula Otti Kolla Vaada Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udhatu Saayathula Otti Kolla Vaada Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Viral Thee Kuchiyae Patha Veika Vaadi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Viral Thee Kuchiyae Patha Veika Vaadi Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattabomman Peran Nee Kathi Meesa Veeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattabomman Peran Nee Kathi Meesa Veeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Vechu Kuthi Kollu Sethu Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Vechu Kuthi Kollu Sethu Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayavi Thaan Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayavi Thaan Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae Mayangiputen Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae Mayangiputen Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathangarai Moginiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathangarai Moginiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Vaadi Ennai Katti Pudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Ennai Katti Pudika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chillax Chillax Chillak Chillak Chillax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillax Chillax Chillak Chillak Chillax"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillaaaaax Chillax Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillaaaaax Chillax Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Udambu Panju Metha Kitta Vandhu Kaatu Vitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udambu Panju Metha Kitta Vandhu Kaatu Vitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Idupu Vaazhai Matta Naa Pudicha Thaanga Maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idupu Vaazhai Matta Naa Pudicha Thaanga Maata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhu Bondhu Veedu Nee Vandhu Vilayaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Bondhu Veedu Nee Vandhu Vilayaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaa Vaanga Thevaiyilla Kottaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaa Vaanga Thevaiyilla Kottaa Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetiya Thaan Serthu Un Maarapula Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetiya Thaan Serthu Un Maarapula Korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennavo Pannuriyae Nenju Kulla Ketta Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennavo Pannuriyae Nenju Kulla Ketta Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Chillax Chillax Chillak Chillak Chillax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillax Chillax Chillak Chillak Chillax"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja Nethi Marathu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Nethi Marathu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiya Vechi Mayaki Puta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiya Vechi Mayaki Puta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Katta Townu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Katta Townu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Kalandha Sema Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Kalandha Sema Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayi Rendum Urutu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayi Rendum Urutu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Vetta Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Vetta Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Ratham Sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Ratham Sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuku Varra Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuku Varra Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyanae Thevai Illa Vithudalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanae Thevai Illa Vithudalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Laamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiriyil Matum Inga Vachukalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiriyil Matum Inga Vachukalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupaachi Meesaiyilae Sikikalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupaachi Meesaiyilae Sikikalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Laamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyachu Naanaachu Paathukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyachu Naanaachu Paathukalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Chillax Chillax Chillak Chillak Chillax
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillax Chillax Chillak Chillak Chillax"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah Aah Aah Aah Aah Aah Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Aah Aah Aah Aah Aah Aah"/>
</div>
</pre>
