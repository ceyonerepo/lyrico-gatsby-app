---
title: "diname diname song lyrics"
album: "Kilometers and Kilometers"
artist: "Sooraj S. Kurup"
lyricist: "B. K. Harinarayanan"
director: "Jeo Baby"
path: "/albums/kilometers-and kilometers-lyrics"
song: "Diname Diname"
image: ../../images/albumart/kilometers-and-kilometers.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/F_A7a-zdG4k"
type: "happy"
singers:
  - Mridul Anil
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Diname Diname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diname Diname"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Chirakakunnu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Chirakakunnu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruthil Muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruthil Muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyan Nizhalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyan Nizhalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Palathilithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Palathilithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanju Kaalamakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Kaalamakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaka Thulya Viralaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaka Thulya Viralaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punarumorasilivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punarumorasilivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Palathilithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Palathilithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanju Kaalamakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Kaalamakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaka Thulya Viralaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaka Thulya Viralaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punarumorasilivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punarumorasilivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa Ahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gathivekam Muriyum Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathivekam Muriyum Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyano Thuniyum Manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyano Thuniyum Manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thinkal Kalapol Mukilil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thinkal Kalapol Mukilil"/>
</div>
<div class="lyrico-lyrics-wrapper">Marayano Kanavin Thiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marayano Kanavin Thiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orkkum Thorum Yadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orkkum Thorum Yadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyayi Vaarum Sathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyayi Vaarum Sathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Diname Diname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diname Diname"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Chirakakunnu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Chirakakunnu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruthil Muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruthil Muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyan Nizhalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyan Nizhalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Palathilithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Palathilithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanju Kaalamakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Kaalamakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaka Thulya Viralaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaka Thulya Viralaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punarumorasilivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punarumorasilivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ven Dhoomamayi Mrithiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Dhoomamayi Mrithiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Padarunna Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padarunna Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerilayi Smrithiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerilayi Smrithiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitharunnu Thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitharunnu Thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatha Palathilithile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Palathilithile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanju Kaalamakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanju Kaalamakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaka Thulya Viralaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaka Thulya Viralaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punarumorasilivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punarumorasilivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa Ahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm"/>
</div>
</pre>
