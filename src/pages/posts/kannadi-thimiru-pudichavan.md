---
title: "kannadi song lyrics"
album: "Thimiru Pudichavan"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Ganeshaa"
path: "/albums/thimiru-pudichavan-lyrics"
song: "Kannadi"
image: ../../images/albumart/thimiru-pudichavan.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r1VKgGE6Tek"
type: "love"
singers:
  - Abhay Jodhpurkar
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannadi sillagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi sillagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thool aanen unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thool aanen unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannalae kaithagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalae kaithagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen naan annaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen naan annaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmooda villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmooda villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthayam pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthayam pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urangavum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangavum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkae nee unnaiyae meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkae nee unnaiyae meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithaai kuduthaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaai kuduthaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirukkul nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukkul nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyavum illai ariyavum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyavum illai ariyavum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam kai soottil ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam kai soottil ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Urigida vaithaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urigida vaithaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pona jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pona jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vanthu pogum endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vanthu pogum endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee seiyuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee seiyuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh unnudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh unnudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhntha vazhvai enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhntha vazhvai enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkalai oottuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkalai oottuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal koottuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal koottuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aayul regai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aayul regai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyil ooduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyil ooduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae kai koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae kai koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae enthan arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae enthan arugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey orr nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey orr nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee parthu pona pinbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee parthu pona pinbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan thookamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan thookamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porkodi thookki poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkodi thookki poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh unnidan pesavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh unnidan pesavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai ondru serthu vaithum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai ondru serthu vaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerilae parthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerilae parthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththangal kuduthaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal kuduthaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viruthangal mudithaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruthangal mudithaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru jenmam naanum eduthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru jenmam naanum eduthaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you love you love you bae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you love you love you bae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi sillagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi sillagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thool aanen unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thool aanen unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannalae kaithagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalae kaithagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen naan annaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen naan annaalae"/>
</div>
</pre>
