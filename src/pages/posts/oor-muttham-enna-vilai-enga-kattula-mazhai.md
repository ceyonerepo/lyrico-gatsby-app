---
title: "oor muttham enna vilai song lyrics"
album: "Enga Kattula Mazhai"
artist: "Srivijay"
lyricist: "Snehan"
director: "Sri Balaji"
path: "/albums/enga-kattula-mazhai-lyrics"
song: "Oor Muttham Enna Vilai"
image: ../../images/albumart/enga-kattula-mazhai.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2AXpBxA0tRc"
type: "love"
singers:
  - Haricharan
  - Suganya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oor Muththam Enna Vilai uyirai ketkirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Muththam Enna Vilai uyirai ketkirai "/>
</div>
<div class="lyrico-lyrics-wrapper">nan enna seithen pizhai vetti saikirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan enna seithen pizhai vetti saikirai "/>
</div>
<div class="lyrico-lyrics-wrapper">va mudhal irave mudhalil thodangatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va mudhal irave mudhalil thodangatume "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudhal uravaai ulagil irukatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudhal uravaai ulagil irukatume "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un kaitheenda entheygam katril aeruthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaitheenda entheygam katril aeruthey "/>
</div>
<div class="lyrico-lyrics-wrapper">oor Muththam unthan uyirai villayai ketkuthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor Muththam unthan uyirai villayai ketkuthey "/>
</div>
<div class="lyrico-lyrics-wrapper">nee seitha chella pizhai vetti saikuthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seitha chella pizhai vetti saikuthey "/>
</div>
<div class="lyrico-lyrics-wrapper">va mudhal irave mudhalil thodangatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va mudhal irave mudhalil thodangatume "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudhal uravaai ulagil irukatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudhal uravaai ulagil irukatume "/>
</div>
<div class="lyrico-lyrics-wrapper">nee theenda entheygam katril aeriduthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee theenda entheygam katril aeriduthey "/>
</div>
<div class="lyrico-lyrics-wrapper">konjalum kenjalum minjalum anjalum kathalin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjalum kenjalum minjalum anjalum kathalin "/>
</div>
<div class="lyrico-lyrics-wrapper">bhashaiyadi athai kangale pesumadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhashaiyadi athai kangale pesumadi "/>
</div>
<div class="lyrico-lyrics-wrapper">uudalum kuudalum theydalum nadalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uudalum kuudalum theydalum nadalum "/>
</div>
<div class="lyrico-lyrics-wrapper">udalkalin vaarthaiyada athai udalkale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalkalin vaarthaiyada athai udalkale "/>
</div>
<div class="lyrico-lyrics-wrapper">pesumada pagalai iravaku athiley enai theku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesumada pagalai iravaku athiley enai theku"/>
</div>
<div class="lyrico-lyrics-wrapper">aahaa thevai kuduthey sevai theduthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahaa thevai kuduthey sevai theduthey "/>
</div>
<div class="lyrico-lyrics-wrapper">vervai theemuutu aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vervai theemuutu aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">athiley enai vattu aahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athiley enai vattu aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">penmai vaaduthey unmai theduthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penmai vaaduthey unmai theduthey "/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo engeyo thesaimari poovamva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo engeyo thesaimari poovamva "/>
</div>
<div class="lyrico-lyrics-wrapper">aathamum evallai maru jenmam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathamum evallai maru jenmam "/>
</div>
<div class="lyrico-lyrics-wrapper">aavomva bhumiyai puthidhai parpom va 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavomva bhumiyai puthidhai parpom va "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Muththam Enna Vilai uyirai ketkirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Muththam Enna Vilai uyirai ketkirai "/>
</div>
<div class="lyrico-lyrics-wrapper">nan enna seithen pizhai vetti saikirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan enna seithen pizhai vetti saikirai "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnidam uruguven unnaiye paruguven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnidam uruguven unnaiye paruguven "/>
</div>
<div class="lyrico-lyrics-wrapper">katilil midhathiduven ennai thorkadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katilil midhathiduven ennai thorkadi "/>
</div>
<div class="lyrico-lyrics-wrapper">pukazhthiduven aasaigal anaithilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pukazhthiduven aasaigal anaithilum "/>
</div>
<div class="lyrico-lyrics-wrapper">karanum theduven siramangal enakilaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karanum theduven siramangal enakilaye "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai sithaipathu kanakilaye alayai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai sithaipathu kanakilaye alayai "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinum modhi udaithen saripaathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinum modhi udaithen saripaathi "/>
</div>
<div class="lyrico-lyrics-wrapper">onrayai neeyada venrai poovada iravo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onrayai neeyada venrai poovada iravo "/>
</div>
<div class="lyrico-lyrics-wrapper">niramari illamai ratham eri vanthai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niramari illamai ratham eri vanthai "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyadi thanthai noyadi sorgathin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyadi thanthai noyadi sorgathin "/>
</div>
<div class="lyrico-lyrics-wrapper">munnale munnale nirkinrom yarkayel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnale munnale nirkinrom yarkayel "/>
</div>
<div class="lyrico-lyrics-wrapper">yarkayel saviena ketkinrom enkayel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarkayel saviena ketkinrom enkayel "/>
</div>
<div class="lyrico-lyrics-wrapper">savi ullathadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savi ullathadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Muththam Enna Vilai uyirai ketkirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Muththam Enna Vilai uyirai ketkirai "/>
</div>
<div class="lyrico-lyrics-wrapper">nee seitha chella pizhai vetti saikuthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seitha chella pizhai vetti saikuthey "/>
</div>
<div class="lyrico-lyrics-wrapper">va mudhal irave mudhalil thodangatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va mudhal irave mudhalil thodangatume "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudhal uravaai ulagil irukatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudhal uravaai ulagil irukatume "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudhal uravaai ulagil irukatume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudhal uravaai ulagil irukatume "/>
</div>
</pre>
