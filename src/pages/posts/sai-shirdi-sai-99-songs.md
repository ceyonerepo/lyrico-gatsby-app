---
title: "sai shirdi sai song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Mashook Rahman"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Sai Shirdi Sai"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o-no_OOBUA4"
type: "Devotional"
singers:
  - Bela Shende
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagin arasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin arasae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvin oli aavaaiyooo….oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvin oli aavaaiyooo….oo"/>
</div>
<div class="lyrico-lyrics-wrapper">En nilai paaraaiyooo mannavaaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nilai paaraaiyooo mannavaaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Endan saai… nee…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endan saai… nee…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin arasae vaaraaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin arasae vaaraaiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvin oli aavaaiyoo…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvin oli aavaaiyoo…ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal mozhi aavaaiyoo mannavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal mozhi aavaaiyoo mannavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan saai… nee…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan saai… nee…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En swaasathin dhaagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swaasathin dhaagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai."/>
</div>
<div class="lyrico-lyrics-wrapper">En naadiyin naadhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadiyin naadhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhaya dhaabam thaniyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhaya dhaabam thaniyumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai."/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ezhai thaedum arasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ezhai thaedum arasanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaa…..aaa…..aaa…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaa…..aaa…..aaa….."/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm…aa…aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm…aa…aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa…re….aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa…re….aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir saai…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir saai…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee aega sruthiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aega sruthiyaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarvam nee saaii….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam nee saaii…."/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aega sruthiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aega sruthiyaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarvam nee saai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam nee saai…."/>
</div>
<div class="lyrico-lyrics-wrapper">Guruvaai varuvaai nee saai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruvaai varuvaai nee saai…."/>
</div>
<div class="lyrico-lyrics-wrapper">Karai serkkum karamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai serkkum karamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha kadalae saai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha kadalae saai…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En swaasathin dhaagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swaasathin dhaagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai…."/>
</div>
<div class="lyrico-lyrics-wrapper">En naadiyin naadhamae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadiyin naadhamae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagin arasae vaaraaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin arasae vaaraaiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin oli aavaaiyoo…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin oli aavaaiyoo…ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai arulvaaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai arulvaaiyooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En swaasathin dhaagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swaasathin dhaagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai…."/>
</div>
<div class="lyrico-lyrics-wrapper">En naadiyin naadhamae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadiyin naadhamae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paaradadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paaradadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paavam ariyaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paavam ariyaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaai ullam anaikkummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaai ullam anaikkummaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aah aahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aah aahh"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aah aahh aaa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aah aahh aaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aah aahh ….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aah aahh …."/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aah aahh …haaa…aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aah aahh …haaa…aaa…aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa….aaa….aaa.haaa….aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa….aaa….aaa.haaa….aaa…aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaai ullam anaikkummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaai ullam anaikkummaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai."/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvin artham kidaikkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvin artham kidaikkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaaai……saaaai……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaaai……saaaai……"/>
</div>
<div class="lyrico-lyrics-wrapper">En suvana kadhavum thirakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En suvana kadhavum thirakkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm ooo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm ooo hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shirdi….saaaai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shirdi….saaaai.."/>
</div>
<div class="lyrico-lyrics-wrapper">En irai nesa naadhanae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En irai nesa naadhanae…"/>
</div>
</pre>
