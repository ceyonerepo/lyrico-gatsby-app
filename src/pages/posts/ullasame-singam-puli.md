---
title: "ullasame song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Ullasame"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sCH9774Iujo"
type: "happy"
singers:
  - Ranjith
  - Janani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullaasame Nee Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasame Nee Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sandhoshame Ennulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhoshame Ennulle Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchaaghame Nee Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urchaaghame Nee Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Inbam Ellaam Pennulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Inbam Ellaam Pennulle Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Lets Lets Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Lets Lets Life"/>
</div>
<div class="lyrico-lyrics-wrapper">Love With Those Spice
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love With Those Spice"/>
</div>
<div class="lyrico-lyrics-wrapper">Hear Wat He Says And Go With It Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hear Wat He Says And Go With It Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Has A Sorrow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Has A Sorrow"/>
</div>
<div class="lyrico-lyrics-wrapper">Live Like Tomorrow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live Like Tomorrow"/>
</div>
<div class="lyrico-lyrics-wrapper">I Called You Cummon And A Come With Me Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Called You Cummon And A Come With Me Boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaattam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamathil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamathil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraadha Peran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraadha Peran"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhan Madhan Madhan Madhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhan Madhan Madhan Madhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhan Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasame Nee Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasame Nee Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sandhoshame Ennulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhoshame Ennulle Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penn Enum Inam Mattum Illaiyinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Enum Inam Mattum Illaiyinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan Magan Sugam Inge Ennaavadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan Magan Sugam Inge Ennaavadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niram Gunam Manam Ulla Pennin Munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram Gunam Manam Ulla Pennin Munnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Porul Panam Ellaam Veen Thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Panam Ellaam Veen Thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yes U Rite Baby Cum With Me Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes U Rite Baby Cum With Me Now"/>
</div>
<div class="lyrico-lyrics-wrapper">There Is No Rite Will Live Like U Want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There Is No Rite Will Live Like U Want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuvile Palappala Ragatha Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuvile Palappala Ragatha Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhulaiyum Pudhu Pudhu Suvaiya Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhulaiyum Pudhu Pudhu Suvaiya Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Erangi Nee Vilaiyaadudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Erangi Nee Vilaiyaadudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiya Inimaiyaa Kondaadu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiya Inimaiyaa Kondaadu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance It Up Baby Shake It Up Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance It Up Baby Shake It Up Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing With Me Baby Love Life Rite Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing With Me Baby Love Life Rite Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaattam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamathil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamathil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraadha Peran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraadha Peran"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhan Madhan Madhan Madhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhan Madhan Madhan Madhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhan Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasame Nee Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasame Nee Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sandhoshame Ennulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhoshame Ennulle Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangarai Vilakkamum Thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangarai Vilakkamum Thevaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalula Moozhgura Aalum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalula Moozhgura Aalum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhulaiyum Ivanukku Tholviyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhulaiyum Ivanukku Tholviyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oru Murai Pazhagittaa Chellapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oru Murai Pazhagittaa Chellapulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yes U Rite Baby Cum With Me Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes U Rite Baby Cum With Me Now"/>
</div>
<div class="lyrico-lyrics-wrapper">There Is No Rite Will Live Like U Want
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There Is No Rite Will Live Like U Want"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhamum Nanjumaai Kalandhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhamum Nanjumaai Kalandhiruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhaikkandu Gavanamaai Pirithedudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaikkandu Gavanamaai Pirithedudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Maru Jananamum Unakkillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Maru Jananamum Unakkillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvarai Sagalamum Anubhavidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvarai Sagalamum Anubhavidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance It Up Baby Shake It Up Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance It Up Baby Shake It Up Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing With Me Baby Love Life Rite Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing With Me Baby Love Life Rite Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kaadhalil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kaadhalil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadha Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaattam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamathil Naan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamathil Naan Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraadha Peran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraadha Peran"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhan Madhan Madhan Madhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhan Madhan Madhan Madhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhan Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhan Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasame Nee Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasame Nee Ennodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sandhoshame Ennulle Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhoshame Ennulle Dhaan"/>
</div>
</pre>
