---
title: "azhagu ponnu onnu song lyrics"
album: "Pririyadha Varam Vendum"
artist: "S. A. Rajkumar"
lyricist: "Pazhani Bharathi"
director: "	Kamal"
path: "/albums/pririyadha-varam-vendum-lyrics"
song: "Azhagu Ponnu Onnu"
image: ../../images/albumart/piriyadha-varam-vendum.jpg
date: 2001-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yuCUQ2RFHBQ"
type: "happy"
singers:
  - Anuradha Sriram
  - Devan
  - Krishnaraj
  - Jayanthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagu Ponnu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Ponnu Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Partha Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boy Friendu Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy Friendu Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endral Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endral Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagu Ponnu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Ponnu Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Partha Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boy Friendu Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy Friendu Neethan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endral Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endral Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appaavin Pocketil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaavin Pocketil Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruda Thuttu Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruda Thuttu Iruntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mun Veetu Pen Veetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Veetu Pen Veetil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Panna Signal Vizhuntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Panna Signal Vizhuntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theatre Iruttukku Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theatre Iruttukku Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Corner Seattukku Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corner Seattukku Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beach ku Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beach ku Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundal Payanukku Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundal Payanukku Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhgu Paiyan Onna Partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhgu Paiyan Onna Partha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girl Friendu Neethaan Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl Friendu Neethaan Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sukriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sukriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Adichadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Adichadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leave Kidaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leave Kidaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiriyoda Koottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiriyoda Koottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cross Talkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cross Talkil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jodi Maattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodi Maattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluda Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluda Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parichai Ketha Bit Kedaikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichai Ketha Bit Kedaikutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bus Stop il Bussu Ninnutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus Stop il Bussu Ninnutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ration La Sarkkarai Kidaikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ration La Sarkkarai Kidaikutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluda Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluda Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichi Pesum Conductor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichi Pesum Conductor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Sevai Therinja Doctor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Sevai Therinja Doctor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamilil Pesum Colector
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilil Pesum Colector"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Somdha Kasil Unnum Inspector
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somdha Kasil Unnum Inspector"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhuvazhvil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhuvazhvil Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhikkum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhikkum Podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluda Sukkuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluda Sukkuriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhgu Paiyan Onna Partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhgu Paiyan Onna Partha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girl Friendu Neethaan Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl Friendu Neethaan Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sukriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sukriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam Kuduthathum Seatu Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Kuduthathum Seatu Kedaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhavikkoru Route Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhavikkoru Route Kedaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Pudikkavum Rate Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pudikkavum Rate Kedaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluda Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluda Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoguthiyil Idai Therthal Varugutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoguthiyil Idai Therthal Varugutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Poda Koottam Varugutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Poda Koottam Varugutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaveriyil Vellam Perugutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaveriyil Vellam Perugutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solluda Sukriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluda Sukriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhi Selai Nazhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhi Selai Nazhuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Munne Pesum Maanikkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munne Pesum Maanikkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottrai Punnagai Thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai Punnagai Thanthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Impress Agi Thinam Sandhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Impress Agi Thinam Sandhikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirukku Nee Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirukku Nee Kudukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagu Poonu Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Poonu Onna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartha Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boy Friendu Needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy Friendu Needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endral Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endral Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theatre Iruttukku Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theatre Iruttukku Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Corner Seattukku Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corner Seattukku Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beach ku Sollu Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beach ku Sollu Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundal Payanukku Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundal Payanukku Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Sukkriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Sukkriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkriya Ahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkriya Ahhh"/>
</div>
</pre>
