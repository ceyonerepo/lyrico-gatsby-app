---
title: "unna nenachu nenachu song lyrics"
album: "Psycho"
artist: "Ilayaraja"
lyricist: "Kabilan"
director: "Mysskin"
path: "/albums/psycho-song-lyrics"
song: "Unna Nenachu Nenachu"
image: ../../images/albumart/psycho.jpg
date: 2020-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jFWsj_QT0G8"
type: "Sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Avalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Avalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Theendum Kaatrin Viralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Theendum Kaatrin Viralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Avalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Avalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaalaattum Thaayin Kuralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaalaattum Thaayin Kuralo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasam Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivai Thaane Endhan Uravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivai Thaane Endhan Uravey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Neenda Iravendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Neenda Iravendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Iravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Iravey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Unnaal Ennai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Unnaal Ennai Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Moodi Kadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Moodi Kadhal Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Ponaalum Paadhai Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Ponaalum Paadhai Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thavira Unnidam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thavira Unnidam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethuvum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuvum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhu Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhu Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaatha Yezhai Ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaatha Yezhai Ivano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Thirandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Thirandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaatha Oomai Ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaatha Oomai Ivano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhil Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhil Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheebam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheebam Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyil Naan Yenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Naan Yenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Neethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyillaamal Kanneerukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyillaamal Kanneerukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgi Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moozhgi Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Avalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Avalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Theendum Kaatrin Viralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Theendum Kaatrin Viralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Avalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Avalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaalaattum Thaayin Kuralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaalaattum Thaayin Kuralo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachu Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi Ponen Mezhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi Ponen Mezhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja Odhachu Odhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Odhachu Odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu Ponaa Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Ponaa Azhagaa"/>
</div>
</pre>
