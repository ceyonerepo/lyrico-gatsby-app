---
title: "azhagu thalire song lyrics"
album: "Chinnanjiru Kiliye"
artist: "Mastan Khader"
lyricist: "Geetha Karthik Netha"
director: "Sabarinathan Muthupandian "
path: "/albums/chinnanjiru-kiliye-lyrics"
song: "Azhagu Thalire"
image: ../../images/albumart/chinnanjiru-kiliye.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EMwdzMoKxEY"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">azhagu thalire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu thalire "/>
</div>
<div class="lyrico-lyrics-wrapper">valarum piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum piraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan dhisaiyum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan dhisaiyum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangum moochum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangum moochum"/>
</div>
<div class="lyrico-lyrics-wrapper">thengum paechum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thengum paechum"/>
</div>
<div class="lyrico-lyrics-wrapper">dheiva magale neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheiva magale neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kizhinjal polae odhungi ponaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhinjal polae odhungi ponaen"/>
</div>
<div class="lyrico-lyrics-wrapper">muthena vanthaai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthena vanthaai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">enadhu irullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enadhu irullai"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham soozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham soozha"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalai thandhai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalai thandhai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">enuyir neeyadi un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enuyir neeyadi un "/>
</div>
<div class="lyrico-lyrics-wrapper">nizhal naan dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhal naan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir neeyadi unadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir neeyadi unadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalil vaazhvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalil vaazhvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhagu thalire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu thalire "/>
</div>
<div class="lyrico-lyrics-wrapper">valarum piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum piraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan dhisaiyum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan dhisaiyum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangum moochum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangum moochum"/>
</div>
<div class="lyrico-lyrics-wrapper">thengum paechum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thengum paechum"/>
</div>
<div class="lyrico-lyrics-wrapper">dheiva magale neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheiva magale neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelelelo thalelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelelelo thalelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">yelelelo thalelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelelelo thalelelo"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhiyil irandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyil irandu"/>
</div>
<div class="lyrico-lyrics-wrapper">thooli amaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooli amaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayile unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayile unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaati vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaati vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">paathai enave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathai enave"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum virinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum virinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paatha malarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatha malarai"/>
</div>
<div class="lyrico-lyrics-wrapper">entha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu pooranam aagirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu pooranam aagirai"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu thai madi aagiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu thai madi aagiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">netri varudum oar nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netri varudum oar nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">sisuvaai marinaen naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sisuvaai marinaen naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">theentamizh amizhdham neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theentamizh amizhdham neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varum sorgame neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varum sorgame neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">theentamizh amizhdham neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theentamizh amizhdham neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varum sorgame neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varum sorgame neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhagu thalire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu thalire "/>
</div>
<div class="lyrico-lyrics-wrapper">valarum piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum piraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan dhisaiyum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan dhisaiyum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangum moochum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangum moochum"/>
</div>
<div class="lyrico-lyrics-wrapper">thengum paechum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thengum paechum"/>
</div>
<div class="lyrico-lyrics-wrapper">dheiva magale neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheiva magale neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyiril irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiril irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sollai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theynae unakaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theynae unakaai"/>
</div>
<div class="lyrico-lyrics-wrapper">isaithu vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaithu vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">thoagai neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoagai neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">koalgal yeazhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koalgal yeazhum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhil karuvaai sumakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhil karuvaai sumakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">enaiye unakkai padaikiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaiye unakkai padaikiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam neeyaai paarkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam neeyaai paarkiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayae thaanae ketkiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayae thaanae ketkiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">theentamizh amizhdham neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theentamizh amizhdham neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varum sorgame neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varum sorgame neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">theentamizh amizhdham neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theentamizh amizhdham neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varum sorgame neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varum sorgame neeyae"/>
</div>
</pre>
