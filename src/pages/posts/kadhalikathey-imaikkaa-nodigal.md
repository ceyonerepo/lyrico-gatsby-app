---
title: "kadhalikathey song lyrics"
album: "Imaikkaa Nodigal"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "R. Ajay Gnanamuthu"
path: "/albums/imaikkaa-nodigal-lyrics"
song: "Kadhalikathey"
image: ../../images/albumart/imaikkaa-nodigal.jpg
date: 2018-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/X0ss4GI0RCA"
type: "sad"
singers:
  - Hiphop Tamizha
  - Kaushik Krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhalikathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Kanda Naaiyelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanda Naaiyelaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendunu Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendunu Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyaana Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaana Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechanda Kolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechanda Kolli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda Kanda Naaiyelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanda Naaiyelaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendunu Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendunu Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyaana Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaana Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechanda Kolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechanda Kolli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalum Naanum Irukumattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalum Naanum Irukumattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Vu Romba Superu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Vu Romba Superu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvulatha Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvulatha Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Friendunu Oru Jokeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Friendunu Oru Jokeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyilathaan Maatikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyilathaan Maatikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthaanda Sekaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthaanda Sekaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Coolana Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Coolana Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension Agumunne Odidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Agumunne Odidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashtapattu Correctu Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtapattu Correctu Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Figure Unakku Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Figure Unakku Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattum Maattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattum Maattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naal Maattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Maattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matthavanga Figure Ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthavanga Figure Ella"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtathukku Correct Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtathukku Correct Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Figure Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Figure Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirma Oore Oottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirma Oore Oottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoonga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Night Ellam Kadala Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Night Ellam Kadala Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Vidalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanga Vidalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Vidalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vaanga Vidalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaanga Vidalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paritchayila Pass Marku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paritchayila Pass Marku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Vidalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Vidalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikathaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikathaey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Kaadhalikathaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Kaadhalikathaey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalichi Kadaisiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalichi Kadaisiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyithil Thongaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyithil Thongaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadarkaraiyila Sundal Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadarkaraiyila Sundal Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valartha Kaadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valartha Kaadhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikku Internetil Twitter Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikku Internetil Twitter Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valakkuraangappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valakkuraangappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munna Pinna Theriyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna Pinna Theriyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Koodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Koodathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalapottu Friendship Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalapottu Friendship Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilikkuraangappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilikkuraangappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaadi Nadakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaadi Nadakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmamaa Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmamaa Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhavan Ponavan Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhavan Ponavan Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Katti Pudipadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Katti Pudipadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lighta Valikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lighta Valikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu Thudikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Thudikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Ayyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Ayyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamma Nadikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamma Nadikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoonga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Night Ellam Kadala Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Night Ellam Kadala Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Vidalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vaanga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaanga Vidalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paritchayila Pass Marku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paritchayila Pass Marku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Vidalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Vidalaye"/>
</div>
</pre>
