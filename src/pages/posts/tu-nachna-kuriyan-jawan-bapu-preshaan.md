---
title: "tu nachna song lyrics"
album: "Kuriyan Jawan Bapu Preshaan"
artist: "Laddi Gill"
lyricist: "Happy Raikoti"
director: "Avtar Singh"
path: "/albums/kuriyan-jawan-bapu-preshaan-lyrics"
song: "Tu Nachna"
image: ../../images/albumart/kuriyan-jawan-bapu-preshaan.jpg
date: 2021-04-16
lang: panjabi
youtubeLink: "https://www.youtube.com/embed/qgOKrxeVDos"
type: "Enjoy"
singers:
  - Gurnam Bhullar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho gabru saare tainu takkde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho gabru saare tainu takkde"/>
</div>
<div class="lyrico-lyrics-wrapper">Sang te sandal laake rakhde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sang te sandal laake rakhde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho gabru saare tainu takkde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho gabru saare tainu takkde"/>
</div>
<div class="lyrico-lyrics-wrapper">Sang te sandal laake rakhde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sang te sandal laake rakhde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni lakh lakh mull miluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni lakh lakh mull miluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Beshkeemti ne lakk de hulare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beshkeemti ne lakk de hulare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir ho jinna chir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir ho jinna chir"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda raat rok lu naare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda raat rok lu naare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass kehda tainu aa pasand billo geet ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass kehda tainu aa pasand billo geet ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere hisaab naal ve judi je utte beat ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere hisaab naal ve judi je utte beat ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass kehda tainu aa pasand billo geet ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass kehda tainu aa pasand billo geet ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere hisaab naal vajju deed utte beat ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere hisaab naal vajju deed utte beat ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho takk ke beauty teri nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho takk ke beauty teri nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekh ambran ton tuttde ne taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh ambran ton tuttde ne taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir ni jinna chir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir ni jinna chir"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jinna chir tu nachna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda raat rok lu naare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda raat rok lu naare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sohniyan tan bahut par teri jehi hor ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sohniyan tan bahut par teri jehi hor ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere naal chadh gi club nu vi lor ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere naal chadh gi club nu vi lor ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sohniyan tan bahut par teri jehi hor ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sohniyan tan bahut par teri jehi hor ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere naal chadh gi club nu vi lor ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere naal chadh gi club nu vi lor ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni saariyan hi ankhan tere te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni saariyan hi ankhan tere te"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahe dekh le ghoom passe chare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahe dekh le ghoom passe chare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir ni jinna chir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir ni jinna chir"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda raat rok lu naare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda raat rok lu naare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho din na chadhu mutiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din na chadhu mutiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jinna chir tu nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jinna chir tu nachna"/>
</div>
</pre>
