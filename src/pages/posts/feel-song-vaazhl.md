---
title: "feel song song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Arun Prabhu Purushothaman"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Feel Song"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UHZ5kYaX0_E"
type: "happy"
singers:
  - Deva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">hard to feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hard to feel"/>
</div>
<div class="lyrico-lyrics-wrapper">too hard to feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="too hard to feel"/>
</div>
<div class="lyrico-lyrics-wrapper">veetukulla than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukulla than"/>
</div>
<div class="lyrico-lyrics-wrapper">mohatha maraikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohatha maraikura"/>
</div>
<div class="lyrico-lyrics-wrapper">kasukaga than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasukaga than"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa olikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa olikira"/>
</div>
<div class="lyrico-lyrics-wrapper">gatekulla adanjukittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gatekulla adanjukittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">theevukulla maatikittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theevukulla maatikittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">get ready folks
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="get ready folks"/>
</div>
<div class="lyrico-lyrics-wrapper">ha to feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha to feel"/>
</div>
<div class="lyrico-lyrics-wrapper">ha to feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha to feel"/>
</div>
<div class="lyrico-lyrics-wrapper">ha to feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ha to feel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ott la than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ott la than"/>
</div>
<div class="lyrico-lyrics-wrapper">olagam suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olagam suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">online la than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="online la than"/>
</div>
<div class="lyrico-lyrics-wrapper">poosa nadathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosa nadathura"/>
</div>
<div class="lyrico-lyrics-wrapper">phone kulla maatikittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phone kulla maatikittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">podhaikulla sikikittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhaikulla sikikittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaiya kaluvuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya kaluvuna"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai maruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai maruma"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kaluvuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kaluvuna"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">arrear vachu all pass ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arrear vachu all pass ah"/>
</div>
<div class="lyrico-lyrics-wrapper">gdp than poota case ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gdp than poota case ah"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<div class="lyrico-lyrics-wrapper">how to feelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how to feelu"/>
</div>
<div class="lyrico-lyrics-wrapper">how how how 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how how how "/>
</div>
<div class="lyrico-lyrics-wrapper">how how
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="how how"/>
</div>
</pre>
