---
title: "mandaara song lyrics"
album: "Bhaagamathie"
artist: "S Thaman"
lyricist: "Sreejo"
director: "G Ashok"
path: "/albums/bhaagamathie-lyrics"
song: "Mandaara"
image: ../../images/albumart/bhaagamathie-telugu.jpg
date: 2018-01-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fdgAMPzwqnU"
type: "melody"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mandaara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Thellaarelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Thellaarelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiranaale Nanne Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiranaale Nanne Cherelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaaraa Kallaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaraa Kallaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthunnaa Kallaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunnaa Kallaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikotha Sneham Darichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotha Sneham Darichera"/>
</div>
<div class="lyrico-lyrics-wrapper">Alikidi Chese Naalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alikidi Chese Naalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Adagani Prashne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagani Prashne"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Asaladhi Badhulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Asaladhi Badhulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Adhi Thelenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Adhi Thelenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudurugaaa Unde Madhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudurugaaa Unde Madhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipigaa Egire Edhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipigaa Egire Edhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyani Bhaavam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyani Bhaavam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thelise Katha Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelise Katha Maarenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Venta Aduge Vesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Venta Aduge Vesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Needanai Gamanisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Needanai Gamanisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ninnallo Leni Nanne ilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ninnallo Leni Nanne ilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Chusthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Chusthunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Thellaarelaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Thellaarelaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kiranaale Nanne Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kiranaale Nanne Cherelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaaraa Kallaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaraa Kallaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthunnaavaa Kallaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunnaavaa Kallaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sarikotha Sneham Dharicheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sarikotha Sneham Dharicheraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaara Sundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaara Sundara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Thellaarelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Thellaarelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiranaale Nanne Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiranaale Nanne Cherelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urikini Chaate Oopiri Kudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urikini Chaate Oopiri Kudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliki Padelaa Undhe Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliki Padelaa Undhe Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalonainaa Kalaganaledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalonainaa Kalaganaledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidipothundhani Aramarikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidipothundhani Aramarikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalai Naalo Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalai Naalo Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanai Neelo Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanai Neelo Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatai Odhige Kshaname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatai Odhige Kshaname "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Premenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Premenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaalane Maripisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaalane Maripisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhame Andhisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhame Andhisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prayaanamai Naa Gamyanivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prayaanamai Naa Gamyanivai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nuvvavuthunnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nuvvavuthunnaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Thellaarelaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Thellaarelaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kiranaale Nanne Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kiranaale Nanne Cherelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaaraa Kallaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaaraa Kallaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthunnaavaa Kallaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthunnaavaa Kallaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sarikotha Sneham Dharicheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sarikotha Sneham Dharicheraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaara Mandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaara Mandaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Thellaarelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Thellaarelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiranaale Nanne Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiranaale Nanne Cherelaa"/>
</div>
</pre>
