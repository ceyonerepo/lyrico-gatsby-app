---
title: "gift of life song lyrics"
album: "Raja Ranguski"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Raj"
director: "Dharani Dharan"
path: "/albums/raja-ranguski-lyrics"
song: "Gift of Life"
image: ../../images/albumart/raja-ranguski.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-unF87PK2Nw"
type: "melody"
singers:
  - Faridha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal vidinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal vidinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayangal maraindhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayangal maraindhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum thorangal maarum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum thorangal maarum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvin barangal kuraiyum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvin barangal kuraiyum ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneer athaipola vairam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneer athaipola vairam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriyai nesithaal tholvi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriyai nesithaal tholvi illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalveesi erindhaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalveesi erindhaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal adaiyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal adaiyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi odumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi odumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannuku theriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannuku theriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Verkuda maramthanae vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verkuda maramthanae vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaipesum nilavugalNaangal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaipesum nilavugalNaangal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai thanthai emakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai thanthai emakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhirum iragukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirum iragukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum pozhdhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum pozhdhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siluvai siragaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluvai siragaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaithooki erinthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaithooki erinthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Em thaaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em thaaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannithu maru vazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannithu maru vazhvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerai kaanikai seigindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerai kaanikai seigindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munnae theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un munnae theva"/>
</div>
</pre>
