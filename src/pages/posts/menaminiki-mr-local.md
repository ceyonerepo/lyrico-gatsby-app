---
title: "menaminiki song lyrics"
album: "Mr. Local"
artist: "Hiphop Tamizha"
lyricist: "Rokesh"
director: "M. Rajesh"
path: "/albums/mr-local-lyrics"
song: "Menaminiki"
image: ../../images/albumart/mr-local.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ThHtfamsjME"
type: "love"
singers:
  - Benny Dayal
  - Snigdha Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edakoodam Ekkachekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakoodam Ekkachekkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyila Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyila Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaathukkum Oru Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum Oru Naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkuthada Endu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkuthada Endu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattaya Naan Aagitten Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattaya Naan Aagitten Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Azhaga Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhaga Kandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavume Tuckeru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavume Tuckeru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudaiya Trendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Trendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So Nillummaa Nillummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Nillummaa Nillummaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirmala Nirmala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirmala Nirmala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkinna Unakkinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkinna Unakkinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumalaa Irumalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumalaa Irumalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesendi Pesendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesendi Pesendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Normala Normala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Normala Normala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaandaara Gaandaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaandaara Gaandaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamula Maamula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamula Maamula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalu Bunnu Kannam Sevakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalu Bunnu Kannam Sevakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegamaa Kaattura Nee Veruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamaa Kaattura Nee Veruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aambala Paiyan Amaithiya Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambala Paiyan Amaithiya Irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu Yen Kaluttura Seruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Yen Kaluttura Seruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Naan Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Naan Sollala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Cleopaatara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Cleopaatara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunthaalum Irukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaalum Irukkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Moonji Better ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Moonji Better ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiya Sonnathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya Sonnathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thalaiyila Kottura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thalaiyila Kottura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyaatha Bashaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaatha Bashaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kanna Pinnanu Thittura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kanna Pinnanu Thittura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Currentukku Famous Kalpaakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Currentukku Famous Kalpaakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalpaakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpaakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana Kadhalil Vizhuntha Keezhpaakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Kadhalil Vizhuntha Keezhpaakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillummaa Nillummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillummaa Nillummaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirmala Nirmala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirmala Nirmala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkinna Unakkinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkinna Unakkinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumalaa Irumalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumalaa Irumalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesendi Pesendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesendi Pesendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Normala Normala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Normala Normala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirmala Nirmala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirmala Nirmala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makku Maramanda Madaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makku Maramanda Madaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Kuduppen Badhil Adiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Kuduppen Badhil Adiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Vaanga Vanthudu Readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Vaanga Vanthudu Readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poththinu Gammunu Poda Podiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poththinu Gammunu Poda Podiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandamaa Valarndha Thadiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandamaa Valarndha Thadiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Dhilluruntha Enna Thoduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhilluruntha Enna Thoduyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuduthiduva Unakku Keduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduthiduva Unakku Keduyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kottaiyil Parakkum En Kodi Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kottaiyil Parakkum En Kodi Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaala Surutikka Vena Enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Surutikka Vena Enkitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechchikkaatha Seththu Pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechchikkaatha Seththu Pova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Edam Koduththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Edam Koduththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Adam Pudikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Adam Pudikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Adikkara Adiyila Azhunjuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Adikkara Adiyila Azhunjuduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madam Thaanga Menaminikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madam Thaanga Menaminikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhula Paaru Goldu Jimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Paaru Goldu Jimikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naisa Antha Nagaiya Amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naisa Antha Nagaiya Amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuduvom Azhaga Dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuduvom Azhaga Dimikki"/>
</div>
</pre>
