---
title: "basthi pogaru song lyrics"
album: "Ichata Vahanamulu Niluparadu"
artist: "Praveen Lakkaraju"
lyricist: "RollRida"
director: "S Darshan"
path: "/albums/ichata-vahanamulu-niluparadu-lyrics"
song: "Basthi Pogaru"
image: ../../images/albumart/ichata-vahanamulu-niluparadu.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mVq9WT-Z83s"
type: "mass"
singers:
  - RollRida
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Galeez Lekkalesthe Gully Lona Moguthaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galeez Lekkalesthe Gully Lona Moguthaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Gichhukunnaa Gaani Chesthaamu Nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Gichhukunnaa Gaani Chesthaamu Nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhi Veshaalu Vesthe Puchhe Needhi Pagulthaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhi Veshaalu Vesthe Puchhe Needhi Pagulthaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachhigaane Chepthunna Pettukoku Pangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachhigaane Chepthunna Pettukoku Pangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badsmash Enter Gaani Baadhalu Padakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badsmash Enter Gaani Baadhalu Padakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Beshuggaa Untaamu Memu Endhuku Baabu Benga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beshuggaa Untaamu Memu Endhuku Baabu Benga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rechhipo Kaaka Dhookipo Kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rechhipo Kaaka Dhookipo Kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthaam Makathika Choosuko Nijamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthaam Makathika Choosuko Nijamgaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaade Naa Dosthu Kadaa… Veede Naa Dosthu Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaade Naa Dosthu Kadaa… Veede Naa Dosthu Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maake Memu Boss Kada… Kada KadaKada Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maake Memu Boss Kada… Kada KadaKada Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona Josh Kada Milenge Adda Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona Josh Kada Milenge Adda Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettisthaam Edikaada Dhada DhadaDhada Dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettisthaam Edikaada Dhada DhadaDhada Dhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gully Sootthe Maadhi Saana Sinnaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gully Sootthe Maadhi Saana Sinnaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Illu Kaastha Irakatangaa Untaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Kaastha Irakatangaa Untaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Dillu Masthu Saafuguntaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Dillu Masthu Saafuguntaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamotthe Kalisikattuguntaamuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamotthe Kalisikattuguntaamuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gully Sootthe Maadhi Saana Sinnaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gully Sootthe Maadhi Saana Sinnaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Illu Kaastha Irakatangaa Untaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Kaastha Irakatangaa Untaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Dillu Masthu Saafuguntaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Dillu Masthu Saafuguntaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamotthe Kalisikattuguntaamuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamotthe Kalisikattuguntaamuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruku Irukugaa Maatho Padithe Madathegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Irukugaa Maatho Padithe Madathegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraa Milke Julke Utke Jhutke Duniya Badkegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraa Milke Julke Utke Jhutke Duniya Badkegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Untaam Kalisi Melisigaa Ladkiya Maapai Fidagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untaam Kalisi Melisigaa Ladkiya Maapai Fidagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basthi Poragaallu Untaar Chooda Hattakattagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthi Poragaallu Untaar Chooda Hattakattagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini Erugani Premani Choopenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini Erugani Premani Choopenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayapadi Thadabadi Choopulu Dhaachenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayapadi Thadabadi Choopulu Dhaachenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudibudi Aatalu Kalbali Chesenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudibudi Aatalu Kalbali Chesenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakunna Veluguni Palikedhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakunna Veluguni Palikedhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dosthuga Unte Daggara Theesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthuga Unte Daggara Theesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaarkaar Chesthe Baireelu Vesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarkaar Chesthe Baireelu Vesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtaal Vasthe Kalisi Untaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtaal Vasthe Kalisi Untaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayyamkosthe Kotti Vadesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayyamkosthe Kotti Vadesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Attigitti Golapetti Madatha Pettaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attigitti Golapetti Madatha Pettaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittipotti Guttakesi Lollipettaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittipotti Guttakesi Lollipettaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttoorantha Chettikatha Attaage Ittaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttoorantha Chettikatha Attaage Ittaage"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevulu Pagiletattu Gallaa Etthi Nuvvu Kottaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevulu Pagiletattu Gallaa Etthi Nuvvu Kottaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gully Sootthe Maadhi Saana Sinnaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gully Sootthe Maadhi Saana Sinnaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Illu Kaastha Irakatangaa Untaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Kaastha Irakatangaa Untaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Dillu Masthu Saafuguntaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Dillu Masthu Saafuguntaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamotthe Kalisikattuguntaamuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamotthe Kalisikattuguntaamuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Gully Sootthe Maadhi Saana Sinnaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Gully Sootthe Maadhi Saana Sinnaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Illu Kaastha Irakatangaa Untaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illu Kaastha Irakatangaa Untaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Dillu Masthu Saafuguntaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Dillu Masthu Saafuguntaadhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamotthe Kalisikattuguntaamuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamotthe Kalisikattuguntaamuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basthikeli Vachhinollu Esukondi Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthikeli Vachhinollu Esukondi Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthu Chesukunta Pothe Isthaadhi Haai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthu Chesukunta Pothe Isthaadhi Haai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Pori Jolikosthe Neeku Pagulthaadhi Bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Pori Jolikosthe Neeku Pagulthaadhi Bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkagunte Cheptha Neeku One Glass Chai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkagunte Cheptha Neeku One Glass Chai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gully Chivara Adda Kaada Chesthaamu Haai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully Chivara Adda Kaada Chesthaamu Haai"/>
</div>
<div class="lyrico-lyrics-wrapper">Goda Meeda Chillukotti Aithaamu High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goda Meeda Chillukotti Aithaamu High"/>
</div>
<div class="lyrico-lyrics-wrapper">Golagola Pettukuntu Untaamu Bhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golagola Pettukuntu Untaamu Bhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkadunna Kali Maut Daalo Bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkadunna Kali Maut Daalo Bhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meeda Button Vipparaa Sleeves Madatha Pettaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeda Button Vipparaa Sleeves Madatha Pettaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaathi Konchem Lepi Jara Pogaru Sooparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaathi Konchem Lepi Jara Pogaru Sooparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogaraa Thoogaraa Thaagaraa Vaagaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogaraa Thoogaraa Thaagaraa Vaagaraa"/>
</div>
</pre>
