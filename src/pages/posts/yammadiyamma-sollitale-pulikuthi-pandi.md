---
title: "yammadiyamma sollitale song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Mohan Raj"
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Yammadiyamma Sollitale"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AXZPMZxiSvs"
type: "Love"
singers:
  - Jagadeesh Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yammadiyamma Sollittale Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yammadiyamma Sollittale Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Manasa Pola Kattittale Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Manasa Pola Kattittale Yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadiyamma Adi Ponen Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadiyamma Adi Ponen Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Moochil Kathil Koduthu Vitten Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Moochil Kathil Koduthu Vitten Umma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha alamaram Sachiduche Vazhamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha alamaram Sachiduche Vazhamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkama Kedachiduche Ullukulla Nenacha Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkama Kedachiduche Ullukulla Nenacha Varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala Nooru Tharam Sirikkiren Nanum Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala Nooru Tharam Sirikkiren Nanum Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama Nuzhanjiduche Enakkum Antha Kadhal Jhoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama Nuzhanjiduche Enakkum Antha Kadhal Jhoram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Koonthalula Thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Koonthalula Thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Poova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poova Mara Vehnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kollum Nenappu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kollum Nenappu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Noova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noova Mara Vehnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaluthu Poo Vasam Manasula Thee Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaluthu Poo Vasam Manasula Thee Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Mazhai Nenappala Kodukkura Jelathosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Mazhai Nenappala Kodukkura Jelathosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikadi Kan Pesum Athu Than Sandhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Kan Pesum Athu Than Sandhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye Anbala Yezhuthala Eedhigasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Anbala Yezhuthala Eedhigasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Nan Kettu Pakkam Pogayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Nan Kettu Pakkam Pogayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puruvam Aruvala Mari Tholaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puruvam Aruvala Mari Tholaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vehnam Vambunu Dhooram Pogayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vehnam Vambunu Dhooram Pogayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Udathu Halwava Ennai Azhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udathu Halwava Ennai Azhaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Koonthalula Thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Koonthalula Thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Poova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poova Mara Vehnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kollum Nenappu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kollum Nenappu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Noova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noova Mara Vehnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verappu Kuraiyama Iruthen Pala Nala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verappu Kuraiyama Iruthen Pala Nala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Nan Parthu Vizhunthen Thalaikila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nan Parthu Vizhunthen Thalaikila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku Poranthenu Nenaippen Sila Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Poranthenu Nenaippen Sila Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Partha Enakku Adhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Partha Enakku Adhu Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangala Suththa Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangala Suththa Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucham Thalaikkul Yeri Nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucham Thalaikkul Yeri Nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thaniya Pesa Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thaniya Pesa Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kanna Thoranthe Thoonga Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kanna Thoranthe Thoonga Vekkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Koonthalula Thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Koonthalula Thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Poova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poova Mara Vehnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kollum Nenappu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kollum Nenappu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Noova Mara Vehnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noova Mara Vehnum"/>
</div>
</pre>
