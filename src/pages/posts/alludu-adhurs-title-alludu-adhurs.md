---
title: "alludu adhurs title song lyrics"
album: "Alludu Adhurs"
artist: "Devi Sri Prasad"
lyricist: "Ramajogayya Sastry"
director: "Santosh Srinivas"
path: "/albums/alludu-adhurs-lyrics"
song: "Alludu Adhurs Title Song"
image: ../../images/albumart/alludu-adhurs.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eDgJFW8P1gY"
type: "title song"
singers:
  - Jaspreet Jasz
  - Vaishnavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Crazy baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Letme show yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letme show yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate cake meedha cheery la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate cake meedha cheery la"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentha mudhugunnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentha mudhugunnaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu ribbon kattukunna rocket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu ribbon kattukunna rocket la"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave raave raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave raave raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot hot chicken curry la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot hot chicken curry la"/>
</div>
<div class="lyrico-lyrics-wrapper">Noroorinchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noroorinchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gunde meedha golden locket la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gunde meedha golden locket la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve nuvve nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve nuvve nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nadum meedha tattoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadum meedha tattoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na favourite spot-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na favourite spot-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaa endhukinka late-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaa endhukinka late-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppuley dhaantho paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steppuley dhaantho paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyababoi nee naa jodi super hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyababoi nee naa jodi super hittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crazy baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me show yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me show yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu gallalungi kattukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gallalungi kattukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu look-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu look-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla kallajodu pettukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla kallajodu pettukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Classu look-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classu look-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Peta sherwani vesukunna rajulaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peta sherwani vesukunna rajulaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli gurramekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli gurramekku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu kanchipattu kattukunna juliet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kanchipattu kattukunna juliet"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku chandamama kindhikochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku chandamama kindhikochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishti pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishti pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">So life long ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So life long ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gundelona dhachipettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gundelona dhachipettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Excellent pilladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Excellent pilladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaada dhorikinaadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaada dhorikinaadani"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma lekkalandharu pachi muchata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma lekkalandharu pachi muchata"/>
</div>
<div class="lyrico-lyrics-wrapper">Wavelength kudirina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wavelength kudirina"/>
</div>
<div class="lyrico-lyrics-wrapper">Wall postaer veyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wall postaer veyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamanthataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamanthataa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kantikunna kaatukalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kantikunna kaatukalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamantha thodai untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamantha thodai untaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melikalu thirigina nee muscles-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melikalu thirigina nee muscles-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthu masthugunna nee manliness-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthu masthugunna nee manliness-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dream land theater lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dream land theater lo"/>
</div>
<div class="lyrico-lyrics-wrapper">House fulls-u nee anni shows-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="House fulls-u nee anni shows-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chakkanaina vaasthu unna pilla missu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkanaina vaasthu unna pilla missu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rangu pusthakam nee sogassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rangu pusthakam nee sogassu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake dhakkene lucky chance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake dhakkene lucky chance"/>
</div>
<div class="lyrico-lyrics-wrapper">Many thanks-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Many thanks-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee six pack vompulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee six pack vompulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Silk parupulesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silk parupulesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Romantic paatale paadukuntale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romantic paatale paadukuntale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tiktok chempalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tiktok chempalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulanni theesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulanni theesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojukokka diwali jarupukuntale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojukokka diwali jarupukuntale"/>
</div>
<div class="lyrico-lyrics-wrapper">Gram kuda vadhalakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gram kuda vadhalakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Glamour antha dochesthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glamour antha dochesthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilladu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilladu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gilludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gilludu adhurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema jalludu ahdurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema jalludu ahdurs"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa nannaki alludu adhurs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa nannaki alludu adhurs"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crazy baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me show yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me show yeah"/>
</div>
</pre>
