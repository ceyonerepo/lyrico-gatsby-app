---
title: "oru viral puratchi song lyrics"
album: "Sarkar"
artist: "AR Rahman"
lyricist: "Vivek"
director: "AR Murugadoss"
path: "/albums/sarkar-lyrics"
song: "Oru Viral Puratchi"
image: ../../images/albumart/sarkar.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/989LcoUNg1o"
type: "mass"
singers:
  - AR Rahman
  - Srinidhi Venkatesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nethu varaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaali yemaali yemaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaali yemaali yemaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethu varaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu varaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaali yemaali yemaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaali yemaali yemaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali poraali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali poraali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraali poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraali poraali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru viral puratchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru viral puratchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudhaa unarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudhaa unarchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru viral puratchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru viral puratchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudhaa unarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudhaa unarchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoooo oooooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoooo oooooooooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoooo oooooooooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoooo oooooooooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhmaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhmaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olikkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Olipathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olipathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarchiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarchiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruppi adikka irukku neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi adikka irukku neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralin nuniyil vilattum karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralin nuniyil vilattum karuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un murai aiyoo nee thoonginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un murai aiyoo nee thoonginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasai pettru pin yenginaaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasai pettru pin yenginaaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanam vittru yedhai vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam vittru yedhai vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo ooooo hooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo ooooo hooo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru viral puratchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru viral puratchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudhaa unarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudhaa unarchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam ondraai kelvigal kettalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ondraai kelvigal kettalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakkum kai angu nadungaathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakkum kai angu nadungaathoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhiya manithan ezhuthum vithiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhiya manithan ezhuthum vithiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya ulagam thodangaathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya ulagam thodangaathoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooo ooooo hooo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo ooooo hooo ooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai vettigal angangu silai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai vettigal angangu silai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal vervaiyum raththamum vilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal vervaiyum raththamum vilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum vedhanaiyae ingae nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum vedhanaiyae ingae nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu maatra paravaiyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu maatra paravaiyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethiyai kolgiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethiyai kolgiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamaai pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaai pogirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaigal desathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaigal desathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaiyum moodinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaiyum moodinom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkalin aatchiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkalin aatchiyaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru naam vaalgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru naam vaalgirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Porgalai thaandi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porgalai thaandi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottraiyae kaangirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottraiyae kaangirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhorgangal thaakiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorgangal thaakiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhiyil saagirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiyil saagirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluthidum kangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluthidum kangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyana vaazhgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyana vaazhgirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru viral puratchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru viral puratchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudhaa unarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudhaa unarchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhmaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhmaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olikkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Olipathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olipathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarchiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarchiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooooo hooo ooooo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo hooo ooooo hooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooo hooo ooooo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo hooo ooooo hooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanam vittru yedhai vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam vittru yedhai vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalathai soorai aadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalathai soorai aadinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanam vittru yedhai vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam vittru yedhai vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalathai soorai aadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalathai soorai aadinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanam vittru yedhai vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam vittru yedhai vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalathai soorai aadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalathai soorai aadinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanam vittru yedhai vaanginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam vittru yedhai vaanginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalathai soorai aadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalathai soorai aadinaai"/>
</div>
</pre>
