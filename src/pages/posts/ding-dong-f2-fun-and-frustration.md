---
title: "ding dong song lyrics"
album: "F2 Fun and Frustration"
artist: "Devi Sri Prasad"
lyricist: "Kasarla Shyam"
director: "Anil Ravipudi"
path: "/albums/f2-fun-and-frustration-lyrics"
song: "Ding Dong"
image: ../../images/albumart/f2-fun-and-frustration.jpg
date: 2019-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jDefi-ImWsw"
type: "happy"
singers:
  - Rahul Sipligunj
  - Malathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Centre lo left leg petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Centre lo left leg petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enter ga brother-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enter ga brother-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey silku lungi ethi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey silku lungi ethi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamki angi bothaletti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamki angi bothaletti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethiki bondu mallelu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethiki bondu mallelu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminaru athar kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminaru athar kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vochesnamu ro memu ochesnamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vochesnamu ro memu ochesnamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey silku lungi ethi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey silku lungi ethi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamki angi bothaletti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamki angi bothaletti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethiki bondu mallelu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethiki bondu mallelu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Charminaru athar kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charminaru athar kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vochesnamu ro memu ochesnamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vochesnamu ro memu ochesnamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey soda buddi kissuna nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey soda buddi kissuna nokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara buddi sankana kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara buddi sankana kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandi maisamma thalliki mokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandi maisamma thalliki mokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bora banda gaadi ekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bora banda gaadi ekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vochesnamu ro memu ochesnamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vochesnamu ro memu ochesnamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey soda buddi kissuna nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey soda buddi kissuna nokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara buddi sankana kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara buddi sankana kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandi maisamma thalliki mokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandi maisamma thalliki mokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bora banda gaadi ekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bora banda gaadi ekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vochesnamu ro memu ochesnamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vochesnamu ro memu ochesnamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yey iddaram itta dappulu vatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yey iddaram itta dappulu vatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaragotte paate katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaragotte paate katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurrumanna burru pittala pattestamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurrumanna burru pittala pattestamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding-u dong-u ding-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding-u dong-u ding-u "/>
</div>
<div class="lyrico-lyrics-wrapper">dong-u ding-u dong-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dong-u ding-u dong-u ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi silakala kosam gorinkala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi silakala kosam gorinkala "/>
</div>
<div class="lyrico-lyrics-wrapper">item song-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="item song-u ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding-u dong-u ding-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding-u dong-u ding-u "/>
</div>
<div class="lyrico-lyrics-wrapper">dong-u ding-u dong-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dong-u ding-u dong-u ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi silakala kosam gorinkala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi silakala kosam gorinkala "/>
</div>
<div class="lyrico-lyrics-wrapper">item song-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="item song-u ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bugga sukka neney petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sukka neney petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gallu galluna gajjalu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gallu galluna gajjalu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadapa dati vaddam ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadapa dati vaddam ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganpedu asalu daddulu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpedu asalu daddulu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesindendi ro nuv jesindi endiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesindendi ro nuv jesindi endiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bugga sukka neney petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sukka neney petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gallu galluna gajjalu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gallu galluna gajjalu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadapa dati vaddam ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadapa dati vaddam ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganpedu asalu daddulu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganpedu asalu daddulu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesindendi ro nuv jesindi endiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesindendi ro nuv jesindi endiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundamma kathanu gundraga tippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundamma kathanu gundraga tippi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakamma kathanu andanga cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakamma kathanu andanga cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka selliki link lu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka selliki link lu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Agreement nu addam petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agreement nu addam petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam chestiviro, nuv aagam chestiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam chestiviro, nuv aagam chestiviro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundamma kathanu gundraga tippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundamma kathanu gundraga tippi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakamma kathanu andanga cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakamma kathanu andanga cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka selliki link lu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka selliki link lu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Agreement nu addam petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agreement nu addam petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam chestiviro, nuv aagam chestiviro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam chestiviro, nuv aagam chestiviro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lagaam madyla link e petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagaam madyla link e petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggam pattina ninne simpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggam pattina ninne simpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokke chimpi Dole kottaga ochesnamu ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokke chimpi Dole kottaga ochesnamu ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding-u dong-u ding-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding-u dong-u ding-u "/>
</div>
<div class="lyrico-lyrics-wrapper">dong-u ding-u dong-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dong-u ding-u dong-u ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi silakala kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi silakala kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">gorinkala item song-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gorinkala item song-u ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding-u dong-u ding-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding-u dong-u ding-u "/>
</div>
<div class="lyrico-lyrics-wrapper">dong-u ding-u dong-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dong-u ding-u dong-u ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi silakala kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi silakala kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">gorinkala item song-u ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gorinkala item song-u ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are pachani intla chiche vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are pachani intla chiche vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu magala rechagottti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu magala rechagottti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na inti deepam ne intla vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na inti deepam ne intla vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na seat kind mante vedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na seat kind mante vedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadipotavu ro nuv madipotavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadipotavu ro nuv madipotavuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are pachani intla chiche vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are pachani intla chiche vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu magala rechagottti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu magala rechagottti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na inti deepam ne intla vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na inti deepam ne intla vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Na seat kind mante vedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na seat kind mante vedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadipotavu ro nuv madipotavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadipotavu ro nuv madipotavuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kaalla oosaravelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaalla oosaravelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira kaaka nethoni lolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira kaaka nethoni lolli"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosindi sudu Naakayi lilly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosindi sudu Naakayi lilly"/>
</div>
<div class="lyrico-lyrics-wrapper">Etla daaniki chestav pelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etla daaniki chestav pelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Bokkala estanro bokkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bokkala estanro bokkal "/>
</div>
<div class="lyrico-lyrics-wrapper">chura chestanro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chura chestanro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kaalla oosaravelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaalla oosaravelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Endira kaaka nethoni lolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endira kaaka nethoni lolli"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosindi sudu Naakayi lilly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosindi sudu Naakayi lilly"/>
</div>
<div class="lyrico-lyrics-wrapper">Etla daaniki chestav pelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etla daaniki chestav pelli"/>
</div>
<div class="lyrico-lyrics-wrapper">Bokkala estanro bokkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bokkala estanro bokkal "/>
</div>
<div class="lyrico-lyrics-wrapper">chura chestanro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chura chestanro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iga chepaku malla inko story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iga chepaku malla inko story"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukochinam sudu pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukochinam sudu pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Thali kattu nuvvu ee sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thali kattu nuvvu ee sari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakaralu apeyyi ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakaralu apeyyi ro"/>
</div>
</pre>
