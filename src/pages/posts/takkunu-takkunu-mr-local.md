---
title: "takkunu takkunu song lyrics"
album: "Mr. Local"
artist: "Hiphop Tamizha"
lyricist: "Mirchi Vijay"
director: "M. Rajesh"
path: "/albums/mr-local-lyrics"
song: "Takkunu Takkunu"
image: ../../images/albumart/mr-local.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/meITXEnkPaA"
type: "love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ava Nera Paakaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Nera Paakaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Gera Aaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Gera Aaguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Gera Sirikkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Gera Sirikkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Noora Norunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Noora Norunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Week Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Week Aaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Irukkuthu Paarvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Irukkuthu Paarvaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittutu Pona Thappey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutu Pona Thappey illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyaala Enna Thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaala Enna Thaakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyaala Enna Thaakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaala Enna Thaakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovi Koovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovi Koovi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vikkura Car ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkura Car ru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roadu Mela Poguthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadu Mela Poguthu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athil Senthu Naama Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Senthu Naama Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sema Joru Aamaan Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Joru Aamaan Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Area la Kettu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area la Kettu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Minja Aalu Yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Minja Aalu Yaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee okay Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee okay Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life fe Thaar Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life fe Thaar Maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollen Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollen Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">White ta Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="White ta Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bright ta Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bright ta Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum Paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Paalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athukku Expirey Date tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku Expirey Date tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just Orey Oru Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Orey Oru Naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dark ah Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dark ah Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brown ah Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brown ah Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum Thenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Thenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Athu pol Naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Athu pol Naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keda Maatten Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keda Maatten Kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Irukkuthu Paarvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Irukkuthu Paarvaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittutu Pona Thappey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutu Pona Thappey illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Siruppu Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Siruppu Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Azhagula Mincha Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Azhagula Mincha Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naduvula Antha Ego Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula Antha Ego Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkula Venaam di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkula Venaam di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musically laam Bore ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musically laam Bore ru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Compose Panren Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Compose Panren Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Team Oda Tune Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Team Oda Tune Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paada Poren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Poren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayen Onna Senthu Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayen Onna Senthu Povom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dating Meeting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dating Meeting"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya Konjam Konaya Vechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Konjam Konaya Vechchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selfie Edukatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Edukatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaren En Kannula Kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaren En Kannula Kadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayen Oru Muththam Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayen Oru Muththam Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay thaan Neeyum Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay thaan Neeyum Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Husband Aagatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Husband Aagatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkunu Takkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Takkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakkunu Bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Bakkunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Kick Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Kick Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Onnu Irukkuthu Paarvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Onnu Irukkuthu Paarvaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Onnu Look Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Onnu Look Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittutu Pona Thappey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutu Pona Thappey illa"/>
</div>
</pre>
