---
title: "vaa vasuki song lyrics"
album: "Seeru"
artist: "D. Imman"
lyricist: "Viveka"
director: "Rathnasiva"
path: "/albums/seeru-lyrics"
song: "Vaa Vasuki"
image: ../../images/albumart/seeru.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/C2cIXGOpCiQ"
type: "Love"
singers:
  - Shivam Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Poovizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poovizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Araaroo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araaroo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhai Aagayam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhai Aagayam Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver Endrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Endrume"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mannodu Thaan Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mannodu Thaan Ulladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oorengum Sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oorengum Sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Ellaam Adi Unnodu Thaan Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Ellaam Adi Unnodu Thaan Ulladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Theeradha Aaradha Peraasaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Theeradha Aaradha Peraasaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Naan Enna Per Vaipadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Naan Enna Per Vaipadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu Illaamal Pugai Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Illaamal Pugai Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thee Ennai Soozhgindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thee Ennai Soozhgindrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu Thavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu Thavudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu Yengudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Poovizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poovizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Araaroo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araaroo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Or Iru Naal Uraiadalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Iru Naal Uraiadalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ver Oru Thoranai Aagirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ver Oru Thoranai Aagirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhudhum Muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhudhum Muzhudhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeranai Sooranai Pol Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranai Sooranai Pol Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhum Manadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhum Manadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Veedulla Veedhiyil Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veedulla Veedhiyil Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udharum Udharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udharum Udharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paaramal Ver Edhum Pani Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paaramal Ver Edhum Pani Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal Neraga Paarkindra Thunivillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Neraga Paarkindra Thunivillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Indri En Naatkal Ini Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Indri En Naatkal Ini Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Nee Endrum Naan Endrum Thani Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Nee Endrum Naan Endrum Thani Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Vaasam Nugarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vaasam Nugarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nodi Pozhudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nodi Pozhudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulle Yedho Ondru Nadakkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulle Yedho Ondru Nadakkiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Poovizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poovizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Araaroo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araaroo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhai Aagayam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhai Aagayam Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver Endrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Endrume"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mannodu Thaan Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mannodu Thaan Ulladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oorengum Sendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oorengum Sendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Ellaam Adi Unnodu Thaan Ulladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Ellaam Adi Unnodu Thaan Ulladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Theeradha Aaradha Peraasaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Theeradha Aaradha Peraasaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Naan Enna Per Vaipadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Naan Enna Per Vaipadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu Illaamal Pugai Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Illaamal Pugai Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thee Ennai Soozhgindrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thee Ennai Soozhgindrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu Thavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu Thavudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu Yengudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi Thaan Thavudhu Thavudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi Thaan Thavudhu Thavudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaan Yengudhu Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaan Yengudhu Yengudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa"/>
</div>
</pre>
