---
title: "sollividavaa song lyrics"
album: "Sollividava"
artist: "Jassie Gift"
lyricist: "Madhan Karky"
director: "Arjun Sarja"
path: "/albums/sollividava-lyrics"
song: "Sollividavaa"
image: ../../images/albumart/sollividava.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sjg7j7YHOz8"
type: "love"
singers:
  - Sathya Prakash
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sollividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividava aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividava aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thekiya bhashaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thekiya bhashaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralilae addakiya aasaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralilae addakiya aasaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae pootiya osaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae pootiya osaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai solla vidu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai solla vidu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi adaipadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi adaipadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi thadupadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi thadupadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kirukan aakadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kirukan aakadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai maraikka koodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai maraikka koodadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhan yena vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan yena vedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil adhu baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil adhu baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennulae vaanam – ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulae vaanam – ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondridum ondrinai sollidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondridum ondrinai sollidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallai en thotram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai en thotram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullae neer ottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullae neer ottam"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullae aazhiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullae aazhiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongidum ondrinai sollidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongidum ondrinai sollidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mouna saalai kadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna saalai kadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam ittu yaavum sollividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam ittu yaavum sollividava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai solla vidu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai solla vidu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi adaipadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi adaipadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi thadupadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi thadupadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kirukan aakadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kirukan aakadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai maraikka koodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai maraikka koodadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaagam avan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagam avan kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Megham naan vinnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham naan vinnil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sorkkal pozhigayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sorkkal pozhigayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaviyendraigiyae maraivadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviyendraigiyae maraivadhu yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyai aval munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai aval munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovo en pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovo en pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai neetti tharugayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai neetti tharugayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrinil poo adhu karaivadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrinil poo adhu karaivadhu yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru sollividavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru sollividavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenmam sila thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam sila thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai solla vidu nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai solla vidu nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi adaipadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi adaipadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi thadupadhu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi thadupadhu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kiruki aakadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kiruki aakadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai maraikka koodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai maraikka koodadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollividavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividava"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai muzhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai muzhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyilum porkkala bhoomiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyilum porkkala bhoomiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhumae porkkalam aanadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhumae porkkalam aanadhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthaigal ennai kolludhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthaigal ennai kolludhuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollividavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividavaaa"/>
</div>
</pre>
