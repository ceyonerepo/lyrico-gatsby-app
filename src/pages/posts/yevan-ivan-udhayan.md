---
title: "yevan ivan song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Yevan Ivan"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2esZHav1-kw"
type: "love"
singers:
  - Shruti Hassan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evan ivan ivan ragasiyan kaadhalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan ivan ivan ragasiyan kaadhalan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan azhagiya raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan azhagiya raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan sirikkira paadhagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan sirikkira paadhagan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan kulirgira sooriyaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan kulirgira sooriyaney"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam koabakaaran kollum paarvaiyaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam koabakaaran kollum paarvaiyaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai meyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai meyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam moottiye veppam pookkakkoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam moottiye veppam pookkakkoodum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai theendaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai theendaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">kallam seiyavenum poiyum pesidavenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallam seiyavenum poiyum pesidavenum"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum maraikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum maraikkaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai serumboadhu penmai theergiraboadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai serumboadhu penmai theergiraboadu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">siru punnagai poothida vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru punnagai poothida vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">poi oru paarvai poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi oru paarvai poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">adadaa thirudaa azhagaa tharudaa nizhalaai vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adadaa thirudaa azhagaa tharudaa nizhalaai vaaraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan ivan ivan ragasiyan kaadhalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan ivan ivan ragasiyan kaadhalan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan azhagiya raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan azhagiya raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan sirikkira paadhagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan sirikkira paadhagan"/>
</div>
<div class="lyrico-lyrics-wrapper">evan ivan ivan kulirgira sooriyaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evan ivan ivan kulirgira sooriyaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu iravin micha kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu iravin micha kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalin moththa nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalin moththa nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavo nijamo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavo nijamo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paniyil pudhaindha analaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paniyil pudhaindha analaa"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilai kalandha kuliraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilai kalandha kuliraa"/>
</div>
<div class="lyrico-lyrics-wrapper">analo kuliro theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="analo kuliro theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">oru neechal undhan panikkaatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru neechal undhan panikkaatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">en mounam mella kalaitheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mounam mella kalaitheney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee virumbikkettu thirumbi paarthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee virumbikkettu thirumbi paarthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyil artham izhandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyil artham izhandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analaa nizhalaa ena meyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaa nizhalaa ena meyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaa karudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaa karudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enai theendaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai theendaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">tharunam kurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharunam kurudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum maraikaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum maraikaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiruum piriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiruum piriyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu ulagin moththa sugamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ulagin moththa sugamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin uchcha ranamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin uchcha ranamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamo ranamo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamo ranamo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee uruga thariththa kuralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee uruga thariththa kuralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kuralil podhindha porulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuralil podhindha porulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kuralo porulo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuralo porulo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir nehi chillum oru nodipoal edhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir nehi chillum oru nodipoal edhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan visaalam kondu kalitheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan visaalam kondu kalitheney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nerungi vandhu irukkiyanaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nerungi vandhu irukkiyanaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam endru ketteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam endru ketteney"/>
</div>
</pre>
