---
title: "vaa vellai raasathi song lyrics"
album: "Kanne Kalaimane"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Seenu Ramasamy"
path: "/albums/kanne-kalaimane-lyrics"
song: "Vaa Vellai Raasathi"
image: ../../images/albumart/kanne-kalaimane.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ovt4OOMVwic"
type: "sad"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Vellai Raasathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vellai Raasathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Irangi Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Irangi Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Pasi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Pasi Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Irangi Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Irangi Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvil Rusi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvil Rusi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam Konda Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam Konda Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaipaadu Maarathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaipaadu Maarathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathin Uyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathin Uyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduma Kuraiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduma Kuraiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam Irangi Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Irangi Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Pasi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Pasi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Kannil Kuttram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Kannil Kuttram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram Parthal Ange Anbillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram Parthal Ange Anbillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkum Yethavum Sirithu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum Yethavum Sirithu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Panithuli Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panithuli Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbin Kadal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbin Kadal Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbil Sirithu Perithu Kidaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Sirithu Perithu Kidaiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Atrin Sagala Thuliyum Samame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atrin Sagala Thuliyum Samame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedham Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvar Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvar Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Per Vendama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Per Vendama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengo Piranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengo Piranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Sernthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Sernthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal Koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal Koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam Aavomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam Aavomma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai Kiliyin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Kiliyin "/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu Naraikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Naraikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil Idhayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Idhayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkum Idhayam Urugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Idhayam Urugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam Irangi Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Irangi Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Pasi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Pasi Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham Irangi Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham Irangi Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvil Rusi Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvil Rusi Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesam Konda Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesam Konda Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaipaadu Maarathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaipaadu Maarathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathin Uyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathin Uyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduma Kuraiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduma Kuraiyuma"/>
</div>
</pre>
