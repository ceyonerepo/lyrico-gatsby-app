---
title: "pennirkor song lyrics"
album: "Miga Miga Avasaram"
artist: "Ishaan Dev"
lyricist: "Cheran"
director: "Suresh Kamatchi"
path: "/albums/miga-miga-avasaram-lyrics"
song: "Pennirkor"
image: ../../images/albumart/miga-miga-avasaram.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/os4xAnGSOzM"
type: "sad"
singers:
  - Ishaan Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pennirkor theemai seithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennirkor theemai seithom"/>
</div>
<div class="lyrico-lyrics-wrapper">aval perinba kavarchiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval perinba kavarchiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">bothai endrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothai endrom"/>
</div>
<div class="lyrico-lyrics-wrapper">ennillaaa kodumai seithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennillaaa kodumai seithom"/>
</div>
<div class="lyrico-lyrics-wrapper">aaninam pasi theerkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaninam pasi theerkum"/>
</div>
<div class="lyrico-lyrics-wrapper">adimaigal aaki vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaigal aaki vaithom"/>
</div>
<div class="lyrico-lyrics-wrapper">aaki vaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaki vaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thayaiyum thai boomiyiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayaiyum thai boomiyiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sithaithavar naam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithaithavar naam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">ini marumurai uyirthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini marumurai uyirthida"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum undo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneer valinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneer valinthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">katchiyum ithuvandro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katchiyum ithuvandro"/>
</div>
<div class="lyrico-lyrics-wrapper">ini marumurai uyirthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini marumurai uyirthida"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval paavam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval paavam andro"/>
</div>
<div class="lyrico-lyrics-wrapper">pen enbaval oru sabam andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen enbaval oru sabam andro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennillaa kodumai seithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennillaa kodumai seithom"/>
</div>
</pre>
