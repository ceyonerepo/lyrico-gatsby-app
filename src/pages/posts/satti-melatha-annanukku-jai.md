---
title: "satti melatha song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Gaana Bala"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Satti Melatha"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JFBYDAgk48I"
type: "happy"
singers:
  - Gaana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Parakka ududa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka ududa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathi kambathula kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi kambathula kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthi poduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram vaala vediya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram vaala vediya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyagi pichai perumal annanoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyagi pichai perumal annanoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasiyal vazhka mudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyal vazhka mudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer anjali poster 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer anjali poster "/>
</div>
<div class="lyrico-lyrics-wrapper">adichi ottungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichi ottungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma oorukellaam theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma oorukellaam theriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satti molatha nee kaaichuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satti molatha nee kaaichuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma otha adiya vaasida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma otha adiya vaasida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu pochu annan moochuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu pochu annan moochuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru full-ah ivaru pechuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru full-ah ivaru pechuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru full-ah ivaru pechuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru full-ah ivaru pechuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada aalamaran sanjichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aalamaran sanjichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma periya thala maanduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma periya thala maanduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba amarkalama vazhnthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba amarkalama vazhnthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo oora vittu pooyiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo oora vittu pooyiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo oora vittu pooyiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo oora vittu pooyiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondriyam thoongiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondriyam thoongiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondiyaa mannukku poyiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondiyaa mannukku poyiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam valarndhiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam valarndhiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku maavattam kochikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku maavattam kochikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhukku rendu kattila poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhukku rendu kattila poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annana kalathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana kalathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma e.ma.ve.ka katchi kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma e.ma.ve.ka katchi kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela porthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela porthuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha mullai nagar oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mullai nagar oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Annana pola yaaruIvar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana pola yaaruIvar "/>
</div>
<div class="lyrico-lyrics-wrapper">azhagu mugatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu mugatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai ketta vanangum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai ketta vanangum ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodiya pudichikunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiya pudichikunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu nikkum kumbala paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu nikkum kumbala paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhai jananga mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhai jananga mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanukku nalla peruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanukku nalla peruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annana pathi innamum soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana pathi innamum soldren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasa podungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasa podungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha body yaarukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha body yaarukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthaminnu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthaminnu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Toss-eh podungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toss-eh podungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga tha.pa..ka katchiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga tha.pa..ka katchiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thaanda singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thaanda singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu hall mark-u thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu hall mark-u thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pannathada bangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pannathada bangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokka langa kuruvi langa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokka langa kuruvi langa"/>
</div>
<div class="lyrico-lyrics-wrapper">Langa soap-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Langa soap-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai sokka pootta picha perumal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai sokka pootta picha perumal"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan sokku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan sokku da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annana thookinu poganum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana thookinu poganum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudukaattuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudukaattuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukulla muppathu roova kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukulla muppathu roova kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothuran munnooru oottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuran munnooru oottukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annana thookinu poganum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana thookinu poganum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudukaattuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudukaattuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukulla muppathu roova kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukulla muppathu roova kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothuran munnooru oottukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuran munnooru oottukku"/>
</div>
</pre>
