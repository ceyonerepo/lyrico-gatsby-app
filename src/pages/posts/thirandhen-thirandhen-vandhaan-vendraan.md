---
title: "thirandhen thirandhen song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Madhan Karky"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Thirandhen Thirandhen"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9ECxSBf87f0"
type: "love"
singers:
  - Aalaap Raju
  - Javed Ali
  - Matsz
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thirandhen thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhen thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae nee vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae nee vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moota thiradhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moota thiradhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhae urangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhae urangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enullae cell ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enullae cell ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppikum un perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppikum un perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketka thiranden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketka thiranden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhai thozhai ena ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhai thozhai ena ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae kettu kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae kettu kondenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En mamadhayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mamadhayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhai nuzhai unnai ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhai nuzhai unnai ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae maatri kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae maatri kondenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En sarithayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sarithayilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulai yethum illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulai yethum illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen koodo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen koodo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaivedhum illatha un kaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaivedhum illatha un kaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaivedhum illatha maanaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaivedhum illatha maanaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayamena ninaithirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayamena ninaithirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Poidhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poidhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirandhen thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhen thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae nee vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae nee vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moota thiradhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moota thiradhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhae urangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhae urangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enullae cell ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enullae cell ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppikum un perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppikum un perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketka thiranden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketka thiranden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugathinai thirudinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathinai thirudinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirai kadhai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai kadhai padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Agathinai varudinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agathinai varudinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai kadai pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai kadai pidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae unnai thuravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae unnai thuravi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaal varai kulambi poyinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaal varai kulambi poyinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuravaram thurakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuravaram thurakiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulai yethum illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulai yethum illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen koodo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen koodo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaivedhum illatha un kaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaivedhum illatha un kaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaivedhum illatha maanaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaivedhum illatha maanaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayamena ninaithirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayamena ninaithirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Poidhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poidhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhen thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhen thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae nee vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae nee vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moota thiradhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moota thiradhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhae urangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhae urangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enullae cell ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enullae cell ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppikum un perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppikum un perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketka thiranden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketka thiranden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayangal haai sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangal haai sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalodu bye sollum muthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalodu bye sollum muthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaaga sooderum muthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaaga sooderum muthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru mozhi varuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru mozhi varuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai veliayi koorungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai veliayi koorungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaigal valanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaigal valanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai varai thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai varai thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marungugal meeriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marungugal meeriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Madai udaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madai udaithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oraayiram iravil serthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oraayiram iravil serthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Or-eer nodi eraval ketkiraaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or-eer nodi eraval ketkiraaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Porumayin sigaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porumayin sigaramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulai yethum illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulai yethum illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen koodo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen koodo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nulaivedhum illatha un kaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nulaivedhum illatha un kaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaivedhum illatha maanaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaivedhum illatha maanaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayamena ninaithirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayamena ninaithirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Poidhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poidhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirandhen thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhen thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti thirandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti thirandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullae nee vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullae nee vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moota thiradhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moota thiradhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhae urangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhae urangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enullae cell ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enullae cell ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppikum un perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppikum un perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketka thiranden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketka thiranden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottu sottaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottu sottaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai ennul iranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai ennul iranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En rekkai rendu thulirkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En rekkai rendu thulirkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitu thittaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitu thittaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal enmel padiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal enmel padiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Settu settaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settu settaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru muthathinai pozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru muthathinai pozhiya"/>
</div>
</pre>
