---
title: "inba visai song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Arun Prabhu Purushothaman"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Inba Visai"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r1I10LjZwM4"
type: "happy"
singers:
  - Pradeep Kumar
  - Lalita Vijayakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">peravaludan anbu uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peravaludan anbu uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kondu meyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kondu meyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">peravaludan anbu uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peravaludan anbu uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kondu meyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kondu meyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angum ingum alaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angum ingum alaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thavalai vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavalai vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">pongum inba kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongum inba kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">noki saalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noki saalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angum ingum alaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angum ingum alaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thavalai vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavalai vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">pongum inba kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongum inba kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">noki saalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noki saalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundum kuli putharinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundum kuli putharinile"/>
</div>
<div class="lyrico-lyrics-wrapper">puthainthu pogum munnare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthainthu pogum munnare"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu uyir aasai rusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu uyir aasai rusi"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi alainthadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi alainthadume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gundum kuli putharinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundum kuli putharinile"/>
</div>
<div class="lyrico-lyrics-wrapper">puthainthu pogum munnare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthainthu pogum munnare"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu uyir aasai rusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu uyir aasai rusi"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi alainthadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi alainthadume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">peravaludan anbu uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peravaludan anbu uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kondu meyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kondu meyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">peravaludan anbu uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peravaludan anbu uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam kondu meyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam kondu meyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inba visai maangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba visai maangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ula poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ula poguthe"/>
</div>
</pre>
