---
title: "veesiya visiri song lyrics"
album: "Aruvam"
artist: "S. Thaman"
lyricist: "Madhan Karky"
director: "Sai Sekhar"
path: "/albums/aruvam-lyrics"
song: "Veesiya Visiri"
image: ../../images/albumart/aruvam.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n0cUf0SR92Y"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veesiya visiri paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya visiri paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai verodu vetti saaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai verodu vetti saaithavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaithavalae saaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaithavalae saaithavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesiya visiri paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya visiri paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai verodu vetti saaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai verodu vetti saaithavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaithavalae saaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaithavalae saaithavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhesiya geetham kettavan polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesiya geetham kettavan polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Polae polae polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polae polae polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai asaiyaamal nikka vaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai asaiyaamal nikka vaithavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesaagi medhakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesaagi medhakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosaagi parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosaagi parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi enakkul naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi enakkul naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakku thannae sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakku thannae sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanaga therikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaga therikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ennai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ennai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum nenaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum nenaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En naadi narambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadi narambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethiyial maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethiyial maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum raththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum raththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ogippuyal seetram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogippuyal seetram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththam orr uththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththam orr uththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kaadhal uththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kaadhal uththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam orr saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam orr saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu idhayathu saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu idhayathu saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththum idhu koththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththum idhu koththum"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhegam moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhegam moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thittamittuVattam poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thittamittuVattam poduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesiya visiri paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya visiri paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai verodu vetti saaithavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai verodu vetti saaithavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya puyal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya puyal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En anukkal ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anukkal ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhaya thuddippum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhaya thuddippum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moondraanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondraanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi nimidam naal aaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi nimidam naal aaguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannin paarvai dhevamirtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin paarvai dhevamirtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal odhum aindham vaedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal odhum aindham vaedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae neeyum pennae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae neeyum pennae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraam boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraam boodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinjae thaan tholaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinjae thaan tholaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaama karaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaama karaiyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thevai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thevai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedi thaedi alaiyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi thaedi alaiyiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En naadi narambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadi narambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethiyial maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethiyial maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum raththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum raththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ogippuyal seetram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogippuyal seetram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththam orr uththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththam orr uththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kaadhal uththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kaadhal uththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam orr saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam orr saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu idhayathu saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu idhayathu saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththum idhu koththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththum idhu koththum"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhegam moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhegam moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thittamittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thittamittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam poduven"/>
</div>
</pre>
