---
title: "simha garjana song lyrics"
album: "Lakshmi's NTR"
artist: "Kalyani Malik"
lyricist: "Sira Sri"
director: "Ram Gopal Varma - Agasthya Manju"
path: "/albums/lakshmi's-ntr-lyrics"
song: "Simha Garjana"
image: ../../images/albumart/lakshmis-ntr.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/p-VmKbp8-AA"
type: "mass"
singers:
  - P. Ravi Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Garjana Garjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garjana Garjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Simha Garjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simha Garjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Hakku Ane Dhikkaramutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hakku Ane Dhikkaramutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Avineethini Gattiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avineethini Gattiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhee Godadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhee Godadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkara Thaskara Mukalapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkara Thaskara Mukalapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Penu Singapu Garjana Cheddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu Singapu Garjana Cheddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Juster Vesina Dhustulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juster Vesina Dhustulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerupekkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerupekkina"/>
</div>
<div class="lyrico-lyrics-wrapper">Drustitho Bayapedadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drustitho Bayapedadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddhenu Yekkina Brastulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddhenu Yekkina Brastulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Ukkiri Bikkiri Cheddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Ukkiri Bikkiri Cheddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Penu Dhobidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu Dhobidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaarula Petthaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarula Petthaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Voddhani Chitthuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Voddhani Chitthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada Godadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Godadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshana Kshana Dheeksha Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshana Kshana Dheeksha Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhakshatha Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhakshatha Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanu Lakshamu Kakshalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanu Lakshamu Kakshalave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Garjana Simha Garjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garjana Simha Garjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Garjana Simha Garjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garjana Simha Garjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dialogue  Na Jeevithanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue  Na Jeevithanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Chedagottadanike Dhapurinchave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedagottadanike Dhapurinchave "/>
</div>
<div class="lyrico-lyrics-wrapper">Munda Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamulu Uramaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamulu Uramaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamulu Urakaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamulu Urakaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppenai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rudhirapu Karamulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rudhirapu Karamulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Maragaga Jwalalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Maragaga Jwalalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogarutho Naramulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogarutho Naramulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Melikelu Thiruguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melikelu Thiruguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Krodhamouvvagaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krodhamouvvagaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalabadi Kalabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalabadi Kalabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaddharillu Vayugdhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaddharillu Vayugdhamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dialogue  Lakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue  Lakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodagottina Muttadi Kattadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodagottina Muttadi Kattadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Palu Neechapu Vyakthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palu Neechapu Vyakthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Padadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Padadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Medalanu Vanchi Nikrustulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medalanu Vanchi Nikrustulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gatha Vaibhava Dhurgamune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gatha Vaibhava Dhurgamune"/>
</div>
<div class="lyrico-lyrics-wrapper">Padadhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padadhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hakku Ane Dhikkaramutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hakku Ane Dhikkaramutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Avineethini Gattiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avineethini Gattiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhee Godadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhee Godadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkara Thaskara Mukalapy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkara Thaskara Mukalapy"/>
</div>
<div class="lyrico-lyrics-wrapper">Penu Singapu Garjana Cheddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu Singapu Garjana Cheddham"/>
</div>
</pre>
