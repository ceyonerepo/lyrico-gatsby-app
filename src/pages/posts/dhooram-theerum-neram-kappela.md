---
title: "Dhooram Theerum Neram song lyrics"
album: "Kappela"
artist: "Sushin Shyam"
lyricist: "Vinayak Sasikumar"
director: "Muhammad Musthafa"
path: "/albums/kappela-lyrics"
song: "Dhooram Theerum Neram"
image: ../../images/albumart/kappela.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/c9CTIqnznxk"
type: "love"
singers:
  - Aavani Malhar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dooram Theerum Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram Theerum Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Doore Mele Neelaakaasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doore Mele Neelaakaasham "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaakkadal Theeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaakkadal Theeram "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalolam Akameyaazham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalolam Akameyaazham "/>
</div>
<div class="lyrico-lyrics-wrapper">Dinamere Maanjille 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinamere Maanjille "/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhinjille 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhinjille "/>
</div>
<div class="lyrico-lyrics-wrapper">Neermizhikal Alakal Pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neermizhikal Alakal Pole "/>
</div>
<div class="lyrico-lyrics-wrapper">Peythille 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peythille "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalin Varavu Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalin Varavu Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira Paadum Nurakal Choodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thira Paadum Nurakal Choodi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuvare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Nimisham Ithupol Thudaraan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nimisham Ithupol Thudaraan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Mathiyen Arike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Mathiyen Arike"/>
</div>
<div class="lyrico-lyrics-wrapper">Arike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooram Theerum Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram Theerum Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Doore Mele Neelaakaasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doore Mele Neelaakaasham "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaakkadal Theeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaakkadal Theeram "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalolam Akameyaazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalolam Akameyaazham"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Randum Theenaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Randum Theenaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvoram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvoram "/>
</div>
<div class="lyrico-lyrics-wrapper">Mey Muzhuki Izhuki Neengum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mey Muzhuki Izhuki Neengum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Kaatham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Kaatham "/>
</div>
<div class="lyrico-lyrics-wrapper">Niramelum Ninavu Thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niramelum Ninavu Thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kothi Theeraa Kadhakal Thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothi Theeraa Kadhakal Thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivide Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivide Naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Nimisham Ithupol Thudaraan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nimisham Ithupol Thudaraan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nee Mathiyen Arike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nee Mathiyen Arike"/>
</div>
</pre>
