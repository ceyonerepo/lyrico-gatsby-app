---
title: "poovukku song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Vairamuthu"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Poovukku"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Bk-4AQIaQpw"
type: "happy"
singers:
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilavasa veyil vandhu vizhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavasa veyil vandhu vizhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai idhamaai thodumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai idhamaai thodumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanam vadikatti anupidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanam vadikatti anupidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai mazhai mazhai thuli vizhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai mazhai mazhai thuli vizhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En marmam thodumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En marmam thodumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai eerathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai eerathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thundu megam thuvattidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thundu megam thuvattidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odai engal thaai paal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odai engal thaai paal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha oorum mannum thaaimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha oorum mannum thaaimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae illai noi nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae illai noi nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha idam nalla idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha idam nalla idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu endhan thalai nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu endhan thalai nagaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakku uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakku uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakkuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakkuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soththu sogam thedugira manusha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu sogam thedugira manusha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha sogamae varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha sogamae varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam kaasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam kaasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pani thuli velai peruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pani thuli velai peruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaiveli pozhapukku thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiveli pozhapukku thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam engi kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam engi kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae akkam pakkam vandhu kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae akkam pakkam vandhu kedakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam poochi pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam poochi pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam padhungi mella pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam padhungi mella pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu parakkumbodhu thorkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu parakkumbodhu thorkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidharai marandhoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidharai marandhoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraivayin varam peralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraivayin varam peralaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovukku thaappa edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku thaappa edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku kadhava irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku kadhava irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakku uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakku uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaam thorandhae kedakkuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaam thorandhae kedakkuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili aaga aasai enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili aaga aasai enakku"/>
</div>
</pre>
