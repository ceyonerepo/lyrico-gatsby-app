---
title: "very very bad song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Yugabharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Very Very Bad"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mbjXh4ljR94"
type: "mass"
singers:
  - Santhosh Narayanan
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku engala adikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku engala adikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey khaaki color-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalutha pudichi nerikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalutha pudichi nerikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seththu pona aalukellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu pona aalukellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiya neeyum thorakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaiya neeyum thorakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththu pona engala yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththu pona engala yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba thirumba othaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba thirumba othaikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Station-la ethukkuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Station-la ethukkuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta panjayathu nadathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta panjayathu nadathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevi vitta dog-u kellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevi vitta dog-u kellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbu thunda porukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu thunda porukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathavi veri accusitkkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathavi veri accusitkkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannuriyae ooliyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuriyae ooliyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala pottu midhikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala pottu midhikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha jananaayagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha jananaayagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh no…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh no…"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gang rape-u senjavanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gang rape-u senjavanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraan poraan car-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraan poraan car-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu murder senjavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu murder senjavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthen shopping mall-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthen shopping mall-ula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollai adicha manthiri maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai adicha manthiri maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaran helipad-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaran helipad-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Porukkikellam badaa size-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porukkikellam badaa size-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Banner intha oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banner intha oorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanae ellaam koondula yetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae ellaam koondula yetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku irukkaa dhairiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku irukkaa dhairiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala pottu midhikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala pottu midhikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha jananaayagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha jananaayagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah yeah yeah yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s too bad ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s too bad ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core…ore….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core…ore…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very…"/>
</div>
<div class="lyrico-lyrics-wrapper">Charge…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charge…"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands up…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands up…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very…"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very…"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re under arrest…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re under arrest…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chain arutha pervazhi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chain arutha pervazhi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie eduthu anuppuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie eduthu anuppuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooli pada thaivanum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooli pada thaivanum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku cake-u ooturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku cake-u ooturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi kola kaaran ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi kola kaaran ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tv petti kodukkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tv petti kodukkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhiya vikkum thirudan ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhiya vikkum thirudan ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">EPCO va kizhikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="EPCO va kizhikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana ellaam koondula yetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana ellaam koondula yetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku irukka dhairiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku irukka dhairiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala pottu midhikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala pottu midhikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha jananaayagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha jananaayagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very…very very
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very…very very"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga vethacha nelaththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga vethacha nelaththula"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai yethanai building-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai yethanai building-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora vittu thorathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora vittu thorathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pochi en pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pochi en pangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga mannai pudugunavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga mannai pudugunavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeroplane-il parakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeroplane-il parakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha janangga surundu padukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha janangga surundu padukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jail-u kadhava thorakkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jail-u kadhava thorakkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaanu kettaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanu kettaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundaasula podunguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundaasula podunguraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad to the core
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad to the core"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa…aaa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa…aaa….aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki…"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaaki color-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaaki color-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Very very bad…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very bad…"/>
</div>
</pre>
