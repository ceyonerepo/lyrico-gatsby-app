---
title: "yaamili yaamiliyaa song lyrics"
album: "Laabam"
artist: "D.Imman"
lyricist: "Yugabharathi"
director: "SP. Jhananathan"
path: "/albums/laabam-lyrics"
song: "Yaamili Yaamiliyaa"
image: ../../images/albumart/laabam.jpg
date: 2021-09-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_F9VcxBNMBk"
type: "happy"
singers:
  - Divya Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thorkkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thorkkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kodipidi onnaana athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kodipidi onnaana athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaaru vandhaaru pakkiraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaaru vandhaaru pakkiraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vai mudhaladi paatoda whistle adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vai mudhaladi paatoda whistle adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai sera nalvaazhva thantharaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai sera nalvaazhva thantharaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thorkkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thorkkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarisamama yevaraiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarisamama yevaraiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathura oh azhagula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathura oh azhagula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Olagam pudhusa undaagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olagam pudhusa undaagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagalarumae enanjidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagalarumae enanjidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiramum thirumbiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiramum thirumbiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai vilangae thundaagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai vilangae thundaagudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyathadhu boomiyil onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyathadhu boomiyil onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhal varum maarudhal thulla thulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhal varum maarudhal thulla thulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thorkkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thorkkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kodipidi onnaana athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kodipidi onnaana athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaaru vandhaaru pakkiraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaaru vandhaaru pakkiraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamiliyaa yaamiliyaa yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamiliyaa yaamiliyaa yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamiliyaa miliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamiliyaa miliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamiliyaa mili mili mili mili mili mili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamiliyaa mili mili mili mili mili mili"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhu nyayam yedhu needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu nyayam yedhu needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena arindha oruvan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena arindha oruvan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan unadhu vazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan unadhu vazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimira thodanguromae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimira thodanguromae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaiyalam theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyalam theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukuzhiyil kedantha naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukuzhiyil kedantha naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arival thelivum adaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arival thelivum adaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal mudhalil sirikkiromae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal mudhalil sirikkiromae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannoda vaasanai unmela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannoda vaasanai unmela thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam niththam suththum nikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam niththam suththum nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiya un vaarthaiya ellarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiya un vaarthaiya ellarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikkittom michcham vaikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikkittom michcham vaikkama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru rekka katti vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru rekka katti vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu nenja vandhu thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu nenja vandhu thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu moththa makkal un kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu moththa makkal un kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thorkkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thorkkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kodipidi onnaana athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kodipidi onnaana athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaaru vandhaaru pakkiraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaaru vandhaaru pakkiraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani oruvan thalamaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani oruvan thalamaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalaiyum varuvathillae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalaiyum varuvathillae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindhae edhaiyum velvomaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhae edhaiyum velvomaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana urudhi peruvathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana urudhi peruvathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamadhiyae jeiyichidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamadhiyae jeiyichidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivappae vazhiyai kolvomaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivappae vazhiyai kolvomaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhuvaagira bhoomiyil thunbam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuvaagira bhoomiyil thunbam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paliyaadugal naam ini alla alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paliyaadugal naam ini alla alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum maaruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum maaruthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamili yaamiliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamili yaamiliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thorkkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thorkkuthaiya"/>
</div>
</pre>
