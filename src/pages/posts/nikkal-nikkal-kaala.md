---
title: "nikkal nikkal song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Dopeadelicz - Logan"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Nikkal Nikkal"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_DIMQrd8Ga8"
type: "mass"
singers:
  - Dopeadelicz
  - Vivek
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ithu enga koottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enga koottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vandha vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vandha vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru whistle onnu adichakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru whistle onnu adichakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambum pettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambum pettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga sirusu kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga sirusu kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannum periya settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannum periya settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga perusunga vanthakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga perusunga vanthakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathu route-eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu route-eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathungi adangi vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi adangi vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga innum slave ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga innum slave ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninju ethirthu nippomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninju ethirthu nippomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ippo brave ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ippo brave ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nelamaiya maathurennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelamaiya maathurennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelatha aattaiya podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelatha aattaiya podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai mosadi senjuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai mosadi senjuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga olinju odura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga olinju odura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaiyoda vayithula adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaiyoda vayithula adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bank balance-ah yethatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank balance-ah yethatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mogamoodi pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamoodi pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi pithalaattam pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi pithalaattam pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaloda othumai thurumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaloda othumai thurumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikkatha irumbuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikkatha irumbuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavu ellaam inga palikkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanavu ellaam inga palikkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kela kela kela kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kela kela kela kelambu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal chalth tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal chalth tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal nikkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal nikkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama kelambu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga nadakathu unga alumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga nadakathu unga alumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba vachukkaatha oram othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba vachukkaatha oram othungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa tie-ah katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa tie-ah katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla file thooki vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla file thooki vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikaatha veenaa velagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikaatha veenaa velagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu enga kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enga kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha unarndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha unarndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda nenaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda nenaikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha odambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi kudutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi kudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga maatta dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga maatta dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha vazhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha vazhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">paathu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu nikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu nikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu nikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu nikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu nikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu nikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu nikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu nikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagakira thakkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagakira thakkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkitta sikkitta sikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkitta sikkitta sikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaa daa indhaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa daa indhaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkitta sikkitta sikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkitta sikkitta sikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaa daa indhaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa daa indhaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkitta sikkitta sikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkitta sikkitta sikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaa daa indhaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa daa indhaa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei"/>
</div>
</pre>
