---
title: "adugira maatai song lyrics"
album: "Kullanari Koottam"
artist: "V. Selvaganesh"
lyricist: "V. Elango"
director: "Sribalaji"
path: "/albums/kullanari-koottam-lyrics"
song: "Adugira Maatai"
image: ../../images/albumart/kullanari-koottam.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_1_ZsEIDcK0"
type: "happy"
singers:
  - Krishna Iyer
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaakki Taakki Vachikkinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakki Taakki Vachikkinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakki Nammalaiye Paarkkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Nammalaiye Paarkkudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baakki Nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baakki Nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Giraakki Pannaama Paadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giraakki Pannaama Paadungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam Podungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Podungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appadiyaa Ippo Naan Onnu Kathattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiyaa Ippo Naan Onnu Kathattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Labukku Labukku Labaailabukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labukku Labukku Labaailabukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Labaai Labuku Balaasa Ballaasa Balaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labaai Labuku Balaasa Ballaasa Balaasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onniyum Puriala Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onniyum Puriala Maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Puriyira Maari Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Puriyira Maari Paadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadugira Maatta Aadi Karadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadugira Maatta Aadi Karadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugira Maatta Paadi Karakkanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugira Maatta Paadi Karakkanumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kullakkathirikkaaye Adidaa Tea Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kullakkathirikkaaye Adidaa Tea Ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Settai Thaangalaiye Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Settai Thaangalaiye Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ududaa Ravusu Idhuvaa Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ududaa Ravusu Idhuvaa Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbaa Love vu Jeyikkanundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbaa Love vu Jeyikkanundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaa Irundhom Aniyaa Serndhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaa Irundhom Aniyaa Serndhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anilaai Inime Udhavidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anilaai Inime Udhavidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbargal Illaa Ponaalum Aagaadhadaa Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbargal Illaa Ponaalum Aagaadhadaa Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Namma Madharaadiya Keluppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Namma Madharaadiya Keluppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Indhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Indhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ngoiyaalo Ngoiyaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ngoiyaalo Ngoiyaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandavudan Kaadhal Panninathaal Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavudan Kaadhal Panninathaal Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammudaiya Natpu Adhuthaane Ipoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammudaiya Natpu Adhuthaane Ipoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkichatta Poda Poda Naama Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkichatta Poda Poda Naama Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukku Naam Kaiyilaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukku Naam Kaiyilaganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvin Alavai Alandhaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Alavai Alandhaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Natpin Aazham Alandhaaraa Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Natpin Aazham Alandhaaraa Raa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Naamum Inaivom Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Naamum Inaivom Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadavul Nammai Padaithaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadavul Nammai Padaithaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthangal Pesadha Vishyangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthangal Pesadha Vishyangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Pesalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Pesalaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thanjavuru Adiya Ketkuriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thanjavuru Adiya Ketkuriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththurenlla Nallaa Yeththi Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththurenlla Nallaa Yeththi Adraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Adraa Adraa Adhethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Adraa Adraa Adhethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dommeellu Dommeellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dommeellu Dommeellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeraththathaandi Inga Jeyichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeraththathaandi Inga Jeyichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaniya Nenaichaa Vetrigal Varudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaniya Nenaichaa Vetrigal Varudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerangal Endru Inimel Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerangal Endru Inimel Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiye Vandhaa Yendum Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiye Vandhaa Yendum Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Valaiyil Sikkiya Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Valaiyil Sikkiya Nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval Thuraiyil Nuzhaivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Thuraiyil Nuzhaivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumanamaagi Manaivi Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumanamaagi Manaivi Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala Nenachippaarpiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Nenachippaarpiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalil Pala Naale Paarthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalil Pala Naale Paarthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Podhundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Podhundaa"/>
</div>
</pre>
