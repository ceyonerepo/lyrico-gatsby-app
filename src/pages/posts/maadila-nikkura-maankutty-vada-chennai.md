---
title: "maadila nikkura maankutty song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Gana Bala"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Maadila Nikkura Maankutty"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sFF4egYz_eA"
type: "happy"
singers:
  - Gana Bala
  - Dhee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadila nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Melavaa kaaturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melavaa kaaturen"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi thottiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi thottiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Color-u meenaa suthuraadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color-u meenaa suthuraadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan munnaadi ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan munnaadi ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondila pottaa kathuraadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondila pottaa kathuraadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi paathan oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi paathan oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pola yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pola yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu perum onnu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu perum onnu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi paathan oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi paathan oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pola yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pola yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu perum onnu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu perum onnu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakala idhupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakala idhupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisam poduvaa ennathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisam poduvaa ennathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram badhila solla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram badhila solla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathuren dhenamum unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathuren dhenamum unnathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadila nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Melavaa kaaturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melavaa kaaturen"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannathaanaa nannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannathaanaa nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannathaanaa nannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannathaanaa nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannathaanaa nannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannathaanaa nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nattaar kada thenga badha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattaar kada thenga badha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasapattaa maanga thunnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasapattaa maanga thunnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali kodathil kaiya vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali kodathil kaiya vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanku thanni naanum utten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanku thanni naanum utten"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadha dosa enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadha dosa enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppipottu joraa varthuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppipottu joraa varthuputtaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli soda ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli soda ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Odachu oothi gaali panniputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odachu oothi gaali panniputtaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maari pochu en life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari pochu en life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae avathaan en wife-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae avathaan en wife-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Asdfgf-u eppammaa adippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asdfgf-u eppammaa adippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan type-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan type-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadila nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Melavaa kaaturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melavaa kaaturen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amman koyil theru ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amman koyil theru ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadimaasam koozhu ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadimaasam koozhu ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasimedu meenu ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasimedu meenu ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Cut and rightu aalu ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cut and rightu aalu ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda choru enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda choru enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhamba oothi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhamba oothi "/>
</div>
<div class="lyrico-lyrics-wrapper">vaari thunnuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaari thunnuputtaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa kaatiputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa kaatiputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkitta irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-eh eduthukittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-eh eduthukittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nightu lightu velichathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightu lightu velichathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sightu adika vandhaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sightu adika vandhaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Whiteu color-u strickerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whiteu color-u strickerula"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppa pocket-il pottaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppa pocket-il pottaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadila nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Meladhaan vandhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meladhaan vandhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru etti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru etti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillavula naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillavula naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaaga porandhan onakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnaaga porandhan onakkuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada onakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada onakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaadha onna eppo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaadha onna eppo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuva enakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuva enakkuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi paathan oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi paathan oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pola yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pola yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu perum onnu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu perum onnu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi paathan oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi paathan oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pola yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pola yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu perum onnu sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu perum onnu sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kaalam thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kaalam thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakala idhupol ponnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakala idhupol ponnathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisam poduvaa ennai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisam poduvaa ennai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadila ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram badhila solla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram badhila solla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathuren thinamum unnai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathuren thinamum unnai thaan"/>
</div>
</pre>
