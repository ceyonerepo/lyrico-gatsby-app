---
title: "tum bhi ho wahi song lyrics"
album: "Kites"
artist: "Rajesh Roshan"
lyricist: "Nasir Faraaz"
director: "Anurag Basu"
path: "/albums/kites-lyrics"
song: "Tum Bhi Ho Wahi"
image: ../../images/albumart/kites.jpg
date: 2010-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/L-HXEqAJy9w"
type: "love"
singers:
  - Suraj Jagan
  - Vishal Dadlani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tum bhi ho wahi hum bhi hai wahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum bhi ho wahi hum bhi hai wahi"/>
</div>
<div class="lyrico-lyrics-wrapper">baat phir ab lagati hai nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baat phir ab lagati hai nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">kya hua galat aur kya sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kya hua galat aur kya sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">dono sath hai sach to hai yahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dono sath hai sach to hai yahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaade vo lagti rah gayi piche kahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaade vo lagti rah gayi piche kahi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye pyar ab hame aage khiche kahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pyar ab hame aage khiche kahi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye dur tak bechain hai sab raste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye dur tak bechain hai sab raste"/>
</div>
<div class="lyrico-lyrics-wrapper">is haal me tu muskura mere vaaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="is haal me tu muskura mere vaaste"/>
</div>
<div class="lyrico-lyrics-wrapper">tum bhi ho wahi ham bhi hai wahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum bhi ho wahi ham bhi hai wahi"/>
</div>
<div class="lyrico-lyrics-wrapper">baat phir ab lagati hai nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baat phir ab lagati hai nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">kya hua galat aur kya sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kya hua galat aur kya sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">dono sath hai sach to hai yahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dono sath hai sach to hai yahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ab din dhale chahe jaha ab sham ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ab din dhale chahe jaha ab sham ho"/>
</div>
<div class="lyrico-lyrics-wrapper">parvah kya ab chahe jo anjam ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parvah kya ab chahe jo anjam ho"/>
</div>
<div class="lyrico-lyrics-wrapper">vo aasman ho ya jami hamne likha pyar ko har kahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vo aasman ho ya jami hamne likha pyar ko har kahi"/>
</div>
<div class="lyrico-lyrics-wrapper">shishe ka har ek pal isme chupa apna kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shishe ka har ek pal isme chupa apna kal"/>
</div>
<div class="lyrico-lyrics-wrapper">tute na ye sambhalo ise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tute na ye sambhalo ise"/>
</div>
<div class="lyrico-lyrics-wrapper">shishe ka har ek pal isme chupa apna kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shishe ka har ek pal isme chupa apna kal"/>
</div>
<div class="lyrico-lyrics-wrapper">tute na ye sambhalo ise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tute na ye sambhalo ise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tum bhi ho wahi hum bhi hai wahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum bhi ho wahi hum bhi hai wahi"/>
</div>
<div class="lyrico-lyrics-wrapper">baat phir ab lagati hai nayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baat phir ab lagati hai nayi"/>
</div>
<div class="lyrico-lyrics-wrapper">kya hua galat aur kya sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kya hua galat aur kya sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">dono sath hai sach to hai yahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dono sath hai sach to hai yahi"/>
</div>
</pre>
