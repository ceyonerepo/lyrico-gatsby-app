---
title: "kites in the sky song lyrics"
album: "Kites"
artist: "Rajesh Roshan"
lyricist: "Asif Ali Beg"
director: "Anurag Basu"
path: "/albums/kites-lyrics"
song: "Kites In the Sky"
image: ../../images/albumart/kites.jpg
date: 2010-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/89REtILyAqY"
type: "love"
singers:
  - Suzanne D Mello
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kites in the sky soaring together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kites in the sky soaring together"/>
</div>
<div class="lyrico-lyrics-wrapper">lovers forever forever is a lie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovers forever forever is a lie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">who  holds the time in his hand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="who  holds the time in his hand"/>
</div>
<div class="lyrico-lyrics-wrapper">what has destiny planned
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what has destiny planned"/>
</div>
<div class="lyrico-lyrics-wrapper">will the heart understand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="will the heart understand"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kites in the sky soaring together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kites in the sky soaring together"/>
</div>
<div class="lyrico-lyrics-wrapper">lovers forever forever is a lie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovers forever forever is a lie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kites in the sky lovers forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kites in the sky lovers forever"/>
</div>
</pre>
