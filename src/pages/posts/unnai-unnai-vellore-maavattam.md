---
title: "unnai unnai song lyrics"
album: "Vellore Maavattam"
artist: "Sundar C Babu"
lyricist: "Thamarai"
director: "R.N.R. Manohar"
path: "/albums/vellore-maavattam-lyrics"
song: "Unnai Unnai"
image: ../../images/albumart/vellore-maavattam.jpg
date: 2011-10-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Qb812at4cOQ"
type: "love"
singers:
  - Krish
  - Mahathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai unnai ondru ketpen solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai ondru ketpen solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">illai illai endru solli chelvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai illai endru solli chelvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum neeyum paarppoamendru ninaithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum neeyum paarppoamendru ninaithaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha naalil indha neram ganithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha naalil indha neram ganithaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piravi nooru naan thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piravi nooru naan thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu nindren innaalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu nindren innaalil"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku munbey nee vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku munbey nee vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathirundhaai ponnaalul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathirundhaai ponnaalul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai unnai ondru ketpen solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai ondru ketpen solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">illai illai endru solli chelvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai illai endru solli chelvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum neeyum paarppoamendru ninaithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum neeyum paarppoamendru ninaithaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha naalil indha neram ganithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha naalil indha neram ganithaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal kaadhal yedhedho seiyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal kaadhal yedhedho seiyudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">munnooru aandu aasai vandhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnooru aandu aasai vandhadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaicherndhu vinaadi neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaicherndhu vinaadi neramey"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhaalum poadhum endrey thoandrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhaalum poadhum endrey thoandrudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayathaa na piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathaa na piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">un meedhu konda aasai poagaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meedhu konda aasai poagaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">valarum indha inbam aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valarum indha inbam aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">nooraandu thaandum manalaanaal piragey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooraandu thaandum manalaanaal piragey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiyoasai kaadhoaram ketkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyoasai kaadhoaram ketkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">verendha Osaiyum kaadhil ketkavillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verendha Osaiyum kaadhil ketkavillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadikaaram sutraamal nirkkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadikaaram sutraamal nirkkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">mul rendum sandai poattu kondadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mul rendum sandai poattu kondadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">karaindhaalum karaiyum indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaindhaalum karaiyum indru "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai vittu poagaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vittu poagaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugangal veru veru aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugangal veru veru aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nammai serendru ondril ondru serndhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai serendru ondril ondru serndhadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai unnai ondru ketpen solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai ondru ketpen solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">illai illai endru solli chelvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai illai endru solli chelvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum neeyum paarppoamendru ninaithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum neeyum paarppoamendru ninaithaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha naalil indha neram ganithaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha naalil indha neram ganithaayaa"/>
</div>
</pre>
