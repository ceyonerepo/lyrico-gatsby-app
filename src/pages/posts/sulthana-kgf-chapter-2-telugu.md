---
title: "sulthana song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Ramajogayya Sastry"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Sulthana"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kgLlMeHoqHo"
type: "mass"
singers:
  - Sai Krishna
  - Prudhvi Chandra
  - Arun Kaundinya
  - Sai Charan
  - Santhosh Venky
  - Mohan Krishna
  - Sachin Basrur
  - Ravi Basrur
  - Puneeth Rudranag
  - Manish Dinakar
  - Harini Ivaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugetthe neeli gaganaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugetthe neeli gaganaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamotthe vela bhuvanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamotthe vela bhuvanaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavanche neeku shikharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavanche neeku shikharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jejelu palike khanijaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jejelu palike khanijaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niluvetthu nee kadamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvetthu nee kadamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkarulapaali ukku sammeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkarulapaali ukku sammeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anitharamu nee padamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anitharamu nee padamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amavasya cheelchu aggibaavuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amavasya cheelchu aggibaavuta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragile pagile nitturpulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragile pagile nitturpulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vennudhanne odaarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vennudhanne odaarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa bathukidhigo neekai mudupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa bathukidhigo neekai mudupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinchara thoorupu vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinchara thoorupu vaipu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sultana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sultana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhamettina balavikramudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhamettina balavikramudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Duritamatulu pani pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duritamatulu pani pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettegina prati vairukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettegina prati vairukala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudami odiki balipettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudami odiki balipettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kattakatika rakkasude okkokkadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kattakatika rakkasude okkokkadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetukokadu origettu ventapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetukokadu origettu ventapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaragamana samavartivai nedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaragamana samavartivai nedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satrujanula pranalapainabadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satrujanula pranalapainabadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tathyamuga jarigi tiravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tathyamuga jarigi tiravale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirataka daityula veta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirataka daityula veta"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaccitamuga nee khadga siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaccitamuga nee khadga siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Guritappadepudu ee cota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guritappadepudu ee cota"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragile pagile nitturpulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragile pagile nitturpulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vennudhanne odaarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vennudhanne odaarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa bathukidhigo neekai mudupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa bathukidhigo neekai mudupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinchara thoorupu vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinchara thoorupu vaipu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugetthe neeli gaganaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugetthe neeli gaganaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamotthe vela bhuvanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamotthe vela bhuvanaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavanche neeku shikharaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavanche neeku shikharaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rana rana rana rana dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rana rana rana rana dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jejelu palike khanijaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jejelu palike khanijaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niluvetthu nee kadamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvetthu nee kadamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkarulapaali ukku sammeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkarulapaali ukku sammeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anitharamu nee padamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anitharamu nee padamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amavasya cheelchu aggibaavuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amavasya cheelchu aggibaavuta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragile pagile nitturpulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragile pagile nitturpulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vennudhanne odaarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vennudhanne odaarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa bathukidhigo neekai mudupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa bathukidhigo neekai mudupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinchara thoorupu vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinchara thoorupu vaipu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sultana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sultana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera sura sulthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera sura sulthana"/>
</div>
</pre>
