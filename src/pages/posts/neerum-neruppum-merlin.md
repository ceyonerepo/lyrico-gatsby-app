---
title: "neerum neruppum song lyrics"
album: "Merlin"
artist: "Ganesh Raghavendra"
lyricist: "Saavee"
director: "Keera"
path: "/albums/merlin-song-lyrics"
song: "Neerum Neruppum"
image: ../../images/albumart/merlin.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n2q4-8PkVBU"
type: "love"
singers:
  - Ganesh Raghavendra
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neerum Neruppum Modhikonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerum Neruppum Modhikonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pola Varuma Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pola Varuma Nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhavanae Ennai Thozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhavanae Ennai Thozhudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Vaeragi Ponalum Maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vaeragi Ponalum Maaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaeraagi Pogaamal Saagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaeraagi Pogaamal Saagaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakena Naanum Uyirthezhuvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakena Naanum Uyirthezhuvaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Varuvadhendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Varuvadhendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavena Naanum Kalaindhiduvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavena Naanum Kalaindhiduvaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pirindhu Sendral Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pirindhu Sendral Oh Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Pirai Pogum Payanathilbodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pirai Pogum Payanathilbodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhayae Thaedum Uyir Siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhayae Thaedum Uyir Siragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavinil Thondrum Kavi Mugam Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinil Thondrum Kavi Mugam Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirayidam Nenjam Murayidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirayidam Nenjam Murayidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silaikullae Poovanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaikullae Poovanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Sirayitta Thaen Malaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Sirayitta Thaen Malaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thodum Mogam Idhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thodum Mogam Idhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal Sudum Thaagamidhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal Sudum Thaagamidhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaikkullae Aayiram Kanavugalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaikkullae Aayiram Kanavugalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaithidu Uyirkondu Velivarum Meipada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaithidu Uyirkondu Velivarum Meipada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravinil Pookkum Minmini Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinil Pookkum Minmini Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli Indri Pon Parappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli Indri Pon Parappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaigalin Maelae Maanikkam Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaigalin Maelae Maanikkam Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Magizhndhaayae Thamizharasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Magizhndhaayae Thamizharasae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadapadhu Naadagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadapadhu Naadagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Udaithida Thaamadhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Udaithida Thaamadhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirthidum Kaaviyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthidum Kaaviyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaindhidum Aayudhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaindhidum Aayudhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaithidum Namakena Pudhu Ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaithidum Namakena Pudhu Ulagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithidum Padai Kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithidum Padai Kondu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikiraen Ninaikiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikiraen Ninaikiraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerum Neruppum Modhikonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerum Neruppum Modhikonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pola Varuma Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pola Varuma Nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhavanae Ennai Thozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhavanae Ennai Thozhudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Vaeragi Ponalum Maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vaeragi Ponalum Maaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaeraagi Pogaamal Saagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaeraagi Pogaamal Saagaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakena Naanum Uyirthezhuvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakena Naanum Uyirthezhuvaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Varuvadhendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Varuvadhendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavena Naanum Kalaindhiduvaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavena Naanum Kalaindhiduvaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pirindhu Sendral Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pirindhu Sendral Oh Oh Oh"/>
</div>
</pre>
