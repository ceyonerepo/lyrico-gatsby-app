---
title: "dhaga dhagamane song lyrics"
album: "Agnyaathavaasi"
artist: "Anirudh Ravichander"
lyricist: "Sri Mani"
director: "Trivikram Srinivas"
path: "/albums/agnyaathavaasi-lyrics"
song: "Dhaga Dhagamane"
image: ../../images/albumart/agnyaathavaasi.jpg
date: 2018-01-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lfX3ZT3jVic"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhaga Dhagamaney Toorupu Disa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga Dhagamaney Toorupu Disa "/>
</div>
<div class="lyrico-lyrics-wrapper">Padamara Nisay Mugisene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamara Nisay Mugisene "/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Mane Nadi Padanisa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Mane Nadi Padanisa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirulo Tadisene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirulo Tadisene "/>
</div>
<div class="lyrico-lyrics-wrapper">Kodavali Bujamunavesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodavali Bujamunavesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukey Kothaku Kadhiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukey Kothaku Kadhiley "/>
</div>
<div class="lyrico-lyrics-wrapper">Darbanu Dhanuvuga Visire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darbanu Dhanuvuga Visire "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhargavaramudu Veedey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhargavaramudu Veedey "/>
</div>
<div class="lyrico-lyrics-wrapper">Saaduvukale Veluguna Padi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaduvukale Veluguna Padi "/>
</div>
<div class="lyrico-lyrics-wrapper">Satyamila Merisene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satyamila Merisene "/>
</div>
<div class="lyrico-lyrics-wrapper">Agnyathame Marugunapadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agnyathame Marugunapadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Aayudamegasene Ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudamegasene Ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erragaa Tadipene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erragaa Tadipene "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Raacha Raktamo Ningine 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Raacha Raktamo Ningine "/>
</div>
<div class="lyrico-lyrics-wrapper">Korragaa Merisene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korragaa Merisene"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasigarika Anchuno Kiraname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasigarika Anchuno Kiraname "/>
</div>
<div class="lyrico-lyrics-wrapper">Merupula Deepam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupula Deepam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chamuralle Cheekativompi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamuralle Cheekativompi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo Velthuru Nimpi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo Velthuru Nimpi "/>
</div>
<div class="lyrico-lyrics-wrapper">Chirujallulu Kuripistaadeyy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirujallulu Kuripistaadeyy "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukula Daaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula Daaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Chivaranchuku Ningini Chutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivaranchuku Ningini Chutti "/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurinche Nelaku Katti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurinche Nelaku Katti "/>
</div>
<div class="lyrico-lyrics-wrapper">Renditini Kalpestadeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renditini Kalpestadeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavuni Varam Eeledani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavuni Varam Eeledani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyoriney Narikeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyoriney Narikeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaa Ani Analedhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaa Ani Analedhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Doodane Nalipeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Doodane Nalipeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodavali Bujamunavesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodavali Bujamunavesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukey Kothaku Kadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukey Kothaku Kadhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Darbanu Dhanuvuga Visire 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darbanu Dhanuvuga Visire "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhargavaramudu Veedey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhargavaramudu Veedey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siddudi Pranavamlaa Veedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siddudi Pranavamlaa Veedu "/>
</div>
<div class="lyrico-lyrics-wrapper">Buddudi Sravanamlaa Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddudi Sravanamlaa Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddamantha Shabdam Veedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddamantha Shabdam Veedu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedoka Pramaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedoka Pramaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ranamulaa Ninadistuntadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranamulaa Ninadistuntadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Saramulaa Yedurostuntadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saramulaa Yedurostuntadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Sarana Toranamitadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Sarana Toranamitadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedoka Pramadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedoka Pramadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppanchuna Kala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppanchuna Kala "/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna Nijamai Kanipinchelaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna Nijamai Kanipinchelaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedo Vidudala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedo Vidudala "/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Pranaala Mounalakivela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Pranaala Mounalakivela "/>
</div>
<div class="lyrico-lyrics-wrapper">Merupula Deepam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupula Deepam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chamuralle Cheekativompi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamuralle Cheekativompi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo Velthuru Nimpi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo Velthuru Nimpi "/>
</div>
<div class="lyrico-lyrics-wrapper">Chirujallulu Kuripistaadeyy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirujallulu Kuripistaadeyy "/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukula Daaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula Daaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Chivaranchuku Ningini Chutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivaranchuku Ningini Chutti "/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurinche Nelaku Katti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurinche Nelaku Katti "/>
</div>
<div class="lyrico-lyrics-wrapper">Renditini Kalpestadeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renditini Kalpestadeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Erragaa Tadipene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erragaa Tadipene "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Raacha Raktamo Ningine 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Raacha Raktamo Ningine "/>
</div>
<div class="lyrico-lyrics-wrapper">Korragaa Merisene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korragaa Merisene"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasigarika Anchuno Kiraname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasigarika Anchuno Kiraname "/>
</div>
<div class="lyrico-lyrics-wrapper">Mullinani Vivarinchadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullinani Vivarinchadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvula Yade Yennadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvula Yade Yennadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannellane Vadaboyadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannellane Vadaboyadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Megham Yeppupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham Yeppupu"/>
</div>
</pre>
