---
title: "morrakka song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Morrakka"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ielNlsacdk8"
type: "happy"
singers:
  - Uthara Unnikrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Morakka mattrakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morakka mattrakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthachae diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthachae diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Morakka mattrakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morakka mattrakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavi aadum poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi aadum poova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maffrakka mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maffrakka mettarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En kuda aade nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kuda aade nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maffrakka mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maffrakka mettarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh dancing show-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh dancing show-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey…naan michael jacksi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…naan michael jacksi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavu nelavu nadai kaatetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavu nelavu nadai kaatetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…naan hrithik roshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…naan hrithik roshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaivu nelivu athil sekketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaivu nelivu athil sekketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey…naan baryshnikovin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…naan baryshnikovin"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakum nadai oru ballet thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakum nadai oru ballet thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naan prabhu devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naan prabhu devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakku modakku yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakku modakku yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadakku padakku yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadakku padakku yena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechan adi vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechan adi vechan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha roadum koda medah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha roadum koda medah"/>
</div>
<div class="lyrico-lyrics-wrapper">Chacha eh chacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacha eh chacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En heartu beatum aatam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heartu beatum aatam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Morakka mattrakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morakka mattrakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthachae diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthachae diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Morakka mattrakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morakka mattrakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavi aadum poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi aadum poova"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maffrakka mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maffrakka mettarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En kuda aade nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kuda aade nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maffrakka mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maffrakka mettarikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh dancing show-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh dancing show-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril malar aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril malar aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeril veyil aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril veyil aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam yethil illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam yethil illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu mettarikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettil nizhal aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettil nizhal aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil uyil aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil uyil aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam aatam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam aatam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu mettarikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu mettarikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salsa eh samba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salsa eh samba"/>
</div>
<div class="lyrico-lyrics-wrapper">En mango yengo tango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mango yengo tango"/>
</div>
<div class="lyrico-lyrics-wrapper">Volca kokomar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volca kokomar"/>
</div>
<div class="lyrico-lyrics-wrapper">En kuththu dance-eh paarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kuththu dance-eh paarungo"/>
</div>
</pre>
