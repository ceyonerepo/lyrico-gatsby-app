---
title: "vettikullara ona song lyrics"
album: "Super Duper"
artist: "Divakara Thiyagarajan"
lyricist: "AK"
director: "Arun Karthik"
path: "/albums/super-duper-lyrics"
song: "Vettikullara Ona"
image: ../../images/albumart/super-duper.jpg
date: 2019-09-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Pqt5T3pWBX0"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Once Upon A Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once Upon A Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Oorula Oru Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Oorula Oru Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Roattula Paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Roattula Paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Aathaa Illadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Aathaa Illadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Super-Boy Ah!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super-Boy Ah!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Once Upon A Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once Upon A Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Oorula Oru Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Oorula Oru Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Naala Pakkamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Naala Pakkamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Loanaa Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loanaa Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaya Kadandha Haayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaya Kadandha Haayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vangiya Kadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangiya Kadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi Kudukavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi Kudukavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaya Peiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Peiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanjadhu Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanjadhu Aaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattikku Badhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattikku Badhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Payyana Koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payyana Koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealing’a Mudichayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealing’a Mudichayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona Yedathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Yedathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Kedachadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Kedachadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senior Age’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senior Age’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gap Kedachadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap Kedachadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Essu Aanadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Essu Aanadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Life’eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life’eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhool Dhaan Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhool Dhaan Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiya Namba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya Namba"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthanum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthanum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiya Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara Onaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara Onaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Super Duper Jodi Scene ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Super Duper Jodi Scene ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Kolladha Plan’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Kolladha Plan’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Podum Kootani Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Podum Kootani Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soluva Sikkaadha Meenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soluva Sikkaadha Meenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Veesa Vandha Spiderman’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Veesa Vandha Spiderman’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nikkadha Maanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nikkadha Maanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Localu Hunter Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Localu Hunter Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara Onaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara Onaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Super Duper Jodi Scene ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Super Duper Jodi Scene ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Kolladha Plan’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Kolladha Plan’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Podum Kootani Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Podum Kootani Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soluva Sikkaadha Meenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soluva Sikkaadha Meenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Veesa Vandha Spiderman’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Veesa Vandha Spiderman’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nikkadha Maanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nikkadha Maanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Localu Hunter Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Localu Hunter Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinna Cheating’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Cheating’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Senju Senju Boraachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senju Senju Boraachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kachidhamaa Pottachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachidhamaa Pottachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Route’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Route’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Owner Illa Guest House
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Owner Illa Guest House"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadhippo Rest House
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadhippo Rest House"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttiyum Neruppu Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttiyum Neruppu Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Koottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Koottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feesu Mela…Fees Ethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feesu Mela…Fees Ethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Mela Kaasaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Mela Kaasaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Develp’u Aanad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Develp’u Aanad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru School’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru School’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bank Balance Sethaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank Balance Sethaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvithandhai Aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvithandhai Aayachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Owner’u Kedi Dhanapaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Owner’u Kedi Dhanapaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhanapalukkoru Phone Callu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhanapalukkoru Phone Callu"/>
</div>
<div class="lyrico-lyrics-wrapper">Phone Callil Oru Black Mailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone Callil Oru Black Mailu"/>
</div>
<div class="lyrico-lyrics-wrapper">Schoolu Pullainga Naalu Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Schoolu Pullainga Naalu Peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkunadhaaru Namma Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkunadhaaru Namma Aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiya Solli Parandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya Solli Parandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaariyam Smoothaa Mudichachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariyam Smoothaa Mudichachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavattittu Ponadhu 4 Lakhs-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavattittu Ponadhu 4 Lakhs-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Twistula Mudinjadhu Climax’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twistula Mudinjadhu Climax’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiya Solli Parandhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiya Solli Parandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaariyam Smoothaa Mudichachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariyam Smoothaa Mudichachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavattittu Ponadhu 4 Lakhs-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavattittu Ponadhu 4 Lakhs-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Twistula Mudinjadhu Climax’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twistula Mudinjadhu Climax’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara Onaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara Onaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Super Duper Jodi Scene ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Super Duper Jodi Scene ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Kolladha Plan’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Kolladha Plan’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Podum Kootani Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Podum Kootani Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soluva Sikkaadha Meenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soluva Sikkaadha Meenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Veesa Vandha Spiderman’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Veesa Vandha Spiderman’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Nikkadha Maanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Nikkadha Maanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Localu Hunter Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Localu Hunter Maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara Onaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara Onaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Kolladha Plan’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Kolladha Plan’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soluva Sikkaadha Meenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soluva Sikkaadha Meenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Duperu Scene’a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Duperu Scene’a"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Kullara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Kullara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Kullareyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Kullareyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti kullara Onaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti kullara Onaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Super Duper Jodi Scene ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Super Duper Jodi Scene ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati Kolladha Plan’aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maati Kolladha Plan’aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Podum Kootani Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Podum Kootani Maa"/>
</div>
</pre>
