---
title: "na oruthikitta matikittan song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "Mohan Rajan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Na Oruthikitta Matikittan"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eghGmETAwfU"
type: "sad"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa Oruthikitta Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Oruthikitta Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Varuthathula Idha Yethikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Varuthathula Idha Yethikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha World War-eh Thevala Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha World War-eh Thevala Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Ponnuga Tholla Thaangala Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ponnuga Tholla Thaangala Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thookki Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thookki Veesitta"/>
</div>
<div class="lyrico-lyrics-wrapper">En Heart-ah Ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Heart-ah Ava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeni Pola Kottittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeni Pola Kottittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Periya Periya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Periya Periya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Senjen Vittuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Senjen Vittuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Chinna Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Chinna Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusu Paduthi Pirichittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusu Paduthi Pirichittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thookki Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thookki Veesitta"/>
</div>
<div class="lyrico-lyrics-wrapper">En Heart-ah Ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Heart-ah Ava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeni Pola Kottittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeni Pola Kottittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Periya Periya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Periya Periya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Senjen Vittuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Senjen Vittuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Chinna Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Chinna Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusu Paduthi Pirichittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusu Paduthi Pirichittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Vachi Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Vachi Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothaattam Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothaattam Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker-ah Aakidum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker-ah Aakidum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaneer Enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneer Enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Pichi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Pichi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tsunami Daa Odu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tsunami Daa Odu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Polachavan Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Polachavan Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavila Veguna Idli Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavila Veguna Idli Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aaviyae Veguna Kaadhalu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aaviyae Veguna Kaadhalu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Yaarumae Solladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yaarumae Solladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathuvam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathuvam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First-u Mass Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Mass Aakkum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last-u Loose Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last-u Loose Aakkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Aavila Veguna Idli Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Aavila Veguna Idli Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aaviyae Veguna Kaadhalu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aaviyae Veguna Kaadhalu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Yaarumae Solladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yaarumae Solladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathuvam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathuvam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First-u Mass Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Mass Aakkum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last-u Loose Aakkum Daa Kaadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last-u Loose Aakkum Daa Kaadhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idiyaappam Pola Kaadhal Sikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiyaappam Pola Kaadhal Sikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avukka Theriyala… Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avukka Theriyala… Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigara Mulla Suththi Sozhaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigara Mulla Suththi Sozhaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa Vazhi Onnum Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa Vazhi Onnum Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatti Vaikkum Kaadhal Adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatti Vaikkum Kaadhal Adha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootti Vaikka Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootti Vaikka Mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatti Vittu Poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti Vittu Poyiduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Aaduthu Paaduthu Ponen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Aaduthu Paaduthu Ponen Veena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavila Veguna Idli Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavila Veguna Idli Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aaviyae Veguna Kaadhalu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aaviyae Veguna Kaadhalu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Yaarumae Solladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yaarumae Solladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathuvam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathuvam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First-u Mass Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Mass Aakkum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last-u Loose Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last-u Loose Aakkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Aavila Veguna Idli Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Aavila Veguna Idli Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aaviyae Veguna Kaadhalu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aaviyae Veguna Kaadhalu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Yaarumae Solladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yaarumae Solladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathuvam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathuvam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First-u Mass Aakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First-u Mass Aakkum Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Last-u Loose Aakkum Daa Kaadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last-u Loose Aakkum Daa Kaadhalu"/>
</div>
</pre>
