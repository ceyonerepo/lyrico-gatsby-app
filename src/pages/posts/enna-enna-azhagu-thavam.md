---
title: "enna enna azhagu song lyrics"
album: "Thavam"
artist: "Srikanth Deva"
lyricist: "Mayil"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Enna Enna Azhagu"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bKTLbW-n8YM"
type: "happy"
singers:
  - Karunguil James
  - Hema Ambika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna enna azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oora pola ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oora pola ethum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi neyum paru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi neyum paru "/>
</div>
<div class="lyrico-lyrics-wrapper">pakathula aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathula aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">suthamana kathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthamana kathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un thotathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thotathil "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum vasanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum vasanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">poo parikira alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo parikira alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">yosanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yosanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">un thotathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thotathil "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum vasanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum vasanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">poo parikira alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo parikira alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">yosanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yosanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">itha enni neyum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha enni neyum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku eeda unga oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku eeda unga oora"/>
</div>
<div class="lyrico-lyrics-wrapper">itha enni neyum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha enni neyum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku eeda unga oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku eeda unga oora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oora pola ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oora pola ethum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi neyum paru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi neyum paru "/>
</div>
<div class="lyrico-lyrics-wrapper">pakathula aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathula aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">suthamana kathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthamana kathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">themangu than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="themangu than "/>
</div>
<div class="lyrico-lyrics-wrapper">themangu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="themangu than"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu namma ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ooru themangu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ooru themangu than"/>
</div>
<div class="lyrico-lyrics-wrapper">elelo elelo elelo elelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elelo elelo elelo elelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonthamana selipa paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthamana selipa paru"/>
</div>
<div class="lyrico-lyrics-wrapper">sengarumbu thotatha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sengarumbu thotatha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">vayal veliyil katha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayal veliyil katha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">vasiyam panum natha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasiyam panum natha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadi aatam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadi aatam than"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadi aatam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadi aatam than"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi thiriyum kalagal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi thiriyum kalagal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">aalaiyum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalaiyum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam potu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam potu than"/>
</div>
<div class="lyrico-lyrics-wrapper">seeri payum manju viratu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeri payum manju viratu "/>
</div>
<div class="lyrico-lyrics-wrapper">kala paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu angu kanbathunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu angu kanbathunda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithil ethum angu unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithil ethum angu unda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu angu kanbathunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu angu kanbathunda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithil ethum angu unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithil ethum angu unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oora pola ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oora pola ethum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi neyum paru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi neyum paru "/>
</div>
<div class="lyrico-lyrics-wrapper">pakathula aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathula aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">suthamana kathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthamana kathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karakatam kavadiyattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karakatam kavadiyattam"/>
</div>
<div class="lyrico-lyrics-wrapper">oyilatam mayilatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyilatam mayilatam"/>
</div>
<div class="lyrico-lyrics-wrapper">silambatam kummiattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silambatam kummiattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thappatam bommalatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappatam bommalatam"/>
</div>
<div class="lyrico-lyrics-wrapper">thevaratam kalaiyattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevaratam kalaiyattam"/>
</div>
<div class="lyrico-lyrics-wrapper">sevalatam kolatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevalatam kolatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaliyatam kanigalatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaliyatam kanigalatam"/>
</div>
<div class="lyrico-lyrics-wrapper">raja rani aatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raja rani aatam "/>
</div>
<div class="lyrico-lyrics-wrapper">kokali kattai aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokali kattai aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu poi kal kuthirai aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu poi kal kuthirai aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thennanthopu katha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennanthopu katha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvoram aatam thoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvoram aatam thoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">malai neram aagum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai neram aagum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayiloda aatam paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayiloda aatam paru"/>
</div>
<div class="lyrico-lyrics-wrapper">karunguyilin satham onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunguyilin satham onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiyila ketkum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyila ketkum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">kathiravana vanangum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathiravana vanangum "/>
</div>
<div class="lyrico-lyrics-wrapper">pongaluku vanthu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongaluku vanthu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru peruma paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru peruma paru"/>
</div>
<div class="lyrico-lyrics-wrapper">itha pathu pathila kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha pathu pathila kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ooru peruma paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ooru peruma paru"/>
</div>
<div class="lyrico-lyrics-wrapper">itha pathu pathila kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha pathu pathila kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oora pola ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oora pola ethum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi neyum paru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi neyum paru "/>
</div>
<div class="lyrico-lyrics-wrapper">pakathula aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathula aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">suthamana kathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthamana kathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum illa"/>
</div>
</pre>
