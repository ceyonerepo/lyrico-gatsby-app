---
title: "vennilave song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Vennilave"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h8Hfi9hfQd4"
type: "melody"
singers:
  - G.V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vennilave Vevennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilave Vevennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Venmegam Unnai Indru Thediduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmegam Unnai Indru Thediduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maarbil Nee Thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maarbil Nee Thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Kaatchi Kanneril Karainthiduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Kaatchi Kanneril Karainthiduthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire Uyir Nee Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Uyir Nee Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril Uyirum Illayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril Uyirum Illayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Unnaal Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Unnaal Unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Udaindhen Unmaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Udaindhen Unmaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Enni Unnai Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Enni Unnai Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Unthan Vaasam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Unthan Vaasam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilave Vevennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilave Vevennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Venmegam Unnai Indru Thediduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmegam Unnai Indru Thediduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee Enge Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee Enge Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Thedi Thedi Alaigirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Thedi Thedi Alaigirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey Anbey Un Anbaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Anbey Un Anbaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Naanum Arindheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Naanum Arindheney"/>
</div>
<div class="lyrico-lyrics-wrapper">Allum Pagalum Unai Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allum Pagalum Unai Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Naanum Sumandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Naanum Sumandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angum Ingum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum Ingum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Unnai Thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Unnai Thedugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Arthamilla Vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arthamilla Vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Vaazhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Vaazhugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raare Raare Raare Raare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raare Raare Raare Raare"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaraarre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaraarre"/>
</div>
<div class="lyrico-lyrics-wrapper">Raare Raare Raare Raare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raare Raare Raare Raare"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaraarre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaraarre"/>
</div>
</pre>
