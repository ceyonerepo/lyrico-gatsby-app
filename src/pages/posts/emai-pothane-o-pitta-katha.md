---
title: "emai pothane song lyrics"
album: "O Pitta Katha"
artist: "Pravin Lakkaraju"
lyricist: "Sreejo"
director: "Chendu Muddhu"
path: "/albums/o-pitta-katha-lyrics"
song: "Emai Pothane"
image: ../../images/albumart/o-pitta-katha.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Q1pE5dS_AsU"
type: "love"
singers:
  - Pravin Lakkaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Emaipotaane Manasika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaipotaane Manasika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagela Lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagela Lede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasala Anchulapai Chilipiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasala Anchulapai Chilipiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvadugestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvadugestunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Naa Jagamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Naa Jagamantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni Sagamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sagamantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruga Ledante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruga Ledante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adire Gundela Chuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adire Gundela Chuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KavaliKaastuu Uupirinevalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KavaliKaastuu Uupirinevalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emaipotaane Manasika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaipotaane Manasika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagelaa Lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagelaa Lede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asala Anchulapai Chilipiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asala Anchulapai Chilipiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvadugestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvadugestunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teliyadugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyadugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tega Tondara chesina vayasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tega Tondara chesina vayasuki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalu Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka maatani Palakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka maatani Palakani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavula Yeduruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavula Yeduruga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu Kallu Vaadistunnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Kallu Vaadistunnaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasulu Malli Malli Longipovaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulu Malli Malli Longipovaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leni poni Aatalemito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni poni Aatalemito"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu nannu prema loki laagutunnaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nannu prema loki laagutunnaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emaipotaaane Manasika aagela lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaipotaaane Manasika aagela lede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asala Anchulapai Chilipiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asala Anchulapai Chilipiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvadugestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvadugestunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">galvadhuga okaneme samugunda kodesere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galvadhuga okaneme samugunda kodesere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minaparaka ninuvethege 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minaparaka ninuvethege "/>
</div>
<div class="lyrico-lyrics-wrapper">natalupula aaliserakshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natalupula aaliserakshana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nannakutti arukumake kunatundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannakutti arukumake kunatundi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanneveri villipumake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanneveri villipumake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nillanonne unnetunare tunkichuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nillanonne unnetunare tunkichuru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvve nille ninne untare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve nille ninne untare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emaipotaaane Manasika aagela lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaipotaaane Manasika aagela lede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asala Anchulapai Chilipiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asala Anchulapai Chilipiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvadugestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvadugestunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Naa Jagamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Naa Jagamantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni Sagamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sagamantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veruga Ledante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruga Ledante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adire Gundela Chuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adire Gundela Chuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KavaliKaastuu Uupirinevalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KavaliKaastuu Uupirinevalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emaipotaaane Manasika aagela lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaipotaaane Manasika aagela lede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asala Anchulapai Chilipiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asala Anchulapai Chilipiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvadugestunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvadugestunte"/>
</div>
</pre>
