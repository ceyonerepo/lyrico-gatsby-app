---
title: "ava yaaru song lyrics"
album: "Yaagan"
artist: "Niro Pirabakaran"
lyricist: "Padmavathy"
director: "Vinoth Thangavel"
path: "/albums/yaagan-lyrics"
song: "Ava Yaaru"
image: ../../images/albumart/yaagan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/l-He5asyi5w"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ava Yarunu Thedunenv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yarunu Thedunenv"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaarunu Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaarunu Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulla Vanthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla Vanthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Innamum Pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innamum Pogala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Ooru Per Ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Ooru Per Ethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvume Ketkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvume Ketkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Nenaipe Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Nenaipe Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver Ethuvum Thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Ethuvum Thonala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalkandu Pechil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkandu Pechil Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karachithan Poguraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karachithan Poguraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolkandu Pola Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolkandu Pola Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipottu Poguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipottu Poguraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silvandu Pola Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silvandu Pola Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Solli Poguraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Solli Poguraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolkondu Ponnu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolkondu Ponnu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Thalli Poguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thalli Poguraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Peru Ooru Maranthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peru Ooru Maranthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Thookam Ellam Tholanjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thookam Ellam Tholanjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaaru Enna Therinjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru Enna Therinjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalala Vazhkkai Thirumbiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalala Vazhkkai Thirumbiduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaala Enna Kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaala Enna Kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaala Pola Arachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumpaala Pola Arachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pola Nenja Nenachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pola Nenja Nenachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Thaane Thanthuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Thaane Thanthuputtaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaahaaaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaahaaaaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Yaraunu Thedunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaraunu Thedunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaarunu Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaarunu Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulla Vanthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla Vanthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Innamum Pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innamum Pogala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alamaari Pola Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alamaari Pola Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaipaaiyum Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaipaaiyum Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukkaga Vechirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukkaga Vechirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Oraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Oraththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saramaari Neeyum Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saramaari Neeyum Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkul Pugundhukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkul Pugundhukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaichittu Poniyedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaichittu Poniyedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Thoorathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yaaradi Nee Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yaaradi Nee Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Kooradi Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Kooradi Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aaradi En Aaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aaradi En Aaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaladi Serume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaladi Serume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Ennadi Adi Ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ennadi Adi Ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Punnagai Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Punnagai Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Boomiyai Thaandi Kaalkalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Boomiyai Thaandi Kaalkalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil Pogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Pogume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Peru Ooru Maranthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peru Ooru Maranthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Thookam Ellam Tholanjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thookam Ellam Tholanjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaaru Enna Therinjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru Enna Therinjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalala Vazhkkai Thirumbiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalala Vazhkkai Thirumbiduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaala Enna Kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaala Enna Kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaala Pola Arachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumpaala Pola Arachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pola Nenja Nenachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pola Nenja Nenachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Thaane Thanthuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Thaane Thanthuputtaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalkandu Pechil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkandu Pechil Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karachithan Poguraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karachithan Poguraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolkandu Pola Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolkandu Pola Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipottu Poguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipottu Poguraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silvandu Pola Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silvandu Pola Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Solli Poguraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Solli Poguraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolkondu Ponnu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolkondu Ponnu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Thalli Poguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thalli Poguraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Peru Ooru Maranthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peru Ooru Maranthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Thookam Ellam Tholanjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thookam Ellam Tholanjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaaru Enna Therinjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru Enna Therinjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalala Vazhkkai Thirumbiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalala Vazhkkai Thirumbiduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaala Enna Kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaala Enna Kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaala Pola Arachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumpaala Pola Arachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pola Nenja Nenachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pola Nenja Nenachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Thaane Thanthuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Thaane Thanthuputtaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo Senjiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Senjiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Peru Ooru Maranthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peru Ooru Maranthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Thookam Ellam Tholanjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thookam Ellam Tholanjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yaaru Enna Therinjiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru Enna Therinjiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalala Vazhkkai Thirumbiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalala Vazhkkai Thirumbiduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo Senjiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Senjiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaala Enna Kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaala Enna Kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaala Pola Arachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumpaala Pola Arachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pola Nenja Nenachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pola Nenja Nenachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Thaane Thanthuputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Thaane Thanthuputtaa"/>
</div>
</pre>
