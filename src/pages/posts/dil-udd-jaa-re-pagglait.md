---
title: "dil udd jaa re song lyrics"
album: "Pagglait"
artist: "Arijit Singh"
lyricist: "Neelesh Misra"
director: "Umesh Bist"
path: "/albums/pagglait-lyrics"
song: "Dil Udd Jaa Re"
image: ../../images/albumart/pagglait.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/s8HxMirkEvs"
type: "Mass"
singers:
  - Neeti Mohan
  - Sunny M.R.
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lamha yun dukhta kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha yun dukhta kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun main sau dafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun main sau dafa"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se hun khafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se hun khafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaise puchhun, nikla kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaise puchhun, nikla kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna bewafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna bewafa"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se hun khafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se hun khafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwahishein to karte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwahishein to karte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi se darte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi se darte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Doobte ubarte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doobte ubarte hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toote jo taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toote jo taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Roothe hain saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roothe hain saare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tu udd ja re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tu udd ja re"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasta dikhla re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasta dikhla re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Armaan yeh, gumsum se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Armaan yeh, gumsum se"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahein yeh kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahein yeh kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko kya pata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko kya pata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inmein jo sapne thhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inmein jo sapne thhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun woh laapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun woh laapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko kya pata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko kya pata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwahishein to karte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwahishein to karte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi se darte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi se darte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Doobte ubarte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doobte ubarte hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toote jo taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toote jo taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Roothe hain saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roothe hain saare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil tu udd ja re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil tu udd ja re"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasta dikhla re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasta dikhla re"/>
</div>
</pre>
