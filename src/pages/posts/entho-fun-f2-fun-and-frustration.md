---
title: "entho fun song lyrics"
album: "F2 Fun and Frustration"
artist: "Devi Sri Prasad"
lyricist: "Shreemani"
director: "Anil Ravipudi"
path: "/albums/f2-fun-and-frustration-lyrics"
song: "Entho Fun"
image: ../../images/albumart/f2-fun-and-frustration.jpg
date: 2019-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Pfd52X4a0HY"
type: "happy"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Swargame nelapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargame nelapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalinattu ningilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalinattu ningilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarale chethiloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarale chethiloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaarinattu gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaarinattu gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolavaana kurisinattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolavaana kurisinattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nemalike paatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nemalike paatale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpinattu koyilamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpinattu koyilamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommapai koochipudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommapai koochipudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadinanntu kottha kottha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadinanntu kottha kottha"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaramule puttinattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaramule puttinattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalidasu kavyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalidasu kavyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thyaagaraaya geyamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thyaagaraaya geyamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipi manasu paadinattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipi manasu paadinattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina oohalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina oohalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni aashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni aashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakilantha ompinattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakilantha ompinattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu kallu kalupukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kallu kalupukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalalu panchukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalalu panchukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamantha saagiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamantha saagiponi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohamantha karigipothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohamantha karigipothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahamantha virigipothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahamantha virigipothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramantha cherigiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramantha cherigiponi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathirante kammanaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathirante kammanaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaugilintha pilupani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaugilintha pilupani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellavaarulu melukovadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellavaarulu melukovadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udayamante thiyyanaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udayamante thiyyanaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu melukolupani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu melukolupani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dongalaaga nidrapovadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dongalaaga nidrapovadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rojukokka bottu bille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojukokka bottu bille"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkapeduthu chilipi addham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkapeduthu chilipi addham"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte navve navvuthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte navve navvuthonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Baitikelle vela nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baitikelle vela nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichi icche valapu muddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichi icche valapu muddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayuvedho penchuthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuvedho penchuthondhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intikelle vela antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intikelle vela antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallepoola parimalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallepoola parimalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthu jalli gurthu cheyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthu jalli gurthu cheyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti baita chinnadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti baita chinnadani"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduru choopu kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduru choopu kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha uthsavaanni nimpadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha uthsavaanni nimpadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthi fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthi fun"/>
</div>
</pre>
