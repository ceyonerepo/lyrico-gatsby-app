---
title: "nambaadhe song lyrics"
album: "Khyla"
artist: "Shravan"
lyricist: "Vadivarasu"
director: "Baskar Sinouvassane"
path: "/albums/khyla-lyrics"
song: "Nambaadhe"
image: ../../images/albumart/khyla.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F7DOYalXQfQ"
type: "happy"
singers:
  - Gold Devaraj
  - Shravan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kal malaiyagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal malaiyagum "/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyum kallagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyum kallagum"/>
</div>
<div class="lyrico-lyrics-wrapper">poi nijamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi nijamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamum poiyagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamum poiyagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam kaadagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam kaadagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadum nilamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadum nilamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">athuvum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuvum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">varam saabamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam saabamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">sabamum varamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabamum varamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kasada thabara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasada thabara"/>
</div>
<div class="lyrico-lyrics-wrapper">yarala vazhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarala vazhala"/>
</div>
<div class="lyrico-lyrics-wrapper">naganana namana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naganana namana"/>
</div>
<div class="lyrico-lyrics-wrapper">val lidai mellinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="val lidai mellinam"/>
</div>
<div class="lyrico-lyrics-wrapper">nokka nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nokka nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">ketka ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketka ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">unara unara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unara unara"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi sevi mathiyinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi sevi mathiyinam"/>
</div>
<div class="lyrico-lyrics-wrapper">nambathe edhayum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambathe edhayum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nerungi vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nerungi vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi odum"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">adhaiyum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhaiyum ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">kilai varai than ilaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilai varai than ilaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">keezhe vizhin sarugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keezhe vizhin sarugu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir varai than peyargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir varai than peyargal"/>
</div>
<div class="lyrico-lyrics-wrapper">pirindhu poyin pinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirindhu poyin pinam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal varai than nadhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal varai than nadhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">kalandhu poyin kadale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalandhu poyin kadale"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi varai than kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi varai than kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhu poyin nere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhu poyin nere"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum adhuvai irukathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum adhuvai irukathu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhaiyum adhuvai ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaiyum adhuvai ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalum nijamaai irukadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalum nijamaai irukadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamum nijamai irukathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamum nijamai irukathu"/>
</div>
<div class="lyrico-lyrics-wrapper">o paarkum edhaiyum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o paarkum edhaiyum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">parka parka yematrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parka parka yematrum"/>
</div>
<div class="lyrico-lyrics-wrapper">parkum edhaiyum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkum edhaiyum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">indrin nee netrin veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrin nee netrin veru"/>
</div>
<div class="lyrico-lyrics-wrapper">edhayum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhayum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">athaiyum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athaiyum ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">edhayum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhayum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum ninaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuvum nambathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuvum nambathe"/>
</div>
<div class="lyrico-lyrics-wrapper">athaiyum ninaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athaiyum ninaikathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siru theekuchi adhu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru theekuchi adhu than"/>
</div>
<div class="lyrico-lyrics-wrapper">perum kattaiye erikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum kattaiye erikum"/>
</div>
<div class="lyrico-lyrics-wrapper">oor anugundu athu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor anugundu athu than"/>
</div>
<div class="lyrico-lyrics-wrapper">pala uyirgalai parikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala uyirgalai parikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakka nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakka nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">kadakka kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadakka kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">edukka edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edukka edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">nada kada eduppinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nada kada eduppinam"/>
</div>
<div class="lyrico-lyrics-wrapper">padikka padikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padikka padikka"/>
</div>
<div class="lyrico-lyrics-wrapper">mudikka mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudikka mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">thudikka thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudikka thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">padi mudi thudipinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi mudi thudipinam"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom pa"/>
</div>
<div class="lyrico-lyrics-wrapper">paka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom pa"/>
</div>
<div class="lyrico-lyrics-wrapper">paka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom pa"/>
</div>
<div class="lyrico-lyrics-wrapper">paka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom pa"/>
</div>
<div class="lyrico-lyrics-wrapper">paka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bam bam"/>
</div>
</pre>
