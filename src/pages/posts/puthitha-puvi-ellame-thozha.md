---
title: "puthitha puvi ellame song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Puthitha Puvi Ellame"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hLRW93eHL9w"
type: "Melody"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puthitha Puvi Ellame Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthitha Puvi Ellame Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthitha Manam Ellame Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthitha Manam Ellame Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Yaakkai Oru Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yaakkai Oru Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Kood Vaazha Vaazhvenna Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kood Vaazha Vaazhvenna Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Koodu Siru Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Koodu Siru Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atharkulle Kuppai Medenna En Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkulle Kuppai Medenna En Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthithaa Puvi Ellamae Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaa Puvi Ellamae Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthithaa Manam Ellamae Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaa Manam Ellamae Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Yen Yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Yosanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethu Illai Unnulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Illai Unnulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannin Ulle Kanavaai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannin Ulle Kanavaai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swasa Paiyil Kaatraa Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasa Paiyil Kaatraa Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyaththil Vaazhum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyaththil Vaazhum Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannathil Panjangal Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathil Panjangal Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannimai Thaane Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannimai Thaane Thunaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thunai Thaane Udan Varum Irudhivarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunai Thaane Udan Varum Irudhivarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogira Pokkil Rekkai Viri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogira Pokkil Rekkai Viri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuppaigal Ellam Thooki Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaigal Ellam Thooki Yeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Paarthu Konjam Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Paarthu Konjam Siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthitha Puvi Ellame Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthitha Puvi Ellame Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthitha Manam Ellame Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthitha Manam Ellame Puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Yaakkai Oru Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yaakkai Oru Vaazhkkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Kood Vaazha Vaazhvenna Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kood Vaazha Vaazhvenna Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Koodu Siru Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Koodu Siru Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atharkullae Kuppai Medenna En Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharkullae Kuppai Medenna En Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Ellame Puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Ellame Puthitha"/>
</div>
</pre>
