---
title: "oru malayoram song lyrics"
album: "Avan Ivan"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bala"
path: "/albums/avan-ivan-lyrics"
song: "Oru Malayoram"
image: ../../images/albumart/avan-ivan.jpg
date: 2011-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HW0pdIlMUCQ"
type: "melody"
singers:
  - Vijay Yesudas
  - Priyanka
  - Srinisha Jayaseelan
  - Nithyashree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru malaiyoaram anagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru malaiyoaram anagam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhan adivaaram oru veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhan adivaaram oru veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaikoarthu en thalai saaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaikoarthu en thalai saaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">angu vendumadaa en koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu vendumadaa en koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">chellam konjam konji neeppesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellam konjam konji neeppesa"/>
</div>
<div class="lyrico-lyrics-wrapper">ullammarugi naan ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullammarugi naan ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">andha nimidam poadhum madaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha nimidam poadhum madaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha jenmam theerum madaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha jenmam theerum madaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru malaiyoaram anagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru malaiyoaram anagam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhan adivaaram oru veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhan adivaaram oru veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaikoarthu en thalai saaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaikoarthu en thalai saaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">angu vendumadaa en koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu vendumadaa en koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penney mudhal murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penney mudhal murai"/>
</div>
<div class="lyrico-lyrics-wrapper">un arugiley vaa zhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un arugiley vaa zhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">poadhum vidu un ninaiviley koaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poadhum vidu un ninaiviley koaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaanadhu endhan nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanadhu endhan nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">yen indha maatramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen indha maatramo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaanadhu naanam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaanadhu naanam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">than velaiyai kaattumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than velaiyai kaattumo"/>
</div>
<div class="lyrico-lyrics-wrapper">edhiriley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhiriley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvumey pesida vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvumey pesida vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">mounangal aayiram pesumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounangal aayiram pesumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En oli irandhadhey pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En oli irandhadhey pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">innum ennathaan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum ennathaan pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha mayakkam poadhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha mayakkam poadhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">innum nerukkam vendummadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum nerukkam vendummadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru malaiyoaram anagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru malaiyoaram anagam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhan adivaaram oru veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhan adivaaram oru veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaikoarthu en thalai saaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaikoarthu en thalai saaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">angu vendumadaa en koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu vendumadaa en koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai kanumvarai naan kanaviley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kanumvarai naan kanaviley"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kanden penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kanden penney"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaiviley vaazhndhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaiviley vaazhndhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thanimaiyin Oram vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanimaiyin Oram vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">inimaigal oottinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimaigal oottinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thaayidam pesumboadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thaayidam pesumboadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">verumaiyai koottinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verumaiyai koottinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaadhaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaadhaliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamadhu pugaiyilai poaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamadhu pugaiyilai poaley"/>
</div>
<div class="lyrico-lyrics-wrapper">maraindhadhu yaarumey illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraindhadhu yaarumey illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennulley serndhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulley serndhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">engey enai naan maraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey enai naan maraikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha vaarthai poadhummadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vaarthai poadhummadi"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan vaazhkkai maarummadi penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan vaazhkkai maarummadi penney"/>
</div>
</pre>
