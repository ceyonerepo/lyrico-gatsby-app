---
title: "oyo song lyrics"
album: "Mr And Miss"
artist: "Yashwanth Nag"
lyricist: "Pavan Rachepalli"
director: "T. Ashok Kumar Reddy"
path: "/albums/mr-and-miss-lyrics"
song: "OYO - Iddharikokarante Okarishtam Antu"
image: ../../images/albumart/mr-and-miss.jpg
date: 2021-01-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ClRAM2MNXxQ"
type: "happy"
singers:
  - Mama Sing
  - yashwanth nag
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Mister Hey Miss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mister Hey Miss"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharikokarante Okarishtam Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharikokarante Okarishtam Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakenake Thirigesthuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakenake Thirigesthuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisunnappudu Kissulatho Rojanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisunnappudu Kissulatho Rojanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Batting Chesthuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batting Chesthuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Okati Rendu Lekke Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okati Rendu Lekke Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gantalu Gantalu Gadipesthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gantalu Gantalu Gadipesthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Raathri Thedaa Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Raathri Thedaa Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vichhalavidigaa Thirigesthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vichhalavidigaa Thirigesthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa Ante Thandhaanaa Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Ante Thandhaanaa Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida Thom Thom Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida Thom Thom Antaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarle Ani Vinukuntu Pothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarle Ani Vinukuntu Pothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Antaaru Love-Rantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Antaaru Love-Rantaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Huggantaaru Kissantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huggantaaru Kissantaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dating Antu Chatting Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dating Antu Chatting Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intlo Evaru Lerantaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intlo Evaru Lerantaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Incase Aa Plan Biscuit Ayithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Incase Aa Plan Biscuit Ayithe"/>
</div>
<div class="lyrico-lyrics-wrapper">OYO ROOM BOOK Chesukuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OYO ROOM BOOK Chesukuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Walk Antaaru Bike Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walk Antaaru Bike Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantha Vaare Untaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantha Vaare Untaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppallone Thappulu Chesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppallone Thappulu Chesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Camera Kantiki Chikkesthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Camera Kantiki Chikkesthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutteleni Chavatallaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutteleni Chavatallaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Liftllonu Kaanisthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liftllonu Kaanisthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Barullonu Bussullonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barullonu Bussullonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moothulake Pani Cheputhuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moothulake Pani Cheputhuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagesthaaru Chindhesthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagesthaaru Chindhesthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">THaaginanthaa Kakkesthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="THaaginanthaa Kakkesthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss Avuthaaru Plus Avuthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss Avuthaaru Plus Avuthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikalannee Karigisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikalannee Karigisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindhunnaamo Meedhunnaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindhunnaamo Meedhunnaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Lonunnaamo Bayatunnaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lonunnaamo Bayatunnaamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Epudu Ekada Chappudu Cheyyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudu Ekada Chappudu Cheyyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadikevadu Dhorikesthaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadikevadu Dhorikesthaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre Arre Maava Ee Premanthaa Oo Draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Arre Maava Ee Premanthaa Oo Draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminaavo Maavaa Naakipoddhi Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminaavo Maavaa Naakipoddhi Janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre Arre Maava Ee Premanthaa Oo Draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Arre Maava Ee Premanthaa Oo Draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminaavo Maavaa Naakipoddhi Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminaavo Maavaa Naakipoddhi Janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chethullo Veelli Puvvisthuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethullo Veelli Puvvisthuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothallo Veellu Kavvisthuntaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothallo Veellu Kavvisthuntaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulne Raddhantu Vedi Muddhullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulne Raddhantu Vedi Muddhullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Guddhullo Theli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guddhullo Theli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalise Pantaaru Malli Break-Up Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise Pantaaru Malli Break-Up Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Premedhi Sodharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premedhi Sodharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">WhatsApp Chatting Lo Undhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="WhatsApp Chatting Lo Undhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premedhi Sodharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premedhi Sodharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenamma Dinner Lo Undhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenamma Dinner Lo Undhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Premantu Chukkalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premantu Chukkalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Choopisthuntaaru Kanakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopisthuntaaru Kanakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maataltho Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataltho Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Muripisthuntaaru Vinakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muripisthuntaaru Vinakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You Too Anamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Too Anamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Torture Pedathaaru Anakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture Pedathaaru Anakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaku Anaku Vinaku Vinaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaku Anaku Vinaku Vinaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaku Kanaku Chedakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaku Kanaku Chedakuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre Arre Maava Ee Premanthaa Oo Draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Arre Maava Ee Premanthaa Oo Draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminaavo Maavaa Naakipoddhi Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminaavo Maavaa Naakipoddhi Janma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre Arre Maava Ee Premanthaa Oo Draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Arre Maava Ee Premanthaa Oo Draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminaavo Maavaa Naakipoddhi Janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminaavo Maavaa Naakipoddhi Janma"/>
</div>
</pre>
