---
title: "one two one two dance song lyrics"
album: "Hello Charlie"
artist: "Tanishk Bagchi"
lyricist: "Vayu"
director: "Pankaj Saraswat"
path: "/albums/hello-charlie-lyrics"
song: "One Two One Two Dance"
image: ../../images/albumart/hello-charlie.jpg
date: 2021-04-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/pdPkFWAVa5Q"
type: "Entertainment"
singers:
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Toto ke left right ek
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toto ke left right ek"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil pe na indicater
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil pe na indicater"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevan ke ringtone pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan ke ringtone pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Body iski vibrator
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body iski vibrator"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandhiyan chale to jahdiyan hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandhiyan chale to jahdiyan hile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tel mein tale to jaise puriyan hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tel mein tale to jaise puriyan hile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tunak tunak tun bajti koyi jo dhun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tunak tunak tun bajti koyi jo dhun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharr tharr antaraatma hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharr tharr antaraatma hile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandhiyan chale to jahdiyan hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandhiyan chale to jahdiyan hile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tel mein tale to jaise puriyan hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tel mein tale to jaise puriyan hile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tunak tunak tun bajti koyi jo dhun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tunak tunak tun bajti koyi jo dhun"/>
</div>
<div class="lyrico-lyrics-wrapper">Thar thar antaraatma hile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thar thar antaraatma hile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beat baje ya baange phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat baje ya baange phir bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu to sabse aage phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu to sabse aage phir bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se lagbagh michael jackson
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se lagbagh michael jackson"/>
</div>
<div class="lyrico-lyrics-wrapper">Log saboot to maange phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Log saboot to maange phir bhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One two one two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One two one two dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two dance"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance, dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance, dance"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Center mein aaja raja mere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Center mein aaja raja mere"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikhlaade andar kya hai tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikhlaade andar kya hai tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya mein kalandar hai bathere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya mein kalandar hai bathere"/>
</div>
<div class="lyrico-lyrics-wrapper">Par tu ik number mere shere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par tu ik number mere shere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beat baje ya baange phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat baje ya baange phir bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu to sabse aage phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu to sabse aage phir bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se lagbagh michael jackson
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se lagbagh michael jackson"/>
</div>
<div class="lyrico-lyrics-wrapper">Log saboot to maange phir bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Log saboot to maange phir bhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One two one two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One two one two dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two dance"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance, dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance, dance"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara kamar hilaake, dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara kamar hilaake, dance"/>
</div>
<div class="lyrico-lyrics-wrapper">One two one two dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two one two dance"/>
</div>
</pre>
