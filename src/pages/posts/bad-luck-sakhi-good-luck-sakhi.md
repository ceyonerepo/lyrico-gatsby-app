---
title: "bad luck sakhi song lyrics"
album: "Good Luck Sakhi"
artist: "Devi Sri Prasad"
lyricist: "Shree Mani"
director: "Nagesh Kukunoor"
path: "/albums/good-luck-sakhi-lyrics"
song: "Bad Luck Sakhi"
image: ../../images/albumart/good-luck-sakhi.jpg
date: 2021-12-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Vs186rr4trU"
type: "happy"
singers:
  - Haripriya
  - Sameera Bharadwaj
  - M.L.R. Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raave raave sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave raave sakhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Murise mucchatlaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murise mucchatlaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarada sayyataki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarada sayyataki"/>
</div>
<div class="lyrico-lyrics-wrapper">Taki taki taki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taki taki taki"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkennaalle sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkennaalle sakhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pappannaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pappannaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thwaraga o maganiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thwaraga o maganiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aipovate sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aipovate sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mukkukila thaadesevadevade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mukkukila thaadesevadevade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pakkaki ilaga imkeppudu vasthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pakkaki ilaga imkeppudu vasthade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Luck ye look ye vesi lakumuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck ye look ye vesi lakumuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oggesinde ninne chekumuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oggesinde ninne chekumuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkesaave ila chivariki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkesaave ila chivariki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve bad luck ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve bad luck ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rojulu rojulu edhure choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojulu rojulu edhure choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasina ee gaajulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasina ee gaajulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Em chebuthaave inkepudante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em chebuthaave inkepudante"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee laggam gadiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee laggam gadiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatiki deniki ee godava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatiki deniki ee godava"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu thatte ee thadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu thatte ee thadava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thilakam diddhi hangulu addhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thilakam diddhi hangulu addhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Musthabamma ilaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musthabamma ilaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupasalundha ninu andhanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupasalundha ninu andhanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaarinche paniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaarinche paniki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthandhaaniki singaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthandhaaniki singaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalavasarama mee saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalavasarama mee saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Javaabule ala visaraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Javaabule ala visaraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Navaabula ila thiragaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navaabula ila thiragaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naseebune chalo manchiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naseebune chalo manchiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Marche dhaaretiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marche dhaaretiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapendehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapendehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck le gicku le naa pelliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck le gicku le naa pelliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalle ellandi mee illaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalle ellandi mee illaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee adhrusthaalani nammanu nenasale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee adhrusthaalani nammanu nenasale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee dhrushtini minchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee dhrushtini minchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishte ledhasale abbo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishte ledhasale abbo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poddhuna podhhuna nuv lesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhuna podhhuna nuv lesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jwaramosthaade suryudiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwaramosthaade suryudiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhu thirugudu puvvulu kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhu thirugudu puvvulu kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala vakchesthayilaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala vakchesthayilaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvedhuraina neekedhuraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvedhuraina neekedhuraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodindhe ika aallaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodindhe ika aallaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Laabham dhandinga unnollaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laabham dhandinga unnollaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakatte aakhariki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakatte aakhariki"/>
</div>
<div class="lyrico-lyrics-wrapper">Perigi perigi nee keerthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perigi perigi nee keerthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakindhe pakkoollaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakindhe pakkoollaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenno yenno maaruthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenno yenno maaruthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarpedhe nee raathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarpedhe nee raathaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigo vasthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigo vasthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad luck sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad luck sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegabadathaarendhi yegathaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegabadathaarendhi yegathaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chicki chick chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chicki chick chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chick chiki chiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chick chiki chiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalam veyyandi mee nollaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam veyyandi mee nollaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee koothalatho naakassalu yem panila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee koothalatho naakassalu yem panila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa raathanila hahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa raathanila hahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene raasesthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene raasesthaale"/>
</div>
</pre>
