---
title: "dosti song lyrics"
album: "RRR Telugu"
artist: "Maragathamani"
lyricist: "Sirivennela Seetharama Sastry "
director: "S.S. Rajamouli "
path: "/albums/rrr-telugu-lyrics"
song: "Dosti"
image: ../../images/albumart/rrr-telugu.jpg
date: 2022-03-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VPT_EIo89cc"
type: "Friendship"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puliki Vilukadiki Thalaki Uri Thaduki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliki Vilukadiki Thalaki Uri Thaduki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhile Karchichhuki Kasire Padagallaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile Karchichhuki Kasire Padagallaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravikimeghaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravikimeghaniki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dosti Dosti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosti Dosti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohinchani Chitra Vichitram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohinchani Chitra Vichitram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Snehaniki Chachina Hastham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehaniki Chachina Hastham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prananiki Pranam Isthundo Theesthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prananiki Pranam Isthundo Theesthundho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhamdhana Dhamdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamdhana Dhamdham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padapatiki Jadivaanaki Dosti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapatiki Jadivaanaki Dosti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhiraathaki Edhureedhani Dosti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiraathaki Edhureedhani Dosti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penu Jwalaki Himanagamichhina Kougili Ee Dosti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penu Jwalaki Himanagamichhina Kougili Ee Dosti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharadhamdhan Dhamdhara Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharadhamdhan Dhamdhara Dham Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhamdhana Dhamdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamdhana Dhamdham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anukoni Gaali Dhumaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoni Gaali Dhumaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheripindhi Iruvuri Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripindhi Iruvuri Dhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Untara Ika Pai Ilaga Vairame Burivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untara Ika Pai Ilaga Vairame Burivai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadichedhi Okate Daarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadichedhi Okate Daarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethikedhi Matram Verai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikedhi Matram Verai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thegipodha Edho Kshanana Snehamai Drohamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipodha Edho Kshanana Snehamai Drohamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Thondhara Padi Padi Urukaletthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Thondhara Padi Padi Urukaletthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppena Parugula Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppena Parugula Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhuga Teliyadhu Edhuru Vachhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhuga Teliyadhu Edhuru Vachhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappani Malupule Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappani Malupule Hoo"/>
</div>
</pre>
