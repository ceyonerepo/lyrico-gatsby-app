---
title: "ninne naa song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Ninne Naa Ninne"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X76thEw7iP8"
type: "melody"
singers:
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninne Naa Ninne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Naa Ninne "/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikindi Naa Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikindi Naa Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nee Nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nee Nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">Marichave Naathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichave Naathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasth Pothunnadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasth Pothunnadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Roju Suridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Roju Suridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Testhadani Chusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Testhadani Chusthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinipinche Prathi Maata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinipinche Prathi Maata "/>
</div>
<div class="lyrico-lyrics-wrapper">Sadi Chese Prathi Paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadi Chese Prathi Paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Usemonani Vintunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Usemonani Vintunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Naa Ninne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Naa Ninne "/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikindi Naa Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikindi Naa Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nee Nanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nee Nanne "/>
</div>
<div class="lyrico-lyrics-wrapper">Marichave Naathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichave Naathone"/>
</div>
</pre>
