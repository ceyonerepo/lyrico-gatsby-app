---
title: "milaa yun song lyrics"
album: "Haseen Dillruba"
artist: "Amit Trivedi"
lyricist: "Kshitij Patwardhan"
director: "Vinil Mathew"
path: "/albums/haseen-dillruba-lyrics"
song: "Milaa Yun"
image: ../../images/albumart/haseen-dillruba.jpg
date: 2021-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/bRbDEwhNOyA"
type: "love"
singers:
  - Yashita Sharma
  - Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jo Beeta Hai Woh Kal Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Beeta Hai Woh Kal Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Jo Naya Pal Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Jo Naya Pal Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Tham Le Ise Zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Tham Le Ise Zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheere Dheere Sang Tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheere Dheere Sang Tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Din Yeh Sambhalne Laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din Yeh Sambhalne Laga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo Sapne Sambhale Ab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Sapne Sambhale Ab"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere Hawale Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere Hawale Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Dekhle Inhein Zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Dekhle Inhein Zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bandhi Saansein Is Tarah Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhi Saansein Is Tarah Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Mausam Badalne Laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Mausam Badalne Laga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mila Yun Jaise Bichda Na Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mila Yun Jaise Bichda Na Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Milaa Yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Milaa Yun"/>
</div>
<div class="lyrico-lyrics-wrapper">Khila Yun Jaise Vigda Na Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khila Yun Jaise Vigda Na Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Khila Yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khila Yun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Hai Tu Mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Hai Tu Mera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Teri Khushbu Liye Ab Mehka Karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Teri Khushbu Liye Ab Mehka Karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Aankhon Se Baatein Karun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Aankhon Se Baatein Karun"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri Saanson Mein Dhadkan Apni Sunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri Saanson Mein Dhadkan Apni Sunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee Uthoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee Uthoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meri Lakeere Ab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri Lakeere Ab"/>
</div>
<div class="lyrico-lyrics-wrapper">Hain Tere Hathon Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hain Tere Hathon Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban Jaaun Saya Tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban Jaaun Saya Tera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muskurane Ka Bahana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muskurane Ka Bahana"/>
</div>
<div class="lyrico-lyrics-wrapper">De Du Tujhe Naya Naya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De Du Tujhe Naya Naya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Milaa Yun Jaise Bichda Na Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Milaa Yun Jaise Bichda Na Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mila Yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mila Yun"/>
</div>
<div class="lyrico-lyrics-wrapper">Khila Yun Jaise Vigda Na Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khila Yun Jaise Vigda Na Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Khila Yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khila Yun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera Hai Tu Mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Hai Tu Mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Hai Tu Mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Hai Tu Mera"/>
</div>
</pre>
