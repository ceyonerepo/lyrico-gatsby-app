---
title: "thaane mounam song lyrics"
album: "Kilometers and Kilometers"
artist: "Sooraj S. Kurup"
lyricist: "	Vinayak Sasikumar - Nisha Nair"
director: "Jeo Baby"
path: "/albums/kilometers-and kilometers-lyrics"
song: "Thaane Mounam"
image: ../../images/albumart/kilometers-and-kilometers.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/Z9E_tpMZHaU"
type: "happy"
singers:
  - Sooraj S. Kurup
  - Yadu Krishnan K
  - Neethu Naduvathettu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh let them gaily come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh let them gaily come"/>
</div>
<div class="lyrico-lyrics-wrapper">Let them gaily come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let them gaily come"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaane maunam pularnnuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaane maunam pularnnuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">marakkaan manase mozhinjuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakkaan manase mozhinjuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">paate njaaninnerinjuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paate njaaninnerinjuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">kithappaarnnu shvaasam thengiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kithappaarnnu shvaasam thengiyo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kyse me bathaavu dilko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyse me bathaavu dilko "/>
</div>
<div class="lyrico-lyrics-wrapper">duriyaa milaa le hamko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duriyaa milaa le hamko "/>
</div>
<div class="lyrico-lyrics-wrapper">maarkar vo raasthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarkar vo raasthe "/>
</div>
<div class="lyrico-lyrics-wrapper">bathaa rahee he aarsu   
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathaa rahee he aarsu   "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaane maunam pularnnuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaane maunam pularnnuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">marakkaan manase mozhinjuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakkaan manase mozhinjuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">paate njaaninnerinjuvo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paate njaaninnerinjuvo "/>
</div>
<div class="lyrico-lyrics-wrapper">kithappaarnnu shvaasam thengiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kithappaarnnu shvaasam thengiyo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naam katanna paathayoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam katanna paathayoram"/>
</div>
<div class="lyrico-lyrics-wrapper">inniruttu veena neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inniruttu veena neram"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal pathinja mannu pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal pathinja mannu pole"/>
</div>
<div class="lyrico-lyrics-wrapper">ullilennumormmayaayu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullilennumormmayaayu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenchakam thiranjathaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenchakam thiranjathaare"/>
</div>
<div class="lyrico-lyrics-wrapper">kankavarnnu maanjathaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kankavarnnu maanjathaara"/>
</div>
<div class="lyrico-lyrics-wrapper">mennakam niranja ven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mennakam niranja ven"/>
</div>
<div class="lyrico-lyrics-wrapper">katal katannu maayave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katal katannu maayave"/>
</div>
</pre>
