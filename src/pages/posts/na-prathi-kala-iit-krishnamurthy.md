---
title: "na prathi kala song lyrics"
album: "IIT Krishnamurthy"
artist: "Naresh Kumaran"
lyricist: "Ramanjhaneyulu Sankarpu"
director: "Sree Vardhan"
path: "/albums/iit-krishnamurthy-lyrics"
song: "Na Prathi Kala"
image: ../../images/albumart/iit-krishnamurthy.jpg
date: 2020-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NfHJlkj3G4k"
type: "love"
singers:
  - Sreerama Chandra
  - Harini Ivaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa Prathi Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prathi Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Valle Nijame Ilaa Elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Valle Nijame Ilaa Elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhaalane Dhaate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhaalane Dhaate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvula Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvula Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Cheshaavo Naalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Cheshaavo Naalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Modhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Modhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Emantaavo Badhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emantaavo Badhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Rangullo Nede Thadisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Rangullo Nede Thadisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vallegaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vallegaa Ilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kallallo Nuvvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallo Nuvvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo Nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo Nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppa Moosi Kaalamlonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppa Moosi Kaalamlonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Kaaleve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Kaaleve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Alaa Egase Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Alaa Egase Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Naake Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Naake Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Chese Manthram Veshaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese Manthram Veshaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kallallo Nuvvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallo Nuvvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo Nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo Nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppa Moosi Kaalamlonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppa Moosi Kaalamlonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Kaaleve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Kaaleve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Alaa Egase Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Alaa Egase Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Naake Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Naake Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Chese Manthram Veshaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese Manthram Veshaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evarevaro Edhurupade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarevaro Edhurupade"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Etupoye Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Etupoye Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuledhuta Okarele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuledhuta Okarele"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Itu Etu Choopu Padina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Itu Etu Choopu Padina"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalaa Undhi Ee Kshanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalaa Undhi Ee Kshanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Nammani Nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nammani Nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilaa Nuvvu Cheradam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilaa Nuvvu Cheradam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhalo Sadavvadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhalo Sadavvadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vallegaa Preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vallegaa Preme"/>
</div>
<div class="lyrico-lyrics-wrapper">Naapai Valavesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapai Valavesene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Bandhinchesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Bandhinchesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhinchindhe Neeke Naa Cheyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhinchindhe Neeke Naa Cheyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Haayiki Kolathedhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Haayiki Kolathedhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Dhaarullona Preme 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Dhaarullona Preme "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheri Poole Challele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheri Poole Challele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Kallallo Nuvvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallo Nuvvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalo Nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalo Nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppa Moosi Kaalamlonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppa Moosi Kaalamlonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Kaaleve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Kaaleve"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Alaa Egase Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Alaa Egase Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Naake Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Naake Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Chese Manthram Veshaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese Manthram Veshaave"/>
</div>
</pre>
