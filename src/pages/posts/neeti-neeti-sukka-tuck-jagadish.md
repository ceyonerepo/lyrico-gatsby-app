---
title: "neeti neeti sukka song lyrics"
album: "Tuck Jagadish"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthy"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Neeti Neeti Sukka"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/v9YS0ccMsgg"
type: "happy"
singers:
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeti neeti sukka neelala sukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti neeti sukka neelala sukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabaadi kuravaali neerendayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabaadi kuravaali neerendayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Varinaaru gutthanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varinaaru gutthanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthetthi koose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthetthi koose"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootugaa pandithe putamesi senu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootugaa pandithe putamesi senu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhakaapu ichhenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhakaapu ichhenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariputla vodlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariputla vodlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Korukonchi suseti kottha alivelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korukonchi suseti kottha alivelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maagaadi dhunneti monagaadu evare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maagaadi dhunneti monagaadu evare"/>
</div>
<div class="lyrico-lyrics-wrapper">Garigolla pilagaade ganamaina vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garigolla pilagaade ganamaina vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittayya kanikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittayya kanikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">O gollabaama egadhanni nilusthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O gollabaama egadhanni nilusthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvetthu kanki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvetthu kanki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumonchi veseti naaru vallanki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumonchi veseti naaru vallanki"/>
</div>
</pre>
