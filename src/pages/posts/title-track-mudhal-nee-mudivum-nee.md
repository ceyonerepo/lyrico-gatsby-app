---
title: "title track song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Thamarai"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Title Track"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/btWnZxF-Hck"
type: "title track"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal nee mudivum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal nee mudivum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Moondru kaalam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondru kaalam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal nee karaiyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal nee karaiyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru kooda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kooda nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhoram oru kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhoram oru kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennadha naal illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennadha naal illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaga naanum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaga naanum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyengum pala bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyengum pala bimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil naan saaya thozh illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil naan saaya thozh illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pola yaarum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pola yaarum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera nadhi neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera nadhi neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhaamal naan moozhgi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhaamal naan moozhgi ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanadi vaanil madhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanadi vaanil madhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyalla naan thaanae theindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyalla naan thaanae theindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi kaanagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kaanagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kaanamal ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kaanamal ponavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paavai kaal thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paavai kaal thadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai thedamal theinthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai thedamal theinthavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha baaram en nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha baaram en nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai illa naan andrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai illa naan andrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal ellaam pogum aanalum naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal ellaam pogum aanalum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir illadha udalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir illadha udalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal nee mudivum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal nee mudivum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Moondru kaalam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondru kaalam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal nee karaiyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal nee karaiyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru kooda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kooda nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoora desathil thozhainthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora desathil thozhainthaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmani"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thedi kandathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thedi kandathum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannellaam minnmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannellaam minnmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinnokki kaalam pogum enil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnokki kaalam pogum enil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mannippai kooruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mannippai kooruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannokki nerai paarkkum kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannokki nerai paarkkum kanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai ellaamae kalaiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai ellaamae kalaiven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oo hoo oo hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo oo hoo oo hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal nee mudivum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal nee mudivum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Moondru kaalam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondru kaalam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal nee karaiyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal nee karaiyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru kooda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kooda nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagaradha kadigaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaradha kadigaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pol naanum nindrirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pol naanum nindrirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee engu sendraai kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee engu sendraai kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana aridhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana aridhaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Velipaarvaikku poosi konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velipaarvaikku poosi konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaigaikku podhum kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnaigaikku podhum kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ketkavae en padalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketkavae en padalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aasai raagathil seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aasai raagathil seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un punnagai pon minnalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un punnagai pon minnalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan korthu aangaangu neidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan korthu aangaangu neidhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal nee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal nee eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivum nee eeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivum nee eeee"/>
</div>
</pre>
