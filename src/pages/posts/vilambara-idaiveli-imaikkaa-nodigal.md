---
title: "vilambara idaiveli song lyrics"
album: "Imaikkaa Nodigal"
artist: "Hiphop Tamizha"
lyricist: "Kabilan Vairamuthu"
director: "R. Ajay Gnanamuthu"
path: "/albums/imaikkaa-nodigal-lyrics"
song: "Vilambara Idaiveli"
image: ../../images/albumart/imaikkaa-nodigal.jpg
date: 2018-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dsrku40uZMc"
type: "love"
singers:
  - Christopher Stanley
  - Sudarshan Ashok
  - Srinisha Jayaseelan
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oli illaa Un Mozhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli illaa Un Mozhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Thedum En Vizhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Thedum En Vizhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikkaatha Nam Nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkaatha Nam Nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigaara Thean Thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaara Thean Thuligal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vaayaara Un Kadhal Nee Solladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaayaara Un Kadhal Nee Solladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraatha Nadippellaam Vendamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraatha Nadippellaam Vendamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnanjal Kuruncheithi Anuppaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnanjal Kuruncheithi Anuppaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Munne Unthan Ennam Kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Munne Unthan Ennam Kooradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilambara Idaiveli Maalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilambara Idaiveli Maalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thirumugam Thirakkindra Velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thirumugam Thirakkindra Velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niramattra Idhayathil Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niramattra Idhayathil Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Enna Nilai Unthan Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Enna Nilai Unthan Manathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unathey Adi Nee Enathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unathey Adi Nee Enathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamal Naanum Theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamal Naanum Theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">illai Endre Sonnaal Indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai Endre Sonnaal Indre"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moga Paarvai Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moga Paarvai Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Poovai Naan Yetru Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Poovai Naan Yetru Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaththiruppu Niraivaagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaththiruppu Niraivaagume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththiruppu Athu Theernthuvittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththiruppu Athu Theernthuvittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaal Thadangal Avai Thisai Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaal Thadangal Avai Thisai Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivalin Kanavo Ulle Oliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalin Kanavo Ulle Oliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum idhayam Vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum idhayam Vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum Kanavu Idhazhai Adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyum Kanavu Idhazhai Adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Kaatchiyil Athu Vaarthai Aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Kaatchiyil Athu Vaarthai Aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilambara Idaiveli Maalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilambara Idaiveli Maalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thirumugam Thirakkindra Velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thirumugam Thirakkindra Velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niramattra Idhayathil Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niramattra Idhayathil Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Enna Nilai Unthan Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Enna Nilai Unthan Manathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilam Ellaam Un Thadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Ellaam Un Thadame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavellaam Un Padame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavellaam Un Padame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamellaam Un Nirame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamellaam Un Nirame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivellaam Un Nayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivellaam Un Nayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathuram Konjum ilangan Neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathuram Konjum ilangan Neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathame illaa iraivan Neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathame illaa iraivan Neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayathai Kadikkum Kuzhanthai Neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathai Kadikkum Kuzhanthai Neeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varambu Meeralo Ennai Thodarum Thooralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varambu Meeralo Ennai Thodarum Thooralo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unathey Adi Nee Enathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unathey Adi Nee Enathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamale Naanum Theiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamale Naanum Theiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">illai Endre Sonnaal Indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai Endre Sonnaal Indre"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moga Paarvai Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moga Paarvai Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unathey Adi Nee Enathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unathey Adi Nee Enathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamale Naan Theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamale Naan Theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">illai Endre Sonnaal Indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai Endre Sonnaal Indre"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moga Paarvai Naan Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moga Paarvai Naan Mooduven"/>
</div>
</pre>
