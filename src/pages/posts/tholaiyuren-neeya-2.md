---
title: "tholaiyuren song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Bhavan Mitthra"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Tholaiyuren"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Id7hxLSfp5I"
type: "love"
singers:
  - Shabir
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi konda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi konda nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukulla inippathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla inippathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu unnai naadi vanthuchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu unnai naadi vanthuchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam mella thudippathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam mella thudippathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru ennai thedi vanthuchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru ennai thedi vanthuchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh…oooo hoo hoo mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh…oooo hoo hoo mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi konda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi konda nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam vacha idangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam vacha idangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham vecha idam ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham vecha idam ethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla ninaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla ninaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un…udaloda soottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un…udaloda soottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu iravum kazhipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu iravum kazhipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam poora sooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam poora sooradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul theeya vechitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul theeya vechitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai meeri oodaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai meeri oodaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai yetho senjitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yetho senjitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kondu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kondu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazha vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazha vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam neeya kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neeya kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muppozhuthum unnudaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppozhuthum unnudaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanaiyil tholainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanaiyil tholainjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham indri unnazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham indri unnazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegathula karainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegathula karainjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uraiyaada poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uraiyaada poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru udhadugalaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru udhadugalaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollum pechu meerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum pechu meerama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kaalaai nadappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kaalaai nadappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullathaachi naanagi unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullathaachi naanagi unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal sumappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal sumappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum neeyum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhezhu jenmam koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhezhu jenmam koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvom pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvom pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukulla inippathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla inippathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu unnai naadi vanthuchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu unnai naadi vanthuchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm…mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm…mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam mella thudippathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam mella thudippathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru ennai thedi vanthuchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru ennai thedi vanthuchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh oo hoo hoommm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh oo hoo hoommm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaiyuren tholaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuren tholaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholanjae poren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholanjae poren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thedi kondaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thedi kondaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaaaa aa"/>
</div>
</pre>
