---
title: "yeduruga ranamunna song lyrics"
album: "Oka Chinna Viramam"
artist: "Bharath Manchiraju"
lyricist: "Ganesh Salaadi"
director: "Sundeep Cheguri"
path: "/albums/oka-chinna-viramam-lyrics"
song: "Yeduruga Ranamunna"
image: ../../images/albumart/oka-chinna-viramam.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MNzVTeD9gAg"
type: "melody"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chelikshemama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelikshemama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kushalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kushalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neevv
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevv"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Praname"/>
</div>
<div class="lyrico-lyrics-wrapper">Padilama Emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padilama Emo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthundo Ledo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthundo Ledo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chivariki Brathukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivariki Brathukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduruga Ranamunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduruga Ranamunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaninnechusene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaninnechusene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharecheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharecheekati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velugukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuuve Oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuuve Oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugule Neekai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugule Neekai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Evariki Eesaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evariki Eesaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridi Le Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridi Le Gaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Raali Chukkaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raali Chukkaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Guvvaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Guvvaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyade ningi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyade ningi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethone vvntune unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone vvntune unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neecheyy Iiaandade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neecheyy Iiaandade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedurugaranamunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurugaranamunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaninnechusene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaninnechusene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Evariki Eesaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evariki Eesaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..O…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..O…"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaridi Le Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaridi Le Gaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurugaranamunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurugaranamunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaninnechusene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaninnechusene"/>
</div>
<div class="lyrico-lyrics-wrapper">O..o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O..o…"/>
</div>
</pre>
