---
title: "repati kala song lyrics"
album: "Kousalya Krishnamurthy"
artist: "Dhibu Ninan Thomas"
lyricist: "Ramajogayya Sastry"
director: "Bhimaneni Srinivasa Rao"
path: "/albums/kousalya-krishnamurthy-lyrics"
song: "Repati Kala"
image: ../../images/albumart/kousalya-krishnamurthy.jpg
date: 2019-08-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jUfMQOkxojI"
type: "happy"
singers:
  - Swaraag Keerthan
  - Manisha Errabathini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Repati repati repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati repati repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppalu daatina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalu daatina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalu saachina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu saachina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Egarave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarave alaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo lenidedi ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo lenidedi ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vruthapodu nee tarfeedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vruthapodu nee tarfeedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nuvvu ulivai maluchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nuvvu ulivai maluchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">repati repati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati repati"/>
</div>
<div class="lyrico-lyrics-wrapper">Sraminchande edi raadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sraminchande edi raadu"/>
</div>
<div class="lyrico-lyrics-wrapper">repati repati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati repati"/>
</div>
<div class="lyrico-lyrics-wrapper">Visramiste vijayam ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visramiste vijayam ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">repati repati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati repati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattudalato lakshyam geluchuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattudalato lakshyam geluchuko "/>
</div>
<div class="lyrico-lyrics-wrapper">repati repati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati repati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtame nadipinche indhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtame nadipinche indhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishtagaa kadaladame nee balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishtagaa kadaladame nee balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamainaa viraminchani gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamainaa viraminchani gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliche lakshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliche lakshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupukai pranalaidoopadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupukai pranalaidoopadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Otamini odistu prati kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamini odistu prati kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakramiste needegaa kreeda pranganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakramiste needegaa kreeda pranganam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indaakaa vacchaavu kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indaakaa vacchaavu kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkonchem parledu padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkonchem parledu padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddhamlo gaayaale kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddhamlo gaayaale kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">repati kathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati kathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repati repati repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati repati repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppalu daatina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalu daatina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalu saachina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu saachina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Egarave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarave alaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaa parugulu nerpu neeloni kalalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaa parugulu nerpu neeloni kalalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupani antu ye chota nilavaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupani antu ye chota nilavaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijrumbinchu avakasamodalaku sadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijrumbinchu avakasamodalaku sadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suluvagu gelupu maargaalu vetakaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suluvagu gelupu maargaalu vetakaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi chaalantu ila paine migalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi chaalantu ila paine migalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Avadhulu penchi sikharaala anchuku padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avadhulu penchi sikharaala anchuku padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Noppi badha anni kacchitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noppi badha anni kacchitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippullonaa nadake jeevitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippullonaa nadake jeevitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Savaallenni edurainaa sare suswaagatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaallenni edurainaa sare suswaagatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo kooda undo adbhutam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo kooda undo adbhutam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madinchande raade amrutam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madinchande raade amrutam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaste raani ante leni avarodhaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaste raani ante leni avarodhaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupe abhimatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupe abhimatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indaakaa vacchaavu kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indaakaa vacchaavu kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkonchem parledu padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkonchem parledu padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddhamlo gaayaale kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddhamlo gaayaale kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">repati kathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="repati kathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo lenidedi ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo lenidedi ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vruthapodu nee tarfeedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vruthapodu nee tarfeedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nuvvu ulivai maluchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nuvvu ulivai maluchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Sraminchande edi raadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sraminchande edi raadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Visramiste vijayam ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visramiste vijayam ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattudalato lakshyam geluchuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattudalato lakshyam geluchuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtame nadipinche indhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtame nadipinche indhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishtagaa kadaladame nee balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishtagaa kadaladame nee balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtamainaa viraminchani gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtamainaa viraminchani gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Geliche lakshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geliche lakshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupukai pranalaidoopadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupukai pranalaidoopadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Otamini odistu prati kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamini odistu prati kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakramiste needegaa kreeda pranganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakramiste needegaa kreeda pranganam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repati repati repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati repati repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppalu daatina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalu daatina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalu saachina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu saachina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Egarave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarave alaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repati repati repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati repati repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppalu daatina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalu daatina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalu saachina repati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu saachina repati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Egarave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egarave alaa"/>
</div>
</pre>
