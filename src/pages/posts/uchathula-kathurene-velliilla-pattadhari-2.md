---
title: "uchathula kathurene naanum song lyrics"
album: "Vellaiilla Pattadhari 2"
artist: "Sean Roldan"
lyricist: "Dhanush"
director: "Soundarya Rajinikanth"
path: "/albums/vellaiilla-pattadhari-2-lyrics"
song: "Uchathula Kathurene Naanum"
image: ../../images/albumart/vellaiilla-pattadhari-2.jpg
date: 2017-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Rd1jM8JxhTg"
type: "Love"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Pacha Manam Pichikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pacha Manam Pichikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppa Idhu Aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa Idhu Aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pattadhellam Sollithaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattadhellam Sollithaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathukada Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukada Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raguvaran Konjam Peppy Aana Track Ah Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raguvaran Konjam Peppy Aana Track Ah Irundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bettera Irukumenu Boys Ellam Feel Panranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bettera Irukumenu Boys Ellam Feel Panranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da Dei Oppari Kooda Kuththu Pattula Than Venuma Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da Dei Oppari Kooda Kuththu Pattula Than Venuma Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Well You Know The Reach Is Better
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Well You Know The Reach Is Better"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My God Sari Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God Sari Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ucchathula Kathurene Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucchathula Kathurene Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi Innum Konjam venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi Innum Konjam venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathiyila Nikurene Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiyila Nikurene Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kalyanam Dhaan Kattikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kalyanam Dhaan Kattikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poidume Maanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poidume Maanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaazhkaia Thaan Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaia Thaan Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Vandhuruchu Nyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Vandhuruchu Nyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pacha Manam Pichikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha Manam Pichikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppa Idhu Aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa Idhu Aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pattadhellam Sollithaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattadhellam Sollithaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathukada Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukada Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Is A Danger Da Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Is A Danger Da Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damagu Neeyum Yethuka Dhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damagu Neeyum Yethuka Dhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondatti Vandha Life Eh Oru Coma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondatti Vandha Life Eh Oru Coma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhosam Ellam Aayidum Dream ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosam Ellam Aayidum Dream ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Senjum Podhavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Senjum Podhavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Onnum Puriavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Onnum Puriavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Thanda Seiyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thanda Seiyavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyama Naan Nalla Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Naan Nalla Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sindhadha Kanneer Sindhadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhadha Kanneer Sindhadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Senjalum Thembaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Senjalum Thembaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedatha Nimmadhi Thedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedatha Nimmadhi Thedatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Confirm Ah Mudinjadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Confirm Ah Mudinjadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Eathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eathukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kaalaththil Ava thaan Karpooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kaalaththil Ava thaan Karpooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam Mudinjakka Adhuve Thaan Bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam Mudinjakka Adhuve Thaan Bootham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Pagal kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Pagal kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Ketta Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Ketta Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Kanavu Ketta Kanavu Ketta Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Kanavu Ketta Kanavu Ketta Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchathula Kathurene Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchathula Kathurene Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi Innum Konjam venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi Innum Konjam venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathiyila Nikurene Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiyila Nikurene Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kalyanam Dhaan Kattikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kalyanam Dhaan Kattikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poidume Maanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poidume Maanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaazhkaya Thaan Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaya Thaan Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Vandhuduchu Nyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Vandhuduchu Nyanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pacha Manam Pichikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pacha Manam Pichikichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppa Idhu Aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa Idhu Aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pattadhellam Sollithaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattadhellam Sollithaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathukada Neeum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukada Neeum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam Is A Danger Da Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam Is A Danger Da Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damagu Neeyum Yethuka Dhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damagu Neeyum Yethuka Dhaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondatti Vandha Life Eh Oru Coma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondatti Vandha Life Eh Oru Coma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhosam Ellam Aayidum Dream ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosam Ellam Aayidum Dream ah"/>
</div>
</pre>
