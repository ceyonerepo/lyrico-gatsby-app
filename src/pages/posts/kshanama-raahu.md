---
title: "kshanama song lyrics"
album: "Raahu"
artist: "Praveen Lakkaraju"
lyricist: "Srinivasa Mouli"
director: "Subbu Vedula"
path: "/albums/raahu-lyrics"
song: "Kshanama"
image: ../../images/albumart/raahu.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gQAs-xO2_Jw"
type: "melody"
singers:
  - Anurag Kulkarni
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalla mundhu ninnu choodagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla mundhu ninnu choodagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamanthaa kadhilinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamanthaa kadhilinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha maate yedabaataithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha maate yedabaataithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasanthaa mariginadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasanthaa mariginadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamaa kshanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamaa kshanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gundeloni gaaya madhigoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundeloni gaaya madhigoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamaa kshanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamaa kshanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv korukunna theeraminka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv korukunna theeraminka "/>
</div>
<div class="lyrico-lyrics-wrapper">cherakunda vellamaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherakunda vellamaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo vellamaake Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo vellamaake Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamunu kalaluga thoochelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamunu kalaluga thoochelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho yedhoo maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho yedhoo maaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanabadi dhorakadhu yee needa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanabadi dhorakadhu yee needa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho yedhoo maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho yedhoo maaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve nee choota nuvu leni choota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve nee choota nuvu leni choota"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu leni choota nenendhuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu leni choota nenendhuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanama kshanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanama kshanama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu koru kunna varama dhigoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu koru kunna varama dhigoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanama kshanama Nuv korukunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanama kshanama Nuv korukunna "/>
</div>
<div class="lyrico-lyrics-wrapper">theeraminka cherakunda vellamaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraminka cherakunda vellamaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo vellamaake Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo vellamaake Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raahu valle nannu kaatu vese kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raahu valle nannu kaatu vese kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jnapakale mullu naadhi yenti neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jnapakale mullu naadhi yenti neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundharundhi naaku thoduleni dhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundharundhi naaku thoduleni dhaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvuna vedhana rege
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvuna vedhana rege"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adigaa adigaa adigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigaa adigaa adigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamaa kshanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamaa kshanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mounam dhaatadhu badhuledhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mounam dhaatadhu badhuledhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gundeloni gaaya madhigoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundeloni gaaya madhigoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Migilaa migilaa shilagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migilaa migilaa shilagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamaa kshanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamaa kshanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa repati dhaariki velugedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa repati dhaariki velugedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv korukunna theeraminka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv korukunna theeraminka "/>
</div>
<div class="lyrico-lyrics-wrapper">cherakunda vellamaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherakunda vellamaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
</pre>
