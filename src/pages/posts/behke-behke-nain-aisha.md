---
title: "behke behke song lyrics"
album: "Aisha"
artist: "Amit Trivedi"
lyricist: "Javed Akhtar"
director: "Rajshree Ojha"
path: "/albums/aisha-lyrics"
song: "Behke Behke Nain"
image: ../../images/albumart/aisha.jpg
date: 2010-08-06
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2zVLY4XUNaw"
type: "love"
singers:
  - Anushka Manchanda
  - Samrat Kaushal
  - Raman Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Behke behke nain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Behke behke nain"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheene dil ka chain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheene dil ka chain"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh ye pyar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh ye pyar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi intezar karde bekaraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi intezar karde bekaraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh yeh pyar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh yeh pyar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadein dil ko tadpaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadein dil ko tadpaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saansein ulajti jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saansein ulajti jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhen sapne dikhlaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhen sapne dikhlaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab anjaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab anjaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon se tu ghabraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon se tu ghabraye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaton mein so na paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaton mein so na paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathon ko malta jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathon ko malta jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu deewane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu deewane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hote hote oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hote hote oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jane do na pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jane do na pyar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shh shh shake that booty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shh shh shake that booty"/>
</div>
<div class="lyrico-lyrics-wrapper">Till I wanna get summer back duty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Till I wanna get summer back duty"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody know user queue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody know user queue"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody get little bit movie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody get little bit movie"/>
</div>
<div class="lyrico-lyrics-wrapper">Now your body your revenge got style
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now your body your revenge got style"/>
</div>
<div class="lyrico-lyrics-wrapper">Tell me wonder in the about all dua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell me wonder in the about all dua"/>
</div>
<div class="lyrico-lyrics-wrapper">She just fuse the way that you move
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She just fuse the way that you move"/>
</div>
<div class="lyrico-lyrics-wrapper">She got gang she got the moves
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She got gang she got the moves"/>
</div>
<div class="lyrico-lyrics-wrapper">Just bombay just bombay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just bombay just bombay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoondti hai nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoondti hai nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek haseen ko agar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek haseen ko agar"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi armaan leke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi armaan leke"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh prem mein nagar ki taraf hai safar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh prem mein nagar ki taraf hai safar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka sauda leke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka sauda leke"/>
</div>
<div class="lyrico-lyrics-wrapper">Pighla sa pighla sa ho ju man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pighla sa pighla sa ho ju man"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundar sa sundar sa ho ju tan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundar sa sundar sa ho ju tan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil jo dhadakte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil jo dhadakte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shole bhadakte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shole bhadakte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Hosh toh khota hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hosh toh khota hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar jab hota hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar jab hota hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hote hote oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hote hote oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jane do na pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jane do na pyar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Behke behke nain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Behke behke nain"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheene dil ka chain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheene dil ka chain"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh ye pyar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh ye pyar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi intezar karde bekaraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi intezar karde bekaraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh yeh pyar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh yeh pyar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadein dil ko tadpaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadein dil ko tadpaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Saansein ulajti jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saansein ulajti jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhen sapne dikhlaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhen sapne dikhlaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab anjaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab anjaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon se tu ghabraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon se tu ghabraye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaton mein so na paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaton mein so na paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathon ko malta jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathon ko malta jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu deewane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu deewane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hote hote oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hote hote oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bhi jane do na pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhi jane do na pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">Hote hote oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hote hote oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bhi jane do na pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bhi jane do na pyar"/>
</div>
</pre>
