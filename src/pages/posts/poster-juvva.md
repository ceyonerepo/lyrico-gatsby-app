---
title: "poster song lyrics"
album: "Juvva"
artist: "M M Keeravani"
lyricist: "Anantha Sriram"
director: "Trikoti Peta"
path: "/albums/juvva-lyrics"
song: "Poster"
image: ../../images/albumart/juvva.jpg
date: 2018-02-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MOYBQqRAhPQ"
type: "happy"
singers:
  -	Baba Seghal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anyday Anywhere Yevvadikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anyday Anywhere Yevvadikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Elage Isthara Chuskonaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elage Isthara Chuskonaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Anytime Anyplace Evademanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anytime Anyplace Evademanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhera Ayyedhi Kaskonaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhera Ayyedhi Kaskonaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Janalu Thirige Junctionlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janalu Thirige Junctionlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Sales Jarige Centerlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Sales Jarige Centerlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy Kaina Baba Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy Kaina Baba Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Don Kaina Dada Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don Kaina Dada Kaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
30*40 <div class="lyrico-lyrics-wrapper">Size Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<div class="lyrico-lyrics-wrapper">Yea Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yea Degreekaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yea Degreekaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Ichesthara Certificate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichesthara Certificate"/>
</div>
<div class="lyrico-lyrics-wrapper">Original Ki Baap 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Original Ki Baap "/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Naa Duplicate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Naa Duplicate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevadi Jigreekaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevadi Jigreekaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Set Chestha Adharcard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Set Chestha Adharcard"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Government Ki Thatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Government Ki Thatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Naa Office Board
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Naa Office Board"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambani Naina Ammesthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambani Naina Ammesthara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kohinoor Naina Konestha Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kohinoor Naina Konestha Chudara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boss Kaina Badmash Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss Kaina Badmash Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thopu Kaina Thopass Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thopu Kaina Thopass Kaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boss Kaina Badmash Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss Kaina Badmash Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thopu Kaina Thopass Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thopu Kaina Thopass Kaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chupe Scanner 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupe Scanner "/>
</div>
<div class="lyrico-lyrics-wrapper">Esesthadu Banner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esesthadu Banner"/>
</div>
<div class="lyrico-lyrics-wrapper">Vintha Vinthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintha Vinthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Untadhi Edi Manner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untadhi Edi Manner"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusesthadu Butter 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusesthadu Butter "/>
</div>
<div class="lyrico-lyrics-wrapper">Chala Undhi Matter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chala Undhi Matter"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadhu Cheyadame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadhu Cheyadame "/>
</div>
<div class="lyrico-lyrics-wrapper">Edi Meter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edi Meter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hello Pillo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hello Pillo"/>
</div>
<div class="lyrico-lyrics-wrapper">Followaipollo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Followaipollo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chammakuchallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chammakuchallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthasepu Aguthave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthasepu Aguthave "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammakachalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammakachalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Assalu Ballo Gullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assalu Ballo Gullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppevanni Nammaku Thallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppevanni Nammaku Thallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhakosthe Vayistha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhakosthe Vayistha "/>
</div>
<div class="lyrico-lyrics-wrapper">Dammaku Dolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaku Dolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Araganta Lone Kanistha Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araganta Lone Kanistha Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aru Janmalaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aru Janmalaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthoche Range Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthoche Range Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shape Kaina Structure Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shape Kaina Structure Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Kaina Feeling Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Kaina Feeling Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Shape Kaina Structure Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shape Kaina Structure Kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Kaina Feeling Kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Kaina Feeling Kaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
<div class="lyrico-lyrics-wrapper">Poster Yesaira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster Yesaira"/>
</div>
</pre>
