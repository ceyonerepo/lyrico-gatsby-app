---
title: "polladha ulagathiley song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Polladha Ulagathiley"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TyHn0SdZ3rI"
type: "sad"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Intha Pollatha Ulagathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pollatha Ulagathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Ennai Padaithaai Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Ennai Padaithaai Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Thangamal Katharum Katharal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Thangamal Katharum Katharal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakke Ketkka Villaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakke Ketkka Villaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Thikkodum Poi Iruppavan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikkodum Poi Iruppavan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu Poi Tholainthaai Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Poi Tholainthaai Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karum Kallana Unai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karum Kallana Unai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poluthum Tholuthen Pothavillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poluthum Tholuthen Pothavillaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi Vathangum Aelaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Vathangum Aelaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vathaithaal Aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vathaithaal Aagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Vilakkai Etri Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Vilakkai Etri Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothiyanaithaal Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiyanaithaal Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannire Valithunaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannire Valithunaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninrene Ithu Vithiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninrene Ithu Vithiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame Therinthavan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Therinthavan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappattra Manam Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappattra Manam Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethanai Melum Vethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanai Melum Vethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvathum Un Velai Aanatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvathum Un Velai Aanatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravindri En Uyir Novatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravindri En Uyir Novatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettu Naan Vaanga Villaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Naan Vaanga Villaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduththa Nee Vaangi Povatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduththa Nee Vaangi Povatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Indri Naan Thaniyaavatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Indri Naan Thaniyaavatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanatha Kanavai Nee Kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanatha Kanavai Nee Kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Vanthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Vanthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Serntha Nilavai Paaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Serntha Nilavai Paaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Sornthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Sornthathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam Thaaramal Nee Ponaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Thaaramal Nee Ponaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Soramal Por Iduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soramal Por Iduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Aanalume Oyamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aanalume Oyamale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paathai Naan Thodarvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathai Naan Thodarvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannire Valithunaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannire Valithunaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninrene Ithu Vithiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninrene Ithu Vithiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame Therinthavan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Therinthavan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappattra Manam Illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappattra Manam Illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thediye Kaalgal Oyinthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye Kaalgal Oyinthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikalum Veernthu Ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikalum Veernthu Ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Kannilum Pugai Sulnthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Kannilum Pugai Sulnthathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervarai Theeyum Paynthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervarai Theeyum Paynthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Verumaiyil Naatkal Neeluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verumaiyil Naatkal Neeluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigaaramo Vilaiyaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaaramo Vilaiyaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Ooram Aanathai Mel Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Ooram Aanathai Mel Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeni Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeni Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilnthaalum Vidamal Thozhthanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilnthaalum Vidamal Thozhthanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathi Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathi Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Noole Illaa Kathadi Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Noole Illaa Kathadi Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladuthe Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladuthe Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ennagumo Ethagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ennagumo Ethagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Sollamal Pogathu Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Sollamal Pogathu Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Pollatha Ulagathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Pollatha Ulagathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Ennai Padaithaai Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Ennai Padaithaai Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Thangamal Katharum Katharal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Thangamal Katharum Katharal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakke Ketkka Villaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakke Ketkka Villaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Thikkodum Poi Iruppavan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikkodum Poi Iruppavan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu Poi Tholainthaai Iraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Poi Tholainthaai Iraivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karum Kallana Unai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karum Kallana Unai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poluthum Tholuthen Pothavillaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poluthum Tholuthen Pothavillaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi Vathangum Aelaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Vathangum Aelaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vathaithaal Aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vathaithaal Aagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Vilakkai Etri Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Vilakkai Etri Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothiyanaithaal Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiyanaithaal Niyayamaa"/>
</div>
</pre>
