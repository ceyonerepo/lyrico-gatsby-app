---
title: "ab yevaro nee baby song lyrics"
album: "Agnyaathavaasi"
artist: "Anirudh Ravichander"
lyricist: "Sri Mani"
director: "Trivikram Srinivas"
path: "/albums/agnyaathavaasi-lyrics"
song: "AB Yevaro Nee Baby"
image: ../../images/albumart/agnyaathavaasi.jpg
date: 2018-01-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XgnoUB-lr_E"
type: "happy"
singers:
  - Nakash Aziz
  - Arjun Chandy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Ooo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Ooo "/>
</div>
<div class="lyrico-lyrics-wrapper">AB Evaro Nee baby 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AB Evaro Nee baby "/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Itu Merupula Ratiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Itu Merupula Ratiri "/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Valapula Vaikari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Valapula Vaikari "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Nari Nari Naduma Murari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Nari Nari Naduma Murari "/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Nee Daari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Nee Daari "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandrude Chukkalo Chikkero 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandrude Chukkalo Chikkero "/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo Nakkero 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo Nakkero "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Premavihaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Premavihaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Ra Nee Guri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Ra Nee Guri "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaipu Volcano 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaipu Volcano "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaipu Cycloneu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaipu Cycloneu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanikenu Tadisina Nagaramla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanikenu Tadisina Nagaramla "/>
</div>
<div class="lyrico-lyrics-wrapper">Koluke Chedirina Hrudayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluke Chedirina Hrudayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaipu Cynaideu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaipu Cynaideu "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Vaipu Uritaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Vaipu Uritaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula Jaillo Khaidila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula Jaillo Khaidila "/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Dhaari Leni Tharunam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Dhaari Leni Tharunam "/>
</div>
<div class="lyrico-lyrics-wrapper">Bosho Devuda Puvvulatho Pranayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bosho Devuda Puvvulatho Pranayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kougili Kalahama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougili Kalahama "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvulatho Narakame Nyayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvulatho Narakame Nyayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Devuda Vennelatho Vinayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Devuda Vennelatho Vinayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ayudha Poojale Andamtho Cheyyadam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayudha Poojale Andamtho Cheyyadam "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaavyama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaavyama "/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Itu Merupula Ratiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Itu Merupula Ratiri "/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Valapula Vaikari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Valapula Vaikari "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Nari Nari Naduma Murari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Nari Nari Naduma Murari "/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Nee Daari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Nee Daari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo AB Yevaro Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo AB Yevaro Nee Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Nadakala Natyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Nadakala Natyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Paadam Tana Sotthantundo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Paadam Tana Sotthantundo "/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunagavula Lasyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunagavula Lasyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Pedaviki Sontham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pedaviki Sontham "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupapala Swapnam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupapala Swapnam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kannu Tana Hakkantundo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kannu Tana Hakkantundo "/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Theerapu Sandhram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Theerapu Sandhram "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Vodduku Sontham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Vodduku Sontham "/>
</div>
<div class="lyrico-lyrics-wrapper">AB Evaro Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AB Evaro Nee Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannullo Aagi Aagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannullo Aagi Aagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavula Anchunu Daatanu Andho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavula Anchunu Daatanu Andho "/>
</div>
<div class="lyrico-lyrics-wrapper">AB Evaro Nee Baby 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AB Evaro Nee Baby "/>
</div>
<div class="lyrico-lyrics-wrapper">Madilone Daagi Daagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madilone Daagi Daagi "/>
</div>
<div class="lyrico-lyrics-wrapper">Bayataku Raanandho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayataku Raanandho "/>
</div>
<div class="lyrico-lyrics-wrapper">Bosho Devuda Puvvulatho Pranayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bosho Devuda Puvvulatho Pranayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kougili Kalahama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougili Kalahama "/>
</div>
<div class="lyrico-lyrics-wrapper">Navvulatho Narakame Nyayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvulatho Narakame Nyayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hola Devuda Vennelatho Vinayamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola Devuda Vennelatho Vinayamaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ayudha Poojale Andamtho Cheyyadam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayudha Poojale Andamtho Cheyyadam "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaavyama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaavyama "/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela Itu Merupula Ratiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela Itu Merupula Ratiri "/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Valapula Vaikari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Valapula Vaikari "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Nari Nari Naduma Murari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Nari Nari Naduma Murari "/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Nee Daari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Nee Daari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo AB Yevaro Nee Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo AB Yevaro Nee Baby"/>
</div>
</pre>
