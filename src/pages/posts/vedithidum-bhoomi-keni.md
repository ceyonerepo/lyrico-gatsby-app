---
title: "vedithidum bhoomi song lyrics"
album: "Keni"
artist: "Kallara Gopan"
lyricist: "Palani Bharathi"
director: "M.A. Nishad"
path: "/albums/keni-song-lyrics"
song: "Vedithidum Bhoomi"
image: ../../images/albumart/keni.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kPJAnBTKiG4"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vedithidum boomiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedithidum boomiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal aarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal aarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraamalae pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraamalae pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai maari pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai maari pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugilgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugilgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilae thangiya eeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilae thangiya eeramum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangi thidalaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi thidalaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marangal viragaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangal viragaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">karugum naerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karugum naerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marangal viragaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangal viragaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">karugum naerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karugum naerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalgalum vegudhae theeyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalgalum vegudhae theeyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agadhiyaai idhayangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agadhiyaai idhayangal "/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyum nilai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyum nilai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidharudhu kanneer moochodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidharudhu kanneer moochodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaraaga oru kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaraaga oru kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">sarugaaga oru kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarugaaga oru kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyinil idhu enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyinil idhu enna "/>
</div>
<div class="lyrico-lyrics-wrapper">panjamaai or yugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjamaai or yugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhaigal vilaiyaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhaigal vilaiyaada "/>
</div>
<div class="lyrico-lyrics-wrapper">pozhiyattum thaai megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pozhiyattum thaai megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vanna pookkalaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vanna pookkalaal "/>
</div>
<div class="lyrico-lyrics-wrapper">kulirattum poo vanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulirattum poo vanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirattum poo vanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirattum poo vanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedithidum boomiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedithidum boomiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal aarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal aarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraamalae pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraamalae pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai maari pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai maari pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugilgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugilgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilae thangiya eeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilae thangiya eeramum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangi thidalaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi thidalaagumo"/>
</div>
</pre>
