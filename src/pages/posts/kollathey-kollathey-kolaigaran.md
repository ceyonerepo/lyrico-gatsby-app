---
title: "kollathey kollathey song lyrics"
album: "Kolaigaran"
artist: "Simon K. King"
lyricist: "Dhamayanthi"
director: "Andrew Louis"
path: "/albums/kolaigaran-song-lyrics"
song: "Kollathey Kollathey"
image: ../../images/albumart/kolaigaran.jpg
date: 2019-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FVUB3H5wnwo"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kollathe Kollathe Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollathe Kollathe Kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralgal Nenjathil Aaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralgal Nenjathil Aaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannooram Thee Thoori Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannooram Thee Thoori Pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathin Eerangal Kaayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Eerangal Kaayathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Naduve Thaagam Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Naduve Thaagam Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Nadungi Ponathu Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Nadungi Ponathu Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Perungkanavin Thavanaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perungkanavin Thavanaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Unnai Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Unnai Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Then Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollathe Kollathe Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollathe Kollathe Kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralgal Nenjathil Aaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralgal Nenjathil Aaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannooram Thee Thoori Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannooram Thee Thoori Pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathin Eerangal Kaayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Eerangal Kaayathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Naduve Thaagam Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Naduve Thaagam Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Nadungi Ponathu Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Nadungi Ponathu Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Perungkanavin Thavanaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perungkanavin Thavanaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Endrum Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Endrum Endrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illaa Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaa Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann Moodum Piraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann Moodum Piraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum Urainthe Ponathu Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum Urainthe Ponathu Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Indri Nee Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indri Nee Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvathum Pizhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvathum Pizhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Sarugaai Aanathu Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Sarugaai Aanathu Inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorangal Por Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorangal Por Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaigalil Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaigalil Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Perungadal Sirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perungadal Sirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangalil Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangalil Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivenbathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivenbathu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakkavum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkavum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Theduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Then Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollathe Kollathe Kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollathe Kollathe Kollathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeralgal Nenjathil Aaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralgal Nenjathil Aaraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannooram Thee Thoori Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannooram Thee Thoori Pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathin Eerangal Kaayathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathin Eerangal Kaayathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Naduve Thaagam Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Naduve Thaagam Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Nadungi Ponathu Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Nadungi Ponathu Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Perungkanavin Thavanaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perungkanavin Thavanaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Unnai Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Unnai Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Then Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaniye"/>
</div>
</pre>
