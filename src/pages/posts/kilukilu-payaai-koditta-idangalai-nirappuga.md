---
title: "kilukilu payaai song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "R Parthiepan"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "Kilukilu Payaai"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oxhG-UFbAHo"
type: "love"
singers:
  - Sreerama Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuppaiyaakki poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaiyaakki poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokkaiyaakki poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokkaiyaakki poraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilu kiluppaiyaai sinungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilu kiluppaiyaai sinungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum kilukiluppai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum kilukiluppai "/>
</div>
<div class="lyrico-lyrics-wrapper">tharum pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharum pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukuduppaiyaai kulungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukuduppaiyaai kulungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil salasalappai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil salasalappai "/>
</div>
<div class="lyrico-lyrics-wrapper">tharum pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharum pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraalae manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraalae manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppaiyaakki poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaiyaakki poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraalae moolaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraalae moolaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokkaiyaakki poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokkaiyaakki poraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu vellai kattaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu vellai kattaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">En viral magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viral magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Color coloraai mettugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color coloraai mettugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval vizhi isaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval vizhi isaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala vaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala vaasikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasikkiraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasikkiraaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil naa sikkurenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil naa sikkurenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa naan nesikkirennn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa naan nesikkirennn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilu kiluppaiyaai sinungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilu kiluppaiyaai sinungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum kilukiluppai tharum pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum kilukiluppai tharum pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukuduppaiyaai kulungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukuduppaiyaai kulungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil salasalappai tharum pennae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil salasalappai tharum pennae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala vaasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala vaasikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasikkiraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasikkiraaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil naa sikkurenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil naa sikkurenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sikkurennn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sikkurennn"/>
</div>
</pre>
