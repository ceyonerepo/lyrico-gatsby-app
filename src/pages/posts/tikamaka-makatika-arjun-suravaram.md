---
title: "tikamaka makatika song lyrics"
album: "Arjun Suravaram"
artist: "Sam CS"
lyricist: "Varikuppala Yadagiri"
director: "T Santhosh"
path: "/albums/arjun-suravaram-lyrics"
song: "Tikamaka Makatika"
image: ../../images/albumart/arjun-suravaram.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X9nY2PLuFzA"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Makatika Tikamaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makatika Tikamaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Makatika Tikamaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makatika Tikamaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Makatika Tikamaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makatika Tikamaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tikamaka Makatika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikamaka Makatika "/>
</div>
<div class="lyrico-lyrics-wrapper">Tikamaka Makatika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikamaka Makatika "/>
</div>
<div class="lyrico-lyrics-wrapper">Tikamaka Makatika Tikamaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikamaka Makatika Tikamaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiri Kunuku Athuku Athuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri Kunuku Athuku Athuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu Solupula Nadumala Bathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu Solupula Nadumala Bathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthala Koraku Vuruku Vuruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthala Koraku Vuruku Vuruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere Kaalu Edaa Nilavani Vuruku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Kaalu Edaa Nilavani Vuruku "/>
</div>
<div class="lyrico-lyrics-wrapper">Soose Soopu Soopula Vuduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soose Soopu Soopula Vuduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Edi Idusani Duduku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edi Idusani Duduku "/>
</div>
<div class="lyrico-lyrics-wrapper">Saatu Maatu Kanapadu Kathaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saatu Maatu Kanapadu Kathaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathakadura Methuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathakadura Methuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadise Prathi Manishilo Doruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadise Prathi Manishilo Doruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Chedu Matalabuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Chedu Matalabuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo Giru Girruna Tirige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo Giru Girruna Tirige"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilekarla Burralaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilekarla Burralaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebukkollu Tikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebukkollu Tikka "/>
</div>
<div class="lyrico-lyrics-wrapper">Lepe Tweetarlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lepe Tweetarlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsappullo Panikirani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsappullo Panikirani "/>
</div>
<div class="lyrico-lyrics-wrapper">Lollula Chakkarlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lollula Chakkarlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tellarina Poddugookina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tellarina Poddugookina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Vartha Pidugulekka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Vartha Pidugulekka "/>
</div>
<div class="lyrico-lyrics-wrapper">Meedabadathado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedabadathado"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosamo Saavugandamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosamo Saavugandamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasesi Masi Poosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasesi Masi Poosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharesi Ayithamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharesi Ayithamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattara Githara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattara Githara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattara Githara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattara Githara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattara Githara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattara Githara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattara Githara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattara Githara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thikamaka Hey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thikamaka Hey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Tika Tika Maka Tika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tika Tika Maka Tika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dorala Musugullo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorala Musugullo "/>
</div>
<div class="lyrico-lyrics-wrapper">Tirugu Dopidi Gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirugu Dopidi Gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeda Emundo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeda Emundo "/>
</div>
<div class="lyrico-lyrics-wrapper">Katthulai Soothai Maa Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthulai Soothai Maa Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemoolako Paati Pettinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemoolako Paati Pettinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijaanni Ethukuladi Theesukochchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijaanni Ethukuladi Theesukochchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Janamlaki Entha Vasthado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janamlaki Entha Vasthado"/>
</div>
<div class="lyrico-lyrics-wrapper">Tvllo Soosedaaka Burrantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tvllo Soosedaaka Burrantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathara Gathara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathara Gathara "/>
</div>
<div class="lyrico-lyrics-wrapper">Gathara Gathara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathara Gathara "/>
</div>
<div class="lyrico-lyrics-wrapper">Gathara Gathara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathara Gathara "/>
</div>
<div class="lyrico-lyrics-wrapper">Gathara Gathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathara Gathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathara Gathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathara Gathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tika Tika Maka Tika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tika Tika Maka Tika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tika Tika Maka Tika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tika Tika Maka Tika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Thoka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Thoka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekathoka Mey Makatika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekathoka Mey Makatika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikamaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikamaka"/>
</div>
</pre>
