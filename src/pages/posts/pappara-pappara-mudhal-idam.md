---
title: "pappara pappara song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Kabilan"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Pappara Pappara"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wUnUn34Ba2Q"
type: "happy"
singers:
  - Priya Subramaniam
  - Ananthu
  - Aalap Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu rajaen rajaen aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu rajaen rajaen aanda ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu rajaen rajaen aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu rajaen rajaen aanda ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pattasatham pathavachi pattu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pattasatham pathavachi pattu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pambarama aatam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pambarama aatam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru Anbu theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru Anbu theru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu iluthaale thagaraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu iluthaale thagaraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu rajaen rajaen aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu rajaen rajaen aanda ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga pazhapzham mulla pola ullavan ullavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga pazhapzham mulla pola ullavan ullavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala urichi paatha thithikira nallavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala urichi paatha thithikira nallavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kela vaasal periya kovil enga ellai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kela vaasal periya kovil enga ellai da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella kulla vandhu putta aagum thollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella kulla vandhu putta aagum thollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella veeti, veerathi veerana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella veeti, veerathi veerana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraandi vaangadi aada, ivanoda vilayaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraandi vaangadi aada, ivanoda vilayaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara muttaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara muttaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye Pappara Pappara Pappara sithaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye Pappara Pappara Pappara sithaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga aala thotta kaalamaada muttuvom muttuvomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga aala thotta kaalamaada muttuvom muttuvomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga vaaliilla kaathadi pola vettuvomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga vaaliilla kaathadi pola vettuvomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja veetu pilla pola oora suthuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja veetu pilla pola oora suthuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaja beedi vaangi vandhu pallaakuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaja beedi vaangi vandhu pallaakuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangaaali vandhale vandhidum yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangaaali vandhale vandhidum yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale engaloda neram nalla neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale engaloda neram nalla neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara "/>
</div>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Pappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Pappara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu rajaen rajaen aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu rajaen rajaen aanda ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru ooru thanjavooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru ooru thanjavooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu rajaen rajaen aanda ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu rajaen rajaen aanda ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pattasatham pathavachi pattu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pattasatham pathavachi pattu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pambarama aatam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pambarama aatam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaru Anbu theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaru Anbu theru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu iluthaale thagaraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu iluthaale thagaraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara"/>
</div>
</pre>
