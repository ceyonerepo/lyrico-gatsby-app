---
title: "mudhal murai song lyrics"
album: "Irumbu Thirai"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "P.S. Mithran"
path: "/albums/irumbu-thirai-song-lyrics"
song: "Mudhal Murai"
image: ../../images/albumart/irumbu-thirai.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1CRMB-tIAQ8"
type: "love"
singers:
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohhhoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhhoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai mazhai paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai mazhai paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyai pol naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyai pol naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi virinthida paarthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi virinthida paarthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval ulagathil veezhnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval ulagathil veezhnthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu varai suvai theetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai suvai theetum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigalai thotrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigalai thotrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval thiruthida poothenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval thiruthida poothenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu pulangalai serthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pulangalai serthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai patta ottai thotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai patta ottai thotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam paarthu thaandi sendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam paarthu thaandi sendren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu vaasal poovai yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu vaasal poovai yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thaanga indrae kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thaanga indrae kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathodu nesam pesum kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathodu nesam pesum kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkendrae nirkum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkendrae nirkum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam thozhlai thattum kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam thozhlai thattum kaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinna artham undaakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna artham undaakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini sugam mattum endraakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini sugam mattum endraakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalukkul sirpam undaakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalukkul sirpam undaakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam indha kanneer endraakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam indha kanneer endraakinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen thondrinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen thondrinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarnthu kolla neram indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarnthu kolla neram indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Odinen odu odinenYen ennai maatrinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odinen odu odinenYen ennai maatrinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kobamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kobamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoora veesi kaayum nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoora veesi kaayum nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram poosi thoosi neeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram poosi thoosi neeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooimai aakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooimai aakinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen vaazhvil oramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vaazhvil oramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaatha peralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaatha peralai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyaatha kaatrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyaatha kaatrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbindru neralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbindru neralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinna artham undaakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna artham undaakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini sugam mattum endraakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini sugam mattum endraakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalukkul sirpam undaakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalukkul sirpam undaakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam indha kanneer endraakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam indha kanneer endraakinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal murai mazhai paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai mazhai paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyai pol naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyai pol naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi virinthida paarthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi virinthida paarthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval ulagathil veezhnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval ulagathil veezhnthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu varai suvai theetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai suvai theetum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigalai thotrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigalai thotrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval thiruthida poothenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval thiruthida poothenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu pulangalai serthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pulangalai serthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey heeyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey heeyyyy"/>
</div>
</pre>
