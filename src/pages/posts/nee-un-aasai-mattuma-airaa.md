---
title: 'nee un aasai mattuma lyrics'
album: 'Airaa'
artist: 'Sundaramurthy K.S.'
lyricist: 'Madhan Karky'
director: 'Sarjun KM'
path: '/albums/airaa-song-lyrics'
song: 'Nee Un Aasai Mattuma'
image: ../../images/albumart/airaa.jpg
date: 2019-03-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DFdIRcVgW64"
type: 'happy'
singers: 
- Sid Sriram
- Rap - NAVZ-47 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Un Aasai Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Aasai Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vedam Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vedam Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dhegam Ennum Poi Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhegam Ennum Poi Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Un Inbam Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Inbam Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thunbam Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunbam Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un EnnaKoottin Ottu Moththama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un EnnaKoottin Ottu Moththama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaakkal Nooraayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaakkal Nooraayiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Nee Vaazhgindra Vaazhve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Nee Vaazhgindra Vaazhve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaithu Nee Povathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaithu Nee Povathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudintha Un Vaazhvin Neelve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudintha Un Vaazhvin Neelve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Noolileri Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Noolileri Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogiraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogiraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Aatuvipathaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Aatuvipathaarena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kaanuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanuvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Un Pookkal Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Pookkal Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mutkal Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mutkal Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaasam Theernthaalo Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Theernthaalo Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Un Punnagaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Punnagaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vedhanaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vedhanaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aazhathil Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aazhathil Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Noolileri Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Noolileri Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogiraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogiraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Aatuvipathaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Aatuvipathaarena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kaanuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanuvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Meleri Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Meleri Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaamal Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamal Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vergalai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vergalai Thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Idaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Idaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhappamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhappamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Po Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Po Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram Nenjile Un Nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Nenjile Un Nenjile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralil Senthooralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralil Senthooralil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneeriley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeriley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Noolileri Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Noolileri Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogiraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogiraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Aatuvipathaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Aatuvipathaarena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kaanuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanuvaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Un Pookkal Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Pookkal Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mutkal Mattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mutkal Mattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaasam Theernthaalo Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Theernthaalo Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Un Punnagaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Un Punnagaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vedhanaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vedhanaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aazhathil Nee Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aazhathil Nee Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Yaro Aval Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Yaro Aval Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Vero Per Vero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Vero Per Vero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogiraai Engu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogiraai Engu Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Moodiyum Un Thedal Thodaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodiyum Un Thedal Thodaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paathai Meendum Malarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paathai Meendum Malarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Urangum Neram Allave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Urangum Neram Allave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhigal Thiranthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhigal Thiranthidave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Uruvam Therinthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uruvam Therinthidave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nizhalum Thodargirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhalum Thodargirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Kettaar Un Viruppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Kettaar Un Viruppai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Vanthaar Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Vanthaar Unakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Thudaithaar Un Kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Thudaithaar Un Kanneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badhil Solla Vaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil Solla Vaariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mudivu Thedi Neeyum Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mudivu Thedi Neeyum Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mudivu Thedi Neeyum Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mudivu Thedi Neeyum Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Udalum En Uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udalum En Uyirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thunbamum En Inbamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunbamum En Inbamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onraai Serum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onraai Serum Velai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathaa Va En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathaa Va En"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Noolileri Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Noolileri Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogiraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogiraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaariga En Kaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariga En Kaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Aatuvipathaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Aatuvipathaarena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kaanuvaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaanuvaayo"/>
</div>
</pre>