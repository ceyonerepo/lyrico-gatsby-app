---
title: "malarudhu pudhu naale song lyrics"
album: "Petromax"
artist: "Ghibran"
lyricist: "S.N. Anuradha"
director: "Rohin Venkatesan"
path: "/albums/petromax-lyrics"
song: "Malarudhu Pudhu Naale"
image: ../../images/albumart/petromax.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/de8hQM0NmQ0"
type: "happy"
singers:
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Malaruthu pudhu naalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu pudhu naalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarathu oli dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarathu oli dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinga idham dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinga idham dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisaya varam dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya varam dhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaruthu pudhu naalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu pudhu naalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarathu oli dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarathu oli dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinga idham dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinga idham dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisaya varam dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya varam dhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana nesangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana nesangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaradhini theeradhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaradhini theeradhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeredhum tharumo sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeredhum tharumo sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai edhirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai edhirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopookuthu manam veesudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopookuthu manam veesudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaradhoo peraanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaradhoo peraanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeiyaadhu naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeiyaadhu naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaningu vinmeenkulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaningu vinmeenkulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha dheebham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha dheebham"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayadhu vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayadhu vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedingu orr aalayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedingu orr aalayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa.aaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa.aaaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa.aaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa.aaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala vidha kili dhinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vidha kili dhinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru manam adainthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru manam adainthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala uravugal oru uyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala uravugal oru uyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruganam inaithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganam inaithidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavinil parandhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavinil parandhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravugal thunai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugal thunai varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudu veiyilinil karu nizhalena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudu veiyilinil karu nizhalena"/>
</div>
<div class="lyrico-lyrics-wrapper">Anudhinam avasiyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anudhinam avasiyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thazhuvalil pala virisalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thazhuvalil pala virisalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru kanavena maraiyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru kanavena maraiyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nodiyinil mana varuthamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nodiyinil mana varuthamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani thiraiyena vilagaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani thiraiyena vilagaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravugalingu manadharuginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugalingu manadharuginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidham vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidham vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginai ingu sumanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginai ingu sumanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru balamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru balamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imainai ingu vizhi irandinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imainai ingu vizhi irandinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai kaakumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai kaakumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir varai ingu kalanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir varai ingu kalanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nijamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nijamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaruthu pudhu naalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu pudhu naalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarathu oli dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarathu oli dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinga idham dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinga idham dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisaya varam dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya varam dhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaruthu pudhu naalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu pudhu naalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarathu oli dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarathu oli dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinga idham dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinga idham dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisaya varam dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya varam dhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana nesangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana nesangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaradhini theeradhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaradhini theeradhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeredhum tharumo sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeredhum tharumo sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai edhirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai edhirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopookuthu manam veesudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopookuthu manam veesudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaradhoo peraanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaradhoo peraanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeiyaadhu naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeiyaadhu naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaningu vinmeenkulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaningu vinmeenkulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha dheebham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha dheebham"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayadhu vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayadhu vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedingu orr aalayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedingu orr aalayam"/>
</div>
</pre>
