---
title: "naan pogum song lyrics"
album: "Kadhal Kan Kattudhe"
artist: "Pavan"
lyricist: "Mohanraja"
director: "Shivaraj"
path: "/albums/kadhal-kan-kattudhe-lyrics"
song: "Naan Pogum"
image: ../../images/albumart/kadhal-kan-kattudhe.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gHf_fAUqE88"
type: "love"
singers:
  - Naresh Iyer
  - Jananie SV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan pogum idamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pogum idamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu vara koodathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu vara koodathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedum nodi ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedum nodi ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuvida koodathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthuvida koodathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththathin eeram nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathin eeram nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam seiyum theriyaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam seiyum theriyaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam kaadhal adadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam kaadhal adadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam pechai ketkadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam pechai ketkadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pogum idamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pogum idamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu vara koodathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu vara koodathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaivittu pogumpodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaivittu pogumpodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga nirkumpodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga nirkumpodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedigaaram velai nirutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedigaaram velai nirutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Seikindrathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seikindrathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaigal korkumpodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaigal korkumpodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholil saayumpodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En tholil saayumpodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho ennam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho ennam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai kolluthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai kolluthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mai vaitha kangal ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai vaitha kangal ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo thottu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo thottu sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara minnal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara minnal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul vandhu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul vandhu sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini enna naan sollavoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna naan sollavoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai konjamaai Kondraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai konjamaai Kondraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai konjamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne endru sollum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne endru sollum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan endru sollum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan endru sollum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam endru kadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam endru kadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu ketkindrathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu ketkindrathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ariyaatha pillai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaatha pillai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaamal kangal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaamal kangal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppattaa thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppattaa thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagai paarkindrathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagai paarkindrathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpendra peril unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpendra peril unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum ottikolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum ottikolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappendru sollithandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappendru sollithandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam katrukkolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam katrukkolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan indru enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan indru enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellavaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai konjamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai konjamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo seithaai"/>
</div>
</pre>
