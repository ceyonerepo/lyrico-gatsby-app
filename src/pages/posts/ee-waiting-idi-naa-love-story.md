---
title: "ee waiting song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Ramanjaneyulu V V"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Ee Waiting"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uWsKioQxY-8"
type: "love"
singers:
  -	Shakthi Sree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ee waiting inkenthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee waiting inkenthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">I miss you ninnenthagano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I miss you ninnenthagano"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you cheppali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you cheppali "/>
</div>
<div class="lyrico-lyrics-wrapper">neetho raava chenthake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho raava chenthake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa dreams emo neethoti ninde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dreams emo neethoti ninde"/>
</div>
<div class="lyrico-lyrics-wrapper">na heartemo aa maata daache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na heartemo aa maata daache"/>
</div>
<div class="lyrico-lyrics-wrapper">ee secret-u neethone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee secret-u neethone "/>
</div>
<div class="lyrico-lyrics-wrapper">cheppe roje eppudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppe roje eppudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee smilemo naakosamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee smilemo naakosamante"/>
</div>
<div class="lyrico-lyrics-wrapper">nee feelingsu naa paina unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee feelingsu naa paina unte"/>
</div>
<div class="lyrico-lyrics-wrapper">ee life antha naathone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee life antha naathone "/>
</div>
<div class="lyrico-lyrics-wrapper">unte anthe chaalule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unte anthe chaalule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee waiting inkenthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee waiting inkenthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">I miss you ninnenthagano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I miss you ninnenthagano"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you cheppali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you cheppali "/>
</div>
<div class="lyrico-lyrics-wrapper">neetho raava chenthake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho raava chenthake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenani"/>
</div>
<div class="lyrico-lyrics-wrapper">vidadheese duram enthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidadheese duram enthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene nuvvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene nuvvani"/>
</div>
<div class="lyrico-lyrics-wrapper">mudi vese bandham premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudi vese bandham premani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yechota nuvvunnaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yechota nuvvunnaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paruguna vodi cherava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruguna vodi cherava"/>
</div>
<div class="lyrico-lyrics-wrapper">Na venake dakunnaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na venake dakunnaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chitikaina veyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitikaina veyava"/>
</div>
<div class="lyrico-lyrics-wrapper">na kalle neekai veche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na kalle neekai veche"/>
</div>
<div class="lyrico-lyrics-wrapper">raathrantha nidure raadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raathrantha nidure raadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee waiting inkenthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee waiting inkenthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">I miss you ninnenthagano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I miss you ninnenthagano"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you cheppali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you cheppali "/>
</div>
<div class="lyrico-lyrics-wrapper">neetho raava chenthake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho raava chenthake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho sandhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho sandhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvichellavu gundeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvichellavu gundeke"/>
</div>
<div class="lyrico-lyrics-wrapper">yenti thondhare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenti thondhare "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvochedaka aagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvochedaka aagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalooni ee prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalooni ee prema"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavadu ika aapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavadu ika aapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paine na prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paine na prema "/>
</div>
<div class="lyrico-lyrics-wrapper">eduraithe choopana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduraithe choopana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee oopiri naadhe aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee oopiri naadhe aina"/>
</div>
<div class="lyrico-lyrics-wrapper">neethone mudipadi undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethone mudipadi undhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee waiting inkenthasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee waiting inkenthasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">I miss you ninnenthagano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I miss you ninnenthagano"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you cheppali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you cheppali "/>
</div>
<div class="lyrico-lyrics-wrapper">neetho raava chenthake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho raava chenthake"/>
</div>
</pre>
