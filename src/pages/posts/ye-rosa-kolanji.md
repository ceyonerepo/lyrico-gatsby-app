---
title: "ye rosa song lyrics"
album: "Kolanji"
artist: "Natarajan Sankaran"
lyricist: "Naveen"
director: "Dhanaram Saravanan"
path: "/albums/kolanji-lyrics"
song: "ye rosa"
image: ../../images/albumart/kolanji.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ty2e10PwBXo"
type: "love"
singers:
  - Deepak
  - Subiksha Richray
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rosaa eei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosaa eei"/>
</div>
<div class="lyrico-lyrics-wrapper">rosaa rosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosaa rosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah rosaa unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah rosaa unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalichu thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalichu thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya pudipen anboda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya pudipen anboda"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah rosaa unnoda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah rosaa unnoda "/>
</div>
<div class="lyrico-lyrics-wrapper">anba mattum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anba mattum than"/>
</div>
<div class="lyrico-lyrics-wrapper">vendi nipen panboda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendi nipen panboda"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah rosaa munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah rosaa munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kettiyaaki suttiyaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kettiyaaki suttiyaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">veeranada pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeranada pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthen manmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthen manmela"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah rosaa ippaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah rosaa ippaala"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vettiyaaki veenaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vettiyaaki veenaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">mappadichu kipadicha oyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mappadichu kipadicha oyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaa nee thupi pona emmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaa nee thupi pona emmela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatukkulla oru kallusanthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatukkulla oru kallusanthil"/>
</div>
<div class="lyrico-lyrics-wrapper">matikittu thathalikum palli pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matikittu thathalikum palli pola"/>
</div>
<div class="lyrico-lyrics-wrapper">maatikiten un nenjukulla naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatikiten un nenjukulla naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo meela oru vali illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo meela oru vali illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veesi poona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veesi poona"/>
</div>
<div class="lyrico-lyrics-wrapper">oru otha paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru otha paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">urinjitu pooche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urinjitu pooche"/>
</div>
<div class="lyrico-lyrics-wrapper">en otha usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en otha usura"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than di chireki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than di chireki"/>
</div>
<div class="lyrico-lyrics-wrapper">en usurarukum karumbu choga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usurarukum karumbu choga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podaa nee aaluyenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podaa nee aaluyenna"/>
</div>
<div class="lyrico-lyrics-wrapper">kamalahasanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamalahasanada"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sokki nikkathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sokki nikkathan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaada en eruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada en eruma"/>
</div>
<div class="lyrico-lyrics-wrapper">maatu kaluvitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatu kaluvitu"/>
</div>
<div class="lyrico-lyrics-wrapper">podaa nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podaa nee "/>
</div>
<div class="lyrico-lyrics-wrapper">mokka paiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mokka paiyathan"/>
</div>
<div class="lyrico-lyrics-wrapper">thaana vilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana vilum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha palamunu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha palamunu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">veenaa ne enna vennandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaa ne enna vennandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaa pesum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaa pesum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">naaku unmaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaku unmaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">penaa nippola sharpudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penaa nippola sharpudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda than naan pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda than naan pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti peththukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti peththukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">nallapadi vaalayippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallapadi vaalayippa"/>
</div>
<div class="lyrico-lyrics-wrapper">nallavali nee sollipodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavali nee sollipodi"/>
</div>
<div class="lyrico-lyrics-wrapper">kalli neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalli neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">villipoala thalinika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villipoala thalinika"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda nenappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda nenappula"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vechan thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vechan thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">so undhan ithayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so undhan ithayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nee endku thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee endku thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">titanic jack apola sethupoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="titanic jack apola sethupoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nanum valavepen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nanum valavepen "/>
</div>
<div class="lyrico-lyrics-wrapper">nambi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambi vaadi"/>
</div>
</pre>
