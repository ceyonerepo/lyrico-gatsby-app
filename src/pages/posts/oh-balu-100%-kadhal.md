---
title: "oh balu song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Oh Balu"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WZiaeAmJnaM"
type: "happy"
singers:
  - Sudharshan Ashok
  - Suchith Suresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Exam Enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Exam Enbathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ex lover Pola Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ex lover Pola Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Memoryla Vanthu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memoryla Vanthu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Torture Pannumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture Pannumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Paranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Paranu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkura Ageula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkura Ageula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padikanumnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikanumnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Kandupudichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Kandupudichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Plus Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Plus Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannugira Stagela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannugira Stagela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaos Theory
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaos Theory"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Padikka Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Padikka Vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh God"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Paranthida Nenachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Paranthida Nenachom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too Bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too Bad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Rekka Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Rekka Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh God"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Rainbow Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Rainbow Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too Bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too Bad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Valachittiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Valachittiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Seriya Seivaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Seriya Seivaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Thaan Balu Oh Balu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thaan Balu Oh Balu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pola Vera Yaarume Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pola Vera Yaarume Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Nee Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Nee Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oscar Athathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscar Athathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vangaalanu Nenachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangaalanu Nenachom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rahman Mundhi Adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahman Mundhi Adha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vangittaarungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangittaarungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Rank Adhaiyaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Rank Adhaiyaachum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vangaalanu Padichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangaalanu Padichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balu Paiyan Irukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balu Paiyan Irukkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyalango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyalango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathroomil Cinema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathroomil Cinema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattukku Thaan Bathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattukku Thaan Bathila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Formulava Paatta Paadidadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Formulava Paatta Paadidadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Syllabus Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Syllabus Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Virumbi Padikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Virumbi Padikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannathirai Puthagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannathirai Puthagamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padichidadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichidadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Sariyaga Sonnan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Sariyaga Sonnan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balu Oh Oh Balu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balu Oh Oh Balu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho oh Ada Avan Pol Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho oh Ada Avan Pol Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Ada Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Ada Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Seriya Seivaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Seriya Seivaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Thaan Balu Balu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thaan Balu Balu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pola Yaarume Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pola Yaarume Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Nee Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Nee Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edison Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edison Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porakkaama Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porakkaama Irundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bulba Kandu Pudichiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulba Kandu Pudichiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Grahambel Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grahambel Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaama Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaama Irundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Telephone Adha Uruvaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telephone Adha Uruvaakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Telephone Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telephone Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhuminnu Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuminnu Irundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cellphone Kaiyil Vandhirukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellphone Kaiyil Vandhirukkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cyclum Bikum Lifeinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cyclum Bikum Lifeinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Benz Gethu Kaattirukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benz Gethu Kaattirukkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Sariyaga Sonnan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Sariyaga Sonnan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balu Oh Oh Balu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balu Oh Oh Balu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho oh Ada Avan Pol Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho oh Ada Avan Pol Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Ada Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Ada Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Seriya Seivaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Seriya Seivaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Thaan Balu Oh Balu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thaan Balu Oh Balu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pola Yaarume Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pola Yaarume Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Nee Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Nee Kelu"/>
</div>
</pre>
