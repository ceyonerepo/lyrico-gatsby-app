---
title: "maruthani sevakalaiya song lyrics"
album: "Marudhavelu"
artist: "James Vasanthan"
lyricist: "Thanjai Ayyappan"
director: "R.K.R. Aathimoolam"
path: "/albums/marudhavelu-lyrics"
song: "Maruthani Sevakalaiya"
image: ../../images/albumart/marudhavelu.jpg
date: 2011-11-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r-641NSy4ZM"
type: "happy"
singers:
  - Thanjai Ayyappan
  - Thanjai Selvi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maruthaani sevakkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthaani sevakkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manja thechi kulikkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja thechi kulikkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthaani sevakkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthaani sevakkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manja thechi kulikkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja thechi kulikkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manja thechi kulikkaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja thechi kulikkaiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman enna nenaikkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman enna nenaikkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman enna nenaikkalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman enna nenaikkalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ye sendhaazham poonguyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye sendhaazham poonguyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kannaana kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kannaana kanmaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi enjoadi neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi enjoadi neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi enjoadi neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi enjoadi neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenanthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenanthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan nenaicha nenappellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan nenaicha nenappellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kaaladiyil naan kedappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaaladiyil naan kedappen"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kanneerai naan thodaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kanneerai naan thodaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenanthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenanthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan nenaicha nenappellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan nenaicha nenappellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kaaladiyil naan kedappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaaladiyil naan kedappen"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kanneerai naan thodaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kanneerai naan thodaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maariyamman koayilukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maariyamman koayilukku"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai rendu vaangivandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai rendu vaangivandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">maaney nee sammadhicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaney nee sammadhicha"/>
</div>
<div class="lyrico-lyrics-wrapper">mathavanga thunai edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathavanga thunai edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">mathavanga thunai edharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathavanga thunai edharku"/>
</div>
<div class="lyrico-lyrics-wrapper">ye senthaazham poonguyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye senthaazham poonguyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kannaana kan maniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kannaana kan maniye"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">adi enjoadi neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi enjoadi neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettulaiyum sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettulaiyum sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">koayilukku naanum vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koayilukku naanum vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettulaiyum sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettulaiyum sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">koayilukku naanum vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koayilukku naanum vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">samsaar endru sollureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samsaar endru sollureenga"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaadhikka venaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaadhikka venaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaadhikka venaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaadhikka venaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga kaaladiyil naan kedappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaaladiyil naan kedappen"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kanneerai naan thodaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kanneerai naan thodaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu ketka naanum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu ketka naanum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">polandhuruvaa onga amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polandhuruvaa onga amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu ketka naanum vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu ketka naanum vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">polandhuruvaa onga amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polandhuruvaa onga amma"/>
</div>
<div class="lyrico-lyrics-wrapper">maadi veedaa machi veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadi veedaa machi veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olai veedaa gudisai veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olai veedaa gudisai veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">odhavaadhunnu solliduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhavaadhunnu solliduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">odhavaadhunnu solliduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhavaadhunnu solliduvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadi veedaa machi veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedaa machi veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olai veedaa gudisai veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olai veedaa gudisai veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadi veedaa machi veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedaa machi veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olai veedaa gudisai veedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olai veedaa gudisai veedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">odhavaadhendru sollivittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhavaadhendru sollivittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odiduvaa ooravittey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odiduvaa ooravittey"/>
</div>
<div class="lyrico-lyrics-wrapper">Odiduvaa ooravittey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odiduvaa ooravittey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga kaaladiyil naan kedappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaaladiyil naan kedappen"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kanneerai naan thodaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kanneerai naan thodaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenanthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenanthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenthenamum un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenthenamum un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenaaga inikkithaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenaaga inikkithaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan nenaicha nenappellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan nenaicha nenappellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavaagi poaguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavaagi poaguthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kaaladiyil naan kedappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaaladiyil naan kedappen"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kanneerai naan thodaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kanneerai naan thodaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nenachen manasu thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nenachen manasu thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ada emmaaman neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada emmaaman neethaaney"/>
</div>
</pre>
