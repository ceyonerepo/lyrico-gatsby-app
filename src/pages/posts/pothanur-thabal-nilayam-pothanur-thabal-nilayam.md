---
title: "pothanur thabal nilayam song lyrics"
album: "Pothanur Thabal Nilayam"
artist: "Tenma"
lyricist: "Siennor"
director: "Praveen"
path: "/albums/pothanur-thabal-nilayam-lyrics"
song: "Pothanur Thabal Nilayam"
image: ../../images/albumart/pothanur-thabal-nilayam.jpg
date: 2022-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v6kQGciZIUA"
type: "happy"
singers:
  - Suchith Suresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kada kada vena kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada kada vena kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kilamba vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilamba vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada athiradi velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada athiradi velai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarka vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarka vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">sada sada vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sada sada vena"/>
</div>
<div class="lyrico-lyrics-wrapper">neram aana pinbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram aana pinbu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhamai veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhamai veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbalam"/>
</div>
<div class="lyrico-lyrics-wrapper">gara gara vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gara gara vena"/>
</div>
<div class="lyrico-lyrics-wrapper">motor vandi pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motor vandi pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada adhile master
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada adhile master"/>
</div>
<div class="lyrico-lyrics-wrapper">mella pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">viru viru vena ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru viru vena ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">attendance pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="attendance pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">aluvalagame parakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aluvalagame parakkume"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">ingudan seidhigal kuviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingudan seidhigal kuviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">silar vaazhkaiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silar vaazhkaiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">ingudhan thuvangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingudhan thuvangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru orutharum veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru orutharum veru"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">thannan thaniye ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannan thaniye ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">serndhu oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serndhu oda"/>
</div>
<div class="lyrico-lyrics-wrapper">thanakena oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanakena oru "/>
</div>
<div class="lyrico-lyrics-wrapper">nooru kanaviruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru kanaviruka"/>
</div>
<div class="lyrico-lyrics-wrapper">amaipil amarndhu nagaralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amaipil amarndhu nagaralam"/>
</div>
<div class="lyrico-lyrics-wrapper">avar avarin aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar avarin aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">avarai aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avarai aala"/>
</div>
<div class="lyrico-lyrics-wrapper">avar avarin thevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar avarin thevai"/>
</div>
<div class="lyrico-lyrics-wrapper">avarai soozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avarai soozha"/>
</div>
<div class="lyrico-lyrics-wrapper">idainidaye ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idainidaye ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazha vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thadavai sirikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thadavai sirikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nerathirku vanthidum paati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerathirku vanthidum paati"/>
</div>
<div class="lyrico-lyrics-wrapper">aval kolam poduval 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval kolam poduval "/>
</div>
<div class="lyrico-lyrics-wrapper">tharai kooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharai kooti"/>
</div>
<div class="lyrico-lyrics-wrapper">porupai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porupai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhuthula maati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhuthula maati"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai mudikka udhavidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai mudikka udhavidum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru ounce tea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ounce tea"/>
</div>
<div class="lyrico-lyrics-wrapper">madhiya unavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhiya unavai"/>
</div>
<div class="lyrico-lyrics-wrapper">chattiyil katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chattiyil katti"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai kandu sirithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai kandu sirithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thabal petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thabal petti"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">ingudan seithigal kuviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingudan seithigal kuviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">silar vaazhkaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silar vaazhkaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">ingu than thuvangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu than thuvangum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu than seithigal kuviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu than seithigal kuviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur thabal nilayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur thabal nilayam"/>
</div>
<div class="lyrico-lyrics-wrapper">silar vaazhkaiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silar vaazhkaiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">ingu than thuvangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu than thuvangum"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanur"/>
</div>
</pre>
