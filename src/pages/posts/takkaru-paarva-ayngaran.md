---
title: "takkaru paarva song lyrics"
album: "Ayngaran"
artist: "G.V. Prakash Kumar"
lyricist: "Shiva Shankar"
director: "Ravi Arasu"
path: "/albums/ayngaran-lyrics"
song: "Takkaru Paarva"
image: ../../images/albumart/ayngaran.jpg
date: 2022-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KWGHbODVdw4"
type: "happy"
singers:
  - Siddharth Mahadevan
  - G.V. Prakash Kumar
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Takkaru Paarva Pakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru Paarva Pakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkunu Botha Yethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Botha Yethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkunu Paatha Mathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Paatha Mathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Nee Pesa Vaikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Nee Pesa Vaikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkaiyil Neram Kooturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaiyil Neram Kooturiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkaiyil Thaagam Theekuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaiyil Thaagam Theekuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathil Vethala Poduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathil Vethala Poduriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil Vantha Oduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil Vantha Oduriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matharu Tongue-ga Maranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matharu Tongue-ga Maranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Setharu Thenga Aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setharu Thenga Aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyila Naa Methanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyila Naa Methanthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutheetti Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutheetti Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththittu Poguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththittu Poguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththa Nee Katturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa Nee Katturiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Katturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katturiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaviyean Manasa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaviyean Manasa Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Oon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Oon"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchiya Amatchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchiya Amatchiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaru Paarva Pakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru Paarva Pakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkunu Botha Yethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Botha Yethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkunu Paatha Mathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Paatha Mathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Nee Pesa Vaikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Nee Pesa Vaikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarezhu Maniyaana Theenthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarezhu Maniyaana Theenthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Android’u Naaniladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Android’u Naaniladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhezhu Jenmamum Koodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhezhu Jenmamum Koodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoda Udpiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoda Udpiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyatha Orankattum Kannae Oan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyatha Orankattum Kannae Oan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaththaan Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaththaan Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Olangengumae Thedipaathaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olangengumae Thedipaathaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnattam Pennae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnattam Pennae Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enkathal Whatsapp’ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkathal Whatsapp’ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae Dp Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanae Dp Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Night Ellam Naan Thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night Ellam Naan Thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnala Romba Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnala Romba Tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaru Paarva Pakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru Paarva Pakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkunu Botha Yethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Botha Yethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkunu Paatha Mathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Paatha Mathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Nee Pesa Vaikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Nee Pesa Vaikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo Munnala Ava Nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Munnala Ava Nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaaru Nila Vattamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru Nila Vattamthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Powercuttu Aanalum Paravaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powercuttu Aanalum Paravaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Inverter Ava Kannunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inverter Ava Kannunthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eragupanthu Naanthaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eragupanthu Naanthaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeguriththaan Engeyo Ponendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeguriththaan Engeyo Ponendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakilayae Ongittathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakilayae Ongittathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatiyae Nikkuraendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatiyae Nikkuraendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiveesi Nee Nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiveesi Nee Nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathellam Kirukirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathellam Kirukirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarpatham Pattathanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarpatham Pattathanal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukkum Matham Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukkum Matham Pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaru Paarva Pakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru Paarva Pakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkunu Botha Yethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu Botha Yethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkunu Paatha Mathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkunu Paatha Mathuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Nee Pesa Vaikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Nee Pesa Vaikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkaiyil Neram Kooturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaiyil Neram Kooturiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkaiyil Thaagam Theekuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaiyil Thaagam Theekuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathil Vethala Poduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathil Vethala Poduriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil Vantha Oduriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil Vantha Oduriyae"/>
</div>
</pre>
