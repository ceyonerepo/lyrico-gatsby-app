---
title: "enathuyire nigazhum song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Enathuyire Nigazhum"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/83d_nDOE838"
type: "Melody"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enadhuyire Nigazhum Nodi Podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyire Nigazhum Nodi Podhume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyire Nigazhum Nodi Podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyire Nigazhum Nodi Podhume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigazhum Nodiyin Aazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhum Nodiyin Aazham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhudhum Unara Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhudhum Unara Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhil Paravum Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil Paravum Mounam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhilae Midhakka Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Midhakka Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Punnagai Yaar Kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Punnagai Yaar Kannile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Punnagai Un kannil Naan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Punnagai Un kannil Naan Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi Vaa En Kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Vaa En Kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enathuyire Uyire Innum Neendidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathuyire Uyire Innum Neendidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirae Uyire Unnai Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae Uyire Unnai Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyire Nigalum Nodi Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyire Nigalum Nodi Pothume"/>
</div>
</pre>
