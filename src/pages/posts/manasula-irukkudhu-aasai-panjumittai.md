---
title: "manasula irukkudhu aasai song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "Manasula Irukkudhu Aasai"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BZr0HuVmDac"
type: "love"
singers:
  - Sarath Santhosh
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manasulla irukuthu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasulla irukuthu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai maraikavum mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai maraikavum mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyira varaiyilum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyira varaiyilum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vivaramum theriyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vivaramum theriyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagi vilagi sinungaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi vilagi sinungaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">udal athu kothikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal athu kothikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum kodukka naan irukaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum kodukka naan irukaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">avasaram athu eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avasaram athu eppadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum pothum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum pothum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasulla irukuthu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasulla irukuthu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai maraikavum mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai maraikavum mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyira varaiyilum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyira varaiyilum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vivaramum theriyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vivaramum theriyala mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pasi edukayil pathuki vaikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi edukayil pathuki vaikava"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva pilla enai serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva pilla enai serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">avathi kaatu nee eduthu thingave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avathi kaatu nee eduthu thingave"/>
</div>
<div class="lyrico-lyrics-wrapper">nila kolanju oodi aanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nila kolanju oodi aanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi sollurathu unaku itha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi sollurathu unaku itha"/>
</div>
<div class="lyrico-lyrics-wrapper">enakonnum vilanga villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakonnum vilanga villa"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi nikalayum thodal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi nikalayum thodal"/>
</div>
<div class="lyrico-lyrics-wrapper">iruku kooda onnu paarvaiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruku kooda onnu paarvaiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman pecha ketka venum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman pecha ketka venum pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasulla irukuthu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasulla irukuthu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai maraikavum mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai maraikavum mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyira varaiyilum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyira varaiyilum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vivaramum theriyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vivaramum theriyala mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu varaikume kulantha pilla than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varaikume kulantha pilla than"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyavilla padam kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyavilla padam kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">ragasiyangala therinthu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragasiyangala therinthu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">than kannukulla vilaiyaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than kannukulla vilaiyaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathu mothamume unakena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathu mothamume unakena"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kodukuren ithu vara than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kodukuren ithu vara than"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnathu appadiye seyalpadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnathu appadiye seyalpadutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaikiren mai theetura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaikiren mai theetura"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman neenga seira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman neenga seira"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu kura than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu kura than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasulla irukuthu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasulla irukuthu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai maraikavum mudiyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai maraikavum mudiyala mama"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyira varaiyilum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyira varaiyilum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vivaramum theriyala mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vivaramum theriyala mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilagi vilagi sinungaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagi vilagi sinungaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">udal athu kothikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal athu kothikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum kodukka naan irukaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum kodukka naan irukaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">avasaram athu eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avasaram athu eppadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum pothum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum pothum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram enna"/>
</div>
</pre>
