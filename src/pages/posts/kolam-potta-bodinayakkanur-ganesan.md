---
title: "kolam potta song lyrics"
album: "Bodinayakkanur Ganesan"
artist: "John Peter"
lyricist: "Nandalala"
director: "O. Gnanam"
path: "/albums/bodinayakkanur-ganesan-lyrics"
song: "Kolam Potta"
image: ../../images/albumart/bodinayakkanur-ganesan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hyRu7ea2jtw"
type: "sad"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koalam poatta vaasalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koalam poatta vaasalula"/>
</div>
<div class="lyrico-lyrics-wrapper">kottudhadi koadaimazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottudhadi koadaimazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiyidhu kalaiyidhu kalaiyidhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyidhu kalaiyidhu kalaiyidhu "/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjil vaazhum gnaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjil vaazhum gnaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyidhu oru amaidhi illaa jaadhagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyidhu oru amaidhi illaa jaadhagam"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhaledhu naduvedhu mudivedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhaledhu naduvedhu mudivedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaazhkkai ellaam naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaazhkkai ellaam naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">oththasollil iththupponen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththasollil iththupponen "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koalam poatta vaasalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koalam poatta vaasalula"/>
</div>
<div class="lyrico-lyrics-wrapper">kottudhadi koadaimazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottudhadi koadaimazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhum indha vaazhkkai saabam poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum indha vaazhkkai saabam poala"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam ottippoachey saayam poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam ottippoachey saayam poala"/>
</div>
<div class="lyrico-lyrics-wrapper">paambuchattai poala indha jenmam urichippoadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paambuchattai poala indha jenmam urichippoadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yekkam thandhaai penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yekkam thandhaai penney"/>
</div>
<div class="lyrico-lyrics-wrapper">naan enna solli nandru koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan enna solli nandru koora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koalam poatta vaasalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koalam poatta vaasalula"/>
</div>
<div class="lyrico-lyrics-wrapper">kottudhadi koadaimazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottudhadi koadaimazhai"/>
</div>
</pre>
