---
title: "title song song lyrics"
album: "Kuruthi Aattam"
artist: "Yuvan Shankar Raja"
lyricist: "MC Sanna"
director: "Sri Ganesh"
path: "/albums/kuruthi-aattam-lyrics"
song: "Title Song"
image: ../../images/albumart/kuruthi-aattam.jpg
date: 2022-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9YrE5b_pPm8"
type: "title track"
singers:
  - MC Sanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">click lock bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="click lock bang"/>
</div>
<div class="lyrico-lyrics-wrapper">vari bullat ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vari bullat ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">raa daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa daa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">en kodi uyara parakutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kodi uyara parakutha"/>
</div>
<div class="lyrico-lyrics-wrapper">pettaikul naanga machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pettaikul naanga machan"/>
</div>
<div class="lyrico-lyrics-wrapper">rombave than vera maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rombave than vera maari"/>
</div>
<div class="lyrico-lyrics-wrapper">kalathula iranguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalathula iranguven"/>
</div>
<div class="lyrico-lyrics-wrapper">munnadi than ninnu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnadi than ninnu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">gang gangah bettar makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gang gangah bettar makka"/>
</div>
<div class="lyrico-lyrics-wrapper">ran ran nooru vagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran ran nooru vagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aattam podathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattam podathada"/>
</div>
<div class="lyrico-lyrics-wrapper">vesam silumisam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesam silumisam"/>
</div>
<div class="lyrico-lyrics-wrapper">baby pakka thaara kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baby pakka thaara kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">urasida paakatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasida paakatha "/>
</div>
<div class="lyrico-lyrics-wrapper">prampara ravudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prampara ravudi"/>
</div>
<div class="lyrico-lyrics-wrapper">mela paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela paru"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi pidikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi pidikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaan nattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaan nattam"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja pol iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja pol iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">mela paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela paru"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi pidikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi pidikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaan nattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaan nattam"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja pol iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja pol iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatil valupavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatil valupavan"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai aadi theera vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai aadi theera vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai aadupavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai aadupavan "/>
</div>
<div class="lyrico-lyrics-wrapper">maranathai vella vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranathai vella vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiri yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiri yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kootali yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootali yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir mela aasa irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir mela aasa irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">thuninthu oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuninthu oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">rowthiram palagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rowthiram palagu"/>
</div>
<div class="lyrico-lyrics-wrapper">sarithiram unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarithiram unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthu vaa what i mean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthu vaa what i mean"/>
</div>
<div class="lyrico-lyrics-wrapper">theemaiyai kondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theemaiyai kondru"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaimaiayai vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaimaiayai vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal virithu poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal virithu poradu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela paru"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi pidikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi pidikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaan nattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaan nattam"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja pol iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja pol iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">mela paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela paru"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi pidikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi pidikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaan nattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaan nattam"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja pol iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja pol iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">paana kathadi pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana kathadi pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha than vettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha than vettum"/>
</div>
<div class="lyrico-lyrics-wrapper">ran bai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ran bai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kuruthi aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kuruthi aattam"/>
</div>
</pre>
