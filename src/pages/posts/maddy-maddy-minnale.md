---
title: "maddy maddy oh oh maddy song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "C. S. Amudhan"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Maddy Maddy oh oh Maddy"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u-pc_US75J4"
type: "mass"
singers:
  - Karthik
  - Timmy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maddy Maddy Oh Oh Mady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddy Maddy Oh Oh Mady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mady Mady Oh Oh Mady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mady Mady Oh Oh Mady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">This Guys On Some Fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Guys On Some Fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Object of Desire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Object of Desire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never in the Sack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never in the Sack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And Off the Beaten Track
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Off the Beaten Track"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Watch Way Up One Jot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Watch Way Up One Jot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He’s Really Kick Ass Hot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He’s Really Kick Ass Hot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Watch Way Up One Jot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Watch Way Up One Jot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He’s Really Kick Ass Hot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He’s Really Kick Ass Hot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Maddy Maddy Oh Oh Mady Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Maddy Maddy Oh Oh Mady Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Maddy Maddy Oh Oh Mady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Maddy Maddy Oh Oh Mady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go Dude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go Dude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maddy Maddy Oh Oh Mady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddy Maddy Oh Oh Mady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Give it Away Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Give it Away Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maddy Maddy Oh Oh Mady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddy Maddy Oh Oh Mady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Give it Away Give it Away Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Give it Away Give it Away Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Give it Away Give it Away Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Give it Away Give it Away Now"/>
</div>
</pre>
