---
title: 'neruppe sikki mukki song lyrics'
album: 'Vettaiyadu Vilayadu'
artist: 'Harris Jayaraj'
lyricist: 'Thamarai'
director: 'Gowtham Vasudev Menon'
path: '/albums/vettaiyadu-vilayadu-song-lyrics'
song: 'Neruppe Sikki Mukki'
image: ../../images/albumart/vettaiyadu-vilayadu.jpg
date: 2006-08-25
lang: tamil
singers: 
- Solar Sai
- Sowmya Raoh
- Franko
youtubeLink: "https://www.youtube.com/embed/7hO_8qr7AY4"
type: 'item'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Neruppae sikki mukki neruppae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppae sikki mukki neruppae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithamaa othadam koduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithamaa othadam koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Methuvaa sokki sokki mayakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Methuvaa sokki sokki mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil paduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyil paduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum onna onna nenachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum onna onna nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu kuchiyaa izhachen
<input type="checkbox" class="lyrico-select-lyric-line" value="Udambu kuchiyaa izhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil etti etti paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavil etti etti paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal pozhachen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanaal pozhachen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oho megam megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oho megam megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogattummm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram pogattummm"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum pothae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral podattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooral podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Megam megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum pothae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral podattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooral podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppae sikki mukki neruppae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppae sikki mukki neruppae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithamaa othadam koduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithamaa othadam koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Methuvaa sokki sokki mayakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Methuvaa sokki sokki mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil paduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyil paduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum onna onna nenachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum onna onna nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu kuchiyaa izhachen
<input type="checkbox" class="lyrico-select-lyric-line" value="Udambu kuchiyaa izhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil etti etti paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavil etti etti paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal pozhachen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanaal pozhachen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mazhaiyae.. mazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhaiyae.. mazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En melae vanthu
<input type="checkbox" class="lyrico-select-lyric-line" value="En melae vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizha vaa..vizha vaaa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizha vaa..vizha vaaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilae… veyilae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Veyilae… veyilae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vervai valaiyai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vervai valaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthida vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Niruthida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paniyae…paniyae..
<input type="checkbox" class="lyrico-select-lyric-line" value="Paniyae…paniyae.."/>
</div>
<div class="lyrico-lyrics-wrapper">En paayil koncham
<input type="checkbox" class="lyrico-select-lyric-line" value="En paayil koncham"/>
</div>
<div class="lyrico-lyrics-wrapper">Padu vaa…padu vaaa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Padu vaa…padu vaaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ithazh oram sirippi pirakkirathae..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithazh oram sirippi pirakkirathae.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa puthusaaga ethaiyo nenaichen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaa puthusaaga ethaiyo nenaichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppae sikki mukki neruppae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppae sikki mukki neruppae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithamaa othadam koduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithamaa othadam koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Methuvaa sokki sokki mayakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Methuvaa sokki sokki mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil paduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyil paduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum onna onna nenachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum onna onna nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu kuchiyaa izhachen
<input type="checkbox" class="lyrico-select-lyric-line" value="Udambu kuchiyaa izhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil etti etti paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavil etti etti paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal pozhachen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanaal pozhachen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eheeeeeeeeeeeeeee
<input type="checkbox" class="lyrico-select-lyric-line" value="Eheeeeeeeeeeeeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaa…..aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaaaaaaa…..aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaa…aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaaaaaaa…aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaalilaaaaaaaaaaaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Alaalilaaaaaaaaaaaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haawwwa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haawwwa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagiyae… sagiyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Sagiyae… sagiyae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Sallabha therin
<input type="checkbox" class="lyrico-select-lyric-line" value="Sallabha therin"/>
</div>
<div class="lyrico-lyrics-wrapper">Maniyae… maniyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Maniyae… maniyae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rathiyae… rathiyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rathiyae… rathiyae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un navil naanum nuzhaithida vaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Un navil naanum nuzhaithida vaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaniyae… kaniyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaniyae… kaniyae…"/>
</div>
<div class="lyrico-lyrics-wrapper">En naavil unthan
<input type="checkbox" class="lyrico-select-lyric-line" value="En naavil unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusiyae… rusiyae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Rusiyae… rusiyae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral odu viralgal nerugidavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Viral odu viralgal nerugidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathodu nadanam thodangumm…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagathodu nadanam thodangumm…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppae sikki mukki neruppae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppae sikki mukki neruppae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithamaa othadam koduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithamaa othadam koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Methuvaa sokki sokki mayakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Methuvaa sokki sokki mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil paduppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Madiyil paduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum onna onna nenachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum onna onna nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu kuchiyaa izhachen
<input type="checkbox" class="lyrico-select-lyric-line" value="Udambu kuchiyaa izhachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil etti etti paarthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavil etti etti paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal pozhachen…
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanaal pozhachen…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oho megam megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oho megam megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogattummm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram pogattummm"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum pothae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral podattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooral podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Megam megam
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum pothae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral podattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooral podattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aha ha ha….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha ha ha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ha ha….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha ha ha…."/>
</div>
<div class="lyrico-lyrics-wrapper">Meu keu keu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Meu keu keu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Meu keu keu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Meu keu keu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ha ha…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha ha ha….."/>
</div>
</pre>