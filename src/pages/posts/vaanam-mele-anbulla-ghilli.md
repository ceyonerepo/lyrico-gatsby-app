---
title: "vaanam mele song lyrics"
album: "Anbulla Ghilli"
artist: "Arrol Corelli"
lyricist: "Srinath Ramalingam"
director: "Srinath Ramalingam"
path: "/albums/anbulla-ghilli-lyrics"
song: "Vaanam Mele"
image: ../../images/albumart/anbulla-ghilli.jpg
date: 2022-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2AWSKq6Nq10"
type: "happy"
singers:
  - Arrol Corelli
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">cora cora cora corona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cora cora cora corona"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyo romba sorry na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyo romba sorry na"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan thanniku marina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan thanniku marina"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ellorukum jollyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ellorukum jollyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye vaanam mela boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vaanam mela boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">keele padacha thaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keele padacha thaaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">padachavane inge vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padachavane inge vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">confuse aavan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="confuse aavan da"/>
</div>
<div class="lyrico-lyrics-wrapper">maratha vetnanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maratha vetnanga"/>
</div>
<div class="lyrico-lyrics-wrapper">paper ah pathanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paper ah pathanga"/>
</div>
<div class="lyrico-lyrics-wrapper">atha than kaasu panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha than kaasu panam"/>
</div>
<div class="lyrico-lyrics-wrapper">thuttun giranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuttun giranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maratha vetnanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maratha vetnanga"/>
</div>
<div class="lyrico-lyrics-wrapper">paper ah pathanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paper ah pathanga"/>
</div>
<div class="lyrico-lyrics-wrapper">atha than kaasu panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha than kaasu panam"/>
</div>
<div class="lyrico-lyrics-wrapper">thuttun giranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuttun giranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iyarkaiya than maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkaiya than maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalran manusan payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalran manusan payapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">seyarkaya than thooki kitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyarkaya than thooki kitu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatran panathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatran panathula"/>
</div>
<div class="lyrico-lyrics-wrapper">plastic kooduthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="plastic kooduthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha vaalunga oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha vaalunga oru "/>
</div>
<div class="lyrico-lyrics-wrapper">naal panja paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal panja paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">paada poronga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paada poronga"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai maarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai maarana"/>
</div>
<div class="lyrico-lyrics-wrapper">seyarkai vaalathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyarkai vaalathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya urakka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya urakka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">poigala mithuchu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poigala mithuchu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai maarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai maarana"/>
</div>
<div class="lyrico-lyrics-wrapper">seyarkai vaalathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyarkai vaalathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya urakka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya urakka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">poigala mithuchu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poigala mithuchu thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cora cora cora corona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cora cora cora corona"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyo romba sorry na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyo romba sorry na"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan thanniku marina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan thanniku marina"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ellorukum jollyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ellorukum jollyna"/>
</div>
<div class="lyrico-lyrics-wrapper">cora cora cora corona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cora cora cora corona"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyo romba sorry na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyo romba sorry na"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan thanniku marina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan thanniku marina"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ellorukum jollyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ellorukum jollyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mukka vaasi vandi viduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukka vaasi vandi viduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppu moochu nga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu moochu nga"/>
</div>
<div class="lyrico-lyrics-wrapper">atha naama ellam mopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha naama ellam mopam"/>
</div>
<div class="lyrico-lyrics-wrapper">puduchu saaga poronga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puduchu saaga poronga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakiya potukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakiya potukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">thuppaki thookikunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuppaki thookikunu"/>
</div>
<div class="lyrico-lyrics-wrapper">ghilli vachu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghilli vachu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">mannu raanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannu raanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai matum katrathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai matum katrathula"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga thanunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga thanunga "/>
</div>
<div class="lyrico-lyrics-wrapper">aduchalum uthachalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchalum uthachalum"/>
</div>
<div class="lyrico-lyrics-wrapper">maara matonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara matonga"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam ponguthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam ponguthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiyil chennaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyil chennaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">raagul ah eppadiyachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raagul ah eppadiyachum"/>
</div>
<div class="lyrico-lyrics-wrapper">paaka porenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaka porenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iyarkai maarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai maarana"/>
</div>
<div class="lyrico-lyrics-wrapper">seyarkai vaalathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyarkai vaalathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya urakka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya urakka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">poigala mithuchu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poigala mithuchu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai maarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai maarana"/>
</div>
<div class="lyrico-lyrics-wrapper">seyarkai vaalathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyarkai vaalathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiya urakka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiya urakka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">poigala mithuchu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poigala mithuchu thallu"/>
</div>
</pre>
