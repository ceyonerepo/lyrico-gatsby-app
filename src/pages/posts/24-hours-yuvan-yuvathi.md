---
title: "24 hours song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "24 Hours"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_GnX8r3ciFo"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Come In Dance On You Offi Dance Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come In Dance On You Offi Dance Up"/>
</div>
<div class="lyrico-lyrics-wrapper">When You Hear The Music On You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When You Hear The Music On You"/>
</div>
<div class="lyrico-lyrics-wrapper">Put Your Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Your Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Are You Steady Gonsup And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Steady Gonsup And"/>
</div>
<div class="lyrico-lyrics-wrapper">Put Your Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Your Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody Dema Jammawala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Dema Jammawala"/>
</div>
<div class="lyrico-lyrics-wrapper">Put Them Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put Them Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24 <div class="lyrico-lyrics-wrapper">Mani Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaikku Inge Podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaikku Inge Podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">America Ponna Paathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="America Ponna Paathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Ooru Pudikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Ooru Pudikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginga Ulagam Idhuvaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginga Ulagam Idhuvaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palagida Thudikkum Perumoochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagida Thudikkum Perumoochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anugunda Thookki Poottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anugunda Thookki Poottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalunga Aattam Adangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalunga Aattam Adangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Thedum Paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Thedum Paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthum Pengal Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthum Pengal Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Bomb Pole Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Bomb Pole Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalungalaale Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalungalaale Vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkai Inge Varavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkai Inge Varavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam Saami Thuravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam Saami Thuravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light Illaamal Uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light Illaamal Uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Meendum Iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Meendum Iravu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24 <div class="lyrico-lyrics-wrapper">Manineram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manineram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaikku Inge Podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaikku Inge Podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">America Ponna Paathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="America Ponna Paathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Ooru Pudikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Ooru Pudikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Ri Ga Ma Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ri Ga Ma Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ri Ga Ma Ri Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ri Ga Ma Ri Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ri Ga Ma Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ri Ga Ma Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ri Ga Pa Ma Ga Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ri Ga Pa Ma Ga Ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coffee Ya Kudi Nee Paattu Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee Ya Kudi Nee Paattu Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukku Pudichadha Nee Senju Mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku Pudichadha Nee Senju Mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangidu Adi Vaanathu Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangidu Adi Vaanathu Idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangida Thaangida Aagu Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangida Thaangida Aagu Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvadhu Varam Koothaadum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvadhu Varam Koothaadum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Thedhiyum Vaaliba Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Thedhiyum Vaaliba Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasoru Kudam Verum Kaali Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasoru Kudam Verum Kaali Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Rombi Valiyattum Thedu Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Rombi Valiyattum Thedu Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Naalai Ellaame Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Naalai Ellaame Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtam Pole Indru Vazhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Pole Indru Vazhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Naatkal Ottum Nam Pozhudhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Naatkal Ottum Nam Pozhudhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaadum Mun Jolly Thedada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaadum Mun Jolly Thedada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24 <div class="lyrico-lyrics-wrapper">Manineram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manineram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaikku Inge Podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaikku Inge Podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">America Ponna Paathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="America Ponna Paathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Ooru Pudikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Ooru Pudikkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooh Ooh Ooh Ooh Ooh Ooh Ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Ooh Ooh Ooh Ooh Ooh Ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Rara Rara Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Rara Rara Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Rara Rara Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Rara Rara Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh Ooh Ooh Ooh Ooh Ooh Ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Ooh Ooh Ooh Ooh Ooh Ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Rara Rara Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Rara Rara Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Rara Rara Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Rara Rara Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Na Na Naa Na Na Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Na Na Naa Na Na Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Na Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Na Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Na Na Naa Thana Naa Na Naa Na Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Na Na Naa Thana Naa Na Naa Na Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Na Na Naa Na Na Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Na Na Naa Na Na Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Na Na Na Na Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Na Na Na Na Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Na Na Naa Thana Na Na Na Na Na Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Na Na Naa Thana Na Na Na Na Na Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Platina Sillai Vizhi Veesum Valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Platina Sillai Vizhi Veesum Valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvayil Bodhayil Suthuthu Thalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvayil Bodhayil Suthuthu Thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anugadhir Ulai Sood Yethum Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anugadhir Ulai Sood Yethum Alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyudhu Seiyudhu Uyirai Kolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyudhu Seiyudhu Uyirai Kolai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkena Udai Kikkaana Idai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkena Udai Kikkaana Idai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathai Thaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Thaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Rara Rara Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Rara Rara Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiradai Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradai Padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathai Udai Aasaikku Thadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathai Udai Aasaikku Thadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil Kangalil Podu Yedai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Kangalil Podu Yedai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Pookkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pookkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaikaaikkum Azhaghu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaikaaikkum Azhaghu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkum Podhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkum Podhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam Podhum Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam Podhum Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarum Meesai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarum Meesai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Neeludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Neeludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24 <div class="lyrico-lyrics-wrapper">Mani Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaikku Inge Podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaikku Inge Podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">America Ponna Paathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="America Ponna Paathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Ooru Pudikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Ooru Pudikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginga Ulagam Idhuvaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginga Ulagam Idhuvaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palagida Thudikkum Perumoochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagida Thudikkum Perumoochchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anugunda Thookki Poottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anugunda Thookki Poottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalunga Aattam Adangaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalunga Aattam Adangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hands Up Every Body Hands Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hands Up Every Body Hands Up"/>
</div>
</pre>
