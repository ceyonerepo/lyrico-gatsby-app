---
title: "dhongana kodukkaa song lyrics"
album: "Paramapadham"
artist: "Amrish"
lyricist: "K.R. Dharan"
director: "K. Thirugnanam"
path: "/albums/Paramapadham-song-lyrics"
song: "Dhongana Kodukkaa"
image: ../../images/albumart/paramapadham.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XkCzqFC5kvc"
type: "Entertainment"
singers:
  - Amrish
  - Bhargavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ne oru dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne oru dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukka kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukka kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">kodukka kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodukka kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">locala vaadi locala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="locala vaadi locala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam thookala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam thookala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">palapalanu thathalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palapalanu thathalanu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">poodalama chumma poodalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poodalama chumma poodalama"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu round 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu round "/>
</div>
<div class="lyrico-lyrics-wrapper">sarakku podalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarakku podalala"/>
</div>
<div class="lyrico-lyrics-wrapper">dhonganna kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhonganna kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">locala vadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="locala vadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam thookala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam thookala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">molaga pacha molaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="molaga pacha molaga"/>
</div>
<div class="lyrico-lyrics-wrapper">pola kaaram ethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola kaaram ethura"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil colour coloura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil colour coloura"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatchi katura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatchi katura"/>
</div>
<div class="lyrico-lyrics-wrapper">thodada vaya thdara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodada vaya thdara"/>
</div>
<div class="lyrico-lyrics-wrapper">enna ippadi oothura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ippadi oothura"/>
</div>
<div class="lyrico-lyrics-wrapper">chuinna urasaluke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuinna urasaluke"/>
</div>
<div class="lyrico-lyrics-wrapper">surungi pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surungi pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi emmadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi emmadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ippadi aathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippadi aathura"/>
</div>
<div class="lyrico-lyrics-wrapper">bp ehtura potu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bp ehtura potu "/>
</div>
<div class="lyrico-lyrics-wrapper">thakura neka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakura neka"/>
</div>
<div class="lyrico-lyrics-wrapper">ada sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo thaluku moluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo thaluku moluku"/>
</div>
<div class="lyrico-lyrics-wrapper">thaluku moluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaluku moluku"/>
</div>
<div class="lyrico-lyrics-wrapper">erangi vanthu kuluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erangi vanthu kuluku"/>
</div>
<div class="lyrico-lyrics-wrapper">ni kulukalana kulukalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni kulukalana kulukalana"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthukuven suluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthukuven suluku"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">locala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="locala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam thookala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam thookala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">aie kuthu vanthu kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aie kuthu vanthu kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gummanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gummanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">inike eluthi thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inike eluthi thariya"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda sothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda sothu"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhusa romba pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhusa romba pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">ni kattura thinusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni kattura thinusa"/>
</div>
<div class="lyrico-lyrics-wrapper">edhura paakalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhura paakalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">immaam perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="immaam perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">munji mogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munji mogara"/>
</div>
<div class="lyrico-lyrics-wrapper">yegura pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegura pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">trouser unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trouser unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">avura poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avura poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">raa raa rephuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa rephuku"/>
</div>
<div class="lyrico-lyrics-wrapper">raa raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuwu dhongana kodukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuwu dhongana kodukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">locala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="locala vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam thookala vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam thookala vaadi"/>
</div>
</pre>
