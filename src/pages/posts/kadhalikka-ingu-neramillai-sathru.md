---
title: "kadhalikka ingu neramillai song lyrics"
album: "Sathru"
artist: "Amresh Ganesh - Surya Prasadh R"
lyricist: "Kabilan - Karky - Sorko"
director: "Naveen Nanjundan"
path: "/albums/sathru-lyrics"
song: "Kadhalikka Ingu Neramillai"
image: ../../images/albumart/sathru.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9lrJKWTgEIM"
type: "love"
singers:
  - Sharath Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollai Pannu Mullaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai Pannu Mullaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhu Vaazhvai Oru Naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu Vaazhvai Oru Naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhathaan Thudikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhathaan Thudikaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalaalae Azhagaai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalaalae Azhagaai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathai Kudikkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Kudikkathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Endrae Ondru Mannil Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Endrae Ondru Mannil Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aluvalgal Noor Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluvalgal Noor Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulugalgal Vendaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulugalgal Vendaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhuvalgal Theerum Mun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhuvalgal Theerum Mun"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhuvalgal Vendaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuvalgal Vendaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Thithikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thithikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenai Pola Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenai Pola Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Rusithaal Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Rusithaal Podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Pola Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Pola Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Theera Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vendumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aasai Theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Urugi Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Urugi Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhathaanae Saagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhathaanae Saagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolladhae Kaadhaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolladhae Kaadhaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Illaiyaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illaiyaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Neram Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Neram Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Neram Illaiyaeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Neram Illaiyaeaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Endrae Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Endrae Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Illiayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Illiayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollai Pannu Mullaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai Pannu Mullaiyae"/>
</div>
</pre>
