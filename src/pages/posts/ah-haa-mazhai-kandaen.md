---
title: "ah haa mazhai song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Thamarai"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Ah Haa Mazhai"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OK9Jv00nbpo"
type: "melody"
singers:
  - Krish
  - Ujjayinee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aahaa Mazhai Manadhukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa Mazhai Manadhukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai Aval Aruginil Naanirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai Aval Aruginil Naanirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil Salasalavena Aadum Selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Salasalavena Aadum Selai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Adhu Thazhuvida Naan Karaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Adhu Thazhuvida Naan Karaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Naal Paarthabodhe Maarinene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Naal Paarthabodhe Maarinene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa Mazhai Manadhukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa Mazhai Manadhukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai Poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai Poovai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Aruginil Naanirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Aruginil Naanirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Pondra Pennai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Pondra Pennai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil Paarthadhillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil Paarthadhillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Aayul Kaalamum Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Aayul Kaalamum Kaathiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa Mazhai Manadhukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa Mazhai Manadhukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai Poovai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai Poovai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Aruginil Naanirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Aruginil Naanirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Padhiththu Neeyum Sendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Padhiththu Neeyum Sendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thadangal Sellum Pinnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thadangal Sellum Pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaarthai Neeyum Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaarthai Neeyum Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Meera Mudiyaadhu Ennaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Meera Mudiyaadhu Ennaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sirippil Ennai Tholaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippil Ennai Tholaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kuraigal Ellaam Karaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kuraigal Ellaam Karaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Yengavaithaval Yaarivalo….oo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Yengavaithaval Yaarivalo….oo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa Mazhai Manadhukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa Mazhai Manadhukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai Aval Aruginil Naanirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai Aval Aruginil Naanirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalilaiyin Azhagaipole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalilaiyin Azhagaipole"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhayam Kanden Maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhayam Kanden Maane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Thoongum Kannan Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Thoongum Kannan Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Kettu Paarthen Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Kettu Paarthen Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thuvangivaikkum Naale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thuvangivaikkum Naale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaiyindri Pogum Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaiyindri Pogum Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Neengi Poanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Neengi Poanadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Pondra Pennai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Pondra Pennai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvaraiyil Paarthadhillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraiyil Paarthadhillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Aayul Kaalamum Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Aayul Kaalamum Kaathiruppen"/>
</div>
</pre>
