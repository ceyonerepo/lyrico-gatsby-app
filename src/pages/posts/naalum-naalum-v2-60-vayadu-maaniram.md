---
title: "naalum naalum v2 song lyrics"
album: "60 Vayadu Maaniram"
artist: "Ilaiyaraaja"
lyricist: "Vivek"
director: "Radha Mohan"
path: "/albums/60-vayadu-maaniram-lyrics"
song: "Naalum Naalum V2"
image: ../../images/albumart/60-vayadu-maaniram.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rCc0hd0XLFo"
type: "melody"
singers:
  - Monali Thakur
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Shaana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shana naana naa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram maadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram maadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudam aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudam aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha kaalam ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaalam ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai thannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai thannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann malarndha poovum sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann malarndha poovum sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanaiyo kaalangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanaiyo kaalangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirundhu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirundhu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan indru malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan indru malarndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adhisayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkaiyin azhagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkaiyin azhagilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shaana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaana naana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shana naana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shana naana naa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pul manathin ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pul manathin ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli yerum kalai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli yerum kalai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodi vaitha unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi vaitha unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vendru antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vendru antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu kangal sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu kangal sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollivida vendum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollivida vendum endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennivarum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennivarum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorkal ennai kattipoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorkal ennai kattipoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla solla othigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla solla othigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam nindra vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam nindra vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharathil vittu poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharathil vittu poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaatha nam unarvellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaatha nam unarvellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi inghu sollathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi inghu sollathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumanangal inaindhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumanangal inaindhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalangal varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalangal varaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjangal irandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjangal irandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannai tholaithathum enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai tholaithathum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann imaigal mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann imaigal mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ulaginil kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ulaginil kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannidathil thuligalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannidathil thuligalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengi vaitha megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengi vaitha megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha nodi mazhai tharumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha nodi mazhai tharumoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu kilaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu kilaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu varum kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu varum kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha nodi poovai thodumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha nodi poovai thodumoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi pogum dhisaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi pogum dhisaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru odam sellathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru odam sellathoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanangal ithu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal ithu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalodum thodaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalodum thodaratho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram maadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram maadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudam aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudam aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha kaalam ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaalam ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai thannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai thannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann malarndha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann malarndha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovum sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovum sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanaiyo kaalangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanaiyo kaalangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirundhu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirundhu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan indru malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan indru malarndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adhisayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkaiyin azhagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkaiyin azhagilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthaal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthaal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
</pre>
