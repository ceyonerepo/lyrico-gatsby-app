---
title: "monkistha kinkistha song lyrics"
album: "Koothan"
artist: "Balz G"
lyricist: "Rokesh"
director: "Venky AL"
path: "/albums/koothan-lyrics"
song: "Monkistha Kinkistha"
image: ../../images/albumart/koothan.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0mLmSlKVccM"
type: "happy"
singers:
  - T Rajender
  - Shenbagaraj
  - Baby Shajani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chai chai chai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chai chai chai"/>
</div>
<div class="lyrico-lyrics-wrapper">chai chan chai chan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chai chan chai chan"/>
</div>
<div class="lyrico-lyrics-wrapper">money money money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="money money money maa"/>
</div>
<div class="lyrico-lyrics-wrapper">money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="money maa"/>
</div>
<div class="lyrico-lyrics-wrapper">lo lo nu alaiyuraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lo lo nu alaiyuraaa"/>
</div>
<div class="lyrico-lyrics-wrapper">asathal alltappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asathal alltappu"/>
</div>
<div class="lyrico-lyrics-wrapper">aahaa aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahaa aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">bongu podharru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bongu podharru"/>
</div>
<div class="lyrico-lyrics-wrapper">kelambinirru kelambinirru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelambinirru kelambinirru"/>
</div>
<div class="lyrico-lyrics-wrapper">veruppa velluthi pannatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veruppa velluthi pannatha"/>
</div>
<div class="lyrico-lyrics-wrapper">matter ah ammmukku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matter ah ammmukku daa"/>
</div>
<div class="lyrico-lyrics-wrapper">mama gumukku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama gumukku daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">monkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha kinkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha kinkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinki payasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinki payasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">monkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha kinkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha kinkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinki payasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinki payasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para parappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para parappa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthuran jola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthuran jola"/>
</div>
<div class="lyrico-lyrics-wrapper">illada vellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illada vellai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakalapaaa machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakalapaaa machan"/>
</div>
<div class="lyrico-lyrics-wrapper">na pola maja va vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na pola maja va vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">para para suthurom joola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para suthurom joola"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala maja va vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala maja va vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kala kala kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kala kala kala"/>
</div>
<div class="lyrico-lyrics-wrapper">para para para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para para para para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alti no sonnaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alti no sonnaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladha daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladha daa"/>
</div>
<div class="lyrico-lyrics-wrapper">darra ni don attam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="darra ni don attam"/>
</div>
<div class="lyrico-lyrics-wrapper">thulladha daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulladha daa"/>
</div>
<div class="lyrico-lyrics-wrapper">jolly ya irukurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolly ya irukurom"/>
</div>
<div class="lyrico-lyrics-wrapper">purse il la dhuddu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purse il la dhuddu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">daaaaa da da da daaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daaaaa da da da daaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">makkana ponnuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkana ponnuna"/>
</div>
<div class="lyrico-lyrics-wrapper">oram kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">dokkanna cancy ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dokkanna cancy ya"/>
</div>
<div class="lyrico-lyrics-wrapper">thatti vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatti vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">semma chilkkana firure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma chilkkana firure"/>
</div>
<div class="lyrico-lyrics-wrapper">na machan un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na machan un "/>
</div>
<div class="lyrico-lyrics-wrapper">maas ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maas ah kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">monkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha kinkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha kinkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinki payasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinki payasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pilla vidum figure pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla vidum figure pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">povadha ni vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povadha ni vada"/>
</div>
<div class="lyrico-lyrics-wrapper">first konja vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first konja vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">pova pova genja vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pova pova genja vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">sumarana munja vechinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumarana munja vechinu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnadiye alaiya vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnadiye alaiya vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisiyila kalati vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisiyila kalati vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">chumma chumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chumma chumma"/>
</div>
<div class="lyrico-lyrics-wrapper">chumma chumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chumma chumma"/>
</div>
<div class="lyrico-lyrics-wrapper">chumma chumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chumma chumma"/>
</div>
<div class="lyrico-lyrics-wrapper">yamma yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yamma yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yamma yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yamma yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yamma yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yamma yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yamma yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yamma yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gumukku khulfi ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gumukku khulfi ni"/>
</div>
<div class="lyrico-lyrics-wrapper">north india
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="north india"/>
</div>
<div class="lyrico-lyrics-wrapper">thunda thiriyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunda thiriyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">niyum ondiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyum ondiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">night la sun irukadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night la sun irukadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">whiteula black irukadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whiteula black irukadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">purinjha doubt edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purinjha doubt edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">niyooo kd
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyooo kd"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nondi sakka than solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nondi sakka than solli"/>
</div>
<div class="lyrico-lyrics-wrapper">vandiya ottura thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandiya ottura thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhai yaa kaladukannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhai yaa kaladukannu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaa podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaa podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kastam munnu vanthu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam munnu vanthu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">area vae pakkathulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="area vae pakkathulla"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum illa rathathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum illa rathathula"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham than da mothathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham than da mothathula"/>
</div>
<div class="lyrico-lyrics-wrapper">thukki vida yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thukki vida yarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">maadi vittu car rum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadi vittu car rum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda aasai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda aasai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga aadum aatathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga aadum aatathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">monkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha kinkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha kinkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinki payasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinki payasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">monkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha kinkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha kinkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinkistha monkistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinkistha monkistha"/>
</div>
<div class="lyrico-lyrics-wrapper">kinki payasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinki payasa"/>
</div>
</pre>
