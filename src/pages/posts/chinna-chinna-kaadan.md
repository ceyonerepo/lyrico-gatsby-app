---
title: "chinna chinna song lyrics"
album: "Kaadan"
artist: "Shantanu Moitra"
lyricist: "Vanamali"
director: "Prabhu Solomon"
path: "/albums/kaadan-song-lyrics"
song: "Chinna Chinna"
image: ../../images/albumart/kaadan.jpg
date: 2021-03-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YHaXwd6xs48"
type: "Love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chinna chinna megam kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna megam kooduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla thural poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla thural poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna chinna megam kooduthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna megam kooduthe "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla thural poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla thural poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhala ithu kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhala ithu kadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">kandavudan enna kolluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandavudan enna kolluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mothalal sila mothalal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothalal sila mothalal"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna sollu potu thakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna sollu potu thakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaive nenaive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaive nenaive"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura thedi sellu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura thedi sellu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaive nenaive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaive nenaive"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalodu vanthu nillu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalodu vanthu nillu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh oh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh oh "/>
</div>
</pre>
