---
title: "thala kodhum song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Raju Murugan"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Thala Kodhum"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YwVHHZNtdKU"
type: "melody"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thala Kodhum Elangaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kodhum Elangaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saedhi Kondu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saedhi Kondu Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramaagum Vidhai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramaagum Vidhai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Sollitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Sollitharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaadha Kalangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaadha Kalangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaadha Vidiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaadha Vidiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Ingu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Ingu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Kodhum Elangaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kodhum Elangaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saedhi Kondu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saedhi Kondu Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramaagum Vidhai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramaagum Vidhai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Sollitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Sollitharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaadha Kalangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaadha Kalangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaadha Vidiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaadha Vidiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Ingu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Ingu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Pakkamdhan Pakkamdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pakkamdhan Pakkamdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal Nikkudhae Nikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Nikkudhae Nikkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Pakkamdhan Pakkamdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pakkamdhan Pakkamdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal Nikkudhae Nikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Nikkudhae Nikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nambi Nee Munna Pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nambi Nee Munna Pogaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Undaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannoram Yen Kaneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Yen Kaneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbala Nee Kai Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbala Nee Kai Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neela Vanna Koora Illaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Vanna Koora Illaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilam Ingu Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Ingu Yedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Ennum Thozhan Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ennum Thozhan Unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigala Meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigala Meeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarumo Thaana Nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarumo Thaana Nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae Thannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae Thannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadu Neeyae Aram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu Neeyae Aram"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaagum Manmelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaagum Manmelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meedhi Irul Nee Kadandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi Irul Nee Kadandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Oli Vaasal Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Oli Vaasal Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil Nammai Yendhi Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Nammai Yendhi Kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkaana Naal Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkaana Naal Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Kodhum Elangaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kodhum Elangaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saedhi Kondu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saedhi Kondu Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramaagum Vidhai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramaagum Vidhai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Sollitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Sollitharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaadha Kalangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaadha Kalangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaadha Vidiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaadha Vidiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Ingu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Ingu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Pakkamdhan Pakkamdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pakkamdhan Pakkamdhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal Nikkudhae Nikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Nikkudhae Nikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nambi Nee Munna Pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nambi Nee Munna Pogaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadha Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Undaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannoram Yen Kaneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Yen Kaneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbala Nee Kai Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbala Nee Kai Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannoram Yen Kaneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Yen Kaneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbala Nee Kai Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbala Nee Kai Seru"/>
</div>
</pre>
