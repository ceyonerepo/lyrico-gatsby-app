---
title: "ranamagi pochae song lyrics"
album: "Naruvi"
artist: "Christy"
lyricist: "Christy"
director: "Raja Muralidharan"
path: "/albums/naruvi-lyrics"
song: "Ranamagi Pochae"
image: ../../images/albumart/naruvi.jpg
date: 2021-10-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JEsJqztH274"
type: "sad"
singers:
  - Christy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ranamagi poche manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranamagi poche manase"/>
</div>
<div class="lyrico-lyrics-wrapper">paliyagi poche usure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paliyagi poche usure"/>
</div>
<div class="lyrico-lyrics-wrapper">tharisagi poche vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharisagi poche vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">usure usure adi en usure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usure usure adi en usure"/>
</div>
<div class="lyrico-lyrics-wrapper">usure usure adi en usure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usure usure adi en usure"/>
</div>
</pre>
