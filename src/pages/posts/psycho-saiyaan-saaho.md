---
title: "psycho saiyaan song lyrics"
album: "Saaho"
artist: "Tanishk Bagchi"
lyricist: "Sreejo"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Psycho Saiyaan"
image: ../../images/albumart/saaho-telugu.jpg
date: 2019-08-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/I7HwFFJGWhw"
type: "love"
singers:
  - Anirudh Ravichander
  - Dhvani Bhanushali
  - Tanishk Bagchi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagali Niku Chukkalu In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagali Niku Chukkalu In The Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyantha Mingenu Nidhurani Kalalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyantha Mingenu Nidhurani Kalalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nashe Mein Nannu Munchi Inka To Be Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nashe Mein Nannu Munchi Inka To Be Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise Mana Manasulu Waikoi Waikoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise Mana Manasulu Waikoi Waikoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhurupade Kallatho Mesmeraise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhurupade Kallatho Mesmeraise"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanne Nitho Take You High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanne Nitho Take You High"/>
</div>
<div class="lyrico-lyrics-wrapper">O Churakese Dil Thu Fire Fly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Churakese Dil Thu Fire Fly"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja Nuvu Nenu Chalu Chai ko Chai ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja Nuvu Nenu Chalu Chai ko Chai ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Saiyya Saiyya Ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Saiyya Saiyya Ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Sarasaki Saiyya Ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Sarasaki Saiyya Ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Chupulu Veiya Gundeni Koiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Chupulu Veiya Gundeni Koiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Zara Kaasko Kaasko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Zara Kaasko Kaasko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadika Saiyya Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadika Saiyya Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadika Saiyya Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadika Saiyya Psycho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talakindhulu Chese Mayakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talakindhulu Chese Mayakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupinchave Ni Maatallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupinchave Ni Maatallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Talakindhulu Chese Mayakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talakindhulu Chese Mayakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chupinchave Ni Maatallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupinchave Ni Maatallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangarendhukule Take It’S Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangarendhukule Take It’S Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaina Ee Kalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaina Ee Kalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">O Saiyya Saiyya Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Saiyya Saiyya Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikosam Thaiyyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikosam Thaiyyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikepudo Yaresa Vadhalanu Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikepudo Yaresa Vadhalanu Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Katha Raasko Raasko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Katha Raasko Raasko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadika Saiyaan Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadika Saiyaan Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadika Saiyaan Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadika Saiyaan Psycho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera Main Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabi Dabi Dum Main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabi Dabi Dum Main"/>
</div>
</pre>
