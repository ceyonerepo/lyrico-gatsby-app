---
title: "munnae po song lyrics"
album: "Akka Kuruvi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Samy"
path: "/albums/akka-kuruvi-lyrics"
song: "Munnae Po"
image: ../../images/albumart/akka-kuruvi.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/On-AewXdWOs"
type: "mass"
singers:
  - Anitha K
  - Dhivya S
  - Kavitha I
  - AS Namratha
  - C Sowmiya
  - Devu Treesa Mathew
  - Rasika V
  - Vaishnavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">munne po munne po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne po munne po"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnal thirumbi paarkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnal thirumbi paarkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munne po munne po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne po munne po"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram kandu malaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram kandu malaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">race kai odum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="race kai odum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaikaga odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaikaga odu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakendru oru idam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakendru oru idam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">un idathukai odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idathukai odu"/>
</div>
<div class="lyrico-lyrics-wrapper">race kai odum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="race kai odum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaikaga odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaikaga odu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakendru oru idam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakendru oru idam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">un idathukai odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un idathukai odu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri padi erinthudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri padi erinthudu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri kodi ettuthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri kodi ettuthikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un vetri vetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vetri vetri"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri padi erinthudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri padi erinthudu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri kodi ettuthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri kodi ettuthikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un vetri vetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vetri vetri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veetukul nee irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukul nee irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">petrorku pillai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petrorku pillai than"/>
</div>
<div class="lyrico-lyrics-wrapper">veliyil nee vanthu vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliyil nee vanthu vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">siragu ethum illai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragu ethum illai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kootathil neeyum oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootathil neeyum oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">endru endrum thalarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru endrum thalarathe"/>
</div>
<div class="lyrico-lyrics-wrapper">muneru munne sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muneru munne sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">valikaata thavarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valikaata thavarathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu latchiyangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu latchiyangal"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaalvil munne vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaalvil munne vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikkai vitudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikkai vitudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munne sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">kolgai un munne nirka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolgai un munne nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">kootam un pinnal nirka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam un pinnal nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiya sarithirathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiya sarithirathai"/>
</div>
<div class="lyrico-lyrics-wrapper">eluthi kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthi kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">yarachum enga ida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarachum enga ida"/>
</div>
<div class="lyrico-lyrics-wrapper">poyachu antha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyachu antha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum ne muthal idam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum ne muthal idam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sithadai pol aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithadai pol aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">sitrarai paainthu sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitrarai paainthu sel"/>
</div>
<div class="lyrico-lyrics-wrapper">vatratha nathiyai perugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatratha nathiyai perugi"/>
</div>
<div class="lyrico-lyrics-wrapper">katrai valamakki sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrai valamakki sel"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri sellum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri sellum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">pin illukum aal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin illukum aal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai ne mun vaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai ne mun vaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnal karkal kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnal karkal kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne pothum pathaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne pothum pathaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">aetram irakkam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aetram irakkam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">nirkamal neyum po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirkamal neyum po po"/>
</div>
<div class="lyrico-lyrics-wrapper">nindru vidathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nindru vidathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigal enna seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigal enna seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">udaithe podi podi aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaithe podi podi aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vilippai idathai endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilippai idathai endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vidathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vidathe"/>
</div>
<div class="lyrico-lyrics-wrapper">etharkum ini ethirthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharkum ini ethirthu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">mulaithe ne munne sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulaithe ne munne sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum ne muthal idam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum ne muthal idam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">munne po munne po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne po munne po"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnal thirumbi paarkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnal thirumbi paarkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">munne po munne po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne po munne po"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram kandu malaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram kandu malaikathe"/>
</div>
</pre>
