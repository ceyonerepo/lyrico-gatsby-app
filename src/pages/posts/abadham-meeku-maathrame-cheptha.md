---
title: "abadham song lyrics"
album: "Meeku Maathrame Cheptha"
artist: "Sivakumar"
lyricist: "AsurA"
director: "Shammeer Sultan"
path: "/albums/meeku-maathrame-cheptha-lyrics"
song: "Abadham"
image: ../../images/albumart/meeku-maathrame-cheptha.jpg
date: 2019-11-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yh-OR2oSdL8"
type: "happy"
singers:
  - Chandana Raju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Mundhu Rendu Dhaarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mundhu Rendu Dhaarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Batasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Batasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherchunu Ninnu Gootike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherchunu Ninnu Gootike"/>
</div>
<div class="lyrico-lyrics-wrapper">E Gunthalunna Dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Gunthalunna Dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinchu Ninnu Kaatike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinchu Ninnu Kaatike"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Thappulunna Aa Gaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Thappulunna Aa Gaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu Dheni Empiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu Dheni Empiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpadhuga Eh Badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpadhuga Eh Badi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Abadham Abadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Abadham Abadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe Antha Varake Andham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe Antha Varake Andham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Abadham Abadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Abadham Abadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Marche Chudu Kadha Lo Kadhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marche Chudu Kadha Lo Kadhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veluthuranthaa Daachipette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthuranthaa Daachipette"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizam Leni Needa Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizam Leni Needa Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappulanni Kappi Puche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappulanni Kappi Puche"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina Cheekati Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina Cheekati Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okokkati Allukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okokkati Allukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammukundhi Raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammukundhi Raathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothu Pothu Munchadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Pothu Munchadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Peeka Lothu Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Peeka Lothu Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Abadham Abadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Abadham Abadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe Antha Varake Andham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe Antha Varake Andham"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Abadham Abadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Abadham Abadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Marche Chudu Kadha Lo Kadhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marche Chudu Kadha Lo Kadhanam"/>
</div>
</pre>
