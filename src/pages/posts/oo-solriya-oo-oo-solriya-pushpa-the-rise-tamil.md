---
title: "oo solriya oo oo solriya song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Sukumar"
path: "/albums/pushpa-the-rise-song-lyrics"
song: "Oo Solriya Oo Oo Solriya"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3Tnf9AxEtnc"
type: "happy"
singers:
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sela Sela Sela Katna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Sela Sela Katna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru Kuru Kurunun Paappaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuru Kuru Kurunun Paappaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutta Kutta Gown-u Poatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutta Kutta Gown-u Poatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurukka Marukka Paappaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurukka Marukka Paappaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sela Blouse-o Chinna Gown-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Blouse-o Chinna Gown-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Dressuila onnum Illinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dressuila onnum Illinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Vandha Suththi Suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vandha Suththi Suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alayaa Alayum Aambala Buththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayaa Alayum Aambala Buththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Colour-Ah Irukkum Ponna Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colour-Ah Irukkum Ponna Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakku Pannu Thudipaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku Pannu Thudipaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppa Irukkum Ponna Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppa Irukkum Ponna Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaya Irukkunu Solvaangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaya Irukkunu Solvaangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Colour-u Karuppu Maaniramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colour-u Karuppu Maaniramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerathila Onnum Illinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerathila Onnum Illinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeni Sakkara Kattiya Suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeni Sakkara Kattiya Suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerumba Thiriyum Aambla Buththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerumba Thiriyum Aambla Buththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeyy Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeyy Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettayaaga Valandha Ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettayaaga Valandha Ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimindhu Nimindhu Paappaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimindhu Nimindhu Paappaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttayaaga Irukkum Ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttayaaga Irukkum Ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninju Valanju Paappaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuninju Valanju Paappaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netta Ponno Kutta Ponno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netta Ponno Kutta Ponno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam Ellaam Onnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Ellaam Onnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Degam Yellaam Mogam Muththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Degam Yellaam Mogam Muththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudan Yengum Aambala Buththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudan Yengum Aambala Buththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeyy Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeyy Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhukka Mozhukka Valantha Ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhukka Mozhukka Valantha Ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummunu Irukku Solvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummunu Irukku Solvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchchi Odamba Kaari Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchchi Odamba Kaari Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchchidhammunu Vazhivaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchchidhammunu Vazhivaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhu Kozhu Odamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhu Kozhu Odamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchchi Odambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchchi Odambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sizela Onnum Illinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sizela Onnum Illinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Alwa Maari Azhaga Chuththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alwa Maari Azhaga Chuththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla Thudikkum Aambala Buththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla Thudikkum Aambala Buththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeyy Oo Solriya Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeyy Oo Solriya Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Periya Periya Manushanunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Periya Manushanunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Silar Inga Varuvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Silar Inga Varuvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhukka Munna Naane Dhaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhukka Munna Naane Dhaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olari Silaru Thireevaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olari Silaru Thireevaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhukka Seelan Osantha Manishan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhukka Seelan Osantha Manishan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya Podum Veshamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Podum Veshamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakka Anachaa Podhum Yellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakka Anachaa Podhum Yellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hu Hu Hu Hu Velaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hu Hu Hu Hu Velaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Anachaa Podhum Yellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anachaa Podhum Yellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakku Maarum Onnuthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakku Maarum Onnuthaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Solvame Paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solvame Paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solvama Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solvama Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Solvame Paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solvame Paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solvama Paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solvama Paapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Solriya Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Solriya Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Solriya Maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Solriya Maamaa"/>
</div>
</pre>
