---
title: "maaman magale song lyrics"
album: "Kuttram Kuttrame"
artist: "Ajesh"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/kuttram-kuttrame-lyrics"
song: "Maaman Magale"
image: ../../images/albumart/kuttram-kuttrame.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jTbbyx5lK2s"
type: "love"
singers:
  - Benny Dayal
  - Pravin Saivi
  - Ajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathula Un Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula Un Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunkallula Un Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunkallula Un Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi Yen Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi Yen Pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda Kadha Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda Kadha Pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakula Un Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakula Un Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Nenjula Un Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Nenjula Un Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polladha Un Moochil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha Un Moochil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallandhey Uyir Koosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallandhey Uyir Koosum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Un Kolusu Mani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Un Kolusu Mani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Poli Podudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Poli Podudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasu Vedikkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu Vedikkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi Podavayume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Podavayume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Virala Kekkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Virala Kekkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Munnaala Ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Munnaala Ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manvaasam Thookkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manvaasam Thookkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Valavi Kaatura Kaatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Valavi Kaatura Kaatula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaala Odanjene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaala Odanjene"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinjukka Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinjukka Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedham Nedham Tholanjene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedham Nedham Tholanjene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panangalla Kudichadhu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panangalla Kudichadhu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambula Oru Medhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambula Oru Medhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyavum Chollaavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyavum Chollaavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Pala Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Pala Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyiloda Mazha Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyiloda Mazha Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thaanaa Thalumbudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thaanaa Thalumbudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyil Koova Elangaathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyil Koova Elangaathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohesa Maari Thirumbudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohesa Maari Thirumbudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madimela Sarinjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madimela Sarinjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thoga Varududhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thoga Varududhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Una Naalum Thona Saendhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Naalum Thona Saendhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Varame Tharume Sogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Varame Tharume Sogame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu Vidiyummattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu Vidiyummattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla Koovu Koovu Kuyile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla Koovu Koovu Kuyile "/>
</div>
</pre>
