---
title: "poraali anthem song lyrics"
album: "Traffic Ramasamy"
artist: "Balamurali Balu"
lyricist: "Muthamizh - Tee Jay - Adithya Surendar"
director: "Vicky"
path: "/albums/traffic-ramasamy-lyrics"
song: "Poraali Anthem"
image: ../../images/albumart/traffic-ramasamy.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_lRSxM7JmcQ"
type: "mass"
singers:
  - Deva
  - Ramya NSK
  - MC Vickey
  - Tee Jay
  - Varsha Ranjith
  - Adithya Surendar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nooru karangal adikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru karangal adikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram kaalgal mithikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram kaalgal mithikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">lacham padaigal ethirkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lacham padaigal ethirkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">sornthidathe thadaigalai parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sornthidathe thadaigalai parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mothi malaiyai thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mothi malaiyai thaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi oodu vetrii munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi oodu vetrii munne"/>
</div>
<div class="lyrico-lyrics-wrapper">palavenum vitu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palavenum vitu "/>
</div>
<div class="lyrico-lyrics-wrapper">balasaliyai kaaliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balasaliyai kaaliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ontrai maram thoppu aagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ontrai maram thoppu aagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">nan otha maram than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan otha maram than"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum aala maram da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum aala maram da"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram paravaigal kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram paravaigal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum saranalayam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum saranalayam than"/>
</div>
<div class="lyrico-lyrics-wrapper">alinju olinju veelthida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alinju olinju veelthida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalthu than katuven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalthu than katuven da"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam ondru potu vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam ondru potu vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">komaliyan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komaliyan than"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi matha kelvi ketal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi matha kelvi ketal"/>
</div>
<div class="lyrico-lyrics-wrapper">poraali enban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraali enban "/>
</div>
<div class="lyrico-lyrics-wrapper">illatha mallu katti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha mallu katti "/>
</div>
<div class="lyrico-lyrics-wrapper">thimurai neeyum katu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimurai neeyum katu da"/>
</div>
<div class="lyrico-lyrics-wrapper">veetuku pillai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetuku pillai pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">mathilai thiranthu katu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathilai thiranthu katu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sornthidathe thadaigalai parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sornthidathe thadaigalai parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mothi malaiyai thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mothi malaiyai thaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi oodu vetrii munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi oodu vetrii munne"/>
</div>
<div class="lyrico-lyrics-wrapper">palavenum vitu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palavenum vitu "/>
</div>
<div class="lyrico-lyrics-wrapper">balasaliyai kaaliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balasaliyai kaaliyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mun sellada velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun sellada velvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thannale neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannale neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">poo kallinal ambai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo kallinal ambai"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri paayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri paayada"/>
</div>
<div class="lyrico-lyrics-wrapper">tharum sothanaigal yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharum sothanaigal yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini sathanaigal aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini sathanaigal aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu paathai podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu paathai podu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai kaviya thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai kaviya thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee endru sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee endru sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sornthidathe thadaigalai parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sornthidathe thadaigalai parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mothi malaiyai thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mothi malaiyai thaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi oodu vetri munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi oodu vetri munne"/>
</div>
<div class="lyrico-lyrics-wrapper">palavenum vitu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palavenum vitu "/>
</div>
<div class="lyrico-lyrics-wrapper">balasaliyai kaaliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balasaliyai kaaliyai"/>
</div>
</pre>
