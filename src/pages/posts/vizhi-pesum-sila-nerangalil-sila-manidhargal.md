---
title: "vizhi pesum song lyrics"
album: "Sila Nerangalil Sila Manidhargal"
artist: "Radhan"
lyricist: "RJ Vijay"
director: "Vishal Venkat"
path: "/albums/sila-nerangalil-sila-manidhargal-lyrics"
song: "Vizhi Pesum"
image: ../../images/albumart/sila-nerangalil-sila-manidhargal.jpg
date: 2022-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SZCorYjtaUg"
type: "love"
singers:
  - Chinmayi
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhi pesum mozhi innum neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi pesum mozhi innum neelumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai pala arthangal thondrumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai pala arthangal thondrumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralgal thaakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralgal thaakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamarai vaadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamarai vaadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnai seravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai seravey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil kolamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil kolamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovangal yaavumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovangal yaavumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanalaai maarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaai maarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkangal kaathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkangal kaathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viral pada udaindhidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral pada udaindhidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagey nee illaipaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagey nee illaipaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru megam naan pidithidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru megam naan pidithidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhorum kadhai pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhorum kadhai pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavai naan vaangava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai naan vaangava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kulithu thalai kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kulithu thalai kaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal kaatrai naan azhaithidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal kaatrai naan azhaithidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithirayil unnai kaakha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithirayil unnai kaakha"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyanai thallava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyanai thallava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaaga naanum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaaga naanum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un badhil varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un badhil varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathil unmaarbil saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathil unmaarbil saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam adhu tharumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam adhu tharumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal korthu kaadhal seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal korthu kaadhal seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin varamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil vaazhum indh vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil vaazhum indh vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhaal sugamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhaal sugamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaipaaga varum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaipaaga varum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parka vaasal varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parka vaasal varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhundhin oli ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhundhin oli ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Netripottu soodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netripottu soodavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee urangum en madiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee urangum en madiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalainayaai maatridava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalainayaai maatridava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nal iravil nee sinunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nal iravil nee sinunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaatum paadava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaatum paadava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyai pola thaaram neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyai pola thaaram neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhai uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhai uravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu vaazhum naatkal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu vaazhum naatkal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin padhivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin padhivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai anbil minjum oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai anbil minjum oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil varuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai endru ennai sollli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai endru ennai sollli"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamum tharuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamum tharuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi pesum mozhi innum neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi pesum mozhi innum neelumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai pala arthangal thondrumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai pala arthangal thondrumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralgal thaakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralgal thaakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamarai vaadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamarai vaadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnai seravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai seravey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil kolamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil kolamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovangal yaavumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovangal yaavumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanalaai maarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaai maarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkangal kaathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkangal kaathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viral pada udaindhidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral pada udaindhidumo"/>
</div>
</pre>
