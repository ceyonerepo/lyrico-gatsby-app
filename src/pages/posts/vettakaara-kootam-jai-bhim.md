---
title: "vettakaara kootam song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Vettakaara Kootam"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mslP-LJ3usQ"
type: "happy"
singers:
  - Anthony Daasan
  - Niranjana Ramanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ingadoi Angadoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingadoi Angadoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingadoi Ingadoi Angadoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingadoi Ingadoi Angadoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettakaara Kootam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara Kootam Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Villiyarum Naanga Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villiyarum Naanga Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Otta Koorayila Vanathai Pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Koorayila Vanathai Pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Desingu Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Desingu Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kaata Metta Kaapom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaata Metta Kaapom Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Pottu Uzhaippom Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pottu Uzhaippom Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta Kaatilum Ooththa Surakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Kaatilum Ooththa Surakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththaikaaranga Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththaikaaranga Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazha Porantha Kootam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha Porantha Kootam Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguliyaana Jananga Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguliyaana Jananga Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Kuraiya Iruntha Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Kuraiya Iruntha Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu Poduvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu Poduvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vettakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vettakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Villiyarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Villiyarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettakaara Kootam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara Kootam Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Villiyarum Naanga Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villiyarum Naanga Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Otta Koorayila Vanathai Pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Koorayila Vanathai Pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Desingu Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Desingu Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kaata Metta Kaapom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaata Metta Kaapom Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Pottu Uzhaippom Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pottu Uzhaippom Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta Kaatilum Ooththa Surakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Kaatilum Ooththa Surakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththaikaaranga Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththaikaaranga Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingadoi Angadoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingadoi Angadoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingadoi Angadoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingadoi Angadoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalana Un Pechila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalana Un Pechila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagasathai Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagasathai Kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pola Un Kai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pola Un Kai Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoologathai Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoologathai Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maarula Sanjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maarula Sanjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thookkathai Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thookkathai Kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooda Un Moochila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooda Un Moochila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Koochathai Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Koochathai Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta Kozhi Nee Menjida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Kozhi Nee Menjida"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa Kaattaiyum Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa Kaattaiyum Kaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Pottu Nee Thaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Pottu Nee Thaangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenmam Yelaiyum Kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Yelaiyum Kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanathathai Kandevita Nikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanathathai Kandevita Nikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nitham Podu Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nitham Podu Koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettakaara Kootam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara Kootam Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Villiyarum Naanga Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villiyarum Naanga Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Otta Koorayila Vanathai Pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Koorayila Vanathai Pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Desingu Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Desingu Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kaata Metta Kaapom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaata Metta Kaapom Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Pottu Uzhaippom Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pottu Uzhaippom Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta Kaatilum Ooththa Surakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Kaatilum Ooththa Surakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththaikaaranga Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththaikaaranga Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazha Porantha Kootam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha Porantha Kootam Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguliyaana Jananga Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguliyaana Jananga Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Kuraiya Iruntha Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Kuraiya Iruntha Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu Poduvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu Poduvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Villiyarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Villiyarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettakaara Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Villiyarum Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Villiyarum Kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettakaara Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettakaara Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Villiyarum Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Villiyarum Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vettakaara Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vettakaara Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Villiyarum Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Villiyarum Kootam"/>
</div>
</pre>
