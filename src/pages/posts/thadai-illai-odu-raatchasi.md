---
title: "thadai illai odu song lyrics"
album: "Raatchasi"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "Sy Gowthamraj"
path: "/albums/raatchasi-lyrics"
song: "Thadai Illai Odu"
image: ../../images/albumart/raatchasi.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/c1ZhwHQEEho"
type: "happy"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Odu Thadai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Thadai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Unakku Thadai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Unakku Thadai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninthittaal Illai Kaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthittaal Illai Kaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkillai Yellai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkillai Yellai Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyai Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyai Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Odinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Odinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai maedellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai maedellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kundruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kundruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paayayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paayayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padum Paadaellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padum Paadaellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Aadaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Aadaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninthittaal Illai Kaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthittaal Illai Kaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkillai Yellai Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkillai Yellai Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Thadai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Thadai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viru Viru Viru Nadayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viru Viru Viru Nadayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthaal Tholaivaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthaal Tholaivaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedu Yedu Yedu Urudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu Yedu Yedu Urudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilae Uyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Uyaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhu Yedhu Yedhu Sariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu Yedhu Yedhu Sariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilae Nadaipoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Nadaipoodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orumurai Unai Unaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumurai Unai Unaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraiyum Thavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiyum Thavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maramae Ooyindhu Pooyinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramae Ooyindhu Pooyinum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru dhanai Yaerkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru dhanai Yaerkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalae Kaaindhu Pooyenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalae Kaaindhu Pooyenum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kana Vendrum Theeraadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana Vendrum Theeraadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Vanthaalum Adhu Ooyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Vanthaalum Adhu Ooyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninthittaal Illai Kaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthittaal Illai Kaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkillai Yellai Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkillai Yellai Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyai Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyai Veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Odinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Odinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai maedellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai maedellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kundruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kundruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paayayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paayayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padum Paadaellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padum Paadaellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Aadaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Aadaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninthittaal Illai Kaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthittaal Illai Kaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Illai Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkillai Yellai Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkillai Yellai Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisayettum Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisayettum Thaedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odu Thadai Illai Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Thadai Illai Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Unakku Thadai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Unakku Thadai Illai"/>
</div>
</pre>
