---
title: "siragai song lyrics"
album: "Hey Sinamika"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Brinda"
path: "/albums/hey-sinamika-lyrics"
song: "Siragai"
image: ../../images/albumart/hey-sinamika.jpg
date: 2022-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jWFQUrvPN6s"
type: "happy"
singers:
  - Keerthana Vaidyanathan
  - Sai Prabha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poo poovaye acham yenadi vetkam yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo poovaye acham yenadi vetkam yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyuthirthu aadavaavena azhaikkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyuthirthu aadavaavena azhaikkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne munne vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne munne vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjile kottum thaalavum unthan thaalavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjile kottum thaalavum unthan thaalavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inainth aadida kaalam tholida azhaikkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainth aadida kaalam tholida azhaikkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam pole neeyum aadaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam pole neeyum aadaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli meenaay thulli aadaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli meenaay thulli aadaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">India polum endre aalaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="India polum endre aalaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathakeyum pothum aadaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathakeyum pothum aadaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraayo mede neraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraayo mede neraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragai siragai viri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai siragai viri"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraya siraya piri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraya siraya piri"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai paravaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai paravaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai paravai parai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai paravai parai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravai iravai kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravai iravai kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhavai idhavai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhavai idhavai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnodum mannodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodum mannodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodum vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodum vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragai siragai viri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai siragai viri"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraya siraya piri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraya siraya piri"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai paravaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai paravaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai paravai parai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai paravai parai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravai iravai kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravai iravai kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhavai idhavai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhavai idhavai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnodum mannodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodum mannodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodum vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodum vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalilalaai oo oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalilalaai oo oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan mele nilavaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mele nilavaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kannilaadaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kannilaadaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmele puyalaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmele puyalaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarirul neekum theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarirul neekum theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil yezhnthu nee aadu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil yezhnthu nee aadu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraiyil modhum maramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraiyil modhum maramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola veezhnthaadadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola veezhnthaadadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalile kangal moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalile kangal moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vezhnthaayo engum thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vezhnthaayo engum thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhum varai vaazhve pirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhum varai vaazhve pirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhntha sirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhntha sirai"/>
</div>
</pre>
