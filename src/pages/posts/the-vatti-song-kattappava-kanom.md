---
title: "the vatti song song lyrics"
album: "Kattappava Kanom"
artist: "Santhosh Dhayanidhi"
lyricist: "Muthamil"
director: "Mani Seiyon"
path: "/albums/kattappava-kanom-lyrics"
song: "The Vatti Song"
image: ../../images/albumart/kattappava-kanom.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZxIskSeHqpw"
type: "happy"
singers:
  -	Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa Vatti Vitu Vecha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Vatti Vitu Vecha "/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti Potu Nikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Potu Nikum "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Patadhelam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Patadhelam "/>
</div>
<div class="lyrico-lyrics-wrapper">veenaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotaa Nee Ketaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaa Nee Ketaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mota Thalaikum Muti Kaalukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mota Thalaikum Muti Kaalukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudicha Izhuthu Poduran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudicha Izhuthu Poduran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitha Kaaturan Thitam Theeturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitha Kaaturan Thitam Theeturan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vita Edatha Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vita Edatha Theduranda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ucha Katta Aasadhaa Aasadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucha Katta Aasadhaa Aasadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Kaalil Nikuran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Kaalil Nikuran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Muti Vechu Namba Veika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muti Vechu Namba Veika"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Poraan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Poraan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatti Vitu Vecha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatti Vitu Vecha "/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti Potu Nikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Potu Nikum "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Patadhelam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Patadhelam "/>
</div>
<div class="lyrico-lyrics-wrapper">veenaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotaa Nee Ketaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotaa Nee Ketaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Romba Veeram Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Romba Veeram Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaha Sooram Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaha Sooram Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaiku Poga Poga Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiku Poga Poga Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadungudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadungudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Naan Onnu Nenaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naan Onnu Nenaikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Onnu Nenaikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Onnu Nenaikira"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukum Adangaama Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukum Adangaama Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Nadakudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Nadakudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraluku Oru Pakkam Idi Dhaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraluku Oru Pakkam Idi Dhaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Suthi Unna Suthi Adi Dhaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suthi Unna Suthi Adi Dhaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiya Killi Vitu Vetiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiya Killi Vitu Vetiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotila Aaturada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotila Aaturada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mota Thalaikum Muti Kaalukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mota Thalaikum Muti Kaalukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudicha Izhuthu Poduran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudicha Izhuthu Poduran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitha Kaaturan Thitam Theeturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitha Kaaturan Thitam Theeturan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vita Edatha Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vita Edatha Theduranda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Yanaiku Thumbika Ivanuko Nambika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yanaiku Thumbika Ivanuko Nambika"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerithaan Poga Poga Yeni Sarukudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerithaan Poga Poga Yeni Sarukudhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutaala Yosichi Ketaane Ipodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutaala Yosichi Ketaane Ipodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munn Vecha Kaala Pinna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munn Vecha Kaala Pinna "/>
</div>
<div class="lyrico-lyrics-wrapper">Eduka Mudiyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduka Mudiyalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badhiluku Badhilu Theriyala Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhiluku Badhilu Theriyala Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhingi Paarthum Mudiyala Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhingi Paarthum Mudiyala Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinadha Chinadha Serthu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinadha Chinadha Serthu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikalaa Seerudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikalaa Seerudhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mota Thalaikum Muti Kaalukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mota Thalaikum Muti Kaalukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudicha Izhuthu Poduran Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudicha Izhuthu Poduran Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitha Kaaturan Thitam Theeturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitha Kaaturan Thitam Theeturan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vita Edatha Theduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vita Edatha Theduranda"/>
</div>
</pre>
