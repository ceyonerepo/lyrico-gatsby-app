---
title: "desame song lyrics"
album: "Boomerang"
artist: "Radhan"
lyricist: "Vivek"
director: "R. Kannan"
path: "/albums/boomerang-lyrics"
song: "Desame"
image: ../../images/albumart/boomerang.jpg
date: 2019-03-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FLe8EkCE664"
type: "love"
singers:
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Desame Kan Mulichukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desame Kan Mulichukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kootamaai Kai Pudichuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kootamaai Kai Pudichuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per Vendaam Madham Saadhi Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Vendaam Madham Saadhi Vendaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Onnaakki Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Onnaakki Kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulum Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulum Andha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Government Um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Government Um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Usarathil Ingey Irukku Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Usarathil Ingey Irukku Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serntha Thaan Nedundhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serntha Thaan Nedundhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurala Nee Yetthi  Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurala Nee Yetthi  Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram Sedi Manushana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Sedi Manushana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marupadi Vethaichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi Vethaichiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Vandha Orey Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Vandha Orey Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodiyila Mudichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyila Mudichiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadi Vechu Kalachida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Vechu Kalachida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenachida Seeriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachida Seeriduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalkarai Sollum Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalkarai Sollum Kathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarithiram Aakiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Aakiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kootam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kootam Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Koochal Pogaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koochal Pogaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivasaayi Illaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasaayi Illaiyena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Kai Virippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Kai Virippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Avan Vazhva Sorandaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Avan Vazhva Sorandaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadathu Thol Urippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadathu Thol Urippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaveri Illayena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaveri Illayena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervaiyil Neer Eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyil Neer Eduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Orunaalu Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Orunaalu Enga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaiyya Meeteduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyya Meeteduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathodu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathodu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Vandhuttom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Vandhuttom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kammayila Meena Mungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammayila Meena Mungi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhuttom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuttom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nelloda Nizhala Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nelloda Nizhala Nambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhuttom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuttom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Mannoda Madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Mannoda Madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namba Porul Naasamaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Porul Naasamaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga Vera Naatil Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Vera Naatil Poi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallarasu Aakuromey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallarasu Aakuromey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarathula Ettu Naalu Angey Velai Senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarathula Ettu Naalu Angey Velai Senju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namba Aaviya Pookuromey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Aaviya Pookuromey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Laabatha Yethurome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Laabatha Yethurome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Corporate Cheap Rateu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corporate Cheap Rateu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Adimai Thalaigala Seiyudhu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Adimai Thalaigala Seiyudhu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Noteu Kai Naata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Noteu Kai Naata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orru Attai Poochiya Thinnudhu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orru Attai Poochiya Thinnudhu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Unnai Jaichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Unnai Jaichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimir Odambu Vervayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimir Odambu Vervayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnudhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnudhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Illa Namm Mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Illa Namm Mannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethaichadha Aruthukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethaichadha Aruthukkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru Thandha Amma Iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Thandha Amma Iva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalapaiya Vanangikka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapaiya Vanangikka da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhesamey Kan Mulichukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesamey Kan Mulichukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Koottamaai Kai Pudichukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Koottamaai Kai Pudichukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per Vendam Madham Saadhi Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Vendam Madham Saadhi Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Onnaakki Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Onnaakki Kaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulum Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulum Andha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Government Um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Government Um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Usarathil Ingey Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Usarathil Ingey Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serntha Thaan Nedundhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serntha Thaan Nedundhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulum Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulum Andha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Government Um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Government Um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Usarathil Ingey Irukku Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Usarathil Ingey Irukku Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serntha Thaan Nedundhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serntha Thaan Nedundhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurala Nee Yetthi  Kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurala Nee Yetthi  Kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram Sedi Manushana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Sedi Manushana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marupadi Vethaichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi Vethaichiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Vandha Orey Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Vandha Orey Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodiyila Mudichiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyila Mudichiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadi Vechu Kalachida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Vechu Kalachida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenachida Seeriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachida Seeriduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalkarai Sollum Kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalkarai Sollum Kathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarithiram Aakiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Aakiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivasaayi Illaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasaayi Illaiyena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Kai Virippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Kai Virippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Avan Vazhva Sorandaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Avan Vazhva Sorandaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadathu Thol Urippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadathu Thol Urippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaveri Illayena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaveri Illayena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervaiyil Neer Eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyil Neer Eduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Orunaalu Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Orunaalu Enga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaiyya Meeteduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyya Meeteduppom"/>
</div>
</pre>
