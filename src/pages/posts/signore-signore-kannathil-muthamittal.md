---
title: "signore signore song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Signore Signore"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_F8BDZavRx8"
type: "happy"
singers:
  - Swarnalatha
  - Rafique
  - Noel James
  - Anupama
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Salli Thanda Ponga Malliyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Salli Thanda Ponga Malliyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senyorey Senyorey Senyorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senyorey Senyorey Senyorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaju Kamayil Kaello Paarkalam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaju Kamayil Kaello Paarkalam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senyorey Senyorey Senyorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senyorey Senyorey Senyorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pajavu Pottu Pajavu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pajavu Pottu Pajavu Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theadalam Vaa Vaa.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theadalam Vaa Vaa."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaju Penney Gaju Penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaju Penney Gaju Penney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyenna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyenna Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaju Penney Gaju Penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaju Penney Gaju Penney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyenna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyenna Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Pattu Viral Thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Pattu Viral Thotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyum Serkathey Dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyum Serkathey Dee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Etti Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Etti Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannai Muzhichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannai Muzhichchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Nee Paakara Paakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Nee Paakara Paakara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Malli Malli Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Malli Malli Malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kello Kollo Yaaluvenna Aatharenga Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kello Kollo Yaaluvenna Aatharenga Malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalukum Kamaththukum Venum Idaveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalukum Kamaththukum Venum Idaveli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Salli Thanda Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Salli Thanda Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malliyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Salli Thanda Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Salli Thanda Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malliyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senyorey Senyorey Senyorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senyorey Senyorey Senyorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolpaes Kadaloram Pogalam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolpaes Kadaloram Pogalam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senyorey Senyorey Senyorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senyorey Senyorey Senyorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudaikku Keezhey Jodi Theadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaikku Keezhey Jodi Theadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulavalaam Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulavalaam Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karukelley Karukelley Perenna Dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukelley Karukelley Perenna Dee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karukelley Karukelley  Perenna Dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukelley Karukelley  Perenna Dee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaloram Kaikorthu Pogalam Vaa Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaloram Kaikorthu Pogalam Vaa Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Angey Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Angey Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Purushanai Kootitu Vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Purushanai Kootitu Vaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Haiyo Nangi Nangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Haiyo Nangi Nangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavaripochu Thavaripochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaripochu Thavaripochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enn Thangachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn Thangachchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machchana Naan Ketadhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchana Naan Ketadhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Thangachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Thangachchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Pakadi Panni Asadu Vazhiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Pakadi Panni Asadu Vazhiyidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yo Yaalu Venna Yenna Yaaluoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo Yaalu Venna Yenna Yaaluoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Salli Thanda Poanga Malliyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Salli Thanda Poanga Malliyey"/>
</div>
</pre>
