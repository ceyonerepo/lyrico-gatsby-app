---
title: "veyyon silli song lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "Vivek"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Veyyon Silli"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-11-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0f_ho4Wem0w"
type: "love"
singers:
  - Harish Sivaramakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Seeyan sirukkikitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Seeyan sirukkikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Seevana tholachchiten
<input type="checkbox" class="lyrico-select-lyric-line" value="Seevana tholachchiten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottu valavikkulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Sottu valavikkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikka valanchiten
<input type="checkbox" class="lyrico-select-lyric-line" value="Maattikka valanchiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ulla pattraya pottuttu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla pattraya pottuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezharaiya kootittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezharaiya kootittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappichu poraalae angittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thappichu poraalae angittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iva veedhiyil vaaratha
<input type="checkbox" class="lyrico-select-lyric-line" value="Iva veedhiyil vaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkai paakathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedikkai paakathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluntha meganga embuttu
<input type="checkbox" class="lyrico-select-lyric-line" value="Viluntha meganga embuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idukkiyae.. yae…yae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idukkiyae.. yae…yae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Idukkiyae..yae….yae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Idukkiyae..yae….yae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkiraa.. adikkiraa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Adikkiraa.. adikkiraa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Adukkiyae..adukkiyae..
<input type="checkbox" class="lyrico-select-lyric-line" value="Adukkiyae..adukkiyae.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veyyon silli ippo nilathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Veyyon silli ippo nilathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi anaththuraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Irangi anaththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Landhaa pesi enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Landhaa pesi enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oranda ezhukkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Oranda ezhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaari kannalae uttaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattaari kannalae uttaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Therikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaara sitaala mappaagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottaara sitaala mappaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="kedakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En usurula sallada salichchi
<input type="checkbox" class="lyrico-select-lyric-line" value="En usurula sallada salichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen sirikkira arakkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sirikkira arakkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kurukkula ennaiya mudinchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kurukkula ennaiya mudinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadakkura tharukkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nadakkura tharukkiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mallatta rendaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Mallatta rendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaattam vanthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennaattam vanthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooi.."/>
</div>
<div class="lyrico-lyrics-wrapper">En usurula sallada salichchi
<input type="checkbox" class="lyrico-select-lyric-line" value="En usurula sallada salichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen sirikkira arakkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sirikkira arakkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kurukkula ennaiya mudinchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kurukkula ennaiya mudinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadakkura tharukkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nadakkura tharukkiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyyyyy…. eyyy…..heyyyyy eyyyy eeyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyyyy…. eyyy…..heyyyyy eyyyy eeyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enkaathu javvula
<input type="checkbox" class="lyrico-select-lyric-line" value="Enkaathu javvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Esaaiyum ovvulaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Esaaiyum ovvulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum pesadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum pesadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhattum naaluttum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhattum naaluttum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum ungala ichonnu veesadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum ungala ichonnu veesadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannalu udhadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannalu udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalu thagadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnalu thagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thaanadi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku thaanadi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Sattaiyil pocketae thachathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattaiyil pocketae thachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaya padhukkathaanadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaya padhukkathaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thinna
<input type="checkbox" class="lyrico-select-lyric-line" value="Thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanam vechchu thinna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanam vechchu thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla un gokkamakka ninna
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla un gokkamakka ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooi.."/>
</div>
<div class="lyrico-lyrics-wrapper">En usurula sallada salichchi
<input type="checkbox" class="lyrico-select-lyric-line" value="En usurula sallada salichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen sirikkira arakkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sirikkira arakkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kurukkula ennaiya mudinchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kurukkula ennaiya mudinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadakkura tharukkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nadakkura tharukkiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thoratti korala peratti eviya
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoratti korala peratti eviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam parichiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayam parichiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Currentu kambiyaa sorandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Currentu kambiyaa sorandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidantha kathanda yerichiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kidantha kathanda yerichiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh padhanam udhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh padhanam udhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanam sethara
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavanam sethara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa kalachiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasa kalachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karukka poluthil
<input type="checkbox" class="lyrico-select-lyric-line" value="Karukka poluthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichu tholachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirichu tholachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagala padachiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagala padachiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theeya iva vandhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeya iva vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda vellam thundaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Manda vellam thundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaa indha jigirthandaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Undaa indha jigirthandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooi..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooi.."/>
</div>
<div class="lyrico-lyrics-wrapper">En usurula sallada salichchi
<input type="checkbox" class="lyrico-select-lyric-line" value="En usurula sallada salichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen sirikkira arakkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sirikkira arakkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kurukkula ennaiya mudinchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kurukkula ennaiya mudinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadakkura tharukkiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nadakkura tharukkiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veyyon silli ippo nilathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Veyyon silli ippo nilathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi anaththuraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Irangi anaththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Landhaa pesi enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Landhaa pesi enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oranda ezhukkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Oranda ezhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaari kannalae uttaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattaari kannalae uttaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Therikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottaara sitaala mappaagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottaara sitaala mappaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="kedakkuren"/>
</div>
</pre>
