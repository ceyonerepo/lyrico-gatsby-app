---
title: "bakku bakku song lyrics"
album: "Thodraa"
artist: "RN Uthamaraja - Navin Shander"
lyricist: "Madhuraj"
director: "Madhuraj"
path: "/albums/thodraa-lyrics"
song: "Bakku Bakku"
image: ../../images/albumart/thodraa.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pAw2UwTgoJQ"
type: "love"
singers:
  - Silambarasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bakku bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakku bakkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenamum thikku thikkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum thikku thikkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu lappu tappunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu lappu tappunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku adikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku adikathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye laga laganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye laga laganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo labo thibonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo labo thibonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brainu pattu pattunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brainu pattu pattunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku vedikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mandaiyoda uchiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mandaiyoda uchiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho pannuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu onnumilla oru kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu onnumilla oru kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaadhal sonnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kaadhal sonnathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakku bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakku bakkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenamum thikku thikkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum thikku thikkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu lappu tappunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu lappu tappunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku adikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku adikathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla kattivechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla kattivechen"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangathukku koyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangathukku koyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla ivala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla ivala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumilla azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumilla azhagula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sammathatha solluvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathatha solluvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama nambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama nambala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangojama iruntha enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangojama iruntha enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshamthaan thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshamthaan thaangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaichalum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaichalum naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavilla valachenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavilla valachenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichalum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichalum naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraikka solli rasippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraikka solli rasippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna enni nethiyila pottu vechaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna enni nethiyila pottu vechaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhapora vazhkaikuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazhapora vazhkaikuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam pottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam pottalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vechi thachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vechi thachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enum noolula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enum noolula"/>
</div>
<div class="lyrico-lyrics-wrapper">Engirunthu vantha iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engirunthu vantha iva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennudaiya devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaiya devathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiram thandiram pannamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram thandiram pannamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakki putta maappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakki putta maappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmathana pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmathana pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathiputta shokkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathiputta shokkula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirichalum manala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirichalum manala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayiraaga thirichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayiraaga thirichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Methanthaalum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methanthaalum naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgathula methenthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathula methenthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna venum sollu pulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna venum sollu pulla "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thanthidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna alli eppothum un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna alli eppothum un"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil kannil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil kannil "/>
</div>
<div class="lyrico-lyrics-wrapper">kannil thanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil thanthidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakku bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakku bakkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku thikkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku thikkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey lappu thappunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey lappu thappunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu adikkuthu adikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu adikkuthu adikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Brainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brainu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku vedi vedi vedi vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku vedi vedi vedi vedikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaiyoda uchiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaiyoda uchiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku yetho pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakku yetho pannuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu onnumilla oru kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu onnumilla oru kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaadhal sonnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kaadhal sonnathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bakku bakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakku bakkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenamum thenamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum thenamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikku thikkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikku thikkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu lappu tappunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu lappu tappunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku adikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku adikathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye laga laganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye laga laganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo labo thibonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo labo thibonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brainu pattu pattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brainu pattu pattu "/>
</div>
<div class="lyrico-lyrics-wrapper">pattu pattu pattunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pattu pattunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku enakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku enakku "/>
</div>
<div class="lyrico-lyrics-wrapper">enakku vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku vedikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi haha"/>
</div>
</pre>
