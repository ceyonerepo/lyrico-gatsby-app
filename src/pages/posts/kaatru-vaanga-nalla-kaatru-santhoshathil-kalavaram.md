---
title: "kaatru vaanga nalla kaatru song lyrics"
album: "Santhoshathil Kalavaram"
artist: "Sivanag"
lyricist: "Mani Amudhavan"
director: "Kranthi Prasad"
path: "/albums/santhoshathil-kalavaram-lyrics"
song: "Kaatru Vaanga Nalla Kaatru"
image: ../../images/albumart/santhoshathil-kalavaram.jpg
date: 2018-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/L0Ivv3cavYs"
type: "happy"
singers:
  - Sooraj Santhosh
  - Rita Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe kaadu thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe kaadu thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vanthom vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vanthom vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puluthu kaatrai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthu kaatrai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugaiyin natram vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugaiyin natram vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thesam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thesam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ipothu neyum nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipothu neyum nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhi vaasi ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhi vaasi ada"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum ingu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum ingu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">yosi yosi thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yosi yosi thinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theerathu iyarkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerathu iyarkai"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi pesi maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi pesi maram"/>
</div>
<div class="lyrico-lyrics-wrapper">chediyai kodiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chediyai kodiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nesi nesi ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesi nesi ada"/>
</div>
<div class="lyrico-lyrics-wrapper">nee naane inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee naane inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe kaadu thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe kaadu thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vanthom vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vanthom vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puluthu kaatrai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthu kaatrai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugaiyin natram vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugaiyin natram vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thesam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thesam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ingulla paravai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingulla paravai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbargal aaki kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbargal aaki kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pesum baasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pesum baasai"/>
</div>
<div class="lyrico-lyrics-wrapper">katru kolvoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katru kolvoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanathu poochi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanathu poochi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nammodu serthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammodu serthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">poo meethu thottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo meethu thottil"/>
</div>
<div class="lyrico-lyrics-wrapper">katta solvoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta solvoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoongida veedu ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongida veedu ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai thookanang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai thookanang"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvi koodu pothatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvi koodu pothatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minsaara vilakugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minsaara vilakugal"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">poochi pothatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poochi pothatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">otukal ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otukal ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">semikka thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semikka thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pola inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pola inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">engum illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum illai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe kaadu thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe kaadu thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vanthom vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vanthom vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puluthu kaatrai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthu kaatrai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugaiyin natram vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugaiyin natram vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thesam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thesam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marangalai veti veti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marangalai veti veti"/>
</div>
<div class="lyrico-lyrics-wrapper">jannalgal seithu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jannalgal seithu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrai nee veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrai nee veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnal veesaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnal veesaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marangale illai endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marangale illai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">manitharku swasam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitharku swasam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">perinba kaadugalai alikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinba kaadugalai alikathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paravaikku thangam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaikku thangam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai pasiyaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai pasiyaara "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyangal pothatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyangal pothatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panam kaasai vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam kaasai vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu nilathai ilantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu nilathai ilantha"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaiku unakenna pasikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaiku unakenna pasikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manitharku matum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitharku matum "/>
</div>
<div class="lyrico-lyrics-wrapper">intha boomi than 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha boomi than "/>
</div>
<div class="lyrico-lyrics-wrapper">sontham illai kaathukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham illai kaathukul"/>
</div>
<div class="lyrico-lyrics-wrapper">aetru inthe sollai sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aetru inthe sollai sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe kaadu thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe kaadu thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vanthom vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vanthom vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puluthu kaatrai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthu kaatrai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugaiyin natram vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugaiyin natram vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thesam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thesam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ipothu neyum nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipothu neyum nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhi vaasi ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhi vaasi ada"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum ingu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum ingu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">yosi yosi thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yosi yosi thinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theerathu iyarkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerathu iyarkai"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi pesi maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi pesi maram"/>
</div>
<div class="lyrico-lyrics-wrapper">chediyai kodiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chediyai kodiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">nesi nesi ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesi nesi ada"/>
</div>
<div class="lyrico-lyrics-wrapper">nee naane inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee naane inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaatru vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaatru vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe kaadu thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe kaadu thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vanthom vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vanthom vanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puluthu kaatrai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puluthu kaatrai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugaiyin natram vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugaiyin natram vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thesam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thesam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vanthom"/>
</div>
</pre>
