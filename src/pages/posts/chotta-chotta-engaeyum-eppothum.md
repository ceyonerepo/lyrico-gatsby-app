---
title: "chotta chotta song lyrics"
album: "Engaeyum Eppothum"
artist: "C Sathya"
lyricist: "Na Muthukumar"
director: "M. Saravanan"
path: "/albums/engaeyum-eppothum-lyrics"
song: "Chotta Chotta"
image: ../../images/albumart/engaeyum-eppothum.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fDFe8b4rt9w"
type: "love"
singers:
  - Chinmayi
  - Sathya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chotta chotta nanaiya vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotta chotta nanaiya vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal kodhikka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal kodhikka vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaadha idathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaadha idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai parakka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai parakka vaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta thatta karaiya vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thatta karaiya vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittaamal alaiya vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittaamal alaiya vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittaamal thitti thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittaamal thitti thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal unara vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal unara vaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rayil varum paalamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayil varum paalamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo endhan idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo endhan idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thada thada thadavena thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada thada thadavena thudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru naal oru naal vithaiyaai vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru naal oru naal vithaiyaai vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthaai kannukkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaai kannukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi paarkkum pothae maramaai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi paarkkum pothae maramaai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthaai nenjukkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthaai nenjukkullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ini enna nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ini enna nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam nadanthathai nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam nadanthathai nadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuttippoonai polaKaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuttippoonai polaKaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">etti paarkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti paarkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu acham madam naanam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu acham madam naanam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti paarkkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti paarkkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkuthae paarkkuthae thorkkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkuthae paarkkuthae thorkkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha kadavul adadaa aangal nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kadavul adadaa aangal nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugil seithaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhugil seithaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu ovvoru nodiyum pennai kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu ovvoru nodiyum pennai kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugida vaithaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugida vaithaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha mounathin mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mounathin mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba pidikkuthu enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba pidikkuthu enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pechum moochum ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechum moochum ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakki vittu chendrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakki vittu chendrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vittu chendra nyaabagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vittu chendra nyaabagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Patri kondathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patri kondathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondathae kondathae vendrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondathae kondathae vendrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chotta chotta nanaiya vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotta chotta nanaiya vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal kodhikka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal kodhikka vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaadha idathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaadha idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai parakka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai parakka vaithaai"/>
</div>
</pre>
