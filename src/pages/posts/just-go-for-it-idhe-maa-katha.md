---
title: "just go for it song lyrics"
album: "Idhe Maa Katha"
artist: "Sunil Kashyup"
lyricist: "Giri Koduri"
director: "Guru Pawan"
path: "/albums/idhe-maa-katha-lyrics"
song: "Just Go For It"
image: ../../images/albumart/idhe-maa-katha.jpg
date: 2021-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3vxdLoBS_jk"
type: "happy"
singers:
  - Hema Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yevare Ni Anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevare Ni Anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethuredivunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuredivunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaaka Nuvvundi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaaka Nuvvundi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhooramaina Yekashtamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhooramaina Yekashtamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundalo Thaakinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundalo Thaakinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasane Nisamuka Palachupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasane Nisamuka Palachupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Time Kosam Vechi Chusavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Kosam Vechi Chusavo"/>
</div>
<div class="lyrico-lyrics-wrapper">You Can Never Fly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can Never Fly"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey Nuvve Adventure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey Nuvve Adventure"/>
</div>
<div class="lyrico-lyrics-wrapper">Reach The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reach The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduke Munduku Vesesei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduke Munduku Vesesei"/>
</div>
<div class="lyrico-lyrics-wrapper">Learn To Live
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Learn To Live"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Always Like A Race
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Always Like A Race"/>
</div>
<div class="lyrico-lyrics-wrapper">Change The Gear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Change The Gear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Koridhedo Chusesei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Koridhedo Chusesei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalanndi Sadinchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalanndi Sadinchey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundu Aduke Ika Vesesei Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundu Aduke Ika Vesesei Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevaru Em Anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevaru Em Anukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">You Don't Care
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Don't Care"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalsindi Ony Dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalsindi Ony Dare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gamyam Vaipe Chusesei Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam Vaipe Chusesei Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sawsinchi Nee Kaladevu Pirika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sawsinchi Nee Kaladevu Pirika"/>
</div>
<div class="lyrico-lyrics-wrapper">That Is He Yettam Edhuvey Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That Is He Yettam Edhuvey Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Madhuli Nu Chele Therali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Madhuli Nu Chele Therali"/>
</div>
<div class="lyrico-lyrics-wrapper">Start The Bike Kan Pogattuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start The Bike Kan Pogattuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevare Ni Anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevare Ni Anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethuredivunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuredivunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaaka Nuvvundi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaaka Nuvvundi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhooramaina Yekashtamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhooramaina Yekashtamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundalo Thaakinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundalo Thaakinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasane Nisamuka Palachupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasane Nisamuka Palachupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Time Kosam Vechi Chusavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Kosam Vechi Chusavo"/>
</div>
<div class="lyrico-lyrics-wrapper">You Can Never Fly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can Never Fly"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey Nuvve Adventure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey Nuvve Adventure"/>
</div>
<div class="lyrico-lyrics-wrapper">Reach The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reach The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduke Munduku Vesesei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduke Munduku Vesesei"/>
</div>
<div class="lyrico-lyrics-wrapper">Learn To Live
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Learn To Live"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Always Like A Race
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Always Like A Race"/>
</div>
<div class="lyrico-lyrics-wrapper">Change The Gear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Change The Gear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema Annadi Oh Amrutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Annadi Oh Amrutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Neeke Andhinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Neeke Andhinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedo Jeevam Nipposuruna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedo Jeevam Nipposuruna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Daamiki Chirunama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Daamiki Chirunama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhi Cheyutha Neetharama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhi Cheyutha Neetharama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalame Gamyam Chupinchenura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalame Gamyam Chupinchenura"/>
</div>
</pre>
