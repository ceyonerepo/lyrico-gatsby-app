---
title: "odu odu song lyrics"
album: "Bow Bow"
artist: "Marc D Muse - Denis Vallaban. A"
lyricist: "Muthamil - Manju - Shiva - Rahul Ghandi"
director: "S. Pradeep Kilikar"
path: "/albums/bow-bow-lyrics"
song: "Odu Odu"
image: ../../images/albumart/bow-bow.jpg
date: 2019-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IOVKr3DMkN4"
type: "sad"
singers:
  - Madhu balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">odu odu vazhkai unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu vazhkai unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedu thedu ellam kaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedu thedu ellam kaiyodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu vazhkai unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu vazhkai unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedu thedu ellam kaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedu thedu ellam kaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">pennin madiyil pirakum manithan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennin madiyil pirakum manithan "/>
</div>
<div class="lyrico-lyrics-wrapper">mannin adiyil mudinthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannin adiyil mudinthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">mutti modhum chedigal thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutti modhum chedigal thane"/>
</div>
<div class="lyrico-lyrics-wrapper">boomikellam nizhal tharume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikellam nizhal tharume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manithan thedi manithan pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithan thedi manithan pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai inge mudiyatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai inge mudiyatume"/>
</div>
<div class="lyrico-lyrics-wrapper">manithan thedi manithan poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithan thedi manithan poga"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai ellai viriyatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai ellai viriyatume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidiyal thedum ovoru iravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal thedum ovoru iravum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidintha piragum azhivathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidintha piragum azhivathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vithaigal ellam vizhithu vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithaigal ellam vizhithu vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">vanangalukum kavalai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanangalukum kavalai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aararivu ulla manithanin gnanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aararivu ulla manithanin gnanam"/>
</div>
<div class="lyrico-lyrics-wrapper">ainthilinge adangiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ainthilinge adangiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam podum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam podum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">manithanin kaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manithanin kaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradiyil ingu mudangiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradiyil ingu mudangiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai anbu ilagil perithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai anbu ilagil perithu"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai konda vazhkai arithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai konda vazhkai arithu"/>
</div>
<div class="lyrico-lyrics-wrapper">bandha pasam panathirku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandha pasam panathirku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">panathil mathipo pinathirku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panathil mathipo pinathirku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than vazhkaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than vazhkaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than vazhkaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than vazhkaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbai thavira veru yethilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai thavira veru yethilum"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagin artham therivathilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagin artham therivathilai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirgal idathil anbai kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirgal idathil anbai kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">sorgam thaniye thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorgam thaniye thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">avaigal endru solvathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaigal endru solvathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">avargalaga valam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avargalaga valam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">avargal peyaril vazhum manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avargal peyaril vazhum manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">avaigalaga thirivathu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaigalaga thirivathu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhu nada uyir kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhu nada uyir kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai udai padai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai udai padai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than vazhkaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than vazhkaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than vazhkaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than vazhkaiyada"/>
</div>
</pre>
