---
title: "thank you song lyrics"
album: "Krishna Rao Super Market"
artist: "Bhole Shavali"
lyricist: "Sai Siri"
director: "Sreenath Pulakuram"
path: "/albums/krishna-rao-super-market-lyrics"
song: "Thank You"
image: ../../images/albumart/krishna-rao-super-market.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BIKQpTpvtWw"
type: "love"
singers:
  - Prudhvi Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naa kallu lifelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kallu lifelona"/>
</div>
<div class="lyrico-lyrics-wrapper">first time ninnu choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first time ninnu choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">surprise tho naaku cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surprise tho naaku cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">thank youuu thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank youuu thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa ollu lifulona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa ollu lifulona"/>
</div>
<div class="lyrico-lyrics-wrapper">first time vibretho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first time vibretho"/>
</div>
<div class="lyrico-lyrics-wrapper">goose bumps naaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goose bumps naaku "/>
</div>
<div class="lyrico-lyrics-wrapper">cheppe thank youuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppe thank youuu"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa watchloni mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa watchloni mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">first time ninnu kalipi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first time ninnu kalipi"/>
</div>
<div class="lyrico-lyrics-wrapper">good time naaku cheppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="good time naaku cheppi"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">total ga naa manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="total ga naa manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">anni thanksu theesukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anni thanksu theesukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">prematho neeku cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prematho neeku cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">oo pillah thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo pillah thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oo pillaaah thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo pillaaah thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">naakenaa vellavehhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naakenaa vellavehhee"/>
</div>
<div class="lyrico-lyrics-wrapper">ohooo eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohooo eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">padipooyaadrooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padipooyaadrooo"/>
</div>
<div class="lyrico-lyrics-wrapper">asalemaindiraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asalemaindiraa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa choosi choodagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa choosi choodagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">maa madhyalo lekkaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa madhyalo lekkaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">sweetu memorilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sweetu memorilu"/>
</div>
<div class="lyrico-lyrics-wrapper">puttukocchinaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puttukocchinaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">abbhaa appude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abbhaa appude"/>
</div>
<div class="lyrico-lyrics-wrapper">magadeera story laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magadeera story laa"/>
</div>
<div class="lyrico-lyrics-wrapper">gathamlo thanakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gathamlo thanakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">youddale chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="youddale chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">prema gelichinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prema gelichinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">ahaa kshanaalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ahaa kshanaalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">yougaalenno daatinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yougaalenno daatinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">aa chupele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa chupele"/>
</div>
<div class="lyrico-lyrics-wrapper">swagathaalu palikinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swagathaalu palikinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kosame annattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kosame annattu"/>
</div>
<div class="lyrico-lyrics-wrapper">paivadu pampinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paivadu pampinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">prathiyakshamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathiyakshamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">kalala raanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalala raanike"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you ohh pillaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you ohh pillaah"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you ooo pillaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you ooo pillaaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ohh ak 47 gunlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh ak 47 gunlo"/>
</div>
<div class="lyrico-lyrics-wrapper">valapu thuutalu vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapu thuutalu vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">fastugaa pelchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fastugaa pelchi"/>
</div>
<div class="lyrico-lyrics-wrapper">roastu chesinaadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roastu chesinaadii"/>
</div>
<div class="lyrico-lyrics-wrapper">abbooo avunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abbooo avunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mostu wanted 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mostu wanted "/>
</div>
<div class="lyrico-lyrics-wrapper">criminalaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="criminalaina "/>
</div>
<div class="lyrico-lyrics-wrapper">thananu choosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thananu choosi "/>
</div>
<div class="lyrico-lyrics-wrapper">best lover man gaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="best lover man gaa "/>
</div>
<div class="lyrico-lyrics-wrapper">maare beautyradee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maare beautyradee "/>
</div>
<div class="lyrico-lyrics-wrapper">a gutidho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a gutidho "/>
</div>
<div class="lyrico-lyrics-wrapper">gorinka pilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gorinka pilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa gundelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa gundelo"/>
</div>
<div class="lyrico-lyrics-wrapper">goodu kattukunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goodu kattukunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">inthandamgundiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthandamgundiro"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kallu kuttero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kallu kuttero"/>
</div>
<div class="lyrico-lyrics-wrapper">naa sonthamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sonthamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">rojuke cheppukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rojuke cheppukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you u u u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you u u u "/>
</div>
<div class="lyrico-lyrics-wrapper">thank you u u u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you u u u"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you u u u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you u u u "/>
</div>
<div class="lyrico-lyrics-wrapper">ooo pillaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo pillaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">thank you thank you 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you thank you "/>
</div>
<div class="lyrico-lyrics-wrapper">thank you ooo phillaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thank you ooo phillaaah"/>
</div>
</pre>
