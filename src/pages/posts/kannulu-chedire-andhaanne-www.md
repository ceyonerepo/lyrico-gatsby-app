---
title: "kannulu chedire song lyrics"
album: "WWW"
artist: "Simon K King "
lyricist: "Anantha Sriram"
director: "K V Guhan"
path: "/albums/www-lyrics"
song: "Kannulu Chedire Andhaanne"
image: ../../images/albumart/www.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MIjqfO-RSGE"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannulu chedhire andhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulu chedhire andhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela terapai chusaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela terapai chusaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile kaaalaanne nimicham nilipesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile kaaalaanne nimicham nilipesaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannika neelo vidichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannika neelo vidichaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalu gaallo kalipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu gaallo kalipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude inkolaa ne mallee puttaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude inkolaa ne mallee puttaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kanulaa keratamulonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanulaa keratamulonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopulilaa muniginavemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulilaa muniginavemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkaane chepai ne theegalu leni ee vallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkaane chepai ne theegalu leni ee vallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nemmadhigaa nuvvodhile navvula gaalaallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nemmadhigaa nuvvodhile navvula gaalaallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulu chedhire andhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulu chedhire andhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela terapai chusaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela terapai chusaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile kaaalaanne nimicham nilipesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile kaaalaanne nimicham nilipesaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannika neelo vidichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannika neelo vidichaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalu gaallo kalipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu gaallo kalipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude inkolaa ne mallee puttaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude inkolaa ne mallee puttaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho nuvvochhi naa prapanchamouthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho nuvvochhi naa prapanchamouthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchame venakki pothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchame venakki pothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvichhina kalallo nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvichhina kalallo nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanthame thalonchukuntundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanthame thalonchukuntundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adagaale gaani jeevithamainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaale gaani jeevithamainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kshaname neekai raasichheynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kshaname neekai raasichheynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkaane chepai ne theegalu leni ee vallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkaane chepai ne theegalu leni ee vallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nemmadhigaa nuvvodhile navvula gaalaallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nemmadhigaa nuvvodhile navvula gaalaallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulu chedhire andhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulu chedhire andhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela terapai chusaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela terapai chusaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile kaaalaanne nimicham nilipesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile kaaalaanne nimicham nilipesaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannika neelo vidichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannika neelo vidichaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalu gaallo kalipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu gaallo kalipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude inkolaa ne mallee puttaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude inkolaa ne mallee puttaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O ho vayassulo erakka nenunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ho vayassulo erakka nenunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogassulo irukkupothunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogassulo irukkupothunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manassulo nijamgaa nee pere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassulo nijamgaa nee pere"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapassulaa smarinchukuntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapassulaa smarinchukuntunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhurai nee roopam ninchoni unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhurai nee roopam ninchoni unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Egirelli ningi anchuna untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egirelli ningi anchuna untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaake veellekunnaa ninnandhukuntunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaake veellekunnaa ninnandhukuntunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukaa thalukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalukaa thalukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannulu chedhire andhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulu chedhire andhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela terapai chusaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela terapai chusaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile kaaalaanne nimicham nilipesaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile kaaalaanne nimicham nilipesaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannika neelo vidichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannika neelo vidichaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalu gaallo kalipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu gaallo kalipaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude inkolaa ne mallee puttaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude inkolaa ne mallee puttaane"/>
</div>
</pre>
