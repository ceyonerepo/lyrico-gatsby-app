---
title: "keelu gurram song lyrics"
album: "Middle Class Melodies"
artist: "Sweekar Agasthi"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Vinod Anantoju"
path: "/albums/middle-class-melodies-lyrics"
song: "Keelu Gurram"
image: ../../images/albumart/middle-class-melodies.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X5d22lXfb20"
type: "happy"
singers:
  - Anurag Kulkarni
  - Sweekar Agasthi
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Keelu gurramekkaade kinda meeda paddade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keelu gurramekkaade kinda meeda paddade"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakumari kaavalannade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakumari kaavalannade"/>
</div>
<div class="lyrico-lyrics-wrapper">Oobiloki dhookade ootha gatra lenode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oobiloki dhookade ootha gatra lenode"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorinalla yelalannaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorinalla yelalannaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaligaadi theere maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaligaadi theere maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaadioki vacchaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaadioki vacchaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavameedha yaaparaanne pettade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavameedha yaaparaanne pettade"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru nooru ayyedhaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru nooru ayyedhaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavalinchanannaade aagaleka saagelage unnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavalinchanannaade aagaleka saagelage unnade"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey swari cheyra kaalam meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey swari cheyra kaalam meedha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seekusinthalunnode sunna kanna sinnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekusinthalunnode sunna kanna sinnode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinna soopu soododhannaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinna soopu soododhannaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithanni pillode pattalekkinchesade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithanni pillode pattalekkinchesade"/>
</div>
<div class="lyrico-lyrics-wrapper">Mailu raayi dhatinchesaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mailu raayi dhatinchesaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Noti neeru oore oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noti neeru oore oore"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanta eedu sethaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanta eedu sethaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnavaadu aahalokam soothade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnavaadu aahalokam soothade"/>
</div>
<div class="lyrico-lyrics-wrapper">Notu meedha gandhi thathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notu meedha gandhi thathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvukuntu vachaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvukuntu vachaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvukunta gallapetti seraade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvukunta gallapetti seraade"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adde neeku ledhiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adde neeku ledhiyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nekai veeche pillagali eevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekai veeche pillagali eevela"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasalle cherindha edhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasalle cherindha edhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna monna unna bhadha eevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna unna bhadha eevela"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayalle maarindha madhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayalle maarindha madhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadileni jadivaana saradaga kurisnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadileni jadivaana saradaga kurisnda"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasara thadilona podipranam thadisinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasara thadilona podipranam thadisinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello unde prema kallallo cherindhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello unde prema kallallo cherindhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopullo unde prema dhaagena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopullo unde prema dhaagena"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhachalanna kudaradhu suma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhachalanna kudaradhu suma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalokamaataina evaritho anakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalokamaataina evaritho anakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aligelli poindhi dhooramendhulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aligelli poindhi dhooramendhulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni santhosham ganthulese ee nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni santhosham ganthulese ee nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddhokkatayyero ipude ipude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddhokkatayyero ipude ipude"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye bhadhabandi ledhiyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye bhadhabandi ledhiyyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhi nedu bagane ninna monna lagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhi nedu bagane ninna monna lagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu kooda untanantava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu kooda untanantava"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddu rojulosthene kashtam suttam aithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddu rojulosthene kashtam suttam aithene"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarpu neelo vasthadhantava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarpu neelo vasthadhantava"/>
</div>
<div class="lyrico-lyrics-wrapper">Eethibadhalochayantu boruborumantava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eethibadhalochayantu boruborumantava"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhu odhu ante nuvve intaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhu odhu ante nuvve intaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Odaleni reve neevai bosipoyi untava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaleni reve neevai bosipoyi untava"/>
</div>
<div class="lyrico-lyrics-wrapper">Odiponi vare leroi choosthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odiponi vare leroi choosthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sayam chese kalam raadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sayam chese kalam raadha"/>
</div>
</pre>
