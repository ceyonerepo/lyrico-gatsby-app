---
title: "puli onnu song lyrics"
album: "Aaruthra"
artist: "Vidyasagar"
lyricist: "Pa Vijay"
director: "Pa Vijay"
path: "/albums/aaruthra-lyrics"
song: "Puli Onnu"
image: ../../images/albumart/aaruthra.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e0wAR47dv9c"
type: "melody"
singers:
  - Pa Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">puli onnu vetaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli onnu vetaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">than poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than poguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu katta vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu katta vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">natukulla oduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natukulla oduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paathaiyila paavigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaiyila paavigala"/>
</div>
<div class="lyrico-lyrics-wrapper">theduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">senja paavathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senja paavathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">sampalatha vaanguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sampalatha vaanguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyamaa sakthi undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyamaa sakthi undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerukku"/>
</div>
<div class="lyrico-lyrics-wrapper">unkoppan sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unkoppan sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">samikku than kanirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samikku than kanirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puli onnu vetaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli onnu vetaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">than poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than poguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu katta vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu katta vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">natukulla oduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natukulla oduthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boomikulla nooru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikulla nooru "/>
</div>
<div class="lyrico-lyrics-wrapper">nesam puthanju iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesam puthanju iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">athu puthaya villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu puthaya villa"/>
</div>
<div class="lyrico-lyrics-wrapper">veliya vara olunju iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliya vara olunju iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathula athanaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathula athanaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">vilai iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilai iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">paava mootaiku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paava mootaiku than"/>
</div>
<div class="lyrico-lyrics-wrapper">suma kooli engu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suma kooli engu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarukume mela oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukume mela oru"/>
</div>
<div class="lyrico-lyrics-wrapper">theerpu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerpu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">pala raasiyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala raasiyame"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulla poyirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulla poyirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjak keeri katida than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjak keeri katida than"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">antha natpukulla throgam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha natpukulla throgam"/>
</div>
<div class="lyrico-lyrics-wrapper">munna usuru ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munna usuru ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna solla vithu thinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna solla vithu thinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">nakku irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakku irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha thuliya vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha thuliya vida"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneeruku vali irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneeruku vali irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">olagathaiye mathunavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olagathaiye mathunavan"/>
</div>
<div class="lyrico-lyrics-wrapper">kathai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">avan kallaraiya kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan kallaraiya kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">kandu siruchu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu siruchu iruku"/>
</div>
</pre>
