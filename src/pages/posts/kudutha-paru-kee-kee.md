---
title: "kudutha paru kee song lyrics"
album: "Kee"
artist: "Vishal Chandrasekhar"
lyricist: "Syd Ibu (Tupakeys)"
director: "Kalees"
path: "/albums/kee-lyrics"
song: "Kudutha Paru Kee"
image: ../../images/albumart/kee.jpg
date: 2019-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7KXA8YGyNyc"
type: "mass"
singers:
  - Syd Ibu (Tupakeys)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Taka taka taka ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka laka la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka laka la"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Om Om grim kali sooli kala kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Om grim kali sooli kala kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae enga kitta maatikitta kola kola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae enga kitta maatikitta kola kola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichi pesi saachi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichi pesi saachi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch-eh pootu sikka vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch-eh pootu sikka vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka scene-eh pootu kinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka scene-eh pootu kinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai senji putta hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai senji putta hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taka taka taka ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka laka la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka laka la"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taka taka taka ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka laka la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka laka la"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settha mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settha mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Om grim kali sooli kala kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om grim kali sooli kala kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae enga kitta maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae enga kitta maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola Hahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola Hahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyooo olinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyooo olinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka taka ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka laka la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka laka la"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyooo olinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyooo olinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka taka ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka laka la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka laka la"/>
</div>
<div class="lyrico-lyrics-wrapper">Laka laka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laka laka hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatchi putta paaru kee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatchi putta paaru kee"/>
</div>
<div class="lyrico-lyrics-wrapper">Athulae workout anae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athulae workout anae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai enna seiva nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai enna seiva nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni kanni kaathali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni kanni kaathali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single parvaiyil ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single parvaiyil ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paraka vittu puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraka vittu puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike-il eri achae jillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike-il eri achae jillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivvunu kuduthu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvunu kuduthu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock aanen loosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock aanen loosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiko tight-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiko tight-ah"/>
</div>
</pre>
