---
title: "mr.local theme song lyrics"
album: "Mr. Local"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha - Paul B Sailus - SanGan - K.R. Dharan"
director: "M. Rajesh"
path: "/albums/mr-local-lyrics"
song: "Mr.Local Theme"
image: ../../images/albumart/mr-local.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S7Xr_7o3mh8"
type: "mass"
singers:
  - Hiphop Tamizha
  - Paul B Sailus
  - SanGan
  - Palaniammal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mr Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mr Mr.Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modha Ninaikkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modha Ninaikkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Ivan Than Mr.Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Ivan Than Mr.Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Area Vandhu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area Vandhu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Respect Konjam Thookkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Respect Konjam Thookkalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nakkalu Kindalu No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkalu Kindalu No"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyum Unthan Parkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyum Unthan Parkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kitta Vechikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kitta Vechikaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai Illatha Sikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai Illatha Sikkalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon Take It Globalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon Take It Globalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamana Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamana Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Kuda Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Kuda Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluraanga Makkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluraanga Makkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Semma Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Semma Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama Machan Thalli Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Machan Thalli Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovathil Orathil Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovathil Orathil Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Thuliyum Azhukilayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Thuliyum Azhukilayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pocketil Kasilla Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pocketil Kasilla Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukkura Manasukku Korai Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukkura Manasukku Korai Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mr Localu Kaataatha Nakkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mr Localu Kaataatha Nakkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utta Odanji Pogum Un Bagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utta Odanji Pogum Un Bagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mr.Localu Pannatha Sikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mr.Localu Pannatha Sikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Collara Thookki Vittu Oothu Biglu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collara Thookki Vittu Oothu Biglu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon Take It Globalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon Take It Globalu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Respect Konjam Thookalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Respect Konjam Thookalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Kuda Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Kuda Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Solluranga Makkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Solluranga Makkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Semma Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Semma Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha Konjam Thalli Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha Konjam Thalli Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon Take It Globalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon Take It Globalu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Respect Konjam Thookalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Respect Konjam Thookalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Kuda Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Kuda Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Solluranga Makkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Solluranga Makkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Semma Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Semma Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha Konjam Thalli Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha Konjam Thalli Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemana Thookki Thinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemana Thookki Thinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavukku Polladhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavukku Polladhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Evanukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Evanukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavumey Anjaadhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumey Anjaadhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachanai Ellam Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachanai Ellam Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Neechal Adippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Neechal Adippan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnatha Senjikaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnatha Senjikaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamana Velaikaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamana Velaikaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evalavu Adichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalavu Adichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezha Thalli Midhichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezha Thalli Midhichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannikkulla Kumizhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannikkulla Kumizhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Mela Varubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Mela Varubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindal Geli Pannunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindal Geli Pannunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambu Thumbu Izhuthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambu Thumbu Izhuthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum Paasam Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Paasam Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annai Thamizh Magan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Thamizh Magan Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We Call Him Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Call Him Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mr Mr. Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mr Mr. Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kitta Vachikaatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kitta Vachikaatha "/>
</div>
<div class="lyrico-lyrics-wrapper">Thevillatha Sikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevillatha Sikkalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaaru Maaru Localu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaaru Maaru Localu"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon Take It Globalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon Take It Globalu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Respect Konjam Thookalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Respect Konjam Thookalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Kuda Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Kuda Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Solluranga Makkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Solluranga Makkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Semma Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Semma Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha Konjam Thalli Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha Konjam Thalli Nillu"/>
</div>
</pre>
