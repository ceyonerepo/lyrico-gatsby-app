---
title: "kaonkeya wala song lyrics"
album: "San 47"
artist: "Nick Dhammu"
lyricist: "Veet Baljit"
director: "Backbencherz"
path: "/albums/san-47-lyrics"
song: "Kaonkeya Wala"
image: ../../images/albumart/san-47.jpg
date: 2021-05-20
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/ay_GS7IOt-M"
type: "Mass"
singers:
  - Veet Baljit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho matlab kadd di kadd di oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho matlab kadd di kadd di oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere dil chon nikal gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere dil chon nikal gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Si mein sona samajh ke laiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Si mein sona samajh ke laiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatti pittal nikal gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatti pittal nikal gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho masla koyi khaas nahi si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho masla koyi khaas nahi si"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq oss nu raas nahi si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq oss nu raas nahi si"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh khandi si dil nu noch ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh khandi si dil nu noch ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu khun di pyaas nahi si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu khun di pyaas nahi si"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kade ishq samajh nai aaya kahun kamjata nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kade ishq samajh nai aaya kahun kamjata nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadd kaale balaan vich safedi kyun lagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadd kaale balaan vich safedi kyun lagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadon pittgi bottle wali feeling oh lagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadon pittgi bottle wali feeling oh lagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoya ki hai aape kahu halatan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoya ki hai aape kahu halatan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohnu stock home de battal suke laggange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohnu stock home de battal suke laggange"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve bolak ni dihnu phool bhi fikke laggange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve bolak ni dihnu phool bhi fikke laggange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadon adkhad umre ishq chadu barsatan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadon adkhad umre ishq chadu barsatan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadd adkhad umre ishq chadu barsatan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadd adkhad umre ishq chadu barsatan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadon geet taar de dars nazare lochugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadon geet taar de dars nazare lochugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hotel ready sun de bare mud mud sochugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotel ready sun de bare mud mud sochugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek nal veet de yaad karu mulaqatan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek nal veet de yaad karu mulaqatan nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kaonkeya wala dissda kahugi rataan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaonkeya wala dissda kahugi rataan nu"/>
</div>
</pre>
