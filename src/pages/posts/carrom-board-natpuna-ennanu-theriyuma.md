---
title: "carrom board song lyrics"
album: "Natpuna Ennanu Theriyuma"
artist: "Dharan"
lyricist: "Mirchi Vijay"
director: "Shiva Arvind"
path: "/albums/natpuna-ennanu-theriyuma-lyrics"
song: "Carrom Board"
image: ../../images/albumart/natpuna-ennanu-theriyuma.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-O0fqZ1vYkM"
type: "love"
singers:
  - Mirchi Vijay
  - Remya Nambeesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Penne Nee Vaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Nee Vaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamonnu Thaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamonnu Thaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigududhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigududhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaa Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaa Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dha Un Maaman Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dha Un Maaman Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravellam Dhoorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravellam Dhoorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallivittu Vandhen Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallivittu Vandhen Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Podhum Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Podhum Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Veredhuvum Venam Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhuvum Venam Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Carrom Boardu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Carrom Boardu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coina Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coina Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Natpu De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Natpu De"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Nu Oru Striker Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Nu Oru Striker Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu Kalachurum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Kalachurum De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Coin Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Coin Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Locku Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Locku Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pocket Pannidum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pocket Pannidum De"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Striker Mattum Ulla Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Striker Mattum Ulla Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliya Vandhurum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Vandhurum De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Powder Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padham Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padham Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Game Ah Aadum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Game Ah Aadum De"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketcha Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketcha Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendsa Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendsa Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Gaali Pannum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Gaali Pannum De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Therinjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Therinjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkal Purinjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkal Purinjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-la Vizhurom De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-la Vizhurom De"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumam Kanna Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumam Kanna Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatula Vittalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatula Vittalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Podhum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Podhum De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Nee Vaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Nee Vaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamonnu Thaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamonnu Thaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigududhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigududhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaa Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaa Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dha Un Maaman Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dha Un Maaman Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravellam Dhoorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravellam Dhoorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallivittu Vandhen Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallivittu Vandhen Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Podhum Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Podhum Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Veredhuvum Venam Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhuvum Venam Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey You"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Come And Dance With Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come And Dance With Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey You"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Come And Groove With Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come And Groove With Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendsa Pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendsa Pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumaya Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumaya Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Emkitta Nadikadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emkitta Nadikadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavan Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavan Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikura Paiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikura Paiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnuku Pudikadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnuku Pudikadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oota Scene Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oota Scene Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Potadhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potadhu Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Ta Maathu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Ta Maathu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Groupla Doopu Aagida Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Groupla Doopu Aagida Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki Vaasi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Vaasi Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Propose Panna Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Propose Panna Sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Face Sa Thethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face Sa Thethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Voice Sa Maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voice Sa Maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love U Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love U Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dha Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dha Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Glamorous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glamorous"/>
</div>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Dangerous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Babylicious
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babylicious"/>
</div>
<div class="lyrico-lyrics-wrapper">They Call Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Call Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Glamorous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glamorous"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Irresistible
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Irresistible"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Just Dangerous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Just Dangerous"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’ttouch Me Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’ttouch Me Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t Come Too Close To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Come Too Close To Me"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll Be Your New Addiction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll Be Your New Addiction"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ll Make You Burn With Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ll Make You Burn With Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Area Ulla Kettu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Area Ulla Kettu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Engappa Mass-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappa Mass-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukku Maapilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukku Maapilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagi Putta Neeyum Class-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagi Putta Neeyum Class-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman Ponnu 30 Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Ponnu 30 Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Linela Nikkudhu De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Linela Nikkudhu De"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Vu Onna Pannadhu Naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Vu Onna Pannadhu Naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Sikkudhu De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Sikkudhu De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanda Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vaa De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vaa De"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Saayen De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Saayen De"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiss-u Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss-u Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Directa Kelen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Directa Kelen Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Nee Vaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Nee Vaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamonnu Thaayenmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamonnu Thaayenmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigududhamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigududhamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dhaa Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dhaa Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaan Da En Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaan Da En Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravellam Dhoorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravellam Dhoorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallivittu Vandhen Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallivittu Vandhen Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Podhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Podhum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Veredhuvum Venam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhuvum Venam Da"/>
</div>
</pre>
