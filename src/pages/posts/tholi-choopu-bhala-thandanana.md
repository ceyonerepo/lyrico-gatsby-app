---
title: "tholi choopu song lyrics"
album: "Bhala Thandanana"
artist: "Mani Sharma"
lyricist: "Kalyan Chakravarthy Tripuraneni"
director: "Chaitanya Dantuluri"
path: "/albums/bhala-thandanana-lyrics"
song: "Tholi Choopu"
image: ../../images/albumart/bhala-thandanana.jpg
date: 2022-05-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eSfU6Zm7uOc"
type: "love"
singers:
  - Anurag Kulkarni
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tholi choopu thongi choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi choopu thongi choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi naadu thochalede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi naadu thochalede"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahusaa bahusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahusaa bahusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi choopu thongi choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi choopu thongi choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa telusaa telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa telusaa telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi naadu thochalede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi naadu thochalede"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahusaa bahusaa bahusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahusaa bahusaa bahusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa vayasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholi choopu thongi choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi choopu thongi choose"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa telusaa telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa telusaa telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi naadu thochalede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi naadu thochalede"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahusaa bahusaa bahusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahusaa bahusaa bahusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hm gadasarilaa kalisindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hm gadasarilaa kalisindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hm kurula sadai kadilindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hm kurula sadai kadilindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapalila kodithe inthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapalila kodithe inthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchadu naake o maaya ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchadu naake o maaya ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhehamela andharilo unnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhehamela andharilo unnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Spandhichaleka endharilono aaganidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spandhichaleka endharilono aaganidhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa vayasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa vayasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee chilipikale tharigindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee chilipikale tharigindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chelimikila dorikindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chelimikila dorikindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidavani mudila padene lolo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidavani mudila padene lolo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasata leka nee kalalila naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasata leka nee kalalila naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanipettalede manamee prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipettalede manamee prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipettukuntu kalisipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipettukuntu kalisipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa vayasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sangathemite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathemite"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa manasa manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa manasa manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sangathe mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sangathe mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasa vayasa vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa vayasa vayasaa"/>
</div>
</pre>
