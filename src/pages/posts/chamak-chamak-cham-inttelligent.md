---
title: "chamak chamak cham song lyrics"
album: "Inttelligent"
artist: "S Thaman"
lyricist: "Sirivennela Seetharama Sastry"
director: "V V Vinayak"
path: "/albums/inttelligent-lyrics"
song: "Chamak Chamak Cham"
image: ../../images/albumart/inttelligent.jpg
date: 2018-02-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Fp1_dxjyXwo"
type: "love"
singers:
  - S P B Charan
  - Harini Ivaturi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chikku Chikku Chikku Chikku Chikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku Chikku Chikku Chikku Chikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikku Chikku Chikku Chikku Chikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku Chikku Chikku Chikku Chikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are Chamakku Chamakku Chaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Chamakku Chamakku Chaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttuko Chuttuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttuko Chuttuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Dhorikero Hoyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Dhorikero Hoyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakku Janakku Jaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakku Janakku Jaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattuko Pattuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattuko Pattuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Champe Dharuvule Veyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champe Dharuvule Veyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Vayyaram Sayyandhayyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Vayyaram Sayyandhayyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Ayyaare Thassadhiyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Ayyaare Thassadhiyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Thvaragaa Ichchey Nee Lancham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvaragaa Ichchey Nee Lancham "/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Chorave Chesey Marikonchem 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chorave Chesey Marikonchem "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Chamakku Chamakku Chaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Chamakku Chamakku Chaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttuko Chuttuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttuko Chuttuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Dhorikero Hoyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Dhorikero Hoyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakku Janakku Jaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakku Janakku Jaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattuko Pattuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattuko Pattuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Champe Dharuvule Veyyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champe Dharuvule Veyyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga Swaramula Laagindhayyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga Swaramula Laagindhayyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Theega Sogasu Chudayya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theega Sogasu Chudayya "/>
</div>
<div class="lyrico-lyrics-wrapper">Naagu Pogarutho Regindhayya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagu Pogarutho Regindhayya "/>
</div>
<div class="lyrico-lyrics-wrapper">Kode Padaga Kaateyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kode Padaga Kaateyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Maikam Putte Ragam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maikam Putte Ragam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vintoo Saagedhettagayya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintoo Saagedhettagayya "/>
</div>
<div class="lyrico-lyrics-wrapper">Manthram Vesthe Kassu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthram Vesthe Kassu "/>
</div>
<div class="lyrico-lyrics-wrapper">Bussu Itte Aagalayyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussu Itte Aagalayyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham Vesthavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham Vesthavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Andhe Andham Tho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhe Andham Tho "/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhem Vesthavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhem Vesthavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thulle Pantham Tho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulle Pantham Tho "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Kaipe Repe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Kaipe Repe "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaate Vestha Kharaarugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaate Vestha Kharaarugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Mudharaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Mudharaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakku Janakku Jaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakku Janakku Jaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattuko Pattuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattuko Pattuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Champe Dharuvule Veyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champe Dharuvule Veyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Chamakku Chamakku Chaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Chamakku Chamakku Chaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttuko Chuttuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttuko Chuttuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Dhorikero Hoyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Dhorikero Hoyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Ayyaare Thassadhiyyaa  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Ayyaare Thassadhiyyaa  "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Vayyaram Sayyandhayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Vayyaram Sayyandhayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Chorave Chesey Marikonchem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chorave Chesey Marikonchem"/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Thvaragaa Ichchey Nee Lancham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvaragaa Ichchey Nee Lancham "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aggi Jallula Kurise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggi Jallula Kurise "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase Neggalekapothunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase Neggalekapothunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eetha Mullula Edhalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eetha Mullula Edhalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigero Jaathi Vannedheehjaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigero Jaathi Vannedheehjaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antho Intho Saayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antho Intho Saayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyaa Cheyyandhiyyalayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyaa Cheyyandhiyyalayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyani Gaayam Maayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyani Gaayam Maayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chese Margam Choodaalammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese Margam Choodaalammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajeekosthaale Kaage Kougillo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajeekosthaale Kaage Kougillo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajyam Isthaale Neeke Naa Ollo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajyam Isthaale Neeke Naa Ollo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Repo Maapo Aape Oope Hushaaruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Repo Maapo Aape Oope Hushaaruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhapadhamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhapadhamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamakku Chamakku Chaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamakku Chamakku Chaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttuko Chuttuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttuko Chuttuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Dhorikero Hoyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Dhorikero Hoyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakku Janakku Jaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakku Janakku Jaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattuko Pattuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattuko Pattuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Champe Dharuvule Veyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champe Dharuvule Veyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Vayyaram Sayyandhayyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Vayyaram Sayyandhayyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyyaare Hoyyaa Hoyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyyaare Hoyyaa Hoyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Hoy Ayyaare Thassadhiyyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoy Ayyaare Thassadhiyyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Thvaragaa Ichchey Nee Lancham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvaragaa Ichchey Nee Lancham "/>
</div>
<div class="lyrico-lyrics-wrapper">Cham Cham Chakacham Chakacham Cham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cham Cham Chakacham Chakacham Cham "/>
</div>
<div class="lyrico-lyrics-wrapper">Chorave Chesey Marikonchem 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chorave Chesey Marikonchem "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Chamakku Chamakku Chaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Chamakku Chamakku Chaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttuko Chuttuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttuko Chuttuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Dhorikero Hoyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Dhorikero Hoyya "/>
</div>
<div class="lyrico-lyrics-wrapper">Janakku Janakku Jaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakku Janakku Jaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattuko Pattuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattuko Pattuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Champe Dharuvule Veyyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champe Dharuvule Veyyaa"/>
</div>
</pre>
