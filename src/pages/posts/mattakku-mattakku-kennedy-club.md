---
title: "mattakku mattakku song lyrics"
album: "Kennedy Club"
artist: "D. Imman"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kennedy-club-lyrics"
song: "Mattakku Mattakku"
image: ../../images/albumart/kennedy-club.jpg
date: 2019-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZzJkTAEbaHY"
type: "motivational"
singers:
  - Keerthi Sagathia
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sema Kaattu Kaattu Sema Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Kaattu Kaattu Sema Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kaaththu Kaaththu Kila Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaaththu Kaaththu Kila Poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor Theettu Theettu Koor Theettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Theettu Theettu Koor Theettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri Paarthu Vetrigalai Eettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Paarthu Vetrigalai Eettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyatilae Vilayadidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyatilae Vilayadidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigarathin Baashaigal Palavitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigarathin Baashaigal Palavitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Veri Erutha Vizhi Seerutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veri Erutha Vizhi Seerutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thaanae Un Kaigalil Ayutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thaanae Un Kaigalil Ayutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madha Yaanai Polae Thatti Melae Munnaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha Yaanai Polae Thatti Melae Munnaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Meeri Vendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Meeri Vendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Naalai Varalaru Vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Naalai Varalaru Vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale Bale Bale Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale Bale Bale Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sema Kaattu Kaattu Sema Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Kaattu Kaattu Sema Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kaaththu Kaaththu Kila Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaaththu Kaaththu Kila Poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor Theettu Theettu Koor Theettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Theettu Theettu Koor Theettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri Paarthu Vetrigalai Eettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Paarthu Vetrigalai Eettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoom Machalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoom Machalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hosh Udalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hosh Udalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sar Uthakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sar Uthakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeet Manalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeet Manalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadai Kodiyil Yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai Kodiyil Yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Ooril Piranthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ooril Piranthomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Perthoda Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Perthoda Yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paadhai Adainthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paadhai Adainthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endro Vendra Unnoda Kathaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endro Vendra Unnoda Kathaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Ketkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Ketkaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Seithaai Ippothu Endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seithaai Ippothu Endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkum Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum Munneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaigal Vetri Kaana Naalagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaigal Vetri Kaana Naalagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrigal Vanthaal Yaavum Thoolagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrigal Vanthaal Yaavum Thoolagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerai Kaatti Kolla Koodaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerai Kaatti Kolla Koodaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Regai Theindhaal Kaigal Thorkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Regai Theindhaal Kaigal Thorkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale Bale Bale Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale Bale Bale Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sema Kaattu Kaattu Sema Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sema Kaattu Kaattu Sema Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kaaththu Kaaththu Kila Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaaththu Kaaththu Kila Poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor Theettu Theettu Koor Theettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Theettu Theettu Koor Theettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri Paarthu Vetrigalai Eettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Paarthu Vetrigalai Eettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyerava Vazhi Thaanginom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyerava Vazhi Thaanginom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamadi Un Aatralai Kaattidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamadi Un Aatralai Kaattidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Vervaiyil Mazhai Moozhgumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Vervaiyil Mazhai Moozhgumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathi Koottaigal Paarvaiyil Saithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathi Koottaigal Paarvaiyil Saithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madha Yaanai Polae Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha Yaanai Polae Thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Melae Munnaeru Thadai Meeri Vendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melae Munnaeru Thadai Meeri Vendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Naalai Varalaru Vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Naalai Varalaru Vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Bale Bale Bale Vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Bale Bale Bale Vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakku Mataakku Mattakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakku Mataakku Mattakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakku Mattakkuta Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakku Mattakkuta Hey"/>
</div>
</pre>
