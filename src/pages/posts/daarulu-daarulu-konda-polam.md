---
title: "daarulu daarulu song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "Sirivennela Seetharama Sastry"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Daarulu Daarulu"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/t8Pu0SXl2uI"
type: "mass"
singers:
  - M. M. Keeravani
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daarulu daarulu daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarulu daarulu daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli charalu charalu charalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli charalu charalu charalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagaka thappani daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaka thappani daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye jadani cheppani theerulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye jadani cheppani theerulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Methukuni vethike aashala mooralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methukuni vethike aashala mooralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukuni korike aakali koralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukuni korike aakali koralu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rhaavo revo thelevaraku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rhaavo revo thelevaraku "/>
</div>
<div class="lyrico-lyrics-wrapper">aagakanna poli meralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagakanna poli meralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarulu daarulu daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarulu daarulu daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli charalu charalu charalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli charalu charalu charalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Methukunu vethikie… aashaga poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methukunu vethikie… aashaga poradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukunu korike… aakali koralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukunu korike… aakali koralu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daarulu daarulu daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarulu daarulu daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli charalu charalu charalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli charalu charalu charalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarulu daarulu daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarulu daarulu daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli charalu charalu charalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli charalu charalu charalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O baddhem mukkuna kattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O baddhem mukkuna kattukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanam pidikita kattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanam pidikita kattukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvutho kayyam pettukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvutho kayyam pettukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gayam gayam thattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gayam gayam thattukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Panta polam needhaipoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panta polam needhaipoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondapolam cheyattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondapolam cheyattukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chavo revo thelevaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavo revo thelevaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagakanna polimeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagakanna polimeralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavo revo o thelevaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavo revo o thelevaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavo revo thelevaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavo revo thelevaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagakanna polimeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagakanna polimeralu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daarulu daarulu daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarulu daarulu daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli charalu charalu charalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli charalu charalu charalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagaka thappani daarulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagaka thappani daarulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye jadani cheppani theerulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye jadani cheppani theerulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray Ray ray rayyaray"/>
</div>
</pre>
