---
title: 'aayiram mugangal song lyrics'
album: 'Hero'
artist: 'Yuvan Shankar Raja'
lyricist: 'Rokesh, Pa.Vijay'
director: 'P.S.Mithran'
path: '/albums/hero'
song: 'Aayiram Mugangal'
image: ../../images/albumart/hero.jpg
date: 2019-12-20
singers: 
- Ilayaraja
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mU74rjoRY34"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Aayiram mugangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram mugangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Serndhu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraanathae oru mugamaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraanathae oru mugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivan nanban aanavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar ivan nanban aanavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnetrinaan oorvalamaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Munnetrinaan oorvalamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saadhanai seiya vaaipugal
<input type="checkbox" class="lyrico-select-lyric-line" value="Saadhanai seiya vaaipugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarena kaattida medaigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarena kaattida medaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum yaarukum kidaikanumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarukkum yaarukum kidaikanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Minmini koottangal ondraanathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Minmini koottangal ondraanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriya panthondru undanathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sooriya panthondru undanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthagam namai indru padikindrathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthagam namai indru padikindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigarangal kai neetti azhaikindrathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sigarangal kai neetti azhaikindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naalaigalin
<input type="checkbox" class="lyrico-select-lyric-line" value="Naalaigalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaigalin…
<input type="checkbox" class="lyrico-select-lyric-line" value="Naalaigalin…"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkurippil
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkurippil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkurippil…
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkurippil…"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam iruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam iruppom………….
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam iruppom…………."/>
</div>
</pre>