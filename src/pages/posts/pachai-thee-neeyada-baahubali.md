---
title: 'pachai thee neeyada song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Pachai Thee Neeyada'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qIef34bj_xY"
type: 'Love'
singers: 
- Karthik
- Damini
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Pachai thee neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai thee neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchai poo naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchai poo naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai paarvai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai paarvai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patrikondaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patrikondaaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetru kal naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetru kal naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettum uzhi neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettum uzhi neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpa paarai ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpa paarai ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpam seithaayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam seithaayada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae mann minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae mann minnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangai serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangai serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poon thaarigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poon thaarigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal naam korkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal naam korkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragaagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vaanangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vaanangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai thee neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai thee neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchai poo naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchai poo naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai paarvai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai paarvai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patrikondaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patrikondaaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maan vizhikkul endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan vizhikkul endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal ondrai kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal ondrai kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamalai ondreri vandhenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamalai ondreri vandhenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam ondru ullathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam ondru ullathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhaipaalae kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhaipaalae kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inum enai irukki anaithida thudithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inum enai irukki anaithida thudithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae mann minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae mann minnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil veezhgindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil veezhgindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Poon thaarigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poon thaarigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan tholudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan tholudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol serkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol serkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil thogaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil thogaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai thee neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai thee neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchai poo naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchai poo naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai paarvai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai paarvai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patrikondaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patrikondaaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetru kal naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetru kal naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettum uzhi neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettum uzhi neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpa paarai ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpa paarai ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpam seithaayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam seithaayada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keeralil undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeralil undaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Geethangal ketaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geethangal ketaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothalin mogangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothalin mogangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piravi pala eduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piravi pala eduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhum kanam naan maravenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhum kanam naan maravenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigalai varamena thandhida ketenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalai varamena thandhida ketenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye mann minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye mann minnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven thaaragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven thaaragai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil thean sinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil thean sinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poon thaarigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poon thaarigai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan nenjukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan nenjukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan neendhinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan neendhinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal aalathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal aalathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaangiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai thee neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai thee neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchai poo naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchai poo naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai paarvai kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai paarvai kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patrikondaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patrikondaaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetru kal naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetru kal naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettum uzhi neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettum uzhi neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arpa paarai ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpa paarai ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpam seithaayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam seithaayada"/>
</div>
</pre>