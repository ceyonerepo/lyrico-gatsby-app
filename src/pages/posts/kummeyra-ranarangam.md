---
title: "kummeyra song lyrics"
album: "Ranarangam"
artist: "Karthik Rodriguez"
lyricist: "Krishna Chaitanya"
director: "Sudheer Varma"
path: "/albums/ranarangam-lyrics"
song: "Kummeyra"
image: ../../images/albumart/ranarangam.jpg
date: 2019-08-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Id54bvAeTb0"
type: "happy"
singers:
  - Karthik Rodriguez
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mama Premaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Premaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peddha Balasiksha Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddha Balasiksha Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Rendu Pagelloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Rendu Pagelloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardhamavadhu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardhamavadhu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baava Vemana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava Vemana "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamarachi Cheppera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamarachi Cheppera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinaga Thinaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinaga Thinaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vepa Kudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vepa Kudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyanavunuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyanavunuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamuntee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamuntee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepeyraa"/>
</div>
<div class="lyrico-lyrics-wrapper">No Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Govindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govindha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padha Padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha Padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamani Anadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamani Anadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakala Chilake Idhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakala Chilake Idhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Moravini Manase Ivve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moravini Manase Ivve"/>
</div>
<div class="lyrico-lyrics-wrapper">Maguva Teeyyaku Paruve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maguva Teeyyaku Paruve"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Sari Chinthamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Sari Chinthamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Eelesane Rarammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eelesane Rarammani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Choope Iseremani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Choope Iseremani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka Ne Aa Pi Vadini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka Ne Aa Pi Vadini"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Petteyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petteyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamuntee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamuntee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepeyraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuseeti Rail Engine La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuseeti Rail Engine La"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindi Veedhilokilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindi Veedhilokilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Machha Emo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machha Emo "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumu Pi Ala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumu Pi Ala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathekkey Vippe Saralaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathekkey Vippe Saralaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Sari Chinthamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Sari Chinthamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Eelesane Rarammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eelesane Rarammani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Choope Iseremani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Choope Iseremani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka Ne Aa Pi Vadini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka Ne Aa Pi Vadini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummeyraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummeyraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamuntee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamuntee"/>
</div>
<div class="lyrico-lyrics-wrapper">Patteyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patteyraa"/>
</div>
</pre>
