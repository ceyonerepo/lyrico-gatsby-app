---
title: "i am in love song lyrics"
album: "Once Upon a Time in Mumbai"
artist: "Pritam Chakraborty"
lyricist: "Neelesh Misra"
director: "Milan Luthria"
path: "/albums/once-upon-a-time-in-mumbai-lyrics"
song: "I am in Love"
image: ../../images/albumart/once-upon-a-time-in-mumbai.jpg
date: 2010-07-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/5QIfWdGQ0zU"
type: "love"
singers:
  - Dominique Cerejo
  - Kay Kay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho.. aaj kal tanha main kahan hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho.. aaj kal tanha main kahan hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Saath chalta koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saath chalta koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uski hamein aadat hone ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uski hamein aadat hone ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadat ho gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadat ho gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh jo mila hai jab se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh jo mila hai jab se"/>
</div>
<div class="lyrico-lyrics-wrapper">Uski sohbat ho gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uski sohbat ho gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik zara masoom se dil ki aafat ho gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik zara masoom se dil ki aafat ho gayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sun le zara, sun le zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le zara, sun le zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ne kaha itna bas mujhe pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ne kaha itna bas mujhe pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">I am in love, I am in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am in love, I am in love"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi bata jaane kya mujhe hua hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi bata jaane kya mujhe hua hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oas boondon mein tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oas boondon mein tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhein moondun main tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhein moondun main tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishaon das tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishaon das tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi hai bas tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi hai bas tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka shehar tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka shehar tu hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchi khabar tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchi khabar tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Fursat ki hansi tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fursat ki hansi tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi thi kami tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi thi kami tu hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oas boondon mein tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oas boondon mein tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhein moondun main tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhein moondun main tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishaon das tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishaon das tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi hai bas tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi hai bas tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka shehar tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka shehar tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchi khabar tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchi khabar tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Fursat ki hansi tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fursat ki hansi tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi thi kami tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi thi kami tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hai mera, tu hai mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hai mera, tu hai mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh main jaanoon na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh main jaanoon na"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna bas mujhe pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna bas mujhe pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">I am in love, I am in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am in love, I am in love"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi bata jaane kya mujhe hua hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi bata jaane kya mujhe hua hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadal pe chalta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadal pe chalta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Girta sambhalta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girta sambhalta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwahishein karta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwahishein karta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Khone se darta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khone se darta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaga na soya hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga na soya hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Musafir khoya hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musafir khoya hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh sirfira sa hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh sirfira sa hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhu zara sa hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhu zara sa hoon main"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadal pe chalta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadal pe chalta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Girta sambhalta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girta sambhalta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwahishein karta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwahishein karta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Khone se darta hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khone se darta hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaga na soya hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga na soya hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Musafir khoya hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musafir khoya hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh sirfira sa hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh sirfira sa hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhu zara sa hoon main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhu zara sa hoon main"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil kya kare, dil kya kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil kya kare, dil kya kare"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bina itna bas mujhe pata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bina itna bas mujhe pata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">I am in love, I am in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am in love, I am in love"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi bata jaane kya mujhe hua hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi bata jaane kya mujhe hua hai"/>
</div>
</pre>
