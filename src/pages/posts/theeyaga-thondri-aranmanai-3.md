---
title: "theeyaga thondri song lyrics"
album: "Aranmanai 3"
artist: "C. Sathya"
lyricist: "Mohan Rajan"
director: "Sundar C"
path: "/albums/aranmanai-3-lyrics"
song: "Theeyaga Thondri"
image: ../../images/albumart/aranmanai-3.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ceIyDtmNgdM"
type: "happy"
singers:
  - Shankar Mahadevan
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theeyaga Thondri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga Thondri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyagum Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyagum Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Megamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Megamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Ootruvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Ootruvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaradha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaradha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamagum Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamagum Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam Neettiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam Neettiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arul Ootrruvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arul Ootrruvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedaadha Podhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedaadha Podhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Mun Thoandruvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Mun Thoandruvaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadadha Poodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadadha Poodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Neer Aatruvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Neer Aatruvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera En Thaedalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera En Thaedalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaladi Naan Thaedinene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaladi Naan Thaedinene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mara Un Poovadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara Un Poovadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaippidi Thaan Saereno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaippidi Thaan Saereno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathir Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathir Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Velaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Velaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu Theeyorai Kaluvetru En Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Theeyorai Kaluvetru En Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Illaarai Meletru En Baalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Illaarai Meletru En Baalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Kollarai Nee Maatru Pon Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Kollarai Nee Maatru Pon Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondu Seyvorai Dhinam Kakkum Sev Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondu Seyvorai Dhinam Kakkum Sev Velayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engu Paarthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Paarthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Uruvam Thaan Velaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uruvam Thaan Velaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu Nadanthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu Nadanthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale Thaan Murugaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Thaan Murugaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevar Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevar Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha Sirupillai Needhanayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha Sirupillai Needhanayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Unakkillai Veraarurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Unakkillai Veraarurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eede Aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eede Aiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaga Paalaaga Ootrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaga Paalaaga Ootrida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenaga Poomaalai Soottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenaga Poomaalai Soottida"/>
</div>
<div class="lyrico-lyrics-wrapper">Oangaara Unroopam Paarthida Vanthomayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oangaara Unroopam Paarthida Vanthomayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanam Poosiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam Poosiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhanin Thirumuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanin Thirumuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharsinam Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharsinam Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Varamarulvayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Varamarulvayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yagamum Thodangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagamum Thodangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Megamum Polindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megamum Polindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheebathai Yaetrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheebathai Yaetrida"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam Tharuvayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam Tharuvayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That That Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That That Thada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutrum Thadai Vilagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutrum Thadai Vilagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Elu Muruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elu Muruga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tit Tit Tidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tit Tit Tidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum Idi Mulangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Idi Mulangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiru Muruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Muruga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal Kondu Deepam Yetrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Kondu Deepam Yetrida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Nammai Kaaval Kaathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Nammai Kaaval Kaathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Undhan Paadhai Serndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Undhan Paadhai Serndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandha Nee Arulvaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandha Nee Arulvaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathir Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathir Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Velaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Velaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu Theeyorai Kaluvetru En Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Theeyorai Kaluvetru En Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Illaarai Meletru En Baalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Illaarai Meletru En Baalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Kollarai Nee Maatru Pon Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Kollarai Nee Maatru Pon Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondu Seyvorai Dhinam Kakkum Sev Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondu Seyvorai Dhinam Kakkum Sev Velayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engu Paarthalum Un Uruvam Thaan Velaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Paarthalum Un Uruvam Thaan Velaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aethu Nadanthalum Unnaale Thaan Murugaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aethu Nadanthalum Unnaale Thaan Murugaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevar Kulam Kaatha Sirupillai Needhanayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevar Kulam Kaatha Sirupillai Needhanayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Unakkillai Veraarurum Eede Aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Unakkillai Veraarurum Eede Aiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aragarogaraa Aragarogaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aragarogaraa Aragarogaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu Theeyorai Kaluvetru En Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Theeyorai Kaluvetru En Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Illaarai Meletru En Baalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Illaarai Meletru En Baalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Kollaarai Nee Matru Pon Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Kollaarai Nee Matru Pon Velayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondu Seyvorai Dhinam Kakkum Sev Velayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondu Seyvorai Dhinam Kakkum Sev Velayya"/>
</div>
</pre>
