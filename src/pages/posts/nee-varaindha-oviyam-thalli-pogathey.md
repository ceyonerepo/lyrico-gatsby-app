---
title: "nee varaindha oviyam song lyrics"
album: "Thalli Pogathey"
artist: "Gopi Sunder"
lyricist: "Kabilan Vairamuthu"
director: "R. Kannan"
path: "/albums/thalli-pogathey-song-lyrics"
song: "Nee Varaindha Oviyam"
image: ../../images/albumart/thalli-pogathey.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IrI8NvXCI8w"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee varaitha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varaitha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu theendalai marakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu theendalai marakuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nanaindha neeralai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nanaindha neeralai "/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu thunbangal tholaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu thunbangal tholaikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">or uravu mattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or uravu mattume"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya madathin theepama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya madathin theepama"/>
</div>
<div class="lyrico-lyrics-wrapper">athu yar endra kelviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu yar endra kelviye"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha piraviyin payanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha piraviyin payanama"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu varuvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu varuvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnodu maraivayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnodu maraivayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee varaitha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varaitha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu theendalai marakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu theendalai marakuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nanaindha neeralai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nanaindha neeralai "/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu thunbangal tholaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu thunbangal tholaikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oonjaladum pothu un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjaladum pothu un"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal rendum moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal rendum moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">oodal aadum pothu pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodal aadum pothu pala"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal thondrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal thondrume"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum neeyum vaaltha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum neeyum vaaltha "/>
</div>
<div class="lyrico-lyrics-wrapper">natkal pothavillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natkal pothavillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum neeyum vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum neeyum vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvu nyayamillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvu nyayamillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu varuvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu varuvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnodu maraivayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnodu maraivayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moondru mounam inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moondru mounam inge"/>
</div>
<div class="lyrico-lyrics-wrapper">ondrodu ondru mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrodu ondru mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum mounam endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum mounam endra"/>
</div>
<div class="lyrico-lyrics-wrapper">veliyera koodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliyera koodumo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal enna panju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal enna panju"/>
</div>
<div class="lyrico-lyrics-wrapper">pondra poonai pathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pondra poonai pathama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam poga poga mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam poga poga mella"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai keeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai keeruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee varaitha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee varaitha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu theendalai marakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu theendalai marakuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nanaindha neeralai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nanaindha neeralai "/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu thunbangal tholaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu thunbangal tholaikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">or uravu mattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or uravu mattume"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya madathin theepama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya madathin theepama"/>
</div>
<div class="lyrico-lyrics-wrapper">athu yar endra kelviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu yar endra kelviye"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha piraviyin payanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha piraviyin payanama"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu varuvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu varuvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnodu maraivayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnodu maraivayo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu varuvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu varuvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnodu maraivayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnodu maraivayo"/>
</div>
</pre>
