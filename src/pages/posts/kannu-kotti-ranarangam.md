---
title: "kannu kotti song lyrics"
album: "Ranarangam"
artist: "Karthik Rodriguez"
lyricist: "Krishna Chaitanya"
director: "Sudheer Varma"
path: "/albums/ranarangam-lyrics"
song: "Kannu Kotti"
image: ../../images/albumart/ranarangam.jpg
date: 2019-08-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_EcB6qUL9mA"
type: "love"
singers:
  - Karthik Rodriguez
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannukotti choosenanta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukotti choosenanta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundari sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeti vellenanta manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeti vellenanta manohari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manohari manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manohari manohari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukotti choosenanta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukotti choosenanta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeti vellenanta manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeti vellenanta manohari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa letha kallallo munigipoyanemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa letha kallallo munigipoyanemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee choopu soodhalle guchukundho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee choopu soodhalle guchukundho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manohari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukotti choosenanta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukotti choosenanta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeti vellenanta manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeti vellenanta manohari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La la la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are my groove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my groove"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my kick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my kick"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my snare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my snare"/>
</div>
<div class="lyrico-lyrics-wrapper">And you are my song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And you are my song"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my green
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my green"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my blue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my blue"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my peace
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my peace"/>
</div>
<div class="lyrico-lyrics-wrapper">And you are my pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And you are my pain"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my breath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my breath"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my smile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my smile"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my cry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my cry"/>
</div>
<div class="lyrico-lyrics-wrapper">An you are my die
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An you are my die"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my soul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my soul"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my feel"/>
</div>
<div class="lyrico-lyrics-wrapper">You are my heel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are my heel"/>
</div>
<div class="lyrico-lyrics-wrapper">And you are my LOVE
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And you are my LOVE"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinthavo niyanthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthavo niyanthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvevaraina ninnuney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvevaraina ninnuney"/>
</div>
<div class="lyrico-lyrics-wrapper">Varinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattuko aakattuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattuko aakattuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Antondhi manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antondhi manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyilo smarinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyilo smarinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhati saari addhamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhati saari addhamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaithe choosukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaithe choosukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku nenu nacchano ledho ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku nenu nacchano ledho ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti paina padeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti paina padeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthyaala vendi vaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthyaala vendi vaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkipodha naaloki neela ilaa halaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkipodha naaloki neela ilaa halaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukotti choosenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukotti choosenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeti vellenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeti vellenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukotti choosenanta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukotti choosenanta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeti vellenanta manohari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeti vellenanta manohari"/>
</div>
</pre>
