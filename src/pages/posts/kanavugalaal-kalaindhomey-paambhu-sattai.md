---
title: "kanavugalaal kalaindhomey song lyrics"
album: "Paambhu Sattai"
artist: "Ajeesh"
lyricist: "Yugabharathi"
director: "Adam Dasan"
path: "/albums/paambhu-sattai-lyrics"
song: "Kanavugalaal Kalaindhomey"
image: ../../images/albumart/paambhu-sattai.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Vqgz9ZXsBjM"
type: "sad"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nisa Nisa Gasani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisa Nisa Gasani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisagama Paama Gaa Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisagama Paama Gaa Pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugalaal Kalaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalaal Kalaindhome"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaigalaal Tholaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalaal Tholaindhome"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Naaldhorum Nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Naaldhorum Nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saedhaaram Yaaraale Ariyom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saedhaaram Yaaraale Ariyom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Ponaalum Oorai Seraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Ponaalum Oorai Seraadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Edhanaal Arivaar Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Edhanaal Arivaar Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugalaal Kalaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalaal Kalaindhome"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaigalaal Tholaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalaal Tholaindhome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilangaadha Vidhi Ezhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilangaadha Vidhi Ezhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaidhaano Ivalum Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaidhaano Ivalum Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaadha Thanimaiyile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadha Thanimaiyile "/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhaale Pozhudhum Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaindhaale Pozhudhum Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Edhaiyum Seiyaadh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Edhaiyum Seiyaadh"/>
</div>
<div class="lyrico-lyrics-wrapper">irundhaalum Yeno Siluvai Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irundhaalum Yeno Siluvai Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam Idhanai Yeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam Idhanai Yeno "/>
</div>
<div class="lyrico-lyrics-wrapper">Unaraamal Ponaal Vidhavai Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraamal Ponaal Vidhavai Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakkum Kadavul Enge Ponaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkum Kadavul Enge Ponaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Kadhaiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Kadhaiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruthida Maattaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruthida Maattaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugalaal Kalaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalaal Kalaindhome"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaigalaal Tholaindhome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalaal Tholaindhome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thaayin Azhugaiyile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thaayin Azhugaiyile "/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaagum Jananam Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaagum Jananam Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Perin Azhugaiyile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Perin Azhugaiyile "/>
</div>
<div class="lyrico-lyrics-wrapper">Arangerum Maranam Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangerum Maranam Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Ulagin Soodhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Ulagin Soodhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Unaraamal Illai Jayame Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraamal Illai Jayame Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Paname Ennum Nilai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Paname Ennum Nilai "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeno Solvaai Jagame Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeno Solvaai Jagame Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Maname Oonjal Aadaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Maname Oonjal Aadaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam Mudindhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Mudindhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangavum Koodaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangavum Koodaadhe"/>
</div>
</pre>
