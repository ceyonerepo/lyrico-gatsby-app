---
title: "deivam neethanee song lyrics"
album: "Udanpirappe"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Era. Saravanan"
path: "/albums/udanpirappe-lyrics"
song: "Deivam Neethanee"
image: ../../images/albumart/udanpirappe.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CcuHjkfwroU"
type: "affection"
singers:
  - Pavithra Chari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Annane unai thalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annane unai thalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Annane unathanbume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annane unathanbume"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai kaanaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai kaanaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave aadum oonjal kayiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave aadum oonjal kayiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam varaiyil neelume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam varaiyil neelume"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire podum kolam idhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire podum kolam idhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin vasanthan koodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin vasanthan koodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirum un kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirum un kannile"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugum aananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugum aananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam ointhalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam ointhalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum nam sonthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum nam sonthame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Annane unai thalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annane unai thalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyilo mazhaiyo varumun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilo mazhaiyo varumun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaiyaagidum uravu idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiyaagidum uravu idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhavame puriya varam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavame puriya varam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan endru aanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan endru aanadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuraiye sollatha un anbaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiye sollatha un anbaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum kanneerai paarthathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum kanneerai paarthathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiye illaatha un munnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiye illaatha un munnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Viragum aagadho poongilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragum aagadho poongilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil irundhaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil irundhaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">azudhu vazhinthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azudhu vazhinthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivunai naalum pesume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivunai naalum pesume"/>
</div>
<div class="lyrico-lyrics-wrapper">Naraigal viluthaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraigal viluthaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">narambu thalarnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambu thalarnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaga virumbaathu paasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaga virumbaathu paasame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Annane unai thalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annane unai thalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pogaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Annane unathanbume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annane unathanbume"/>
</div>
<div class="lyrico-lyrics-wrapper">Marai kaanaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marai kaanaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urave aadum oonjal kayiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave aadum oonjal kayiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam varaiyil neelume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam varaiyil neelume"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire podum kolam idhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire podum kolam idhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin vasanthan koodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin vasanthan koodume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olirum un kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olirum un kannile"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugum aananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugum aananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam ointhalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam ointhalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum nam sonthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum nam sonthame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deivam neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayum nee thaane"/>
</div>
</pre>
