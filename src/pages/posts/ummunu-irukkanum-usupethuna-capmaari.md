---
title: "ummunu irukkanum usupethuna song lyrics"
album: "Capmaari"
artist: "Siddharth Vipin"
lyricist: "Mohan Rajan"
director: "S.A. Chandrasekhar"
path: "/albums/capmaari-lyrics"
song: "Ummunu Irukkanum Usupethuna"
image: ../../images/albumart/capmaari.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EdQChjWM6QU"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Unakku King-u Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Unakku King-u Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enakku King-u Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enakku King-u Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaigalai Vittu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalai Vittu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadalaam Paattu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaam Paattu Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Unakku Right-u Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Unakku Right-u Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enakku Right-u Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enakku Right-u Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly-a Sernthu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly-a Sernthu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikkalaam Party Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkalaam Party Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bottle Gaaali Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle Gaaali Aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Body Jolly Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body Jolly Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshama Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshama Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Shortcut Irukku Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shortcut Irukku Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ummunnu Irukkanum Usupethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummunnu Irukkanum Usupethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunnu Irukkanum Kadupethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunnu Irukkanum Kadupethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammunu Irukkum Life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammunu Irukkum Life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Annan Sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Annan Sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ummunnu Irukkanum Usupethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummunnu Irukkanum Usupethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunnu Irukkanum Kadupethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunnu Irukkanum Kadupethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammunu Irukkum Life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammunu Irukkum Life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Annan Sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Annan Sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Poga Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Poga Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Keezha Izhuppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Keezha Izhuppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Mattamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Mattamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambukku Izhuppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambukku Izhuppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pera Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pera Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandha Oora Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandha Oora Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Ennennamo Sandaikku Varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ennennamo Sandaikku Varuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Oru Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Oru Vattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Thothavan Jaiyipaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Thothavan Jaiyipaan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Thaan Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Thaan Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Correct-ah Irukkum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Correct-ah Irukkum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja Kaalam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Bhoomi Mela Atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomi Mela Atha"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy-ah Vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy-ah Vaazhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Un Moochu Ponaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Un Moochu Ponaakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjoondu Elumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjoondu Elumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyaama Kedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyaama Kedakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivlo Thaan Un Life-u Daaaa Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivlo Thaan Un Life-u Daaaa Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyila Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyila Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Giridam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giridam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanama Irukkalaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanama Irukkalaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Mattum Gaanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Mattum Gaanama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka Koodathu Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka Koodathu Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu Vandhu Ponaalumae Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu Vandhu Ponaalumae Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ummunnu Irukkanum Usupethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummunnu Irukkanum Usupethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunnu Irukkanum Kadupethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunnu Irukkanum Kadupethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammunu Irukkum Life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammunu Irukkum Life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Annan Sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Annan Sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ummunnu Irukkanum Usupethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummunnu Irukkanum Usupethuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunnu Irukkanum Kadupethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunnu Irukkanum Kadupethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammunu Irukkum Life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammunu Irukkum Life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Annan Sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Annan Sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phaa Phaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phaa Phaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phapha Phapha Phapha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phapha Phapha Phapha"/>
</div>
</pre>
