---
title: "ennodu va song lyrics"
album: "Plan Panni Pannanum"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Badri Venkatesh"
path: "/albums/plan-panni-pannanum-lyrics"
song: "Ennodu Va"
image: ../../images/albumart/plan-panni-pannanum.jpg
date: 2021-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eeCjKJQmlwo"
type: "love"
singers:
  - Rajaganapathy
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Perazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Perazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pogiraai Nee Veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pogiraai Nee Veliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Endhan Kaadhal Ellaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Endhan Kaadhal Ellaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoo Sugam Tharum Poiyazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoo Sugam Tharum Poiyazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thodum Ennavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thodum Ennavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Illaamal Yedhum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illaamal Yedhum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Iru Vizhi Kalavaram Seidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iru Vizhi Kalavaram Seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudum Neruppena Idhayathai Theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudum Neruppena Idhayathai Theendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Nilaiyilum Enakkadhu Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Nilaiyilum Enakkadhu Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Inbam Podhum Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Inbam Podhum Eppodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thedi Thedi Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Thedi Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkiren En Nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkiren En Nenjoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Innum Neeludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Innum Neeludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Perazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Perazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pogiraai Nee Veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pogiraai Nee Veliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Endhan Kaadhal Ellaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Endhan Kaadhal Ellaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaale Naalum Maayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Naalum Maayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal Pogum Nyaayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal Pogum Nyaayangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanaaga Maari Odume Nerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanaaga Maari Odume Nerangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondraagi Pona Swasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraagi Pona Swasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennangal Yaavum Gandhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennangal Yaavum Gandhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesadha Vaarthai Pesidum Mounangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesadha Vaarthai Pesidum Mounangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalaagi Kaamamaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalaagi Kaamamaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Aaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Aaaginaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengidatha Mogam Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengidatha Mogam Thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaidhi Aakinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaidhi Aakinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Soozhal Sorgamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Soozhal Sorgamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Kandaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kandaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thedi Thedi Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Thedi Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkiren En Nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkiren En Nenjoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Innum Neeludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Innum Neeludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thedi Thedi Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedi Thedi Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkiren En Nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkiren En Nenjoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Innum Neeludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Innum Neeludhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Perazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Perazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pogiraai Nee Veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pogiraai Nee Veliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Endhan Kaadhal Ellaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Endhan Kaadhal Ellaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Vaa Perazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaa Perazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Pogiraai Nee Veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pogiraai Nee Veliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Endhan Kaadhal Ellaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Endhan Kaadhal Ellaiye"/>
</div>
</pre>
