---
title: "mass ah rough & tough ah song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Mass ah Rough & Tough ah"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fCIIqk4r_vQ"
type: "mass"
singers:
  -	Tippu
  - Malathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Morattu Massu Motta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Massu Motta "/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Ketta Shiva Is Back 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Ketta Shiva Is Back "/>
</div>
<div class="lyrico-lyrics-wrapper">In Action It's 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In Action It's "/>
</div>
<div class="lyrico-lyrics-wrapper">A Mass Local Song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Mass Local Song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakka Mass Marana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Mass Marana "/>
</div>
<div class="lyrico-lyrics-wrapper">Mass Mass Mass Mass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass Mass Mass Mass"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mass'ah Rough And 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass'ah Rough And "/>
</div>
<div class="lyrico-lyrics-wrapper">Tough'ah Theriyuvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tough'ah Theriyuvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figure'ah Kodutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure'ah Kodutha "/>
</div>
<div class="lyrico-lyrics-wrapper">Super'ah Konjuvom Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super'ah Konjuvom Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavana Konjuvom Pagalula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavana Konjuvom Pagalula "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjuvom Sirichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvom Sirichalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjuvom Morachalum Konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvom Morachalum Konjuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo Lo Lo Local Marana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Lo Lo Local Marana "/>
</div>
<div class="lyrico-lyrics-wrapper">Massu Pakka Local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massu Pakka Local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathuvuttu Ethirvettu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathuvuttu Ethirvettu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnungellam Engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnungellam Engae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maas'ah Kitta Vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'ah Kitta Vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kiss Onnu Thanga Kannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss Onnu Thanga Kannathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kissungo Kazhuthula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissungo Kazhuthula "/>
</div>
<div class="lyrico-lyrics-wrapper">Kissungo Idupula Kissungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissungo Idupula Kissungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Konjam Missungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Konjam Missungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Shiva Da Romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Shiva Da Romba "/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Shiva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Shiva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Shiva Da Romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Shiva Da Romba "/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Shiva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Shiva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Shiva Ketta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Shiva Ketta "/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Motta Thala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Motta Thala "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda Maas'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda Maas'u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Morattupudiyum Maas'u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Morattupudiyum Maas'u "/>
</div>
<div class="lyrico-lyrics-wrapper">Da Andha Style'um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Andha Style'um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimirum Maas'u Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirum Maas'u Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Eppadi Vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Eppadi Vandhalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maas'u Da Lo Lo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'u Da Lo Lo "/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Local Marana Massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Local Marana Massu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakka Local Un Mukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Local Un Mukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Muliyum Maas'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muliyum Maas'u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mullukutthum Meesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mullukutthum Meesa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maas'u Da Nee Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'u Da Nee Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandha Maas'u Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandha Maas'u Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalum Enakku Maas'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalum Enakku Maas'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Da Lo Lo Lo Local 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Lo Lo Lo Local "/>
</div>
<div class="lyrico-lyrics-wrapper">Marana Massu Pakka Local Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Massu Pakka Local Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Looku Vidum Style'um 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looku Vidum Style'um "/>
</div>
<div class="lyrico-lyrics-wrapper">Maas'u Da Nee Aadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'u Da Nee Aadura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegam'um Maas'u Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam'um Maas'u Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Adichu Pudichallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Adichu Pudichallum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maas'u Da Enna Alli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'u Da Enna Alli "/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichalum Maas'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichalum Maas'u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo Lo Lo Local Marana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Lo Lo Local Marana "/>
</div>
<div class="lyrico-lyrics-wrapper">Mass'u Pakka Local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass'u Pakka Local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaandha Karuppu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaandha Karuppu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Maas'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Maas'u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kalla Sirippu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kalla Sirippu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Maas'u Da Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Maas'u Da Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthamum Konjalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamum Konjalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Maas'u Da Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'u Da Ada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mothathula Mass'kay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathula Mass'kay "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maas'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maas'u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mass'ah Rough And 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass'ah Rough And "/>
</div>
<div class="lyrico-lyrics-wrapper">Tough'ah Theriyuvom Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tough'ah Theriyuvom Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figure'ah Kodutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure'ah Kodutha "/>
</div>
<div class="lyrico-lyrics-wrapper">Super'ah Konjuvom Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super'ah Konjuvom Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavana Konjuvom Pagalula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavana Konjuvom Pagalula "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjuvom Sirichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvom Sirichalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjuvom Morachalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvom Morachalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjuvom Lo Lo Lo Local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvom Lo Lo Lo Local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Mass'u Pakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Mass'u Pakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Local Pakkathuvuttu Ethirvettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local Pakkathuvuttu Ethirvettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnungellam Engae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnungellam Engae "/>
</div>
<div class="lyrico-lyrics-wrapper">Maas'ah Kitta Vandhu Kiss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maas'ah Kitta Vandhu Kiss"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Thanga Kannathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Thanga Kannathula "/>
</div>
<div class="lyrico-lyrics-wrapper">Kissungo Kazhuthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissungo Kazhuthula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kissungo Idupula Kissungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissungo Idupula Kissungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Just Konjam Missungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Konjam Missungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Shiva Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Shiva Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Ketta Shiva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Ketta Shiva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Shiva Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Shiva Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Ketta Shiva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Ketta Shiva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Motta Shiva Yeh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Motta Shiva Yeh "/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Shiva Ketta Shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Shiva Ketta Shiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Motta Shiva Motta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Motta Shiva Motta "/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva Ketta Shiva MOTTA
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva Ketta Shiva MOTTA"/>
</div>
</pre>
