---
title: "aatha thottilile song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Yugabharathi"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Aatha Thottilile"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7m4-X0JcRhw"
type: "sad"
singers:
  - Nivas K. Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaththa Thottilile Arai Naali Aadaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththa Thottilile Arai Naali Aadaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Thozhmela Ambaari Yeralaiye…aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Thozhmela Ambaari Yeralaiye…aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaththa Thottilile Arai Naali Aadaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththa Thottilile Arai Naali Aadaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Thozhmela Ambaari Yeralaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Thozhmela Ambaari Yeralaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Ootti Thingakalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Ootti Thingakalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagapan Idam Thangalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagapan Idam Thangalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyootti Valarthapullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyootti Valarthapullai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Mootta Nikkirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Mootta Nikkirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Ootti Thingakalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Ootti Thingakalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagapan Idam Thangalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagapan Idam Thangalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyootti Valarthapullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyootti Valarthapullai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Mootta Nikkirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Mootta Nikkirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaannu Palliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaannu Palliyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Arichuvadi Padikkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arichuvadi Padikkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaa Thaan Ellaamnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaa Thaan Ellaamnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Enni Valandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Enni Valandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kunjukku Panjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunjukku Panjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodangaiyile Irunthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodangaiyile Irunthathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaaram Kalanthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaaram Kalanthathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathi Uyir Arunthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi Uyir Arunthathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudicha Thaai Paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudicha Thaai Paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumattil Irukkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumattil Irukkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Vanthu Vizhunthathunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Vanthu Vizhunthathunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarum Sonnaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum Sonnaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasatha Parikoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasatha Parikoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhivali Nikkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhivali Nikkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamsamtha Palikoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamsamtha Palikoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyaththu Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyaththu Ponene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaamaa Thaan Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamaa Thaan Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththu Ponavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththu Ponavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaama Ponadhenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama Ponadhenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthadi En Usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuthadi En Usuru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Erumbu Andumnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Erumbu Andumnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikkaama Irunthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkaama Irunthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorumakka Vanthiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorumakka Vanthiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urunguriye Mulippatheppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urunguriye Mulippatheppo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadu Alaralaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Alaralaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Valipoonai Marikkalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valipoonai Marikkalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaada Saakkuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaada Saakkuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Thaan Kolaikaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Thaan Kolaikaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi Kavura Poguthunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi Kavura Poguthunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kevuliyum Sollaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kevuliyum Sollaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada Velakkananju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada Velakkananju"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranaththa Kuraliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranaththa Kuraliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattilile Kuththa Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilile Kuththa Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Amukka Nenachirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Amukka Nenachirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Illaa Kattilila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Illaa Kattilila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannurakkam Kondathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannurakkam Kondathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Maala Naan Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Maala Naan Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aasi Vaangumunne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasi Vaangumunne"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Maalai Oor Edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Maalai Oor Edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunthookkam Kondathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunthookkam Kondathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeraaru Saarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeraaru Saarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetri Vara Naanirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetri Vara Naanirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraaru Adikkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraaru Adikkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulluranga Ponathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulluranga Ponathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaththa"/>
</div>
</pre>
