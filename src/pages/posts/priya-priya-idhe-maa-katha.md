---
title: "priya priya song lyrics"
album: "Idhe Maa Katha"
artist: "Sunil Kashyup"
lyricist: "Mangu Balaji"
director: "Guru Pawan"
path: "/albums/idhe-maa-katha-lyrics"
song: "Priya Priya"
image: ../../images/albumart/idhe-maa-katha.jpg
date: 2021-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/K0scs1cVAa8"
type: "happy"
singers:
  - Sunil Kashyup
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee mate vintunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mate vintunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thone nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thone nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisindhe premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisindhe premante"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundhe ee chotey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundhe ee chotey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasanthaa cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasanthaa cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Marchaveey dhari dhare mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchaveey dhari dhare mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh silsila tune kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh silsila tune kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jaaneman o saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaaneman o saathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mate vintunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mate vintunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thone nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thone nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisindhe premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisindhe premante"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundhe ee chotey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundhe ee chotey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna monna na kalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna na kalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve eppudu ralede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve eppudu ralede"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nannu kalipese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nannu kalipese"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam ela bagunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam ela bagunde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalu moosa na pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalu moosa na pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Erojee chusa dhanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erojee chusa dhanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo undani o manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo undani o manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne chusakee telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chusakee telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpindhi preme neeusuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpindhi preme neeusuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh silsila tune kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh silsila tune kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jaaneman o saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaaneman o saathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mate vintunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mate vintunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thone nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thone nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisindhe premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisindhe premante"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundhe ee chotey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundhe ee chotey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni ashalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni ashalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiguristhu nee bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguristhu nee bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne nenu vadhilesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nenu vadhilesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aypoyaa neesontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aypoyaa neesontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethodee chusthu eelokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethodee chusthu eelokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkenthoo bagundheeandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkenthoo bagundheeandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Loloo tesalee parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loloo tesalee parugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nevaipesalaa aa adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nevaipesalaa aa adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesthunna yedhapai neemusugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesthunna yedhapai neemusugu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh silsila tune kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh silsila tune kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jaaneman o saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaaneman o saathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mate vintunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mate vintunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thone nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thone nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisindhe premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisindhe premante"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundhe ee chotey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundhe ee chotey"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasanthaa cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasanthaa cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Marchaveey dhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchaveey dhari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh silsila tune kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh silsila tune kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jaaneman o saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jaaneman o saathiya"/>
</div>
</pre>
