---
title: "unmai kadhal song lyrics"
album: "Mullil Panithuli"
artist: "Benny Pradeep "
lyricist: "NM Jegan"
director: "NM Jegan"
path: "/albums/mullil-panithuli-lyrics"
song: "Unmai Kadhal"
image: ../../images/albumart/mullil-panithuli.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N6ELjdyb4-Y"
type: "mass"
singers:
  - Adam Smith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unmai kadhal engu endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai kadhal engu endru"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu engum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu engum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">endra pinbu nanum vadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endra pinbu nanum vadinen"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">kan kandavudan kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan kandavudan kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">endru sonna naadidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru sonna naadidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">engum illai unmai kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum illai unmai kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">endre nanum ingu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endre nanum ingu indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">pennai aanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennai aanum"/>
</div>
<div class="lyrico-lyrics-wrapper">aanai pennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai pennum"/>
</div>
<div class="lyrico-lyrics-wrapper">ematrum ulagamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ematrum ulagamada"/>
</div>
<div class="lyrico-lyrics-wrapper">kamame kadhal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamame kadhal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">per mari urumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="per mari urumari"/>
</div>
<div class="lyrico-lyrics-wrapper">azhindhe ponathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhindhe ponathada"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai kadhal indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai kadhal indru"/>
</div>
<div class="lyrico-lyrics-wrapper">expire aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="expire aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">chating innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chating innum"/>
</div>
<div class="lyrico-lyrics-wrapper">dating innum mari pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dating innum mari pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">devigama kadhalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devigama kadhalu "/>
</div>
<div class="lyrico-lyrics-wrapper">inrunthuchu oru kalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inrunthuchu oru kalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">apo smart phone ngra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apo smart phone ngra"/>
</div>
<div class="lyrico-lyrics-wrapper">kandravi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandravi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">hall la iruntha telephone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hall la iruntha telephone"/>
</div>
<div class="lyrico-lyrics-wrapper">mobile phone ah mari pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mobile phone ah mari pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">cameravum kuda vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cameravum kuda vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">senthu kichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthu kichu"/>
</div>
<div class="lyrico-lyrics-wrapper">bedroom kum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bedroom kum "/>
</div>
<div class="lyrico-lyrics-wrapper">bathroom kum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathroom kum"/>
</div>
<div class="lyrico-lyrics-wrapper">travel aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="travel aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">photos um vedios um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="photos um vedios um"/>
</div>
<div class="lyrico-lyrics-wrapper">upload aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="upload aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unmai kadhal engu endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai kadhal engu endru"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu engum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu engum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum vadunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vadunen"/>
</div>
</pre>
