---
title: "hey danny song lyrics"
album: "Danny"
artist: "Santhosh Dhayanidhi - Sai Bhaskar"
lyricist: "K Sathish Kumar"
director: "LC Santhanamoorthy"
path: "/albums/danny-lyrics"
song: "Hey Danny"
image: ../../images/albumart/danny.jpg
date: 2020-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pXZwCCjHpCQ"
type: "mass"
singers:
  - Udhay Kannan
  - Sai Bhaskar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thathaavi thaavi thalaiyaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathaavi thaavi thalaiyaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalaatti looty ivan saettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalaatti looty ivan saettai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan pogum route-laam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pogum route-laam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada megam kuraiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada megam kuraiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan meiyum kaattil thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan meiyum kaattil thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam edhum kidaiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam edhum kidaiyadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tough case-ah naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tough case-ah naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-ah nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-ah nippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara naalum oda veppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara naalum oda veppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bow bow bow bow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow bow bow bow"/>
</div>
<div class="lyrico-lyrics-wrapper">Chu chu chu chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chu chu chu chu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bow bow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow bow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Crime case-ah thatta vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crime case-ah thatta vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Acquest-ah alli poo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acquest-ah alli poo nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vaettai thadam thaan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vaettai thadam thaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mass-ah kaati poo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mass-ah kaati poo nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor kaaval ulla nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor kaaval ulla nikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarodum mallu katturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum mallu katturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vattam pottu nikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vattam pottu nikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivan.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram thurathi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram thurathi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal parandhu vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal parandhu vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedi thadayam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedi thadayam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattudhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattudhae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasa mirugam idha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa mirugam idha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesha valaiyil vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesha valaiyil vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naasa naragamondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasa naragamondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonduthae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonduthae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan pakkam vanthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pakkam vanthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saththam podaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saththam podaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pakka plan ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pakka plan ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pappum vegaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pappum vegaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatukkullum kootaikullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatukkullum kootaikullum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval kaappanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval kaappanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Spotu-kulla scene-ah ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spotu-kulla scene-ah ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaippaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theevellaam thadayam thoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevellaam thadayam thoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandanae kuttra pirivae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandanae kuttra pirivae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aram sinam ivan mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram sinam ivan mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiyaadum vettaikaaran ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiyaadum vettaikaaran ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathaavi thaavi thalaiyaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathaavi thaavi thalaiyaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalaatti looty ivan saettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalaatti looty ivan saettai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan pogum route-laam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pogum route-laam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada megam kuraiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada megam kuraiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan meiyum kaattil thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan meiyum kaattil thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam edhum kidaiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam edhum kidaiyadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tough case-ah naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tough case-ah naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-ah nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-ah nippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara naalum oda veppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara naalum oda veppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bow bow bow bow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow bow bow bow"/>
</div>
<div class="lyrico-lyrics-wrapper">Chu chu chu chu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chu chu chu chu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bow bow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bow bow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Crime case-ah thatta vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crime case-ah thatta vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Acquest-ah alli poo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acquest-ah alli poo nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vaettai thadam thaan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vaettai thadam thaan nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey danny hey danny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey danny hey danny"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mass-ah kaati poo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mass-ah kaati poo nee"/>
</div>
</pre>
