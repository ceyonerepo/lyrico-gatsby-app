---
title: "yeno penne song lyrics"
album: "Ispade Rajavum Idhaya Raniyum"
artist: "Sam C. S."
lyricist: "Sam C. S."
director: "Ranjit Jeyakodi"
path: "/albums/idpade-rajavum-idhaya-raniyum-lyrics"
song: "Yeno Penne"
image: ../../images/albumart/ispade-rajavum-idhaya-raniyum.jpg
date: 2019-03-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j6W5_gHL6D4"
type: "love"
singers:
  - D. Sathyaprakash
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaanaa Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veena Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Maaruren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadha Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukku Nooraakki Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukku Nooraakki Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nee Konnuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Konnuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Udan Vaazhkaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Udan Vaazhkaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parugida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Ini Vaazvadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ini Vaazvadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaduthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Theerumun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Theerumun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalai Meerave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Meerave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varamkodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamkodu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mela Vechcha Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela Vechcha Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Pooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kadhal Unna Theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Unna Theendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saththam Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaraadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Kadhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Kadhaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Theerum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Theerum Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaa Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veena Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Maaruren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadha Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomiyil Unnai Nagalaakkida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyil Unnai Nagalaakkida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veredhum Thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhum Thonala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Uyir Serndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Uyir Serndhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ini Vaazhvadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ini Vaazhvadhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Uyir Kootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uyir Kootula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamum Theeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum Theeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ennil Poottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ennil Poottida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Nodi Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Nodi Kuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Neengaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Neengaathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Velagippona En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Velagippona En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaangathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Vida Inga Veredhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vida Inga Veredhuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Naal Dhorum Nee Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naal Dhorum Nee Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kooda di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kooda di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaa Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veena Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Maaruren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadha Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paakkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukku Nooraakki Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukku Nooraakki Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nee Konnuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Konnuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Jahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Jahan Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dard Wahan Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard Wahan Hai"/>
</div>
</pre>
