---
title: "asku laska amour song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "Asku Laska Amour"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p7-q2P5uZzs"
type: "Love"
singers:
  - Vijay Prakash
  - Chinmayi
  - Suvi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeno Thannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Thannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Laska Amour Amour
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Amour Amour"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii Asth Asth Leibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Asth Asth Leibe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaava Bolingo Cintha Cintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaava Bolingo Cintha Cintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Ishq Meile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Ishq Meile"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Ishtam Premam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ishtam Premam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaro Pyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaro Pyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Undhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Undhan Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanai Mozhiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai Mozhiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthai Ovvondru Koithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthai Ovvondru Koithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamai Korthu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamai Korthu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Sendondru Seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Sendondru Seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam Neetinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Neetinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalai Kattinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Kattinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Thannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Thannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Laska Amour Amour
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Amour Amour"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii Asth Asth Leibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Asth Asth Leibe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaava Bolingo Cintha Cintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaava Bolingo Cintha Cintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Ishq Meile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Ishq Meile"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Ishtam Premam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ishtam Premam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaro Pyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaro Pyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Undhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Undhan Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Je Ji Ji Ji Jee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Je Ji Ji Ji Jee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee Ji Ji Ji Jeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee Ji Ji Ji Jeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jingilinga Jingilinga Jingilinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingilinga Jingilinga Jingilinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pluto Vil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pluto Vil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nan Koodetruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nan Koodetruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmengal Porukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmengal Porukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodetruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodetruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkonangal Padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkonangal Padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mookkin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mookkin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittam Mattam Padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittam Mattam Padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellidaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellidaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikodu Naan Aaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikodu Naan Aaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Platovin Maganaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Platovin Maganaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Dhegamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dhegamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraichi Nadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraichi Nadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Koodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Koodama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paazhum Noyil Vizhunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhum Noyil Vizhunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannil Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Unnum Marunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Unnum Marunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mutham Thanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mutham Thanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjil Naadi Manivaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjil Naadi Manivaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kadhal Endre Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kadhal Endre Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Laska Amour Amour
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Amour Amour"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii Asth Asth Leibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Asth Asth Leibe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaava Bolingo Cintha Cintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaava Bolingo Cintha Cintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Ishq Meile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Ishq Meile"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Ishtam Premam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ishtam Premam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaro Pyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaro Pyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Undhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Undhan Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Asku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Asku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Asku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thejavoo Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thejavoo Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Moottinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Moottinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja En Manadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja En Manadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vaattinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vaattinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kappam Kettu Miratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappam Kettu Miratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veppam Kondai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veppam Kondai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratham Moththam Kodhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham Moththam Kodhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pakkam Vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pakkam Vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Nilaavaga Idhamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Nilaavaga Idhamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuliroottava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliroottava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Nilavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Nilavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Koosinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Koosinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Venvanna Nizhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venvanna Nizhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann Veesinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Veesinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullil Pootha Pani Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullil Pootha Pani Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kallam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kallam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virus Illa Kanini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virus Illa Kanini"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ullam Vellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullam Vellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kollai Malli Mullai Poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kollai Malli Mullai Poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai Mellum Sollai Poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai Mellum Sollai Poley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asku Laska Amour Amour
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Amour Amour"/>
</div>
<div class="lyrico-lyrics-wrapper">Asku Laska Amour Amour
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asku Laska Amour Amour"/>
</div>
<div class="lyrico-lyrics-wrapper">Aii Asth Asth Leibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Asth Asth Leibe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaava Bolingo Cintha Cintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaava Bolingo Cintha Cintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Ishq Meile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Ishq Meile"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Ishtam Premam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ishtam Premam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaro Pyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaro Pyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Undhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Undhan Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanai Mozhiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai Mozhiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthai Ovvondru Koithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthai Ovvondru Koithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamai Korthu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamai Korthu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Sendondru Seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Sendondru Seidhen"/>
</div>
 & <div class="lyrico-lyrics-wrapper">Unnidam Neetinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Neetinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalai Kattinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalai Kattinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Thannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Thannaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Meley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kondeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kondeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Unnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Unnaley"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Kandene"/>
</div>
</pre>
