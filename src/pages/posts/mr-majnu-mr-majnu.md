---
title: "mr majnu song lyrics"
album: "Mr. Majnu"
artist: "S. Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/mr-majnu-lyrics"
song: "Mr Majnu"
image: ../../images/albumart/mr-majnu.jpg
date: 2019-01-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tTpQbfHj4j4"
type: "title track"
singers:
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Devadasu manavado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadasu manavado"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhudiki varasudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhudiki varasudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavyamulo kaamudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavyamulo kaamudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakanna rasikudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakanna rasikudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Perfume navvutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perfume navvutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatthukune yavanudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatthukune yavanudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalevi vadhaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalevi vadhaladu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harmones lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harmones lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Segalu repu pogalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Segalu repu pogalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Short time boyfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Short time boyfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi muddhu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi muddhu peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mister majnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mister majnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Krate ki cousin-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krate ki cousin-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mister majnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mister majnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannela prison-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannela prison-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devadasu manavado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadasu manavado"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadhudiki varasudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadhudiki varasudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavyamulo kaamudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavyamulo kaamudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakanna rasikudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakanna rasikudo"/>
</div>
<div class="lyrico-lyrics-wrapper">Perfume navvutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perfume navvutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatthukune yavanudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatthukune yavanudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalevi vadhaladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalevi vadhaladu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harmones lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harmones lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Segalu repu pogalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Segalu repu pogalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Short time boyfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Short time boyfriend"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedi muddhu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedi muddhu peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalone undadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalone undadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu manake dhorakade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu manake dhorakade"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerojantha manadhe dhandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerojantha manadhe dhandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Grandhasaangude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grandhasaangude"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnachote undade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnachote undade"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanne chaatu krishnude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanne chaatu krishnude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundellona baanamalle veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellona baanamalle veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaallunna noppi theliyaneedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaallunna noppi theliyaneedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mister majnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mister majnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Krate ki cousin-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krate ki cousin-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mister majnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mister majnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannela prison-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannela prison-u"/>
</div>
</pre>
