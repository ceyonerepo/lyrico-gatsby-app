---
title: "senthoora song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Thamarai"
director: "Lakshman"
path: "/albums/bogan-lyrics"
song: "Senthoora"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/P6z1zU3UxWU"
type: "love"
singers:
  - Luksimi Sivaneswaralingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nidha nidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidha nidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhanamaga yosithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhanamaga yosithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilla nilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilla nilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillamal odi yosithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillamal odi yosithalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee than manam thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee than manam thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanbaalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanbaalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaai enai yendhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaai enai yendhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boobalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boobalan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En madiyin manavalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En madiyin manavalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thondruthae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena thondruthae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengandhal poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theraa ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeithayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeithayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengandhal poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theraa ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeithayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeithayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakaiyil anaithavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakaiyil anaithavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalai pinaithavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalai pinaithavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai ezhum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai ezhum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkam varum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkam varum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee piriya varam thandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee piriya varam thandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvae pothummmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvae pothummmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengandhal poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theraa ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai kaatrilYeithayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai kaatrilYeithayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthuraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthuraa ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhayin iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhayin iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kudayinil nadappoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kudayinil nadappoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marathin adiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marathin adiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani kanakkinil kadhaipoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani kanakkinil kadhaipoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadal ketpoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal ketpoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadi paarpoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadi paarpoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgathan vendamaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moozhgathan vendamaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarum kaanaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarum kaanaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil vandhu vizhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil vandhu vizhuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee indri ini ennal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri ini ennal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhida mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhida mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengandhal poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theraa ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeithayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeithayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaidhu naan kalaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaidhu naan kalaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogumbothu alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogumbothu alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melindhu naan ilaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melindhu naan ilaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Povathaga solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathaga solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetil nalabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetil nalabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyaga sila neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaga sila neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaivaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaivaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan tholainthal unai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan tholainthal unai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi solvaayaaa aahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi solvaayaaa aahh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthae selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthae selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthoora ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthoora ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengandhal poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengandhal poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theraa ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theraa ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maran ambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maran ambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthum veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainthum veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai kaatril"/>
</div>
</pre>
