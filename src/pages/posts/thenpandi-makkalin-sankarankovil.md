---
title: "thenpandi makkalin song lyrics"
album: "Sankarankovil"
artist: "Rajini"
lyricist: "Mohan Rajan"
director: "Palanivel Raja"
path: "/albums/sankarankovil-lyrics"
song: "Thenpandi Makkalin"
image: ../../images/albumart/sankarankovil.jpg
date: 2011-08-12
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">henpaandi makkalin kankanda dheivamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="henpaandi makkalin kankanda dheivamey"/>
</div>
<div class="lyrico-lyrics-wrapper">sankaran koavilin isakkiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sankaran koavilin isakkiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhandhai oru kaiyiley soolam oru kaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhandhai oru kaiyiley soolam oru kaiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhandhai oru kaiyiley soolam oru kaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhandhai oru kaiyiley soolam oru kaiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">thaangidum isakkiyammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaangidum isakkiyammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">em kulam kaathidum enniyadhu eederum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em kulam kaathidum enniyadhu eederum"/>
</div>
<div class="lyrico-lyrics-wrapper">siva sudarai annaiye isakkiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siva sudarai annaiye isakkiyamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanalavukku manam padaicha vallal ivan paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanalavukku manam padaicha vallal ivan paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaleduthu nadandhuvandhaa vanangi nirkkum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaleduthu nadandhuvandhaa vanangi nirkkum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">thennagaththu singamadaa oorulaga kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennagaththu singamadaa oorulaga kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivana thoora ninnu paarthaa aiyanaara poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivana thoora ninnu paarthaa aiyanaara poala"/>
</div>
<div class="lyrico-lyrics-wrapper">paakkam vandhu paartha pachappulla poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakkam vandhu paartha pachappulla poala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vana thoora ninnu paarthaa aiyanaara poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vana thoora ninnu paarthaa aiyanaara poala"/>
</div>
<div class="lyrico-lyrics-wrapper">paakkam vandhu paartha pachappulla poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakkam vandhu paartha pachappulla poala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanalavukku manam padaicha vallal ivan paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanalavukku manam padaicha vallal ivan paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaleduthu nadandhuvandhaa vanangi nirkkum ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaleduthu nadandhuvandhaa vanangi nirkkum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">thennagaththu singamadaa oorulaga kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennagaththu singamadaa oorulaga kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">ivana thoora ninnu paarthaa aiyanaara poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivana thoora ninnu paarthaa aiyanaara poala"/>
</div>
<div class="lyrico-lyrics-wrapper">paakkam vandhu paartha pachappulla poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakkam vandhu paartha pachappulla poala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soadhanai iruppavanthaan saadhanai padaichiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soadhanai iruppavanthaan saadhanai padaichiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanai namakkedhukku velavan anuppirukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanai namakkedhukku velavan anuppirukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">koorvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koorvaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manadhaal manidhanai uravena uyirena thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manadhaal manidhanai uravena uyirena thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">gunathaal padigalai neruppena moadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunathaal padigalai neruppena moadhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">sudu sudu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudu sudu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaikkira manasirundhaa vedhaikkira nel sezhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaikkira manasirundhaa vedhaikkira nel sezhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga isakkiyum thunaiyirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga isakkiyum thunaiyirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvukku vazhiporakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvukku vazhiporakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi mugam paarthupputtaa pandhipoadum nenjamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi mugam paarthupputtaa pandhipoadum nenjamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perukku vaazhgiravan boomikku baaramadaa ye Thoazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukku vaazhgiravan boomikku baaramadaa ye Thoazha"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukku vaazhgiravan saamikki greedamadaa vaa veeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukku vaazhgiravan saamikki greedamadaa vaa veeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbura dhisaiyellaam pagalena vilakkugal eriyudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbura dhisaiyellaam pagalena vilakkugal eriyudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">odhungura idamellaam idiyena saravedi vedikkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhungura idamellaam idiyena saravedi vedikkudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theradi veedhiyila oorvalam poagudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theradi veedhiyila oorvalam poagudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enga oorula sondhamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga oorula sondhamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">magizhchiyil gudhikkidhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magizhchiyil gudhikkidhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamanoada vettiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamanoada vettiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">manjathanni koalamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjathanni koalamadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattangatti ooru sanam suththiyiruppadha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattangatti ooru sanam suththiyiruppadha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ada enga ooru vizhaapoala endha oorla kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada enga ooru vizhaapoala endha oorla kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattangatti ooru sanam suththiyiruppadha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattangatti ooru sanam suththiyiruppadha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ada enga ooru vizhaapoala endha oorla kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada enga ooru vizhaapoala endha oorla kelu"/>
</div>
</pre>
