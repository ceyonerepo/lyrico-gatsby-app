---
title: "chellama chellam song lyrics"
album: "Aaruthra"
artist: "Vidyasagar"
lyricist: "Pa Vijay"
director: "Pa Vijay"
path: "/albums/aaruthra-lyrics"
song: "Chellama Chellam"
image: ../../images/albumart/aaruthra.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LoMHgklWZHs"
type: "happy"
singers:
  - Karthik
  - Varsha Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamma chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellamma chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamma chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En pecha vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pecha vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththikkudha thiththikkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththikkudha thiththikkudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttymaa kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttymaa kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilamma paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilamma paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththukkudhaa oththukkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkudhaa oththukkudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appavukku amma naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appavukku amma naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammavukkum appa naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavukkum appa naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah aaaaaaah aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aaaaaaah aaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamma chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pecha vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pecha vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththikkudhae thiththikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththikkudhae thiththikkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttymaa kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttymaa kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilamma paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilamma paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththukkudhae oththukkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkudhae oththukkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangai ivalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangai ivalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil thogaiyum yengadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil thogaiyum yengadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal ellaam ivalukku mun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal ellaam ivalukku mun"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopokkavae thayangadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopokkavae thayangadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalan kudai kullaeThoongum erumbai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalan kudai kullaeThoongum erumbai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Appa tholodu siru thookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa tholodu siru thookam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa paasathil palaya sathathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa paasathil palaya sathathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma kaivaasam rusi aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kaivaasam rusi aakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam sondham pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam sondham pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgamum irukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgamum irukkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai poomugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharum sugam kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharum sugam kedaikkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamma chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En pecha vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pecha vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththikkudha thiththikkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththikkudha thiththikkudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttymaa kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttymaa kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilamma paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilamma paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththukkudhaa oththukkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkudhaa oththukkudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velan azhago mayilodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velan azhago mayilodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal mayilae vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal mayilae vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyil thaanae nam veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil thaanae nam veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kula deivangal nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kula deivangal nammodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal veetukku yaar thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal veetukku yaar thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uliyin saththangal varaverkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uliyin saththangal varaverkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavae illatha anbai mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavae illatha anbai mattum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam nammodu ethirpaarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam nammodu ethirpaarkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En annanin kaigalae oonjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En annanin kaigalae oonjala"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaai mozhi aanadhae konjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaai mozhi aanadhae konjala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa aaa"/>
</div>
</pre>
