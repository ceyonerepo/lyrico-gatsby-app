---
title: "jee ke dekhenge song lyrics"
album: "operation MBBD season 2"
artist: "Karthik Rao"
lyricist: "Durgesh Singh"
director: "Karthik Rao"
path: "/albums/operation-mbbs-season2-lyrics"
song: "Jee Ke Dekhenge"
image: ../../images/albumart/operation-mbbs-season2.jpg
date: 2021-03-22
lang: hindi
youtubeLink: "https://www.youtube.com/embed/-rtBZ2whYhQ"
type: "Love"
singers:
  - Ronkini Gupta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Honshlo ko lagi nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honshlo ko lagi nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">Befikro ko huyi fikar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Befikro ko huyi fikar"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat ki duaon ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ki duaon ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Din mein kho sa gaya asar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din mein kho sa gaya asar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu jab paas tha, na ehsaas tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jab paas tha, na ehsaas tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaf karde galtiyan humari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaf karde galtiyan humari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang na sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang na sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Par chal ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par chal ke dekhenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin hi sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin hi sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum jee ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum jee ke dekhenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu saath ho na ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu saath ho na ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum raah takte rahenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum raah takte rahenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang na sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang na sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum jee ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum jee ke dekhenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bezubaan si bhanwaron ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bezubaan si bhanwaron ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Man mein gham ke pehre hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man mein gham ke pehre hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagi bhaagi sadkon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagi bhaagi sadkon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ped saare thehre hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ped saare thehre hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu jab paas tha, na ehsaas tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jab paas tha, na ehsaas tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhool jaana nadaniyan humari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhool jaana nadaniyan humari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi kahan rukegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi kahan rukegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Log saare kehte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Log saare kehte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunke sabki baaton ko hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunke sabki baaton ko hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoye khoye se rehte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoye khoye se rehte hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu jab paas tha, yeh ehsaas tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jab paas tha, yeh ehsaas tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Door hongi uljhanein humari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door hongi uljhanein humari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang na sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang na sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Par chal ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par chal ke dekhenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere bin hi sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere bin hi sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum jee ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum jee ke dekhenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu saath ho na ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu saath ho na ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum raah takte rahenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum raah takte rahenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh tere sang na sahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh tere sang na sahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum jee ke dekhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum jee ke dekhenge"/>
</div>
</pre>
