---
title: "hur funn maula song lyrics"
album: "Koi Jaane Na"
artist: "Tanishk Bagchi"
lyricist: "Amitabh Bhattacharya"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Hur Funn Maula"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/StpojvYoxvc"
type: "happy"
singers:
  - Vishal Dadlani
  - Zara Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haadason ke is shehar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haadason ke is shehar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj koyi aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj koyi aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Khol dega raaz ya phir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khol dega raaz ya phir"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaz khud ban jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaz khud ban jayega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raat ki aaghosh mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat ki aaghosh mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh raaz rehne deejiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh raaz rehne deejiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Keejiyega kal khulasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keejiyega kal khulasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj rehne deejiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj rehne deejiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai koyi parwana yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai koyi parwana yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo jaan pe apni khelega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo jaan pe apni khelega"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashiqui mein hoke fanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashiqui mein hoke fanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanhon mein shama ko le lega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanhon mein shama ko le lega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itni haseenon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itni haseenon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitni dafa yeh dola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitni dafa yeh dola"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir bhi fakeeron ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir bhi fakeeron ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehne huye hai chola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehne huye hai chola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itar ki sheeshi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itar ki sheeshi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan bhi khul jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan bhi khul jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hawa mein ghul jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hawa mein ghul jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil jogira har funn maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil jogira har funn maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaye to kayi karne idhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaye to kayi karne idhar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil-e-bekarar ki dawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil-e-bekarar ki dawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shab se shehar hote hi magar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shab se shehar hote hi magar"/>
</div>
<div class="lyrico-lyrics-wrapper">Baari baari saare ho gaye hawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baari baari saare ho gaye hawa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shab ke andhere mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shab ke andhere mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jannat dikhane wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannat dikhane wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Main to savere bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main to savere bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil na dukhane wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil na dukhane wala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke meri baahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke meri baahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat jo guzre woh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat jo guzre woh"/>
</div>
<div class="lyrico-lyrics-wrapper">Umra bhar jaisi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umra bhar jaisi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil jogira har funn maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil jogira har funn maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Har funn maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har funn maula"/>
</div>
</pre>
