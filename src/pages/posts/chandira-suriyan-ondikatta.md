---
title: "chandira suriyan song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "R Dharmaraj"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Chandira Suriyan"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J8Rx-CRMd1U"
type: "happy"
singers:
  - Mukesh
  - Malathi
  - Shaini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ye chandira suriyan vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye chandira suriyan vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilanjidum santhana solaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilanjidum santhana solaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">natta nadu saamathu velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natta nadu saamathu velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudar ellam thangidum solaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudar ellam thangidum solaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">ipa thanan thaniya thani vali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipa thanan thaniya thani vali "/>
</div>
<div class="lyrico-lyrics-wrapper">poraye sangathi enna pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraye sangathi enna pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla pulla sangathi enna pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla pulla sangathi enna pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">udalalum uyirum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalalum uyirum unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum udalalum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum udalalum unaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada naanga poranthathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada naanga poranthathu "/>
</div>
<div class="lyrico-lyrics-wrapper">vadake vegu thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadake vegu thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">vallarai kelum maiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallarai kelum maiya"/>
</div>
<div class="lyrico-lyrics-wrapper">akka thangai vaaka patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akka thangai vaaka patta"/>
</div>
<div class="lyrico-lyrics-wrapper">neram purusanoda vambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram purusanoda vambu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanda vivaram ipo vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanda vivaram ipo vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyama thaayar veetuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyama thaayar veetuku"/>
</div>
<div class="lyrico-lyrics-wrapper">varuthathoda porom nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuthathoda porom nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo varuthathoda porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo varuthathoda porom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">udalalum uyirum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalalum uyirum unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum udalalum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum udalalum unaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye kovam porukaama veeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kovam porukaama veeta"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vantha kutti en katalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vantha kutti en katalage"/>
</div>
<div class="lyrico-lyrics-wrapper">nethiyila kunguma potalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethiyila kunguma potalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kailyila kulunguthadi valaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kailyila kulunguthadi valaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">un koonthal alagukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koonthal alagukum"/>
</div>
<div class="lyrico-lyrics-wrapper">kunguma pottukum kodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunguma pottukum kodave"/>
</div>
<div class="lyrico-lyrics-wrapper">naan varuven pulla pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varuven pulla pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">ketathu ellam tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketathu ellam tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">udalalum uyirum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalalum uyirum unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">ada intha intha irukku intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada intha intha irukku intha"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum udalalum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum udalalum unaku"/>
</div>
</pre>
