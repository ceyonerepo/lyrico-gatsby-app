---
title: "andhra telangana song lyrics"
album: "Sakala Kala Vallabhudu"
artist: "Ajay Patnaik"
lyricist: "Giridhar Naidu"
director: "Shiva Ganesh"
path: "/albums/sakala-kala-vallabhudu-lyrics"
song: "Andhra Telangana"
image: ../../images/albumart/sakala-kala-vallabhudu.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1wbIXc8nPHs"
type: "happy"
singers:
  - KPSS Aishwarya
  - Murthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aandhra Telangana Allude Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandhra Telangana Allude Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Horseula Dookaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horseula Dookaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatuga Sweetuga Allare Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatuga Sweetuga Allare Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yama Hottuga Unnaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yama Hottuga Unnaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu Chusi Gundekaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Chusi Gundekaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadel Dhadel Kotteskundeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadel Dhadel Kotteskundeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kallu Naave Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kallu Naave Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chada Mada Thittesthundee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chada Mada Thittesthundee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Antu Earth Shocku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Antu Earth Shocku"/>
</div>
<div class="lyrico-lyrics-wrapper">Saraa Sari Ekkesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saraa Sari Ekkesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Esaru Pette Chupulathoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esaru Pette Chupulathoti"/>
</div>
<div class="lyrico-lyrics-wrapper">oopiri Teeyoddhe Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopiri Teeyoddhe Pillaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Royal Enfield Bandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Royal Enfield Bandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kosame Puttesindee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kosame Puttesindee"/>
</div>
<div class="lyrico-lyrics-wrapper">Benzu Loni Range Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benzu Loni Range Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lookkullo Thiseskundee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lookkullo Thiseskundee"/>
</div>
<div class="lyrico-lyrics-wrapper">Reddu Bullu Soft Drink
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reddu Bullu Soft Drink"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Swedamtho Chesesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Swedamtho Chesesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Piulseru Bandini Speeduga Techhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piulseru Bandini Speeduga Techhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedaku Dookodde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedaku Dookodde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wrondu Rputulona Bandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrondu Rputulona Bandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayyi Rayyi Rayyande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayyi Rayyi Rayyande"/>
</div>
<div class="lyrico-lyrics-wrapper">Swingulona Silk Chocolate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swingulona Silk Chocolate"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayya Sayya Sayyande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayya Sayya Sayyande"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Kaastha Naatu Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kaastha Naatu Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Parlede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Parlede"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheeyi Vesthe Block Buster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeyi Vesthe Block Buster"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnu Bommele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnu Bommele"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Kaastha Naatu Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kaastha Naatu Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Parlede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Parlede"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheeyi Vesthe Block Buster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeyi Vesthe Block Buster"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnu Bommele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnu Bommele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum Dhuduku Maasu Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum Dhuduku Maasu Raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Master Posu Chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master Posu Chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moginche Bandu Bajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moginche Bandu Bajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum Dhuduku Maasu Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum Dhuduku Maasu Raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaruga Tightu Hag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaruga Tightu Hag"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichheskoo Style Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichheskoo Style Raajaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaro Seenu Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaro Seenu Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkesthundi Seenu Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkesthundi Seenu Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabili Kannu Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabili Kannu Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichesthundi Seenu Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichesthundi Seenu Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvam Aduguthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam Aduguthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dookeyyara Seenu Baavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dookeyyara Seenu Baavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Late Eh Chesthe cool Ayipothane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Late Eh Chesthe cool Ayipothane"/>
</div>
<div class="lyrico-lyrics-wrapper">Raceru Bike Lagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raceru Bike Lagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthunnavu Seenu Baavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthunnavu Seenu Baavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Market Chepa Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Market Chepa Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Sizelu Keka Baavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Sizelu Keka Baavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potuga Cheyyi vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potuga Cheyyi vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Night Ki Kotha Baavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Night Ki Kotha Baavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chetta Pattaa Aadeskundame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetta Pattaa Aadeskundame"/>
</div>
<div class="lyrico-lyrics-wrapper">Seenu Seenu Seenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenu Seenu Seenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaraa Chindeskundam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaraa Chindeskundam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seenu Seenu Seenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenu Seenu Seenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaraa Enjoy Chedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaraa Enjoy Chedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Seenu Seenu Seenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenu Seenu Seenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaraa Malli Vaddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaraa Malli Vaddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seenu Seenu Seenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenu Seenu Seenugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seenu Gaadu Vachesade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenu Gaadu Vachesade"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Jarra Sideskoo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Jarra Sideskoo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Lona Seat Eh Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Lona Seat Eh Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karcheff Nuvve Veseskoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karcheff Nuvve Veseskoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Rangu Cotton Cheeralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Rangu Cotton Cheeralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boddutho Nuvve Pettesko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boddutho Nuvve Pettesko"/>
</div>
<div class="lyrico-lyrics-wrapper">AAda Eeda Kotlaatosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AAda Eeda Kotlaatosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattiga Nanne Pattesko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattiga Nanne Pattesko"/>
</div>
<div class="lyrico-lyrics-wrapper">Liquor Buddi Lopala Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liquor Buddi Lopala Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mettaga Cheyye Vesthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mettaga Cheyye Vesthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Tempt Ayipothe Thappe Chesthanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tempt Ayipothe Thappe Chesthanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaru Kottuku Vachesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaru Kottuku Vachesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattiga Nanne Touch Chesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattiga Nanne Touch Chesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchhata Gaane Mudde Chesalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchhata Gaane Mudde Chesalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Kaastha Naatu Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kaastha Naatu Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Parlede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Parlede"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheeyi Vesthe Block Buster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeyi Vesthe Block Buster"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnu Bommele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnu Bommele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu Kaastha Naatu Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kaastha Naatu Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina Parlede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina Parlede"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheeyi Vesthe Block Buster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeyi Vesthe Block Buster"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunnu Bommele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunnu Bommele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum Dhuduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum Dhuduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu Raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Master Posu Chupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master Posu Chupi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moginche Bandu Bajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moginche Bandu Bajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa Palaa Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa Palaa Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum Dhuduku Maasu Raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum Dhuduku Maasu Raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaruga Tightu Hag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaruga Tightu Hag"/>
</div>
<div class="lyrico-lyrics-wrapper">Ichheskoo Style Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ichheskoo Style Raajaa"/>
</div>
</pre>
