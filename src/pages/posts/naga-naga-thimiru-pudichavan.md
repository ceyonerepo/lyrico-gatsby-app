---
title: "naga naga song lyrics"
album: "Thimiru Pudichavan"
artist: "Vijay Antony"
lyricist: "Arun Bharathi"
director: "Ganeshaa"
path: "/albums/thimiru-pudichavan-lyrics"
song: "Naga Naga"
image: ../../images/albumart/thimiru-pudichavan.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hR8EN1L8j5k"
type: "mass"
singers:
  - VM Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thimiru thaan pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru thaan pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiruthaan pudhichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruthaan pudhichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichu ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichu ponavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru thaan pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru thaan pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiruthaan pudhichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruthaan pudhichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichu ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichu ponavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru thaan pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru thaan pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiruthaan pudhichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruthaan pudhichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichu ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichu ponavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velavaa vetri velavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaa vetri velavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerivaa mayilil yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerivaa mayilil yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kudalai uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kudalai uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaigal pottidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaigal pottidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaigal saaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaigal saaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru thaan pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru thaan pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiruthaan pudhichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruthaan pudhichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichu ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichu ponavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velavaa velavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaa velavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavadi nee aadivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavadi nee aadivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiyai pokkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiyai pokkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee vinai theerkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee vinai theerkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adithidum parai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithidum parai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaridum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaridum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Therithida udai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therithida udai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neiya pudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neiya pudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Odungidum padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odungidum padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadungidum thodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadungidum thodai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilithidu sathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilithidu sathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudithidu kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudithidu kadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaalin adiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalin adiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavangal veezhattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavangal veezhattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boothangal odattum saagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boothangal odattum saagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara hara hara hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara hara hara hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara hara hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara hara hara hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara hara hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara hara hara hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara hara hara hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara hara hara hara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velinai thooki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velinai thooki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooranai thaaka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranai thaaka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobathai kaata vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobathai kaata vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peigalai ottavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peigalai ottavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arakkanai sudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakkanai sudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alukinai edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alukinai edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinthathu gedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinthathu gedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katharida vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katharida vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigalai thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai thadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharumathai nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharumathai nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayathinai kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayathinai kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolai vaazh edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolai vaazh edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaalin adiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalin adiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavangal veezhattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavangal veezhattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boothangal odattum saagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boothangal odattum saagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naga naga naga naguna naguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naga naga naga naguna naguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Digu dugu digu diguna diguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digu dugu digu diguna diguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velavaa vetri velavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaa vetri velavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerivaa mayilil yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerivaa mayilil yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kudalai uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kudalai uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaigal pottidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaigal pottidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaigal saaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaigal saaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru thaan pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru thaan pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiruthaan pudhichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiruthaan pudhichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirukkae pudichu ponavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirukkae pudichu ponavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku arogara"/>
</div>
<div class="lyrico-lyrics-wrapper">Muruganukku arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruganukku arogara"/>
</div>
<div class="lyrico-lyrics-wrapper">Velanakku arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velanakku arogara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadambanukku arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadambanukku arogara"/>
</div>
</pre>
