---
title: "tappukokura car song song lyrics"
album: "Mithai"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Prashant Kumar"
path: "/albums/mithai-lyrics"
song: "Tappukokura (Car Song)"
image: ../../images/albumart/mithai.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Q10MpesotVA"
type: "happy"
singers:
  - Smaran Sai
  - Adithya Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dum Da Dum Da Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Da Dum Da Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Da Dum Da Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Da Dum Da Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum Da Dum Da Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Da Dum Da Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Da Dum Da Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Da Dum Da Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tappukokura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappukokura"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifeu Thone Nadavaraa Salaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifeu Thone Nadavaraa Salaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappukaduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappukaduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vunna rutu Marithe Polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunna rutu Marithe Polaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shikaru Kotukuntu Paadali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikaru Kotukuntu Paadali"/>
</div>
<div class="lyrico-lyrics-wrapper">Pukaaru Mosukocche Ee Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pukaaru Mosukocche Ee Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakeerukunna Jeebulo Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakeerukunna Jeebulo Kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavabu Theeru Ninnu Maarchali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavabu Theeru Ninnu Maarchali"/>
</div>
<div class="lyrico-lyrics-wrapper">Timeu Chusukuntu Aagamagamaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeu Chusukuntu Aagamagamaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sademiya Hadavidantha Chaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sademiya Hadavidantha Chaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tappukokura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappukokura"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifeu Thone Nadavaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifeu Thone Nadavaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappukaduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappukaduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vunna Rutu Marithe Polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunna Rutu Marithe Polaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trajadini Dhati Jaarukuntu Paariporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trajadini Dhati Jaarukuntu Paariporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Joku Thoti Koththa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Joku Thoti Koththa "/>
</div>
<div class="lyrico-lyrics-wrapper">Parichayanni Penchukoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayanni Penchukoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katifu Cheppananna Prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katifu Cheppananna Prapancham"/>
</div>
<div class="lyrico-lyrics-wrapper">Colorsu Penchi Chupe Prism
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colorsu Penchi Chupe Prism"/>
</div>
<div class="lyrico-lyrics-wrapper">Karchifu Veyamante Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karchifu Veyamante Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Savala Shivalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savala Shivalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Richu Kaduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Richu Kaduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikunuvvu Nacchithe Chaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikunuvvu Nacchithe Chaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nocchukokuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nocchukokuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maripoku Kotthagaa Salaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maripoku Kotthagaa Salaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shikaru Kottukuntu Paadali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikaru Kottukuntu Paadali"/>
</div>
<div class="lyrico-lyrics-wrapper">Pukaaru Mosukocche Ee Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pukaaru Mosukocche Ee Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakeerukunna Jeebulo Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakeerukunna Jeebulo Kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavabu Theeru Ninnu Maarchali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavabu Theeru Ninnu Maarchali"/>
</div>
<div class="lyrico-lyrics-wrapper">Timeu Chusukuntu Aagamagamaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeu Chusukuntu Aagamagamaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sademiya Hadavidantha Chaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sademiya Hadavidantha Chaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalisi Paadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Paadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Paata Kotthagaa Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Paata Kotthagaa Naalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi Paadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Paadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Paata Kotthagaa Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Paata Kotthagaa Naalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi Paadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Paadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Paata Kotthagaa Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Paata Kotthagaa Naalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi Paadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi Paadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Patha Paata Kotthagaa Naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patha Paata Kotthagaa Naalaa"/>
</div>
</pre>
