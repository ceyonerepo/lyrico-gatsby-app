---
title: "rama kanavemira song lyrics"
album: "Raja Vikramarka"
artist: "Prasanth R Vihari"
lyricist: "Ramajogayya Sastry"
director: "Sri Saripalli"
path: "/albums/raja-vikramarka-lyrics"
song: "Rama Kanavemira"
image: ../../images/albumart/raja-vikramarka.jpg
date: 2021-11-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XQL_zIdqXKk"
type: "devotional"
singers:
  - Anurag Kulkarni
  - Manisha Eerabathini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pudami odilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudami odilo"/>
</div>
<div class="lyrico-lyrics-wrapper">punnami baala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnami baala"/>
</div>
<div class="lyrico-lyrics-wrapper">janakuni janumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janakuni janumam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhanyamu chaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhanyamu chaala"/>
</div>
<div class="lyrico-lyrics-wrapper">kaagala kaaryam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaagala kaaryam"/>
</div>
<div class="lyrico-lyrics-wrapper">jaripinchela srimahalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaripinchela srimahalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">velasenu maralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velasenu maralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jagada dharanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagada dharanam"/>
</div>
<div class="lyrico-lyrics-wrapper">janaanandha karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janaanandha karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">janaki jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janaki jananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dheera gunamandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheera gunamandhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">idhgidhi gora sakhi indhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhgidhi gora sakhi indhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jatha kori 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jatha kori "/>
</div>
<div class="lyrico-lyrics-wrapper">seethaga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seethaga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">ee viribonri veekshinchera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee viribonri veekshinchera"/>
</div>
<div class="lyrico-lyrics-wrapper">sree rama kanavemira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sree rama kanavemira"/>
</div>
<div class="lyrico-lyrics-wrapper">sivadhanusandhu konavemiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivadhanusandhu konavemiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sree rama kanavemira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sree rama kanavemira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ravana ravana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravana ravana "/>
</div>
</pre>
