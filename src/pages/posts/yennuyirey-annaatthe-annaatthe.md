---
title: "yennuyirey song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Thamarai"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Yennuyirey"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8BXmHzl4s0Q"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yennuyire yennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyire yennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thanthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravendru sonnal nee thaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravendru sonnal nee thaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn uthiraththil odum poontheyney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn uthiraththil odum poontheyney"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamum thavamum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamum thavamum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Valium marunthum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valium marunthum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil kalantha en thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil kalantha en thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangm thangam chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyire yennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyire yennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thanthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poomaalai koodi ponnaaram sudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomaalai koodi ponnaaram sudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nirkum koolam ammamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nirkum koolam ammamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraarkal kudi un vaalththu paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraarkal kudi un vaalththu paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoikindra neram kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoikindra neram kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizalena naan neendu iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizalena naan neendu iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyeduththu nee pooga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyeduththu nee pooga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai pidiththey koorai koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai pidiththey koorai koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil mazai kaappaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil mazai kaappaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangai thirumugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangai thirumugam "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil niraikirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil niraikirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbai malarilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbai malarilum "/>
</div>
<div class="lyrico-lyrics-wrapper">manjal vazikirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal vazikirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Puththam puthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puththam puthu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalum pularuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalum pularuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyire yennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyire yennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nee thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum nee thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandil nee irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandil nee irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaarvai thanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaarvai thanthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru koodi pookal naam thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru koodi pookal naam thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyathu saainthum poothomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyathu saainthum poothomey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniya sumaiyai tholil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniya sumaiyai tholil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruga anaiththa naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruga anaiththa naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaiyena enai unarntheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyena enai unarntheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangm thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangm thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">chellaththangam thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chellaththangam thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellaththangam thangam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaththangam thangam "/>
</div>
<div class="lyrico-lyrics-wrapper">thangam chellaththangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam chellaththangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennuyire"/>
</div>
</pre>
