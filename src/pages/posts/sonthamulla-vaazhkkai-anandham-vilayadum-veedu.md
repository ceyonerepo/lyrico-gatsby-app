---
title: "sonthamulla vaazhkkai song lyrics"
album: "Anandham Vilayadum Veedu"
artist: "Siddhu Kumar"
lyricist: "Snehan"
director: "Nandha Periyasamy"
path: "/albums/anandham-vilayadum-veedu-song-lyrics"
song: "Sonthamulla Vaazhkkai"
image: ../../images/albumart/anandham-vilayadum-veedu.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fiL_9qF1YHY"
type: "happy"
singers:
  - Karunguyil Ganesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sondhamulla vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamulla vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgathukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththu sugam yedhum vendamaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu sugam yedhum vendamaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna kadhai illa ketta kadhai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna kadhai illa ketta kadhai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kadhai verethaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kadhai verethaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram yaanai balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram yaanai balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thambi sernthirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thambi sernthirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasathaiyum rosathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasathaiyum rosathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhi vaikka mundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhi vaikka mundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaikkum kanneerukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaikkum kanneerukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verupaadu yedhumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verupaadu yedhumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhamae veedu muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhamae veedu muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli vilaiyadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli vilaiyadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru aalamara vizhudhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aalamara vizhudhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala uravu onna vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala uravu onna vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkum nenjam paasathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkum nenjam paasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal aaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Our kannu kalanginaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our kannu kalanginaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kaigal thudaikka varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kaigal thudaikka varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi oru koottukulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi oru koottukulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhamulla vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamulla vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgathukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththu sugam yedhum vendamaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu sugam yedhum vendamaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna kadhai illa ketta kadhai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna kadhai illa ketta kadhai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kadhai verethaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kadhai verethaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai madi pola thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai madi pola thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan ullam thaangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan ullam thaangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi mugam paarkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi mugam paarkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai mugam thondrudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai mugam thondrudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham vaazhum veettil thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham vaazhum veettil thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivam vandhu kaaval kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivam vandhu kaaval kaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaigal thedi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaigal thedi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha veettil piranthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha veettil piranthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram yaanai balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram yaanai balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thambi sernthirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thambi sernthirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasathaiyum rosathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasathaiyum rosathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhi vaikka mundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhi vaikka mundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaikkum kanneerukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaikkum kanneerukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verupaadu yedhumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verupaadu yedhumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhamae veedu muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhamae veedu muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli vilaiyadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli vilaiyadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru aalamara vizhudhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aalamara vizhudhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala uravu onna vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala uravu onna vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkum nenjam paasathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkum nenjam paasathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal aaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Our kannu kalanginaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Our kannu kalanginaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala kaigal thudaikka varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala kaigal thudaikka varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi oru koottukulla thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi oru koottukulla thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhamulla vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamulla vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgathukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgathukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththu sugam yedhum vendamaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu sugam yedhum vendamaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna kadhai illa ketta kadhai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna kadhai illa ketta kadhai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kadhai verethaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kadhai verethaiyaa"/>
</div>
</pre>
