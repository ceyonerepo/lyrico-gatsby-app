---
title: 'odavum mudiyadhu oliyavum mudiyadhu title track song lyrics'
album: 'Odavum Mudiyadhu Oliyavum Mudiyadhu'
artist: 'Kaushik Krish'
lyricist: 'Saravedi Saran'
director: 'Ramesh Venkat'
path: '/albums/odavum-mudiyadhu-oliyavum-mudiyadhu-song-lyrics'
song: 'Title Track'
image: ../../images/albumart/odavum-mudiyadhu-oliyavum-mudiyadhu.jpg
date: 2020-05-11
lang: tamil
singers: 
- HipHop Tamizha Aadhi
youtubeLink: "https://www.youtube.com/embed/6aNcc39DMsg"
type: 'fun'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga enna panna porom engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga enna panna porom engalukkae theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha song ethukku pottom-nu engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha song ethukku pottom-nu engalukkae theriyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kashthamuna kaiya kuduthu mela thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kashthamuna kaiya kuduthu mela thookkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu vecha malaiya kooda thoola aakuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasu vecha malaiya kooda thoola aakuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan simittum nerathula pannuvom setting-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan simittum nerathula pannuvom setting-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkaga ambaniyae que-la waiting-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Engalukkaga ambaniyae que-la waiting-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanatha suththi paapom rocketu-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanatha suththi paapom rocketu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul enga pocketu-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadavul enga pocketu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Arnold vandhaalum kavalai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Arnold vandhaalum kavalai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eguruna thoguruna oda viduvom daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee eguruna thoguruna oda viduvom daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga enna panna porom engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga enna panna porom engalukkae theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha song ethukku pottom-nu engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha song ethukku pottom-nu engalukkae theriyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dappanguthu gummanguthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dappanguthu gummanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha vaila kuththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta vandha vaila kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu gummanguthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dappanguthu gummanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandha vaila kuththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta vandha vaila kuththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Venam sonna kelu
<input type="checkbox" class="lyrico-select-lyric-line" value="Venam sonna kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga vera aalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga vera aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthaalae kizhinjirum sevulu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta vanthaalae kizhinjirum sevulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sharp-u naanga thaan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Sharp-u naanga thaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka scene-u yen da
<input type="checkbox" class="lyrico-select-lyric-line" value="Mokka scene-u yen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena kodachal kudutha vaanguva bonda
<input type="checkbox" class="lyrico-select-lyric-line" value="Veena kodachal kudutha vaanguva bonda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mass kaati maja pannuvom naanga top-u da
<input type="checkbox" class="lyrico-select-lyric-line" value="Mass kaati maja pannuvom naanga top-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaloda moolai kulla world-u mappu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Engaloda moolai kulla world-u mappu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle-u chainu bladeu kaththi edhuvum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Cycle-u chainu bladeu kaththi edhuvum thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodu poondhu thooka porom oththa saakula
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodu poondhu thooka porom oththa saakula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dappanguthu gummanguthu antha maari da
<input type="checkbox" class="lyrico-select-lyric-line" value="Dappanguthu gummanguthu antha maari da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga sonna andavanae sonna maari da
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga sonna andavanae sonna maari da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatukku raja-na singam thaan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatukku raja-na singam thaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatukkae naanga na bangam thaan da
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatukkae naanga na bangam thaan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga enna panna porom engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga enna panna porom engalukkae theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha song ethukku pottom-nu engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha song ethukku pottom-nu engalukkae theriyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kashthamuna kaiya kuduthu mela thookkuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kashthamuna kaiya kuduthu mela thookkuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu vecha malaiya kooda thoola aakuvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Manasu vecha malaiya kooda thoola aakuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan simittum nerathula pannuvom setting-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan simittum nerathula pannuvom setting-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkaga ambaniyae que-la waiting-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Engalukkaga ambaniyae que-la waiting-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanatha suththi paapom rocketu-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanatha suththi paapom rocketu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul enga pocketu-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadavul enga pocketu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Arnold vandhaalum kavalai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Arnold vandhaalum kavalai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eguruna thoguruna oda viduvom daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee eguruna thoguruna oda viduvom daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga enna panna porom engalukkae theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga enna panna porom engalukkae theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavum mudiyadhu oliyavum mudiyadhu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavum mudiyadhu oliyavum mudiyadhu "/></div>
</div>
</pre>