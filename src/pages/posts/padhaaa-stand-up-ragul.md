---
title: "padhaaa song lyrics"
album: "Stand Up Ragul"
artist: "Sweekar Agasthi"
lyricist: "Rahman"
director: "Santo"
path: "/albums/stand-up-ragul-lyrics"
song: "Padhaaa"
image: ../../images/albumart/stand-up-ragul.jpg
date: 2022-03-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/M9AxbkI26AQ"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Padhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamantondhi Pasi Praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamantondhi Pasi Praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chereti Aaraatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chereti Aaraatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aage Veelledhu Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage Veelledhu Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalaa Nijaala Melakuvalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalaa Nijaala Melakuvalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choope Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choope Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadele Pedaalu Kadalaka Paatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadele Pedaalu Kadalaka Paatale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Adugulu Urukulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Adugulu Urukulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugulu Theesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu Theesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapulu Merupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapulu Merupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyheyeyeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyheyeyeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Valapulu Thalapulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Valapulu Thalapulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapanalu Penchesthunte Aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapanalu Penchesthunte Aashale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamantondhi Pasi Praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamantondhi Pasi Praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Chereti Aaraatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Chereti Aaraatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayatiki Raakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayatiki Raakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevenaa Lolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevenaa Lolona"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuruga Nuvvunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuruga Nuvvunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaarenaa O Maataina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaarenaa O Maataina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anumaanam Ledhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumaanam Ledhinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukoni Edho Vaikhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoni Edho Vaikhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchindhe Eeroje Kadhalannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchindhe Eeroje Kadhalannee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandheham Bandhinchi Penchindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandheham Bandhinchi Penchindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Lo Alajadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Lo Alajadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vivarinche Daaredho Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivarinche Daaredho Mari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emito Kshanaalu Kadhalaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emito Kshanaalu Kadhalaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipoye Aaraadheesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipoye Aaraadheesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Thapinchi Adigenu Nee Jathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Thapinchi Adigenu Nee Jathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Palukulu Padhamulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Palukulu Padhamulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Melikalu Vesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melikalu Vesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhenakalu Munakalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhenakalu Munakalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Choravalu Chanuvulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Choravalu Chanuvulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaburulu Oopesthunte Oogele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaburulu Oopesthunte Oogele"/>
</div>
</pre>
