---
title: 'ela mandela song lyrics'
album: 'Mandela'
artist: 'Bharath Sankar'
lyricist: 'Arivu'
director: 'Madonne Ashwin'
path: '/albums/mandela-song-lyrics'
song: 'Ela Mandela'
image: ../../images/albumart/mandela.jpg
date: 2021-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lJIjJ3quDOk"
type: 'Introduction'

singers:
- Anthony Dasan
- Musa Mashiane (South Africa)
---

<pre class="lyrics-native">
  
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annadang kaachikku ponnaada thookkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annadang kaachikku ponnaada thookkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambani vandhachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambani vandhachu le…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambana pottikku unnoda vote-ukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambana pottikku unnoda vote-ukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Demand-u undaachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Demand-u undaachu le…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cutting-u shaving-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cutting-u shaving-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaandharai ippo sitting-eh style-aachu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaandharai ippo sitting-eh style-aachu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappeesu meenukku leg piece-ah kaamichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappeesu meenukku leg piece-ah kaamichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkara vechachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkara vechachu le…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattatha vaalukku out-aana ball-ukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattatha vaalukku out-aana ball-ukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vote ID thandhachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote ID thandhachu le…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanga Mr Mandela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Mr Mandela"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta poda vandhela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta poda vandhela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkulla oruthanum agapadala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkulla oruthanum agapadala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai mudikkaran pullukattu thaadikaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai mudikkaran pullukattu thaadikaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Oravunnu sollikida oruthan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oravunnu sollikida oruthan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilichavaayana nethu nethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilichavaayana nethu nethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulicha odane soakku soakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulicha odane soakku soakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhappu vanguthu kaathu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhappu vanguthu kaathu kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaicha peru ippa Mandela Mandela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaicha peru ippa Mandela Mandela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savaram panna coolie-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaram panna coolie-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru savaram thangam poduya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru savaram thangam poduya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivaram konjam kooruya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaram konjam kooruya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan power-ah vandhu paaruya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan power-ah vandhu paaruya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un card-ah konjam kaamiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un card-ah konjam kaamiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vote-u podura saamy-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vote-u podura saamy-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh, thalai vandachu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh, thalai vandachu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai senjachu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai senjachu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi nattachu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi nattachu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi pattasu pattasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi pattasu pattasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraathi edunga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraathi edunga da"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasava paaratti paadungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasava paaratti paadungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha naatoda manamtha kaapatha vandharu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha naatoda manamtha kaapatha vandharu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappa bappa bappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa bappa bappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Quarter-ah thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quarter-ah thaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yokkiyan yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yokkiyan yaaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam thaana serum paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam thaana serum paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa thandha vangi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa thandha vangi poduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaangum paname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaangum paname"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungappan kaasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungappan kaasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annadang kaachikku ponnaada thookkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annadang kaachikku ponnaada thookkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambani vandhachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambani vandhachu le…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambana pottikku unnoda vote-ukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambana pottikku unnoda vote-ukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Demand-u undaachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Demand-u undaachu le…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cutting-u shaving-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cutting-u shaving-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaandharai ippo sitting-eh style-aachu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaandharai ippo sitting-eh style-aachu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappeesu meenukku leg piece-ah kaamichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappeesu meenukku leg piece-ah kaamichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkara vechachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkara vechachu le…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattatha vaalukku out-aana ball-ukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattatha vaalukku out-aana ball-ukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vote ID thandhachu le…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote ID thandhachu le…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ela..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandela ela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandela ela…"/>
</div>
</pre>