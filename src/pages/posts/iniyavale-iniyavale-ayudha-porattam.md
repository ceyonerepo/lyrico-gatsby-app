---
title: "iniyavale iniyavale song lyrics"
album: "Ayudha Porattam"
artist: "Nandhan Raj - S.S. Athreya"
lyricist: "Thamizh Amuthan"
director: "Jai Akash"
path: "/albums/ayudha-porattam-lyrics"
song: "Iniyavale, Iniyavale"
image: ../../images/albumart/ayudha-porattam.jpg
date: 2011-09-09
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iniyavaley iniyavaley idhayathil irundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyavaley iniyavaley idhayathil irundhavaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyavaley iniyavaley idhayathil irundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyavaley iniyavaley idhayathil irundhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigaliley nadandhavaley nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigaliley nadandhavaley nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">varavugalil midhandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavugalil midhandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiranuvil pugundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiranuvil pugundhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanneeril sirithavaley nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneeril sirithavaley nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">en swaasam soodaanadhey nee illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en swaasam soodaanadhey nee illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vizhi kooda irulaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vizhi kooda irulaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyavaley iniyavaley idhayathil irundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyavaley iniyavaley idhayathil irundhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigaliley nadandhavaley nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigaliley nadandhavaley nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey nee engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuvaaga en nenjil poo vaithadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaaga en nenjil poo vaithadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaaga ennulley thee vaithadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaaga ennulley thee vaithadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu neeyallavo hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu neeyallavo hO"/>
</div>
<div class="lyrico-lyrics-wrapper">lesaaga punnagai seidhu nee poagindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lesaaga punnagai seidhu nee poagindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanamaana kaayangal pattu naan vaazhgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanamaana kaayangal pattu naan vaazhgindren"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum en vaazhviley vaanavil thoandrumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum en vaazhviley vaanavil thoandrumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyavaley iniyavaley idhayathil irundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyavaley iniyavaley idhayathil irundhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigaliley nadandhavaley nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigaliley nadandhavaley nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey nee engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai thedi en swaasam poagindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai thedi en swaasam poagindradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">unai naadi en vaazhkai theigindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai naadi en vaazhkai theigindradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu kaadhal vali hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu kaadhal vali hO"/>
</div>
<div class="lyrico-lyrics-wrapper">sudum kanneer thuli hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudum kanneer thuli hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu enna boomiyai vittu thaniyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu enna boomiyai vittu thaniyaagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illai enbadhinaaley tharai saaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illai enbadhinaaley tharai saaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhaley neeyum thaan saabamaa moatchamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaley neeyum thaan saabamaa moatchamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyavaley iniyavaley idhayathil irundhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyavaley iniyavaley idhayathil irundhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigaliley nadandhavaley nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigaliley nadandhavaley nee engey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey nee engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey nee engey"/>
</div>
</pre>
