---
title: "ishq namazaa song lyrics"
album: "The Big Bull"
artist: "Gourov Dasgupta"
lyricist: "Kunwar Juneja"
director: "Kookie Gulati"
path: "/albums/the-big-bull-lyrics"
song: "Ishq Namazaa"
image: ../../images/albumart/the-big-bull.jpg
date: 2021-04-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/-1LAtT9u3ag"
type: "Love"
singers:
  - Ankit Tiwari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dil mera mangdi hai kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mera mangdi hai kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu chaahe meri jaan mang le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu chaahe meri jaan mang le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwabon ko bas ik vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon ko bas ik vaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu yaara mere rang, rang de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu yaara mere rang, rang de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil mera mangdi hai kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mera mangdi hai kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu chaahe meri jaan mang le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu chaahe meri jaan mang le"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwabon ko bas ik vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon ko bas ik vaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu yaara mere rang, rang de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu yaara mere rang, rang de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh meetha meetha hai khaara paani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh meetha meetha hai khaara paani"/>
</div>
<div class="lyrico-lyrics-wrapper">Meethiyan teri yaadan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meethiyan teri yaadan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan tere mere ishq diyan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan tere mere ishq diyan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucchiyan hai parwazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucchiyan hai parwazaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhme main nahi tujhme tu nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhme main nahi tujhme tu nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil di sunn aawaaza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil di sunn aawaaza"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jabse mila hoon tujhse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse mila hoon tujhse"/>
</div>
<div class="lyrico-lyrics-wrapper">Main ghumshuda hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main ghumshuda hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Subah sham tu hi tu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subah sham tu hi tu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bata main kahan hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bata main kahan hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh meetha meetha hai khaara paani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh meetha meetha hai khaara paani"/>
</div>
<div class="lyrico-lyrics-wrapper">Meethiyan teri yaadan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meethiyan teri yaadan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan tere mere ishq diyan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan tere mere ishq diyan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucchiyan hai parwazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucchiyan hai parwazaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhme main nahi tujhme tu nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhme main nahi tujhme tu nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil di sunn aawaaza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil di sunn aawaaza"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa main tan paniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa main tan paniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq namazaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq namazaa"/>
</div>
</pre>
