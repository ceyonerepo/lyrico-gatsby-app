---
title: "naan yaarumilla song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "CS Amudhan - Chandru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Naan Yaarumilla"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4FY2-BivRR4"
type: "happy"
singers:
  - Mark
  - Kumaresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tsunamiyin benamiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tsunamiyin benamiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla narigalai ozhikkum nalla nanbanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla narigalai ozhikkum nalla nanbanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru aaniyum pudunga venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aaniyum pudunga venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena vittudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena vittudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Please ena vittudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please ena vittudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Please please ena vittudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please please ena vittudunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ethuvum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ethuvum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan worth-eh illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan worth-eh illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ethuvum seiya maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ethuvum seiya maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkum oorumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum oorumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pozhappu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pozhappu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku arivumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku arivumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ethuvum ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ethuvum ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhikka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhikka maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku mass illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku mass illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velocity illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velocity illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Volume illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volume illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gravity illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gravity illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lion illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lion illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiger illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiger illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Giraffe illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giraffe illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Zebravum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zebravum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruppudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruppudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paba bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Paba bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Paba bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pa bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pa bab paba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">PETA vandha enakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="PETA vandha enakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Methane edutha enakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methane edutha enakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaai thorakka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaai thorakka maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayae thorakka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayae thorakka maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku U venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku U venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tax free venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tax free venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass opening venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass opening venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu mattum podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku U venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku U venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tax free venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tax free venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass opening venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass opening venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu mattum podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga nalla irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga nalla irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha naattil ulla ezhaigalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha naattil ulla ezhaigalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu munnera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlam varlam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlam varlam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlam vaa varlam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlam vaa varlam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varenmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varenmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan eppo varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppo varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae eppadi varuvennu theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae eppadi varuvennu theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku varuvennu theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku varuvennu theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna naan varavae maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna naan varavae maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndha koottam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndha koottam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam junior artist
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam junior artist"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizh paalum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh paalum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu paalum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu paalum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vazha vaithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vazha vaithadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavin paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavin paalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Election vandha pesamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Election vandha pesamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Twitteril karuthu poda maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitteril karuthu poda maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Araisiyalil naan kudhikka maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araisiyalil naan kudhikka maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Beachila dhiyaanam panna maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beachila dhiyaanam panna maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku nadikka theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku nadikka theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engappa enakku sollitharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engappa enakku sollitharala"/>
</div>
<div class="lyrico-lyrics-wrapper">Well odavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Well odavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyavum mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyavum mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan US kilambi poiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan US kilambi poiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paba bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Paba bab paba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paba bab paba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap pa bab pabapabap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap pa bab pabapabap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhu vaazhu vaazhavidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhu vaazhu vaazhavidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Slow motion walk-la therikka vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slow motion walk-la therikka vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum nanbenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyum nanbenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooki adichuduvan paathukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki adichuduvan paathukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan TV-yil irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan TV-yil irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaven daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaven daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En producerukku paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En producerukku paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Care-eh illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Care-eh illa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir moochu sangam daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir moochu sangam daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandabam kattitu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandabam kattitu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En kalyanam daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kalyanam daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thalaivan illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thalaivan illa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku aasai illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku aasai illa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mandram illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mandram illa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhae kalachitten daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhae kalachitten daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan romba pesithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan romba pesithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila padamae illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila padamae illa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam thirunthi vanthuthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam thirunthi vanthuthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa call sheet illa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa call sheet illa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu attack pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu attack pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thagaraaru pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thagaraaru pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu gilma pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu gilma pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu damage pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu damage pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu idea pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu idea pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu assault pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu assault pandra shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu attract pandra shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu attract pandra shiva"/>
</div>
</pre>
