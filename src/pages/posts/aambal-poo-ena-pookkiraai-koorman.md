---
title: "aambal poo ena pookkiraai song lyrics"
album: "Koorman"
artist: "Tony Britto"
lyricist: "Uma Devi"
director: "Bryan B. George"
path: "/albums/koorman-lyrics"
song: "Aambal Poo Ena Pookkiraai"
image: ../../images/albumart/koorman.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vgQDHOgwS1I"
type: "happy"
singers:
  - Diwakar
  - Vaishali Jaishankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aambal poo ena pookirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aambal poo ena pookirai"/>
</div>
<div class="lyrico-lyrics-wrapper">aanil thaaimaiyai serkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanil thaaimaiyai serkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">aalam ver varai pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalam ver varai pogirai"/>
</div>
<div class="lyrico-lyrics-wrapper">maya thoranam aagirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya thoranam aagirai"/>
</div>
<div class="lyrico-lyrics-wrapper">paalaiyil varum neer ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalaiyil varum neer ena"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkiren unai ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkiren unai ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">niraiverithe nijamaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niraiverithe nijamaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">ival aasaigal anai thanduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival aasaigal anai thanduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">soonil aadidum muyalgal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soonil aadidum muyalgal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi naam ingu vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi naam ingu vaalvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andrilai pola thumbinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andrilai pola thumbinil"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthida thunmathil moolgi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthida thunmathil moolgi"/>
</div>
<div class="lyrico-lyrics-wrapper">than paadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than paadume"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilai maarinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilai maarinum"/>
</div>
<div class="lyrico-lyrics-wrapper">innilai yaayinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innilai yaayinum"/>
</div>
<div class="lyrico-lyrics-wrapper">unidam naan vanthu seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unidam naan vanthu seranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam than kai kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam than kai kooduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal than enai aaluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal than enai aaluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pathaigal sugam aaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathaigal sugam aaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paarangal sumai neenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarangal sumai neenguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thevathai unthan paathangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevathai unthan paathangal"/>
</div>
<div class="lyrico-lyrics-wrapper">veetaiye alagakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetaiye alagakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaal itta manam pookuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaal itta manam pookuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thoda sugam kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thoda sugam kooduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">erumbukum pasi aatridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erumbukum pasi aatridum"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhai naan petra thozhi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhai naan petra thozhi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">oor vantha ethirthalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor vantha ethirthalume"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unai piriyenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unai piriyenada"/>
</div>
<div class="lyrico-lyrics-wrapper">aalaiyil varum neer ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalaiyil varum neer ena"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkiren unai ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkiren unai ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">niraiveruthe nijamaaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niraiveruthe nijamaaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ival aasaigal anai thanduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival aasaigal anai thanduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">soolil aadidum muyalgal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolil aadidum muyalgal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi naam ingu vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi naam ingu vaalvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andrilai pola thumbinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andrilai pola thumbinil"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthida thunmathil moolgi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthida thunmathil moolgi"/>
</div>
<div class="lyrico-lyrics-wrapper">than paadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than paadume"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilai maarinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilai maarinum"/>
</div>
<div class="lyrico-lyrics-wrapper">innilai yaayinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innilai yaayinum"/>
</div>
<div class="lyrico-lyrics-wrapper">unidam naan vanthu seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unidam naan vanthu seranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aambal poo ena pookirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aambal poo ena pookirai"/>
</div>
<div class="lyrico-lyrics-wrapper">aanil thaaimaiyai serkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanil thaaimaiyai serkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">aalam ver varai pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalam ver varai pogirai"/>
</div>
<div class="lyrico-lyrics-wrapper">maya thoranam aagirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya thoranam aagirai"/>
</div>
<div class="lyrico-lyrics-wrapper">paalaiyil varum neer ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalaiyil varum neer ena"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkiren unai ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkiren unai ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">niraiverithe nijamaagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niraiverithe nijamaagave"/>
</div>
<div class="lyrico-lyrics-wrapper">ival aasaigal anai thanduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival aasaigal anai thanduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">soonil aadidum muyalgal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soonil aadidum muyalgal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi naam ingu vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi naam ingu vaalvom"/>
</div>
</pre>
