---
title: 'manjal veyil song lyrics'
album: 'Vettaiyadu Vilayadu'
artist: 'Harris Jayaraj'
lyricist: 'Thamarai'
director: 'Gowtham Vasudev Menon'
path: '/albums/vettaiyadu-vilayadu-song-lyrics'
song: 'Manjal Veyil'
image: ../../images/albumart/vettaiyadu-vilayadu.jpg
date: 2006-08-25
lang: tamil
singers: 
- Hariharan
- Krish
- Nakul
youtubeLink: "https://www.youtube.com/embed/Tuw1V4w_cQo"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum idam ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda kooda vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda kooda vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathira pattaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Natchathira pattaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottikondu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottikondu vanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  …………………………
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manjal veyil maalaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Manjal veyil maalaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella iruludhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella mella iruludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Palichidum vilakugal
<input type="checkbox" class="lyrico-select-lyric-line" value="Palichidum vilakugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pol kaattudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagal pol kaattudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thayakangal vilagudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thayakangal vilagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavipugal thodarudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavipugal thodarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthathu enna enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aduthathu enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae thaan theduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endrae thaan theduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum idam ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda kooda vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda kooda vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathira pattaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Natchathira pattaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottikondu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottikondu vanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ulagathin kadaisi naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagathin kadaisi naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru thaanaa enbadhu pol
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru thaanaa enbadhu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi pesi theertha pinnum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesi pesi theertha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho ondru kuraiyudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Etho ondru kuraiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ullae oru chinnan chiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullae oru chinnan chiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maragatha maatram vanthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maragatha maatram vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurukuru minnal ena
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurukuru minnal ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurukkae odudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurukkae odudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum idam ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda kooda vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda kooda vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathira pattaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Natchathira pattaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottikondu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottikondu vanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manjal veyil maalaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Manjal veyil maalaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella iruludhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mella mella iruludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Palichidum vilakugal
<input type="checkbox" class="lyrico-select-lyric-line" value="Palichidum vilakugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pol kaattudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagal pol kaattudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thayakangal vilagudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thayakangal vilagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavipugal thodarudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavipugal thodarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthathu enna enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aduthathu enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae thaan theduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endrae thaan theduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vannangal vannangal attra
<input type="checkbox" class="lyrico-select-lyric-line" value="Vannangal vannangal attra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil vazhiyil sila
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyil vazhiyil sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkiraar… nadakkiraar
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadakkiraar… nadakkiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ..manjalum pachaiyum kondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ah ..manjalum pachaiyum kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peidhu peidhu mazhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Peidhu peidhu mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaigiraar nanaigiraar
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanaigiraar nanaigiraar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaaro yaaro yaaro aval
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro yaaro yaaro aval"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei..yaaro yaaro yaaro avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Hei..yaaro yaaro yaaro avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodum kodum vettikolla
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodum kodum vettikolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru thandavaalum otti chella…
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru thandavaalum otti chella…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum idam ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda kooda vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda kooda vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavae velli velli nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vennilavae velli velli nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathira pattaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Natchathira pattaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottikondu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottikondu vanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Innum konjum nelavendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum konjum nelavendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha nodi intha nodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha nodi intha nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanaiyo kaalam thalli
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethanaiyo kaalam thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjorum panithuli
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjorum panithuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nindru paarka neram indri
<input type="checkbox" class="lyrico-select-lyric-line" value="Nindru paarka neram indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru kondae irundhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sendru kondae irundhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikka vaithaal pesa vaithaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikka vaithaal pesa vaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjorum panithuli..ohooo..hooo…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjorum panithuli..ohooo..hooo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thath thara tha thara tha thara ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line" value="Thath thara tha thara tha thara ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh…hooo…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh…hooo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath thara tha thara tha thara ra ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line" value="Thath thara tha thara tha thara ra ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath thara tha thara tha thara ra ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line" value="Thath thara tha thara tha thara ra ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath thara tha thara tha thara ra ra ra ra….
<input type="checkbox" class="lyrico-select-lyric-line" value="Thath thara tha thara tha thara ra ra ra ra…."/>
</div>
</pre>