---
title: "saregama padhaneesa song lyrics"
album: "Abhiyum Anuvum"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "B.R. Vijayalakshmi"
path: "/albums/abhiyum-anuvum-lyrics"
song: "Saregama Padhaneesa"
image: ../../images/albumart/abhiyum-anuvum.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YDt2-O_2bA0"
type: "happy"
singers:
  - Haricharan
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Pa Tha Nee Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Pa Tha Nee Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Pole En Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Pole En Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Nee Tha Pa Ma Ga Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Nee Tha Pa Ma Ga Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Ahnen Naan Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Ahnen Naan Pudhusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennulle Dhinam Pesikonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle Dhinam Pesikonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthene Unai Kanum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthene Unai Kanum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnale Enthan Sorkkal Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Enthan Sorkkal Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrodu Ketten Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu Ketten Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Re Ga Ma Pa Tha Nee Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Re Ga Ma Pa Tha Nee Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Neethaan Paravasaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Neethaan Paravasaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Nee Tha Pa Ma Ga Re Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Nee Tha Pa Ma Ga Re Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ratham Ellam Vairasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ratham Ellam Vairasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekeyil Paattu Pottathaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekeyil Paattu Pottathaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Tulipsai Veetil Nattathaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tulipsai Veetil Nattathaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Camouflage Ennil Ahnahaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Camouflage Ennil Ahnahaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enni Naan Unnile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enni Naan Unnile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cold Stone Mela Ice Cream Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cold Stone Mela Ice Cream Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Kothi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Kothi Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaco Chippa Unn Vaarthainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaco Chippa Unn Vaarthainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thoovi Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thoovi Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Pola Pesum Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Pola Pesum Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai Whatsapp Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Whatsapp Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechai Kettu En Kan Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechai Kettu En Kan Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bluetikka Mara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bluetikka Mara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkam Irandu Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkam Irandu Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhaivu Irandu Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhaivu Irandu Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvil Irandu Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvil Irandu Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naal Vidumurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Vidumurai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakkam Oru Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkam Oru Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam Maru Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam Maru Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasal Sila Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasal Sila Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukkam Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukkam Dhinam Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum Vizhiyumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum Vizhiyumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanumadiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanumadiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanathil Kai Korthukonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanathil Kai Korthukonde"/>
</div>
</pre>
