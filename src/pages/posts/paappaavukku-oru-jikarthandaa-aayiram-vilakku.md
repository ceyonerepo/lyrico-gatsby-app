---
title: "paappaavukku oru jikarthandaa song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Paappaavukku Oru Jikarthandaa"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PqY3ZgdatTQ"
type: "happy"
singers:
  - Karthik
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa songwayaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa songwayaanoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa songwayaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa songwayaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva thazhukka konjam minukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva thazhukka konjam minukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha thallaakkulamae thazhumbumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha thallaakkulamae thazhumbumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva kolusu konjam kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva kolusu konjam kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha alangaa nalloor alarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha alangaa nalloor alarumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai mulaikka mannai tholaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai mulaikka mannai tholaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thanniyum veppamum seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thanniyum veppamum seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna mayakka pakkam izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna mayakka pakkam izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayamum moolaiyum seranum.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayamum moolaiyum seranum."/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaa.aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaa.aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa songwayaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa songwayaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annikki theppakkola padikkattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annikki theppakkola padikkattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththavachchi nee irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththavachchi nee irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellakkatti onnakkandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakkatti onnakkandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappukkotti sappukkotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappukkotti sappukkotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Echchi vaththiponadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echchi vaththiponadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi vandhu ae pulla nee eeramaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi vandhu ae pulla nee eeramaakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ochadai pullaiyaara neeyum suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ochadai pullaiyaara neeyum suththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththikkolla koazhikkaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththikkolla koazhikkaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchanthalai melum suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchanthalai melum suththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna suththi naanum suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna suththi naanum suththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lachchukkettu ponadhadi en pozhappu.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lachchukkettu ponadhadi en pozhappu."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku oru nalla sedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku oru nalla sedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli anuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli anuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaazhaiyae adi sevvaazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaazhaiyae adi sevvaazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En mogathin vazhi undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mogathin vazhi undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthaanaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthaanaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaadiyae manam thallaadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaadiyae manam thallaadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasukkul vizhunthadhu thallaadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manasukkul vizhunthadhu thallaadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaadiyae perungillaadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaadiyae perungillaadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kilikkunja pudicha naa killaadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kilikkunja pudicha naa killaadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lalalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lalalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigarthandaa jigarthandaa jigarthandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigarthandaa jigarthandaa jigarthandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echchi vachikkuduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echchi vachikkuduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usshaa usshaananaa songwayaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usshaa usshaananaa songwayaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virutchamo vidhaikkulla irundhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virutchamo vidhaikkulla irundhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhaikkathu sondhamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaikkathu sondhamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulla azhagellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulla azhagellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkunnu sonthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkunnu sonthamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirupperungundrathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupperungundrathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumanam kai kuzhandhaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumanam kai kuzhandhaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaichabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keladi dhindukkallu melakkotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keladi dhindukkallu melakkotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesaiyellaam suththi suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesaiyellaam suththi suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegappatta malli ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegappatta malli ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaikkedai vaangivandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaikkedai vaangivandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin pakkam vachchivida enakkaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin pakkam vachchivida enakkaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennazhagae enakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennazhagae enakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannavenum oththaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannavenum oththaasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondaa kondaa kondaa kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa kondaa kondaa kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkoodi vandha azhagellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkoodi vandha azhagellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">onnaa rendaa onna renda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnaa rendaa onna renda"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna ottikkolla pattappaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna ottikkolla pattappaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaa rendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa rendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaa indhaa adi indhaa indhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa indhaa adi indhaa indhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura meenaatchi kungumamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura meenaatchi kungumamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaa indhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaa indhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lalalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lalalalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaakkoru jigarthandaa koduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaakkoru jigarthandaa koduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paappaa adhai echchi vachikkuduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paappaa adhai echchi vachikkuduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva thazhukka konjam minukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva thazhukka konjam minukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha thallaakkulamae thazhumbumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha thallaakkulamae thazhumbumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva kolusu konjam kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva kolusu konjam kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha alangaa nalloor alarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha alangaa nalloor alarumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinghujikku jikkujing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinghujikku jikkujing"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinghujikku jikkujing jing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinghujikku jikkujing jing"/>
</div>
</pre>
