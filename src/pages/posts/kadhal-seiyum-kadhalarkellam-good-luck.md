---
title: "kadhal seiyum kadhalarkellam song lyrics"
album: "Good Luck"
artist: "Manoj Bhatnaghar"
lyricist: "Vairamuthu"
director: "Manoj Bhatnaghar"
path: "/albums/good-luck-song-lyrics"
song: "Kadhal Seiyum Kadhalarkellam"
image: ../../images/albumart/good-luck.jpg
date: 2000-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xsItLjLXnBA"
type: "intro"
singers:
  - S.P. Balasubrahmanyam
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal seiyum kadhalarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal seiyum kadhalarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal thedum manavarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal thedum manavarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo ooho good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo ooho good luck"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai pooththa pookalukkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai pooththa pookalukkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai pookkum pookalukkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai pookkum pookalukkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo oohoo good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo oohoo good luck"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vaaname vaaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vaaname vaaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Good luck solli vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Good luck solli vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey bhoomiye bhoomiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey bhoomiye bhoomiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanukku good luck solli vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanukku good luck solli vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal seiyum kadhalarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal seiyum kadhalarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal thedum manavarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal thedum manavarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo oohoo good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo oohoo good luck"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo oohoo good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo oohoo good luck"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai konda neerthuli ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai konda neerthuli ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo ooho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo ooho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sippiyodu servathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sippiyodu servathum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sippi kanda neerthuli ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sippi kanda neerthuli ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yehae hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yehae hae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthendrum aavathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthendrum aavathum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerthuli sippiyil searattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerthuli sippiyil searattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiku good luck sollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiku good luck sollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram muthu thiralattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram muthu thiralattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sippikku good luck sollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sippikku good luck sollungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendrale thendrale Solaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendrale thendrale Solaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck solli vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck solli vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thingale thingale alliku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thingale thingale alliku"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck solli vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck solli vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal seiyum kadhalarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal seiyum kadhalarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal thedum manavarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal thedum manavarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo ooho good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo ooho good luck"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulirkaalam vanthathum konjam laalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirkaalam vanthathum konjam laalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maarbu theduthu nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maarbu theduthu nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaikaalam vanthathum konjam o ooho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaikaalam vanthathum konjam o ooho"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madi thaedi kenjuthu nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi thaedi kenjuthu nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madiyae kudaiyaai maarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyae kudaiyaai maarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaikku good luck sollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaikku good luck sollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbu soodu kidaikkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbu soodu kidaikkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulirukku good luck sollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulirukku good luck sollungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalaa….kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalaa….kaadhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku good luck sollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku good luck sollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kadhali…..kadhali…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kadhali…..kadhali…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaikku good bye solli vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaikku good bye solli vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal seiyum kadhalarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal seiyum kadhalarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal thedum manavarkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal thedum manavarkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo ooho good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo ooho good luck"/>
</div>
<div class="lyrico-lyrics-wrapper">Good luck oo ooho good luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good luck oo ooho good luck"/>
</div>
</pre>
