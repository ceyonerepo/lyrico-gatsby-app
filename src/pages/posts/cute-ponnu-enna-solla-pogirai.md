---
title: "cute ponnu song lyrics"
album: "Enna Solla Pogirai"
artist: "Vivek – Mervin"
lyricist: "Arivu"
director: "A. Hariharan"
path: "/albums/enna-solla-pogirai-lyrics"
song: "Cute Ponnu"
image: ../../images/albumart/enna-solla-pogirai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JAZuSpFmMCE"
type: "love"
singers:
  - Anirudh Ravichander
  - Vivek Siva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimirana Cute Pie Silaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirana Cute Pie Silaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Modern 9 To 5 Mazhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Modern 9 To 5 Mazhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Zara Kooti Poi Jora Shopping Seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Zara Kooti Poi Jora Shopping Seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Baby Naanum Ready Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Baby Naanum Ready Ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Like Ku Like Ku Panna Nodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Like Ku Like Ku Panna Nodiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Psych Psych Saravediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Psych Psych Saravediye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Alli Cutie Unnai Killi Kooti Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Alli Cutie Unnai Killi Kooti Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tajmahal Kaata Ready Ye Ready Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tajmahal Kaata Ready Ye Ready Ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Free Time La Unakku Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Time La Unakku Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Fce Time La Adikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fce Time La Adikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendsaaga Pazhagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendsaaga Pazhagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene Zone Nu Yedhukku Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Zone Nu Yedhukku Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Moon Palichu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moon Palichu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell Phone Velicham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phone Velicham Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Zen Modu Manasa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zen Modu Manasa Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala Kalacha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Kalacha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jaldi Jaldi Jaldi Jaldi Jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaldi Jaldi Jaldi Jaldi Jaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaldi Jaldi Jaldi Jaldi Jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaldi Jaldi Jaldi Jaldi Jaldi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Ulla Ulla Ulla Ulla Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ulla Ulla Ulla Ulla Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panna Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mella Mella Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mella Mella Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninna Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninna Vandhu Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Ulla Ulla Ulla Ulla Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ulla Ulla Ulla Ulla Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panna Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mella Mella Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mella Mella Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninna Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninna Vandhu Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Money Heist Nairobi Needhan En Vaidhegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money Heist Nairobi Needhan En Vaidhegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Flight Yeri Thaan Polama Savari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Flight Yeri Thaan Polama Savari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kavidhai Sollaama Kanneerum Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kavidhai Sollaama Kanneerum Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja Poo Neettama Oru Gentleman-ah Nadippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Poo Neettama Oru Gentleman-ah Nadippen"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confuse Pannatha Kannala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confuse Pannatha Kannala Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Stun-aren Unnala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Stun-aren Unnala Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Unfollow Pannaalum Altaama Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Unfollow Pannaalum Altaama Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Nippen Un Munnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Nippen Un Munnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Free Time La Unakku Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free Time La Unakku Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Face Time La Adikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face Time La Adikkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendsaaga Pazhagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendsaaga Pazhagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene Zonu Yedhukku Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Zonu Yedhukku Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Moon Palichu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moon Palichu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell Phone Velicham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phone Velicham Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Zen Modu Manasa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zen Modu Manasa Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala Kalacha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Kalacha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jaldi Jaldi Jaldi Jaldi Jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaldi Jaldi Jaldi Jaldi Jaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jaldi Jaldi Jaldi Jaldi Jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jaldi Jaldi Jaldi Jaldi Jaldi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ponnu Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ponnu Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Me Follow Me Follow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Me Follow Me Follow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me Allow Me Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me Allow Me Allow Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Allow Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allow Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Ulla Ulla Ulla Ulla Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ulla Ulla Ulla Ulla Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panna Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mella Mella Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mella Mella Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninna Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninna Vandhu Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Ulla Ulla Ulla Ulla Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ulla Ulla Ulla Ulla Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panna Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Mella Mella Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Mella Mella Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninna Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninna Vandhu Ninna"/>
</div>
</pre>
