---
title: 'payanangal - megam kalaithalum song lyrics'
album: 'Dharala Prabhu'
artist: 'Bharath Shankar'
lyricist: 'Nixy'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Payanangal - Megam Kalainthalum'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Shakthisree Gopalan
youtubeLink: "https://www.youtube.com/embed/vwgCQtsnEy0"
type: 'Sad'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Megam Kalainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Kalainthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Kalaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Kalaiyaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Karainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Karainthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vannam Karaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vannam Karaiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal Ondraga Thedum Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Ondraga Thedum Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu Ponadhu Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu Ponadhu Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaa Varthaigal Sollum Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaa Varthaigal Sollum Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvugalaai Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvugalaai Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal Aarum Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Aarum Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakangal Yen Sol Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakangal Yen Sol Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavariya Vazhigal Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavariya Vazhigal Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">En Moochu Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Innor Valiyai Thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innor Valiyai Thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrathai Naam Muyandrum Thaangidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrathai Naam Muyandrum Thaangidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thedum Poluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thedum Poluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Virupangal Idam Maaridumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virupangal Idam Maaridumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai Thedum Vazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Thedum Vazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanangal Mudinthodidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal Mudinthodidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera Sogam Thee Raavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Sogam Thee Raavum"/>
</div>
</pre>