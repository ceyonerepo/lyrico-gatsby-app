---
title: "suraj pe mangal bhari - title track song lyrics"
album: "Suraj pe mangal bhari"
artist: "Kingshuk Chakravarty"
lyricist: "Abhishek Sharma"
director: "Abhishek Sharma"
path: "/albums/suraj-pe-mangal-bhari-lyrics"
song: "Suraj Pe Mangal Bhari - Title Track"
image: ../../images/albumart/suraj-pe-mangal-bhari.jpg
date: 2020-11-15
lang: hindi
youtubeLink: "https://www.youtube.com/embed/jh407047cNQ"
type: "title track"
singers:
  - Sanj V
  - Chinmayi Tripathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isse Bachne Ka Nahi Koi Bhi Seen Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isse Bachne Ka Nahi Koi Bhi Seen Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi Nahi Bachega Kehla Re Baahu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi Nahi Bachega Kehla Re Baahu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunlo Kya Kehte Hai Gyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunlo Kya Kehte Hai Gyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunlo Kya Kehte Hai Gyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunlo Kya Kehte Hai Gyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka To Status Pemanent Quarntine Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka To Status Pemanent Quarntine Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Quarntine Mein Beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quarntine Mein Beta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutte Billi Ka Bair Inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutte Billi Ka Bair Inka"/>
</div>
<div class="lyrico-lyrics-wrapper">Janam Janam Ka Panga Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janam Janam Ka Panga Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik Duje Ka Pair Kiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik Duje Ka Pair Kiche"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamam Mein Har Koi Nanga Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamam Mein Har Koi Nanga Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanga Hai Bhyi Nanga Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga Hai Bhyi Nanga Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamam Mein Har Koi Nanga Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamam Mein Har Koi Nanga Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraj Ka Gussa Shamat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Ka Gussa Shamat Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal Ki Chaal Qayamat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal Ki Chaal Qayamat Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraj Ka Gussa Shamat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Ka Gussa Shamat Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal Ki Chaal Qayamat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal Ki Chaal Qayamat Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rok Sako To Roko Inko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rok Sako To Roko Inko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey Rok Sako To Roko Inko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Rok Sako To Roko Inko"/>
</div>
<div class="lyrico-lyrics-wrapper">Varna Dunia Pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varna Dunia Pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aafat Hai Aafat Hai Aafat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aafat Hai Aafat Hai Aafat Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunlo Kya Kehte Hai Gyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunlo Kya Kehte Hai Gyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dono Pagle Hai Prani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono Pagle Hai Prani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunlo Kya Kehte Hai Gyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunlo Kya Kehte Hai Gyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka To Status Pemanent Quarntine Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka To Status Pemanent Quarntine Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Kaisi Nayi Bimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaisi Nayi Bimari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraj Pe Mangal Bhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraj Pe Mangal Bhari"/>
</div>
</pre>
