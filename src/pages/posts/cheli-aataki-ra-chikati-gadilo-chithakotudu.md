---
title: "cheli aataki ra song lyrics"
album: "Chikati Gadilo Chithakotudu"
artist: "Balamurali Balu"
lyricist: "Rakendu Mouli"
director: "Santhosh P. Jayakumar"
path: "/albums/chikati-gadilo-chithakotudu-lyrics"
song: "Cheli Aataki Ra"
image: ../../images/albumart/chikati-gadilo-chithakotudu.jpg
date: 2019-03-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-yyPJQsv34s"
type: "love"
singers:
  - Sharanya Gopinath
  - Prathi Balasubrmanian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">cheli aataki raa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheli aataki raa "/>
</div>
<div class="lyrico-lyrics-wrapper">tholi kaatuki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi kaatuki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">raavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavera"/>
</div>
<div class="lyrico-lyrics-wrapper">pai yetthulu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pai yetthulu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi methaga raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi methaga raa"/>
</div>
<div class="lyrico-lyrics-wrapper">raavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavera"/>
</div>
<div class="lyrico-lyrics-wrapper">ragile deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragile deham"/>
</div>
<div class="lyrico-lyrics-wrapper">conthullo dhaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="conthullo dhaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">challarche vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="challarche vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">valeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">ee vayasa vyuham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee vayasa vyuham"/>
</div>
<div class="lyrico-lyrics-wrapper">olikesey moham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olikesey moham"/>
</div>
<div class="lyrico-lyrics-wrapper">marugai marugai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marugai marugai"/>
</div>
<div class="lyrico-lyrics-wrapper">moksham needhera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moksham needhera"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv nokkeseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv nokkeseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">thagaru chichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaru chichera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cheli aataki raa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheli aataki raa "/>
</div>
<div class="lyrico-lyrics-wrapper">tholi kaatuki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi kaatuki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">raavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavera"/>
</div>
<div class="lyrico-lyrics-wrapper">pai yetthulu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pai yetthulu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi methaga raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi methaga raa"/>
</div>
<div class="lyrico-lyrics-wrapper">raavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavera"/>
</div>
<div class="lyrico-lyrics-wrapper">ragile deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ragile deham"/>
</div>
<div class="lyrico-lyrics-wrapper">conthullo dhaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="conthullo dhaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">challarche vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="challarche vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">valeyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valeyra"/>
</div>
<div class="lyrico-lyrics-wrapper">ee vayasa vyuham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee vayasa vyuham"/>
</div>
<div class="lyrico-lyrics-wrapper">olikesey moham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olikesey moham"/>
</div>
<div class="lyrico-lyrics-wrapper">marugai marugai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marugai marugai"/>
</div>
<div class="lyrico-lyrics-wrapper">moksham needhera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moksham needhera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv nokkeseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv nokkeseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">thagaru chichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaru chichera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv nokkeseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv nokkeseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">thagaru chichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaru chichera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">nuv nokkeseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuv nokkeseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">hey rara rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey rara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vacheseyra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vacheseyra"/>
</div>
<div class="lyrico-lyrics-wrapper">thagaru chichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaru chichera"/>
</div>
</pre>
