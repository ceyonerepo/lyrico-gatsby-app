---
title: 'oru neethi song lyrics'
album: 'Mandela'
artist: 'Bharath Sankar'
lyricist: 'Yugabharathi'
director: 'Madonne Ashwin'
path: '/albums/mandela-song-lyrics'
song: 'Oru Neethi'
image: ../../images/albumart/mandela.jpg
date: 2021-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z9oMpGlNoOs"
type: 'Celebration'

singers:
- Anthakudi Ilayaraja
---

<pre class="lyrics-native">
  
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru needhi onbathu saadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru needhi onbathu saadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaane engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaane engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai ketka ukkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai ketka ukkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam enga ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonangaama odum thanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonangaama odum thanneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thambi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thambi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum sandai seivaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum sandai seivaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rose-u garden engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rose-u garden engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chance-eh illai nee paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance-eh illai nee paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai illai coolie-um illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai illai coolie-um illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verumvaaya melvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verumvaaya melvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai kadhaiya solvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai kadhaiya solvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Verumvaaya melvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verumvaaya melvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai kadhaiya solvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai kadhaiya solvaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi enna saadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi enna saadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkkatha nalla aal yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkatha nalla aal yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi enna needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi enna needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga kelvi ketta bejaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga kelvi ketta bejaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Top-u tucker engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top-u tucker engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Trump-u sir-eh sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trump-u sir-eh sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tasmac-u kadaiya kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tasmac-u kadaiya kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhunthelam kudipponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhunthelam kudipponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattoda arumai pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattoda arumai pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvoram padupponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvoram padupponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palanooru bedhamirundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palanooru bedhamirundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvome onnunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvome onnunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalappaga kadhalum senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalappaga kadhalum senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogamaattom gammunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamaattom gammunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Color color-ah perumai pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color color-ah perumai pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimuraave nadappom nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimuraave nadappom nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavasama edhu vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavasama edhu vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedaiya poi kedappom kedappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaiya poi kedappom kedappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velavaasi yerura podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaasi yerura podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velavaasi yerura podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaasi yerura podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiyaattam iruponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyaattam iruponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Panangaasu sernthadhu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panangaasu sernthadhu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavusaave nadipponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavusaave nadipponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velavaasi yerura podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velavaasi yerura podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiyaattam iruponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyaattam iruponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Panangaasu sernthadhu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panangaasu sernthadhu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavusaave nadipponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavusaave nadipponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimaada ozhachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaada ozhachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanaaliyaave vaazhvonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanaaliyaave vaazhvonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaram kedachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaram kedachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikooliyaave poovomga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikooliyaave poovomga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padichalum manguniyaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichalum manguniyaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichalum manguniyaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichalum manguniyaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook-u’la kedapponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook-u’la kedapponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Cricket-u score-la poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cricket-u score-la poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesa patra valapponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesa patra valapponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradum pagaiye illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradum pagaiye illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiye naan vidava vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiye naan vidava vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Porada saname vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porada saname vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri paarthu sudava sudava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri paarthu sudava sudava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onbathu saadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onbathu saadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu thaane engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaane engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai ketka ukkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai ketka ukkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaane engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaane engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai ketka ukkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai ketka ukkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam enga ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonangaama odum thanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonangaama odum thanneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan thambi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan thambi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum sandai seivaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum sandai seivaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rose-u garden engooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rose-u garden engooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chance-eh illai nee paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance-eh illai nee paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tasmac-u kadaiya kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tasmac-u kadaiya kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhunthelam kudipponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhunthelam kudipponga"/>
</div>
<div class="lyrico-lyrics-wrapper">Palanooru bedhamirundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palanooru bedhamirundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvome onnunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvome onnunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradum pagaiye illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradum pagaiye illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiye naan vidava vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiye naan vidava vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Porada saname vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porada saname vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuri paarthu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri paarthu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soru sugam yedhum vehname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru sugam yedhum vehname"/>
</div>
<div class="lyrico-lyrics-wrapper">TV-yila kavalai marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV-yila kavalai marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star-u padam vandhale poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super star-u padam vandhale poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theater-la whistle-ah parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theater-la whistle-ah parappom"/>
</div>
</pre>