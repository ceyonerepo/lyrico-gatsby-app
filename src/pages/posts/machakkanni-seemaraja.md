---
title: "machakkanni song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Machakkanni"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ldWDN6vDbE8"
type: "love"
singers:
  - D Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machakkanni konjam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machakkanni konjam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaren ennai pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaren ennai pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu katta thatti thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu katta thatti thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren kanna pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren kanna pothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inch inch-ah unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inch inch-ah unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu naanum yengi ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu naanum yengi ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju panjaa ennai aaki naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju panjaa ennai aaki naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora pokkula poottu thaakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora pokkula poottu thaakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu thunda odainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu thunda odainjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machakkanni konjam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machakkanni konjam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaren ennai pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaren ennai pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu katta thatti thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu katta thatti thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren kanna pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren kanna pothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga dhimi thaga jhinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga dhimi thaga jhinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thin thaak ku thim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thin thaak ku thim"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga dhimi thaga jhinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga dhimi thaga jhinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeni pechula scene-eh ootura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeni pechula scene-eh ootura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla nozhainjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla nozhainjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aani veraiyum aatti paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aani veraiyum aatti paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla ozhinjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla ozhinjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koora yeravum songa maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora yeravum songa maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora nee kalaivani pethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora nee kalaivani pethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu vecha ennai idara vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vecha ennai idara vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba pathara vechaMotha aalu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba pathara vechaMotha aalu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu vecha sokku podiyum vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu vecha sokku podiyum vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vedi vechi padiya vecha mogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vedi vechi padiya vecha mogini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machakkanni konjam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machakkanni konjam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaren ennai pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaren ennai pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu katta thatti thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu katta thatti thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren kanna pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren kanna pothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalum odala kaiyum odala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalum odala kaiyum odala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai enni kedanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai enni kedanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal noi ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal noi ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera nee oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera nee oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam vaiyen maruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam vaiyen maruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochila vesam oosi pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochila vesam oosi pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora nee isaignani paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora nee isaignani paatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alla vecha ennai alara vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla vecha ennai alara vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai malara vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai malara vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulla vecha nenja thuvala vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulla vecha nenja thuvala vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi alavechi alaya vecha roatu-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi alavechi alaya vecha roatu-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machakkanni konjom kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machakkanni konjom kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaren ennai pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaren ennai pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu katta thatti thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu katta thatti thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren kanna pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren kanna pothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inch inch-ah unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inch inch-ah unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu naanum yengi ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu naanum yengi ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju panjaa ennai aaki naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju panjaa ennai aaki naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora pokkula poottu thaakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora pokkula poottu thaakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu thunda odainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu thunda odainjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machakkanni konjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machakkanni konjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu paaren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu paaren "/>
</div>
</pre>
