---
title: "ilama irunthe enakku song lyrics"
album: "Marutha"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "GRS"
path: "/albums/marutha-lyrics"
song: "Maruthamalli"
image: ../../images/albumart/marutha.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Cyw5VxEkfS0"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Illama irundhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama irundhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu kodutha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu kodutha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama udalukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama udalukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vecha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vecha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sumandha kadan theerka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sumandha kadan theerka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaralum aagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaralum aagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai maaru senjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai maaru senjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarisamama pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarisamama pogaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illama irundhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama irundhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu kodutha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu kodutha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama udalukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama udalukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vecha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vecha thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha kanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha kanneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla ninnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla ninnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanaaga nee ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanaaga nee ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil yendhi kondaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil yendhi kondaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu seraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerodu seraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kalangi vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kalangi vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhada maarboda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhada maarboda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangi konda en thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangi konda en thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma un karunai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma un karunai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Megamaga maarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megamaga maarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda anba solla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda anba solla "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam kooda podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam kooda podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pol sorgam ingae ver yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pol sorgam ingae ver yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illama irundhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama irundhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu kodutha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu kodutha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama udalukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama udalukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vecha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vecha thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennenna aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam ellam unnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam ellam unnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaalum pinnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaalum pinnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumillai ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumillai ennoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga enga ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga enga ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam thaanae kooda varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thaanae kooda varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inba thunba suzhalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inba thunba suzhalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manasu pinna varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manasu pinna varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayikulla eesan undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayikulla eesan undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai mattum thaan ariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai mattum thaan ariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eesanikku thaaiyirundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eesanikku thaaiyirundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Annai nenjam thaan ariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai nenjam thaan ariyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pol sondham ingae ver yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pol sondham ingae ver yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illama irundhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama irundhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu kodutha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu kodutha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama udalukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama udalukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vecha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vecha thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sumandha kadan theerka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sumandha kadan theerka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaralum aagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaralum aagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai maaru senjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai maaru senjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarisamama pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarisamama pogaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illama irundhu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama irundhu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappu kodutha thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappu kodutha thaayae"/>
</div>
</pre>
