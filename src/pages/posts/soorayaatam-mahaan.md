---
title: "soorayaatam song lyrics"
album: "Mahaan"
artist: "Santhosh Narayanan"
lyricist: "Muthamil"
director: "Karthik Subbaraj"
path: "/albums/mahaan-lyrics"
song: "Soorayaatam"
image: ../../images/albumart/mahaan.jpg
date: 2022-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XdmumizJD0M"
type: "mass"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Soorayada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorayada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi alli thanhuru sura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi alli thanhuru sura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal engalukaga yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal engalukaga yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn ellaai yeeri vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn ellaai yeeri vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaandi vaada veeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi vaada veeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu thanjam thangitu pooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu thanjam thangitu pooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mallukkum sollukkum munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mallukkum sollukkum munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan nikkaporaan neraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan nikkaporaan neraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambaa mannukkulla kotti poththi vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambaa mannukkulla kotti poththi vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththi kadana yeththukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi kadana yeththukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeeri midhichuthan peya adakkithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeeri midhichuthan peya adakkithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga paavam theethukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga paavam theethukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna solla kaathutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna solla kaathutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattipaari vaa mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattipaari vaa mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla oththaiya nikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla oththaiya nikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi sollida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi sollida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam pakkam thaakuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam pakkam thaakuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu moththa soozhchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu moththa soozhchiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjikanum vettiyae thalli thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjikanum vettiyae thalli thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vechidaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vechidaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaadha paasiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaadha paasiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda mudiyaadha pagaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda mudiyaadha pagaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarikitta thinga vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarikitta thinga vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeriputta sikka vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeriputta sikka vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaiyodu ariyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaiyodu ariyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhupaadhai pirandhaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhupaadhai pirandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongura pongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongura pongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuru vandhurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuru vandhurudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surayada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surayada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi alli thanhuru sura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi alli thanhuru sura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaal engalukaga yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal engalukaga yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ellai yeeri vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ellai yeeri vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesaam kollum poosai senjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesaam kollum poosai senjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu aara vaadaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu aara vaadaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaa saetta edudaa vaetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaa saetta edudaa vaetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhanum mundhi nee vandhurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhanum mundhi nee vandhurudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhiraan chandhiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhiraan chandhiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhiraan mandhiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraan mandhiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhutha kenjanum endhiridaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhutha kenjanum endhiridaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththana sondhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththana sondhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnathaan numburom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnathaan numburom"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhi nee endhiri endhiridaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhi nee endhiri endhiridaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatti veikka poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatti veikka poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Peiya pooti veika poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiya pooti veika poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadikkum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadikkum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu kooru pooda poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu kooru pooda poraan"/>
</div>
</pre>
