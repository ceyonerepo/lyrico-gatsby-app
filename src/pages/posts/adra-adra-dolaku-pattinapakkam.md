---
title: "adra adra dolaku song lyrics"
album: "Pattinapakkam"
artist: "Ishaan Dev"
lyricist: "Vijay Sagar"
director: "Jayadev"
path: "/albums/pattinapakkam-lyrics"
song: "Adra Adra Dolaku"
image: ../../images/albumart/pattinapakkam.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SK9qtj7weAM"
type: "happy"
singers:
  - Anthony Daasan
  - Jassie Gift
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kigikku kigikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kigikku kigikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kigikku gigikku gikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kigikku gigikku gikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kigikku kigikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kigikku kigikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kigikku gigikku gikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kigikku gigikku gikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye adra adra dolakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye adra adra dolakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pattinam pakkam kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pattinam pakkam kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye pudra pudra poongoththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pudra pudra poongoththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ponnum paiyanum thaulathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ponnum paiyanum thaulathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan aaraaichi venam balaji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan aaraaichi venam balaji"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu aalekaa jodi sernthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu aalekaa jodi sernthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan azhagaana ponnu meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan azhagaana ponnu meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa aayaachi love-uh marriage-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa aayaachi love-uh marriage-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey twinklu twinklu little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey twinklu twinklu little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu singulu thavulu aatchippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu singulu thavulu aatchippaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinglu jinglu songsa paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinglu jinglu songsa paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Lungila lungila dance-a paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungila lungila dance-a paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ading ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ading ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingo ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingo ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ading ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ading ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingo ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingo ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weightaana oru ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightaana oru ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu sight adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu sight adichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayapadaatha un friend irukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayapadaatha un friend irukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fightaana namma kumbal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fightaana namma kumbal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum supportukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum supportukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Slightaathaan oru whistle-ah vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slightaathaan oru whistle-ah vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyoo prechanainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyoo prechanainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendungathaan frontula varanunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendungathaan frontula varanunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae asaltaathaan uyira tharanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae asaltaathaan uyira tharanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appo machi unna brotherunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo machi unna brotherunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu nenaippaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu nenaippaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga adichikinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga adichikinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaa inaippaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa inaippaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa kalyanathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa kalyanathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonuppapadi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonuppapadi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi sernthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi sernthomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa twinklu twinklu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa twinklu twinklu"/>
</div>
<div class="lyrico-lyrics-wrapper">Little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa lungila lungila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa lungila lungila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance-appaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance-appaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa twinklu twinklu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa twinklu twinklu"/>
</div>
<div class="lyrico-lyrics-wrapper">Little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa lungila lungila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa lungila lungila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance-appaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance-appaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa twinklu twinklu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa twinklu twinklu"/>
</div>
<div class="lyrico-lyrics-wrapper">Little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa lungila lungila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa lungila lungila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance-appaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance-appaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa haa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyichaa thaan un life-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyichaa thaan un life-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee appa takkar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee appa takkar"/>
</div>
<div class="lyrico-lyrics-wrapper">Wife-aala adha senjikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-aala adha senjikoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhaichaa thaan thuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhaichaa thaan thuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta varum kissu tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta varum kissu tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olungaa nee ippo polachikkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olungaa nee ippo polachikkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudi kuduthanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi kuduthanama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda vaala neendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda vaala neendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna veedu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna veedu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Geththu kaattanum daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geththu kaattanum daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulla kutti peththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla kutti peththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto mela pera ezhuthanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto mela pera ezhuthanumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma pistavaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma pistavaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi otharanumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi otharanumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neevona oru auto vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevona oru auto vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moona adhila pona pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moona adhila pona pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa twinklu twinklu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa twinklu twinklu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olaral star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olaral star"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa lungila lungila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa lungila lungila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance-appaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance-appaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye adra adra dolakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye adra adra dolakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pattinam pakkam kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pattinam pakkam kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye pudra pudra poongoththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pudra pudra poongoththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ponnum paiyanum thaulathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ponnum paiyanum thaulathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan aaraaichi venam balaji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan aaraaichi venam balaji"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu aalekaa jodi sernthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu aalekaa jodi sernthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan azhagaana ponnu meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan azhagaana ponnu meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa aayaachi love-uh marriage-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa aayaachi love-uh marriage-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey twinklu twinklu little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey twinklu twinklu little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu singulu thavulu aatchippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu singulu thavulu aatchippaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinglu jinglu songsa paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinglu jinglu songsa paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Lungila lungila dance-a paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lungila lungila dance-a paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ading ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ading ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingo ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingo ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ading ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ading ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingo ading
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingo ading"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dingara dingara dingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dingara dingara dingaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei heihei"/>
</div>
</pre>
