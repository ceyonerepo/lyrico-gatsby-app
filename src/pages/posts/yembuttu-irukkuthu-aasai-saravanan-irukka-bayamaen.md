---
title: "yembuttu irukkuthu aasai song lyrics"
album: "Saravanan Irukka Bayamaen"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/saravanan-irukka-bayamaen-lyrics"
song: "Yembuttu Irukkuthu Aasai"
image: ../../images/albumart/saravanan-irukka-bayamaen.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IV9F8WjPA28"
type: "love"
singers:
  -	Sean Roldan
  - Kalyani Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Embuttu irukkudhu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embuttu irukkudhu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un melaadha kaattaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un melaadha kaattaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambuttu azhagaiyum neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuttu azhagaiyum neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaatta kodiyetha vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaatta kodiyetha vaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaththa koduthavan yengumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaththa koduthavan yengumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ummunnu irukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummunnu irukkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellatha eduthukka ketka venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellatha eduthukka ketka venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammammaa asathuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammammaa asathuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottikkavukkura aalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottikkavukkura aalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indhaadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhaadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Embuttu irukkudhu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embuttu irukkudhu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un melaadha kaattaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un melaadha kaattaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambuttu azhagaiyum neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuttu azhagaiyum neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaatta kodiyetha vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaatta kodiyetha vaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallam kabadam illa unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallam kabadam illa unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna irukkudhu melum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna irukkudhu melum pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallam arinji vellaam vadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam arinji vellaam vadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki kedakkuren dhegam koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki kedakkuren dhegam koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu kalandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu kalandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thuninjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thuninjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa olagaiyum paarthidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa olagaiyum paarthidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollikkoduthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollikkoduthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgha kadhavaiyum saaithidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgha kadhavaiyum saaithidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna paarkkaadhadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna paarkkaadhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo neekaattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo neekaattida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham pola yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham pola yerudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Embuttu irukkudhu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embuttu irukkudhu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un melaadha kaattaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un melaadha kaattaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambuttu azhagaiyum neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuttu azhagaiyum neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaatta kodiyetha vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaatta kodiyetha vaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa light-um unna nenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa light-um unna nenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthuvelakkena maari pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthuvelakkena maari pochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna kadhuppu enna parikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kadhuppu enna parikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenukkuzhiathum medu achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenukkuzhiathum medu achu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththuthala konda ravananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththuthala konda ravananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna rasikkanum thookki vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna rasikkanum thookki vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjakkayironnu pottuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjakkayironnu pottuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna iruttilum nee arunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna iruttilum nee arunthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollakkoodadhadha solli yen kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollakkoodadhadha solli yen kattura"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai yera yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai yera yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Embuttu irukkudhu aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Embuttu irukkudhu aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un melaadha kaattaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un melaadha kaattaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambuttu azhagaiyum neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambuttu azhagaiyum neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaatta kodiyetha vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaatta kodiyetha vaaren"/>
</div>
</pre>
