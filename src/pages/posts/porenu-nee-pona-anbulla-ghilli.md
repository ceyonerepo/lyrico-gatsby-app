---
title: "porenu nee pona song lyrics"
album: "Anbulla Ghilli"
artist: "Arrol Corelli"
lyricist: "Rajavel Nagarajan"
director: "Srinath Ramalingam"
path: "/albums/anbulla-ghilli-lyrics"
song: "Porenu Nee Pona"
image: ../../images/albumart/anbulla-ghilli.jpg
date: 2022-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q1f1rPNWqCw"
type: "sad"
singers:
  - Teejay Arunasalam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Porenu nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porenu nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganu naan poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganu naan poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum nee venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum nee venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennoda vaazhkai vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennoda vaazhkai vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaama nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama enna panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama enna panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppa varuvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppa varuvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu enga ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu enga ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu engum pogaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu engum pogaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppo varuva varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppo varuva varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu irukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaama nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama enna panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama enna panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppa varuvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppa varuvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu enga ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu enga ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu engum pogaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu engum pogaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppo varuva varuvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppo varuva varuvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu irukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla vanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla vanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerai thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerai thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla nee vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla nee vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai konnu thinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai konnu thinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam ponaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam ponaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En usura unnai naan paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura unnai naan paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona naanum kozhunthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona naanum kozhunthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam veesum nesa paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam veesum nesa paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju thudikkum dhinam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju thudikkum dhinam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaama nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama enna panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama enna panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppa varuvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppa varuvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai vittu enga ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu enga ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu engum pogaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu engum pogaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppo varuva varuvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppo varuva varuvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu irukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porenu nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porenu nee pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala killatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala killatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennagum intha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagum intha pulla"/>
</div>
</pre>
