---
title: "bacha bachikey namasthe song lyrics"
album: "Parris Jeyaraj"
artist: "Santhosh Narayanan"
lyricist: "Rokesh - Madras Miran"
director: "Johnson K"
path: "/albums/parris-jeyaraj-lyrics"
song: "Bacha Bachikey Namasthe"
image: ../../images/albumart/parris-jeyaraj.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S7ERoYilZ0I"
type: "Enjoy"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja kuruvi nee yen dam-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja kuruvi nee yen dam-u"/>
</div>
<div class="lyrico-lyrics-wrapper">What is your-u nameu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What is your-u nameu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama kitta calm-ah irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama kitta calm-ah irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukkudi exam-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukkudi exam-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily kooda iru di vaangi thaarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily kooda iru di vaangi thaarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bun butter-u jam-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bun butter-u jam-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dead end-u illa oru route-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dead end-u illa oru route-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Polam-nu sonna ready joot-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polam-nu sonna ready joot-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa…aaaa….aaa…..ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa…aaaa….aaa…..ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo seppu mappu thappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo seppu mappu thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo soap-u shampoo surp-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo soap-u shampoo surp-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkara sakkarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkara sakkarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela thaan akkara akkarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela thaan akkara akkarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pikkira pikkirama nenjula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pikkira pikkirama nenjula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkura seekirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkura seekirama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chudidhar-u podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudidhar-u podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju adi rose-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju adi rose-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju adi rose-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju adi rose-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum light-ah jo bright-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum light-ah jo bright-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolikkidhu face’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolikkidhu face’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa vyasarpaadi katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vyasarpaadi katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pannure di matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pannure di matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada neeya irruka vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada neeya irruka vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa nilludi yenkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa nilludi yenkitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhuru vandhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuru vandhuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuru vandhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuru vandhuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamaaten thondhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamaaten thondhuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuru vandhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuru vandhuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuru vandhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuru vandhuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharamaatan thondhuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamaatan thondhuru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sulur petta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulur petta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaptu kuduma konjudu mundhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaptu kuduma konjudu mundhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulur petta sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulur petta sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu mudhiyor seat-u ma endhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu mudhiyor seat-u ma endhiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja maida sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja maida sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannuviya moravaasal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuviya moravaasal-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja maida sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja maida sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannuviya moravaasal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuviya moravaasal-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa eh vaa dho vaa nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa eh vaa dho vaa nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa eh vaa dho vaa nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa eh vaa dho vaa nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salchinu salchinuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salchinu salchinuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakure di morchinu morchinuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakure di morchinu morchinuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Palchunu pilchinuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palchunu pilchinuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkiren di kambiya pudchinuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkiren di kambiya pudchinuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam nee vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam nee vellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En chinnakutti paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chinnakutti paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnakutti paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnakutti paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhan onnadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhan onnadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaguvom deep-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaguvom deep-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thonguren di padila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thonguren di padila"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sariya paakka mudila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sariya paakka mudila"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam etti paaren velila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam etti paaren velila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma kaththuren kai valila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma kaththuren kai valila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nekkal-ah nekkal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekkal-ah nekkal-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekkal-ah nekkal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekkal-ah nekkal-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaature di look-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaature di look-u-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekkal-ah nekkal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekkal-ah nekkal-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekkal-ah nekkal-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekkal-ah nekkal-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaature di look-u-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaature di look-u-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bacha bachikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh bacha bachikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh bacha bachikey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacha bachikey namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacha bachikey namaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda anju paththu rooba samose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda anju paththu rooba samose"/>
</div>
<div class="lyrico-lyrics-wrapper">Namaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsa soodakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsa soodakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard-ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard-ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hard ah ikkuthu-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hard ah ikkuthu-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard ah ikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard ah ikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katchi paatha hard ah ikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi paatha hard ah ikkuthu"/>
</div>
</pre>
