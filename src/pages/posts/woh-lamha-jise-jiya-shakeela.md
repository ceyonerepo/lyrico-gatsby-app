---
title: "woh lamha jise jiya song lyrics"
album: "Shakeela"
artist: "Veer Samarth"
lyricist: "Kumaar"
director: "Indrajit Lankesh"
path: "/albums/shakeela-lyrics"
song: "Woh Lamha Jise Jiya"
image: ../../images/albumart/shakeela.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/m0MgWzuc0vE"
type: "love"
singers:
  - Vishal Mishra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Woh lamha jise jiya hi na thha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh lamha jise jiya hi na thha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai woh thehra hua, teri meri raahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai woh thehra hua, teri meri raahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja isko dhadkale baahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja isko dhadkale baahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo na hua pehle kabhi ab ho jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo na hua pehle kabhi ab ho jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh lamha jise jiya hi na thha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh lamha jise jiya hi na thha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai woh thehra hua, teri meri raahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai woh thehra hua, teri meri raahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja isko dhadkale baahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja isko dhadkale baahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo na hua pehle kabhi ab ho jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo na hua pehle kabhi ab ho jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigaahon mein yeh jo ishare hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigaahon mein yeh jo ishare hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan afsaane inmein tumhare hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan afsaane inmein tumhare hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri baatein tere lafzon hai gumsuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri baatein tere lafzon hai gumsuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri saansein chale tera raasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri saansein chale tera raasta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Huyi jo mulakatein, soche meri aankhein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyi jo mulakatein, soche meri aankhein"/>
</div>
<div class="lyrico-lyrics-wrapper">Huyi jo mulakatein, soche meri aankhein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyi jo mulakatein, soche meri aankhein"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai tu aaj khwaabon mein ya saamne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai tu aaj khwaabon mein ya saamne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh lamha jise jiya hi na thha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh lamha jise jiya hi na thha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai woh thehra hua, teri meri raahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai woh thehra hua, teri meri raahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja isko dhadkale baahon mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja isko dhadkale baahon mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo na hua pehle kabhi ab ho jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo na hua pehle kabhi ab ho jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan.. Hmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Hmm.."/>
</div>
</pre>
