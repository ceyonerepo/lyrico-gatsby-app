---
title: "olamikka song lyrics"
album: "Jada"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Jada Kumaran"
path: "/albums/jada-lyrics"
song: "Olamikka"
image: ../../images/albumart/jada.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qHHEqB1Ii8k"
type: "love"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Nenapu Muzhusum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenapu Muzhusum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerangadikudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerangadikudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Enna Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enna Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usuru Mulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usuru Mulukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam Poraludhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam Poraludhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannula Nee Nulla Vaikkaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannula Nee Nulla Vaikkaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjaiyum Nee Pichi Pikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjaiyum Nee Pichi Pikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanja Noolu Pola Thaanidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja Noolu Pola Thaanidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavu Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavu Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Pogama Vaanama Valaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Pogama Vaanama Valaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Sikka Vaikkira Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Sikka Vaikkira Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Enna Pikkiraa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Enna Pikkiraa Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaa Venaa Kollaadhaae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaa Venaa Kollaadhaae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Paiththiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Paiththiyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakki Nikkiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakki Nikkiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena Ponen Unnaala Unnaalaunnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Ponen Unnaala Unnaalaunnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soora Kaththaa Nerungi Nerungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kaththaa Nerungi Nerungi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nelaya Kolaichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nelaya Kolaichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami Thaeraa Unna Sumakka Sumakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Thaeraa Unna Sumakka Sumakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aavala Vethachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aavala Vethachaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enna Thanthiriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Thanthiriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Unna Suththa Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Unna Suththa Vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendi Yendi Pen Paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Yendi Pen Paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kana Kachithamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kana Kachithamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Poottura Sathiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Poottura Sathiyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Vaadi En Koodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Vaadi En Koodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevaazha Vaadi En Koodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevaazha Vaadi En Koodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu Theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu Theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veguriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vachi Thaikkathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vachi Thaikkathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olamikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olamikkaa"/>
</div>
</pre>
