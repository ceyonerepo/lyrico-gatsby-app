---
title: "dhak dhak dhak song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "dhak dhak dhak - Nuvvu Nenu Ethuraithe"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9EpcZKr6-x4"
type: "love"
singers:
  - Sarath Santosh
  - Hari Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvvu Nenu Eddhuraithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenu Eddhuraithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansu Mansu Dhagraithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansu Mansu Dhagraithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaslu Aalalay Pongoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaslu Aalalay Pongoothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakli Niddura Mingoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakli Niddura Mingoothunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Motham Uppenaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Motham Uppenaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak Dhak Dhak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chupool Pilpulu Mogoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupool Pilpulu Mogoothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalu Gonthulo Aagoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalu Gonthulo Aagoothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundeku Chematalu Padoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeku Chematalu Padoothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhuku Venukaku Nedoothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhuku Venukaku Nedoothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oopiri Motham Upenaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Motham Upenaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak Dhak Dhak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheetiki Maatiki Guruthuste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheetiki Maatiki Guruthuste"/>
</div>
<div class="lyrico-lyrics-wrapper">Migathawani Maruposte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migathawani Maruposte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaniki Ikka Parugoste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaniki Ikka Parugoste"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalochanlaku Dharuwoste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalochanlaku Dharuwoste"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Motham Uppenaite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Motham Uppenaite"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhak Dhak Dhak Dhak Dhak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhak Dhak Dhak Dhak Dhak"/>
</div>
</pre>
