---
title: "nootokka jillala andagadu song lyrics"
album: "Nootokka Jillala Andagadu"
artist: "Shakthikanth Karthick"
lyricist: "Bhaskarabhatla"
director: "Rachakonda Vidyasagar"
path: "/albums/nootokka-jillala-andagadu-lyrics"
song: "Nootokka Jillala Andagadu"
image: ../../images/albumart/nootokka-jillala-andagadu.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cMS1LhB3RXA"
type: "intro"
singers:
  - Simha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kotagummam center lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotagummam center lo"/>
</div>
<div class="lyrico-lyrics-wrapper">anddagadu okadu unnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anddagadu okadu unnadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathipudi junction lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathipudi junction lo"/>
</div>
<div class="lyrico-lyrics-wrapper">sattibabu andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattibabu andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">taadepalli gudem lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taadepalli gudem lo"/>
</div>
<div class="lyrico-lyrics-wrapper">thathabaab andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathabaab andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raajolu chuttupakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajolu chuttupakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">raamaraj andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raamaraj andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellenee thaladhanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellenee thaladhanne"/>
</div>
<div class="lyrico-lyrics-wrapper">aa handsome evadante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa handsome evadante"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">gotthi surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gotthi surnarayana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagaduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagaduuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dubi dubudubikkuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dubi dubudubikkuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">dubi dubudubikkuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dubi dubudubikkuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unggaraalla juttunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unggaraalla juttunna"/>
</div>
<div class="lyrico-lyrics-wrapper">gangaraju andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gangaraju andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pedda pedda kanddaraalu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedda pedda kanddaraalu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">peddireddy andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peddireddy andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">noonugu meesala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noonugu meesala"/>
</div>
<div class="lyrico-lyrics-wrapper">naanibabu andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanibabu andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaru adugula ethunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaru adugula ethunna"/>
</div>
<div class="lyrico-lyrics-wrapper">aadishesu andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadishesu andhagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anddari anddanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anddari anddanee"/>
</div>
<div class="lyrico-lyrics-wrapper">left leg tho thannese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left leg tho thannese"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surnarayana"/>
</div>
<div class="lyrico-lyrics-wrapper">gotthi surnarayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gotthi surnarayana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagaduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagaduuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nootokka Jillala Andhagaduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootokka Jillala Andhagaduuu"/>
</div>
</pre>
