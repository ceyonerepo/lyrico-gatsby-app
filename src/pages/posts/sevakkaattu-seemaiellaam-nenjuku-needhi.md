---
title: "sevakkaattu seemaiellaam song lyrics"
album: "Nenjuku Needhi"
artist: "Dhibu Ninan Thomas"
lyricist: "Yugabharathi"
director: "Arunraja Kamaraj"
path: "/albums/nenjuku-needhi-lyrics"
song: "Sevakkaattu Seemaiellaam"
image: ../../images/albumart/nenjuku-needhi.jpg
date: 2022-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MJ-wAE7im-Y"
type: "mass"
singers:
  - R Guru Ayyadura
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">arichandira maamannar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arichandira maamannar"/>
</div>
<div class="lyrico-lyrics-wrapper">aachiyil nadanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachiyil nadanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo sathiyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo sathiyame"/>
</div>
<div class="lyrico-lyrics-wrapper">sothanaiyai sarinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothanaiyai sarinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">keezhe vilunthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keezhe vilunthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavo sagathore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavo sagathore"/>
</div>
<div class="lyrico-lyrics-wrapper">unarnthiduveer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarnthiduveer"/>
</div>
<div class="lyrico-lyrics-wrapper">sagalarum vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagalarum vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">serum idukaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serum idukaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">mathiyile irukindratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathiyile irukindratho"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo enge theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo enge theduven"/>
</div>
<div class="lyrico-lyrics-wrapper">aeraatha malai aeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeraatha malai aeri"/>
</div>
<div class="lyrico-lyrics-wrapper">irangatha kadal irangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irangatha kadal irangi"/>
</div>
<div class="lyrico-lyrics-wrapper">vadikaatha kanneer vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadikaatha kanneer vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">odinen thedinen odinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odinen thedinen odinen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne maniye endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne maniye endru"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sevakkaatu seemai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevakkaatu seemai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">aandaare arinchandira raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandaare arinchandira raja"/>
</div>
<div class="lyrico-lyrics-wrapper">arasaanda naatai vitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasaanda naatai vitte"/>
</div>
<div class="lyrico-lyrics-wrapper">naganthaare sella kaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naganthaare sella kaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">pottakaatu puluthi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottakaatu puluthi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poga pogaya ponare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga pogaya ponare"/>
</div>
<div class="lyrico-lyrics-wrapper">ellathaiyum elantha poragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellathaiyum elantha poragu"/>
</div>
<div class="lyrico-lyrics-wrapper">eema kaata sernthaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eema kaata sernthaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sevakkaatu seemai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevakkaatu seemai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">aandaare arinchandira raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandaare arinchandira raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattada kuppaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattada kuppaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">palakume veethiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palakume veethiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">ettoorum suthi vanthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettoorum suthi vanthum"/>
</div>
<div class="lyrico-lyrics-wrapper">patinikku soru ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patinikku soru ille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan poga kaatchi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan poga kaatchi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal poga oorum ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal poga oorum ille"/>
</div>
<div class="lyrico-lyrics-wrapper">mannagi pona pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannagi pona pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">maanidarku jaathi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanidarku jaathi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">panjaanga noolu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjaanga noolu than"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam thonduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam thonduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">annadan kaatiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annadan kaatiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthi saaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthi saaikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kottura theivam kooraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottura theivam kooraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pichi kootun thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichi kootun thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnanga kooraiyumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnanga kooraiyumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">veetula vaalaum engalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetula vaalaum engalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam yen konnanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam yen konnanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sevakkaatu seemai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevakkaatu seemai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">aandaare arinchandira raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandaare arinchandira raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mummari peiyuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mummari peiyuthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu pogam velaiyuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu pogam velaiyuthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">oppari vaikum sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oppari vaikum sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">osara vazhi theriyalanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osara vazhi theriyalanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mullodu poo irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mullodu poo irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">muthathula nilam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthathula nilam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kallana kadavul inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallana kadavul inge"/>
</div>
<div class="lyrico-lyrics-wrapper">karunai inge engu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunai inge engu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">karporam ethiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karporam ethiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kaatala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kaatala"/>
</div>
<div class="lyrico-lyrics-wrapper">nei sora potume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nei sora potume"/>
</div>
<div class="lyrico-lyrics-wrapper">mannak kaakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannak kaakala"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyula unmai posungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyula unmai posungi"/>
</div>
<div class="lyrico-lyrics-wrapper">poga ethana sattam ammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga ethana sattam ammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">neethiya kaasuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethiya kaasuku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaangura manusan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangura manusan"/>
</div>
<div class="lyrico-lyrics-wrapper">nikiran paru munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikiran paru munnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sevakkaatu seemai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevakkaatu seemai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">aandaare arinchandira raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandaare arinchandira raja"/>
</div>
<div class="lyrico-lyrics-wrapper">arasaanda naatai vitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasaanda naatai vitte"/>
</div>
<div class="lyrico-lyrics-wrapper">naganthaare sella kaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naganthaare sella kaasa"/>
</div>
<div class="lyrico-lyrics-wrapper">pottakaatu puluthi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottakaatu puluthi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poga pogaya ponare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga pogaya ponare"/>
</div>
<div class="lyrico-lyrics-wrapper">ellathaiyum elantha poragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellathaiyum elantha poragu"/>
</div>
<div class="lyrico-lyrics-wrapper">eema kaata sernthaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eema kaata sernthaare"/>
</div>
<div class="lyrico-lyrics-wrapper">arichandra raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arichandra raasa"/>
</div>
</pre>
