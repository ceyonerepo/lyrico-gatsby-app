---
title: "oh azhage song lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Oh Azhage"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/op5aHic6uic"
type: "love"
singers:
  - Benny Dayal 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Oh azhage oh azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh azhage oh azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta sinungi vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta sinungi vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sinunga nee sinunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sinunga nee sinunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnai pularuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnai pularuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil nenjam malaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil nenjam malaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh maname oh maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh maname oh maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum paniyin keezhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum paniyin keezhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi oliya poi oliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi oliya poi oliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam yenguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil thanjam theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil thanjam theduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoora alaigal kaalai nanaikkum maya nadhi idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoora alaigal kaalai nanaikkum maya nadhi idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam thaninthu vaasal thelikkum thooya mazhai idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam thaninthu vaasal thelikkum thooya mazhai idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhakkal thorum ooramaga nindru paarkka nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhakkal thorum ooramaga nindru paarkka nerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilakkal indri vaanam kooda mounamaaga maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilakkal indri vaanam kooda mounamaaga maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru veedu parivodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru veedu parivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaverka neelum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaverka neelum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanodu urayaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanodu urayaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu pol oru varam ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu pol oru varam ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh azhage oh azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh azhage oh azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta sinungi vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta sinungi vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sinunga nee sinunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sinunga nee sinunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnai pularuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnai pularuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil nenjam malaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil nenjam malaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatru kumizhi pola udainthu pogum magizhvinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kumizhi pola udainthu pogum magizhvinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottri valarthu pottri valarthu kaatru kaakkum oruvanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottri valarthu pottri valarthu kaatru kaakkum oruvanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuzhaavi thedi kannin munbu kondu kaattum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuzhaavi thedi kannin munbu kondu kaattum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaavil kooda nervathillai nernthathintha maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaavil kooda nervathillai nernthathintha maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan yaaro thoduvaano tholaithoora kodu thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan yaaro thoduvaano tholaithoora kodu thaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi thero varalaaro puriya kurunagai thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thero varalaaro puriya kurunagai thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh azhage oh azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh azhage oh azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta sinungi vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta sinungi vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sinunga nee sinunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sinunga nee sinunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnai pularuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnai pularuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh maname oh maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh maname oh maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum paniyin keezhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum paniyin keezhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi oliya poi oliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi oliya poi oliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam yenguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil thanjam theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil thanjam theduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangatha kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangatha kaatre"/>
</div>
</pre>
