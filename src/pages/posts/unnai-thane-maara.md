---
title: "unnai thane song lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Unnai Thane"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/om278haxhfQ"
type: "love"
singers:
  - Deepthi Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ooho ho oo…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho ho oo….."/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaithaanae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaithaanae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaithaanae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaithaanae….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ooril engum thedum urangaa vizhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooril engum thedum urangaa vizhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vendum evarum ariyaa mozhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vendum evarum ariyaa mozhigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya nathiyae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya nathiyae….."/>
</div>
<div class="lyrico-lyrics-wrapper">Yeazhu kadal thaandi naan yaenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeazhu kadal thaandi naan yaenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram ularaatha kaalodu nirkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram ularaatha kaalodu nirkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae oru kanam iru bathil kodu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru kanam iru bathil kodu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooho….oooho oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho….oooho oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho….oooho oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho….oooho oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho….oooho oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho….oooho oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooho….oooho oo oo…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooho….oooho oo oo….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooo…..ooo…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo…..ooo….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai thanae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thanae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Santhipeanae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhipeanae….."/>
</div>
<div class="lyrico-lyrics-wrapper">Yaezhaam malaiyum kadalum thaandi naanae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaezhaam malaiyum kadalum thaandi naanae.."/>
</div>
</pre>
