---
title: "raule song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Sukh Sanghera"
path: "/albums/above-all-lyrics"
song: "Raule"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/8RdDC-_6ICs"
type: "Mass"
singers:
  - Jassa Dhillon
  - Gurlez Akhtar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aye yo!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye yo!"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody knows it's
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody knows it's"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho vehla shaam da tu nikhri phire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho vehla shaam da tu nikhri phire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho vehla shaam da tu nikhri phire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho vehla shaam da tu nikhri phire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni munda dhaniyan ch behke sikhe time chakkna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni munda dhaniyan ch behke sikhe time chakkna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaye ve tere karke main nikhri phiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye ve tere karke main nikhri phiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere karke main nikhri phiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere karke main nikhri phiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu changga lagge tainu chori chori takkna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu changga lagge tainu chori chori takkna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho lagga pata ve tu vaili ae kahaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho lagga pata ve tu vaili ae kahaunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagga pata ve tu vaili ae kahaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagga pata ve tu vaili ae kahaunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind rauleyan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind rauleyan ch pehla naam aunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind raule'yan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind raule'yan ch pehla naam aunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukh rakhe baba tu pasand jatt di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukh rakhe baba tu pasand jatt di"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala pakki yaari na piche hutt di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala pakki yaari na piche hutt di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni main gharon bahla aam tera rang aa badaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni main gharon bahla aam tera rang aa badaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve mera nature ae shy dil'on bhari bethi haami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve mera nature ae shy dil'on bhari bethi haami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kehandi naal di tu yaariyan kamaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kehandi naal di tu yaariyan kamaunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke main kehandi naal di tu yaariyan kamaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke main kehandi naal di tu yaariyan kamaunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind rauleyan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind rauleyan ch pehla naam aunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind raule'yan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind raule'yan ch pehla naam aunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho gall udni aa pakka tere mere ne yarane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho gall udni aa pakka tere mere ne yarane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve ankhan teriyan ne milne de labhne bahaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve ankhan teriyan ne milne de labhne bahaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ditte jaane nahi jawaab bade uthne sawaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ditte jaane nahi jawaab bade uthne sawaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakhe jehda je tu vadda main vi turan naal naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakhe jehda je tu vadda main vi turan naal naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kyon ni jasseya gulaab tu fdaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kyon ni jasseya gulaab tu fdaunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyon ni jasseya gulaab tu fdaunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon ni jasseya gulaab tu fdaunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind rauleyan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind rauleyan ch pehla naam aunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind raule'yan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind raule'yan ch pehla naam aunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh chakkni jo teri ve suroor jeha chadhaundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh chakkni jo teri ve suroor jeha chadhaundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dine gora rang, raati yaad aa sataundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dine gora rang, raati yaad aa sataundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakhe gali vich gehda kaahton dabb vich laake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakhe gali vich gehda kaahton dabb vich laake"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere karke le aaya billo border ton jaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere karke le aaya billo border ton jaake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho munda kavaan te nishane nahiyon launda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho munda kavaan te nishane nahiyon launda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho munda kavaan te nishane nahiyon launda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho munda kavaan te nishane nahiyon launda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind rauleyan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind rauleyan ch pehla naam aunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tere agge bolda hi nahi bolda hi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere agge bolda hi nahi bolda hi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pind raule'yan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pind raule'yan ch pehla naam aunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho raule'yan ch pehla naam aunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho raule'yan ch pehla naam aunda"/>
</div>
</pre>
