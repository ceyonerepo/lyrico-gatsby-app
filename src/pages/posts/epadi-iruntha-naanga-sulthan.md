---
title: "epadi iruntha naanga song lyrics"
album: "Sulthan"
artist: "vivek - mervin"
lyricist: "viveka"
director: "Bakkiyaraj Kannan"
path: "/albums/sulthan-song-lyrics"
song: "Epadi Iruntha Naanga"
image: ../../images/albumart/sulthan.jpg
date: 2020-04-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A8AbWotG_gk"
type: "Motivational"
singers:
  - Anthony Daasan
  - Mahalingam
  - Vivek Siva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Iduppu Pudichikichu,Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Iduppu Pudichikichu,Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyilu Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyilu Kanna Kattuthe Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu Pudichikichu, Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Pudichikichu, Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninji Nimirave Mudiyala Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuninji Nimirave Mudiyala Samy,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gandu Yeruthe Hoi Hoi Hoi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gandu Yeruthe Hoi Hoi Hoi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Kalu Novuthe Hoi Hoi Hoi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Kalu Novuthe Hoi Hoi Hoi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thoppai Idikkuthe Hoi Hoi Hoi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thoppai Idikkuthe Hoi Hoi Hoi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Athadi Gethellam Nethoda Poche,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Athadi Gethellam Nethoda Poche,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalula Kooda Vizhurom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalula Kooda Vizhurom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Karunai Katu Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Karunai Katu Sulthan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalukaga Dhinamum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalukaga Dhinamum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanneer Vidurom Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanneer Vidurom Sulthan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalula Kooda Vizhurom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalula Kooda Vizhurom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Karunai Katu Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Karunai Katu Sulthan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalukaga Dhinamum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalukaga Dhinamum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanneer Vidurom Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanneer Vidurom Sulthan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyilu Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyilu Kanna Kattuthe Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninji Nimirave Mudiyala Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuninji Nimirave Mudiyala Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattuthe Samy Ada Mudiyala Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattuthe Samy Ada Mudiyala Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattuthe Samy,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Rukkumani Hey Hey Rukkumani Hey,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rukkumani Hey Hey Rukkumani Hey,"/>
</div>
<div class="lyrico-lyrics-wrapper">En Rukkumani En Kannumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Rukkumani En Kannumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Sikkuma Nee Summa Erangi Va Ma,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sikkuma Nee Summa Erangi Va Ma,"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemmadi Athadi En Rasathi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmadi Athadi En Rasathi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Voottu Vizhaketha Va Ma,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Voottu Vizhaketha Va Ma,"/>
</div>
<div class="lyrico-lyrics-wrapper">Orukka Nee Siricha Podhum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orukka Nee Siricha Podhum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Surukka Enga Vazhiye Pogum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surukka Enga Vazhiye Pogum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Narukka Kann Adicha Podhum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narukka Kann Adicha Podhum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Marukka Nanga Oorukku Povom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Marukka Nanga Oorukku Povom,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Ah Konjam Love Ah Konjam Oththukko,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ah Konjam Love Ah Konjam Oththukko,"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyana Kettikara Pulla Idhu Koththikko,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyana Kettikara Pulla Idhu Koththikko,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttikkadha Muttikkadha Kattikko,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikkadha Muttikkadha Kattikko,"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaroda Laddu Pola Rendu Pulla Peththukko,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaroda Laddu Pola Rendu Pulla Peththukko,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalula Kooda Vizhurom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalula Kooda Vizhurom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Karunai Katu Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Karunai Katu Sulthan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalukaga Dhinamum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalukaga Dhinamum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanneer Vidurom Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanneer Vidurom Sulthan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kalula Kooda Vizhurom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kalula Kooda Vizhurom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Karunai Katu Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Karunai Katu Sulthan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhalukaga Dhinamum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhalukaga Dhinamum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanneer Vidurom Sulthan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanneer Vidurom Sulthan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyilu Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyilu Kanna Kattuthe Samy,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninji Nimirave Mudiyala Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuninji Nimirave Mudiyala Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Iruntha Nanga Ippadi Ayitom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Iruntha Nanga Ippadi Ayitom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Wanted Ah Vanthu Inga Mattikittom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wanted Ah Vanthu Inga Mattikittom,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthum Veyilu Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthum Veyilu Kanna Kattuthe Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu Pudichikichu Kazhuthu Suzhukikichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Pudichikichu Kazhuthu Suzhukikichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninji Nimirave Mudiyala Samy, Kanna Kattuthe Samy,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuninji Nimirave Mudiyala Samy, Kanna Kattuthe Samy,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Mudiyala Samy, Kanna Kattuthe Samy.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Mudiyala Samy, Kanna Kattuthe Samy."/>
</div>
</pre>
