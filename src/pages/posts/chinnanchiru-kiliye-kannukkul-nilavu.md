---
title: "chinnanchiru kiliye song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Chinnanchiru Kiliye"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xJWFTRHKX_c"
type: "melody"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnanjiru Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Vennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Meedhu Thottil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Meedhu Thottil Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Valarththa Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Valarththa Sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Sogam Kandaal Undhan Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Sogam Kandaal Undhan Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Veesum Kannaa Endhan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Veesum Kannaa Endhan Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Vennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Meedhu Thottil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Meedhu Thottil Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Valarththa Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Valarththa Sooriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Neraththilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Neraththilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaa Ketkumundhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaa Ketkumundhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Nilavu Kudiyirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Nilavu Kudiyirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazh Oraththilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Oraththilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhum Thenthuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhum Thenthuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Amudhaai Amudhaai Adhu Inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhaai Amudhaai Adhu Inikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Siriththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Siriththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Dheiveega Sangeedham Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Dheiveega Sangeedham Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarththal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarththal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Dheebangal En Nenjil Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Dheebangal En Nenjil Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Vennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Meedhu Thottil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Meedhu Thottil Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Valarththa Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Valarththa Sooriyane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Annaiyendraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Annaiyendraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Alli Thandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Alli Thandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganaai Pirandhu Thavam Mudithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganaai Pirandhu Thavam Mudithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Thedi Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Thedi Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi Aadi Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi Aadi Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeriyum Vilakkaai Oli Kuduppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeriyum Vilakkaai Oli Kuduppaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nizhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Magan Pola Paalootta Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Magan Pola Paalootta Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattum Indha Sondhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattum Indha Sondhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Vennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Meedhu Thottil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Meedhu Thottil Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Valarththa Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Valarththa Sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Sogam Kandaal Undhan Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Sogam Kandaal Undhan Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Veesum Kannaa Endhan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Veesum Kannaa Endhan Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Vennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Meedhu Thottil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Meedhu Thottil Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Valarththa Sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Valarththa Sooriyane"/>
</div>
</pre>
