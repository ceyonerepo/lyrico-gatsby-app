---
title: "kathalillaiyea endru song lyrics"
album: "Semmari Aadu"
artist: "Renjith Vasudev"
lyricist: "unknown"
director: "Sathish Subramaniam"
path: "/albums/semmari-aadu-song-lyrics"
song: "Kathalillaiyea Endru"
image: ../../images/albumart/semmari-aadu.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O-aKc2Plf0A"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaadhal illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sonnal adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sonnal adi"/>
</div>
<div class="lyrico-lyrics-wrapper">namba villaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namba villaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">un kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">solla theriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla theriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru santham ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru santham ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ellai illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee mazhaiyaai vanthaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mazhaiyaai vanthaal "/>
</div>
<div class="lyrico-lyrics-wrapper">nanaiven naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiven naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevan naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevan naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sonnal adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sonnal adi"/>
</div>
<div class="lyrico-lyrics-wrapper">namba villaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namba villaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">un kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru vaartha sollu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaartha sollu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasa naanum thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasa naanum thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla neruppai mooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla neruppai mooti"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham kaatura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patta maram pooka thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta maram pooka thane"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa vachen nee than pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa vachen nee than pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">en maarbil sanju enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maarbil sanju enna"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakka paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakka paakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saami kitta varamaa thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami kitta varamaa thana"/>
</div>
<div class="lyrico-lyrics-wrapper">unna than da nanum ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna than da nanum ketpen"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaiye maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiye maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pochu unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi kathula than thoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kathula than thoothu"/>
</div>
<div class="lyrico-lyrics-wrapper">viten kathala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viten kathala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">adi yendi pulla enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi yendi pulla enna"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum paakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum paakuraye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi oodi vanthu senthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi oodi vanthu senthu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kathal athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kathal athu"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha santhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha santhame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathala sollama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathala sollama"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla enna"/>
</div>
<div class="lyrico-lyrics-wrapper">vaati vathakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaati vathakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalai aati saami aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai aati saami aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">enna neyum mayakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna neyum mayakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vaartha solla thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaartha solla thane"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjukulla pacha kuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla pacha kuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thanda vachu iruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thanda vachu iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">thookame maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookame maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pochu unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadu theruvula nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadu theruvula nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha theru pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha theru pola"/>
</div>
<div class="lyrico-lyrics-wrapper">adi suthi suthi ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi suthi suthi ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu paakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu paakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kaathal illa endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kaathal illa endru"/>
</div>
<div class="lyrico-lyrics-wrapper">solla nanum onnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla nanum onnum"/>
</div>
<div class="lyrico-lyrics-wrapper">siththar illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siththar illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal illai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal illai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sonnal adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sonnal adi"/>
</div>
<div class="lyrico-lyrics-wrapper">namba villaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namba villaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">un kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">solla theriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla theriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru santham ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru santham ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ellai illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan megam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee mazhaiyaai vanthaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mazhaiyaai vanthaal "/>
</div>
<div class="lyrico-lyrics-wrapper">nanaiven naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiven naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevan naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevan naanada"/>
</div>
</pre>