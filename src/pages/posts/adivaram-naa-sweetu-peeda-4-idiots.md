---
title: "adivaram song lyrics"
album: "4 Idiots"
artist: "Jaya surya"
lyricist: "Jaya surya"
director: "S. Satish Kumar"
path: "/albums/4-idiots-lyrics"
song: "Adivaram Naa Sweetu Peeda"
image: ../../images/albumart/4-idiots.jpg
date: 2019-11-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wKXWqRn6300"
type: "happy"
singers:
  - Jai Srinivas
  - Sruthika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adivaram Naa Sweetu Peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaram Naa Sweetu Peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Imante Istharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imante Istharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Somavaram Soamajiguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somavaram Soamajiguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramante Vastharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramante Vastharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manglavaaram Muddulicchukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manglavaaram Muddulicchukuntavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhavaaram Bugga Gillanisthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhavaaram Bugga Gillanisthavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruvaaram Guttugocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruvaaram Guttugocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loo Guttu Laageiroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loo Guttu Laageiroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayidalu Endukinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayidalu Endukinka"/>
</div>
<div class="lyrico-lyrics-wrapper">O pattu Pattairooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O pattu Pattairooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adivaram Naa Sweetu Peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaram Naa Sweetu Peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Imante Istharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imante Istharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Somavaram Soamajiguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somavaram Soamajiguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramante Vastharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramante Vastharaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorukunte Naa Dhoorachupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorukunte Naa Dhoorachupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koorinantha Istharoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorinantha Istharoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoochukunte Naa Pattu Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoochukunte Naa Pattu Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Motthamgaa Needheroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motthamgaa Needheroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Etthu Palla choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Etthu Palla choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Chitthainde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Chitthainde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Andham Paina Dooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Andham Paina Dooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikeyalani Vundhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikeyalani Vundhee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siggulanni Chinthapandu Cheyyaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggulanni Chinthapandu Cheyyaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnadaanni Chira Kongu Laagaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnadaanni Chira Kongu Laagaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave Naa Rajahamsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave Naa Rajahamsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thircheyi Naa Aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thircheyi Naa Aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Mallepulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Mallepulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reddygaa Nee Theccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reddygaa Nee Theccha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adivaram Naa Sweetu Peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaram Naa Sweetu Peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Imante Istharoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imante Istharoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Somavaram Soamajiguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somavaram Soamajiguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramante Vastharoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramante Vastharoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aggipulla Gisesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggipulla Gisesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neechupu Thone Naa Yenkee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neechupu Thone Naa Yenkee"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Gulla Chesesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Gulla Chesesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvulatho Kulikee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvulatho Kulikee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thega Nacchaoye Kurrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega Nacchaoye Kurrada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottesei Golisoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottesei Golisoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Vukkiri Bikkiri Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Vukkiri Bikkiri Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Modalettei Gurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Modalettei Gurudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttukona Ninnu Chutta Poddulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttukona Ninnu Chutta Poddulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu Pettukona Nee Paccha Bottulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu Pettukona Nee Paccha Bottulo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi Roju Icchukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Roju Icchukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedavi Thambulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedavi Thambulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathivaaram Pandagele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathivaaram Pandagele"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvala Yevvaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvala Yevvaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adivaram Naa Sweetu Peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaram Naa Sweetu Peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Imante Istharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imante Istharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Somavaram Soamajiguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somavaram Soamajiguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramante Vastharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramante Vastharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manglavaaram Muddulicchukuntavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manglavaaram Muddulicchukuntavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhavaaram Bugga Gillanisthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhavaaram Bugga Gillanisthavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruvaaram Guttugocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruvaaram Guttugocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loo Guttu Laageiroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loo Guttu Laageiroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayidalu Endukinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayidalu Endukinka"/>
</div>
<div class="lyrico-lyrics-wrapper">O pattu Pattairooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O pattu Pattairooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adivaram Naa Sweetu Peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaram Naa Sweetu Peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Imante Istharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imante Istharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Somavaram Soamajiguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somavaram Soamajiguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramante Vastharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramante Vastharaa"/>
</div>
</pre>
