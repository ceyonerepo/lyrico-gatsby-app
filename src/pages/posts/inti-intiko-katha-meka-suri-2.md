---
title: "inti intiko katha song lyrics"
album: "Meka Suri 2"
artist: "Prajwal Krish"
lyricist: "Gaddam Veeraih"
director: "Trinadh Velisala"
path: "/albums/meka-suri-2-lyrics"
song: "Inti Intiko Katha"
image: ../../images/albumart/meka-suri-2.jpg
date: 2020-11-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/y6Q8C5aPH-M"
type: "melody"
singers:
  - Vinayak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inti Intiko Katha Rama Gaadhale Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Intiko Katha Rama Gaadhale Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meka Suri Rani Seetharaamule Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meka Suri Rani Seetharaamule Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajyakaanksha Kaadhuraa Kaanakegaledhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajyakaanksha Kaadhuraa Kaanakegaledhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Veella Janta Meedha Vaale Aapadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Veella Janta Meedha Vaale Aapadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulokate Ayinaa Maraka Padithe Painaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulokate Ayinaa Maraka Padithe Painaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magani Madhini Kosedhi Anumaanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magani Madhini Kosedhi Anumaanaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhula Thalalu Leni Purushathanamu Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhula Thalalu Leni Purushathanamu Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudikathalo Ravanundu Appalanayude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudikathalo Ravanundu Appalanayude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vyasanaparula Pasula Cheralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyasanaparula Pasula Cheralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuvulodise Raanalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuvulodise Raanalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanalo Sagamu Vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanalo Sagamu Vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Vagachi Kurise Vaanalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Vagachi Kurise Vaanalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Katakataalalo Dhivaakarunilaa Kanikarincheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katakataalalo Dhivaakarunilaa Kanikarincheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumani Andaa Vibheeshanudigaa Seshagirigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumani Andaa Vibheeshanudigaa Seshagirigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventanadicheraa Bhayapadakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventanadicheraa Bhayapadakundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaga Bhaga Bhagguna Bhugaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhaga Bhagguna Bhugaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Noppulidise Nippula Sega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noppulidise Nippula Sega"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaga Dhaga Dhikkula Emukalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga Dhaga Dhikkula Emukalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkuna Pagiledi Paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkuna Pagiledi Paga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida Vida Vichhalavidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida Vida Vichhalavidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizrumbhana Shara Lanka Bedhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizrumbhana Shara Lanka Bedhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thege Thegeraa Thegipaderaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thege Thegeraa Thegipaderaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavana Meda Rakkasi Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavana Meda Rakkasi Thoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inti Intiko Katha Rama Gaadhale Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Intiko Katha Rama Gaadhale Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meka Suri Rani Seetharaamule Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meka Suri Rani Seetharaamule Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajyakaanksha Kaadhuraa Kaanakegaledhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajyakaanksha Kaadhuraa Kaanakegaledhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Veella Janta Meedha Vaale Aapadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Veella Janta Meedha Vaale Aapadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kapila Dhandutho Kadhana Dharanipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapila Dhandutho Kadhana Dharanipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulidiseraa Avani Dhadiseraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulidiseraa Avani Dhadiseraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Migatha Dhoochhulaa Madhapu Machhala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migatha Dhoochhulaa Madhapu Machhala "/>
</div>
<div class="lyrico-lyrics-wrapper">Cherapa Vachheraa Thirige Pichhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherapa Vachheraa Thirige Pichhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhiga Dhiga Dhikkuna Dhigamai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhiga Dhiga Dhikkuna Dhigamai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ravanudaa Veerabhadhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravanudaa Veerabhadhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Patapata Pandlanu Korikida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patapata Pandlanu Korikida "/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaranagani Kaki Mudra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaranagani Kaki Mudra"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugekana Posigina Thana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugekana Posigina Thana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kasi Benakani Kalarudhra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Benakani Kalarudhra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranagona Yuddhaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranagona Yuddhaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Chanithodigenu Ghaada Nidra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanithodigenu Ghaada Nidra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inti Intiko Katha Rama Gaadhale Sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Intiko Katha Rama Gaadhale Sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meka Suri Rani Seetharaamule Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meka Suri Rani Seetharaamule Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajyakaanksha Kaadhuraa Kaanakegaledhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajyakaanksha Kaadhuraa Kaanakegaledhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Veella Janta Meedha Vaale Aapadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Veella Janta Meedha Vaale Aapadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulokate Ayinaa Maraka Padithe Painaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulokate Ayinaa Maraka Padithe Painaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magani Madhini Kosedhi Anumaanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magani Madhini Kosedhi Anumaanaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhula Thalalu Leni Purushathanamu Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhula Thalalu Leni Purushathanamu Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudikathalo Ravanundu Appalanayude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudikathalo Ravanundu Appalanayude"/>
</div>
</pre>
