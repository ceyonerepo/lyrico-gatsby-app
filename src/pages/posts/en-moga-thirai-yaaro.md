---
title: "en moga thirai song lyrics"
album: "Yaaro"
artist: "Jose Franklin"
lyricist: "Adithya Suresh"
director: "Sandeep Sai"
path: "/albums/yaaro-lyrics"
song: "En Moga Thirai"
image: ../../images/albumart/yaaro.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8WIUYlTl9DA"
type: "love"
singers:
  - Sathya Prakash
  - MM Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En moga thiraiyaaro nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moga thiraiyaaro nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraimega piraiyaaro neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraimega piraiyaaro neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En swasa araiyaaro nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En swasa araiyaaro nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaarodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro nee yaaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro nee yaaroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi kaattum vazhi yaaro neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi kaattum vazhi yaaro neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamengum valiyaaro nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamengum valiyaaro nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro yaaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaaroda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhiril unai paarpenae ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiril unai paarpenae ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirpaarthu thaan nirkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirpaarthu thaan nirkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiril unai paartha pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiril unai paartha pin"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaamal pongindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaamal pongindrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un thozhgalil saayamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thozhgalil saayamal"/>
</div>
<div class="lyrico-lyrics-wrapper">En kangallum saayathanbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kangallum saayathanbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan irandum saainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan irandum saainthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivaal unarvaal anaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivaal unarvaal anaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival yaaro ival yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival yaaro ival yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayal vizhi vazhi solvaalo ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayal vizhi vazhi solvaalo ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaro ivan yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro ivan yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ivan karai seraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ivan karai seraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival yaaro ival yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival yaaro ival yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayal vizhi vazhi solvaalo ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayal vizhi vazhi solvaalo ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan yaaro ivan yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan yaaro ivan yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ivan karai seraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ivan karai seraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannam sivakkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannam sivakkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu naanam pirakkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu naanam pirakkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai vaanam rasikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai vaanam rasikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kaatrin saaral thoorum podhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kaatrin saaral thoorum podhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu kadhai thodangiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kadhai thodangiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigalai pidithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigalai pidithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru idhazh mulaikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru idhazh mulaikirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam engum undhan sirippin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam engum undhan sirippin satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin kanava idhu kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin kanava idhu kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan serum naal idhuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan serum naal idhuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgalukidayae oru kadhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgalukidayae oru kadhavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai tharavaa oh hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai tharavaa oh hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyae mazhaiyae adai mazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyae mazhaiyae adai mazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai pozhuthin siru veyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai pozhuthin siru veyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarin azhagum un nagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin azhagum un nagalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayu maanavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayu maanavalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isai adhan dhisaiyil undhan pechil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai adhan dhisaiyil undhan pechil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarndhida engum kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarndhida engum kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesidum kaatrinil ilaiyaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesidum kaatrinil ilaiyaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum selgindren naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum selgindren naanae naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En virathangal udainthiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En virathangal udainthiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nam idaiveligal kuzhainthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nam idaiveligal kuzhainthidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilam pen idhazh irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam pen idhazh irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulargiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulargiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada undhan pirivaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada undhan pirivaaladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En virathangal udainthiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En virathangal udainthiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nam idaiveligal kuzhainthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nam idaiveligal kuzhainthidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilam pen idhazh irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam pen idhazh irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulargiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulargiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada undhan pirivaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada undhan pirivaaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannam sivakkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannam sivakkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu naanam pirakkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu naanam pirakkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai vaanam rasikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai vaanam rasikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kaatrin saaral thoorum podhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kaatrin saaral thoorum podhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu kadhai thodangiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kadhai thodangiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigalai pidithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigalai pidithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru idhazh mulaikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru idhazh mulaikirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam engum undhan sirippin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam engum undhan sirippin satham"/>
</div>
</pre>
