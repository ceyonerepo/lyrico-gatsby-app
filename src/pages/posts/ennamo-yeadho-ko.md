---
title: "ennamo yeadho song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky - Sricharan - Emcee Jesz"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Ennamo Yeadho"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fq6egtAzaQM"
type: "love"
singers:
  - Aalap Raju
  - Prashanthini
  - Sricharan
  - Emcee Jesz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennamo Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Thiraluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Thiraluthu Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Piraluthu Ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Piraluthu Ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Iruluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Iruluthu Kanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Mulaikkuthu Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Mulaikkuthu Manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Erinthidum Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Erinthidum Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Aviluthu Kodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Aviluthu Kodiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Uruvamilla Uruvamilla Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Uruvamilla Uruvamilla Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Arai Manathaai Vidiyuthu En Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Arai Manathaai Vidiyuthu En Kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Minni Maraiyuthu Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minni Maraiyuthu Vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Andi Agaluthu Vazhiyyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andi Agaluthu Vazhiyyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhi Sitharuthu Veliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhi Sitharuthu Veliyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki Thavikkuthu Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Thavikkuthu Manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkai Virikkuthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Virikkuthu Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Parakkuthu Tholaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Parakkuthu Tholaivil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Uruvamilla Uruvamilla Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Uruvamilla Uruvamilla Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Arai Manathaai Vidiyuthu En Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Arai Manathaai Vidiyuthu En Kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Ethirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Ethirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Seiyum Manthirama Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Seiyum Manthirama Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththamitta Moochchu Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththamitta Moochchu Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Pattu Kettu Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Pattu Kettu Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vanthu Nirkkum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vanthu Nirkkum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittamittu Etti Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamittu Etti Ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungaathey Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungaathey Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Nenjellaam Nanjaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Nenjellaam Nanjaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaikkaathe Penne Enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikkaathe Penne Enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchangal Achchaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchangal Achchaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippaal Ennai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippaal Ennai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaithaai Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaithaai Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Thiraluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Thiraluthu Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Piraluthu Ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Piraluthu Ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Iruluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Iruluthu Kanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Mulaikkuthu Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Mulaikkuthu Manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Erinthidum Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Erinthidum Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Aviluthu Kodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Aviluthu Kodiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Ethirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Ethirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Seiyum Manthirama Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Seiyum Manthirama Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets Go Wow Wow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Go Wow Wow"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalin Thamizhachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalin Thamizhachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo You are Looking So Fine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo You are Looking So Fine"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka Mudiyalaiye En Manam Andru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka Mudiyalaiye En Manam Andru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manaso Lovely Ippadiye Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manaso Lovely Ippadiye Ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Arugil Naanum Vanthu Seravaa Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Arugil Naanum Vanthu Seravaa Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lady Looking Like a Cinderella Cinderella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady Looking Like a Cinderella Cinderella"/>
</div>
<div class="lyrico-lyrics-wrapper">Naughty Look Ku Vitta Thendrellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naughty Look Ku Vitta Thendrellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lady Looking Like a Cinderella Cinderella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady Looking Like a Cinderella Cinderella"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vattam Idum Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vattam Idum Vennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lady Looking Like a Cinderella Cinderella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady Looking Like a Cinderella Cinderella"/>
</div>
<div class="lyrico-lyrics-wrapper">Naughty Look Ku Vitta Thendrellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naughty Look Ku Vitta Thendrellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lady Looking Like a Cinderella Cinderella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady Looking Like a Cinderella Cinderella"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vattam Idum Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vattam Idum Vennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththi Suththi Unnai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Suththi Unnai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Alaiyum Avasaram Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Alaiyum Avasaram Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththa Saththa Nerisalil Un Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththa Saththa Nerisalil Un Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevigal Ariyum Athisiyam Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevigal Ariyum Athisiyam Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kaana Thaane Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaana Thaane Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kondu Vantheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kondu Vantheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaakkaana Vidaiyum Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaakkaana Vidaiyum Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerum Kondeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum Kondeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhallai Thirudum Mazhalai Naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhallai Thirudum Mazhalai Naano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Thiraluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Thiraluthu Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Piraluthu Ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Piraluthu Ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Iruluthu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Iruluthu Kanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Mulaikkuthu Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Mulaikkuthu Manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Erinthidum Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Erinthidum Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Aviluthu Kodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Aviluthu Kodiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Uruvamilla Uruvamilla Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Uruvamilla Uruvamilla Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Arai Manathaai Vidiyuthu En Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Arai Manathaai Vidiyuthu En Kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Uruvamilla Uruvamilla Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Uruvamilla Uruvamilla Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Kuviyamilla Kuviyamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Kuviyamilla Kuviyamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaatchi Pezhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaatchi Pezhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Arai Manathaai Vidiyuthu Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Arai Manathaai Vidiyuthu Kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho"/>
</div>
</pre>
