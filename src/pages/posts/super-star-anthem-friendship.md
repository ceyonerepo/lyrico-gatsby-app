---
title: 'super star anthem song lyrics'
album: 'Friendship'
artist: 'D.M.Udhaya Kumar'
lyricist: 'Gowtham R'
director: 'John Paul Raj & Sham Surya'
path: '/albums/friendship-song-lyrics'
song: 'Super Star Anthem'
image: ../../images/albumart/friendship.jpg
date: 2020-07-04
lang: tamil
singers: 
- Simbu
youtubeLink: "https://www.youtube.com/embed/jFJ8jaIE6x0"
type: 'sad'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">En maarumela
<input type="checkbox" class="lyrico-select-lyric-line" value="En maarumela"/>
</div>
<div class="lyrico-lyrics-wrapper">En nerambukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="En nerambukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei paadungada
<input type="checkbox" class="lyrico-select-lyric-line" value="Daei paadungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En maarumela
<input type="checkbox" class="lyrico-select-lyric-line" value="En maarumela"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">En nerambukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="En nerambukulla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">En usuru full-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="En usuru full-ah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Summa getha solvom
<input type="checkbox" class="lyrico-select-lyric-line" value="Summa getha solvom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaiya madichuvittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiya madichuvittu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kaapa ethivittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaapa ethivittu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thala kodhivittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thala kodhivittu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Sokka nadanthu vandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Sokka nadanthu vandha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Style mannan super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Style mannan super star"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haeiii….(10 Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Haeiii…."/></div>
</div>
<div class="lyrico-lyrics-wrapper">Super star super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Style mannan super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Style mannan super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Style mannan super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Style mannan super star"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adichu odhachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichu odhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavidum massuku thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavidum massuku thalaivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Avar azhudhu urugi
<input type="checkbox" class="lyrico-select-lyric-line" value="Avar azhudhu urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangadikkum classuku thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalangadikkum classuku thalaivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Stylelukku asthivaaram
<input type="checkbox" class="lyrico-select-lyric-line" value="Stylelukku asthivaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottathu thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Pottathu thalaivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Punch dialogue-il
<input type="checkbox" class="lyrico-select-lyric-line" value="Punch dialogue-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Odavidum paasa thalaivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Odavidum paasa thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikkum single singam da
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikkum single singam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraicha neeyum bangam da
<input type="checkbox" class="lyrico-select-lyric-line" value="Moraicha neeyum bangam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulandha mugam paaru da
<input type="checkbox" class="lyrico-select-lyric-line" value="Kulandha mugam paaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar thaan vera yaaruda
<input type="checkbox" class="lyrico-select-lyric-line" value="Avar thaan vera yaaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan oru thadava sonna
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan oru thadava sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru thadava sonna maathiri…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nooru thadava sonna maathiri…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Super star super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Style mannan super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Style mannan super star"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haeiii..(10 Times)
<input type="checkbox" class="lyrico-select-lyric-line" value="Haeiii.."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">80’s billa
<input type="checkbox" class="lyrico-select-lyric-line" value="80's billa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">90’s basha</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="90's basha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">2’k kaala
<input type="checkbox" class="lyrico-select-lyric-line" value="2'k kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa pettaiyoda orae star
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththa pettaiyoda orae star"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dressa sozhati pottaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Dressa sozhati pottaah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thunda udhari vittaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunda udhari vittaah"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Katti aravanacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti aravanacha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thalaiva thalaiva
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaiva thalaiva"/>
</div>
  <div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line" value="Superstar"/>
</div>
<div class="lyrico-lyrics-wrapper">Super star super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Super star super star"/>
</div>
<div class="lyrico-lyrics-wrapper">Style mannan super star
<input type="checkbox" class="lyrico-select-lyric-line" value="Style mannan super star"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haeiii..(10 Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Haeiii..(10 Times)"/></div>
</div>
</pre>