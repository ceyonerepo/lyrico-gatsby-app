---
title: "vaa saamy song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Arun Bharathi"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Vaa Saamy"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rOlL8YmGbE0"
type: "mass"
singers:
  - Mukesh Mohamed
  - Nochipatti Thirumoorthi
  - Keezhakarai Samsutheen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">satta eduthukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satta eduthukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti maduchukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti maduchukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiriya eruvena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiriya eruvena "/>
</div>
<div class="lyrico-lyrics-wrapper">eri eri erida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri eri erida"/>
</div>
<div class="lyrico-lyrics-wrapper">kannan karupirutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannan karupirutu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaduvai puliveratu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaduvai puliveratu"/>
</div>
<div class="lyrico-lyrics-wrapper">kayavarin kathaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayavarin kathaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudi mudi mudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudi mudi mudida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veecharuva konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veecharuva konda"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola samy"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthuruche paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthuruche paga"/>
</div>
<div class="lyrico-lyrics-wrapper">kola nadunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola nadunga "/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketavana athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketavana athu"/>
</div>
<div class="lyrico-lyrics-wrapper">poli poli poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poli poli poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjuduche avan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjuduche avan "/>
</div>
<div class="lyrico-lyrics-wrapper">thoda nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thimu thimu kuthiraigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimu thimu kuthiraigal"/>
</div>
<div class="lyrico-lyrics-wrapper">thimiri vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimiri vara"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vetaigal nadakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vetaigal nadakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">tharu tharu tharumathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharu tharu tharumathin"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaivanidam un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaivanidam un"/>
</div>
<div class="lyrico-lyrics-wrapper">aatangal mudigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatangal mudigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">meesaiya paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesaiya paru"/>
</div>
<div class="lyrico-lyrics-wrapper">keda keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keda keda"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugatha alika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugatha alika"/>
</div>
<div class="lyrico-lyrics-wrapper">varranada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varranada"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruthiya kudippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruthiya kudippan"/>
</div>
<div class="lyrico-lyrics-wrapper">moda moda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moda moda"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyavar kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyavar kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oodungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veecharuva konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veecharuva konda"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola samy"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthuruche paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthuruche paga"/>
</div>
<div class="lyrico-lyrics-wrapper">kola nadunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola nadunga "/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketavana athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketavana athu"/>
</div>
<div class="lyrico-lyrics-wrapper">poli poli poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poli poli poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjuduche avan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjuduche avan "/>
</div>
<div class="lyrico-lyrics-wrapper">thoda nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda nadunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vella kuthiraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella kuthiraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta thodangaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta thodangaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">narigalin narithanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narigalin narithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu odungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu odungum"/>
</div>
<div class="lyrico-lyrics-wrapper">venga thimirayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venga thimirayila"/>
</div>
<div class="lyrico-lyrics-wrapper">vegam edukaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam edukaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaivanin thodaigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaivanin thodaigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nadu nadu nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadu nadu nadungum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ukkirangal ondru pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ukkirangal ondru pada"/>
</div>
<div class="lyrico-lyrics-wrapper">uchi vaanam rendu pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi vaanam rendu pada"/>
</div>
<div class="lyrico-lyrics-wrapper">uruma katti oora kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruma katti oora kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaran vaaran vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaran vaaran vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">madura veeran mathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura veeran mathan"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">santhathiya kaathu nika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhathiya kaathu nika"/>
</div>
<div class="lyrico-lyrics-wrapper">sangadangal theethu vaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangadangal theethu vaika"/>
</div>
<div class="lyrico-lyrics-wrapper">paava kanaka theethu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paava kanaka theethu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaran vaaran vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaran vaaran vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">madura veeran mathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura veeran mathan"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nerupu kannil eriyuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerupu kannil eriyuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuthu nikka yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuthu nikka yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nerungi varun vinaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerungi varun vinaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam thavidu podiyaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam thavidu podiyaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meesaiya paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesaiya paru"/>
</div>
<div class="lyrico-lyrics-wrapper">keda keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keda keda"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugatha alika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugatha alika"/>
</div>
<div class="lyrico-lyrics-wrapper">varranada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varranada"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruthiya kudippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruthiya kudippan"/>
</div>
<div class="lyrico-lyrics-wrapper">moda moda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moda moda"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyavar kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyavar kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oodungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veecharuva konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veecharuva konda"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola samy"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthuruche paga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthuruche paga"/>
</div>
<div class="lyrico-lyrics-wrapper">kola nadunga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola nadunga "/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketavana athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketavana athu"/>
</div>
<div class="lyrico-lyrics-wrapper">poli poli poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poli poli poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanjuduche avan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanjuduche avan "/>
</div>
<div class="lyrico-lyrics-wrapper">thoda nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy vaa samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi adi idiyena thara therika 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi adi idiyena thara therika "/>
</div>
<div class="lyrico-lyrics-wrapper">oru thandavam nadakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thandavam nadakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pida pida pidariyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pida pida pidariyai"/>
</div>
<div class="lyrico-lyrics-wrapper">iluthu vara perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iluthu vara perum"/>
</div>
<div class="lyrico-lyrics-wrapper">pralayam vedikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pralayam vedikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">salangaigal aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salangaigal aaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sada sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sada sada"/>
</div>
<div class="lyrico-lyrics-wrapper">sadaiyinil irukida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadaiyinil irukida"/>
</div>
<div class="lyrico-lyrics-wrapper">varanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanada"/>
</div>
<div class="lyrico-lyrics-wrapper">udukaiyin oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udukaiyin oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidaa vidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidaa vidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">urumida oongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urumida oongi"/>
</div>
<div class="lyrico-lyrics-wrapper">adinga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adinga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">meesaiya paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesaiya paru"/>
</div>
<div class="lyrico-lyrics-wrapper">keda keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keda keda"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugatha alika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugatha alika"/>
</div>
<div class="lyrico-lyrics-wrapper">varranada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varranada"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruthiya kudippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruthiya kudippan"/>
</div>
<div class="lyrico-lyrics-wrapper">moda moda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moda moda"/>
</div>
<div class="lyrico-lyrics-wrapper">kodiyavar kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodiyavar kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oodungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodungada"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa samy"/>
</div>
</pre>
