---
title: "maanam pochi song song lyrics"
album: "Iruttu Araiyil Murattu Kuththu"
artist: "Balamurali Balu"
lyricist: "Gaana Kadal - Santhosh P - Jayakumar - Ku. Karthik"
director: "Uday yadav"
path: "/albums/iruttu-araiyil-murattu-kuththu-song-lyrics"
song: "Maanam Pochi"
image: ../../images/albumart/iruttu-araiyil-murattu-kuththu.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xDBDbNfzgPI"
type: "gaana"
singers:
  - Teejay
  - Gaana Kadal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machi etha pathi paadanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi etha pathi paadanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sollavae illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sollavae illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bar ah pathi paadra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar ah pathi paadra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi enakkoru quateru sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi enakkoru quateru sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudichathum kattanum billu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudichathum kattanum billu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna sernthu thattu nee cheers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna sernthu thattu nee cheers"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam parantha sorgam than bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam parantha sorgam than bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi maathamae illa barukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi maathamae illa barukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kudikathavana kaatu oorukulla hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kudikathavana kaatu oorukulla hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi enakkoru quateru sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi enakkoru quateru sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudichathum kattanum billu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudichathum kattanum billu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna sernthu thattu nee cheers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna sernthu thattu nee cheers"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam parantha sorgam than bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam parantha sorgam than bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp facebook twitter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp facebook twitter"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa login pannalum figure-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa login pannalum figure-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Flirting dating matteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flirting dating matteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu pannalum enna paththi rumoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu pannalum enna paththi rumoru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta pera pattam aaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta pera pattam aaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi vittom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi vittom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa kelviyala innum wife
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa kelviyala innum wife"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda kedaikavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda kedaikavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei bar-ah pathi paada sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei bar-ah pathi paada sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sontha kadhaiya paaditu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sontha kadhaiya paaditu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry machi ippo paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry machi ippo paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanda kandupudichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanda kandupudichan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuku yethi thirinjom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuku yethi thirinjom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandai kirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandai kirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanam pochi mariyathai pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam pochi mariyathai pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakka pottu ezharai aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakka pottu ezharai aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Areayavanda perellam naari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Areayavanda perellam naari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarandaiyum kettukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarandaiyum kettukiren"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so sorry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi enakkoru quateru sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi enakkoru quateru sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudichathum kattanum billu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudichathum kattanum billu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna sernthu thattu nee cheers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna sernthu thattu nee cheers"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam parantha sorgam than bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam parantha sorgam than bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi maathamae illa barukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi maathamae illa barukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kudikathavana kaatu oorukulla hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kudikathavana kaatu oorukulla hoi"/>
</div>
</pre>
