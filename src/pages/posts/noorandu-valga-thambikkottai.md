---
title: "noorandu valga song lyrics"
album: "Thambikkottai"
artist: "D. Imman"
lyricist: "Viveka"
director: "R.Rahesh"
path: "/albums/thambikkottai-lyrics"
song: "Noorandu Valga"
image: ../../images/albumart/thambikkottai.jpg
date: 2011-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WHxpF_wPNZU"
type: "sentiment"
singers:
  - Naresh Iyer
  - Anitha Karthikeyan
  - Nguyen Xuan Phuc
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkendru paadhai kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru paadhai kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pidiththu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pidiththu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaivittu uyirvaazha chonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaivittu uyirvaazha chonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkooda adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkooda adhigam"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkondru paadhai kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkondru paadhai kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pidiththu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pidiththu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vittu uyirvaazha chonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vittu uyirvaazha chonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkooda adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkooda adhigam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin saayal selaikkoayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin saayal selaikkoayil"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu madiyil enadhu aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu madiyil enadhu aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin saayal selaikkoayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin saayal selaikkoayil"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu madiyil enadhu aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu madiyil enadhu aayul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkendru paadhai kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru paadhai kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pidiththu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pidiththu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaivittu uyirvaazha chonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaivittu uyirvaazha chonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkooda adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkooda adhigam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un echchil paal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un echchil paal thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">en udalil raththamaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en udalil raththamaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathil undhan anbey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathil undhan anbey "/>
</div>
<div class="lyrico-lyrics-wrapper">kalappillaa suththamaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalappillaa suththamaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pizhaigal en kuraigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pizhaigal en kuraigal"/>
</div>
<div class="lyrico-lyrics-wrapper">en thalaigal en valigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thalaigal en valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">neethaaney theerththuvaippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethaaney theerththuvaippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiyandri dheivam yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiyandri dheivam yedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavaai en ninaivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavaai en ninaivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyiraai en unarvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyiraai en unarvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam pookkum mazhalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam pookkum mazhalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhikkaattum dheebachudarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhikkaattum dheebachudarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkendru paadhai kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru paadhai kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pidiththu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pidiththu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaivittu uyirvaazha chonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaivittu uyirvaazha chonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkooda adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkooda adhigam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmmoodi paarththaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmmoodi paarththaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">therigindra uruvamallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therigindra uruvamallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en veettil enakkaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veettil enakkaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">pirandhitta thoazhi allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandhitta thoazhi allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en guruvaai en thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en guruvaai en thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en nizhalaai en nijamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nizhalaai en nijamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhupoala bandhammillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhupoala bandhammillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">neye en anbin ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neye en anbin ellai"/>
</div>
<div class="lyrico-lyrics-wrapper">en arivu en thelivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en arivu en thelivu"/>
</div>
<div class="lyrico-lyrics-wrapper">en thiramai en pudhumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thiramai en pudhumai"/>
</div>
<div class="lyrico-lyrics-wrapper">unai cholla vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai cholla vaarthai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkaai nee vaazhndhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkaai nee vaazhndhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraandu vaazhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraandu vaazhga "/>
</div>
<div class="lyrico-lyrics-wrapper">nalamaaga vaazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamaaga vaazhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkendru paadhai kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru paadhai kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un viral pidiththu payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viral pidiththu payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaivittu uyirvaazha chonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaivittu uyirvaazha chonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodikkooda adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodikkooda adhigam"/>
</div>
</pre>
