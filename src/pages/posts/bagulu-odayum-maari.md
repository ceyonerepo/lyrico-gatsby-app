---
title: 'bagulu odayum dagulu maari song lyrics'
album: 'Maari'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari-song-lyrics'
song: 'Bagulu odayum dagulu maari'
image: ../../images/albumart/Maari.jpg
date: 2015-07-17
youtubeLink: 'https://www.youtube.com/embed/T2V5VWdebBc'
type: 'mass'
lang: tamil
singers: 
- Dhanush
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Bagulu odaiyum dagulu mari
<input type="checkbox" class="lyrico-select-lyric-line" value="Bagulu odaiyum dagulu mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye chikkaangudthaa porada maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye chikkaangudthaa porada maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye vandhaan da romba thaeri..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye vandhaan da romba thaeri.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ai guiltaanaan da enga maari…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ai guiltaanaan da enga maari…"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga maari enga maari enga maari….
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga maari enga maari enga maari…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye paatheenna moracha maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye paatheenna moracha maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonji aayidum vera maari.
<input type="checkbox" class="lyrico-select-lyric-line" value="Moonji aayidum vera maari."/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya vuttaa dhaulath maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaaya vuttaa dhaulath maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhka unakku mudinjamaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhka unakku mudinjamaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pinnadichaa etchcha maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinnadichaa etchcha maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettu vilum andhamaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettu vilum andhamaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy summ veramaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy summ veramaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesamaattaan nallamaari….
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesamaattaan nallamaari…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andhamaari…
<input type="checkbox" class="lyrico-select-lyric-line" value="Andhamaari…"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha andha andha maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha andha andha maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bagulu odaiyum dagulu mari
<input type="checkbox" class="lyrico-select-lyric-line" value="Bagulu odaiyum dagulu mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye chikkaangudthaa porada maari
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye chikkaangudthaa porada maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye vandhaan da romba thaeri..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye vandhaan da romba thaeri.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ai guiltaanaan da enga maari…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ai guiltaanaan da enga maari…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andhamaari..
<input type="checkbox" class="lyrico-select-lyric-line" value="Andhamaari.."/>
</div>
</pre>