---
title: "enga aatam song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Pa. Vijay"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Enga Aatam"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RvWziWtxxrs"
type: "happy"
singers:
  - Nivas K. Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaththi Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku Kaadhu Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Kaadhu Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thaan Enga Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaan Enga Gethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku Kaadhu Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Kaadhu Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thaan Enga Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaan Enga Gethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thola Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thola Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikka Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikka Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruththu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruththu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Senja Atha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Senja Atha Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Vaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkalaiya Thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkalaiya Thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Vaiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooravali Atha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooravali Atha Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatti Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatti Vaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorathenga Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorathenga Onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Vaiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesavittu Polanthu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesavittu Polanthu Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puluthi Veesa Roundu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puluthi Veesa Roundu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engattam Ithu Kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engattam Ithu Kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Kuththaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kuththaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Veriyaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Veriyaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engattam Ithu Kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engattam Ithu Kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Kuththaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kuththaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Devaraattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Devaraattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadatha Aattaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadatha Aattaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Thaan Paappoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Thaan Paappoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Podatha Nottaththa Pottu Nippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podatha Nottaththa Pottu Nippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththoda Pathinonna Aththoda Athilonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththoda Pathinonna Aththoda Athilonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Illaama Vazhndhu Nippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Illaama Vazhndhu Nippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seratha Thikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seratha Thikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Thaan Theerpoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Thaan Theerpoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaratha Oora Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaratha Oora Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaththi Veppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaththi Veppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannathi Mannan Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannathi Mannan Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan Pol Vazhndhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannan Pol Vazhndhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappelaam Thaniyaala Thatti Veppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappelaam Thaniyaala Thatti Veppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku Kaadhu Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Kaadhu Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thaan Enga Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaan Enga Gethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku Kaadhu Kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Kaadhu Kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thaan Enga Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaan Enga Gethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thola Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thola Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikka Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikka Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruththu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruththu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeravel Vetrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeravel Vetrivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeravel Vetrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeravel Vetrivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeravel Vetrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeravel Vetrivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeravel Vetrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeravel Vetrivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenju Kootta Nimiththi Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Kootta Nimiththi Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjuraththa Kaatti Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjuraththa Kaatti Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjagaththa Thaakki Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjagaththa Thaakki Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagutheduthu Kaaval Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagutheduthu Kaaval Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravuthiratha Kaatti Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravuthiratha Kaatti Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranagalaththa Pookki Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranagalaththa Pookki Nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragalagaththi Needhi Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragalagaththi Needhi Vellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeravel Vetrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeravel Vetrivel"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Nee Mundhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Nee Mundhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi Nilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi Nilladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerama Paasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerama Paasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhrogama Nesama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhrogama Nesama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Nee Paathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Nee Paathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam Solladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam Solladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjama Nenjama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjama Nenjama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovamum Theeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovamum Theeruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiya Mattu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya Mattu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottamma Kattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottamma Kattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhuvom Koottama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhuvom Koottama"/>
</div>
<div class="lyrico-lyrics-wrapper">Erikkira Theeya Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erikkira Theeya Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirndhu Nilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu Nilladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Koothaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Koothaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vanthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vanthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Anbodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Anbodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kondaadu Koothaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kondaadu Koothaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaadu Anbodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaadu Anbodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Kondu Vettaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Kondu Vettaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattan Poottan Perumaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattan Poottan Perumaiyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivel Veeravel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivel Veeravel"/>
</div>
</pre>
