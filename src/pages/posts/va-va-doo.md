---
title: "va va song lyrics"
album: "Doo"
artist: "Abhishek — Lawrence"
lyricist: "Yugabharathi"
director: "Sriram Padmanabhan"
path: "/albums/doo-lyrics"
song: "Va Va"
image: ../../images/albumart/doo.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EuzZKjt0UiY"
type: "love"
singers:
  - Reshmonu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa vaa vaa hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa vaa hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikkulley kaadhal neechal poadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkulley kaadhal neechal poadey"/>
</div>
<div class="lyrico-lyrics-wrapper">thithikkudhey boomi boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithikkudhey boomi boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley aasai koochal poada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley aasai koochal poada"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiven saami saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiven saami saami"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai pagalai koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai pagalai koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai nilavai koduppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai nilavai koduppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaney edhaiyum koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaney edhaiyum koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">pirivu illai vaaippai meettu koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivu illai vaaippai meettu koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa yeyei vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa yeyei vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalloori poongaavil kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalloori poongaavil kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Computer chettingil kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer chettingil kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell Phone-il ring tone-il kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phone-il ring tone-il kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">oorengumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorengumey"/>
</div>
</pre>
