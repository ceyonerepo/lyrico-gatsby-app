---
title: "vennira song lyrics"
album: "Nadodi Kanavu"
artist: "Sabesh Murali"
lyricist: "Sirgazhi Sirpi"
director: "Veera Selva"
path: "/albums/nadodi-kanavu-lyrics"
song: "Vennira"
image: ../../images/albumart/nadodi-kanavu.jpg
date: 2018-08-03
lang: tamil
youtubeLink: 
type: "sad"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">venneera oothi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venneera oothi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">veroda saachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veroda saachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu pona pinnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pona pinnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paaram sumakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paaram sumakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha katupatta meera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha katupatta meera"/>
</div>
<div class="lyrico-lyrics-wrapper">oru valiyum illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru valiyum illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">mannoda vaasatha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannoda vaasatha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu thaan povene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu thaan povene"/>
</div>
<div class="lyrico-lyrics-wrapper">ungaloda nizhalaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ungaloda nizhalaga "/>
</div>
<div class="lyrico-lyrics-wrapper">epothum irupene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum irupene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venneera oothi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venneera oothi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">veroda saachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veroda saachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu pona pinnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pona pinnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paaram sumakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paaram sumakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha katupatta meera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha katupatta meera"/>
</div>
<div class="lyrico-lyrics-wrapper">oru valiyum illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru valiyum illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ungaloda nizhalaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ungaloda nizhalaga "/>
</div>
<div class="lyrico-lyrics-wrapper">epothum irupene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum irupene"/>
</div>
<div class="lyrico-lyrics-wrapper">venneera oothi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venneera oothi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">veroda saachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veroda saachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu pona pinnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu pona pinnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paaram sumakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paaram sumakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha katupatta meera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha katupatta meera"/>
</div>
<div class="lyrico-lyrics-wrapper">oru valiyum illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru valiyum illaiya"/>
</div>
</pre>
