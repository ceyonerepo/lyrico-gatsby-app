---
title: "nanbanukku kovila kattu song lyrics"
album: "Kanchana3-Muni4"
artist: "S. Thaman"
lyricist: "Madhan Karky"
director: "Raghava Lawrence"
path: "/albums/kanchana3-muni4-lyrics"
song: "Nanbanukku Kovila Kattu"
image: ../../images/albumart/kanchana3-muni4.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2G1fpuzpLKI"
type: "Friendship"
singers:
  - Saravedi Saran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Nanbanukku Kovilakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Nanbanukku Kovilakattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pova Maataan Unna Thaan Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pova Maataan Unna Thaan Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillova Irukkum Friendoda Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillova Irukkum Friendoda Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpu Thaney Ennoda Moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpu Thaney Ennoda Moochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Maccha Valallam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maccha Valallam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoni Raja Goli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoni Raja Goli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Renduperum Onna Irundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renduperum Onna Irundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhanaalum Jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhanaalum Jolly"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Onnuinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Onnuinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Thaandaa Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Thaandaa Thudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpunnuu Sollipaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpunnuu Sollipaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulukku Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulukku Pudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Nanbanukku Kovilakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Nanbanukku Kovilakattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pova Maataan Unna Thaan Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pova Maataan Unna Thaan Vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillova Irukkum Friendoda Pechi Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillova Irukkum Friendoda Pechi Aachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpu Thaney Ennoda Moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpu Thaney Ennoda Moochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban Potta Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban Potta Soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Naalum Thinben Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naalum Thinben Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Naaley Gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Naaley Gethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Nadaiye Sothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Nadaiye Sothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vaalzhaveipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vaalzhaveipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholzh Kuduppaan Machaan Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholzh Kuduppaan Machaan Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatu Vandi Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Vandi Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Ukkaruvom Keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Ukkaruvom Keela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sikkal Naaka Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sikkal Naaka Onna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai City Fullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai City Fullah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sutthuvomey Dhillah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sutthuvomey Dhillah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kadala Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kadala Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaandi Poyiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Poyiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Figurelaam Kaasa Karaikkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figurelaam Kaasa Karaikkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa Natpuyinna Kadaisivaraikkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Natpuyinna Kadaisivaraikkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban Vandhaa Kadhavu Thorakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban Vandhaa Kadhavu Thorakkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambala Thotta Soda Bottleu Parakkum Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambala Thotta Soda Bottleu Parakkum Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei Goldu Chain Orasi Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei Goldu Chain Orasi Paatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu Thenjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Thenjidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Mela Kaiyi Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Mela Kaiyi Pattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhu Keenjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Keenjidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aei Goldu Chain Orasi Paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aei Goldu Chain Orasi Paathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu Thenjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Thenjidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Mela Kaiyi Pattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Mela Kaiyi Pattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mooku Keenjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku Keenjidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Nanbanukku Kovilakattu Kattu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Nanbanukku Kovilakattu Kattu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pova Maataan Unna Thaan Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pova Maataan Unna Thaan Vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillova Irukkum Friendoda Pechi Achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillova Irukkum Friendoda Pechi Achi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpu Thaney Ennoda Moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpu Thaney Ennoda Moochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vediya Kaalaila Ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vediya Kaalaila Ezhundhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Theatre Ulla Poondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Theatre Ulla Poondhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Padattha Paapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Padattha Paapaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Firstu Naalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firstu Naalula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanga Ammavaanda Solluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga Ammavaanda Solluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanga Appavaanda Solluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga Appavaanda Solluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Annana Minja Inimey Aalu Illey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Annana Minja Inimey Aalu Illey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cut Outtukku Maalai Poduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cut Outtukku Maalai Poduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Adikku Danceu Aaduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Adikku Danceu Aaduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasatathellam Avan Oram Thalluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasatathellam Avan Oram Thalluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padattha Patthi Veliya Getha Solluvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padattha Patthi Veliya Getha Solluvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Famous Aava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Famous Aava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Theruvaa Poster Ottuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Theruvaa Poster Ottuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Dance Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dance Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theatre Ulla Kaiya Thattuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theatre Ulla Kaiya Thattuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Kastampatta Kaasu Kudutthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kastampatta Kaasu Kudutthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ticketu Vaanguvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ticketu Vaanguvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padam Nooru Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam Nooru Naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odanum Nu Saamiya Venduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odanum Nu Saamiya Venduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Nanbanukku Kovilakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Nanbanukku Kovilakattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah Kattu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Kattu Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pova Maataan Unna Thaann Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pova Maataan Unna Thaann Vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillova Irukkum Friendoda Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillova Irukkum Friendoda Pechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natpu Thaney Ennoda Moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natpu Thaney Ennoda Moochi"/>
</div>
</pre>
