---
title: "vaa un neram vandhadhae song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Vaa Un Neram Vandhadhae"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uJx6WzgBTs4"
type: "happy"
singers:
  - Premgi Amaren
  - Sakthi Amaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">va un neram vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va un neram vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnaaram thandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnaaram thandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi maaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi maaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyoodu kannigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyoodu kannigai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidaadhu paadidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidaadhu paadidu"/>
</div>
<div class="lyrico-lyrics-wrapper">varaadhu minthadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaadhu minthadai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ketta vazhkai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ketta vazhkai than"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum jolly thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum jolly thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irukkum varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkum varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">therikka vidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikka vidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">anubhavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubhavi"/>
</div>
<div class="lyrico-lyrics-wrapper">siragai virithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragai virithu"/>
</div>
<div class="lyrico-lyrics-wrapper">paraka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paraka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan veli"/>
</div>
<div class="lyrico-lyrics-wrapper">pirandha pirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandha pirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum varuvathilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum varuvathilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ketta vilaiyatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ketta vilaiyatta"/>
</div>
<div class="lyrico-lyrics-wrapper">pala notta ketti melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala notta ketti melam"/>
</div>
<div class="lyrico-lyrics-wrapper">pola kottumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola kottumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">va un neram vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="va un neram vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnaaram thandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnaaram thandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi maaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi maaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyoodu kannigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyoodu kannigai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidaadhu paadidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidaadhu paadidu"/>
</div>
<div class="lyrico-lyrics-wrapper">varaadhu minthadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaadhu minthadai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ketta vazhkai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ketta vazhkai than"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum jolly thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum jolly thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thei vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thei vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai koodi vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai koodi vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en vazhkai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vazhkai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhaala povadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhaala povadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un vazhkai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vazhkai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vazha mattum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vazha mattum than"/>
</div>
<div class="lyrico-lyrics-wrapper">nee selvam serthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee selvam serthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee selavu seiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee selavu seiyathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thedi serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pootu potu vaithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pootu potu vaithadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai thirandheduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai thirandheduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">selavu seidhal nallathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selavu seidhal nallathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazha thane selvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazha thane selvame"/>
</div>
<div class="lyrico-lyrics-wrapper">nee podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee podu"/>
</div>
<div class="lyrico-lyrics-wrapper">podhu rootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhu rootu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasu thaenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasu thaenju"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum munnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan thei vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thei vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai koodi vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai koodi vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en vazhkai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vazhkai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhaala povadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhaala povadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un vazhkai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vazhkai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vazha mattum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vazha mattum than"/>
</div>
<div class="lyrico-lyrics-wrapper">nee selvam serthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee selvam serthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee selavu seiyathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee selavu seiyathan"/>
</div>
</pre>
