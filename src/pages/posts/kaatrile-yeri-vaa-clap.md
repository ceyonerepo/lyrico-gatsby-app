---
title: "kaatrile yeri vaa song lyrics"
album: "Clap"
artist: "Ilaiyaraaja"
lyricist: "Pa Vijay"
director: "Prithivi Adithya"
path: "/albums/clap-lyrics"
song: "Kaatrile Yeri Vaa"
image: ../../images/albumart/clap.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DaXwnUB8bZI"
type: "happy"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaatrile yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrile yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thikettum vetri kodi yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikettum vetri kodi yetru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiye porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiye porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poridu nee jeyithu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poridu nee jeyithu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri enum netri pottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri enum netri pottai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam unaku vaikatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam unaku vaikatum"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri tholvi rendum thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri tholvi rendum thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">nirka vendum nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirka vendum nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">neye neye neye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neye neye neye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathanaigal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathanaigal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">sothanaikku pinnal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothanaikku pinnal than"/>
</div>
<div class="lyrico-lyrics-wrapper">vethanaigal thangi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanaigal thangi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri un munnal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri un munnal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">koppai yenthum unthan kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koppai yenthum unthan kai"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum nenjil nambikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum nenjil nambikai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi nindraal odidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi nindraal odidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil ellam vaadikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil ellam vaadikkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrile yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrile yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thikettum vetri kodi yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikettum vetri kodi yetru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiye porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiye porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poridu nee jeyithu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poridu nee jeyithu kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrile yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrile yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thikettum vetri kodi yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikettum vetri kodi yetru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnil oru vegam puthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil oru vegam puthu "/>
</div>
<div class="lyrico-lyrics-wrapper">vegam piranthal piranthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam piranthal piranthal"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thadai seiyum malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thadai seiyum malai"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam podi aagatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam podi aagatho"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil thidamaana valimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil thidamaana valimai"/>
</div>
<div class="lyrico-lyrics-wrapper">than irunthal irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than irunthal irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">indro maru naale magudangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indro maru naale magudangal"/>
</div>
<div class="lyrico-lyrics-wrapper">unai seraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai seraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri ingu oru naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri ingu oru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">oivedukka pogathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oivedukka pogathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamaiku inge ethu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamaiku inge ethu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu enbathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu enbathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ondrum kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrum kidaiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrile yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrile yeri vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thikettum vetri kodi yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikettum vetri kodi yetru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiye porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiye porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poridu nee jeyithu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poridu nee jeyithu kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri enum netri pottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri enum netri pottai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam unaku vaikatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam unaku vaikatum"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri tholvi rendum thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri tholvi rendum thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">nirka vendum nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirka vendum nee nee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">neye neye neye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neye neye neye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathanaigal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathanaigal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">sothanaikku pinnal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothanaikku pinnal than"/>
</div>
<div class="lyrico-lyrics-wrapper">vethanaigal thangi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanaigal thangi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri un munnal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri un munnal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">koppai yenthum unthan kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koppai yenthum unthan kai"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum nenjil nambikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum nenjil nambikai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi nindraal odidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi nindraal odidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil ellam vaadikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil ellam vaadikkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathanaigal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathanaigal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">sothanaikku pinnal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothanaikku pinnal than"/>
</div>
<div class="lyrico-lyrics-wrapper">vethanaigal thangi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanaigal thangi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri un munnal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri un munnal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">koppai yenthum unthan kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koppai yenthum unthan kai"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum nenjil nambikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum nenjil nambikai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi nindraal odidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi nindraal odidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil ellam vaadikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil ellam vaadikkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrile yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrile yeri vaa"/>
</div>
</pre>
