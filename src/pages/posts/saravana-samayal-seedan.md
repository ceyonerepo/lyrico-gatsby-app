---
title: "saravana samayal song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Pa. Vijay"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Saravana Samayal"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DxmJXefORrY"
type: "happy"
singers:
  - Dhanush
  - Hariharan
  - Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Om rudra nethraya vidhmagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om rudra nethraya vidhmagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakthi hasthaaya dheemagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakthi hasthaaya dheemagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanno agni prajodhayathh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanno agni prajodhayathh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaa aaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ma ga ma pa ne dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ma ga ma pa ne dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ni dha ni sa ma ni dha ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ni dha ni sa ma ni dha ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ni dha ni sa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ni dha ni sa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru kaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru kaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusigal maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusigal maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru rusiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru rusiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyum aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyum aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayal aaruvagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal aaruvagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ni dha pa ma ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ni dha pa ma ga ri sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhilum undu kannukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhilum undu kannukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neruppai kurachu vanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neruppai kurachu vanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruppu vendhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruppu vendhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam vandhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam vandhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam serthu erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam serthu erakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahaa indha masaalaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahaa indha masaalaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Mixiyila araikaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mixiyila araikaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam ammiyila arachittu vaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam ammiyila arachittu vaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththannae konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththannae konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengaavam odaichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengaavam odaichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuruvittu vaangalen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuruvittu vaangalen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiya kaaikariya narukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiya kaaikariya narukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuvakkoodaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuvakkoodaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuvi narukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuvi narukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaah aaaah aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaah aaaah aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaa seiyum samayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaa seiyum samayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai ootti valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai ootti valarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatti seiyum samayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatti seiyum samayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthuvam irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthuvam irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayai ninaivoottinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai ninaivoottinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangaiyoda samayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangaiyoda samayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhuchuvai koodinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhuchuvai koodinal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anniyoda samayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyoda samayal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manaivi samaippadhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaivi samaippadhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoazhi samaiyal thappum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoazhi samaiyal thappum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidiththu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiththu irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru suvaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru suvaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru uravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru uravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruthuvam irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruthuvam irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Magathuva unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magathuva unavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira thira naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thira thira naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendakkaa poriyalla konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendakkaa poriyalla konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tthayir vittu erakkunna vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tthayir vittu erakkunna vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhavazhappaana vendakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhavazhappaana vendakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Morumorunnu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morumorunnu irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban veettu unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban veettu unavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyaaga irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyaaga irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaipodum unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaipodum unavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamaicholli kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaicholli kodukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetrathaazhvai pokkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetrathaazhvai pokkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Samappandhi unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samappandhi unavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaiyai natpaakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaiyai natpaakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiriveettu unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiriveettu unavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasam kodhikkiradhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasam kodhikkiradhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaadi erakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaadi erakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panbaattai unarththidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbaattai unarththidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhaakkaala unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaakkaala unavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam aayulaiyae koottumammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam aayulaiyae koottumammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann paanai unavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann paanai unavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keerai neram maaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keerai neram maaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkanumnnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkanumnnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjoondu vellaththa serthukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjoondu vellaththa serthukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru unavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru unavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniththani sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniththani sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayalil vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayalil vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththuvam irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththuvam irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira thira naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thira thira naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru kaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru kaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusigal maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusigal maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru rusiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru rusiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyum aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyum aarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayal aaruvagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal aaruvagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheeranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheeranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ni dha pa ma ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ni dha pa ma ga ri sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saravana samaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravana samaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu sarva yogini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu sarva yogini"/>
</div>
<div class="lyrico-lyrics-wrapper">Arusuvai tharuvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arusuvai tharuvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval anna poorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval anna poorani"/>
</div>
</pre>
