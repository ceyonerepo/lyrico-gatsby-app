---
title: "olu song lyrics"
album: "Maniyarayile Ashokan"
artist: "Sreehari K. Nair"
lyricist: "Shamzu Zayba"
director: "Shamzu Zayba"
path: "/albums/maniyarayile-ashokan-lyrics"
song: "Olu"
image: ../../images/albumart/maniyarayile-ashokan.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/RvpdHH_agAU"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothankiri kili kannullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothankiri kili kannullorolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissakalil paranjora chelullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissakalil paranjora chelullorolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothankiri kili kannullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothankiri kili kannullorolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissakalil paranjora chelullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissakalil paranjora chelullorolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olonnu peyyunna bahraya neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olonnu peyyunna bahraya neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhangalla kannil suruma parathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhangalla kannil suruma parathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirakal pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakal pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathoram chollunna porishayellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathoram chollunna porishayellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olenna penninte namangalayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olenna penninte namangalayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissakal pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissakal pole "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothankiri kili kannullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothankiri kili kannullorolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissakalil paranjora chelullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissakalil paranjora chelullorolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oloru chiriyale alamilake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oloru chiriyale alamilake"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravoli pakarumbol roohonnathudiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravoli pakarumbol roohonnathudiche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothankiri kili kannullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothankiri kili kannullorolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissakalil paranjora chelullorolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissakalil paranjora chelullorolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olu Olu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olu Olu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thurannu khalbu midichu munnilolund
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thurannu khalbu midichu munnilolund"/>
</div>
</pre>
