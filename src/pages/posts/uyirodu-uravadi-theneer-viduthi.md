---
title: "uyirodu uravadi song lyrics"
album: "Theneer Viduthi"
artist: "S.S. Kumaran"
lyricist: "Murugan Manthiram"
director: "S.S. Kumaran"
path: "/albums/theneer-viduthi-lyrics"
song: "Uyirodu Uravadi"
image: ../../images/albumart/theneer-viduthi.jpg
date: 2011-07-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4WVHBPwbbJc"
type: "sad"
singers:
  - S.S. Kumaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uyirodu uravaadi ulagam marandhupovom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu uravaadi ulagam marandhupovom"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhoadu enadhaagi karuvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhoadu enadhaagi karuvil"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaindhu tholaindhupoavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaindhu tholaindhupoavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyoadu nilamaaga nilaimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyoadu nilamaaga nilaimai"/>
</div>
<div class="lyrico-lyrics-wrapper">karaindhu nanaindhupoaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaindhu nanaindhupoaven"/>
</div>
<div class="lyrico-lyrics-wrapper">un penmaigal yengidey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un penmaigal yengidey"/>
</div>
<div class="lyrico-lyrics-wrapper">un aamaigal thoondidudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aamaigal thoondidudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nam karaigal udaigiradhey vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam karaigal udaigiradhey vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">tham tham thara tham thara they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tham tham thara tham thara they"/>
</div>
<div class="lyrico-lyrics-wrapper">tham tham thara tham thara they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tham tham thara tham thara they"/>
</div>
<div class="lyrico-lyrics-wrapper">tham tham thara tham thara they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tham tham thara tham thara they"/>
</div>
<div class="lyrico-lyrics-wrapper">tham tham thara tham thara they
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tham tham thara tham thara they"/>
</div>
</pre>
