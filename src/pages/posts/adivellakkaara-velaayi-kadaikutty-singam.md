---
title: "adivellakkaara velaayi song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Adivellakkaara Velaayi"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ay92dzwAHZc"
type: "love"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etha vechi en nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etha vechi en nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul irukkum idhayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul irukkum idhayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un vizhigal eetiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigal eetiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu uduruvi paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu uduruvi paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un muzhi enna saataiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un muzhi enna saataiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saalam poda vaikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saalam poda vaikuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellathil adichi izhupathu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellathil adichi izhupathu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhigalil izhuthu poorayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigalil izhuthu poorayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallathil vizhuntha en uyirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallathil vizhuntha en uyirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jillyena thookki poorayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jillyena thookki poorayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella manam ullavandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella manam ullavandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nattiyedi kaadhal sedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattiyedi kaadhal sedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthen paranthen paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthen paranthen paranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai kodu illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai kodu illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arinthen arinthen arinthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arinthen arinthen arinthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai unnal arinthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai unnal arinthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etha vechi en nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etha vechi en nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tholacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tholacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul irukkum idhayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul irukkum idhayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un vizhigal eetiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhigal eetiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu uduruvi paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu uduruvi paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un muzhi enna saataiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un muzhi enna saataiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saalam poda vaikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saalam poda vaikuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vellakkaara velaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vellakkaara velaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kollakaari aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kollakaari aanaayae"/>
</div>
</pre>
