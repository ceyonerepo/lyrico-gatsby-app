---
title: "nee kannu song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Shreemani - Raqueeb Alam"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Nee Kannu Neeli Samudram"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/zZl7vDDN8Ek"
type: "love"
singers:
  - Javed Ali
  - Sreekanth Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ishq shifaya, ishq shifaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq shifaya, ishq shifaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq parde mein kisi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq parde mein kisi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon mein labrez hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon mein labrez hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq shifaya mehboob ka saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq shifaya mehboob ka saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq malmal mein yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq malmal mein yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipta huwa tabrez hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipta huwa tabrez hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq hai peer payambar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai peer payambar"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq Ali dum mast kalandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq Ali dum mast kalandar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai peer payambar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai peer payambar"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq Ali dum mast kalandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq Ali dum mast kalandar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq kabhi qatra hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kabhi qatra hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq kabhi hai ek samandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq kabhi hai ek samandar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq kabhi qatra hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kabhi qatra hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq kabhi hai ek samandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq kabhi hai ek samandar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallanaina mungurule mungurule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallanaina mungurule mungurule"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaredho repayile repayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaredho repayile repayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu thappa nakinko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu thappa nakinko"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokanni lekunda kappayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokanni lekunda kappayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghalumante nee gajule, nee gajule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghalumante nee gajule, nee gajule"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallumande naa praname, na praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallumande naa praname, na praname"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukundi vaana jallulaga preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukundi vaana jallulaga preme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni isuka gudu kattina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni isuka gudu kattina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee peru raasi pettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee peru raasi pettina"/>
</div>
<div class="lyrico-lyrics-wrapper">Danni cheripeti keratalu puttaledu telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danni cheripeti keratalu puttaledu telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa goruvanka pakkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa goruvanka pakkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama chiluka entha chakkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama chiluka entha chakkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakante chakkananta nuvvunte na pakana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakante chakkananta nuvvunte na pakana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appu adigane kotha kotha matalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appu adigane kotha kotha matalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappukunaye bhumi paina bashalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappukunaye bhumi paina bashalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalemanna yeh aksharalo premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalemanna yeh aksharalo premani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannu neeli samudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannu neeli samudram"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasemo andhutlo padava prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasemo andhutlo padava prayanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee navvu muthyala haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu muthyala haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu theeraniki lageti daaram daaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu theeraniki lageti daaram daaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee andhamantha uppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee andhamantha uppena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu munchinadhi chapuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu munchinadhi chapuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha munchesina thele banthini nenenana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha munchesina thele banthini nenenana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttu entha chappudochina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu entha chappudochina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee savadedho cheppadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee savadedho cheppadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha dachesina ninnu jalladesi pattna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha dachesina ninnu jalladesi pattna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ugale upirayina pichhodini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ugale upirayina pichhodini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee upire pranamayina pilladini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee upire pranamayina pilladini"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee prema valalo chikkukuna chepani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee prema valalo chikkukuna chepani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq hai peer payambar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai peer payambar"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq Ali dum mast kalandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq Ali dum mast kalandar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq hai peer payambar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq hai peer payambar"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq Ali dum mast kalandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq Ali dum mast kalandar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq kabhi qatra hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kabhi qatra hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq kabhi hai ek samandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq kabhi hai ek samandar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq kabhi qatra hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq kabhi qatra hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ishq kabhi hai ek samandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ishq kabhi hai ek samandar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq shifaya, ishq shifaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq shifaya, ishq shifaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq parde mein kisi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq parde mein kisi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon mein labrez hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon mein labrez hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq shifaya mehboob ka saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq shifaya mehboob ka saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq malmal mein yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq malmal mein yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipta huwa tabrez hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipta huwa tabrez hai"/>
</div>
</pre>
