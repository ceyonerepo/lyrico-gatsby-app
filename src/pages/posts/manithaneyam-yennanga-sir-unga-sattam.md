---
title: "manithaneyam song lyrics"
album: "Yennanga Sir Unga Sattam"
artist: "Guna Balasubramanian"
lyricist: "Karthik Netha"
director: "Prabhu Jeyaram"
path: "/albums/yennanga-sir-unga-sattam-lyrics"
song: "Manithaneyam"
image: ../../images/albumart/yennanga-sir-unga-sattam.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mqPqvwjzL3E"
type: "sad"
singers:
  - Padmapriya Raghavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manithaneyam manithaneyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithaneyam manithaneyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi ponathenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi ponathenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirugam koodi izhukkum theril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirugam koodi izhukkum theril"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan povathenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan povathenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madalgal yaavum yeriyum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalgal yaavum yeriyum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean eduppathu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean eduppathu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangal yaavum katharum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangal yaavum katharum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sirippathu ange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sirippathu ange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithaneyam manithaneyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithaneyam manithaneyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi ponathenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi ponathenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Mirugam koodi izhukkum theril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirugam koodi izhukkum theril"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan povathenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan povathenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhaiyai piranthu vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaiyai piranthu vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhavu paadu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhavu paadu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaiyai azhugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyai azhugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruma paadu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruma paadu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarai kelvi ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai kelvi ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada yaara thedi paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada yaara thedi paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi bodhai yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi bodhai yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada nidhi thevan thoonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada nidhi thevan thoonga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eenam thaandi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenam thaandi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamana meeri poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamana meeri poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhai yengu poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhai yengu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaalil kallum noga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaalil kallum noga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aramellam theruvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aramellam theruvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Agathiyai thiriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agathiyai thiriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Murangale arangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murangale arangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamellam niraiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamellam niraiyuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar saadhi uyanthendre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar saadhi uyanthendre"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanadu nadukkithinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanadu nadukkithinge"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani manithanin azhukuralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani manithanin azhukuralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril theernthu viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril theernthu viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasi vazhangum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasi vazhangum saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnetha vazhiya kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnetha vazhiya kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhikulla saadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhikulla saadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum vaayil thaane needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum vaayil thaane needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varga bedham paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varga bedham paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada varuna bedham paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada varuna bedham paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaporai meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaporai meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ketpathenga sedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ketpathenga sedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirellam iranthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirellam iranthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vazhi kidaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhi kidaikkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaiyai maruthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaiyai maruthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Samuthuvam irukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samuthuvam irukkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pooradi sazhikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooradi sazhikkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor koodi azhugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor koodi azhugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganam porunthiya jananayagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganam porunthiya jananayagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam neengi yezhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam neengi yezhuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayaathi nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayaathi nyaayaamaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatho nyaayaamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatho nyaayaamaare"/>
</div>
</pre>
