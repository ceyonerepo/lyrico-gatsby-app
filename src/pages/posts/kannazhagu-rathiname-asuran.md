---
title: 'kannazhagu rathiname song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Yugabharathi'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Kannazhagu Rathiname'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
lang: tamil
singers: 
- Dhanush
youtubeLink: "https://www.youtube.com/embed/MoJyH2ZH3-g"
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kannazhagu rathinamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannazhagu rathinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai asaiyum porchilaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai asaiyum porchilaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanalaiyae kannae unna kaanalaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanalaiyae kannae unna kaanalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyum kaalum unna enni odalaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyum kaalum unna enni odalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ponnazhagu pettagamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnazhagu pettagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo mudinja kattadamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo mudinja kattadamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanalaiyae thangam unna kaanalaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanalaiyae thangam unna kaanalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum maadum unna enni meiyalaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadum maadum unna enni meiyalaiyae"/>
</div>
</pre>