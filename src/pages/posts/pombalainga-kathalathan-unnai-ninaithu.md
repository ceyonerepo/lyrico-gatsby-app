---
title: "pombalainga kaathalathan song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "P. Vijay"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Pombalainga Kaathalathan"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8uYQuJ5CLjE"
type: "sad"
singers:
  - Manikka Vinayagam
  - P. Unnikrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pombalainga kaadhala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga kaadhala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae nambividadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiyathaal nonthu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiyathaal nonthu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vembividadhae vembividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vembividadhae vembividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaannu solli irupa aasaiya kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaannu solli irupa aasaiya kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanu solli nadapa aalayum maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanu solli nadapa aalayum maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambalai ellam ahimsa party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambalai ellam ahimsa party"/>
</div>
<div class="lyrico-lyrics-wrapper">Pombalai ellam theeviravaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalai ellam theeviravaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalainga kaadhala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga kaadhala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae nambividadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiyathaal nonthu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiyathaal nonthu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vembividadhae vembividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vembividadhae vembividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennalae paithiyama ponavan undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennalae paithiyama ponavan undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu aangalalae paithiyama aanaval unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu aangalalae paithiyama aanaval unda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennalae kaavi katti nadanthavan undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennalae kaavi katti nadanthavan undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu aangalaalae kaavi katti nadanthaval unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu aangalaalae kaavi katti nadanthaval unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennuku tajmahal katti vechanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku tajmahal katti vechanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Evalaachum oru sengal nattu vechaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalaachum oru sengal nattu vechaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae ponna nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae ponna nambividadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae ponna nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae ponna nambividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalainga kaadhala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga kaadhala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae nambividadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiyathaal nonthu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiyathaal nonthu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vembividadhae vembividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vembividadhae vembividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen ellam paritchayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen ellam paritchayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal idam thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal idam thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pasangala thaan engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma pasangala thaan engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanga padika vitaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga padika vitaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen ellam thanga medal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen ellam thanga medal"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichu vanthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichu vanthaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma paiyan mugathil thaadiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma paiyan mugathil thaadiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Molaika vechaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaika vechaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen ellam uzhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen ellam uzhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi aagi vanthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi aagi vanthaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan ellam kaadhaliche thala narachaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan ellam kaadhaliche thala narachaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambividadhae ponna nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae ponna nambividadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae ponna nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae ponna nambividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalainga kaadhala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga kaadhala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambividadhae nambividadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambividadhae nambividadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalainga pombalainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga pombalainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosam illainga mosam illainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosam illainga mosam illainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pombalainga illaiyina neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalainga illaiyina neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Illainga naanum illainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illainga naanum illainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai ingae pethavalum pombalathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ingae pethavalum pombalathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda poranthavalum pombalathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda poranthavalum pombalathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">thapu seiyadhae nee ponna thitaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapu seiyadhae nee ponna thitaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">thapu seiyadhae nee ponna thitaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapu seiyadhae nee ponna thitaadhae"/>
</div>
</pre>
