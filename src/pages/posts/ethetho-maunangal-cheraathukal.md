---
title: "ethetho maunangal song lyrics"
album: "Cheraathukal"
artist: "Anand Nambiar - Pratik Abhyankar"
lyricist: "Anu Kurisinkal"
director: "Shanoob Karuvath "
path: "/albums/cheraathukal-lyrics"
song: "Ethetho Maunangal"
image: ../../images/albumart/cheraathukal.jpg
date: 2021-06-17
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/3CP68ND_qaE"
type: "love"
singers:
  - Vidhu Prathap
  - Mejjo Joseph
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ethetho maunangal mooliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethetho maunangal mooliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aararoo arikilinninayaakavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aararoo arikilinninayaakavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ororo neram naruthoovalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ororo neram naruthoovalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanuum kanaviloruvariyakumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanuum kanaviloruvariyakumoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannithila nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannithila nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Annuyiraaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annuyiraaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellakallathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellakallathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmaniyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmaniyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnumpoonayi mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnumpoonayi mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaril mayangaam njaan mayilpoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaril mayangaam njaan mayilpoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamidum mazhapolan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamidum mazhapolan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamaaka mukilaay nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamaaka mukilaay nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyumee nimiyithilalakalaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyumee nimiyithilalakalaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhukumaa viralin pulakamoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhukumaa viralin pulakamoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mizhikalazhuthum pranayakadhayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mizhikalazhuthum pranayakadhayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithanirayum ninavupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithanirayum ninavupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavililulayum ariyamadhuridha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavililulayum ariyamadhuridha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muralithirayum chodikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muralithirayum chodikalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna dhinamthorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna dhinamthorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnil aliyaam njan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnil aliyaam njan"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne nammiloorumii anuragamaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne nammiloorumii anuragamaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnumpoonayi mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnumpoonayi mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaril mayangaam njaan mayilpoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaril mayangaam njaan mayilpoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamidum mazhapolan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamidum mazhapolan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamaaka mukilaay nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamaaka mukilaay nee"/>
</div>
</pre>
