---
title: "ratatapata song lyrics"
album: "Aranmanai 3"
artist: "C. Sathya"
lyricist: "Arivu"
director: "Sundar C"
path: "/albums/aranmanai-3-lyrics"
song: "Ratatapata"
image: ../../images/albumart/aranmanai-3.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nDGNMuTO2tg"
type: "happy"
singers:
  - Arivu
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ratatapata, ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata, ratatapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata, ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata, ratatapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra ready go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ready go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aranmanai kulla yarauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai kulla yarauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu arandavan kannukku thaan peiyuua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu arandavan kannukku thaan peiyuua"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavu adaipathu yarauda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavu adaipathu yarauda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai bayapada vaikkum vanthu paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai bayapada vaikkum vanthu paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palla kaatum unnaa kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palla kaatum unnaa kekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu kannaa kannaa uruttinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu kannaa kannaa uruttinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu gappula thaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu gappula thaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaa paakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaa paakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuu nenachadha nadathida edam kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuu nenachadha nadathida edam kekkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratatapata hey ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata hey ratatapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata hey ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata hey ratatapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata hey ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata hey ratatapata"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata hey ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata hey ratatapata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra ready for
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ready for"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratatapata unnaa kadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata unnaa kadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata unnaa izhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata unnaa izhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata kanna parikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata kanna parikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratatapata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratatapata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkura idhu oru paada paada nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura idhu oru paada paada nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamthula olaruthu aranmanai kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamthula olaruthu aranmanai kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum konjam neramthula irukkuthu idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum konjam neramthula irukkuthu idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikadi ezhumbudhu chalaa chalaa ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi ezhumbudhu chalaa chalaa ozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnan chiru vayasula solli thaara bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnan chiru vayasula solli thaara bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaraikku pogum bodhu kooda kooda varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaraikku pogum bodhu kooda kooda varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula nenaipathu nenaipilla nejaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula nenaipathu nenaipilla nejaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijathukku bayam thaan unakkilla idaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijathukku bayam thaan unakkilla idaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovam pattu sirikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam pattu sirikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambellam nadungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambellam nadungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya katti izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranmanai alaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai alaikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha pakkam sirikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha pakkam sirikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pakkam moraikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pakkam moraikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavu inga thirakkudhu aranmanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavu inga thirakkudhu aranmanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aranmanai, aranmanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai, aranmanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranmanai, aranmanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai, aranmanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aranmanai aranmanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai aranmanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranmanai aranmanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai aranmanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aranmanai kulla yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai kulla yaaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu arandavan kannukku thaan peiyuua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu arandavan kannukku thaan peiyuua"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavu adaipathu yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavu adaipathu yaaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai bayapada vaikkum vanthu paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai bayapada vaikkum vanthu paaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settaikaara shake it shake it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settaikaara shake it shake it"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pallai kaatti sirikkuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pallai kaatti sirikkuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">break it break it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="break it break it"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam podum rock it rock it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam podum rock it rock it"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aranmanai kadhavula we varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aranmanai kadhavula we varuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga konjam inga konjam ozhinji kaattum nejam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga konjam inga konjam ozhinji kaattum nejam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla pada pada bayatha kaattum mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla pada pada bayatha kaattum mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna pinna theriyathu mudinji pochi kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna pinna theriyathu mudinji pochi kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai kandhal poratti pottu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai kandhal poratti pottu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi munnadi kai kaattuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi munnadi kai kaattuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Illadha polladha pei vaattuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illadha polladha pei vaattuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollama kollama vaazhatuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama kollama vaazhatuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu ennodu kanna paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu ennodu kanna paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna pinna izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna pinna izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolla kolla thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla kolla thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palla palla izhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palla palla izhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartu beatu tholaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu beatu tholaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolai inga idikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolai inga idikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranmanai alaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranmanai alaikkudhu"/>
</div>
</pre>
