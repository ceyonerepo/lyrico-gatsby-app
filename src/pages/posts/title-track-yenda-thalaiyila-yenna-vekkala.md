---
title: "title track song lyrics"
album: "Yenda Thalaiyila Yenna Vekkala"
artist: "A.R. Reihana"
lyricist: "Dharma"
director: "Vignesh Karthick"
path: "/albums/yenda-thalaiyila-yenna-vekkala-song-lyrics"
song: "Title Track"
image: ../../images/albumart/yenda-thalaiyila-yenna-vekkala.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/olebJmFUhqU"
type: "title track"
singers:
  - A.R. Reihana
  - Sajesan Vivekji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yenda thalaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda thalaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vekkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vekkala"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda thalaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda thalaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vekkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vekkala"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda thalaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda thalaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vekkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vekkala"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda yenda"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda thalaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda thalaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vekkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vekkala"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda yenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda yenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">solli adi killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli adi killi"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu attack pandra puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu attack pandra puli"/>
</div>
<div class="lyrico-lyrics-wrapper">kadha vaada kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadha vaada kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thenju pona cd
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thenju pona cd"/>
</div>
<div class="lyrico-lyrics-wrapper">pattasa vedikum naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattasa vedikum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">epavumo trend da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavumo trend da"/>
</div>
<div class="lyrico-lyrics-wrapper">natasa engaloda only
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natasa engaloda only"/>
</div>
<div class="lyrico-lyrics-wrapper">girl friend da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="girl friend da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadakura thudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakura thudikira"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudu murugura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudu murugura"/>
</div>
<div class="lyrico-lyrics-wrapper">vedikira vedikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedikira vedikira"/>
</div>
<div class="lyrico-lyrics-wrapper">pala mari vilugura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala mari vilugura"/>
</div>
<div class="lyrico-lyrics-wrapper">miratura urutura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miratura urutura"/>
</div>
<div class="lyrico-lyrics-wrapper">virutena pidikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virutena pidikira"/>
</div>
<div class="lyrico-lyrics-wrapper">thalakeela thavikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalakeela thavikura"/>
</div>
<div class="lyrico-lyrics-wrapper">kathura muthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathura muthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oodi oodi vanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi oodi vanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu da moochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu da moochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">en dappa dance aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en dappa dance aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pochu da pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pochu da pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukulla evayevano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukulla evayevano"/>
</div>
<div class="lyrico-lyrics-wrapper">panuran da pongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panuran da pongu"/>
</div>
<div class="lyrico-lyrics-wrapper">namakulla kekuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakulla kekuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">tandanakka song uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tandanakka song uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenda thalaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda thalaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vekka villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vekka villa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatho keezo eera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatho keezo eera"/>
</div>
<div class="lyrico-lyrics-wrapper">beejo veera life oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beejo veera life oh"/>
</div>
<div class="lyrico-lyrics-wrapper">spicy aachu ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="spicy aachu ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sotta thala valuku thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sotta thala valuku thala"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam da kastam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam da kastam da"/>
</div>
<div class="lyrico-lyrics-wrapper">yenna vacha aagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna vacha aagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kastam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastam da"/>
</div>
<div class="lyrico-lyrics-wrapper">atham motham karumbu aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atham motham karumbu aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagada kaaya maari pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagada kaaya maari pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nadakuthu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nadakuthu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">summa rajini mathiri siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa rajini mathiri siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yennaya podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennaya podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogi vantha oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogi vantha oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">engum thatha koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum thatha koodu"/>
</div>
</pre>
