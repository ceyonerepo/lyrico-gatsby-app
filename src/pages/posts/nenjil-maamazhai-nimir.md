---
title: "nenjil maamazhai song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Thamarai"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Nenjil Maamazhai"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/k5xRIKbW2t4"
type: "love"
singers:
  - Haricharan
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhu vaanam koothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhu vaanam koothaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu engum poothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu engum poothaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethanai naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai naal paarppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal paarppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru kaaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru kaaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kural paadal ullae odudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kural paadal ullae odudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram paadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram paadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhu vaanam koothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhu vaanam koothaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu engum poothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu engum poothaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathil yethanai naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil yethanai naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadam vandhu paarkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam vandhu paarkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraamal pogum naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraamal pogum naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenn yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenn yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambaaga sandai poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaaga sandai poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaponaal en naatkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaponaal en naatkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam poosi thandhavalum needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam poosi thandhavalum needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullal illaa en paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullal illaa en paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil meenaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil meenaai "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhavalum needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhavalum needhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethanai naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai naal paarppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal paarppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru kaaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru kaaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kural paadal ullae odudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kural paadal ullae odudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram paadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram paadudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhu vaanam koothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhu vaanam koothaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa.haaaahaaa haaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa.haaaahaaa haaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasaangu seidhadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaangu seidhadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaavai thedi kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaavai thedi kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Odumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rosaappoo maalai rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosaappoo maalai rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaamal maatrikolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaamal maatrikolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penngal illaa en veetilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penngal illaa en veetilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham vaithu neeyum vara vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham vaithu neeyum vara vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral illa en thottathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral illa en thottathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal thaanae kaatru varum meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal thaanae kaatru varum meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethanai naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai naal paarppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal paarppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru kaaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru kaaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kural paadal ullae odudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kural paadal ullae odudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram paadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram paadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil maamazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil maamazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhu vaanam koothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhu vaanam koothaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum thaamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum thaamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu engum poothaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu engum poothaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethanai naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai naal paarppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai naal paarppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti nindru kaaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti nindru kaaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla kural paadal ullae odudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla kural paadal ullae odudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kanmoodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kanmoodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram paadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoram paadudhu"/>
</div>
</pre>
