---
title: "hostel gaana song lyrics"
album: "Hostel"
artist: "Bobo Shashi"
lyricist: "Gaana Edwin"
director: "Sumanth Radhakrishnan"
path: "/albums/hostel-lyrics"
song: "Hostel Gaana"
image: ../../images/albumart/hostel.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6VJkf_gl9NQ"
type: "gaana"
singers:
  - Deva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Watchaa maela watchaa vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchaa maela watchaa vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soja maela soja kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soja maela soja kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharadhaan iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharadhaan iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanga king daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga king daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engallukku ullasathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engallukku ullasathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilasame itha daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilasame itha daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amukku dumukku amaal dumaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku dumukku amaal dumaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiri puthiri ajaal kujaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri puthiri ajaal kujaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottanguchi pola room size daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottanguchi pola room size daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku eppovumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku eppovumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae thattula rice daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae thattula rice daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaayaaa vaazhndhiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaayaaa vaazhndhiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaayaa kalakkiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaayaa kalakkiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaavaa irundhiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaavaa irundhiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukkullaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaalikku vaeli illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalikku vaeli illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poliyaa nadichadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poliyaa nadichadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabaikkum jigidingaelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabaikkum jigidingaelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa na"/>
</div>
<div class="lyrico-lyrics-wrapper">Than nanaana na thaa nananana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than nanaana na thaa nananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa nanaana nananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa nanaana nananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa na"/>
</div>
<div class="lyrico-lyrics-wrapper">Than nanaana na thaa nananana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than nanaana na thaa nananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa nanaana nananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa nanaana nananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Watchaa maela watchaa vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchaa maela watchaa vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soja maela soja kaattuvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soja maela soja kaattuvoom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathirinna Iruttula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathirinna Iruttula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhuguvarthi lightla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhuguvarthi lightla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbalaadhan koodikunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbalaadhan koodikunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samathuvatha kaattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samathuvatha kaattuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One and only bottle lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One and only bottle lu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittukuvom battle lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittukuvom battle lu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachadhaellam kedaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachadhaellam kedaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu natchatthira hotel lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu natchatthira hotel lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaana paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga ennaikkumae panjamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga ennaikkumae panjamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetta podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetta podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai parandhirumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai parandhirumdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottu vaazhakkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottu vaazhakkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga undhu illa endhu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga undhu illa endhu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathi maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuniya maathikkuvomdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuniya maathikkuvomdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa kedachchirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kedachchirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakku indha junction machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakku indha junction machi"/>
</div>
<div class="lyrico-lyrics-wrapper">No tension machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No tension machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkia fulla function machi machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkia fulla function machi machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Watchaa maela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchaa maela"/>
</div>
<div class="lyrico-lyrics-wrapper">Watchaa maela aiyooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchaa maela aiyooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soja mela soja mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soja mela soja mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Watchaa Maela Watchaa Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watchaa Maela Watchaa Vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soja maela soja kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soja maela soja kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharadhaan iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharadhaan iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanga king daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga king daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engallukku ullasathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engallukku ullasathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilasame itha daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilasame itha daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amukku dumukku amaal dumaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku dumukku amaal dumaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiri puthiri ajaal kujaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri puthiri ajaal kujaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottanguchi pola room size daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottanguchi pola room size daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku eppovumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku eppovumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae thattula rice daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae thattula rice daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaayaaa vaazhndhiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaayaaa vaazhndhiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaayaa kalakkiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaayaa kalakkiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaavaa irundhiduvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaavaa irundhiduvoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukkullaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaalikku vaeli illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalikku vaeli illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poliyaa nadichadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poliyaa nadichadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabaikkum jigidingaelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabaikkum jigidingaelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa na"/>
</div>
<div class="lyrico-lyrics-wrapper">Than nanaana na thaa nananana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than nanaana na thaa nananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa nanaana nananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa nanaana nananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa na"/>
</div>
<div class="lyrico-lyrics-wrapper">Than nanaana na thaa nananana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than nanaana na thaa nananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa nanaana nananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa nanaana nananaa"/>
</div>
</pre>
