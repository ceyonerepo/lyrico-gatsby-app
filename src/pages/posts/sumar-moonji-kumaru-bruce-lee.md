---
title: "sumar moonji kumaru song lyrics"
album: "Bruce Lee"
artist: "G V Prakash Kumar"
lyricist: "Gana Vinoth"
director: "Prashanth Pandiraj"
path: "/albums/bruce-lee-lyrics"
song: "Sumar Moonji Kumaru"
image: ../../images/albumart/bruce-lee.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qXb9vgzMMKA"
type: "gaana"
singers:
  -	Silambarasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaru thagara dappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru thagara dappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumar moonji kumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumar moonji kumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurukka pesidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurukka pesidaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam neram summarru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam neram summarru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summaana scena pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaana scena pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kitta kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kitta kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalae karuppu gulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalae karuppu gulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaya padam ottaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya padam ottaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachcha pulla enna ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha pulla enna ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashaavathaan aakki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashaavathaan aakki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attu malaa irundha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu malaa irundha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gettup ellaam maathivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gettup ellaam maathivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit-tu aalaa maathiputtaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit-tu aalaa maathiputtaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedaa kodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaa kodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana paadam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana paadam da"/>
</div>
<div class="lyrico-lyrics-wrapper">En gaana-vula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En gaana-vula"/>
</div>
<div class="lyrico-lyrics-wrapper">Karutha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karutha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla paadam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla paadam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavaa moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavaa moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu keda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya udaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya udaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kachchaa muchchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kachchaa muchchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Echcha kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echcha kudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya udaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya udaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumthalakkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala gala gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo haa hoo haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo haa hoo haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala gala gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala gala gala gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala gala gala gal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala gala gala gal"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala gala gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala gala gala gala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththala karula katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththala karula katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalaiyae urula vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalaiyae urula vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththaadi pola naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththaadi pola naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Buildup la perala vutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buildup la perala vutten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meiyaalumae kelu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaalumae kelu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaa scene-nu aalu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaa scene-nu aalu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna-thula don-u machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna-thula don-u machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan super star-u fan-u machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan super star-u fan-u machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atta kaththi mokka kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta kaththi mokka kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigidi kaththidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigidi kaththidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saanavathaan pudikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan saanavathaan pudikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharppu kaththi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharppu kaththi daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadiyae edukkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadiyae edukkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaalu kaaren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaalu kaaren daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa bat-tae thottu paarkkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa bat-tae thottu paarkkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tendulkar-ru daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tendulkar-ru daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavvulunda irukkundhunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavvulunda irukkundhunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumba poi nasukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumba poi nasukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhukulla poondhukinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhukulla poondhukinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya vuttu kasakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya vuttu kasakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atom bomb-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atom bomb-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sounda kettu alarinu oduvadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sounda kettu alarinu oduvadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppdi neeyum engakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppdi neeyum engakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondikondi modhuvadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondikondi modhuvadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silpaaana sarikku vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpaaana sarikku vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanaa kallu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanaa kallu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En body oru jerkavaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En body oru jerkavaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paenaa mulludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paenaa mulludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vartha meenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vartha meenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamba vachchu vala aattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamba vachchu vala aattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan orae oru kai saamaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan orae oru kai saamaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru thagara dappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru thagara dappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumar moonji kumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumar moonji kumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurukka pesidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurukka pesidaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam neram summarru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam neram summarru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summaana scena pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaana scena pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kitta kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kitta kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalae karuppu gulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalae karuppu gulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaya padam ottaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaya padam ottaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachcha pulla enna ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha pulla enna ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashaavathaan aakki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashaavathaan aakki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attu malaa irundha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu malaa irundha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gettup ellaam maathivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gettup ellaam maathivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit-tu aalaa maathiputtaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit-tu aalaa maathiputtaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedaa kodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaa kodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana paadam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana paadam da"/>
</div>
<div class="lyrico-lyrics-wrapper">En gaana-vula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En gaana-vula"/>
</div>
<div class="lyrico-lyrics-wrapper">Karutha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karutha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla paadam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla paadam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavaa moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavaa moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammunu keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammunu keda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya udaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya udaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye kachchaa muchchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kachchaa muchchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Echcha kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echcha kudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya udaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya udaatha"/>
</div>
</pre>
