---
title: "kundelu pilla song lyrics"
album: "Pogaru"
artist: "Chandan Shetty"
lyricist: "Bhaskarabhatla"
director: "Nandha kishor"
path: "/albums/pogaru-lyrics"
song: "Kundelu Pilla - Tractor Nenu Polam Nuvvu"
image: ../../images/albumart/pogaru.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/POhgsVEKAHs"
type: "happy"
singers:
 - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tractor nenu polam nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tractor nenu polam nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruvu nenu chepavi nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruvu nenu chepavi nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chettuni nenu chilakavi nuvvu raave egiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chettuni nenu chilakavi nuvvu raave egiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nippu nenu beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nippu nenu beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu boondhi nenu brandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu boondhi nenu brandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu mix ayipothe super jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu mix ayipothe super jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti manasune chedagottesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti manasune chedagottesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte nidhuralone yuddhalenno chesthunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte nidhuralone yuddhalenno chesthunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundelu pilla tellani kundelu pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundelu pilla tellani kundelu pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve lekunda abbabba kaadhe naa valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve lekunda abbabba kaadhe naa valla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jummantadhe gunde rammantadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jummantadhe gunde rammantadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne teesukochi bokelaga immantadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne teesukochi bokelaga immantadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirr antadhe summa surr antadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirr antadhe summa surr antadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontlo naram naram currentulaa sar antadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontlo naram naram currentulaa sar antadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa premanadugu undadhu ye tarugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa premanadugu undadhu ye tarugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugadgu godugai vente untane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugadgu godugai vente untane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasivaadu manasu pai vaadike telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasivaadu manasu pai vaadike telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottesi chebuthunna naa pranam nuvvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottesi chebuthunna naa pranam nuvvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundelu pilla tellani kundelu pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundelu pilla tellani kundelu pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thali bottu kattesthane tellarekalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thali bottu kattesthane tellarekalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goru muddhe thinnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru muddhe thinnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jola paate vinnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jola paate vinnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Poni amma vollo yenadaina aadukunnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poni amma vollo yenadaina aadukunnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandaga chesthana manchi battalu kattana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandaga chesthana manchi battalu kattana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu bandhuvulaina bandhalaina nuvve antunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu bandhuvulaina bandhalaina nuvve antunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalona baadha vadhilellipoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalona baadha vadhilellipoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanniti sudilo munuguthunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanniti sudilo munuguthunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne korindhedhi naathoti ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne korindhedhi naathoti ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvaina naakinka thodu undave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvaina naakinka thodu undave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundelu pilla ekkadiki pothadhi pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundelu pilla ekkadiki pothadhi pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne kori vasthadhiraa vendi vennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne kori vasthadhiraa vendi vennela"/>
</div>
</pre>
