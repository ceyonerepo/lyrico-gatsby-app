---
title: "idhayane ennai enaa seigirai song lyrics"
album: "Velaikkaran"
artist: "Anirudh Ravichander"
lyricist: "Madhan Karky"
director: "Mohan Raja"
path: "/albums/velaikkaran-lyrics"
song: "Idhayane Ennai Enna Seigirai"
image: ../../images/albumart/velaikkaran.jpg
date: 2017-12-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9MRtXLE46Xg"
type: "Love"
singers:
  - Neeti Mohan
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayane Ennai Enna Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayane Ennai Enna Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimaigal Ennil Seithu Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaigal Ennil Seithu Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayanae Ennai Maayam Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayanae Ennai Maayam Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravugal Vellaiyaakki Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Vellaiyaakki Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Virigiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Virigiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yen Konamaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yen Konamaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Purigiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Purigiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Yen Sinungalaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yen Sinungalaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poigai Neenguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poigai Neenguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Thondruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Thondruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thozhan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thozhan Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhazhgal Koorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhazhgal Koorudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomi Maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Maarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannam Yerudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Yerudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kadhal Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kadhal Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nenjam Koorudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjam Koorudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unpoley Yaarum Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpoley Yaarum Yaarum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oar Ellaiyatra Kadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Ellaiyatra Kadhal Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nibandhanaigal Yethum Ennil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nibandhanaigal Yethum Ennil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanangal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanangal Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaadhey Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadhey Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unpoley Yaarum Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpoley Yaarum Yaarum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oar Ellaiyatra Kadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Ellaiyatra Kadhal Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nibandhanaigal Yethum Ennil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nibandhanaigal Yethum Ennil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanangal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanangal Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaadhey Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadhey Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayane Ennai Enna Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayane Ennai Enna Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimaigal Ennil Seithu Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaigal Ennil Seithu Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayanae Ennai Maayam Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayanae Ennai Maayam Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravugal Vellaiyaakki Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Vellaiyaakki Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirum Pudhirum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirum Pudhirum Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Naan Ninaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Naan Ninaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Udhiram Endru Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Udhiram Endru Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarugu Sarugu Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarugu Sarugu Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Udhirndhu Veezhum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Udhirndhu Veezhum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragu Siragu Thandhu Vaanil Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragu Siragu Thandhu Vaanil Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalmurai Enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmurai Enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai Thaandi Tholai Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Thaandi Tholai Thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvi Indri Ulley Selgiraaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi Indri Ulley Selgiraaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalmurai Enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalmurai Enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nejam Kandu Unmai Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejam Kandu Unmai Kandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Kandu Kaadhal Solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Kandu Kaadhal Solgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unpoley Yaarum Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpoley Yaarum Yaarum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oar Ellaiyatra Kadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Ellaiyatra Kadhal Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nibandhanaigal Yethum Ennil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nibandhanaigal Yethum Ennil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanangal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanangal Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkaadhey Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadhey Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayane Ennai Enna Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayane Ennai Enna Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimaigal Ennil Seithu Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaigal Ennil Seithu Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayanae Ennai Maayam Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayanae Ennai Maayam Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravugal Vellaiyaakki Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Vellaiyaakki Ponaai"/>
</div>
</pre>
