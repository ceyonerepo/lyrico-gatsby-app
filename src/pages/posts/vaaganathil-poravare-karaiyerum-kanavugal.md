---
title: "vaaganathil poravare song lyrics"
album: "Karaiyerum kanavugal"
artist: "A Sathiyamoorthy"
lyricist: "KPS Saamyy"
director: "KPS Saamyy"
path: "/albums/karaiyerum-kanavugal-lyrics"
song: "Vaaganathil Poravare"
image: ../../images/albumart/karaiyerum-kanavugal.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vRkNumneAPM"
type: "mass"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rout ah mathi pogatha da nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rout ah mathi pogatha da nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum rout ah mathi pogatha da nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum rout ah mathi pogatha da nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku kudumbam iruku pathu poda nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku kudumbam iruku pathu poda nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">payanathile thalai kavasam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanathile thalai kavasam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kapathum uyir kavasam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kapathum uyir kavasam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">payanathile thalai kavasam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanathile thalai kavasam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kapathum uyir kavasam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kapathum uyir kavasam nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalai kavasam illame nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kavasam illame nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai kavasam illame nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kavasam illame nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku vandi payanam venamada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku vandi payanam venamada nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iravu nera payanam vendam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu nera payanam vendam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum high way il poga venam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum high way il poga venam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu nera payanam vendam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu nera payanam vendam nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum high way il poga venam nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum high way il poga venam nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kudithu vitu otathada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithu vitu otathada nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">mathuvai kudithu vitu otathada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathuvai kudithu vitu otathada nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiye un uyirai mudithu vidum nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiye un uyirai mudithu vidum nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cell phone nil pesikitu nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone nil pesikitu nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum vaganatha otathada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum vaganatha otathada nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">cell phone nil pesikitu nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cell phone nil pesikitu nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum vaganatha otathada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum vaganatha otathada nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athiga paaram venamada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athiga paaram venamada nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">vandiku athiga paaram venamada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandiku athiga paaram venamada nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">payanam aabathile mudiyumada nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanam aabathile mudiyumada nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaganathil poravare nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaganathil poravare nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vegamaga pogathada nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vegamaga pogathada nanbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manjal vilaku pottaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal vilaku pottaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nindru kavanamaga poga venum nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nindru kavanamaga poga venum nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">manjal vilaku pottaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal vilaku pottaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nindru kavanamaga poga venum nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nindru kavanamaga poga venum nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sigapu vilaku potaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sigapu vilaku potaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">odane vaganathai niruthi vidu nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odane vaganathai niruthi vidu nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">sigapu vilaku potaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sigapu vilaku potaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">odane vaganathai niruthi vidu nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odane vaganathai niruthi vidu nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pachai vilaku potaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachai vilaku potaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum roata pathu poga venum naba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum roata pathu poga venum naba"/>
</div>
<div class="lyrico-lyrics-wrapper">pachai vilaku potaka nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachai vilaku potaka nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum roata pathu poga venum naba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum roata pathu poga venum naba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba nanba ennoda nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba nanba ennoda nanba nanba"/>
</div>
</pre>
