---
title: "rajarshi song lyrics"
album: "NTR Mahanayakudu"
artist: "M.M. Keeravani"
lyricist: "K. Siva Datta - Dr.K. Rama Krishna - M.M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/ntr-mahanayakudu-lyrics"
song: "Rajarshi"
image: ../../images/albumart/ntr-mahanayakudu.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XC2HF3pO670"
type: "happy"
singers:
  - M.M. Keeravani
  - Kaala Bhairava
  - Sreenidhi Tirumala
  - Sharath Santosh
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Na maatha Pitaa Naiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na maatha Pitaa Naiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhurnamithra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhurnamithra"/>
</div>
<div class="lyrico-lyrics-wrapper">Namey Dhwesharagou
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namey Dhwesharagou"/>
</div>
<div class="lyrico-lyrics-wrapper">Namey Lobhamohou
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namey Lobhamohou"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Punyam Na Papam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Punyam Na Papam"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Soukhyam Na Dhukham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Soukhyam Na Dhukham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Yedhi Thandri Yedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Yedhi Thandri Yedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adduthagile Bandhamedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adduthagile Bandhamedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamathalevii Maayalevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamathalevii Maayalevi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuporalaa Masakalevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuporalaa Masakalevi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illu Nee Vallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illu Nee Vallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhantu Ee Chintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhantu Ee Chintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunthaina Leni Ee Nelapy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunthaina Leni Ee Nelapy"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadayaadu Rushivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadayaadu Rushivoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krushitho Nasthi Dhurbikshamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krushitho Nasthi Dhurbikshamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokhanni Sashinchu Manishivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokhanni Sashinchu Manishivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rushivo Rajarshivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rushivo Rajarshivo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yavarivo Neevevarivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavarivo Neevevarivo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevevarivo O Yavarivo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevevarivo O Yavarivo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Mantro Na Teerdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mantro Na Teerdham"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Yagno Na Vedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Yagno Na Vedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Dharmo Na Chardho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Dharmo Na Chardho"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Moksham Na Kaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Moksham Na Kaamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Mruthyurnnasankha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mruthyurnnasankha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namey Jaathi Bedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namey Jaathi Bedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagruthamuloo Jaagu Yedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagruthamuloo Jaagu Yedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathri Edhi Pagalu Yedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathri Edhi Pagalu Yedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karyadhiksha Baddhudavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karyadhiksha Baddhudavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu Yedhi Dhigulu Yedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu Yedhi Dhigulu Yedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchvaasa Nishvaasamula Pranayaganni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchvaasa Nishvaasamula Pranayaganni"/>
</div>
<div class="lyrico-lyrics-wrapper">Urvijanoddharanakai Cheyu Rajayogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urvijanoddharanakai Cheyu Rajayogi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhanarangana Karmayogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhanarangana Karmayogi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aham Nirvikalpo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aham Nirvikalpo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirakara Roopo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirakara Roopo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vibhurvapya Sarvatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vibhurvapya Sarvatra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarvendhriyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvendhriyaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathejo Navayurna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathejo Navayurna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomirnavyomam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomirnavyomam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirvasana Vaasanna Sankshema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirvasana Vaasanna Sankshema"/>
</div>
<div class="lyrico-lyrics-wrapper">Swapnikudu Yithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapnikudu Yithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nishkriyaprachchanna Sangrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishkriyaprachchanna Sangrama"/>
</div>
<div class="lyrico-lyrics-wrapper">Srameekudu Yithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srameekudu Yithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niratha Sanghashreya Sandhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niratha Sanghashreya Sandhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhavukudu Yithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhavukudu Yithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahanayakudu Yithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahanayakudu Yithadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahanayakudu Yithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahanayakudu Yithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Maatha Pitha Naiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Maatha Pitha Naiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhurnamithra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhurnamithra"/>
</div>
<div class="lyrico-lyrics-wrapper">Namey Dhwesharagou
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namey Dhwesharagou"/>
</div>
<div class="lyrico-lyrics-wrapper">Namey Lobhamohou
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namey Lobhamohou"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Punyam Na Papam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Punyam Na Papam"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Soukhyam Na Dhukkham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Soukhyam Na Dhukkham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Mantro Na Teerdham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mantro Na Teerdham"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Yagno Na Vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Yagno Na Vedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Dharmo Na chardho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Dharmo Na chardho"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Moksho Na Kamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Moksho Na Kamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Mrutyurnasankha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mrutyurnasankha"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Mey Jathi Bedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mey Jathi Bedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aham Nirvikalpo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aham Nirvikalpo"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraakara Roopo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraakara Roopo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vibhurvapya Sarvatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vibhurvapya Sarvatra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarvenriyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvenriyaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathejo Navayurna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathejo Navayurna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhumirnavyomam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhumirnavyomam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhanandha Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhanandha Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivoham Shivoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivoham Shivoham"/>
</div>
</pre>
