---
title: "yennatuma vaanathula song lyrics"
album: "Nanjupuram"
artist: "Raaghav"
lyricist: "Magudeshwaran"
director: "Charles"
path: "/albums/nanjupuram-lyrics"
song: "Yennatuma Vaanathula"
image: ../../images/albumart/nanjupuram.jpg
date: 2011-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MdJ9x6IYQrU"
type: "happy"
singers:
  - Raaghav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennattuma vaanathula natchathiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennattuma vaanathula natchathiratha"/>
</div>
<div class="lyrico-lyrics-wrapper">onnilla rendilla meenilla naalilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnilla rendilla meenilla naalilla"/>
</div>
<div class="lyrico-lyrics-wrapper">noorilla koadiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noorilla koadiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sollattumaa vaanathula sinna nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollattumaa vaanathula sinna nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">thaangavilla padaiyalla aaranju sulaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaangavilla padaiyalla aaranju sulaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu velli thoonumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu velli thoonumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennattuma vaanathula natchathiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennattuma vaanathula natchathiratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Min mini poochiga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min mini poochiga "/>
</div>
<div class="lyrico-lyrics-wrapper">seladhu mellappoagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seladhu mellappoagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thennai marathula Ola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennai marathula Ola "/>
</div>
<div class="lyrico-lyrics-wrapper">thirubipadukkudhu poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirubipadukkudhu poala"/>
</div>
<div class="lyrico-lyrics-wrapper">Min mini poochiga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min mini poochiga "/>
</div>
<div class="lyrico-lyrics-wrapper">seladhu mellappoagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seladhu mellappoagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thennai marathula Ola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennai marathula Ola "/>
</div>
<div class="lyrico-lyrics-wrapper">thirubipadukkudhu poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirubipadukkudhu poala"/>
</div>
<div class="lyrico-lyrics-wrapper">kammaakkaraiyila thavalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammaakkaraiyila thavalai "/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi azhaikkidhu thunaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi azhaikkidhu thunaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">sinna chinna thaarsatham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinna chinna thaarsatham "/>
</div>
<div class="lyrico-lyrics-wrapper">sellamma kudukkura mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellamma kudukkura mutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennattuma vaanathula natchathiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennattuma vaanathula natchathiratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada unna suththi imbuttu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada unna suththi imbuttu "/>
</div>
<div class="lyrico-lyrics-wrapper">saththam irukkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saththam irukkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">adhellaam uttubuttu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhellaam uttubuttu "/>
</div>
<div class="lyrico-lyrics-wrapper">indha satham mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha satham mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaadhula thaniyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaadhula thaniyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu yen uzhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu yen uzhudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ush nallaa kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ush nallaa kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">panagaa boththunu vizhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panagaa boththunu vizhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paduthaa asappoadudhu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduthaa asappoadudhu yerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna malarkoottam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna malarkoottam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasanai veesudhu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasanai veesudhu kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panagaa boththunu vizhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panagaa boththunu vizhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paduthaa asappoadudhu yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduthaa asappoadudhu yerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna malarkoottam poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna malarkoottam poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasanai veesudhu kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasanai veesudhu kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanagalum ellaam varattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanagalum ellaam varattum"/>
</div>
<div class="lyrico-lyrics-wrapper">sandhiranum thaniyaa nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandhiranum thaniyaa nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">vera idhathaan valarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera idhathaan valarkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiya paarkkudhu kizhakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiya paarkkudhu kizhakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennattuma vaanathula natchathiratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennattuma vaanathula natchathiratha"/>
</div>
</pre>
