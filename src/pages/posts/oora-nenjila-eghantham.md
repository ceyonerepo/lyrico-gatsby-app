---
title: "oora nenjila song lyrics"
album: "Eghantham"
artist: "Ganesh Raghavendra"
lyricist: "Yuga Bharathi"
director: "Arsal Arumugam"
path: "/albums/eghantham-lyrics"
song: "Oora Nenjila"
image: ../../images/albumart/eghantham.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-v3CgaQBZLE"
type: "happy"
singers:
  - Jayamoorthi
  - Sai Vignesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanamae polinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamae polinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyae velanjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyae velanjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamum ozhaipavarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamum ozhaipavarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulnnu solliduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulnnu solliduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanamae nilaiaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanamae nilaiaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann uravae peridhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann uravae peridhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum indha thiru naata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum indha thiru naata"/>
</div>
<div class="lyrico-lyrics-wrapper">Valam peravae senjiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valam peravae senjiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Valam peravae senjiduvom..aaann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valam peravae senjiduvom..aaann"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga valam peravae senjiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga valam peravae senjiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora nenjila thaangum ungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora nenjila thaangum ungala"/>
</div>
<div class="lyrico-lyrics-wrapper">Potrum oru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potrum oru naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga nandriya maala kattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga nandriya maala kattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podum thiru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podum thiru naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungalathaan deivamunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalathaan deivamunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kumbiduvom koodi ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kumbiduvom koodi ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam pola vaazhum unga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pola vaazhum unga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thottu poojai seiyurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thottu poojai seiyurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadariya paraiya adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadariya paraiya adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanama pugazha adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanama pugazha adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan adhira thavila edu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan adhira thavila edu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana irava izhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana irava izhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora nenjila thaangum ungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora nenjila thaangum ungala"/>
</div>
<div class="lyrico-lyrics-wrapper">Potrum oru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potrum oru naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga nandriya maalai kattiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga nandriya maalai kattiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podum thiru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podum thiru naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhaana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaana thana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaana thana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaana thana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaana thana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaana thana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poosaari thechadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosaari thechadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaari koladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaari koladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaama iruntha oorum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaama iruntha oorum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidi soryethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidi soryethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unava uzhaikalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unava uzhaikalanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum sogam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum sogam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana mathikalanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana mathikalanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooraana josiyaruKurisollum vedhiyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraana josiyaruKurisollum vedhiyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothaaga kanicha noiyum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothaaga kanicha noiyum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mannaru thovaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mannaru thovaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuni velukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuni velukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar munnala irunthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar munnala irunthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaava ariyavaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaava ariyavaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaava vittakkaa padippu illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaava vittakkaa padippu illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvora kadaisiyilae kaadyetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvora kadaisiyilae kaadyetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaattum sirappu illaeeh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaattum sirappu illaeeh eh eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadariya paraiya adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadariya paraiya adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanama pugazha adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanama pugazha adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan adhira thavila edu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan adhira thavila edu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana irava izhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana irava izhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeraalam sambalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalam sambalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho kimbalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho kimbalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaenu alanju oora vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaenu alanju oora vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala naalaaga urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala naalaaga urava"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthirunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthirunthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal raavaaga perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal raavaaga perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarnthirunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarnthirunthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum engala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum engala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaadha anbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaadha anbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaakka thuninja jallikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaakka thuninja jallikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum mannaaga kedanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum mannaaga kedanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorana kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorana kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhai nellaaga nimirndhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhai nellaaga nimirndhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangi kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangi kittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Car odum nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car odum nagarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae poiyaaga nadakuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae poiyaaga nadakuthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ser oda manathula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ser oda manathula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda panpaadu irukuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda panpaadu irukuthunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadariya paraiya adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadariya paraiya adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanama pugazha adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanama pugazha adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan adhira thavila edu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan adhira thavila edu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana irava izhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana irava izhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadariya paraiya adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadariya paraiya adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayanama pugazha adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayanama pugazha adi daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan adhira thavila edu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan adhira thavila edu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana irava izhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana irava izhu daa"/>
</div>
</pre>
