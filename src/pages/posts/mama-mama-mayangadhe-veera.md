---
title: "mama mama mayangadhe song lyrics"
album: "Veera"
artist: "Leon James"
lyricist: "Krishna Kishore"
director: "Rajaraman"
path: "/albums/veera-lyrics"
song: "Mama Mama Mayangadhe"
image: ../../images/albumart/veera.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1KPrCo34S9Y"
type: "happy"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mama mama mayangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama mayangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Maangaa thinna vaikkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Maangaa thinna vaikkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayangi mayangi pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi mayangi pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne mallakka paduthu madangaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne mallakka paduthu madangaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan quarter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan quarter-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava water-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava water-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye mudinju pochu matter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye mudinju pochu matter-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan parthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parthadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sirichadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En life-a maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life-a maathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaga thaga vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaga thaga vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thala thaalathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thala thaalathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey avalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey avalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachaalae girr ungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachaalae girr ungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava appana paarthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava appana paarthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambellaam darr ungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambellaam darr ungudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai naan paarkum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan paarkum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kondenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayirathil oruvanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathil oruvanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithu kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithu kondenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae nee paarkumpodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae nee paarkumpodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellaamae nenachu nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellaamae nenachu nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasichi rasichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasichi rasichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichi sirichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichi sirichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaga thaga vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaga thaga vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thala thaalathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thala thaalathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama mama mayangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama mama mayangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala Maangaa thinna vaikkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Maangaa thinna vaikkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayangi mayangi pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi mayangi pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne mallakka paduthu madangaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne mallakka paduthu madangaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan quarter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan quarter-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava water-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava water-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye mudinju pochu matter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye mudinju pochu matter-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan parthadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parthadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sirichadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En life-a maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life-a maathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaga thaga vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaga thaga vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi sudhi sudhi yoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi sudhi sudhi yoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thala thaalathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thala thaalathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paada vidu aada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu aada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala thala thaalathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala thala thaalathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vidu yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vidu yeah"/>
</div>
</pre>
