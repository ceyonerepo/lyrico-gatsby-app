---
title: "yaarai yaaridam song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Sarathy"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Yaarai Yaaridam"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iO8y2kClh2k"
type: "melody"
singers:
  - Shweta Mohan
  - Sudharshan M Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ore Thedal Adhai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Thedal Adhai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaalgal Ellaam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaalgal Ellaam Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore Thevai Adhil Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Thevai Adhil Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Aasai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Aasai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ore Boomi Idhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ore Boomi Idhan Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Naatkal Vandhu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Naatkal Vandhu Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore Vaazhkai Namakellaam Kidaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Vaazhkai Namakellaam Kidaiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Boomi Idhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Boomi Idhan Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Naatkal Vandhu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Naatkal Vandhu Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore Vaazhkai Namakellaam Kidaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Vaazhkai Namakellaam Kidaiyaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi Valiyile Payanam Maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi Valiyile Payanam Maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirinthu Povomo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthu Povomo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Engira Valaivil Naamume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Engira Valaivil Naamume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Paarppomo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Paarppomo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yean Inainthom Pirinthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean Inainthom Pirinthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Ennaavom Neelgirathe Valiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Ennaavom Neelgirathe Valiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Ranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Ranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Thedal Adhai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Thedal Adhai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaalgal Ellaam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaalgal Ellaam Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore Thevai Adhil Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Thevai Adhil Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Aasai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Aasai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ore Boomi Idhan Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ore Boomi Idhan Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Naatkal Vandhu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Naatkal Vandhu Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore Vaazhkai Namakellaam Kidaiyaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Vaazhkai Namakellaam Kidaiyaadhe"/>
</div>
</pre>
