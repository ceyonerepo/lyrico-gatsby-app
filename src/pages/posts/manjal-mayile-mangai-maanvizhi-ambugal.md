---
title: "manjal mayile song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Vno - Arungopal"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Manjal Mayile"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/npF-QvUf-pg"
type: "happy"
singers:
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Naal Thaanae Unnai Parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Thaanae Unnai Parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunaal Neeyum Kanavil Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaal Neeyum Kanavil Vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum Unnai Kannil Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Unnai Kannil Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavanaalum Kanvizhithirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavanaalum Kanvizhithirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Karuvizhi Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Karuvizhi Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kavara Kavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kavara Kavara"/>
</div>
<div class="lyrico-lyrics-wrapper">En Iru Vizhi Paarvai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iru Vizhi Paarvai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethara Sethara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethara Sethara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Mayilae Ennai Konjum Kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Mayilae Ennai Konjum Kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Ingu Thedi Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Ingu Thedi Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurinji Poovadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurinji Poovadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthai Perisaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthai Perisaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Mounam Melisaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Mounam Melisaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyalum Isaiyum Nadagamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyalum Isaiyum Nadagamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum Nodiyil Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum Nodiyil Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyooo Ennai Indha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyooo Ennai Indha Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthum Paadu Podhumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthum Paadu Podhumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjal Mayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Mayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Konjum Kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Konjum Kuyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haha Ha Haa Haha Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haha Ha Haa Haha Ha Haa"/>
</div>
</pre>
