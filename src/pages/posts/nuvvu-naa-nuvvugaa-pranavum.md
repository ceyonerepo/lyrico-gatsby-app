---
title: "nuvvu naa nuvvuga song lyrics"
album: "Pranavum"
artist: "Padmanav Bharadwaj"
lyricist: "Karuna Kumar"
director: "Kumar.G"
path: "/albums/pranavum-lyrics"
song: "Nuvvu Naa Nuvvuga"
image: ../../images/albumart/pranavum.jpg
date: 2021-02-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/V6sZlL_cs6Y"
type: "love"
singers:
  - R.P. Patnaik
  - Usha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nuvvu naa nuvvugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu naa nuvvugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ika maaravee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika maaravee"/>
</div>
<div class="lyrico-lyrics-wrapper">neenane nenikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenane nenikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mari leney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari leney"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu nee nenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu nee nenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aipothuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aipothuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu lo nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu lo nenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karigaa ley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karigaa ley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ippudey ippudey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudey ippudey"/>
</div>
<div class="lyrico-lyrics-wrapper">mana kathe modalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana kathe modalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ikkadey ikkadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ikkadey ikkadey"/>
</div>
<div class="lyrico-lyrics-wrapper">mana kale nijamaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana kale nijamaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aagi poindi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagi poindi "/>
</div>
<div class="lyrico-lyrics-wrapper">aduguthu undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduguthu undi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalame oo kshanam ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalame oo kshanam ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">intha santhosham ennadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha santhosham ennadu"/>
</div>
<div class="lyrico-lyrics-wrapper">evarilonu chulledatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarilonu chulledatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nijame idi nijame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijame idi nijame"/>
</div>
<div class="lyrico-lyrics-wrapper">epudu munupeppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epudu munupeppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">evaro ee haayini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaro ee haayini"/>
</div>
<div class="lyrico-lyrics-wrapper">asaley chusundarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaley chusundarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvvu naa nuvvugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu naa nuvvugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ika maaravee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika maaravee"/>
</div>
<div class="lyrico-lyrics-wrapper">neenane nenikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenane nenikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mari leney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari leney"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu nee nenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu nee nenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aipothuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aipothuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu lo nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu lo nenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karigaa ley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karigaa ley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanulatho nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanulatho nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">paluku prathi maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paluku prathi maata"/>
</div>
<div class="lyrico-lyrics-wrapper">manasutho vintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasutho vintu"/>
</div>
<div class="lyrico-lyrics-wrapper">badulu isthuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badulu isthuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulesthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulesthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nadaka nenavthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadaka nenavthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanulu moosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanulu moosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalu nenavthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalu nenavthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pedavinai nenunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedavinai nenunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">palukunai nenosthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palukunai nenosthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunukunai vechi unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunukunai vechi unna"/>
</div>
<div class="lyrico-lyrics-wrapper">patanai vinipisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patanai vinipisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nishabhadamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nishabhadamai"/>
</div>
<div class="lyrico-lyrics-wrapper">nidaroye gundekey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidaroye gundekey"/>
</div>
<div class="lyrico-lyrics-wrapper">eda sadi nenai vasthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eda sadi nenai vasthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">edalone koluvunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edalone koluvunta"/>
</div>
<div class="lyrico-lyrics-wrapper">vuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuntaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvvu naa nuvvugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu naa nuvvugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ika maaravee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika maaravee"/>
</div>
<div class="lyrico-lyrics-wrapper">neenane nenikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenane nenikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mari leney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari leney"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu nee nenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu nee nenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aipothuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aipothuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu lo nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu lo nenilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karigaa ley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karigaa ley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadali ala nenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadali ala nenai"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu pilichagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu pilichagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nadula uppenaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadula uppenaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu kalisagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu kalisagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pudami nai untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudami nai untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chinukunai vasthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinukunai vasthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">puvvu nai poosthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvvu nai poosthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thummedai vasthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thummedai vasthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavarannai unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavarannai unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kougilai penavesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kougilai penavesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunuku nenai unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunuku nenai unna"/>
</div>
<div class="lyrico-lyrics-wrapper">korikai allesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korikai allesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">padi unna manasuthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi unna manasuthoo"/>
</div>
<div class="lyrico-lyrics-wrapper">kadathaka payanisthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadathaka payanisthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">prananne parichesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prananne parichesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chesthaa"/>
</div>
</pre>
