---
title: "mavanae yaarukittae remix song lyrics"
album: "Dilluku Dhuddu 2"
artist: "Shabir"
lyricist: "Rabbit Mac - Shabir"
director: "Rambhala"
path: "/albums/dhilluku-dhuddu-2-lyrics"
song: "Mavanae Yaarukittae Remix"
image: ../../images/albumart/dhilluku-dhuddu-2.jpg
date: 2019-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PioCwcAa0tw"
type: "mass"
singers:
  - Rabbit Mac
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mavanae yaarukittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavanae yaarukittae"/>
</div>
<div class="lyrico-lyrics-wrapper">mavanae yaarukittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavanae yaarukittae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhillukku dhuddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhillukku dhuddu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar inga weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar inga weightu"/>
</div>
<div class="lyrico-lyrics-wrapper">yetthanae kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetthanae kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">padae vanthu ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padae vanthu ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">mun vecha kaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun vecha kaal"/>
</div>
<div class="lyrico-lyrics-wrapper">pin vekkamae nikkanunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin vekkamae nikkanunda"/>
</div>
<div class="lyrico-lyrics-wrapper">sondha kallulae nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sondha kallulae nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">rabbit
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rabbit"/>
</div>
<div class="lyrico-lyrics-wrapper">whacha gonna say now?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whacha gonna say now?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mande kollaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mande kollaru"/>
</div>
<div class="lyrico-lyrics-wrapper">makke naa vantha bejaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makke naa vantha bejaru"/>
</div>
<div class="lyrico-lyrics-wrapper">na prichi menje mattere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na prichi menje mattere"/>
</div>
<div class="lyrico-lyrics-wrapper">kekkeruthuku nee yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekkeruthuku nee yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pearru keattu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pearru keattu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">naa than sivaji the boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa than sivaji the boss"/>
</div>
<div class="lyrico-lyrics-wrapper">veetukulleh adaichi vaipen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukulleh adaichi vaipen "/>
</div>
<div class="lyrico-lyrics-wrapper">like im a bigg boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like im a bigg boss"/>
</div>
<div class="lyrico-lyrics-wrapper">gilli getheh paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gilli getheh paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ambasador car ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambasador car ru"/>
</div>
<div class="lyrico-lyrics-wrapper">macha nambe styleh sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="macha nambe styleh sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">nambe paasangeh yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambe paasangeh yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">i am sharp pu like knife fe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i am sharp pu like knife fe"/>
</div>
<div class="lyrico-lyrics-wrapper">saiytan cycle life fe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saiytan cycle life fe"/>
</div>
<div class="lyrico-lyrics-wrapper">naa jallikattu kaaleh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa jallikattu kaaleh"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jatti potta miche eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jatti potta miche eh"/>
</div>
<div class="lyrico-lyrics-wrapper">naangella gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangella gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">you dont be silly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you dont be silly"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongurae singatthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongurae singatthae"/>
</div>
<div class="lyrico-lyrics-wrapper">thatti yezhuppittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatti yezhuppittae"/>
</div>
<div class="lyrico-lyrics-wrapper">mavanae yaarukittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavanae yaarukittae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">khann thoranthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khann thoranthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">thakke thaaru maaru uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakke thaaru maaru uh"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthathinggeh yaaru daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthathinggeh yaaru daa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil dhilliruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil dhilliruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">mutti mutti mothanunmda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutti mutti mothanunmda"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhthakoodae vithai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhthakoodae vithai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">naamae veezhanumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamae veezhanumda"/>
</div>
<div class="lyrico-lyrics-wrapper">haters othungi nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haters othungi nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">losers othungi nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="losers othungi nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">players only dhillirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="players only dhillirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu munnae nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu munnae nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uh"/>
</div>
<div class="lyrico-lyrics-wrapper">ya all know im a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ya all know im a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">mess around cut your head boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mess around cut your head boy"/>
</div>
<div class="lyrico-lyrics-wrapper">i ain't kidding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i ain't kidding"/>
</div>
<div class="lyrico-lyrics-wrapper">pop pop pop pop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop pop pop pop"/>
</div>
<div class="lyrico-lyrics-wrapper">bunny boy killin it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bunny boy killin it"/>
</div>
<div class="lyrico-lyrics-wrapper">oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu vervai illeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu vervai illeh"/>
</div>
<div class="lyrico-lyrics-wrapper">rathem ada sagaleh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathem ada sagaleh"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiikka nenaikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiikka nenaikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">makke ennudayeh kanaveh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makke ennudayeh kanaveh"/>
</div>
<div class="lyrico-lyrics-wrapper">kai vassam irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai vassam irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ulukkulleh thiramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulukkulleh thiramai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagey aandidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagey aandidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">konjamirruntha porumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjamirruntha porumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naangella gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangella gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">you dont be silly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you dont be silly"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongurae singatthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongurae singatthae"/>
</div>
<div class="lyrico-lyrics-wrapper">thaati yezhuppittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaati yezhuppittae"/>
</div>
<div class="lyrico-lyrics-wrapper">mavanae yaarukittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavanae yaarukittae"/>
</div>
</pre>
