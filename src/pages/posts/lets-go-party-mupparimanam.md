---
title: "lets go party song lyrics"
album: "Mupparimanam"
artist: "G V Prakash Kumar"
lyricist: "Na Muthu Kumar"
director: "Adhiroopan"
path: "/albums/mupparimanam-lyrics"
song: "Lets Go Party"
image: ../../images/albumart/mupparimanam.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nA81IDbIhFw"
type: "happy"
singers:
  - GV Prakash Kumar
  - Varun Parandhaman
  - MC Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullaasamai urchaagamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasamai urchaagamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaada da indha ulagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaada da indha ulagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen pookkalai un pookkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen pookkalai un pookkalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikorkkava kannadi azhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikorkkava kannadi azhagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam venam machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam venam machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerum venam machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerum venam machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneril vanna vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneril vanna vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Meengal pola vazhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meengal pola vazhuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen poovai kayyil allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovai kayyil allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perinbam kodi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perinbam kodi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullasam vazhvai vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullasam vazhvai vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum uchcham thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum uchcham thoduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va da machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macha en new year resolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha en new year resolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama sarakku adikka maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama sarakku adikka maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">En new year resolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En new year resolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama dham adikka maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama dham adikka maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudigaaran pechu vidinjaa pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudigaaran pechu vidinjaa pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">En figure moonji marandhae pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En figure moonji marandhae pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan kudutha kaasu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan kudutha kaasu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakkaa maari ulla pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakkaa maari ulla pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naetrodu sogam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetrodu sogam "/>
</div>
<div class="lyrico-lyrics-wrapper">odiponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odiponathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrodu life style 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrodu life style "/>
</div>
<div class="lyrico-lyrics-wrapper">maari ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari ponathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil undu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil undu "/>
</div>
<div class="lyrico-lyrics-wrapper">AB positive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AB positive"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">endrum B positive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum B positive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tai katti povom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tai katti povom "/>
</div>
<div class="lyrico-lyrics-wrapper">tidal parkuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tidal parkuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai katti endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai katti endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhmattom than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhmattom than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam engal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam engal "/>
</div>
<div class="lyrico-lyrics-wrapper">ellai koduthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai koduthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaattam pottaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaattam pottaa "/>
</div>
<div class="lyrico-lyrics-wrapper">sakka poduthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakka poduthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy enjoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy enjoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Va va va va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va va va va va va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va da machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu pesum angel poova nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu pesum angel poova nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannale pesum oonjal theeva nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannale pesum oonjal theeva nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Android-il neethan attai padamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Android-il neethan attai padamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un kaadhal vittu vidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un kaadhal vittu vidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen indri ulagil bodhai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen indri ulagil bodhai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen indri ulagil yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen indri ulagil yedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkana ponna catch pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkana ponna catch pannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaloda life -ah match pannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda life -ah match pannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy enjoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy enjoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Va va va va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va va va va va va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va da machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasamai urchaagamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasamai urchaagamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaada da indha ulagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaada da indha ulagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen pookkalai un pookkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen pookkalai un pookkalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikorkkava kannadi azhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikorkkava kannadi azhagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyanam venam machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam venam machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerum venam machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerum venam machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneril vanna vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneril vanna vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Meengal pola vazhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meengal pola vazhuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen poovai kayyil allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen poovai kayyil allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perinbam kodi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perinbam kodi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullasam vazhvai vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullasam vazhvai vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum uchcham thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum uchcham thoduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va da machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va da machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go party he he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go party he he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go partyhe he
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go partyhe he"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wish you a happy new yea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wish you a happy new yea"/>
</div>
</pre>
