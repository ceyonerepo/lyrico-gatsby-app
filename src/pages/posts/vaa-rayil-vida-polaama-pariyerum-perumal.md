---
title: "vaa rayil vida polaama song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Vaa Rayil Vida Polaama"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NChfa1rmknk"
type: "happy"
singers:
  - Prithika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna kovama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna kovama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manichidu polam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manichidu polam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen etti ninnu paakkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen etti ninnu paakkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pudichu povomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai pudichu povomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu pola polam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu pola polam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai pudikkum enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pudikkum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku naan kaaya pazhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku naan kaaya pazhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee variya nesama sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee variya nesama sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna kovama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna kovama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manichidu polam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manichidu polam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga naan thaniya irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga naan thaniya irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukkunu theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukkunu theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum thaan thaniya irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum thaan thaniya irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu unna parkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu unna parkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osathi korachal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osathi korachal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo enakku athu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo enakku athu puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhukka karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukka karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo enakku athu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo enakku athu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum vera naanum vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vera naanum vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma appa sollitaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma appa sollitaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambaama naan odi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambaama naan odi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga unnai kaanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga unnai kaanum inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa sirichi vilayada polamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa sirichi vilayada polamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pudichu povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai pudichu povomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu pola polam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu pola polam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa sirichi vilayada polamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa sirichi vilayada polamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pudichu povomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai pudichu povomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu pola polam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu pola polam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa rayil vida polaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa rayil vida polaam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa rayil vida polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa rayil vida polaama"/>
</div>
</pre>
