---
title: "alai alai song lyrics"
album: "Evanukku Engeyo Matcham Irukku"
artist: "Natarajan Sankaran"
lyricist: "Viveka"
director: "A R Mukesh"
path: "/albums/evanukku-engeyo-matcham-irukku-lyrics"
song: "Alai Alai"
image: ../../images/albumart/evanukku-engeyo-matcham-irukku.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_qPcwHD7o94"
type: "love"
singers:
  - Deepak
  - Keerthana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharai melae pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai melae pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naanum paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naanum paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil thaen alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil thaen alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valai valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virithaai kaadhal valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virithaai kaadhal valai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salai melae niththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salai melae niththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindra vaaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindra vaaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham ittu kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham ittu kondae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum poi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum poi varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaganamum saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaganamum saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Polae vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polae vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi poongkaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi poongkaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil thaen alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil thaen alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthaal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pesudhu thaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pesudhu thaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavil udal idaiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavil udal idaiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mithakindren naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithakindren naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thames nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thames nadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendralodu pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendralodu pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">London paalam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="London paalam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttri odalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri odalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakesphere-in poem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakesphere-in poem"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi paadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi paadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil thaen alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil thaen alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un poigal un mel nirkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un poigal un mel nirkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaigal yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaigal yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann mai nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann mai nee "/>
</div>
<div class="lyrico-lyrics-wrapper">mitcham vaiththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mitcham vaiththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaar megam ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaar megam ketkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandam vittu kandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandam vittu kandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum paravaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum paravaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalil rekkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil rekkai "/>
</div>
<div class="lyrico-lyrics-wrapper">maatti kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatti kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillil innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillil innum "/>
</div>
<div class="lyrico-lyrics-wrapper">vannam serkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam serkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil thaen alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil thaen alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey valai valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey valai valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai valai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai valai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ole ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ole ole ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ole ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ole ooo"/>
</div>
</pre>