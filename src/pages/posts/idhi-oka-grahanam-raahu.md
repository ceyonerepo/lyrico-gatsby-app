---
title: "idhi oka grahanam song lyrics"
album: "Raahu"
artist: "Praveen Lakkaraju"
lyricist: "Subbu Vedula"
director: "Subbu Vedula"
path: "/albums/raahu-lyrics"
song: "Idhi Oka Grahanam"
image: ../../images/albumart/raahu.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/exlSPNGJveQ"
type: "mass"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhi Oka Grahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Oka Grahanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhika Sharanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhika Sharanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akhari Charanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akhari Charanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruva Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruva Maranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhi Oka Grahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Oka Grahanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhika Sharanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhika Sharanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akhari Charanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akhari Charanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruva Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruva Maranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaga Bhaga Bhanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhaga Bhanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirassu Kori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirassu Kori"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara Kara Rahuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara Kara Rahuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koralu Sachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koralu Sachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Janame Jayudae Vachaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janame Jayudae Vachaena"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahuni Samidhaga Icchena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahuni Samidhaga Icchena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandae Nippuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandae Nippuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mingae Raahuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mingae Raahuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijame Nippai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijame Nippai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalchenu Ayuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalchenu Ayuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Garala Kanthudae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garala Kanthudae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacheyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacheyna"/>
</div>
<div class="lyrico-lyrics-wrapper">Karala Krithyam Apaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karala Krithyam Apaena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhi Oka Grahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Oka Grahanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhika Sharanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhika Sharanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akhari Charanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akhari Charanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruva Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruva Maranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marana Mrudhangam Mogindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana Mrudhangam Mogindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarana Homam Migulindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarana Homam Migulindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">AshruBindhuvulu Asthralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AshruBindhuvulu Asthralai"/>
</div>
<div class="lyrico-lyrics-wrapper">AKhari Yudham Sagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AKhari Yudham Sagalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">saralalanni Sandhinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saralalanni Sandhinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hala Halanni Madhiyinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hala Halanni Madhiyinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthima Vijayam Rakunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthima Vijayam Rakunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera maraname Ika Minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera maraname Ika Minna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera maraname Ika Minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera maraname Ika Minna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhi Oka Grahanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Oka Grahanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhika Sharanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhika Sharanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Akhari Charanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akhari Charanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruva Maranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruva Maranam"/>
</div>
</pre>
