---
title: "party vandhale song lyrics"
album: "Vellore Maavattam"
artist: "Sundar C Babu"
lyricist: "Ezhil Arasu"
director: "R.N.R. Manohar"
path: "/albums/vellore-maavattam-lyrics"
song: "Party Vandhale"
image: ../../images/albumart/vellore-maavattam.jpg
date: 2011-10-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lUx6ir6Stuo"
type: "happy"
singers:
  - Shaan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paartti vandhaaley poothaaley en munnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartti vandhaaley poothaaley en munnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">lootti pannaadhey ennaalum ava pinnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lootti pannaadhey ennaalum ava pinnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Trafic jaamaagum pennaaley iva kannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trafic jaamaagum pennaaley iva kannaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">paartti kondaalum endrumey thappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paartti kondaalum endrumey thappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">panbaattai konjam norukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbaattai konjam norukku"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu paruvathaal vandha kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu paruvathaal vandha kirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">thaagathai thandha neruppey en anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaagathai thandha neruppey en anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">megathai katti izhuppaai un vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathai katti izhuppaai un vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinungum kolusil sidharum manasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungum kolusil sidharum manasai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil thaangi pidippaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil thaangi pidippaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulungum idaiyil thudikkum vayasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulungum idaiyil thudikkum vayasai"/>
</div>
<div class="lyrico-lyrics-wrapper">endru saandhi seivaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru saandhi seivaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">narambil therikkum neruppin analai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambil therikkum neruppin analai"/>
</div>
<div class="lyrico-lyrics-wrapper">udhadaal konjam anaippaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhadaal konjam anaippaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iravin nadhiyil midhappoam naamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravin nadhiyil midhappoam naamey"/>
</div>
<div class="lyrico-lyrics-wrapper">theppam endru aavaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theppam endru aavaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalaaga thegam moagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalaaga thegam moagam"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyaada theerum thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyaada theerum thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">udalinga kadalaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalinga kadalaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">uppukku badhilaai sarkkarai aavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppukku badhilaai sarkkarai aavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartti vandhaaley poothaaley en munnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartti vandhaaley poothaaley en munnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">lootti pannaadhey ennaalum ava pinnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lootti pannaadhey ennaalum ava pinnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Trafic jaamaagum pennaaley iva kannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trafic jaamaagum pennaaley iva kannaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">paartti kondaalum endrumey thappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paartti kondaalum endrumey thappillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi thaan pagalin pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi thaan pagalin pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">indrum maatri amaippoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrum maatri amaippoamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">meithaan meithaan iravin varavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meithaan meithaan iravin varavu"/>
</div>
<div class="lyrico-lyrics-wrapper">endru ezhudhi padippoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru ezhudhi padippoamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">meithaan meithaan ulagai iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meithaan meithaan ulagai iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">inba padaiyal padaippomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inba padaiyal padaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mailvaan mailvaan undhan koondhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mailvaan mailvaan undhan koondhal"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil virithum paduppoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil virithum paduppoamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kacheri innum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kacheri innum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidindhaaley selai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidindhaaley selai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">sorkkathin kadhavai thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorkkathin kadhavai thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thullum kannaaley ennai kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thullum kannaaley ennai kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartti vandhaaley poothaaley en munnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartti vandhaaley poothaaley en munnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">lootti pannaadhey ennaalum ava pinnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lootti pannaadhey ennaalum ava pinnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Trafic jaamaagum pennaaley iva kannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trafic jaamaagum pennaaley iva kannaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">paartti kondaalum endrumey thappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paartti kondaalum endrumey thappillai"/>
</div>
</pre>
