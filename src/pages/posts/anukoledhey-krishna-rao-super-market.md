---
title: "anukoledhey song lyrics"
album: "Krishna Rao Super Market"
artist: "Bhole Shavali"
lyricist: "Sai Siri"
director: "Sreenath Pulakuram"
path: "/albums/krishna-rao-super-market-lyrics"
song: "Anukoledhey"
image: ../../images/albumart/krishna-rao-super-market.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LEJdWUOsPDo"
type: "love"
singers:
  - Hymath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">anukolede ee vinthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukolede ee vinthane"/>
</div>
<div class="lyrico-lyrics-wrapper">na southamenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na southamenani"/>
</div>
<div class="lyrico-lyrics-wrapper">dooranni paaresi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooranni paaresi "/>
</div>
<div class="lyrico-lyrics-wrapper">theeraani thaakaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraani thaakaane"/>
</div>
<div class="lyrico-lyrics-wrapper">maatanlni vetaadi ee ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatanlni vetaadi ee ee"/>
</div>
<div class="lyrico-lyrics-wrapper">mounaani galichaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounaani galichaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenantoo lenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenantoo lenu"/>
</div>
<div class="lyrico-lyrics-wrapper">innallu eeroje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innallu eeroje"/>
</div>
<div class="lyrico-lyrics-wrapper">unnattu a roju raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnattu a roju raani"/>
</div>
<div class="lyrico-lyrics-wrapper">ee roju prathi roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee roju prathi roju"/>
</div>
<div class="lyrico-lyrics-wrapper">unnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dooraani paaresi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooraani paaresi"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraanni thaakaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraanni thaakaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anukolede ee vinthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukolede ee vinthane"/>
</div>
<div class="lyrico-lyrics-wrapper">na southamenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na southamenani"/>
</div>
<div class="lyrico-lyrics-wrapper">dooranni paaresi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooranni paaresi "/>
</div>
<div class="lyrico-lyrics-wrapper">theeraani thaakaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraani thaakaane"/>
</div>
<div class="lyrico-lyrics-wrapper">mounaani vadilesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounaani vadilesi"/>
</div>
<div class="lyrico-lyrics-wrapper">megamgaa maaraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megamgaa maaraane"/>
</div>
</pre>
