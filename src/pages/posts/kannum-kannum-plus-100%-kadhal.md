---
title: "kannum kannum plus song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Kannum Kannum Plus"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eMLVooMkDsY"
type: "love"
singers:
  - G.V Prakash Kumar
  - Maalavika Sundar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannum Kannum Plusse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Plusse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime Illa Minuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime Illa Minuse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Minus Serndha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Minus Serndha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Equation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Equation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">X um Y um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="X um Y um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mix Aagi Into Pola Cross Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mix Aagi Into Pola Cross Aagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Link Aana Sink Aana Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Link Aana Sink Aana Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Infactuation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Infactuation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nano Vaa Ada Nee Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nano Vaa Ada Nee Sirichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speedaaga En Manam Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedaaga En Manam Parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Data Ada En Data
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Data Ada En Data"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera Theera Pesavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera Theera Pesavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Plussey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Plussey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime Illa Minuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime Illa Minuse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Minus Serndha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Minus Serndha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Equation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Equation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">DNA Varaipadathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="DNA Varaipadathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Photo Thaan Therigirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Photo Thaan Therigirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neutron Electron Un Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neutron Electron Un Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neutral Aache En Idhaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neutral Aache En Idhaiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Centigradum Thoolaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Centigradum Thoolaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththa Veppaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththa Veppaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fareheatum Paazhaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fareheatum Paazhaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Ushnaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Ushnaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Newtonin Muzhu Vidhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Newtonin Muzhu Vidhi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Konjum Muzhu Madhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konjum Muzhu Madhi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann Nokki Vanthaalo Gravitation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Nokki Vanthaalo Gravitation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Nokki Veezhthaalo Thaan Infactuation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Nokki Veezhthaalo Thaan Infactuation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Poga Puriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Puriyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaal Manam Idhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Manam Idhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verukkalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darwin Sonna Theory Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darwin Sonna Theory Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One By One Nadakkirathet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One By One Nadakkirathet"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hormone Sollum Hi Bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hormone Sollum Hi Bye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling Varugiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Varugiradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhaiyum Thaandi Yedhyedhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaiyum Thaandi Yedhyedhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerppil Varugiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerppil Varugiradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Sparisam Pudhiraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Sparisam Pudhiraagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru Sparisam Badhil Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Sparisam Badhil Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Numberai Onnaakum Calculation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numberai Onnaakum Calculation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hormonegal Onnakkume Infactuation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hormonegal Onnakkume Infactuation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Plusse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Plusse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime Illa Minuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime Illa Minuse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Minus Serndha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Minus Serndha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Equation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Equation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">X um Y um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="X um Y um"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mix Aagi Into Pola Cross Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mix Aagi Into Pola Cross Aagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Link Aana Sink Aana Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Link Aana Sink Aana Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Infactuation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Infactuation"/>
</div>
</pre>
