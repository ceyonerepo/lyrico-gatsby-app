---
title: "rang bhariya song lyrics"
album: "Darbaan"
artist: "Amartya Bobo Rahut"
lyricist: "Siddhant Kaushal"
director: "Bipin Nadkarni"
path: "/albums/darbaan-lyrics"
song: "Rang Bhariya"
image: ../../images/albumart/darbaan.jpg
date: 2020-12-04
lang: hindi
youtubeLink: "https://www.youtube.com/embed/8-biX0l5lyk"
type: "happy"
singers:
  - Gujraj Singh
  - Amrita Singh
  - Amartya Bobo Rahut
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmi Nimmi Gunguni Si
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmi Nimmi Gunguni Si"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghooni Dhoop Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghooni Dhoop Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Rothi Rothi Kiyario Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rothi Rothi Kiyario Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Naye Roop Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naye Roop Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Le Raha Udaan Dil Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le Raha Udaan Dil Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bade Sauk Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade Sauk Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyi Jo Ye Zindagi Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyi Jo Ye Zindagi Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka Chaund Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka Chaund Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamke Chaubare Saare Galiyaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamke Chaubare Saare Galiyaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagta Utre Hai Ambar Se Sab Taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagta Utre Hai Ambar Se Sab Taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Milan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Milan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Lagan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Lagan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaukhat Pe Lamho Ne Naye Dedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaukhat Pe Lamho Ne Naye Dedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagaat Jo Bisre Palo Ne Li
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagaat Jo Bisre Palo Ne Li"/>
</div>
    <div class="lyrico-lyrics-wrapper">Boojha Halka Ab Hai Kal Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boojha Halka Ab Hai Kal Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Khushiyoo Ka Jo Aangan Me Laga Khema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khushiyoo Ka Jo Aangan Me Laga Khema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bajte Nagaade Ham Jaaye Baare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajte Nagaade Ham Jaaye Baare"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagta Utre Hai Ambar Se Sab Taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagta Utre Hai Ambar Se Sab Taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Milan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Milan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Lagan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Lagan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajte Nagaade Ham Jaaye Baare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajte Nagaade Ham Jaaye Baare"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagta Utre Hai Ambar Se Sab Taare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagta Utre Hai Ambar Se Sab Taare"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Milan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Milan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nore Paak SajAn Tore Karke Lagan Ye Hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nore Paak SajAn Tore Karke Lagan Ye Hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Rang Bhariya…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rang Bhariya…"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuna Kinare Mora Gau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuna Kinare Mora Gau"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavre Aayi Jaiyo Saavre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavre Aayi Jaiyo Saavre"/>
</div>
</pre>
