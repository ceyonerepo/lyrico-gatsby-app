---
title: "aathadi song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Aathadi"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8ydPi2viWFE"
type: "happy"
singers:
  - Hiphop Tamizha
  - V.M. Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aathadi Enna Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anganga Pachcha Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anganga Pachcha Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iymponnaal Senja Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iymponnaal Senja Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Adayalaam Intha Thalumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Adayalaam Intha Thalumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Paakkathaanda Local Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Paakkathaanda Local Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkaa International Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa International Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sollu Sollu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sollu Sollu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Sollu Sollu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Sollu Sollu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Paakkathaanda Local Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Paakkathaanda Local Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkaa International Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaa International Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallu Thallu Thallu Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallu Thallu Thallu Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thallu Thallu Thallu Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallu Thallu Thallu Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kathalil Le Tholvi Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kathalil Le Tholvi Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thol Kudukkum Natpu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukkum Natpu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ponnukkaaga Nanbanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponnukkaaga Nanbanatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalatti Vitta Thappu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalatti Vitta Thappu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summaave Area la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaave Area la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Konjam Geththu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Konjam Geththu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Natpa Paththi Thappa Sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Natpa Paththi Thappa Sonnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vai Mela Kuththu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vai Mela Kuththu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nabanukku Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nabanukku Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu inai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu inai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nabanukku Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nabanukku Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu inai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu inai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saave Vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saave Vanthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nanban Pakkam Nippen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanban Pakkam Nippen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemane Vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemane Vanthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Mela Kaiya Veppen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Mela Kaiya Veppen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbana Pagachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbana Pagachikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Enakku Ethiri Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Enakku Ethiri Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Scene Na Pottavan Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Na Pottavan Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda Poraan Sethari Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Poraan Sethari Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonthakkaaran Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthakkaaran Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edaththa Gaali Pannu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaththa Gaali Pannu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpukku Katta Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukku Katta Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Kovil Onnu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kovil Onnu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nabanukku Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nabanukku Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu inai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu inai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Thunai Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai Natpe Thunai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nabanukku Evanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nabanukku Evanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu inai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu inai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leftu Leftu Leftu Leftu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leftu Leftu Leftu Leftu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leftu Leftu Leftu Leftu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leftu Leftu Leftu Leftu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rightu Rightu Rightu Rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightu Rightu Rightu Rightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rightu Rightu Rightu Rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightu Rightu Rightu Rightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Straightu Straightu Straightu Straightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straightu Straightu Straightu Straightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Straightu Straightu Straightu Straightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straightu Straightu Straightu Straightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adra Adra Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Adra Adra Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithaan En Gangu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithaan En Gangu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Gangu Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Gangu Kuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthu Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthu Aada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottom Intha Songu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottom Intha Songu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anganga Pachcha Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anganga Pachcha Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iymponnaal Senja Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iymponnaal Senja Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Adayalaam Intha Thalumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Adayalaam Intha Thalumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Annan Thangam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Annan Thangam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prechchana Na Vanthu Puttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prechchana Na Vanthu Puttaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bangam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Annan Thangam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Annan Thangam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prechchana Na Vanthu Puttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prechchana Na Vanthu Puttaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bangam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidatha Da  Vidatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidatha Da  Vidatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prabakara Prabakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prabakara Prabakara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidatha Da  Vidatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidatha Da  Vidatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prabakara Prabakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prabakara Prabakara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leftu Leftu Leftu Leftu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leftu Leftu Leftu Leftu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leftu Leftu Leftu Leftu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leftu Leftu Leftu Leftu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rightu Rightu Rightu Rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightu Rightu Rightu Rightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rightu Rightu Rightu Rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightu Rightu Rightu Rightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Straightu Straightu Straightu Straightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straightu Straightu Straightu Straightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Straightu Straightu Straightu Straightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straightu Straightu Straightu Straightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adra Adra Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Adra Adra Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anganga Pachcha Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anganga Pachcha Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iymponnaal Senja Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iymponnaal Senja Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Adayalaam Intha Thalumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Adayalaam Intha Thalumbu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Enna Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Enna Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anganga Pachcha Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anganga Pachcha Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iymponnaal Senja Odambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iymponnaal Senja Odambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Adayalaam Intha Thalumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Adayalaam Intha Thalumbu"/>
</div>
</pre>
