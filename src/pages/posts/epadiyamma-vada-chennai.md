---
title: "epadiyamma song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Sindhai Rev Ravi"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Epadiyamma"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wRd2GkwFDZg"
type: "sad"
singers:
  - Sindhai Rev Ravi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Epadiyamma marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumuruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumuruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maaveeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maaveeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu ponadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epadiyamma marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumuruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumuruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maaveeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maaveeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu ponadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi ellaam nadakkumendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi ellaam nadakkumendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Therindhirunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhirunthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aaaa haaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aaaa haaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi ellaam nadakkumendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi ellaam nadakkumendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Therindhirunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhirunthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha iraivanidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha iraivanidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalellaam kettiruppomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalellaam kettiruppomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nichayamilla vaazhkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamilla vaazhkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thangamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani thangamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamilla vaazhkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamilla vaazhkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thangamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani thangamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha oorinilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha oorinilae "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhu vanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhu vanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyar tharamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyar tharamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha oorinilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha oorinilae "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhndhu vanthaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhndhu vanthaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyar tharamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyar tharamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithai epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithai epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumuruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumuruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maaveeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maaveeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu ponadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaiyitta karumbaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaiyitta karumbaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasaindhaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasaindhaar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa haaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa haaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaiyitta karumbaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaiyitta karumbaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasaindhaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasaindhaar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maalaiyitta manaivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maalaiyitta manaivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru kalanguraar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru kalanguraar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannanillaa mangaiyarku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannanillaa mangaiyarku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal sondhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal sondhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannanillaa mangaiyarku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannanillaa mangaiyarku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal sondhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal sondhama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbu kanavarilla penmanikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu kanavarilla penmanikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovum sondhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovum sondhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu kanavarillaa penmanikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu kanavarillaa penmanikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottum sondhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottum sondhama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithai epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithai epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumuruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumuruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maaveeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maaveeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu ponadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavillamal neelavaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavillamal neelavaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thondruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham thondruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa haaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa haaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavillaamal neelavaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavillaamal neelavaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham thondruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham thondruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaveeran neeyillaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaveeran neeyillaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudumbathilae nimmadhi kidaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudumbathilae nimmadhi kidaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai pirindha sondhangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindha sondhangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanginaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanginaar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirindha sondhangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindha sondhangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanginaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanginaar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parikodutha nanbarellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parikodutha nanbarellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadinaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadinaar aiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai parikodutha nanbarellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parikodutha nanbarellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadinaar aiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadinaar aiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithai epadiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithai epadiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumuruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumuruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maaveeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maaveeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraindhu Pondhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraindhu Pondhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epadiyamma marakka mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epadiyamma marakka mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kumurudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kumurudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal arumai annan maraindhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal arumai annan maraindhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga mudiyalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga mudiyalae"/>
</div>
</pre>
