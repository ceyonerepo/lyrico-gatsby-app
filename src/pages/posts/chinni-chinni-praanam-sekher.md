---
title: "chinni chinni praanam song lyrics"
album: "Sekher"
artist: "Anup Rubens"
lyricist: "Ramajogayya Sastry"
director: "Jeevitha Rajasekhar"
path: "/albums/sekher-lyrics"
song: "Chinni Chinni Praanam"
image: ../../images/albumart/sekher.jpg
date: 2022-05-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J7LUOOL--54"
type: "happy"
singers:
  - Chinmayi
  - Hymath Mohammed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinni chinni praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhulu ade paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhulu ade paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kosam nuvvu panchena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kosam nuvvu panchena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini nalla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nalla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi raani gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi raani gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Istam ga nelaki vochena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam ga nelaki vochena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nene thanu ayi viche gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene thanu ayi viche gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Paade jo laali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade jo laali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali kaali kannulo na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali kaali kannulo na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalala diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalala diwali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhulu ade paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhulu ade paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kosam nuvvu panchena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kosam nuvvu panchena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini nalla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nalla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi raani gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi raani gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Istam ga nelaki vochena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam ga nelaki vochena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachani gudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachani gudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechani thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechani thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhu podichaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhu podichaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha suridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha suridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nindu nattinta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindu nattinta"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu bangaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu bangaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha baagundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha baagundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevitham nedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevitham nedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthe inthe chalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe inthe chalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila unte chalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila unte chalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na cheyi pattinayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na cheyi pattinayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti vellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna monna lene leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna lene leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha swaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha swaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri kori nanne chere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri kori nanne chere"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno varalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno varalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhulu ade paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhulu ade paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kosam nuvvu panchena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kosam nuvvu panchena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini nalla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nalla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi raani gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi raani gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Istam ga nelaki vochena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam ga nelaki vochena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasede paadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasede paadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muvanai nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muvanai nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gallu mantondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gallu mantondhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde sadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde sadule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parekene anchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parekene anchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravalla lone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravalla lone"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasincheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasincheti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana ni ayyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana ni ayyale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo lede ana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo lede ana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedhu badha ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedhu badha ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Marepinchi murepestundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marepinchi murepestundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee prapancham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangu rangu jathaga cherchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rangu jathaga cherchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gesa rangoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gesa rangoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju puse roja puvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju puse roja puvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam navali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam navali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhulu ade paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhulu ade paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kosam nuvvu panchena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kosam nuvvu panchena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini nalla kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nalla kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi raani gaganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi raani gaganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Istam ga nelaki vochena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istam ga nelaki vochena"/>
</div>
</pre>
