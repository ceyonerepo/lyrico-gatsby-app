---
title: "private party song lyrics"
album: "Don"
artist: "Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Cibi Chakaravarthi"
path: "/albums/don-lyrics"
song: "Private Party"
image: ../../images/albumart/don.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/paDG3S3UmQM"
type: "happy"
singers:
  - Anirudh Ravichander
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Modern Radhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Radhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pickup-u Pannidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pickup-u Pannidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakkum Nadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkum Nadhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Spottaiyum Kaattidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Spottaiyum Kaattidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On The Way Il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On The Way Il"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Friendsayum Koopidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Friendsayum Koopidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Fun Moodil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Fun Moodil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dance Sayum Pottidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dance Sayum Pottidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jolly Ya Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Ya Nee Vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Private Party Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Private Party Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj Ellaam Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj Ellaam Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom Attrocity Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom Attrocity Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaaz Music Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaz Music Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Namma Beat Tu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Namma Beat Tu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bouncer Ellaam Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bouncer Ellaam Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Little Heart Tu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Little Heart Tu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Va Thandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Va Thandhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Cute Aana Dance Aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Cute Aana Dance Aale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada Vachaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Vachaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Touch Aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Touch Aale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo High Volt Tu Switchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo High Volt Tu Switchaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darling Prince
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling Prince"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pickup Pu Pannidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pickup Pu Pannidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Feel Song Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Feel Song Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Soundaiyum Yethida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Soundaiyum Yethida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Konjala Paathidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjala Paathidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pora Pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyaiyum Korthidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyaiyum Korthidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jolly Ya Naan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Ya Naan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Private Partydhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Private Partydhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom Atrocity Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom Atrocity Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disco Ellaam Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disco Ellaam Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Kuthu Dance Su Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Kuthu Dance Su Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Song Gu Dedicated To
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Song Gu Dedicated To"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Fans Su Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Fans Su Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Machaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Machaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Vachaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Vachaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Vu Touch Aalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Vu Touch Aalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kannaala Katchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kannaala Katchaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambu Vittaachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambu Vittaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi Set Aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodi Set Aachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Thottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit Aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit Aachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Naan Parandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Naan Parandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Enna Naanae Marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Enna Naanae Marandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi Naan Nadandhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi Naan Nadandhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Siruppula Vizhundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Siruppula Vizhundhaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Naan Tholanchaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Naan Tholanchaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulla Thedi Alanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulla Thedi Alanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mutham Onnu Kedachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mutham Onnu Kedachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jolly Ya Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Ya Nee Vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Private Party Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Private Party Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj Ellaam Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj Ellaam Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivom Attrocity Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivom Attrocity Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaaz Music Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaz Music Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Namma Beat Tu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Namma Beat Tu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bouncer Ellaam Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bouncer Ellaam Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Little Heart Tu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Little Heart Tu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Va Thandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Va Thandhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Cute Aana Dance Aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Cute Aana Dance Aale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada Vachaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Vachaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Touch Aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Touch Aale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo High Volt 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo High Volt "/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Switchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Switchaane"/>
</div>
</pre>
