---
title: "aadadaa karutha paiyaa song lyrics"
album: "Semmari Aadu"
artist: "Renjith Vasudev"
lyricist: "unknown"
director: "Sathish Subramaniam"
path: "/albums/semmari-aadu-song-lyrics"
song: "Aadadaa Karutha Paiyaa"
image: ../../images/albumart/semmari-aadu.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EQxmYY09Y_0"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adada karuththa paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada karuththa paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai nee paakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai nee paakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">paakama paakuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakama paakuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">pesaama pesuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesaama pesuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">semmari aadai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semmari aadai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli oodura enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli oodura enai"/>
</div>
<div class="lyrico-lyrics-wrapper">thookanan kuruvi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookanan kuruvi "/>
</div>
<div class="lyrico-lyrics-wrapper">thonguthu paaru naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thonguthu paaru naa"/>
</div>
<div class="lyrico-lyrics-wrapper">machan en manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiya paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiya paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">yengi thaan thavikiren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi thaan thavikiren da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seema theru veethiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seema theru veethiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">unna naan paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna naan paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela aasa vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela aasa vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaan thudikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan thudikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidala pulla vitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidala pulla vitha"/>
</div>
<div class="lyrico-lyrics-wrapper">vithachu koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithachu koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu sernthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu sernthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nee nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nee nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">putta podi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putta podi pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna matum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna matum than"/>
</div>
<div class="lyrico-lyrics-wrapper">nenachu iruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenachu iruken"/>
</div>
<div class="lyrico-lyrics-wrapper">enna mattum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mattum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaikalana usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaikalana usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda sontham illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda sontham illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna nee nenaikatha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nee nenaikatha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala paathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala paathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kollatha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollatha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye sevatha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye sevatha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiya paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiya paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">paakathe paakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakathe paakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna neyum paakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna neyum paakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">otha adi paathaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha adi paathaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">porayila theriyaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porayila theriyaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">thana thana thanna thanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thana thanna thanna"/>
</div>
<div class="lyrico-lyrics-wrapper">thaanee thaane naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanee thaane naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">machan en manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiya paakura da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiya paakura da"/>
</div>
<div class="lyrico-lyrics-wrapper">yengi thaan thavikiren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi thaan thavikiren da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pacha mannu nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha mannu nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">paana seiya pakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paana seiya pakura"/>
</div>
<div class="lyrico-lyrics-wrapper">sutta mannu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutta mannu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">naan suthi thiriyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan suthi thiriyuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannala pathu thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala pathu thane"/>
</div>
<div class="lyrico-lyrics-wrapper">enna neyum kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna neyum kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">kathala sollama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathala sollama than"/>
</div>
<div class="lyrico-lyrics-wrapper">yenga vaikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenga vaikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasukulla enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukulla enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pooti vachu un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooti vachu un"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukulla enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukulla enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pooti vachu nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooti vachu nee "/>
</div>
<div class="lyrico-lyrics-wrapper">thane enna kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane enna kollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mathura kaara machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathura kaara machan"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seiya paakura da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seiya paakura da"/>
</div>
<div class="lyrico-lyrics-wrapper">yengi thavikiren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi thavikiren da"/>
</div>
</pre>