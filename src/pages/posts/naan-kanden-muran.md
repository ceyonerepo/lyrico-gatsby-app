---
title: "naan kanden song lyrics"
album: "Muran"
artist: "Sajan Madhav"
lyricist: "Priyan"
director: "Rajan Madhav"
path: "/albums/muran-lyrics"
song: "Naan Kanden"
image: ../../images/albumart/muran.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SaY6ctOdISc"
type: "love"
singers:
  - Ranjith
  - Lionel Messigitrachan Mahadevan
  - Astana Divya Vijay Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan kanden kangal pesum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden kangal pesum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nagaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nagaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam adhu vitu vittu thudikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam adhu vitu vittu thudikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanden netru illaa ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden netru illaa ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendathendru indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendathendru indru "/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalaigal ennaith thaakiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalaigal ennaith thaakiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai nindrum naan nanaikindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai nindrum naan nanaikindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli thuliyaai udaigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli thuliyaai udaigindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragillai naan parakkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragillai naan parakkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai yeno marakkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yeno marakkindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kanden kangal pesum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden kangal pesum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nagaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nagaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam adhu vitu vittu thudikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam adhu vitu vittu thudikkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaai nenjile kaayangal serkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaai nenjile kaayangal serkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaruthal naan aagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaruthal naan aagavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai nee unnaiye yen adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nee unnaiye yen adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanveli naan kaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanveli naan kaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhotharam edhedho sonnaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhotharam edhedho sonnaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan kaanaadha thooram nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan kaanaadha thooram nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochukku kaattraai vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moochukku kaattraai vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En aadharam neeye endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aadharam neeye endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kanden kangal pesum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden kangal pesum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nagaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nagaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam adhu vitu vittu thudikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam adhu vitu vittu thudikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Payanangal poga poga nedum paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanangal poga poga nedum paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum jaalam kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum jaalam kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalai korthup poga nee pakkam vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalai korthup poga nee pakkam vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">En bene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En bene"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravinil naan vizhikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinil naan vizhikirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavinil thaan uranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinil thaan uranginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthathai naan marakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthathai naan marakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">indru pudhithaai pirakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru pudhithaai pirakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kanden kangal pesum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden kangal pesum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nagaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nagaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam adhu vitu vittu thudikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam adhu vitu vittu thudikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden"/>
</div>
</pre>
