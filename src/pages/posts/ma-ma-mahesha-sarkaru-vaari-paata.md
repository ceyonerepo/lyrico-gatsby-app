---
title: "ma ma mahesha song lyrics"
album: "Sarkaru Vaari Paata"
artist: "S Thaman"
lyricist: "Ananta Sriram"
director: "Parasuram"
path: "/albums/sarkaru-vaari-paata-lyrics"
song: "Ma Ma Mahesha"
image: ../../images/albumart/sarkaru-vaari-paata.jpg
date: 2022-05-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-PTf2z3Zf6s"
type: "love"
singers:
  - Sri Krishna
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aye Sannajaji Moora Testha Somavaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Sannajaji Moora Testha Somavaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Mallepoola Moora Testha Mangalaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Mallepoola Moora Testha Mangalaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Banthipoola Moora Testha Budhavaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Banthipoola Moora Testha Budhavaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Guthipoola Moora Testha Guruvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Guthipoola Moora Testha Guruvaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Babu Sukka Malli Mood Shukarvaar Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Babu Sukka Malli Mood Shukarvaar Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Babu Tera Sampathi Mood Shanivaar Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Babu Tera Sampathi Mood Shanivaar Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Aadhivaaram Ollookochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Aadhivaaram Ollookochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Mooral Jello Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Mooral Jello Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadesukomandi Andame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadesukomandi Andame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Sannajaji Moora Testha Somavaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Sannajaji Moora Testha Somavaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Mallepoola Moora Testha Mangalaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Mallepoola Moora Testha Mangalaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Banthipoola Moora Testha Budhavaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Banthipoola Moora Testha Budhavaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Guthipoola Moora Testha Guruvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Guthipoola Moora Testha Guruvaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraa Barampuram Bajaruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraa Barampuram Bajaruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Theraa Gulaabi Moora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theraa Gulaabi Moora"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraa Siripuram Sivaruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraa Siripuram Sivaruke"/>
</div>
<div class="lyrico-lyrics-wrapper">Theraa Chengalva Moora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theraa Chengalva Moora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thilidalo Bisirai Koyi Sirunavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thilidalo Bisirai Koyi Sirunavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Piccheki Pothandoi Lo Lo Bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piccheki Pothandoi Lo Lo Bala"/>
</div>
<div class="lyrico-lyrics-wrapper">Magada Nanu Chudatavem Chaligaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magada Nanu Chudatavem Chaligaalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Matheki Pothandoi Naluvaipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matheki Pothandoi Naluvaipula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Galla Pitte Nee Muddhal Thonindale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galla Pitte Nee Muddhal Thonindale"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathiros Mukku Thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathiros Mukku Thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Galla Patti Na Premantha Gunjey Ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galla Patti Na Premantha Gunjey Ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggette Edo Oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggette Edo Oola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Siggethappa Yegotedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Siggethappa Yegotedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledoi Pokiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledoi Pokiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Mogge Thappa Thaggelaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Mogge Thappa Thaggelaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledi Thimmiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledi Thimmiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Sagguviyo Semiyaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Sagguviyo Semiyaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagga Paalo Chakkiresi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagga Paalo Chakkiresi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Glassu Vatta Raamde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Glassu Vatta Raamde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Ma Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Ma Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Mahesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Mahesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Mu Mu Mu Mu Musthabayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Mu Mu Mu Mu Musthabayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Vachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Vachesa"/>
</div>
</pre>
