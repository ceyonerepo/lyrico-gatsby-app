---
title: "vaazhvomae song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Muthamil"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Vaazhvomae"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O3kWUiLlq2U"
type: "melody"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaalvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvome"/>
</div>
<div class="lyrico-lyrics-wrapper">varaivom valiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaivom valiye"/>
</div>
<div class="lyrico-lyrics-wrapper">meelvom naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meelvom naame"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivo aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivo aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarume"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyo thunive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyo thunive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aarathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiyo kodithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiyo kodithe"/>
</div>
<div class="lyrico-lyrics-wrapper">neer polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer polave"/>
</div>
<div class="lyrico-lyrics-wrapper">nilaika nilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaika nilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">neerile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerile"/>
</div>
<div class="lyrico-lyrics-wrapper">mulaikum vidhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulaikum vidhaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyin kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyin kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanbom naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanbom naame"/>
</div>
<div class="lyrico-lyrics-wrapper">irulil oliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulil oliye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">uravu unake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravu unake"/>
</div>
</pre>
