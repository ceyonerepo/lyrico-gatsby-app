---
title: "ithuvarai ithuvarai song lyrics"
album: "Potta Potti"
artist: "Aruldev"
lyricist: "Jayamurasu - Kevin Shadrach"
director: "Yuvaraj Dhayalan"
path: "/albums/potta-potti-lyrics"
song: "Ithuvarai Ithuvarai"
image: ../../images/albumart/potta-potti.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q_psXTdgYVo"
type: "love"
singers:
  - Hariharan
  - Mahathi
  - Aruldev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O karisakkaattu Odaikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O karisakkaattu Odaikkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu vizhundha kannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu vizhundha kannu "/>
</div>
<div class="lyrico-lyrics-wrapper">mani en ponnumani O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mani en ponnumani O"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasippaarththen Orathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasippaarththen Orathula"/>
</div>
<div class="lyrico-lyrics-wrapper">sembavala muththumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sembavala muththumani"/>
</div>
<div class="lyrico-lyrics-wrapper">muththumani en ponnumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththumani en ponnumani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai idhuvarai mugam paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai idhuvarai mugam paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ini mudhal ini mudhal mudicherppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini mudhal ini mudhal mudicherppen"/>
</div>
<div class="lyrico-lyrics-wrapper">maraivaai rasiththeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraivaai rasiththeney"/>
</div>
<div class="lyrico-lyrics-wrapper">manamppoal parandhen naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamppoal parandhen naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhidhaai thaan mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai thaan mella "/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal virigiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal virigiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaai thaan indha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaai thaan indha "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagamey therigiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagamey therigiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">medhuvaai ennappidikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medhuvaai ennappidikkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei yei yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei yei yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yei yei yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei yei yei yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vidha bayam vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vidha bayam vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">idha kattippoadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idha kattippoadudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">pala murai paarkkaavidil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala murai paarkkaavidil "/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyum sududhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyum sududhey"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhaiyum naan unadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhaiyum naan unadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">nizhal ena varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhal ena varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">urakkam indri thadumaarippoanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakkam indri thadumaarippoanen"/>
</div>
<div class="lyrico-lyrics-wrapper">suzhi neerai poala sutri sutri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhi neerai poala sutri sutri"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sutri suzhatrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sutri suzhatrudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">payangal marandhadhey pudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payangal marandhadhey pudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi ondril poagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi ondril poagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kavidhaigal neeppesinaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavidhaigal neeppesinaai "/>
</div>
<div class="lyrico-lyrics-wrapper">maragadha kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maragadha kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">maadham ellaam manasukkulley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadham ellaam manasukkulley "/>
</div>
<div class="lyrico-lyrics-wrapper">adaimazhaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaimazhaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">medhu medhuvaai vizhitheendi poaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medhu medhuvaai vizhitheendi poaga"/>
</div>
<div class="lyrico-lyrics-wrapper">enai thoondippoaga thappi thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thoondippoaga thappi thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan kannil vizhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan kannil vizhundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai idhuvarai mugam paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai idhuvarai mugam paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ini mudhal ini mudhal mudicherppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini mudhal ini mudhal mudicherppen"/>
</div>
<div class="lyrico-lyrics-wrapper">maraivaai rasiththeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraivaai rasiththeney"/>
</div>
<div class="lyrico-lyrics-wrapper">manamppoal parandhen naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamppoal parandhen naaney"/>
</div>
</pre>
