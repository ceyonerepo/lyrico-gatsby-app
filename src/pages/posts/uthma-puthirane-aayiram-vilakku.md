---
title: "uthma puthirane song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Uthma Puthirane"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kp9EzXMqt0M"
type: "sad"
singers:
  - K.J. Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththama puththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththama puththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vandha chiththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vandha chiththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenmadhura paandiyanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenmadhura paandiyanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandha maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandha maayamenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththama puththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththama puththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vandha chiththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vandha chiththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenmadhura paandiyanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenmadhura paandiyanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandha maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandha maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aah haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanukku peru vachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanukku peru vachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">AasamaganAasththiy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AasamaganAasththiy"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam alliththandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam alliththandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karnan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karnan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanukku peru vachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanukku peru vachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasamagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasamagan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasththiyellaam alliththandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasththiyellaam alliththandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karnan avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karnan avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uththama puththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththama puththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vandha chiththiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vandha chiththiranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenmadhura paandiyanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenmadhura paandiyanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho aah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho aah aah haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandha maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandha maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aah haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandha maayamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandha maayamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah aah haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aah haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarirarooo aarirarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarirarooo aarirarooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarirarooo raroooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarirarooo raroooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarirarooo aarirarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarirarooo aarirarooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarirarooo raroooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarirarooo raroooo"/>
</div>
</pre>
