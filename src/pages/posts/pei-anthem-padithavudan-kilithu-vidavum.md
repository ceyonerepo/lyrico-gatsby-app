---
title: "pei anthem song lyrics"
album: "PAdithavudan Kilithu Vidavum"
artist: "Niro Prabakaran"
lyricist: "Abinandhan"
director: "Hari Uthraa"
path: "/albums/padithavudan-kilithu-vidavum-lyrics"
song: "Pei Anthem"
image: ../../images/albumart/padithavudan-kilithu-vidavum.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hkBaEW4uizc"
type: "thrill"
singers:
  - Abinandhan
  - Niro Prabhakaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ethirigal aayiram soola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirigal aayiram soola"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini pole aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini pole aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">throgangal pathara seiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="throgangal pathara seiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai thedi alaigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai thedi alaigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethirigal aayiram soola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirigal aayiram soola"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini pole aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini pole aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">throgangal pathara seiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="throgangal pathara seiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai thedi alaigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai thedi alaigindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanner vanthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanner vanthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kaayam thanthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kaayam thanthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnum mannum thalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnum mannum thalai "/>
</div>
<div class="lyrico-lyrics-wrapper">keelai sutri ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keelai sutri ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil asuththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil asuththam"/>
</div>
<div class="lyrico-lyrics-wrapper">athil vali than micham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil vali than micham"/>
</div>
<div class="lyrico-lyrics-wrapper">un nesam anbu paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nesam anbu paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu veenum aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu veenum aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethirigal aayiram soolthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirigal aayiram soolthum"/>
</div>
<div class="lyrico-lyrics-wrapper">mathiyaamal saavai vendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathiyaamal saavai vendren"/>
</div>
<div class="lyrico-lyrics-wrapper">pali thandum unaku vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pali thandum unaku vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku unartha vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku unartha vanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuvin aalam theriyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvin aalam theriyma"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin veeram puriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin veeram puriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaivan vilai uyir aniyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaivan vilai uyir aniyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than un kavasi naalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than un kavasi naalada"/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbam oru kovil pondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbam oru kovil pondru"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvi pol katiya veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvi pol katiya veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">anaithaiyum oru nodikulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithaiyum oru nodikulle"/>
</div>
<div class="lyrico-lyrics-wrapper">ilanthen theeyil indre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilanthen theeyil indre"/>
</div>
</pre>
