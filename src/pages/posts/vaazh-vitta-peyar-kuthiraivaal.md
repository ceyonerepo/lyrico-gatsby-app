---
title: "vaazh vitta peyar song lyrics"
album: "Kuthiraivaal"
artist: "Pradeep Kumar"
lyricist: "Sathish Raja Dharmar"
director: "Manoj Leonel Jahson - Shyam Sunder"
path: "/albums/kuthiraivaal-lyrics"
song: "Vaazh Vitta Peyar"
image: ../../images/albumart/kuthiraivaal.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YhrUCCeBP88"
type: "melody"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaal vitta peyar pattathu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaal vitta peyar pattathu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">ulam patta peyar vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulam patta peyar vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam vitta thuyar pokuvathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam vitta thuyar pokuvathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham vattathinul thuithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham vattathinul thuithu"/>
</div>
<div class="lyrico-lyrics-wrapper">pani ketta thena koorinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani ketta thena koorinum"/>
</div>
<div class="lyrico-lyrics-wrapper">ninai vinai meeri kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninai vinai meeri kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">idar aeri idar meendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idar aeri idar meendu"/>
</div>
<div class="lyrico-lyrics-wrapper">illa mathil ulla mathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa mathil ulla mathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi irandum kollung kaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi irandum kollung kaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vata thuipil uiththa kilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vata thuipil uiththa kilam"/>
</div>
<div class="lyrico-lyrics-wrapper">valanj sankai sevi serkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valanj sankai sevi serkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaala mathil thiyileri kanaviranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala mathil thiyileri kanaviranga"/>
</div>
<div class="lyrico-lyrics-wrapper">viliththa nilai kaalidukil vaal neeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliththa nilai kaalidukil vaal neeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sutta viral marantha kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutta viral marantha kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaaa kanaaa kanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaaa kanaaa kanaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ariyaathavai araiyaathar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyaathavai araiyaathar"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyaatha akkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyaatha akkanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">arinthaal ariyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arinthaal ariyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">avvaalin agan kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avvaalin agan kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">pirar ariyaa vaal kalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirar ariyaa vaal kalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ena soppanak kilavi naadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena soppanak kilavi naadi"/>
</div>
<div class="lyrico-lyrics-wrapper">seppi sertha pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seppi sertha pin"/>
</div>
<div class="lyrico-lyrics-wrapper">ari vaal pittam vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ari vaal pittam vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">parimaa pittam aera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parimaa pittam aera"/>
</div>
<div class="lyrico-lyrics-wrapper">vaal vitta kanavaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaal vitta kanavaal"/>
</div>
<div class="lyrico-lyrics-wrapper">nee puravi yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee puravi yena"/>
</div>
<div class="lyrico-lyrics-wrapper">kilavi pottil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilavi pottil "/>
</div>
<div class="lyrico-lyrics-wrapper">vatta panthu paava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatta panthu paava"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu pattu vanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu pattu vanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">veetukku veetukku veetukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukku veetukku veetukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malai puravi mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai puravi mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkath thadavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkath thadavi"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyudal thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyudal thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">pon mayir parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon mayir parava"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivaalena mudiya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivaalena mudiya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">malai puravi mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai puravi mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkath thadavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkath thadavi"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyudal thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyudal thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">pon mayir parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon mayir parava"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivaalena mudiya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivaalena mudiya kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manaiyena maanilaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manaiyena maanilaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiyena pennidaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyena pennidaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">pugal paravi vaal enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugal paravi vaal enge"/>
</div>
<div class="lyrico-lyrics-wrapper">ena naan nokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena naan nokka"/>
</div>
<div class="lyrico-lyrics-wrapper">mugam paarthu kani paraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugam paarthu kani paraka"/>
</div>
<div class="lyrico-lyrics-wrapper">eri thiraiyil imai thirakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri thiraiyil imai thirakka"/>
</div>
<div class="lyrico-lyrics-wrapper">ari saaya saainthirunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ari saaya saainthirunthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">thanaraiyil ari saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaraiyil ari saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">saainthirunthaan thanaraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saainthirunthaan thanaraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ari saaya saainthirunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ari saaya saainthirunthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">thanaraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaraiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sudar vili suda mada mangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudar vili suda mada mangai"/>
</div>
<div class="lyrico-lyrics-wrapper">vili sudarena irukaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vili sudarena irukaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavoru puthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavoru puthi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanmanamoru puthir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanmanamoru puthir"/>
</div>
<div class="lyrico-lyrics-wrapper">en manai veetra iputhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manai veetra iputhi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarena uri manai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarena uri manai"/>
</div>
<div class="lyrico-lyrics-wrapper">oruvanukko uiththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruvanukko uiththa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirgal or kanakko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirgal or kanakko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanal kadantha en oor kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanal kadantha en oor kan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum thuvakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum thuvakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">endrena kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrena kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil endra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil endra naan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanka vilitha nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanka vilitha nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalidukkil vaal neeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalidukkil vaal neeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sutta viral marantha kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutta viral marantha kanaa"/>
</div>
</pre>
