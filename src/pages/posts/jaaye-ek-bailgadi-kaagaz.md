---
title: "jaaye ek bailgadi song lyrics"
album: "Kaagaz"
artist: "Pravesh Mallick"
lyricist: "Rashmi Virag"
director: "Satish Kaushik"
path: "/albums/kaagaz-lyrics"
song: "Jaaye Ek Bailgadi"
image: ../../images/albumart/kaagaz.jpg
date: 2021-01-07
lang: hindi
youtubeLink: "https://www.youtube.com/embed/ArlsgIVmCao"
type: "love"
singers:
  - Udit Narayan
  - Alka Yagnik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaara kaisi matwari teri meri yaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara kaisi matwari teri meri yaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum chand pe le jaaye ek bailgadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum chand pe le jaaye ek bailgadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho yaara kaisi matwari teri meri yaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho yaara kaisi matwari teri meri yaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum chand pe le jaaye ek bailgadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum chand pe le jaaye ek bailgadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wahaan naino se naina ladayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahaan naino se naina ladayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein karenge meethi meeth pyaari pyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein karenge meethi meeth pyaari pyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahaan naino se naina ladayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahaan naino se naina ladayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein karenge meethi meeth pyaari pyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein karenge meethi meeth pyaari pyaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tum paas aaye to hum paas aayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum paas aaye to hum paas aayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanso se hum teri saansein milayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanso se hum teri saansein milayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi hai iss dil mein tumko batayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi hai iss dil mein tumko batayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Sach keh rahe hain hum kuch na chupayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach keh rahe hain hum kuch na chupayenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Duniya ko bhool ke tujhmein kho jaayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ko bhool ke tujhmein kho jaayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Waapas naa aayenge, waapas naa aayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waapas naa aayenge, waapas naa aayenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zara jaldi jaldi zara jaldi jaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara jaldi jaldi zara jaldi jaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho zara jaldi jaldi haanko yaara bailgadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho zara jaldi jaldi haanko yaara bailgadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tez bhagane lagi hai dhadkan humari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tez bhagane lagi hai dhadkan humari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dekhenge hum tumko dekhe hi jaayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhenge hum tumko dekhe hi jaayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise hi saara din sang sang bitayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise hi saara din sang sang bitayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Phoolon ki bagiyan mein tumko le jaayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phoolon ki bagiyan mein tumko le jaayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Khusboo ki duniya se tumko milayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khusboo ki duniya se tumko milayenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haath pakad ke hum chalte hi jaayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haath pakad ke hum chalte hi jaayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Waapas naa aayenge, waapas naa aayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waapas naa aayenge, waapas naa aayenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann bhar aaya, mann bhar aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann bhar aaya, mann bhar aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann bhar aaya, ho mann bhar aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann bhar aaya, ho mann bhar aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann bhar aaaya baatein sunke tumhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann bhar aaaya baatein sunke tumhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekho humne bhi kar li hai taiyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho humne bhi kar li hai taiyari"/>
</div>
</pre>
