---
title: "onnuku renda song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Kabilan Vairamuthu"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Onnuku Renda"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dJgcugJF93U"
type: "love"
singers:
  - Senthil Ganesh
  - V.M.Mahalingam
  - Sathya Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnukku Renda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnukku Renda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukku Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachcha Mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachcha Mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etha Thaan Okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etha Thaan Okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannumunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Polambi Kedakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Polambi Kedakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambi Thavikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambi Thavikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandu Padukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandu Padukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Morandu Pudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morandu Pudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Luck Aduchuduchu Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Aduchuduchu Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla Sirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla Sirikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Paiyana Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Paiyana Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila Nadikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila Nadikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alluthu Alluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluthu Alluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alluthu Alluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluthu Alluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulluthu Thulluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulluthu Thulluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulluthu Thulluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulluthu Thulluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolluthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Katti Thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Katti Thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluthu Solluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluthu Solluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Foreign Returna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Returna Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Norunguren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norunguren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Foreign Returna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Returna Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Norunguren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norunguren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Ponnu Irukkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Ponnu Irukkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern Ponnu Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Ponnu Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Maaman Ponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Maaman Ponne"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern Ponna Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Ponna Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lucku Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucku Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththa Ponnu Irukkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Ponnu Irukkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththa Ponnu Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththa Ponnu Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Nalla Ponna Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Nalla Ponna Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo Machcham Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Machcham Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Poi Peter Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Poi Peter Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign Ponnu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Ponnu Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Sela Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Sela Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum En Devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum En Devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Thaan En Mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaan En Mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Yetha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Yetha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Okay Done Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Okay Done Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimey Thaan Fun Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimey Thaan Fun Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Yetha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Yetha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Okay Done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Okay Done"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimey Thaan Fun Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimey Thaan Fun Nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manda Mela Konda Vechcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Mela Konda Vechcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandathu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Illa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Illa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhandachoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandachoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga Vachchaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga Vachchaale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aalu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aalu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Geththa Irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geththa Irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Oththa Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Oththa Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pinnaala Suththa Vittiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pinnaala Suththa Vittiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu Vetti Oora Kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Vetti Oora Kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sora Poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sora Poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kattilukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kattilukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Vachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Vachchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alluthu Alluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluthu Alluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alluthu Alluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluthu Alluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulluthu Thulluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulluthu Thulluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulluthu Thulluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulluthu Thulluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolluthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolluthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolluthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Katti Thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Katti Thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluthu Solluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluthu Solluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Foreign Returna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Returna Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Norunguren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norunguren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Foreign Returna Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Returna Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Norunguren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norunguren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Poova Illa Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Poova Illa Pushpama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Perungaaya Dappa La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Perungaaya Dappa La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Pushpama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Pushpama"/>
</div>
</pre>
