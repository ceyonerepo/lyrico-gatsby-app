---
title: "ye zindagi song lyrics"
album: "Most Eligible Bachelor"
artist: "Gopi Sundar"
lyricist: "Ramajogayya Sastry"
director: "Bommarillu Bhaskar"
path: "/albums/most-eligible-bachelor-lyrics"
song: "Ye Zindagi"
image: ../../images/albumart/most-eligible-bachelor.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ruehhV1MuAo"
type: "happy"
singers:
  - Haniya Nafisa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aakashamantha aanandhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamantha aanandhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellaruthondhe naa kosamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaruthondhe naa kosamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alochanantha aaratamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alochanantha aaratamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anvesisthundhe ee rojukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anvesisthundhe ee rojukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye zindagi ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye zindagi ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kongotthaga navvela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kongotthaga navvela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee mayajalam antha thanedhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee mayajalam antha thanedhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhalu parugayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhalu parugayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalu velugayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalu velugayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">O thodu dorike nedu thanalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O thodu dorike nedu thanalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashamantha aanandhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamantha aanandhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellaruthondhe naa kosamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaruthondhe naa kosamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa pedhavanchuku thana peru thoranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pedhavanchuku thana peru thoranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa chirunavvuku thanega karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa chirunavvuku thanega karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaayi dhaayi dhaayi dhaayi dhaayi dhaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaayi dhaayi dhaayi dhaayi dhaayi dhaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanunte chaalu chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanunte chaalu chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi haayi haayi parimalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi haayi haayi parimalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchava kshanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchava kshanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalayya neevalana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalayya neevalana "/>
</div>
<div class="lyrico-lyrics-wrapper">neethone poorthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethone poorthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakshamantha aanandhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakshamantha aanandhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellaruthundhe naa kosamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaruthundhe naa kosamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alochanantha aaratamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alochanantha aaratamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anvesisthundhe ee rojukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anvesisthundhe ee rojukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye zindagi ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye zindagi ivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kongotthaga navvela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kongotthaga navvela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee mayajalam antha thanedhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee mayajalam antha thanedhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhalu parugayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhalu parugayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalu velugayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalu velugayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">O thodu dorike nedu thanalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O thodu dorike nedu thanalaaga"/>
</div>
</pre>
