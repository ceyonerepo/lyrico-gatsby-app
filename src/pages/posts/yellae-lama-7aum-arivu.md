---
title: "yellae lama song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Na. Muthukumar"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Yellae Lama"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/B122I8dNEtU"
type: "love"
singers:
  - Vijay Prakash
  - Karthik
  - Shalini
  - Shruti Haasan
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yelle Laama Yele Yelamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelle Laama Yele Yelamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamale Ullam Thullumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamale Ullam Thullumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoramaa Nenjin Oramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoramaa Nenjin Oramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalammaa Vellam Allumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaalammaa Vellam Allumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jannal Kathavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannal Kathavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Paarvai Pattu Therika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Paarvai Pattu Therika"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minnal Pozhuthile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minnal Pozhuthile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Ennai Izhuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Ennai Izhuka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalum Vinnil Thaavuthadi Kuthikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalum Vinnil Thaavuthadi Kuthikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelle Laama Yele Yelamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelle Laama Yele Yelamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamale Ullam Thullumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamale Ullam Thullumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoramaa Nenjin Oramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoramaa Nenjin Oramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalammaa Vellam Allumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaalammaa Vellam Allumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jannal Kathavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannal Kathavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Paarvai Pattu Therika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Paarvai Pattu Therika"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minnal Pozhuthile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minnal Pozhuthile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Ennai Izhuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Ennai Izhuka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalum Vinnil Thaavi Athu Kuthikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalum Vinnil Thaavi Athu Kuthikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Newton Apple Vizha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Newton Apple Vizha "/>
</div>
<div class="lyrico-lyrics-wrapper">Puvi Eerpai Kandaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvi Eerpai Kandaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nanum Unnil Vizha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nanum Unnil Vizha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Eerpai Kandenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Eerpai Kandenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai Ketkamale Isai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai Ketkamale Isai "/>
</div>
<div class="lyrico-lyrics-wrapper">Amaithaan Beathovane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaithaan Beathovane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Kekamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Kekamale "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kadhal Sei Nanbaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kadhal Sei Nanbaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthu Mathipaai Ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthu Mathipaai Ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthavalum Neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavalum Neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppai Koodai Pol Nenja 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppai Koodai Pol Nenja "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaichava Neethaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaichava Neethaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum Melum Azhagaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Melum Azhagaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Ponen Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Ponen Naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelle Laama Yele Yelamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelle Laama Yele Yelamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamale Ullam Thullumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamale Ullam Thullumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoramaa Nenjin Oramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoramaa Nenjin Oramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaachumaa Vellam Allumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaachumaa Vellam Allumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jannal Kathavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannal Kathavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Paarvai Pattu Therika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Paarvai Pattu Therika"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minnal Pozhuthile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minnal Pozhuthile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Ennai Izhuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Ennai Izhuka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalum Vinnil Thaavuthadi Kuthikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalum Vinnil Thaavuthadi Kuthikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Neram Illaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Neram Illaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Neerum Illaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Neerum Illaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Veyilum Padaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Veyilum Padaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pookum Inbam Thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pookum Inbam Thanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil Vizhaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil Vizhaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Sirithum Padaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Sirithum Padaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalum Thodaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalum Thodaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Kollai Ittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Kollai Ittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvarum Matum Vaazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Matum Vaazha "/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Ondru Seivomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Ondru Seivomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravondre Pothum Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravondre Pothum Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalidam Solvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalidam Solvomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veru Velai Ethum Indri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Velai Ethum Indri "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seivom Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seivom Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelle Laama Yele Yelamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelle Laama Yele Yelamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamale Ullam Thullumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamale Ullam Thullumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoramaa Nenjin Oramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoramaa Nenjin Oramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalamma Vellam Allumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaalamma Vellam Allumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jannal Kathavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jannal Kathavile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Paarvai Pattu Therika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Paarvai Pattu Therika"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Minnal Pozhuthile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Minnal Pozhuthile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kadhal Ennai Izhuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhal Ennai Izhuka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalum Vinnil Thaavuthadi Kuthikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalum Vinnil Thaavuthadi Kuthikaa"/>
</div>
</pre>
