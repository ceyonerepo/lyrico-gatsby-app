---
title: "kaadamutta song lyrics"
album: "Veerapandiyapuram"
artist: "Jai"
lyricist: "Vairamuthu"
director: "Suseenthiran"
path: "/albums/veerapandiyapuram-lyrics"
song: "Kaadamutta"
image: ../../images/albumart/veerapandiyapuram.jpg
date: 2022-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KqeDnFm6JQE"
type: "love"
singers:
  - Anal Akash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vatta vatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan vatta nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan vatta nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm paththu pura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm paththu pura"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan patchai pura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan patchai pura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta vatta paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta nila vandhathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta nila vandhathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu pura mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu pura mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Patchai pura meiyuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patchai pura meiyuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettai jada potta ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai jada potta ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitchi thinna echi oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitchi thinna echi oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nei kudathil erumbu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nei kudathil erumbu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei kudathil manasu yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei kudathil manasu yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu rendum kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu rendum kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukken vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukken vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta vatta paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta nila vandhathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta nila vandhathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu pura mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu pura mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Patchai pura meiyuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patchai pura meiyuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kaatukulla kedantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kaatukulla kedantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatukaaran pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatukaaran pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella thaan maathi mudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella thaan maathi mudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaikaaran pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaikaaran pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veetil irundha ottadaiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veetil irundha ottadaiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu nee katti mudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu nee katti mudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu kunjam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu kunjam pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju viral thotta sugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju viral thotta sugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukuzhi kulla kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuzhi kulla kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mitcha baagam thottu mudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mitcha baagam thottu mudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcham thaan di enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcham thaan di enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeni podi pola siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeni podi pola siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Siththam raththam katti kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththam raththam katti kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku enna thittam unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku enna thittam unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli podi enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli podi enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeya vecha kozhuthi nenja kadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya vecha kozhuthi nenja kadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyana minjuputta enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyana minjuputta enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu rendum kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu rendum kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukken vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukken vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta vatta paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta nila vandhathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta nila vandhathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu pura mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu pura mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Patchai pura meiyuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patchai pura meiyuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettai jada potta ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai jada potta ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitchi thinna echi oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitchi thinna echi oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nei kudathil erumbu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nei kudathil erumbu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei kudathil manasu yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei kudathil manasu yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu rendum kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu rendum kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukken vaadi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukken vaadi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadamutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadamutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi kitta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi kitta vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta vatta vatta nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta vatta nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu pura patchai pura aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu pura patchai pura aa"/>
</div>
</pre>
