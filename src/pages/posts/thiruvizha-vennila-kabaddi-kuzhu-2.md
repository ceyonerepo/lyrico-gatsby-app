---
title: "thiruvizha song lyrics"
album: "Vennila Kabaddi Kuzhu 2"
artist: "V. Selvaganesh"
lyricist: "Kabilan"
director: "Selva Sekaran"
path: "/albums/vennila-kabaddi-kuzhu-2-lyrics"
song: "Thiruvizha"
image: ../../images/albumart/vennila-kabaddi-kuzhu-2.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kbsh1sx0F4I"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ribbona Dhavaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ribbona Dhavaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Subbamma Kattirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subbamma Kattirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithala Sombadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithala Sombadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakki Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakki Vechirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ribbona Dhavaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ribbona Dhavaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Subbamma Kattirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subbamma Kattirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithala Sombadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithala Sombadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakki Vechirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakki Vechirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papbuna Rajava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papbuna Rajava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappunu Maathiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappunu Maathiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kikkula Kittapona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kikkula Kittapona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkila Kootivandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkila Kootivandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannangala Muttava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangala Muttava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoodi Kittava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoodi Kittava"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthanaiyil Onnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthanaiyil Onnana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mootakattava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mootakattava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandharicha Veppala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandharicha Veppala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamool Tharen Mappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamool Tharen Mappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattanila Pola Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattanila Pola Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Thoopula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Thoopula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhale Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhale Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi Selakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Selakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukutti Mallukatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukutti Mallukatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Edakku Pannudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku Pannudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Madakka Pakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Madakka Pakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killadi Chinnakutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Chinnakutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Vathi Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Vathi Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolutha Pakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolutha Pakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Veluthu Vanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Veluthu Vanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimel Karakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimel Karakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam Nadathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Nadathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppil Evanathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppil Evanathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulandhaya Thukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulandhaya Thukkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhale Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhale Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machanin Kannu Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machanin Kannu Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavattum Kallupola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavattum Kallupola"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Suthudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suthudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenn Kanna Kuthudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenn Kanna Kuthudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalilla Paaraimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalilla Paaraimela"/>
</div>
<div class="lyrico-lyrics-wrapper">Valattum Korangapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valattum Korangapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallakatudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakatudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Pazhatha Kekudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Pazhatha Kekudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukku Pudichiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukku Pudichiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Alaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Alaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku Unnakudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku Unnakudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravinil Illavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinil Illavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Adada Adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Adada Adada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhale Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhale Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaluthula Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaluthula Kumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedakozhi Koovudhu Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedakozhi Koovudhu Summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhale Oru Umma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhale Oru Umma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Udanchi Pochudi Kamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Udanchi Pochudi Kamma"/>
</div>
</pre>
