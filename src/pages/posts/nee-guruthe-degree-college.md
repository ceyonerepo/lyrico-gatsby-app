---
title: "nee guruthe song lyrics"
album: "Degree College"
artist: "Sunil Kashyap"
lyricist: "Vanamali"
director: "Narasimha Nandi"
path: "/albums/degree-college-lyrics"
song: "Nee Guruthe"
image: ../../images/albumart/degree-college.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HZUCRL_nXQ8"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Guruthe Dhooram Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthe Dhooram Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalape Veraipodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalape Veraipodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Maaredhi Vedhinchadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Maaredhi Vedhinchadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Guruthe Dhooram Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthe Dhooram Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalape Veraipodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalape Veraipodhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vaipu Saagali Antunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vaipu Saagali Antunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thodu Kanaraani Ee paadhme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thodu Kanaraani Ee paadhme"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennallu Brathakali Antunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennallu Brathakali Antunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Leka Neeraina Ee Praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Leka Neeraina Ee Praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavaa Jathagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaa Jathagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Guruthe Dhooram Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthe Dhooram Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalape Veraipodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalape Veraipodhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakanti Mundhunna Nee roopame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakanti Mundhunna Nee roopame"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripindhi Dhayaleni Ee Kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripindhi Dhayaleni Ee Kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Nenu Nijame Kshnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Nenu Nijame Kshnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamante Adhi Nammagalama Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamante Adhi Nammagalama Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaa Vinavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaa Vinavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Guruthe Dhooram Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthe Dhooram Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalape Veraipodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalape Veraipodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Maaredhi Vedhinchadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Maaredhi Vedhinchadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Guruthe Dhooram Kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Guruthe Dhooram Kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalape Veraipodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalape Veraipodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Maaredhi Vedhinchadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Maaredhi Vedhinchadhe"/>
</div>
</pre>
