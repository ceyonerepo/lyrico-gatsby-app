---
title: "macho ennacho song lyrics"
album: "Mersal"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Atlee"
path: "/albums/mersal-lyrics"
song: "Macho Ennacho"
image: ../../images/albumart/mersal.jpg
date: 2017-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MmvpbLdaIRs"
type: "Love"
singers:
  - Sid Sriram
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Macho Ennacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macho Ennacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Touchittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Touchittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir In To Two Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir In To Two Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Macho Match Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macho Match Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Speekitta Kuyil Keecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Speekitta Kuyil Keecho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dreamil Huggitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dreamil Huggitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Flower Shower Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flower Shower Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sleepy Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepy Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Snooze Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Snooze Aacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ogala Gala Girl Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogala Gala Girl Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Classy Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Classy Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ogala Gala Gala Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogala Gala Gala Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Maassy Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Maassy Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lookkaa Juicy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lookkaa Juicy"/>
</div>
<div class="lyrico-lyrics-wrapper">Clickaa Classy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clickaa Classy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinky Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinky Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Daisy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Daisy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Icy Doll Asanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Icy Doll Asanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Whistle Udhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Whistle Udhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Morning Flash Ah aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Morning Flash Ah aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiny Po Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiny Po Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Caramel Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caramel Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tasty Paakkurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tasty Paakkurendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Smily Ponaa aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Smily Ponaa aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlie Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlie Gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imachcha Missitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imachcha Missitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vida Unna Lovitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vida Unna Lovitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Heartukkulla Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Heartukkulla Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Rabbit Aayitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Rabbit Aayitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli Varaamale Hoppy Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Varaamale Hoppy Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macho Ennacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macho Ennacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Touchitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Touchitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir In To Two Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir In To Two Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Macho Match Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macho Match Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Speekitta Kuyil Keecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Speekitta Kuyil Keecho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kissa Vennilaa Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kissa Vennilaa Paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Color Malar Urasiye Ozhi Sekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color Malar Urasiye Ozhi Sekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Smellum Koondhalin Poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Smellum Koondhalin Poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naru Manam Izhuthathum Thara Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naru Manam Izhuthathum Thara Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Takittaa Sweet Ah Ponan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Takittaa Sweet Ah Ponan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Veedhiyil Float Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Veedhiyil Float Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Melody Ah Tweetturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melody Ah Tweetturen"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful Memory Ah Treaturen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful Memory Ah Treaturen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maacho Ennacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maacho Ennacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Touchitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Touchitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir In To Two Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir In To Two Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maacho Match Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maacho Match Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Speekitta Kuyil Keecho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Speekitta Kuyil Keecho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dreamil Huggitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dreamil Huggitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Flower Shower Aacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flower Shower Aacho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sleepy Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sleepy Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Snooze Aachcgo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Snooze Aachcgo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ogala Gala Girl Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogala Gala Girl Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Massy Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Massy Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ogala Gala Gala Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ogala Gala Gala Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Classy Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Classy Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lookka Juicy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lookka Juicy"/>
</div>
<div class="lyrico-lyrics-wrapper">Clickaa Classy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clickaa Classy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinky Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinky Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Doosy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Doosy"/>
</div>
</pre>
