---
title: 'tamizhan endru sollada lyrics'
album: 'Bhoomi'
artist: 'D Imman'
lyricist: 'Madhan Karky'
director: 'Lakshman'
path: '/albums/bhoomi-song-lyrics'
song: 'Tamizhan Endru Sollada'
image: ../../images/albumart/bhoomi.jpg
date: 2020-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mX-HaAttuIc"
type: 'mass'
singers: 
- Anirudh Ravichander
- Lavanya Sundararaman,
- D.Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Aa….aaa..aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aa….aaa..aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaa….aa…naa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa….aa…naa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa….aaa….aaa…naa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa….aaa….aaa…naa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Haa…aaa…aaa..aaa…aaa…aaa..aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…aaa..aaa…aaa…aaa..aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada..aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada..aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yee..
<input type="checkbox" class="lyrico-select-lyric-line" value="Yee.."/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi engum sutri vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi engum sutri vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai thottum vandhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinnai thottum vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha mannil edho ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha mannil edho ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaettru mozhi sorkkal ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaettru mozhi sorkkal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu kondae vandhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu kondae vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En tamizhil edho ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="En tamizhil edho ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pirindhidum varai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirindhidum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhan perumaigal edhuvum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhan perumaigal edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arindhidavillai nenjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Arindhidavillai nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marupadi paadhathinai
<input type="checkbox" class="lyrico-select-lyric-line" value="Marupadi paadhathinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan padhikkum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan padhikkum pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkkuthu dhegam konjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Silirkkuthu dhegam konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Narambugal anaithilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Narambugal anaithilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aram yenum uram dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aram yenum uram dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathin mudhal niram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagathin mudhal niram"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh niram dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamizh niram dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhu kodi mugam aanaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhu kodi mugam aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae oru peyar dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Orae oru peyar dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu verum peyar illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu verum peyar illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal uyir dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engal uyir dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pizza burger undu vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Pizza burger undu vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasta thindrum vandhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasta thindrum vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Itiliyil edho ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Itiliyil edho ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhoo ondru….
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhoo ondru…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rock and roll kettu vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Rock and roll kettu vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Jazzil mozhgi vandhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Jazzil mozhgi vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam paraiyil edho ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nam paraiyil edho ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhoo ondru….
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhoo ondru…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uravugal ennum sollin
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravugal ennum sollin"/>
</div>
<div class="lyrico-lyrics-wrapper">Arththam kanduppidikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Arththam kanduppidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru idam mannil illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Veru idam mannil illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye.. mozhi verum oli illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye.. mozhi verum oli illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi endru uraitha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi endru uraitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru inam engum illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Veru inam engum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Narambugal anaithilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Narambugal anaithilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aram yenum uram dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aram yenum uram dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathin mudhal niram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagathin mudhal niram"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh niram dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamizh niram dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhu kodi mugam aanaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhu kodi mugam aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae oru peyar dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Orae oru peyar dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu verum peyar illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu verum peyar illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal uyir dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engal uyir dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Coat-u adhai kalatti vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Coat-u adhai kalatti vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantu adhai koluthiputtu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pantu adhai koluthiputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettiyai ne madichu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettiyai ne madichu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thagida thagida thagida
<input type="checkbox" class="lyrico-select-lyric-line" value="Thagida thagida thagida thagida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sagathiyil kaala vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sagathiyil kaala vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaththu nattum thaalam ittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naaththu nattum thaalam ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu katta paattu kattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ettu katta paattu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thagida thagida thagida
<input type="checkbox" class="lyrico-select-lyric-line" value="Thagida thagida thagida thagida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaiyiram aandin munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaiyiram aandin munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siththar sonnathellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Siththar sonnathellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru dhaan NASA sollum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru dhaan NASA sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nilavai muthamittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavai muthamittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinkalathil yeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinkalathil yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizho vinnai thaandi vellum
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamizho vinnai thaandi vellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kizhavigal mozhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kizhavigal mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubhava uli
<input type="checkbox" class="lyrico-select-lyric-line" value="Anubhava uli"/>
</div>
  <div class="lyrico-lyrics-wrapper">Adhil undu bhoomi pandhin
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhil undu bhoomi pandhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa arivu
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththa arivu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kumarigal vizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kumarigal vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidharidum oli
<input type="checkbox" class="lyrico-select-lyric-line" value="Sidharidum oli"/>
</div>
  <div class="lyrico-lyrics-wrapper">Adhil undu bhoomi pandhin
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhil undu bhoomi pandhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa azhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththa azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhu kodi idhaiyathil orae thudippu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhu kodi idhaiyathil orae thudippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal vizhigalil erivadhu orae neruppu
<input type="checkbox" class="lyrico-select-lyric-line" value="Engal vizhigalil erivadhu orae neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginukkozhi thara athaipparppu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulaginukkozhi thara athaipparppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha inathinil pirappathae thanichirappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha inathinil pirappathae thanichirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tamilan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaniyai nee vellada (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharaniyai nee vellada"/></div>
</div>
</pre>