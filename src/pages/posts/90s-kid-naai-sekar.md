---
title: "90's kid song lyrics"
album: "Naai Sekar"
artist: "Ajesh - Anirudh Ravichander"
lyricist: "Kishore Rajkumar"
director: "Kishore Rajkumar"
path: "/albums/naai-sekar-lyrics"
song: "90'S Kid"
image: ../../images/albumart/naai-sekar.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/icpPzn0ujak"
type: "happy"
singers:
  - G.V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heroine nee thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heroine nee thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joker u naan thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker u naan thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero vey vandhaalum paathukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero vey vandhaalum paathukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">CSK nee thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CSK nee thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">RCB naan thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="RCB naan thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Finals ey vandhaalum thothukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Finals ey vandhaalum thothukkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaane baby ennoda Queen u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaane baby ennoda Queen u"/>
</div>
<div class="lyrico-lyrics-wrapper">Colour ethum red wine u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colour ethum red wine u"/>
</div>
<div class="lyrico-lyrics-wrapper">WhatsApp il naanum vettenma train u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="WhatsApp il naanum vettenma train u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada always yiu are mine u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada always yiu are mine u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
90'<div class="lyrico-lyrics-wrapper">s kid thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Basic aave naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basic aave naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Good maa good maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good maa good maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendzone ey pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendzone ey pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">One side love u maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One side love u maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Love maa love maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love maa love maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
90'<div class="lyrico-lyrics-wrapper">s kid thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Basic aave naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basic aave naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Good maa good maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good maa good maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendzone ey pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendzone ey pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">One side love u maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One side love u maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Love maa love maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love maa love maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kooda ayyo day night ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda ayyo day night ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum alanjen photo edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum alanjen photo edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thnaiye thaan nerungi un kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thnaiye thaan nerungi un kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vantha thalli pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vantha thalli pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava thaan siricha cute ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava thaan siricha cute ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha lite ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha lite ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Saruki vizhunthene flat ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saruki vizhunthene flat ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Devadha neril vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadha neril vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam thaan thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam thaan thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ey aakkittaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ey aakkittaale "/>
</div>
<div class="lyrico-lyrics-wrapper">white ah bright ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="white ah bright ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kooda ippo aaneney friend u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda ippo aaneney friend u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikirirame husband u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikirirame husband u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu ippo latest trend u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu ippo latest trend u"/>
</div>
<div class="lyrico-lyrics-wrapper">En love kku illa end u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love kku illa end u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaane baby ennoda Queen u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaane baby ennoda Queen u"/>
</div>
<div class="lyrico-lyrics-wrapper">Colour ethum red wine u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colour ethum red wine u"/>
</div>
<div class="lyrico-lyrics-wrapper">WhatsApp il naanum vettenma train u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="WhatsApp il naanum vettenma train u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada always yiu are mine u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada always yiu are mine u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
90'<div class="lyrico-lyrics-wrapper">s kid thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Basic aave naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basic aave naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Good maa good maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good maa good maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendzone ey pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendzone ey pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">One side love u maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One side love u maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Love maa love maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love maa love maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
90'<div class="lyrico-lyrics-wrapper">s kid thaan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid thaan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Basic aave naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basic aave naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Good maa good maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good maa good maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendzone ey pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendzone ey pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">One side love u maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One side love u maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Love maa love maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love maa love maa"/>
</div>
</pre>
