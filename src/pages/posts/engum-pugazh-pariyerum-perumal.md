---
title: "engum pugazh song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "Chinnasamidaasan - Mari Selvaraj"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Engum Pugazh"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BOtUHbAiLz8"
type: "happy"
singers:
  - Anthony Daasan
  - Mariappan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engum pugazh thuvanga aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum pugazh thuvanga aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ama ama saamy podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama ama saamy podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naanum naan thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naanum naan thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">O ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaiyaana sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyaana sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ama ama ama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama ama ama"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkavittu thedalaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkavittu thedalaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiya kaanommae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya kaanommae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum pugazh thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum pugazh thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naanum naan thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naanum naan thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum pugazh thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum pugazh thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naanum naan thuvanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naanum naan thuvanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaiyaana sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyaana sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangame"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkavittu thedalaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkavittu thedalaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiya kaanommae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya kaanommae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaiyaana sundhariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyaana sundhariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangame"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkavittu thedalaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkavittu thedalaanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiya kaanommae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiya kaanommae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallikoodam padikka vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam padikka vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavusaaga kooda irundhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavusaaga kooda irundhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam padikka vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam padikka vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavusaaga kooda irundhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavusaaga kooda irundhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam peraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam peraamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhiyila parandhuttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhiyila parandhuttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam peraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam peraamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhiyila parandhuttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhiyila parandhuttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya pidithukondu kadhai kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pidithukondu kadhai kadhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam uraithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam uraithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya pidithukondu kadhai kadhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya pidithukondu kadhai kadhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam uraithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam uraithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhai solli mudikkum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai solli mudikkum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer kadalil kalandhutiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer kadalil kalandhutiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhai solli mudikkum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai solli mudikkum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer kadalil kalandhutiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer kadalil kalandhutiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusai sinikku kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusai sinikku kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Salakku salakku salakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salakku salakku salakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Salakku salakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salakku salakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusai sinikku kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusai sinikku kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandaiyaga enai azhaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandaiyaga enai azhaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusai sinikku kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusai sinikku kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">kuzhandaiyaga enai azhaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhandaiyaga enai azhaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmadi serum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmadi serum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamaga marainjitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamaga marainjitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ama…unmadi serum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama…unmadi serum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamaga marainjitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamaga marainjitiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saapittu kai kazhivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapittu kai kazhivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhaikku poi vaarenunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhaikku poi vaarenunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapittu kai kazhivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapittu kai kazhivi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhaikku poi vaarenunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhaikku poi vaarenunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solliputtu poniyedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliputtu poniyedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponavala varakkanomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavala varakkanomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solliputtu poniyedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliputtu poniyedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaana thangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaana thangamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponavala varakkanomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavala varakkanomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponavala varakkanomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavala varakkanomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponavala varakkanomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavala varakkanomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathinamae kannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathinamae kannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaanae naanaenannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaanae naanaenannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae naannae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae naannae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaanae naanaenannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaanae naanaenannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae naannae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae naannae naanae nannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaanae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaanae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae nananea naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae nananea naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae nannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae nannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae nannae naanae nannae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae nannae naanae nannae "/>
</div>
<div class="lyrico-lyrics-wrapper">thanae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae nannae naanae nannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae nannae naanae nannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannananna thannaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannananna thannaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayooo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayooo ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduyaa poduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduyaa poduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ada ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ada ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliyankulam selvaraasiya kokkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyankulam selvaraasiya kokkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhaa adhaa adhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhaa adhaa adhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nadaiya paaruyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nadaiya paaruyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ama"/>
</div>
<div class="lyrico-lyrics-wrapper">Duuurrraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duuurrraaa"/>
</div>
</pre>
