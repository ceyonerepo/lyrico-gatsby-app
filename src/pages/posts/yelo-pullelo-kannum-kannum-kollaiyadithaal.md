---
title: "yelo pullelo song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Mani Amudhavan"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Yelo Pullelo"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nfH0pa0VSBI"
type: "Enjoyment"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaarey Vaa Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarey Vaa Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re Re Polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Re Polaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarey Vaa Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarey Vaa Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Re Re Polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re Re Polaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiyare Settinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiyare Settinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Re Chal Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Re Chal Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganga Longa Polaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Longa Polaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polaam Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polaam Maame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okka Maakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Maakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Yedhum Kekkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Yedhum Kekkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Right Ah Wronga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Ah Wronga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Edhaiyum Paakkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Edhaiyum Paakkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholu Mela Mani Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholu Mela Mani Manithaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pai Paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pai Paiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha Porom Ini Inithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Porom Ini Inithaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">High Fy Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High Fy Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganga Long Ah Polaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Long Ah Polaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Tharaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Tharaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nora Thathumbura Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nora Thathumbura Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooththi Kudipom Hom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooththi Kudipom Hom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narambula Paravura Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambula Paravura Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhiyaa Kudhipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhiyaa Kudhipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambula Idam Irukkura Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambula Idam Irukkura Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tattoos Pathipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattoos Pathipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kala Kala Cassino Vula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kala Cassino Vula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jackpotu Adippome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jackpotu Adippome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu Porapithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Porapithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidichirukkudhu Indha Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukkudhu Indha Tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula Therinchirunthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula Therinchirunthadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaal Thodanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal Thodanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu Nadanthutho Atha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Nadanthutho Atha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodaru Payanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaru Payanaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Ini Varum Pozhudhugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ini Varum Pozhudhugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeradhey Kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhey Kondaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelolo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelolo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelelo Pullelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelo Yelo Yelelo Pullelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelo Yelo Yelelo Pullelo"/>
</div>
</pre>
