---
title: "sakiye song lyrics"
album: "Super Duper"
artist: "Divakara Thiyagarajan"
lyricist: "AK"
director: "Arun Karthik"
path: "/albums/super-duper-lyrics"
song: "Sakiye"
image: ../../images/albumart/super-duper.jpg
date: 2019-09-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TWzQHYyGx9c"
type: "sad"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Poovin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Poovin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin Thugalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin Thugalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhu pogum neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhu pogum neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Izhandha vaanavil Nagalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Izhandha vaanavil Nagalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Poovin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Poovin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin Thugalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin Thugalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhu pogum neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhu pogum neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Izhandha vaanavil Nagalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Izhandha vaanavil Nagalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Veettukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Veettukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Kallaattam Vizhundhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Kallaattam Vizhundhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamy Selaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamy Selaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaattam Irupadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaattam Irupadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Saamy Petra Varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Saamy Petra Varamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man Mela Parakkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Mela Parakkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vannathu Poochikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vannathu Poochikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eragu Sumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eragu Sumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Kulithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Kulithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannukkul Naan Imaiyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukkul Naan Imaiyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrellam Alaindhodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrellam Alaindhodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irage"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Endha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Endha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaigal Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaigal Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooliyitta Thaayum Angu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooliyitta Thaayum Angu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil Vaadinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil Vaadinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaiyaana Veenai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyaana Veenai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagathai Thedinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagathai Thedinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu Theeyum Mannil Vizhaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Theeyum Mannil Vizhaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalai Thedi Thooram Sellaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalai Thedi Thooram Sellaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagai Sooda Vaazhkkai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagai Sooda Vaazhkkai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhaal Podhum Inbam Kodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhaal Podhum Inbam Kodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Poovin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Poovin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin Thugalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin Thugalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhu pogum neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhu pogum neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Izhandha vaanavil Nagalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Izhandha vaanavil Nagalo"/>
</div>
</pre>
