---
title: "ye manishike majiliyo song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Vanamali"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "Ye Manishike Majiliyo"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bOJqTkeW5lA"
type: "happy"
singers:
  - Arun Gopan
  - Chinmayi
  - Baby Anusha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye manishike majiliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye manishike majiliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paivaadu choopistaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paivaadu choopistaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu korukunte maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu korukunte maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikedi kaadantaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikedi kaadantaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nijamlaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nijamlaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu mudestunte ee nimishaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu mudestunte ee nimishaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu gatamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu gatamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kalallone unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kalallone unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pratisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pratisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee prapamchamla nanu choostunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee prapamchamla nanu choostunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu ade paniga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu ade paniga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu velestoone unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu velestoone unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nanu kadaliloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nanu kadaliloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa keratamalle vidipokunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa keratamalle vidipokunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu oka manasuleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu oka manasuleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Silalaaga maarinaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silalaaga maarinaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye manishike majiliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye manishike majiliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paivaadu choopistaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paivaadu choopistaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu korukunte maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu korukunte maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikedi kaadantaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikedi kaadantaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O madini dooram cheste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O madini dooram cheste"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkoti mudi vestaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkoti mudi vestaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaloni premanu vere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaloni premanu vere"/>
</div>
<div class="lyrico-lyrics-wrapper">Majiliki cherustaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliki cherustaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa ninnaloni aa gurutulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ninnaloni aa gurutulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee manasulonchi cheripedelaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee manasulonchi cheripedelaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisunna praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisunna praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne veru chesi bratikedelaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne veru chesi bratikedelaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee veshame ennaallanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee veshame ennaallanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi aadutondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi aadutondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee naatakaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee naatakaanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nijamlaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nijamlaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu mudestunte ee nimishaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu mudestunte ee nimishaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu gatamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu gatamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kalallone unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kalallone unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pilupu kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pilupu kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetikinidi mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetikinidi mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa varamu kori migilundi praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa varamu kori migilundi praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gundenadugoo chebutundi neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundenadugoo chebutundi neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee oopirundee nee chelimi korake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee oopirundee nee chelimi korake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kosame vechindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosame vechindile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu seda teere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu seda teere"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee prema majili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee prema majili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenu nijamlaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu nijamlaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu mudestunte ee nimishaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu mudestunte ee nimishaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu gatamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gatamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kalallone unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kalallone unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu pratisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu pratisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa prapamchamla ninu choostunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa prapamchamla ninu choostunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu ade paniga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu ade paniga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu velestoone unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu velestoone unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu ninu kadaliloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu ninu kadaliloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa keratamalle vidipokunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa keratamalle vidipokunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu oka manasuleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu oka manasuleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Silalaaga maarinaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silalaaga maarinaavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye manishike majiliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye manishike majiliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paivaadu choopistaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paivaadu choopistaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu korukunte maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu korukunte maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikedi kaadantaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikedi kaadantaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">O madini dooram cheste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O madini dooram cheste"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkoti mudi vestaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkoti mudi vestaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaloni premanu vere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaloni premanu vere"/>
</div>
<div class="lyrico-lyrics-wrapper">Majiliki cherustaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majiliki cherustaadu"/>
</div>
</pre>
