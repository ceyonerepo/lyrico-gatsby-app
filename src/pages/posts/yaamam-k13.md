---
title: "yaamam song lyrics"
album: "K 13"
artist: "Sam C.S."
lyricist: "Sam. C.S"
director: "Barath Neelakantan"
path: "/albums/k13-lyrics"
song: "Yaamam"
image: ../../images/albumart/k13.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wXKN07UU6dE"
type: "mass"
singers:
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Meeri Pochu Saamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Meeri Pochu Saamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyera Paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyera Paakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Meeri Pocho Saamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Meeri Pocho Saamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyera Paakkum Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyera Paakkum Vegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bootham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Ulla Neram Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Ulla Neram Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumarum Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumarum Ennam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikitta Kal Pettikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikitta Kal Pettikul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappikava Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappikava Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavukku Thaan Pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavukku Thaan Pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutaye Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutaye Nera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penn Solli Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Solli Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ull Vanthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ull Vanthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Illaiye Meezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Illaiye Meezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Vattama Pei Kattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Vattama Pei Kattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikitta Veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitta Veezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezha Veezha Veezha Veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha Veezha Veezha Veezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezha Veezha Veezha Veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezha Veezha Veezha Veezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Meeri Pocho Saamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Meeri Pocho Saamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyera Paakkum Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyera Paakkum Vegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bootham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Ulla Neram Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Ulla Neram Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumarum Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumarum Ennam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olam Kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam Kekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thudi Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thudi Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Thaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Thaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Vedi Vedikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Vedi Vedikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olam Kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olam Kekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thudi Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thudi Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Thaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Thaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Vedi Vedikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Vedi Vedikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munn Iravu Kaar Suzhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munn Iravu Kaar Suzhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Venniramaai Kanava Varutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venniramaai Kanava Varutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenguruthi Paakaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenguruthi Paakaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Marathi Meelalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Marathi Meelalaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Thaana Nana Naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Thaana Nana Naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Thaana Nana Naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Thaana Nana Naanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Meeri Pocho Saamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Meeri Pocho Saamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyera Paakkum Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyera Paakkum Vegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bootham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Ulla Neram Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Ulla Neram Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumarum Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumarum Ennam"/>
</div>
</pre>
