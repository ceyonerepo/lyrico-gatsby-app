---
title: "vaane vaane song lyrics"
album: "Carbon"
artist: "Sam C.S."
lyricist: "Arun Bharathi"
director: "R. Srinuvasan"
path: "/albums/carbon-lyrics"
song: "Vaane Vaane"
image: ../../images/albumart/carbon.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/INhwFSuYIiM"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaane vaane nee en vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaane vaane nee en vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan pogum paathaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan pogum paathaiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane naane yetho aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane naane yetho aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tholil saaigaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tholil saaigaiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh en vizhi en vizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh en vizhi en vizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir un imai aaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir un imai aaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madi sernthidaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi sernthidaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam idam porul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam idam porul "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam marakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam marakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aimpulan aththanaium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimpulan aththanaium "/>
</div>
<div class="lyrico-lyrics-wrapper">un kangalil sikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangalil sikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiviral korthidave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiviral korthidave "/>
</div>
<div class="lyrico-lyrics-wrapper">en agam puram adam pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en agam puram adam pidikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathai devathai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai devathai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediye nee nuzhainthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye nee nuzhainthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Devanum deivamum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devanum deivamum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaigal nee arinthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigal nee arinthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothume ithu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothume ithu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvil thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvil thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">verenna ini venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verenna ini venume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayame en kaayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayame en kaayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhi theendum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhi theendum "/>
</div>
<div class="lyrico-lyrics-wrapper">varam kettu yenguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam kettu yenguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi en nenja alai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi en nenja alai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaga uru maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga uru maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthu kudiyera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu kudiyera "/>
</div>
<div class="lyrico-lyrics-wrapper">thavikkintathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikkintathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril karaigintra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril karaigintra"/>
</div>
<div class="lyrico-lyrics-wrapper">Or uppaaga en jeevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or uppaaga en jeevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulle karaigintra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulle karaigintra "/>
</div>
<div class="lyrico-lyrics-wrapper">maayangal seithaye neeum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayangal seithaye neeum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathai devathai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai devathai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediye nee nuzhainthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye nee nuzhainthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Devanum deivamum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devanum deivamum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaigal nee arinthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigal nee arinthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenthuven nan neenthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenthuven nan neenthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonnale neruppaatril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonnale neruppaatril "/>
</div>
<div class="lyrico-lyrics-wrapper">nan neenthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan neenthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthuven kaiyil yenthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthuven kaiyil yenthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan iranthaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan iranthaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">un moochai yenthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moochai yenthuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi en nenja alai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi en nenja alai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaga uru maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaga uru maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vanthu kudiyera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanthu kudiyera "/>
</div>
<div class="lyrico-lyrics-wrapper">thavikkintathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikkintathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril karaigintra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril karaigintra"/>
</div>
<div class="lyrico-lyrics-wrapper">Or uppaaga en jeevan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or uppaaga en jeevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulle karaigintra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulle karaigintra "/>
</div>
<div class="lyrico-lyrics-wrapper">maayangal seithaye neeum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayangal seithaye neeum"/>
</div>
</pre>
