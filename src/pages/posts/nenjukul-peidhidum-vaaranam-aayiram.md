---
title: 'nenjukul peidhidum mamazhai song lyrics'
album: 'Vaaranam Aayiram'
artist: 'Harrish Jayaraj'
lyricist: 'Thamarai'
director: 'Gautham Vasudev Menon'
path: '/albums/vaaranam-aayiram-song-lyrics'
song: 'nenjukul peidhidum mamazhai'
image: ../../images/albumart/vaaranam-aayiram.jpg
date: 2008-11-14
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/FzLpP8VBC6E'
type: 'love'
singers:
- Hariharan
- Devan Ekambaram
- V.V.Prassanna
---

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Nenjukul peidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukul peidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamazhai neerukul
<input type="checkbox" class="lyrico-select-lyric-line" value="Maamazhai neerukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgidum thamarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Moozhgidum thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru marudhu vaanilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattendru marudhu vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae un mel pizhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae un mel pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nillamal veesidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillamal veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peralai nenjukul neenthidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Peralai nenjukul neenthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharagai pon vannam soodiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharagai pon vannam soodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarigai pennae nee kaanchanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaarigai pennae nee kaanchanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh shanthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanthi oh shanthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Shanthi oh shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirai uyirai
<input type="checkbox" class="lyrico-select-lyric-line" value="En uyirai uyirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyenthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen sendrai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sendrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendrai ennai thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sendrai ennai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaan endhan andhaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini neethaan endhan andhaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjukul peidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukul peidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamazhai neerukul
<input type="checkbox" class="lyrico-select-lyric-line" value="Maamazhai neerukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgidum thamarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Moozhgidum thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru marudhu vaanilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattendru marudhu vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae un mel pizhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae un mel pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yedho ondru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedho ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai eerka mookin
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai eerka mookin"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuni marmam serka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nuni marmam serka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla thanam yethum illaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalla thanam yethum illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaiyo boganvilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnagaiyo boganvilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee nindra idam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nindra idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endral vilai yeri pogaadho
<input type="checkbox" class="lyrico-select-lyric-line" value="Endral vilai yeri pogaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sellum vazhi ellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sellum vazhi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Panikatti aagaadho
<input type="checkbox" class="lyrico-select-lyric-line" value="Panikatti aagaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennodu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu varaikum
<input type="checkbox" class="lyrico-select-lyric-line" value="Veedu varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">En veetai paar
<input type="checkbox" class="lyrico-select-lyric-line" value="En veetai paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai pidikum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai pidikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ival yaaro
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaro theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival pinnal nenjae pogadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival pinnal nenjae pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu poiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu poiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyo theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Meiyo theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival pinnal nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival pinnal nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogadhae Pogadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogadhae Pogadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjukul peidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukul peidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamazhai neerukul
<input type="checkbox" class="lyrico-select-lyric-line" value="Maamazhai neerukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgidum thamarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Moozhgidum thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru marudhu vaanilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattendru marudhu vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae un mel pizhai hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae un mel pizhai hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nillamal veesidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillamal veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peralai nenjukul neenthidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Peralai nenjukul neenthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaragai pon vannam soodiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaragai pon vannam soodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarigai pennae nee kaanchanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaarigai pennae nee kaanchanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thookangalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki sendraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooki sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thooki sendraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooki sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yekkangalai thoovi sendraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yekkangalai thoovi sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnai thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pogum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veesum kaatrin
<input type="checkbox" class="lyrico-select-lyric-line" value="Veesum kaatrin"/>
</div>
<div class="lyrico-lyrics-wrapper">Veechu veru
<input type="checkbox" class="lyrico-select-lyric-line" value="Veechu veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nil endru nee sonnaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nil endru nee sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalam nagaradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaalam nagaradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee soodum poovellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee soodum poovellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru podhum udhiradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru podhum udhiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhal enai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketka villai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketka villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkathadhu kaadhal illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketkathadhu kaadhal illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En jeevan
<input type="checkbox" class="lyrico-select-lyric-line" value="En jeevan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevan neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Jeevan neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena thondrum neram idhuthaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ena thondrum neram idhuthaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee illai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endralae en nenjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Endralae en nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam thaangadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjam thaangadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjukul peidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukul peidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamazhai neerukul
<input type="checkbox" class="lyrico-select-lyric-line" value="Maamazhai neerukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgidum thamarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Moozhgidum thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru marudhu vaanilai
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattendru marudhu vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae un mel pizhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennae un mel pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nillamal veesidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillamal veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peralai nenjukul neenthidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Peralai nenjukul neenthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaragai pon vannam soodiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaaragai pon vannam soodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarigai pennae nee kaanchanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaarigai pennae nee kaanchanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh shanthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanthi oh shanthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Shanthi oh shanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirai uyirai
<input type="checkbox" class="lyrico-select-lyric-line" value="En uyirai uyirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyenthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyenthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yen sendrai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen sendrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendrai ennai thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sendrai ennai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaan endhan andhaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini neethaan endhan andhaathi"/>
</div>
</pre>

<pre class="lyrics-native">
</pre> 