---
title: "veerane vaa song lyrics"
album: "Champion"
artist: "Arrol Corelli"
lyricist: "Kabilan"
director: "Suseenthiran"
path: "/albums/champion-lyrics"
song: "Veerane Vaa"
image: ../../images/albumart/champion.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jEdtrR5d-kg"
type: "mass"
singers:
  - Nivas
  - Arrol Corelli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondu Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathai Thaandi Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathai Thaandi Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyilae Nee Thaan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyilae Nee Thaan Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Vetri Poraattamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Vetri Poraattamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Ellai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Ellai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathukku Pillai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathukku Pillai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolaiyaagum Moongil Dhaegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyaagum Moongil Dhaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Osaiyai Endrum Marakathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Osaiyai Endrum Marakathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Vaarthai Alla Adhu Vaazhkai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Vaarthai Alla Adhu Vaazhkai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Serntha Peyaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Serntha Peyaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Kondu Munnae Sendru Odi Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Kondu Munnae Sendru Odi Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Paadhi Uyir Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Paadhi Uyir Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Unnai Enni Kol Nidhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Unnai Enni Kol Nidhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Paadhi Veyil Paadhiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Paadhi Veyil Paadhiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhiyil Veesi Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiyil Veesi Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Unnai Kaiyil Thaangumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Unnai Kaiyil Thaangumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathai Thaandi Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathai Thaandi Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyilae Nee Thaan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyilae Nee Thaan Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Vetri Poraattamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Vetri Poraattamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Ellai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Ellai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathukku Pillai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathukku Pillai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolaiyaagum Moongil Dhaegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyaagum Moongil Dhaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Viral Korthu Vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Viral Korthu Vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Unnai Sutri Sutri Varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Unnai Sutri Sutri Varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyaadha Iravenbadhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaadha Iravenbadhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oduvaai Aaduvaai Thozhum Kaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oduvaai Aaduvaai Thozhum Kaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Thozhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Thozhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathai Thaandi Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathai Thaandi Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyilae Nee Thaan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyilae Nee Thaan Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Vetri Poraattamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Vetri Poraattamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Ellai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Ellai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathukku Pillai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathukku Pillai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolaiyaagum Moongil Dhaegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyaagum Moongil Dhaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Osaiyai Endrum Marakathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Osaiyai Endrum Marakathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Vaarthai Alla Adhu Vaazhkai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Vaarthai Alla Adhu Vaazhkai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Serntha Peyaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Serntha Peyaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Kondu Munnae Sendru Odi Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Kondu Munnae Sendru Odi Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondu Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondu Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeramaai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeramaai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vendru Ulagai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vendru Ulagai Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru Kondae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru Kondae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathai Thaandi Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathai Thaandi Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyilae Nee Thaan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyilae Nee Thaan Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Vetri Poraattamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Vetri Poraattamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Ellai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Ellai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathukku Pillai Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathukku Pillai Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolaiyaagum Moongil Dhaegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyaagum Moongil Dhaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thunaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Osaiyai Endrum Marakathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Osaiyai Endrum Marakathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Vaarthai Alla Adhu Vaazhkai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Vaarthai Alla Adhu Vaazhkai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Champion I Am A Champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champion I Am A Champion"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Serntha Peyaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Serntha Peyaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Kondu Munnae Sendru Odi Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Kondu Munnae Sendru Odi Vaa Daa"/>
</div>
</pre>
