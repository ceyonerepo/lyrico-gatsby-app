---
title: "indha sirruki ippo song lyrics"
album: "Kuppathu Raja"
artist: "G. V. Prakash Kumar"
lyricist: "Logan"
director: "Baba Bhaskar"
path: "/albums/kuppathu-raja-lyrics"
song: "Indha Sirruki Ippo"
image: ../../images/albumart/kuppathu-raja.jpg
date: 2019-04-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pYjpz0vELcY"
type: "love"
singers:
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Indha Sirukki Ippo Vizhundhutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Sirukki Ippo Vizhundhutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Porukki Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Porukki Thatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Muzhusaa Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Muzhusaa Avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulungittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulungittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usura Katti Valichuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usura Katti Valichuttaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju Adi Heightula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Adi Heightula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalathaan Kavuthutaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalathaan Kavuthutaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu Colorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Colorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thaan Mayakkitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thaan Mayakkitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundakkannan Muzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundakkannan Muzhiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolgithaan Kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolgithaan Kedakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Konjam Pesunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Konjam Pesunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraiya Naan Sirikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraiya Naan Sirikkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Konnuputtaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Konnuputtaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Mennuputtaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Mennuputtaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thinnuputtaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thinnuputtaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththamaa Senjuputtaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamaa Senjuputtaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bejaaraana Paiyankitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaaraana Paiyankitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana Poiyu Maatikkinnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Poiyu Maatikkinnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavula Avan Koodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula Avan Koodathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanamum Pannikinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanamum Pannikinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Ennai Paakalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Ennai Paakalanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Naalaa Kulikalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Naalaa Kulikalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinna Soru Sathiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna Soru Sathiyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukulla Serikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Serikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalillaa Kaathaadiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalillaa Kaathaadiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathula Aadavittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathula Aadavittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vootula Pammbaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vootula Pammbaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharaiyila Suthavittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyila Suthavittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Moonji Mogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Moonji Mogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pechu Muzhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Muzhiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthamaa Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthamaa Pudikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usuril Kalandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usuril Kalandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avana Pirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Pirikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaku Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parattai Patha Vechaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parattai Patha Vechaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Sokka Vechaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sokka Vechaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaa Sikkavechaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Sikkavechaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththamaa Thakavechaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamaa Thakavechaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaala Meena Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Meena Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Palanu Nee Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Palanu Nee Irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraa Meenapola Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraa Meenapola Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solattivittu Thattiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solattivittu Thattiduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanukku Onnu Inna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku Onnu Inna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saththiyamaa Thaangamaaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththiyamaa Thaangamaaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanai Pangupoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai Pangupoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaraiyum Naan Vidamaaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraiyum Naan Vidamaaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Porandhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Porandhavana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thaan Kattikikuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaan Kattikikuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Pera Nenjulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pera Nenjulathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachchaiyaa Kuththikkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchaiyaa Kuththikkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kaathu Varattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaathu Varattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu Silukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu Silukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potti Padukkaiya Eduththuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Padukkaiya Eduththuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Dappa Dance Aadapogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Dappa Dance Aadapogudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Edathai Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Edathai Paathukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Pickup Pannaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pickup Pannaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokka Keepup Pannaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Keepup Pannaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Ah Topup Pannunaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ah Topup Pannunaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocketu Takeoff Aananey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocketu Takeoff Aananey"/>
</div>
</pre>
