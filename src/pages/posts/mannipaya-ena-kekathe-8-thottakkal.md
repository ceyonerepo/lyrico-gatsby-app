---
title: "mannipaya ena kekathe song lyrics"
album: "8 Thottakkal"
artist: "KS Sundaramurthy"
lyricist: "Sri Ganesh"
director: "Sri Ganesh"
path: "/albums/8-thottakkal-lyrics"
song: "Mannipaya Ena Kekathe"
image: ../../images/albumart/8-thottakkal.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Nhcn49yjzw8"
type: "melody"
singers:
  -	Udhay Kannan
  - Aparna Balamurali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mannipaaya ena ketkaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannipaaya ena ketkaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum enai neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum enai neengaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanumae naalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanumae naalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhidum iravaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaindhidum iravaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae oosalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae oosalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugidum mezhugaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugidum mezhugaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam kondu neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam kondu neengaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjam thaangaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjam thaangaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbu endrum theeraathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu endrum theeraathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannipaaya ena ketkaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannipaaya ena ketkaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum enai neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum enai neengaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum enai neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum enai neengaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum enai neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum enai neengaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal thorumaeKoodavae varuvaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal thorumaeKoodavae varuvaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangal aatrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal aatrida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un karangalai tharuvaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un karangalai tharuvaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal maarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verethum maaraathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verethum maaraathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenguven thaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenguven thaangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Un tholgalai tharuvaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholgalai tharuvaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaatha naatkalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatha naatkalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai endru aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai endru aagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae endhan aayulum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae endhan aayulum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu neelumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu neelumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam naalum paarkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam naalum paarkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai indri pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai indri pesalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenda thooram pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda thooram pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu kondu vazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu kondu vazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannipaaya ena ketkaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannipaaya ena ketkaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum enai neengaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum enai neengaathae"/>
</div>
</pre>
