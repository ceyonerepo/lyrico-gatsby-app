---
title: "pona povura song lyrics"
album: "Mahaan"
artist: "Santhosh Narayanan"
lyricist: "Asal Kolaar"
director: "Karthik Subbaraj"
path: "/albums/mahaan-lyrics"
song: "Pona Povura"
image: ../../images/albumart/mahaan.jpg
date: 2022-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EC7qVQ7LsPA"
type: "happy"
singers:
  - Gaana Muthu
  - Asal Kolaar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pona povuranu vidavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu vidavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitalenu kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitalenu kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona povuranu vidavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu vidavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitalenu kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitalenu kathari azhuva theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee senjathellam thanniyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee senjathellam thanniyila "/>
</div>
<div class="lyrico-lyrics-wrapper">naan ezhuthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ezhuthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ethachum panna nadagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ethachum panna nadagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Company thodanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Company thodanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee senjathellam thanniyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee senjathellam thanniyila "/>
</div>
<div class="lyrico-lyrics-wrapper">naan ezhuthuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ezhuthuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ethachum panna nadagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ethachum panna nadagam "/>
</div>
<div class="lyrico-lyrics-wrapper">Company thodanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Company thodanguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkum pangu irukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum pangu irukku "/>
</div>
<div class="lyrico-lyrics-wrapper">illanu sollikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illanu sollikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukkunu sevathula naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukkunu sevathula naan "/>
</div>
<div class="lyrico-lyrics-wrapper">thalaya idchukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaya idchukala"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda unakku irukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda unakku irukka "/>
</div>
<div class="lyrico-lyrics-wrapper">kutthu vachukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutthu vachukala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthu poiyu pazhakathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu poiyu pazhakathula "/>
</div>
<div class="lyrico-lyrics-wrapper">unnaye ninachukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaye ninachukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda unakku irukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda unakku irukka "/>
</div>
<div class="lyrico-lyrics-wrapper">kutthu vachukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutthu vachukala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthu poiyu pazhakathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu poiyu pazhakathula "/>
</div>
<div class="lyrico-lyrics-wrapper">unnaye ninachukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaye ninachukkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona povuranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona povuranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona povuranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona povuranu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu "/>
</div>
<div class="lyrico-lyrics-wrapper">vidavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitalenu kathari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitalenu kathari "/>
</div>
<div class="lyrico-lyrics-wrapper">azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhuva theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sweet-ah nadanthukuna un kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet-ah nadanthukuna un kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukka ennai vaari thinnuttu poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukka ennai vaari thinnuttu poita"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai marakanumnu thoongiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai marakanumnu thoongiten"/>
</div>
<div class="lyrico-lyrics-wrapper">Angayum kanavula nuzhainju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angayum kanavula nuzhainju "/>
</div>
<div class="lyrico-lyrics-wrapper">face-ah kaatita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face-ah kaatita"/>
</div>
<div class="lyrico-lyrics-wrapper">En love-u methakkama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love-u methakkama "/>
</div>
<div class="lyrico-lyrics-wrapper">pottu pichu mulungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu pichu mulungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai pola yaaru inime 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai pola yaaru inime "/>
</div>
<div class="lyrico-lyrics-wrapper">kidachuduvan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidachuduvan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai marakama irukka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai marakama irukka "/>
</div>
<div class="lyrico-lyrics-wrapper">heart-ah konjam pazhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart-ah konjam pazhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pose-ah kathunurukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pose-ah kathunurukku "/>
</div>
<div class="lyrico-lyrics-wrapper">kaamachama vizhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamachama vizhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamachama vizhakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamachama vizhakku "/>
</div>
<div class="lyrico-lyrics-wrapper">kaamachama vilakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamachama vilakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammaloda love-ah yendi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaloda love-ah yendi "/>
</div>
<div class="lyrico-lyrics-wrapper">pothakulila pottita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothakulila pottita"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss you-nu sollura oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss you-nu sollura oru "/>
</div>
<div class="lyrico-lyrics-wrapper">nilamaya uruvakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilamaya uruvakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaloda love-ah yendi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaloda love-ah yendi "/>
</div>
<div class="lyrico-lyrics-wrapper">pothakulila pottita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothakulila pottita"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss you-nu sollura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss you-nu sollura "/>
</div>
<div class="lyrico-lyrics-wrapper">oru nilamaya uruvakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nilamaya uruvakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona povuranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona povuranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona povuranu vidavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu vidavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitalenu kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitalenu kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona povuranu vidavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona povuranu vidavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poitalenu kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poitalenu kathari azhuva theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari azhuva theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari azhuva theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari azhuva theriyala"/>
</div>
</pre>
