---
title: "manusangada song lyrics"
album: "Manusangada"
artist: "Aravind – Shankar"
lyricist: "Inquilab"
director: "Amshan Kumar"
path: "/albums/manusangada-lyrics"
song: "Manusangada"
image: ../../images/albumart/manusangada.jpg
date: 2018-10-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KNOYYxD34ng"
type: "mass"
singers:
  - KAG Agaman 
  - KAG Gunavathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pola avana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pola avana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu saanu usaramulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu saanu usaramulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pola avana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pola avana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu saanu usaramulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu saanu usaramulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engaloda maanam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda maanam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvila kedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvila kedakka"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda maanam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda maanam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvila kedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvila kedakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga izhupukkellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga izhupukkellam "/>
</div>
<div class="lyrico-lyrics-wrapper">paniyiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">engalin kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalin kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">unga izhupukkellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga izhupukkellam "/>
</div>
<div class="lyrico-lyrics-wrapper">paniyiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">engalin kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalin kanakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peththa magala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peththa magala"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi veri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi veri"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti saaikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti saaikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">peththa magala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peththa magala"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi veri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi veri"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti saaikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti saaikidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rail paathaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rail paathaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">enga udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga udambu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kedakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">rail paathaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rail paathaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">enga udambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga udambu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kedakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aduvadaikku midhicha nilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduvadaikku midhicha nilam"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu padalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu padalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aduvadaikku midhicha nilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduvadaikku midhicha nilam"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu padalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu padalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neenga aakki adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga aakki adha"/>
</div>
<div class="lyrico-lyrics-wrapper">thinda podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinda podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu padalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu padalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga aakki adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga aakki adha"/>
</div>
<div class="lyrico-lyrics-wrapper">thinda podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinda podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu padalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu padalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga paada pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga paada pona"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum theetu varalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum theetu varalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">unga paada pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga paada pona"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum theetu varalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum theetu varalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga paada kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga paada kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaa mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaa mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu vanthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu vanthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">enga paada kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga paada kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaa mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaa mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">theetu vanthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetu vanthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kazhivuneeru thottiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhivuneeru thottiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">malam mithakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malam mithakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kazhivuneeru thottiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhivuneeru thottiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">malam mithakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malam mithakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naanga kanda kanavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga kanda kanavum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga usurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga usurum"/>
</div>
<div class="lyrico-lyrics-wrapper">senthu midhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthu midhakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga kanda kanavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga kanda kanavum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga usurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga usurum"/>
</div>
<div class="lyrico-lyrics-wrapper">senthu midhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthu midhakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuppaiyoda kuppaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppaiyoda kuppaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kudusai pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudusai pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppaiyoda kuppaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppaiyoda kuppaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kudusai pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudusai pogudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga kookurala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kookurala"/>
</div>
<div class="lyrico-lyrics-wrapper">endha kaadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha kaadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kekka marukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekka marukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kookurala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kookurala"/>
</div>
<div class="lyrico-lyrics-wrapper">endha kaadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha kaadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kekka marukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekka marukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pola avana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pola avana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu saanu usaramulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu saanu usaramulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">manusangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusangada"/>
</div>
</pre>
