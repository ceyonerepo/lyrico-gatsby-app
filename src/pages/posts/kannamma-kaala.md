---
title: "kannamma song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Uma Devi"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Kannamma"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4BD0jvKhFPk"
type: "happy"
singers:
  - Pradeep Kumar
  - Dhee
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poovaaga en kaadhal thaenoorudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaaga en kaadhal thaenoorudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenaaga thaenaaga vaanoorudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenaaga thaenaaga vaanoorudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam saayama thoovaanamethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam saayama thoovaanamethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraama aaraama kayangalethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraama aaraama kayangalethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaadhal vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhegam poosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhegam poosum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal poiyaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal poiyaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeratha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaga mootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga mootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorangal madai maarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorangal madai maarumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan paarthu yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan paarthu yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pullin dhaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pullin dhaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanalgal niraivetrumoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalgal niraivetrumoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerindri meenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerindri meenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerundu vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerundu vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvingu vaazhvaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvingu vaazhvaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam saayama thoovaanamethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam saayama thoovaanamethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraama aaraama kayangalethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraama aaraama kayangalethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meettaadha veenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettaadha veenai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharukindra raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharukindra raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekaathu poogaandhalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekaathu poogaandhalae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottadha thaayin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottadha thaayin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkindra paal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkindra paal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal kidakkindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal kidakkindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayangal aatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal aatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikkodhi thettrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikkodhi thettrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal kaikooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal kaikooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduvaanam indru neduvaanam aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam indru neduvaanam aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodumneram tholaivaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodumneram tholaivaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam saayama thoovaanamethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam saayama thoovaanamethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraama aaraama kaayangalethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraama aaraama kaayangalethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
</pre>
