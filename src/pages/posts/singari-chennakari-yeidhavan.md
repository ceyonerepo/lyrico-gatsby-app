---
title: "singari chennakari song lyrics"
album: "Yeidhavan"
artist: "Paartav Barggo"
lyricist: "Paartav Barggo"
director: "Sakthi Rajasekaran"
path: "/albums/yeidhavan-lyrics"
song: "Singari Chennakari"
image: ../../images/albumart/yeidhavan.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XxSKWuxLbkY"
type: "happy"
singers:
  -	Surmugi
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathukulla thavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukulla thavam"/>
</div>
<div class="lyrico-lyrics-wrapper">kedakkan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedakkan "/>
</div>
<div class="lyrico-lyrics-wrapper">soru thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soru thani"/>
</div>
<div class="lyrico-lyrics-wrapper">marandirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marandirukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">sami kitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sami kitta "/>
</div>
<div class="lyrico-lyrics-wrapper">yedha keattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedha keattan"/>
</div>
<div class="lyrico-lyrics-wrapper">yenna thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">varam keattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam keattan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">six eight tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="six eight tu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu unnakudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu unnakudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thilla tangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thilla tangu"/>
</div>
<div class="lyrico-lyrics-wrapper">yenguda aada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenguda aada than"/>
</div>
<div class="lyrico-lyrics-wrapper">oru loadu kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru loadu kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">yerakka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerakka than"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kacheri mudivula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kacheri mudivula "/>
</div>
<div class="lyrico-lyrics-wrapper">mangalan dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangalan dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">randi babu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="randi babu"/>
</div>
<div class="lyrico-lyrics-wrapper">bagunnara meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bagunnara meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">yetta sugamano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetta sugamano"/>
</div>
<div class="lyrico-lyrics-wrapper">sakka baeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakka baeka"/>
</div>
<div class="lyrico-lyrics-wrapper">chennakithara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennakithara"/>
</div>
<div class="lyrico-lyrics-wrapper">sardhar kaisae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sardhar kaisae"/>
</div>
<div class="lyrico-lyrics-wrapper">ho thu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho thu"/>
</div>
<div class="lyrico-lyrics-wrapper">basha pala uooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="basha pala uooru"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthan veveru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthan veveru"/>
</div>
<div class="lyrico-lyrics-wrapper">asaiku yedhu neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaiku yedhu neram"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppo sevappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppo sevappo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee periya paruppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee periya paruppo"/>
</div>
<div class="lyrico-lyrics-wrapper">inga yellam samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga yellam samam"/>
</div>
<div class="lyrico-lyrics-wrapper">kashmiri appleum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kashmiri appleum"/>
</div>
<div class="lyrico-lyrics-wrapper">nagpuru orangeum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagpuru orangeum"/>
</div>
<div class="lyrico-lyrics-wrapper">kerala nendiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kerala nendiram"/>
</div>
<div class="lyrico-lyrics-wrapper">selathu mambalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selathu mambalam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thottu rusikanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thottu rusikanda"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">adhukkum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhukkum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">suvai yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvai yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">six eight tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="six eight tu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu unnakudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu unnakudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thilla tangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thilla tangu"/>
</div>
<div class="lyrico-lyrics-wrapper">yenguda aada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenguda aada than"/>
</div>
<div class="lyrico-lyrics-wrapper">oru loadu kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru loadu kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">yerakka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerakka than"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kacheri mudivula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kacheri mudivula "/>
</div>
<div class="lyrico-lyrics-wrapper">mangalan dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangalan dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yengin sudachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengin sudachi"/>
</div>
<div class="lyrico-lyrics-wrapper">podu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">oru stoppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru stoppu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandi unna pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandi unna pola"/>
</div>
<div class="lyrico-lyrics-wrapper">oyivu illama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyivu illama "/>
</div>
<div class="lyrico-lyrics-wrapper">ottitae irunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ottitae irunda"/>
</div>
<div class="lyrico-lyrics-wrapper">brake down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brake down"/>
</div>
<div class="lyrico-lyrics-wrapper">aayidum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayidum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">valaingi nelingi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaingi nelingi"/>
</div>
<div class="lyrico-lyrics-wrapper">povunda padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="povunda padha"/>
</div>
<div class="lyrico-lyrics-wrapper">odambu sorndidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu sorndidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">yen nelivu sulivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen nelivu sulivu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathutu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathutu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">uchi kulirnthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi kulirnthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">ootyila panimootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ootyila panimootam"/>
</div>
<div class="lyrico-lyrics-wrapper">delhi la kulir nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="delhi la kulir nadungum"/>
</div>
<div class="lyrico-lyrics-wrapper">andrala veyil koluthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andrala veyil koluthum"/>
</div>
<div class="lyrico-lyrics-wrapper">mumbaila mazhai kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mumbaila mazhai kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivalavu kashtangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivalavu kashtangal"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yenna serndhadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yenna serndhadum"/>
</div>
<div class="lyrico-lyrics-wrapper">parandidum nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandidum nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">maraingidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraingidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">six eight tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="six eight tu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattu unnakudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattu unnakudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thilla tangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thilla tangu"/>
</div>
<div class="lyrico-lyrics-wrapper">yenguda aada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenguda aada than"/>
</div>
<div class="lyrico-lyrics-wrapper">oru loadu kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru loadu kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">yerakka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerakka than"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kacheri mudivula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kacheri mudivula "/>
</div>
<div class="lyrico-lyrics-wrapper">mangalan dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangalan dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva athiradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva athiradi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaguku sonthakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaguku sonthakaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
<div class="lyrico-lyrics-wrapper">singari chennakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singari chennakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">iva thukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva thukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum kollakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum kollakari"/>
</div>
</pre>
