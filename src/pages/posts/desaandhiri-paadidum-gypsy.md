---
title: "desaandhiri paadidum song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Yugabharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Desaandhiri Paadidum"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bLK6IajOGMs"
type: "melody"
singers:
  - Siddharth
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Desaandhiri paadidum paadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri paadidum paadalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram ellam manidharaai aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram ellam manidharaai aadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pull poondugal poochigal patchigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull poondugal poochigal patchigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamaai vazhiyellaam maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamaai vazhiyellaam maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desaandhiri paadidum paadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri paadidum paadalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram ellam manidharaai aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram ellam manidharaai aadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pull poondugal poochigal patchigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull poondugal poochigal patchigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamaai vazhiyellaam maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamaai vazhiyellaam maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal pogura kaadugal medugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal pogura kaadugal medugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyai serkkindradhae isaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyai serkkindradhae isaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai pesidum oosaiyai kaatilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai pesidum oosaiyai kaatilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin jaadaigalae mozhigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin jaadaigalae mozhigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaiyaa odinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaiyaa odinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seralaam kadalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seralaam kadalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhumaai vaazhvathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhumaai vaazhvathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalaam udalaiyae…ae….haehe…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalaam udalaiyae…ae….haehe…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraiyil thanguvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraiyil thanguvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal nilaa solladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal nilaa solladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae selladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae selladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desa….andhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desa….andhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal pogura kaadugal medugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal pogura kaadugal medugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desaandhiri paadidum paadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri paadidum paadalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram ellam manidharaai aadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram ellam manidharaai aadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pull poondugal poochigal patchigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull poondugal poochigal patchigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhamaai vazhiyellaam maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhamaai vazhiyellaam maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvenbadhoor naatiya naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvenbadhoor naatiya naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandham serkkindradhae nadhigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandham serkkindradhae nadhigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal thaedhigal paarthidaa paadhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal thaedhigal paarthidaa paadhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellum paadhaigalae dhisaigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellum paadhaigalae dhisaigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaiyaa odinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaiyaa odinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seralaam kadalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seralaam kadalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhumaai vaazhvathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhumaai vaazhvathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalaam udalaiyae…ae….haehe…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalaam udalaiyae…ae….haehe…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraiyil thanguvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraiyil thanguvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal nilaa solladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal nilaa solladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engumae selladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engumae selladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desa….andhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desa….andhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal pogura kaadugal medugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal pogura kaadugal medugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odaiyaa odinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaiyaa odinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seralaam kadalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seralaam kadalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhumaai vaazhvathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhumaai vaazhvathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalaam udalaiyae…ae….haehe…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalaam udalaiyae…ae….haehe…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desa…andhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desa…andhiri naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desaandhiri naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desaandhiri naan"/>
</div>
</pre>
