---
title: 'jeeva nadhi song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Jeeva Nadhi'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2NeIY36kfvk"
type: 'Emotional'
singers: 
- Geetha Madhuri
- Shweta Raj
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ohooo..ooooo…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo..ooooo…ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo..ooooo…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo..ooooo…ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munaal en ranathai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munaal en ranathai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalathin kannaavai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalathin kannaavai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyilae yenthi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyilae yenthi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeva nadhi….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeva nadhi…."/>
</div>
<div class="lyrico-lyrics-wrapper">Verethum nilai illai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verethum nilai illai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ool vazhiyilae …ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ool vazhiyilae …ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhu udainthu pogirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhu udainthu pogirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeva…nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeva…nadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai thaduthoo..vanam kizhithoo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai thaduthoo..vanam kizhithoo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal nillaa nadhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal nillaa nadhi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevaa…nadhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevaa…nadhi.."/>
</div>
</pre>