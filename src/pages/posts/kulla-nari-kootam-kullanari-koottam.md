---
title: "kulla nari kootam song lyrics"
album: "Kullanari Koottam"
artist: "V. Selvaganesh"
lyricist: "Na. Muthukumar"
director: "Sribalaji"
path: "/albums/kullanari-koottam-lyrics"
song: "Kulla Nari Kootam"
image: ../../images/albumart/kullanari-koottam.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ql3ju0_peEk"
type: "mass"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Siru Narikkoottam Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Narikkoottam Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singathai Veezhthumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singathai Veezhthumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetramaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetramaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Thandhiram Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Thandhiram Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanamaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanamaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhai Vendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhai Vendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kullanari Kullanari Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kullanari Kullanari Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kullanari Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kullanari Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaikkatti Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaikkatti Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kannaamoochi Kannaamoochi Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kannaamoochi Kannaamoochi Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraaga Neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraaga Neraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappadhai Vidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappadhai Vidudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaaga Keezhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaaga Keezhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithadhai Mudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhai Mudidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Narikkoottam Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Narikkoottam Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singathai Veezhthumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singathai Veezhthumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetramaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetramaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Thandhiram Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Thandhiram Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanamaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanamaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhai Vendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhai Vendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenai Alla Then Koottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenai Alla Then Koottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Vaithaal Thavaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Vaithaal Thavaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbathai Vella Thavarellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbathai Vella Thavarellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaagum Manidhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaagum Manidhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimurai Kobam Varum Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Kobam Varum Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Mudhal Paadham Sudum Sudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Mudhal Paadham Sudum Sudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandanai Yaavum Tharum Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandanai Yaavum Tharum Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumam Saagaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumam Saagaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyinil Alaigal Thodum Thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyinil Alaigal Thodum Thodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhadhum Meendum Varum Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaindhadhum Meendum Varum Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyum Adhupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyum Adhupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarndhidum Endrum Thoongaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarndhidum Endrum Thoongaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraaga Neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraaga Neraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappadhai Vidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappadhai Vidudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaaga Keezhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaaga Keezhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithadhai Mudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhai Mudidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Narikkoottam Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Narikkoottam Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singathai Veezhthumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singathai Veezhthumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetramaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetramaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Thandhiram Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Thandhiram Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanamaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanamaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhai Vendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhai Vendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Peyai Pola Engengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Peyai Pola Engengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Lanjathin Kodumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lanjathin Kodumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiyellaam Thee Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiyellaam Thee Vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erithaal Thaan Inimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erithaal Thaan Inimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bharadha Naattil Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharadha Naattil Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivirithaadum Panam Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivirithaadum Panam Panam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarithaikketpaar Idam Valam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarithaikketpaar Idam Valam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Peyaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Peyaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaignargal Koottam Varum Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaignargal Koottam Varum Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhandhathaiyellaam Perum Perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhandhathaiyellaam Perum Perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inivarum Kaalam Sugam Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inivarum Kaalam Sugam Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Therottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Therottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraaga Neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraaga Neraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappadhai Vidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappadhai Vidudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Keezhaaga Keezhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Keezhaaga Keezhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithadhai Mudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithadhai Mudidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Narikkoottam Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Narikkoottam Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singathai Veezhthumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singathai Veezhthumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetramaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetramaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Thandhiram Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Thandhiram Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavanamaai Sendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavanamaai Sendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhai Vendrida Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhai Vendrida Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara Ushaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara Ushaar"/>
</div>
</pre>
