---
title: "nadiyon paar song lyrics"
album: "Roohi"
artist: "Sachin-Jigar"
lyricist: "IP Singh -Jigar Saraiya"
director: "Hardik Mehta"
path: "/albums/roohi-lyrics"
song: "Nadiyon Paar"
image: ../../images/albumart/roohi.jpg
date: 2021-03-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/DKj5m9cSMZs"
type: "happy"
singers:
  - Shamur
  - Rashmeet Kaur
  - IP Singh
  - Sachin-Jigar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho nadiyon paar sajjan da thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nadiyon paar sajjan da thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte kol zaruri jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte kol zaruri jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiyon paar sajjan da thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyon paar sajjan da thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiyon paar sajjan da thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyon paar sajjan da thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte kol zaroori jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte kol zaroori jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte kol zaroori jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte kol zaroori jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayi na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayi na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayi na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayi na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooh aah let the music play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh aah let the music play"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh aah let the music play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh aah let the music play"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera pichhe tutteya dil mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera pichhe tutteya dil mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mainu hosh na hun haasil bismil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mainu hosh na hun haasil bismil"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera ban ke main phirda haan sohneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera ban ke main phirda haan sohneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu kol aaja mere oh bewafa saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kol aaja mere oh bewafa saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya milne vaaste tu mujhko bhejh de naiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya milne vaaste tu mujhko bhejh de naiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kol aaja mere oh bewafa saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kol aaja mere oh bewafa saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya milne vaaste tu mujhko bhejh de naiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya milne vaaste tu mujhko bhejh de naiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiyon paar sajjan da thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyon paar sajjan da thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiyon paar sajjan da thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyon paar sajjan da thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte kol zaroori jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte kol zaroori jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte kol zaroori jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte kol zaroori jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil laa leya beparwah de naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil laa leya beparwah de naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayi na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayi na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayi na kar maan rupaiye wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayi na kar maan rupaiye wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ban ban behnda agge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ban ban behnda agge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooh aah let the music play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh aah let the music play"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh aah let the music play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh aah let the music play"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play the music!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play the music!"/>
</div>
</pre>
