---
title: "muthunne kannukalil song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Muthunne Kannukalil"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/dUQdgy0jV_4"
type: "happy"
singers:
  - Swetha Mohan
  - Swetha Somasundaran	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muthunne Kannukalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthunne Kannukalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Ponkanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Ponkanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhirashi Nee Kanavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhirashi Nee Kanavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Niram Koottunnavaril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram Koottunnavaril"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvore Ethirelkkuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvore Ethirelkkuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theru Nadavazhikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Nadavazhikalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthunne Kannukalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthunne Kannukalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Ponkanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Ponkanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhirashi Nee Kanavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhirashi Nee Kanavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Niram Koottunnavaril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram Koottunnavaril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallikamalar Choodi Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallikamalar Choodi Aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallikamalar Choodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallikamalar Choodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Palluvar Kural Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palluvar Kural Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Chilmabu Aniyum Sakhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Chilmabu Aniyum Sakhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Meettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Meettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Nadayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Nadayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hridayam Niraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hridayam Niraye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oraayiram Mohangalumaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oraayiram Mohangalumaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ekum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ekum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaazhchakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaazhchakalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennum Ennennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennum Ennennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirakalilaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirakalilaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaveshavum Ullasavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaveshavum Ullasavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarnnu Thudichu Parnnidaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarnnu Thudichu Parnnidaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Davani Azhakinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davani Azhakinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Davani Azhakinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davani Azhakinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelezhum Ilam Praayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelezhum Ilam Praayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaratha Tamil Penkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaratha Tamil Penkodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kouthmaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kouthmaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ororo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ororo"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Manameki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Manameki"/>
</div>
</pre>
