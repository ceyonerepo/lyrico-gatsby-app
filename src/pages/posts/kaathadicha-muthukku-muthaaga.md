---
title: "kaathadicha song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Kaathadicha"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2-thRUxuVIo"
type: "sad"
singers:
  - Krishna Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaathadichaa noagumunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadichaa noagumunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">poththi poththi valartheega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poththi poththi valartheega"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppasaami kannukkuththummunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppasaami kannukkuththummunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">soarootti valartheega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soarootti valartheega"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru kannu pattudummunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru kannu pattudummunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">suththi suththi poatteega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suththi suththi poatteega"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaala aagaadha pillaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaala aagaadha pillaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">peththu kanneera vitteega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peththu kanneera vitteega"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha oru jenmathula neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha oru jenmathula neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">En pullaiyaa porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pullaiyaa porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakkitta patta kadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakkitta patta kadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vandhu adaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vandhu adaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan porandha sedhiyathaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan porandha sedhiyathaan "/>
</div>
<div class="lyrico-lyrics-wrapper">oorakkootti sonneenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorakkootti sonneenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhukkuththunaa valikkumunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhukkuththunaa valikkumunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">kanna renda mooduneenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna renda mooduneenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Karikkozhambu pudikkumunnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karikkozhambu pudikkumunnu "/>
</div>
<div class="lyrico-lyrics-wrapper">kadan vaangi senjeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadan vaangi senjeenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadangaaranaa porandha enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadangaaranaa porandha enna "/>
</div>
<div class="lyrico-lyrics-wrapper">paarththu paarththu nondheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarththu paarththu nondheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha oru jenmathula neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha oru jenmathula neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">En pullaiyaa porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pullaiyaa porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungakkitta patta kadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungakkitta patta kadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vandhu adaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vandhu adaikkanum"/>
</div>
</pre>
