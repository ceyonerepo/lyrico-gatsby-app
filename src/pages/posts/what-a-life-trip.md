---
title: "what a life song lyrics"
album: "Trip"
artist: "Siddhu Kumar"
lyricist: "Mohan Rajan"
director: "Dennis Manjunath"
path: "/albums/trip-lyrics"
song: "What a Life"
image: ../../images/albumart/trip.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kvC_bkZeCH0"
type: "Gana"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">what a life-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what a life-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">aachi knife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachi knife-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">katrina kaif-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrina kaif-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukku wife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukku wife-fu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">left-tu pona right-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left-tu pona right-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">right-tu pona left-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right-tu pona left-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga pona muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga pona muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnudatha moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnudatha moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnuputta pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnuputta pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyala pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyala pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">left-tu pona right-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left-tu pona right-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">right-tu pona left-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right-tu pona left-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga pona muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga pona muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnudatha moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnudatha moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnuputta pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnuputta pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyala pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyala pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla"/>
</div>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">zomato delivery boy-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zomato delivery boy-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">odurom paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odurom paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">tomato juice-ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tomato juice-ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nasungurom naanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nasungurom naanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">blue satta maaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="blue satta maaran"/>
</div>
<div class="lyrico-lyrics-wrapper">kitta sikkuna padamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta sikkuna padamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyama maatikittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyama maatikittom"/>
</div>
<div class="lyrico-lyrics-wrapper">thappikka mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappikka mudiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patha vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">namuthupona rocket
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namuthupona rocket"/>
</div>
<div class="lyrico-lyrics-wrapper">oda ninaichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda ninaichi"/>
</div>
<div class="lyrico-lyrics-wrapper">nondi adikkum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nondi adikkum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">athha pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athha pola "/>
</div>
<div class="lyrico-lyrics-wrapper">naanga maari ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga maari ponam"/>
</div>
<div class="lyrico-lyrics-wrapper">ai ai ai ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ai ai ai ai"/>
</div>
<div class="lyrico-lyrics-wrapper">ai ai ai oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ai ai ai oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mutti thenji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutti thenji"/>
</div>
<div class="lyrico-lyrics-wrapper">moonji kilinchi pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonji kilinchi pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">GPS-um signal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GPS-um signal"/>
</div>
<div class="lyrico-lyrics-wrapper">la-laa pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la-laa pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum da pothum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum da pothum da"/>
</div>
<div class="lyrico-lyrics-wrapper">ada oda mudiyala daaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada oda mudiyala daaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nakkalites pola naanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakkalites pola naanga "/>
</div>
<div class="lyrico-lyrics-wrapper">nettula suththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nettula suththala"/>
</div>
<div class="lyrico-lyrics-wrapper">naxalite pola naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naxalite pola naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">suthurom kaatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthurom kaatula"/>
</div>
<div class="lyrico-lyrics-wrapper">big-bossu yengalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="big-bossu yengalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarunu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarunu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aana intha veetukkulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana intha veetukkulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkavey mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkavey mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">what a life-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what a life-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">aachi knife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachi knife-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">katrina kaif-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrina kaif-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukku wife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukku wife-fu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada guindy race su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada guindy race su"/>
</div>
<div class="lyrico-lyrics-wrapper">kudhirai yellam inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudhirai yellam inga"/>
</div>
<div class="lyrico-lyrics-wrapper">gundu satti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundu satti"/>
</div>
<div class="lyrico-lyrics-wrapper">kughiraiyaga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kughiraiyaga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">nondi mela nondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nondi mela nondi"/>
</div>
<div class="lyrico-lyrics-wrapper">adikkutheyda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikkutheyda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ap ap pa pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ap ap pa pa "/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anju nodiyil venthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anju nodiyil venthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pora noodles
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora noodles"/>
</div>
<div class="lyrico-lyrics-wrapper">pola naanga urundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola naanga urundu"/>
</div>
<div class="lyrico-lyrics-wrapper">surundu porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surundu porom"/>
</div>
<div class="lyrico-lyrics-wrapper">ennada nadakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada nadakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada onnum puriyala daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada onnum puriyala daa"/>
</div>
<div class="lyrico-lyrics-wrapper">temple run pola naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="temple run pola naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">odurom thaavurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odurom thaavurom"/>
</div>
<div class="lyrico-lyrics-wrapper">help panna yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="help panna yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">thappikka paakurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappikka paakurom"/>
</div>
<div class="lyrico-lyrics-wrapper">yengala vachi inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengala vachi inga"/>
</div>
<div class="lyrico-lyrics-wrapper">evano oruthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evano oruthan"/>
</div>
<div class="lyrico-lyrics-wrapper">pub-g game aaduranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pub-g game aaduranda"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkavey mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkavey mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">what a life-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what a life-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">aachi knife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachi knife-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">katrina kaif-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrina kaif-fu"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukku wife-fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukku wife-fu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">left-tu pona right-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left-tu pona right-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">right-tu pona left-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right-tu pona left-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga pona muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga pona muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnudatha moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnudatha moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnuputta pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnuputta pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyala pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyala pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">left-tu pona right-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left-tu pona right-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">right-tu pona left-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right-tu pona left-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga pona muttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga pona muttu"/>
</div>
<div class="lyrico-lyrics-wrapper">santhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnudatha moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnudatha moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnuputta pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnuputta pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyala pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyala pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla"/>
</div>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla"/>
</div>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla "/>
</div>
<div class="lyrico-lyrics-wrapper">lal lal lal laaa lalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lal lal lal laaa lalla"/>
</div>
</pre>
