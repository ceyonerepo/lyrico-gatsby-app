---
title: "pini kangirene karbini song lyrics"
album: "Theethum Nandrum"
artist: "C. Sathya"
lyricist: "Muthamizh"
director: "Rasu Ranjith"
path: "/albums/theethum-nandrum-lyrics"
song: "Pini Kangirene Karbini"
image: ../../images/albumart/theethum-nandrum.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Zvy5aFrBEUs"
type: "Sad"
singers:
  - Srinithi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manasellam sariyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam sariyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mala pola aluvuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala pola aluvuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasellam sariyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam sariyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mala pola aluvuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala pola aluvuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">madi thaanga malar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi thaanga malar"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu arumbuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu arumbuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasellam sariyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam sariyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mala pola aluvuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala pola aluvuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iru noolum idam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru noolum idam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">muduchu inga mulikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduchu inga mulikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">iru noolum idam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru noolum idam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">muduchu inga mulikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduchu inga mulikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyatha vidhi ingu nerunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyatha vidhi ingu nerunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasellam sariyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam sariyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mala pola aluvuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala pola aluvuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattilu mukula thoonguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattilu mukula thoonguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">patiyil aadu nonduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patiyil aadu nonduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en rathiri veruchoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en rathiri veruchoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thanima thala viruchu aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanima thala viruchu aaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">illatha idam thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha idam thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumara thallangam velaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumara thallangam velaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadha kota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadha kota"/>
</div>
<div class="lyrico-lyrics-wrapper">vala valanu vayiru valaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala valanu vayiru valaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">varutham than payira velaiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varutham than payira velaiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">varanju vacha kolam kalaiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanju vacha kolam kalaiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaluvellam elagi karaiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluvellam elagi karaiyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pini kangirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene"/>
</div>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manjam kanda maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjam kanda maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">makki ponathu ipa...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makki ponathu ipa..."/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nanjam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nanjam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kotuthu neruppu kuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotuthu neruppu kuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kiranguthu alumbadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kiranguthu alumbadi"/>
</div>
<div class="lyrico-lyrics-wrapper">inno nerambuthe perumbadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inno nerambuthe perumbadi"/>
</div>
<div class="lyrico-lyrics-wrapper">muthaga pethu paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthaga pethu paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathu sutha sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathu sutha sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthaga en valiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthaga en valiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">nikatha ethiroliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikatha ethiroliyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pini kangirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene"/>
</div>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<div class="lyrico-lyrics-wrapper">pini kangirene karbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pini kangirene karbini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manasellam athisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam athisayam"/>
</div>
<div class="lyrico-lyrics-wrapper">mala penja oru sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala penja oru sugam"/>
</div>
</pre>
