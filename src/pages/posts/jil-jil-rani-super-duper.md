---
title: "jil jil rani song lyrics"
album: "Super Duper"
artist: "Divakara Thiyagarajan"
lyricist: "AK"
director: "Arun Karthik"
path: "/albums/super-duper-lyrics"
song: "Jil Jil Rani"
image: ../../images/albumart/super-duper.jpg
date: 2019-09-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8gTpGsg4dbk"
type: "happy"
singers:
  - Ananya Bhat
  - Saisharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeh Pettaiya Kalakkum Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Pettaiya Kalakkum Mayilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aayiram Wala Pattasu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aayiram Wala Pattasu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Pettaiya Kalakkum Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Pettaiya Kalakkum Mayilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aayiram Wala Pattasu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aayiram Wala Pattasu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagula Serum Thimiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Serum Thimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Sugara Enaku Set-ah Aachi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Sugara Enaku Set-ah Aachi Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate-u Mela Oru Cover-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate-u Mela Oru Cover-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Pichipottu Pakkuvama Rusi Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pichipottu Pakkuvama Rusi Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasi Mettula Effil Tower-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Mettula Effil Tower-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Passuport Illama Kudiyeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Passuport Illama Kudiyeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjada Paaruda Kanjavin Kaaduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjada Paaruda Kanjavin Kaaduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennuku Sikkaadha Aal Yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuku Sikkaadha Aal Yaaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukka Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukka Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Angel Vesham Potta Annabell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Angel Vesham Potta Annabell-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Alukki Kulukki Nadantha House Full-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Alukki Kulukki Nadantha House Full-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jallikattu Naanum Jallikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattu Naanum Jallikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Beta Vandhalum Oramkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Beta Vandhalum Oramkattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyila Ottum Sticker Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyila Ottum Sticker Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Idupil Iragum Varaikum Mallukattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Idupil Iragum Varaikum Mallukattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirunthu Poo Pari Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthu Poo Pari Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirunthu Poo Pari Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthu Poo Pari Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice-ula Ninaincha Theepori Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice-ula Ninaincha Theepori Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennazhagu Thenkanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennazhagu Thenkanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Velu Nee Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Velu Nee Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukka Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukka Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Angel Vesham Potta Annabell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Angel Vesham Potta Annabell-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Alukki Kulukki Nadantha House Full-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Alukki Kulukki Nadantha House Full-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Mayakkum Mandhaakini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Mayakkum Mandhaakini"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Vaa En Heronine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaa En Heronine"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Thunaiyaa Neeyum Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Thunaiyaa Neeyum Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkka Parakkum Lambohini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkka Parakkum Lambohini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Molaicha Thamaraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Molaicha Thamaraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasula Aasaiya Sekkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasula Aasaiya Sekkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vandha Thevadhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vandha Thevadhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathula Raattinam Otturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathula Raattinam Otturiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniya Vaazhum Intha Valibathin Valley Ball-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Vaazhum Intha Valibathin Valley Ball-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Game Aadi Kaandae Ethi Pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game Aadi Kaandae Ethi Pogatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal Rani Vava En Nenjodu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Rani Vava En Nenjodu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sweetie Beautie Cuttie Konjida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sweetie Beautie Cuttie Konjida Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukka Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinukka Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Angel Vesham Potta Annabell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Angel Vesham Potta Annabell-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Jingle Bell-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Jingle Bell-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Alukki Kulukki Nadantha House Full-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Alukki Kulukki Nadantha House Full-u"/>
</div>
</pre>
